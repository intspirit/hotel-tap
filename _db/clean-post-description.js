'use strict'

import dbBase from '../server/db/dbBase'

// RUN
// NODE_ENV=development node_modules/.bin/babel-node _db/clean-post-description.js

////////////////////////////
// DEPARTMENT TAGS
////////////////////////////
let depTagsLinks = []

// SEARCH: LIKE %data-type="dept"%
// TODO: ~350 RECORDS ARE NOT PROCESSED

// <span id=""><a href="" rel="" id="" class="dept" data-dept="">@Foo</a></span>
// any text except html tags
// <span id=""><a href="" rel="" id="" class="tag" data-type="dept">#Bar</a></span>
depTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/\d+" rel="\d+" id="\d+" class="dept" data-dept="\d+">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" id="(\d+)" class="tag" data-type="dept">#([^<]+)<\/a><\/span>/g)

// <span id=""><a class="dept" id=""  href="" rel="" data-dept="">@Foo</a></span>
// any text except html tags
// <span id=""><a class="tag" id="" href="" rel="" data-type="dept">#Bar</a></span>
depTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a class="dept" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/\d+" rel="\d+" data-dept="\d+">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a class="tag" id="(\d+)" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" data-type="dept">#([^<]+)<\/a><\/span>/g)

////////////////////////////
// LOCATION/AREA TAGS
////////////////////////////
let areaTagsLinks = []

// SEARCH: LIKE %data-type="area"%
// TODO: ~1200 RECORDS ARE NOT PROCESSED

// <span id=""><a href="" rel="" id="" class="area" data-dept="0">@Foo</a></span>
// any text except html tags
// <span id=""><a href="" rel="" id="" class="tag" data-type="area">#Bar</a></span>
areaTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" id="\d+" class="area" data-dept="0">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" id="(\d+)" class="tag" data-type="area">#([^<]+)<\/a><\/span>/g)

// <span id=""><a class="area" id="" href="" rel="" data-dept="0">@Foo</a></span>
// any text except html tags
// <span id=""><a class="tag" id="" href="" rel="" data-type="area">#Bar</a></span>
areaTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a class="area" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" data-dept="0">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a class="tag" id="(\d+)" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" data-type="area">#([^<]+)<\/a><\/span>/g)

////////////////////////////
// MAINTENANCE TAGS
////////////////////////////
let mtypeTagsLinks = []

// SEARCH: LIKE %data-type="mtype"%
// TODO: ~210 RECORDS ARE NOT PROCESSED

// <span id=""><a href="" rel="" id="" class="mtype" data-dept="0">@Foo</a></span>
// any text except html tags
// <span id=""><a href="" rel="" id="" class="tag" data-type="mtype">#Bar</a></span>
mtypeTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/mboard\/\d+" rel="\d+" id="\d+" class="mtype" data-dept="\d+">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" id="(\d+)" class="tag" data-type="mtype">#([^<]+)<\/a><\/span>/g)

// <span id=""><a class="mtype" id="" href="" rel="" data-dept="0">@Foo</a></span>
// any text except html tags
// <span id=""><a class="tag" id="" href="" rel="" data-type="mtype">#Bar</a></span>
mtypeTagsLinks.push(/<span id="\d+"(?:\sstyle="[^"]*")?><a class="mtype" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/mboard\/\d+" rel="\d+" data-dept="\d+">@([^<]+)<\/a><\/span>[^<]*<span id="\d+"(?:\sstyle="[^"]*")?><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" class="tag" id="(\d+)" data-type="mtype">#([^<]+)<\/a><\/span>/g)

////////////////////////////
// GUEST COMPLIANT TAGS
////////////////////////////
let gcTagsLinks = []

// SEARCH: LIKE %%23GuestComplaint%
// TODO: ~10 RECORDS ARE NOT PROCESSED

// <span id=""><a href="" rel="" id="" class="tag tag-red">#GuestComplaint</a></span>
gcTagsLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+" id="\d+" class="tag tag\-red">#GuestComplaint<\/a><\/span>/g)

// <a href="" rel="" id="" class="tag tag-red">#GuestComplaint</a>
gcTagsLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+" id="\d+" class="tag tag\-red">#GuestComplaint<\/a>/g)

// <span id="" style=""><a href="" rel="" id="" class="tag tag-red">#GuestComplaint</a></span>
gcTagsLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+" id="\d+" class="tag tag\-red">#GuestComplaint<\/a><\/span>/g)

// <span style="" id=""><a id="" class="tag tag-red" href="" rel="">#GuestComplaint</a></span>
gcTagsLinks.push(/<span style="[^"]*" id="\d+"><a id="\d+" class="tag tag\-red" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+">#GuestComplaint<\/a><\/span>/g)

// <span id=""><a class="tag tag-red" id="" href="" rel="" >#GuestComplaint</a></span>
gcTagsLinks.push(/<span id="\d+"><a class="tag tag\-red" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+">#GuestComplaint<\/a><\/span>/g)

// <span style="" id=""><a id="" class="tag tag-red" href="" rel="" data-type="dept">#GuestComplaint</a></span>
gcTagsLinks.push(/<span style="[^"]*" id="\d+"><a id="\d+" class="tag" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/%23GuestComplaint" rel="\d+" data-type="dept">#GuestComplaint<\/a><\/span>/g)

////////////////////////////
// EMPLOYEE BOARDS
////////////////////////////
let empBoardLinks = []

// SEARCH: LIKE %class="emp"%
// TODO: ~63 RECORDS ARE NOT PROCESSED

// <span id=""><a href="" rel="" id="" class="emp" data-dept="0">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp" data\-dept="0">@([^<]+)<\/a><\/span>/g)

// <a href="" rel="" id="" class="emp" data-dept="0">@Something</a>
empBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp" data\-dept="0">@([^<]+)<\/a>/g)

// <span id="" style=""><a href="" rel="" id="" class="emp" data-dept="0">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp" data\-dept="0">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a href="" rel="" id="" class="emp">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp">@([^<]+)<\/a><\/span>/g)

// <span id=""><a href="" rel="" id="" class="emp">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp">@([^<]+)<\/a><\/span>/g)

// <a href="" rel="" id="" class="emp">@Something</a>
empBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" id="\d+" class="emp">@([^<]+)<\/a>/g)

// <span id=""><a href="" id="" class="emp" rel="">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" id="\d+" class="emp" rel="\d+">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a href="" id="" class="emp" rel="">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" id="\d+" class="emp" rel="\d+">@([^<]+)<\/a><\/span>/g)

// <span id="" ><a class="emp" id="" href="" rel="" data-dept="0">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a class="emp" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" data\-dept="0">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a class="emp" id="" href="" rel="">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a class="emp" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a class="emp" id="" href="" rel="" data-dept="0">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a class="emp" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" data\-dept="0">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a class="emp" id="" href="" rel="">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a class="emp" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+">@([^<]+)<\/a><\/span>/g)

// <span id=""><a href="" rel="" class="emp">@Something</a></span>
empBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" class="emp">@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a href="" rel="" class="emp">@Something</a></span>
empBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/empboard\/(\d+)" rel="\d+" class="emp">@([^<]+)<\/a><\/span>/g)

////////////////////////////
// DEPARTMENTS BOARDS
////////////////////////////
let depBoardLinks = []

// SEARCH: LIKE %data-type="dept"%
// TODO: ~350 RECORDS ARE NOT PROCESSED (same as)

// <span id=""><a href="" rel="" id="" class="dept" data-dept="">@Something</a></span>
depBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept" data\-dept="\d+">\s*@([^<]+)<\/a><\/span>/g)

// <a href="" rel="" id="" class="dept" data-dept="">@Something</a><
depBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept" data\-dept="\d+">\s*@([^<]+)<\/a>/g)

// <span id=""><a href="" rel="" id="" class="dept">@Something</a></span>
depBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept">\s*@([^<]+)<\/a><\/span>/g)

// <span style="" id=""><a href="" rel="" id="" class="dept" data-dept="">@Something</a></span>
depBoardLinks.push(/<span style="[^"]*" id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept" data\-dept="\d+">\s*@([^<]+)<\/a><\/span>/g)

// <span style="" id=""><a href="" rel="" id="" class="dept">@Something</a></span>
depBoardLinks.push(/<span style="[^"]*" id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept">\s*@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a href="" rel="" id="" class="dept" data-dept="">@Something</a></span>
depBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept" data\-dept="\d+">\s*@([^<]+)<\/a><\/span>/g)

// <span id="" style=""><a href="" rel="" id="" class="dept">@Something</a></span>
depBoardLinks.push(/<span id="\d+" style="[^"]*"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" id="\d+" class="dept">\s*@([^<]+)<\/a><\/span>/g)

// <a href="" rel="" class="dept">@Something</a></span>
depBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" class="dept">\s*@([^<]+)<\/a>/g)

// <span id=""><a href="" rel="" class="dept">@Something</a></span>
depBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" class="dept">\s*@([^<]+)<\/a><\/span>/g)

// <a href="" rel="" class="dept">@Something</a>
depBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+" class="dept">\s*@([^<]+)<\/a>/g)

// <span id=""><a href="" id="" class="dept" rel="">@Something</a></span>
depBoardLinks.push(/<span id="\d+"><a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" id="\d+" class="dept" rel="\d+">\s*@([^<]+)<\/a><\/span>/g)

// <a href="" id="" class="dept" rel="">@Something</a>
depBoardLinks.push(/<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" id="\d+" class="dept" rel="\d+">\s*@([^<]+)<\/a>/g)

// <span id=""><a class="dept" href="" rel="" id="">@Something</a></span>
depBoardLinks.push(/<span id="\d+"><a class="dept" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" id="\d+">\s*@([^<]+)<\/a><\/span>/g)

// <a class="dept" href="" rel="" id="">@Something</a>
depBoardLinks.push(/<a class="dept" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" id="\d+">\s*@([^<]+)<\/a>/g)

// <span id=""><a class="dept" id="" href="" rel="">@Something</a></span>
depBoardLinks.push(/(?:<span id="\d+">)?<a class="dept" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+">\s*@([^<]+)<\/a>(?:<\/span>)?/g)

// <a class="dept" id="" href="" rel="">@Something</a>
depBoardLinks.push(/<a class="dept" id="\d+" href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/board\/(\d+)" rel="\d+">\s*@([^<]+)<\/a>/g)

// DEBUG ONLY
const customTag1 = /(?:<span id="\d+">)?<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" id="\d+" class="tag" rel="\d+"(?:\s?data\-type="(?:dept|equip)")?>[^<]*<\/a>(?:<\/span>)?/g
const customTag2 = /(?:<span id="\d+">)?<a href="https?:\/\/(?:hotelworks|(?:stage\.|app\.)?hoteltap)\.com\/index.php\/hotel\/search\/[^"]*" rel="\d+" id="\d+" class="tag"(?:\s?data\-type="(?:dept|equip)")?>[^<]*<\/a>(?:<\/span>)?/g
const customTag3 = /(?:<span id="\d+">)?<a href="https?:\/\/hotelworks\.com\/?" rel="\d+" id="\d+" class="tag"(?:\s?data\-type="(?:dept|equip)")?>[^<]*<\/a>(?:<\/span>)?/g

const replace = (patterns, str, format) =>
	patterns.reduce((text, x) => text.replace(x, format), str)

const run = async () => {
	const sql = `SELECT posts.* FROM posts`
	const where = []
	const dbData = await dbBase.findAll(sql, where, null)
	let processedRows = 0

	for (let i = 0, length = dbData.length; i < length; i++) {//10340
		const x = dbData[i]
		let originalDescription = x.description
		let description = x.description

		if (description) {

			// DEBUG ONLY
			//description = description.replace(/<span><\/span>/, '')

			description = replace(depTagsLinks, description, '<a href="/boards/tags/$2">@$1 #$3</a>')
			description = replace(areaTagsLinks, description, '<a href="/boards/tags/$2">@$1 #$3</a>')
			description = replace(mtypeTagsLinks, description, '<a href="/boards/tags/$2">@$1 #$3</a>')
			description = replace(gcTagsLinks, description, '<a href="/boards/complaints">#GuestComplaint</a>')

			description = replace(empBoardLinks, description, '<a href="/boards/employee/$1">@$2</a>')
			description = replace(depBoardLinks, description, '<a href="/boards/departments/$1">@$2</a>')

			// DEBUG ONLY
			// description = description.replace(customTag1, '')
			// description = description.replace(customTag2, '')
			// description = description.replace(customTag3, '')

			if (description !== originalDescription) {
				const updateSql = `
				UPDATE posts SET
					description = :description
				WHERE id = :id`
				const criteria = {
					id: x.id,
					description
				}
				const updatedRows = await dbBase.update(updateSql, criteria)

				if (updatedRows === 0) {
					console.log(`FAILED SQL = ${updateSql}, criteria = ${criteria}`)
				} else {
					console.log(`Processed post id = ${x.id}`)
					processedRows++
				}
			} else {
				console.log(`Skipped post id = ${x.id}`)
			}
		}
	}

	console.log(`Processed rows = ${processedRows}`)

	return processedRows
}

if (require.main === module) {
	run()
}

module.exports = {
	run: run
}
