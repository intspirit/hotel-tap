ALTER TABLE `posts`
ADD COLUMN `location_type_id` VARCHAR(5) NULL COMMENT 'area or room' AFTER `guest_complaint`,
ADD COLUMN `location_id` BIGINT(20) NULL AFTER `location_type_id`;
