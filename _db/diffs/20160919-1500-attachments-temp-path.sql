ALTER TABLE `attachements`
	ADD COLUMN `tmp_file_path` varchar(255) DEFAULT NULL AFTER `file_path`,
	ADD INDEX `tmp_file_path` (`tmp_file_path`);
