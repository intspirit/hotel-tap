ALTER TABLE `post_users`
	DROP PRIMARY KEY;

ALTER TABLE `post_users`
	ADD COLUMN `child_id` BIGINT(20) DEFAULT 0 AFTER `user_id`;

ALTER TABLE `post_users`
	ADD PRIMARY KEY (`hotel_id`,`post_id`, `user_id`, `child_id`, `category`);
