ALTER TABLE `languages`
	ADD COLUMN `translating_phrase` NVARCHAR(255) NULL;

UPDATE languages SET translating_phrase = 'Translating ...'           WHERE code = 'en';
UPDATE languages SET translating_phrase = 'Traducción en curso ...'   WHERE code = 'es';
UPDATE languages SET translating_phrase = 'Traduction en cours ...'   WHERE code = 'fr';
UPDATE languages SET translating_phrase = 'Wird übersetzt ...'        WHERE code = 'de';
UPDATE languages SET translating_phrase = 'Dịch ...'                  WHERE code = 'vi';
UPDATE languages SET translating_phrase = 'Перевод в процессе ...'    WHERE code = 'ru';
UPDATE languages SET translating_phrase = '번역 중 ...'                 WHERE code = 'ko';
UPDATE languages SET translating_phrase = 'Traduzione in corso ...'   WHERE code = 'it';
UPDATE languages SET translating_phrase = '翻訳しています ...'          WHERE code = 'ja';
UPDATE languages SET translating_phrase = 'Tradução em andamento ...' WHERE code = 'pt';
UPDATE languages SET translating_phrase = '正在翻译 ...'               WHERE code = 'zh-CHS';

