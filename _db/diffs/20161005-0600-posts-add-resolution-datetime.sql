ALTER TABLE `posts`
	ADD COLUMN `resolution_dtm` DATETIME NULL AFTER `points_given`,
	ADD COLUMN `resolution_dtm_utc` DATETIME NULL AFTER `resolution_dtm`;
