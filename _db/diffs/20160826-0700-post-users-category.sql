ALTER TABLE `post_users` ADD COLUMN `category` VARCHAR(3) NULL DEFAULT 'cr' AFTER `order_dtm_utc`;
ALTER TABLE `post_users` DROP PRIMARY KEY;
ALTER TABLE `post_users` ADD PRIMARY KEY (`hotel_id`, `post_id`, `user_id`, `category`);
