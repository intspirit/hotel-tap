ALTER TABLE `posts`
ADD COLUMN `total_points` INT NULL DEFAULT 0 COMMENT '' AFTER `guest_complaint`,
ADD COLUMN `passing_score_percentage` INT NULL DEFAULT 100 COMMENT '' AFTER `total_points`,
ADD COLUMN `passing_score_points` INT NULL DEFAULT 0 COMMENT '' AFTER `passing_score_percentage`,
ADD COLUMN `location_id` BIGINT(20) NULL COMMENT '' AFTER `passing_score_points`,
ADD COLUMN `order` SMALLINT(5) NULL DEFAULT 0 COMMENT '' AFTER `location_id`,
ADD COLUMN `deleted` TINYINT(1) NULL DEFAULT 0 COMMENT '' AFTER `order`;
