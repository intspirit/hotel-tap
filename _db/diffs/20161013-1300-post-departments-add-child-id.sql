ALTER TABLE `post_departments`
	DROP PRIMARY KEY;

ALTER TABLE `post_departments`
	ADD COLUMN `child_id` BIGINT(20) DEFAULT 0 AFTER `department_id`;

ALTER TABLE `post_departments`
	ADD PRIMARY KEY (`hotel_id`,`post_id`, `department_id`, `child_id`);
