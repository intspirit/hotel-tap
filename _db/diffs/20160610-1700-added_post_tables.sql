CREATE TABLE IF NOT EXISTS `post_departments` (
	hotel_id BIGINT(20) DEFAULT '0' NOT NULL,
	post_id BIGINT(20) DEFAULT '0' NOT NULL,
	department_id BIGINT(20) DEFAULT '0' NOT NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (hotel_id, post_id, department_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `post_read_statuses` (
	hotel_id BIGINT(20) DEFAULT '0' NOT NULL,
	user_id BIGINT(20) DEFAULT '0' NOT NULL,
	post_id BIGINT(20) DEFAULT '0' NOT NULL,
	status VARCHAR(2),
	CONSTRAINT `PRIMARY` PRIMARY KEY (hotel_id, user_id, post_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `post_tags` (
	hotel_id BIGINT(20) DEFAULT '0' NOT NULL,
	post_id BIGINT(20) DEFAULT '0' NOT NULL,
	tag_id BIGINT(20) DEFAULT '0' NOT NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (hotel_id, post_id, tag_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `post_users` (
	hotel_id BIGINT(20) DEFAULT '0' NOT NULL,
	post_id BIGINT(20) DEFAULT '0' NOT NULL,
	user_id BIGINT(20) DEFAULT '0' NOT NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (hotel_id, post_id, user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `posts` (
	id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	type_id VARCHAR(10),
	hotel_id INT(11),
	description TEXT NOT NULL,
	description_text TEXT,
	language_id VARCHAR(16),
	flagged TINYINT(1),
	flagged_by BIGINT(20),
	flagged_dtm DATETIME,
	flagged_dtm_utc DATETIME,
	flag_due_dtm DATETIME,
	flag_due_dtm_utc DATETIME,
	private TINYINT(1),
	status INT(11),
	created_by BIGINT(20),
	created_dtm DATETIME,
	created_dtm_utc DATETIME,
	parent_id BIGINT(20),
	post_from_type_id VARCHAR(3),
	post_from_id BIGINT(20),
	legacy_type_id VARCHAR(10),
	legacy_id INT(11),
	order_dtm_utc DATETIME,
	comment_count INT(11),
	attachment_count INT(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
