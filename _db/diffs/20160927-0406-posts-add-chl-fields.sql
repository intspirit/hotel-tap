ALTER TABLE `posts`
ADD COLUMN `recurring_type` VARCHAR(10) NULL COMMENT 'daily, weekly, monthly' AFTER `location_id`,
ADD COLUMN `dwm` TEXT NULL COMMENT 'Legacy daily, weekly, monthly schedule pattern.' AFTER `recurring_type`,
ADD COLUMN `end_dtm` DATETIME NULL AFTER `dwm`,
ADD COLUMN `schedule` VARCHAR(200) NULL COMMENT 'Recurrence - schedule pattern new.' AFTER `end_dtm`;
