-- One-off access token for login
ALTER TABLE `users`
	ADD COLUMN `access_token` varchar(255) DEFAULT NULL AFTER `token`,
	ADD INDEX `access_token` (`access_token`);
