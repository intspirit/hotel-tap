ALTER TABLE `posts`
	ADD COLUMN `resolution` VARCHAR(10) NULL AFTER `schedule`,
	ADD COLUMN `cost` INT(11) NULL AFTER `resolution`,
	ADD COLUMN `points_given` INT(11) NULL AFTER `cost`;
