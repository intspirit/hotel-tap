-- add task related fields to the post table
-- IF NOT EXISTS( SELECT NULL
--            FROM INFORMATION_SCHEMA.COLUMNS
--          WHERE table_name = 'posts'
--                       AND table_schema IN ('shiftlinq', 'ebdb')
--                       AND column_name = 'linked_post_id')  THEN

ALTER TABLE `posts` ADD assignment_type_id VARCHAR(3) NULL;
ALTER TABLE `posts` ADD assignment_id BIGINT NULL;
ALTER TABLE `posts` ADD subject NVARCHAR(255) NULL;
ALTER TABLE `posts` ADD due_dtm DATETIME NULL;
ALTER TABLE `posts` ADD due_dtm_utc DATETIME NULL;
ALTER TABLE `posts` ADD completion_status VARCHAR(5) NULL;
ALTER TABLE `posts` ADD completion_dtm DATETIME NULL;
ALTER TABLE `posts` ADD completion_dtm_utc DATETIME NULL;
ALTER TABLE `posts` ADD completed_by BIGINT NULL;
ALTER TABLE `posts` ADD linked_post_id BIGINT NULL;

-- END IF;
