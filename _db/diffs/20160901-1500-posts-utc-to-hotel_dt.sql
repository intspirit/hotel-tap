UPDATE posts
INNER JOIN hotels
	ON posts.hotel_id = hotels.hotel_id
SET
	posts.flagged_dtm = (SELECT CONVERT_TZ(posts.flagged_dtm_utc, 'UTC', hotels.timezone)),
	posts.flag_due_dtm = (SELECT CONVERT_TZ(posts.flag_due_dtm_utc, 'UTC', hotels.timezone)),
	posts.created_dtm = (SELECT CONVERT_TZ(posts.created_dtm_utc, 'UTC', hotels.timezone)),
	posts.due_dtm = (SELECT CONVERT_TZ(posts.due_dtm_utc, 'UTC', hotels.timezone)),
	posts.completion_dtm = (SELECT CONVERT_TZ(posts.completion_dtm_utc, 'UTC', hotels.timezone));
