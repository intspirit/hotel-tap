ALTER TABLE `posts`
ADD COLUMN `order` INT NULL AFTER `schedule`,
ADD COLUMN `deleted` TINYINT(1) NULL DEFAULT 0 AFTER `order`,
ADD COLUMN `inspection_points` INT NULL AFTER `deleted`,
ADD COLUMN `fails_all` TINYINT(1) NULL DEFAULT 0 AFTER `inspection_points`;
