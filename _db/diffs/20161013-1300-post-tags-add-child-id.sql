ALTER TABLE `post_tags`
	DROP PRIMARY KEY;

ALTER TABLE `post_tags`
	ADD COLUMN `child_id` BIGINT(20) DEFAULT 0 AFTER `tag_id`;

ALTER TABLE `post_tags`
	ADD PRIMARY KEY (`hotel_id`,`post_id`, `tag_id`, `child_id`);
