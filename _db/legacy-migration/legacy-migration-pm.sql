/*
DELETE FROM posts WHERE type_id = 'pm' AND legacy_id > 0;
DELETE FROM posts WHERE type_id = 'pms' AND legacy_id > 0;
DELETE FROM posts WHERE type_id = 'pmt' AND legacy_id > 0;
 */

####################################################################################################
# ROOM_LOGS->POSTS
# DELETE FROM posts WHERE type_id = 'pm' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  subject,
  description,
  language_id,
  assignment_type_id,
  assignment_id,
  legacy_type_id,
  legacy_id,
  location_type_id
)
  SELECT
    'pm',
    hotelId,
    name,
    IF(description = '' OR ISNULL(description), name, description),
    'en',
    IF(assignmentType = 'd', 'dep', 'emp'),
    assigneeId,
    'rl',
    id,
    'room'
  FROM roomLogs
  WHERE type = 1;

# TODO: CHANGED !!!!!!!!!!!! Rerun everywhere
####################################################################################################
# ROOM_SCHEDULES->POSTS
# DELETE FROM posts WHERE type_id = 'pms' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  description,
  parent_id,
  legacy_type_id,
  legacy_id,
  location_type_id,
  location_id,
  due_dtm,
  due_dtm_utc,
  completion_dtm,
  completion_status,
  completed_by
)
  SELECT DISTINCT
    'pms',
    roomSchedules.hotelId,
    '',
    posts.id,
    'rs',
    roomSchedules.id,
    'room',
    roomSchedules.roomId,
    roomSchedules.dueDateTime,
    roomSchedules.dueDateTimeUtc,
    roomSchedules.completionDateTime,
    IF(ISNULL(roomSchedules.completionDateTime), 'open', 'done'),
    completedTasks.completedBy
  FROM roomSchedules
    INNER JOIN roomLogs ON roomSchedules.roomLogId = roomLogs.id
    INNER JOIN posts ON posts.legacy_type_id = 'rl' AND posts.legacy_id = roomSchedules.roomLogId
    LEFT JOIN completedTasks ON completedTasks.roomScheduleId = roomSchedules.id AND
                                completedTasks.completionDateTime = (
                                  SELECT max(completionDateTime)
                                  FROM completedTasks
                                  WHERE roomScheduleId = roomSchedules.id
                                )
  WHERE roomLogs.type = 1;

####################################################################################################
# ROOM_SCHEDULES->POSTS
# DELETE FROM posts WHERE type_id = 'pmt' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  subject,
  description,
  sort_order,
  deleted,
  parent_id,
  legacy_type_id,
  legacy_id
)
  SELECT
    'pmt',
    checklistTasks.hotelId,
    checklistTasks.subject,
    IF(ISNULL(checklistTasks.description), '', checklistTasks.description),
    checklistTasks.`order`,
    IF(ISNULL(checklistTasks.deleted), 0, checklistTasks.deleted),
    posts.id,
    'chlt',
    checklistTasks.id
  FROM checklistTasks
    INNER JOIN posts ON
                       posts.legacy_type_id = 'rl' AND
                       posts.hotel_id = checklistTasks.hotelId AND
                       posts.legacy_id = checklistTasks.checklistId;


####################################################################################################
# COMPLETED_TASKS->POSTS
# DELETE FROM posts WHERE type_id = 'pmtc' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  legacy_type_id,
  legacy_id,
  subject,
  description,

  completion_status,
  completed_by,
  completion_dtm,
  completion_dtm_utc,

  parent_id,
  linked_post_id
)
  SELECT DISTINCT
    'pmtc'            AS type_id,
    completedTasks.hotelId,
    'rlstc'           AS legacy_type_id,
    completedTasks.id AS legacy_id,
    completedTasks.subject,
    IF(ISNULL(completedTasks.description), '', completedTasks.description),

    'done',
    completedBy,
    completedTasks.completionDateTime,
    completedTasks.completionDateTimeUtc,

    #   points,
    #   score,
    #  failAll,

    posts_rs.id       AS parent_id,
    posts_chlt.id     AS linked_post_id
  FROM completedTasks
    INNER JOIN roomSchedules ON completedTasks.roomScheduleId = roomSchedules.id
    INNER JOIN roomLogs ON roomSchedules.roomLogId = roomLogs.id
    INNER JOIN posts AS posts_rs ON
                                   posts_rs.legacy_type_id = 'rs' AND
                                   posts_rs.legacy_id = completedTasks.roomScheduleId
    INNER JOIN posts AS posts_chlt ON
                                     posts_chlt.type_id = 'pmt' AND
                                     posts_chlt.legacy_id = completedTasks.checklistTaskId
  WHERE roomLogs.type = 1;

################################################################################################
# POSTS: UTC dates -> Hotel's timezone dates

UPDATE posts
  INNER JOIN hotels
    ON posts.hotel_id = hotels.hotel_id
SET
  posts.completion_dtm_utc = (SELECT CONVERT_TZ(posts.completion_dtm, hotels.timezone, 'UTC'))
WHERE posts.type_id = 'pms';
