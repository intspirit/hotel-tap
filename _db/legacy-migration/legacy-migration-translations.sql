/*
	TRUNCATE TABLE translations;
*/

####################################################################################################
# NOTES -> TRANSLATIONS

INSERT INTO translations (
		hotel_id,
		post_id,
		language,
		description
)
SELECT
	posts.hotel_id,
	posts.id,
	notes.LanguageId,
	notes.description
FROM
	notes
INNER JOIN
	posts
ON
	notes.OriginalMessageId = posts.legacy_id AND posts.legacy_type_id = 'note'
WHERE
	posts.id NOT IN (SELECT post_id from translations);


####################################################################################################
# TASKS -> TRANSLATIONS

INSERT INTO translations (
	hotel_id,
	post_id,
	language,
	description,
	subject
)
SELECT
	posts.hotel_id,
	posts.id,
	tasks.LanguageId,
	tasks.description,
	tasks.subject
FROM
	tasks
INNER JOIN
	posts
ON
	tasks.OriginalMessageId = posts.legacy_id AND posts.legacy_type_id = 'task'
WHERE
	posts.id NOT IN (SELECT post_id from translations);

####################################################################################################
# COMMENTS -> TRANSLATIONS

INSERT INTO translations (
	hotel_id,
	post_id,
	language,
	description
)
SELECT
	posts.hotel_id,
	posts.id,
	comments.LanguageId,
	comments.comment
FROM
	comments
INNER JOIN
	posts
ON
	comments.OriginalMessageId = posts.legacy_id AND posts.legacy_type_id = 'comm'
WHERE
	posts.id NOT IN (SELECT post_id from translations);
