####################################################################################################
# TASKS->POSTS
# DELETE FROM posts WHERE type_id = 'task';
INSERT INTO posts (
  legacy_id, legacy_type_id, type_id,
  hotel_id, description, description_text,
  flagged, flagged_dtm_utc, flag_due_dtm, flagged_by,
  created_by, created_dtm_utc, status,
  language_id, order_dtm_utc,
  subject,
  private,
  assignment_type_id, assignment_id,
  due_dtm_utc,
  completion_status,
  completion_dtm_utc,
  completed_by
)
  SELECT
    task_id,
    'task',
    'task',
    hotel_id,
    description,
    description_text,
    flag,
    IF(flagged_date = '0000-00-00 00:00:00' OR flagged_date IS NULL, NULL, flagged_date) flagged_date,
    flag_end_date,
    flagged_by,
    created_by,
    created_date,
    status,
    LanguageId,
    order_date,
    subject,
    0,
    IF(assignto_dept = 0, 'emp', 'dep'),
    IF(assignto_dept = 0, assignto, assignto_dept),
    STR_TO_DATE(CONCAT(due_date, ' ', due_time), '%Y-%m-%d %H:%i:%s'),
    IF(completed = 1, 'done', 'open'),
    completed_date,
    completed_by
  FROM tasks
  WHERE OriginalMessageId = 0 AND
        task_id > (COALESCE((SELECT max(legacy_id)
                             FROM posts
                             WHERE legacy_type_id = 'task'), 0));

####################################################################################################
# Calc comment_count
UPDATE posts
  INNER JOIN (
               SELECT
                 source_id,
                 count(*) count
               FROM comments
               WHERE comment_source = 'tasks'
               GROUP BY source_id) comment_counts
    ON posts.legacy_type_id = 'task' AND posts.legacy_id = comment_counts.source_id
SET posts.comment_count = comment_counts.count;

# TODO: calc attachment_count
UPDATE posts
  INNER JOIN (
               SELECT
                 source_id,
                 count(*) count
               FROM attachements
               WHERE source = 'tasks'
               GROUP BY source_id) attachment_counts
    ON posts.legacy_type_id = 'task' AND posts.legacy_id = attachment_counts.source_id
SET posts.attachment_count = attachment_counts.count;

####################################################################################################
# POST_DEPARTMENTS from tasks
# TRUNCATE TABLE post_departments;
INSERT INTO post_departments (hotel_id, post_id, department_id, order_dtm_utc)
  SELECT DISTINCT
    all_post_departments.hotel_id,
    posts.id,
    department_id,
    posts.order_dtm_utc
  FROM (
         (SELECT
            hotel_id,
            task_id AS legacy_id,
            dept_id AS department_id
          FROM tasks
          WHERE dept_id != 0 AND OriginalMessageId = 0
         )
         UNION
         (SELECT
            hotel_id,
            task_id       AS legacy_id,
            assignto_dept AS department_id
          FROM tasks
          WHERE assignto_dept != 0 AND OriginalMessageId = 0
         )
         UNION
         (SELECT
            hotel_id,
            tasks.task_id AS legacy_id,
            type_id       AS department_id
          FROM task_mentions_tags
            INNER JOIN tasks ON tasks.task_id = task_mentions_tags.task_id
          WHERE type = 'DEPT' AND OriginalMessageId = 0)) AS all_post_departments
    INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = all_post_departments.legacy_id;

####################################################################################################
# POST_USERS from tasks
# TRUNCATE TABLE post_users;
INSERT INTO post_users (hotel_id, post_id, user_id, category, order_dtm_utc)
  SELECT DISTINCT
    all_post_users.hotel_id,
    posts.id,
    user_id,
    all_post_users.category,
    posts.order_dtm_utc
  FROM (
         (SELECT
            hotel_id,
            task_id    AS legacy_id,
            created_by AS user_id,
            'cr'       AS category
          FROM tasks
          WHERE OriginalMessageId = 0)
         UNION
         (SELECT
            hotel_id,
            task_id AS legacy_id,
            assignto AS user_id,
            'ast' AS category
          FROM tasks
          WHERE OriginalMessageId = 0 AND assignto != 0)
         UNION
         (SELECT
            hotel_id,
            tasks.task_id AS legacy_id,
            type_id       AS user_id,
            IF(comment_id = 0, 'cc', 'com') AS category
          FROM task_mentions_tags
            INNER JOIN tasks ON tasks.task_id = task_mentions_tags.task_id
          WHERE type = 'EMP' AND OriginalMessageId = 0)) AS all_post_users
    INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = all_post_users.legacy_id;

####################################################################################################
# POST_TAGS from tasks
# TRUNCATE TABLE post_tags;
INSERT INTO post_tags (hotel_id, post_id, tag_id)
  SELECT DISTINCT
    all_post_tags.hotel_id,
    posts.id,
    tag_id
  FROM (
         SELECT
           tasks.hotel_id,
           tasks.task_id              AS legacy_id,
           task_mentions_tags.type_id AS tag_id
         FROM task_mentions_tags
           INNER JOIN tags ON task_mentions_tags.type_id = tags.tag_id
           INNER JOIN tasks ON tasks.task_id = task_mentions_tags.task_id
         WHERE
           task_mentions_tags.type = 'TAG' AND
           OriginalMessageId = 0 AND
           task_mentions_tags.comment_id = 0 AND
           tags.special IS NULL)
    AS all_post_tags
    INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = all_post_tags.legacy_id;

####################################################################################################
# POST_READ_STATUSES from tasks
# TRUNCATE TABLE post_read_statuses;
INSERT INTO post_read_statuses (hotel_id, post_id, user_id)
  SELECT DISTINCT
    hotel_id,
    post_id,
    user_id
  FROM (
         (SELECT
            posts.hotel_id,
            posts.id AS        post_id,
            hotels.hotel_admin user_id
          FROM posts
            INNER JOIN hotels ON posts.hotel_id = hotels.hotel_id
          WHERE posts.type_id = 'task')
         UNION
         (SELECT
            posts.hotel_id,
            posts.id AS post_id,
            users.user_id
          FROM posts
            INNER JOIN users ON posts.hotel_id = users.hotel_id
          WHERE users.role_id = 2 AND posts.type_id = 'task')
         UNION
         (SELECT
            posts.hotel_id,
            posts.id AS post_id,
            user_id
          FROM posts
            INNER JOIN post_users ON posts.id = post_users.post_id
          WHERE posts.type_id = 'task')
         UNION
         (SELECT
            posts.hotel_id,
            posts.id                    AS post_id,
            employee_departments.emp_id AS user_id
          FROM posts
            INNER JOIN post_departments ON posts.id = post_departments.post_id
            INNER JOIN employee_departments
              ON post_departments.department_id = employee_departments.dept_id
          WHERE posts.type_id = 'task'))
       all_mentioned_users
  WHERE hotel_id != 0;

UPDATE post_read_statuses
  INNER JOIN
  (SELECT
     posts.id                                    AS post_id,
     IF(read_statuses.status = 'read', 'r', 'u') AS status
   FROM read_statuses
     INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = read_statuses.entity_id AND
                         read_statuses.entity_type = 'task') AS leagcy_read_statuses
    ON post_read_statuses.post_id = leagcy_read_statuses.post_id
SET post_read_statuses.status = leagcy_read_statuses.status;

# mark post as read fo the poster
UPDATE post_read_statuses
  INNER JOIN posts ON posts.id = post_read_statuses.post_id AND post_read_statuses.user_id = posts.created_by
SET post_read_statuses.status = 'r'
WHERE posts.type_id = 'task';

# mark all others as unread
UPDATE post_read_statuses
SET status = 'u'
WHERE status IS NULL;

####################################################################################################
# Attachments
UPDATE attachements
  INNER JOIN posts ON attachements.source = 'tasks' AND attachements.source_id = posts.legacy_id
SET attachements.post_id = posts.id
WHERE posts.legacy_type_id = 'task';

################################################################################################
# TASKS->COMMENTS
# DELETE FROM posts WHERE type_id = 'comm';

INSERT INTO posts (
  legacy_id, legacy_type_id, type_id,
  hotel_id, description, description_text,
  parent_id,
  created_by, created_dtm_utc, status,
  language_id, order_dtm_utc)
  SELECT
    comment_id,
    'comm',
    'comm',
    tasks.hotel_id,
    comment,
    comment_text,
    posts.id,
    comments.created_by,
    comments.created_date,
    comments.status,
    comments.LanguageId,
    comments.created_date
  FROM comments
    INNER JOIN tasks ON comments.comment_source = 'TASKS' AND comments.source_id = tasks.task_id
    INNER JOIN posts ON tasks.task_id = posts.legacy_id AND posts.legacy_type_id = 'task'
  WHERE comments.OriginalMessageId = 0;

####################################################################################################
# POST_DEPARTMENTS from comments
# TRUNCATE TABLE post_departments;
INSERT INTO post_departments (hotel_id, post_id, department_id, order_dtm_utc)
  SELECT DISTINCT
    posts.hotel_id,
    posts.id,
    department_id,
    posts.order_dtm_utc
  FROM (
         SELECT
           task_mentions_tags.comment_id AS legacy_id,
           task_mentions_tags.type_id    AS department_id
         FROM task_mentions_tags
           INNER JOIN comments ON comments.comment_source = 'TASKS' AND comments.comment_id = task_mentions_tags.comment_id
         WHERE
           task_mentions_tags.comment_id > 0 AND
           task_mentions_tags.type = 'dept' AND
           comments.OriginalMessageId = 0
       ) AS all_comments_departments
  INNER JOIN posts ON posts.legacy_type_id = 'comm' AND posts.legacy_id = all_comments_departments.legacy_id;

####################################################################################################
# POST_DEPARTMENTS from comments (parent)
# TRUNCATE TABLE post_departments;
INSERT INTO post_departments (hotel_id, post_id, department_id, child_id, order_dtm_utc)
  SELECT DISTINCT
    p2.hotel_id,
    p2.id,
    department_id,
    p1.id,
    p2.order_dtm_utc
  FROM (
         SELECT
           task_mentions_tags.comment_id AS legacy_id,
           task_mentions_tags.task_id    AS legacy_parent_id,
           task_mentions_tags.type_id    AS department_id
         FROM task_mentions_tags
           INNER JOIN comments ON comments.comment_source = 'TASKS' AND comments.comment_id = task_mentions_tags.comment_id
         WHERE
           task_mentions_tags.comment_id > 0 AND
           task_mentions_tags.type = 'dept' AND
           comments.OriginalMessageId = 0
       ) AS all_comments_departments
  INNER JOIN posts AS p1 ON p1.legacy_type_id = 'comm' AND p1.legacy_id = all_comments_departments.legacy_id
  INNER JOIN posts AS p2 ON p2.legacy_type_id = 'task' AND p2.legacy_id = all_comments_departments.legacy_parent_id;

####################################################################################################
# POST_USERS from comments
# TRUNCATE TABLE post_users;
INSERT INTO post_users (hotel_id, post_id, user_id, category, order_dtm_utc)
  SELECT DISTINCT
    posts.hotel_id,
    posts.id,
    all_comment_users.user_id,
    all_comment_users.category,
    posts.order_dtm_utc
  FROM (
         (SELECT
            comment_id AS legacy_id,
            created_by AS user_id,
            'cr' AS category
          FROM comments
          WHERE comments.comment_source = 'TASKS' AND comments.OriginalMessageId = 0)
         UNION
         (SELECT
            task_mentions_tags.comment_id AS legacy_id,
            task_mentions_tags.type_id AS user_id,
            'cc'  AS category
          FROM task_mentions_tags
            INNER JOIN comments ON comments.comment_id = task_mentions_tags.comment_id
            INNER JOIN users ON task_mentions_tags.type_id = users.user_id
          WHERE
             comments.comment_source = 'TASKS' AND
             task_mentions_tags.type = 'EMP' AND
             comments.OriginalMessageId = 0
          )
       ) AS all_comment_users
    INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = all_comment_users.legacy_id;

####################################################################################################
# POST_USERS from comments (parent)
# TRUNCATE TABLE post_users;
INSERT INTO post_users (hotel_id, post_id, user_id, child_id, category, order_dtm_utc)
  SELECT DISTINCT
    parent.hotel_id,
    parent.id,
    all_comment_users.user_id,
    child.id,
    all_comment_users.category,
    parent.order_dtm_utc
  FROM (
         (SELECT
            comment_id AS legacy_id,
            source_id  AS legacy_parent_id,
            created_by AS user_id,
            'cr' AS category
          FROM comments
          WHERE comments.comment_source = 'TASKS' AND comments.OriginalMessageId = 0)
         UNION
         (SELECT
            task_mentions_tags.comment_id AS legacy_id,
            task_mentions_tags.task_id    AS legacy_parent_id,
            task_mentions_tags.type_id    AS user_id,
            'cc'                           AS category
          FROM task_mentions_tags
            INNER JOIN comments ON comments.comment_id = task_mentions_tags.comment_id
            INNER JOIN users ON task_mentions_tags.type_id = users.user_id
          WHERE
             comments.comment_source = 'TASKS' AND
             task_mentions_tags.type = 'EMP' AND
             comments.OriginalMessageId = 0
          )
       ) AS all_comment_users
    INNER JOIN posts AS child ON child.legacy_type_id = 'comm' AND child.legacy_id = all_comment_users.legacy_id
    INNER JOIN posts AS parent ON parent.legacy_type_id = 'task' AND parent.legacy_id = all_comment_users.legacy_parent_id;

####################################################################################################
# POST_TAGS from comments
# TRUNCATE TABLE post_tags;
INSERT INTO post_tags (hotel_id, post_id, tag_id)
  SELECT DISTINCT
    posts.hotel_id,
    posts.id,
    all_comment_tags.tag_id
  FROM (
          SELECT
            task_mentions_tags.comment_id AS legacy_id,
            task_mentions_tags.type_id    AS tag_id
          FROM task_mentions_tags
            INNER JOIN comments ON comments.comment_id = task_mentions_tags.comment_id
            INNER JOIN tags ON task_mentions_tags.type_id = tags.tag_id
          WHERE
            comments.comment_source = 'TASKS' AND
            task_mentions_tags.type = 'TAG' AND
            comments.OriginalMessageId = 0 AND
            tags.special IS NULL
       ) AS all_comment_tags
    INNER JOIN posts ON posts.legacy_type_id = 'task' AND posts.legacy_id = all_comment_tags.legacy_id;

####################################################################################################
# POST_TAGS from comments (parent)
# TRUNCATE TABLE post_tags;
INSERT INTO post_tags (hotel_id, post_id, tag_id, child_id)
  SELECT DISTINCT
    parent.hotel_id,
    parent.id,
    all_comment_tags.tag_id,
    child.id
  FROM (
          SELECT
            task_mentions_tags.comment_id AS legacy_id,
            task_mentions_tags.task_id    AS legacy_parent_id,
            task_mentions_tags.type_id    AS tag_id
          FROM task_mentions_tags
            INNER JOIN comments ON comments.comment_id = task_mentions_tags.comment_id
            INNER JOIN tags ON task_mentions_tags.type_id = tags.tag_id
          WHERE
            comments.comment_source = 'TASKS' AND
            task_mentions_tags.type = 'TAG' AND
            comments.OriginalMessageId = 0 AND
            tags.special IS NULL
       ) AS all_comment_tags
    INNER JOIN posts AS child ON child.legacy_type_id = 'comm' AND child.legacy_id = all_comment_tags.legacy_id
    INNER JOIN posts AS parent ON parent.legacy_type_id = 'task' AND parent.legacy_id = all_comment_tags.legacy_parent_id;

# Attachments
UPDATE attachements
  INNER JOIN posts ON attachements.source = 'comments' AND attachements.source_id = posts.legacy_id
SET attachements.post_id = posts.id
WHERE posts.legacy_type_id = 'comm';

# Posts -> attachment counts
UPDATE posts
  INNER JOIN (
               SELECT
                 source_id,
                 count(*) count
               FROM attachements
               WHERE source = 'comments'
               GROUP BY source_id) attachment_counts
    ON posts.legacy_type_id = 'comm' AND posts.legacy_id = attachment_counts.source_id
SET posts.attachment_count = attachment_counts.count;


################################################################################################
# POSTS: guest complaints
UPDATE posts
  INNER JOIN (
               SELECT task_id
               FROM task_mentions_tags
                 INNER JOIN tags ON task_mentions_tags.type = 'TAG' AND task_mentions_tags.type_id = tags.tag_id
               WHERE tags.special = 'guest_complaint') complains
    ON posts.legacy_type_id = 'task' AND posts.legacy_id = complains.task_id
SET posts.guest_complaint = 1;

################################################################################################
# POSTS: UTC dates -> Hotel's timezone dates

UPDATE posts
INNER JOIN hotels
	ON posts.hotel_id = hotels.hotel_id
SET
	posts.flagged_dtm = (SELECT CONVERT_TZ(posts.flagged_dtm_utc, 'UTC', hotels.timezone)),
	posts.flag_due_dtm = (SELECT CONVERT_TZ(posts.flag_due_dtm_utc, 'UTC', hotels.timezone)),
	posts.created_dtm = (SELECT CONVERT_TZ(posts.created_dtm_utc, 'UTC', hotels.timezone)),
	posts.due_dtm = (SELECT CONVERT_TZ(posts.due_dtm_utc, 'UTC', hotels.timezone)),
	posts.completion_dtm = (SELECT CONVERT_TZ(posts.completion_dtm_utc, 'UTC', hotels.timezone));




