/*
DELETE FROM posts WHERE type_id = 'chl' AND legacy_id > 0;
DELETE FROM posts WHERE type_id = 'chls' AND legacy_id > 0;
*/

####################################################################################################
# CHECK_LISTS->POSTS
# DELETE FROM posts WHERE type_id = 'chl' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  legacy_type_id,
  legacy_id,
  subject,
  description,
  assignment_type_id,
  assignment_id,
  due_dtm,
  recurring_type,
  dwm,
  end_dtm,
  language_id,
  completion_status,
  completion_dtm_utc,
  completed_by
)
  SELECT
    'chl'                                      AS type_id,
    hotel_id,
    'chl'                                      AS legacy_type_id,
    checklist_id                               AS legacy_id,
    LEFT(name, 255)                            AS subject,
    ''                                         AS description,
    IF(user_id > 0, 'emp', 'dep')              AS assignment_type_id,
    IF(user_id > 0, user_id, dept_id)          AS assignee_id,
    STR_TO_DATE(CONCAT(due_date, ' ', due_time), '%Y-%m-%d %H:%i:%s'),
    recurring_type,
    dwm,
    IF(end_date < '2000-01-01',
       '2099-12-31 00:00:00',
       CONCAT(end_date, ' 00:00:00')),
    LanguageId,
    IF(ISNULL(completed_date), 'open', 'done') AS completion_status,
    completed_date,
    completed_by
  FROM checklists
  WHERE master_id IS NULL AND checklists.is_deleted = 0;

# TODO: is_deleted ????
# TODO: due_dtm_utc, other utc

####################################################################################################
# CHECK_LIST_SCHEDULE->POSTS
# DELETE FROM posts WHERE type_id = 'chls' AND legacy_id > 0;
INSERT INTO posts
(
  type_id,
  hotel_id,
  legacy_type_id,
  legacy_id,
  subject,
  description,
  assignment_type_id,
  assignment_id,
  parent_id,
  due_dtm,
  completion_status,
  completion_dtm_utc,
  completed_by
)
  SELECT
    'chls'                            AS type_id,
    checklists.hotel_id,
    'chls'                            AS legacy_type_id,
    checklist_id                      AS legacy_id,
    LEFT(name, 255)                   AS subject,
    ''                                AS description,
    IF(user_id > 0, 'emp', 'dep')     AS assignee_type_id,
    IF(user_id > 0, user_id, dept_id) AS assignee_id,
    posts.id,
    STR_TO_DATE(CONCAT(due_date, ' ', due_time), '%Y-%m-%d %H:%i:%s'),
    IF(ISNULL(completed_date), 'open', 'done'),
    completed_date,
    checklists.completed_by
  FROM checklists
    INNER JOIN posts ON posts.legacy_type_id = 'chl' AND posts.legacy_id = checklists.master_id
  WHERE master_id IS NOT NULL AND checklists.is_deleted = 0
  ORDER BY 1;

UPDATE posts
INNER JOIN hotels
	ON posts.hotel_id = hotels.hotel_id
SET
	posts.completion_dtm = (SELECT CONVERT_TZ(posts.completion_dtm_utc, 'UTC', hotels.timezone))
WHERE posts.type_id IN ('chls', 'chl');


