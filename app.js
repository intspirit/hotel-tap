'use strict'

if (process.env.NODE_ENV === 'development')
	require('babel-register')({
		presets: ['es2015', 'react'],
		plugins: [
			'transform-class-properties',
			'transform-object-rest-spread',
			'transform-async-to-generator',
			'transform-runtime'
		]
	})

const server = require('./server/server')

server.init(null)
server.start()
