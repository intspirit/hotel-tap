var register = require('babel-core/register')

// Ignore all node_modules except these
var modulesToCompile = [
	'react-native',
	'react-native-vector-icons',
	'react-native-mock'
].map((moduleName) => new RegExp(`/node_modules/${moduleName}`))
var config = {presets: ['react-native']}
config.ignore = function(filename) {
	if (!(/\/node_modules\//).test(filename)) return false

	var matches = modulesToCompile.filter((regex) => regex.test(filename))
	return matches.length === 0
}

register(config)
global.__DEV__ = true
require('react-native-mock/mock')
var React = require('react-native')
React.NavigationExperimental = {
	AnimatedView: React.View
}
