export default {
	getResponseMock: function getResponseMock() {
		return {
			json: data => Promise.resolve(data)
		}
	},
	getRequestMock: function getRequestMock() {
		return {hotelId: 107, userId: 485}
	},
	getRequestMockWithIdInParams: function () {
		return {hotelId: 107, userId: 485, params: { id: 654 }}
	}
}
