'use strict'
import should from 'should'
import _ from 'lodash'
import utils from '../../../../server/core/utils'

should.equal(true, true)

const timezone = 'Australia/Queensland' // offset UTC+10

const testUtcIsoString = '2016-01-01T07:00:00Z'
const testUtcFormatted = '2016-01-01 07:00:00'
const testUtcFormattedCutSeconds = '2016-01-01 07:00'

const testQueenslandIsoString = '2016-01-01T17:00:00+10:00'
const testQueenslandFormatted = '2016-01-01 17:00:00'
const testQueenslandFormattedCutSeconds = '2016-01-01 17:00'

const testLocalIsoString = '2016-01-01T08:00:00+01:00'
const testLocalFormatted = '2016-01-01 08:00:00'

describe('server/core/util', () => {

	describe('#getUserImageUrl', () => {
		it('should return profile url for an existing file name', done => {
			const actual = utils.getUserImageUrl('joe_doe.jpg')

			should(actual).equal('https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/joe_doe.jpg')
			done()
		})

		it('should return default profile url for a null file name', done => {
			const actual = utils.getUserImageUrl()

			should(actual).equal('https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/default.png')
			done()
		})
	})

	describe('#getDepartmentImageUrl', () => {
		it('should return url for an existing file name', done => {
			const actual = utils.getDepartmentImageUrl('front_desk.jpg')

			should(actual).equal('https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/front_desk.jpg')
			done()
		})

		it('should return default url for a null file name', done => {
			const actual = utils.getDepartmentImageUrl()

			should(actual).equal('https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/icons/iconD.png')
			done()
		})
	})

	describe('#getUpdateSetClause', () => {
		it('should return SET clause string for SELECT', done => {
			const expected = `type_id = :typeId,\nguest_complaint = :guestComplaint`
			const mapping = {
				typeId: 'type_id',
				guestComplaint: 'guest_complaint',
				subject: 'subject'
			}
			const properties = {
				typeId: 'note',
				guestComplaint: false,
				description: 'some text'
			}

			const actual = utils.getUpdateSetClause(mapping, properties)

			should(actual).equal(expected)
			done()
		})
	})

	describe('#utcToTimezone', () => {
		it('should return correct datetime in a given timezone converted from utc Date', () => {
			const actual = utils.utcToTimezone(new Date(testUtcIsoString), timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return correct datetime in a given timezone converted from utc String', () => {
			const actual = utils.utcToTimezone(testUtcFormatted, timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return correct datetime in a given timezone converted from utc Number', () => {
			const actual = utils.utcToTimezone(new Date(testUtcIsoString).getTime(), timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return utc datetime if no timezone provided', () => {
			const actual = utils.utcToTimezone(testUtcFormatted)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return datetime in correct format if it is set', () => {
			const actual = utils.utcToTimezone(testUtcFormatted, timezone, 'YYYY-MM-DD HH:mm')

			actual.should.be.equal(testQueenslandFormattedCutSeconds)
		})
	})

	describe('#timezoneToUtc', () => {
		it('should return correct datetime in utc converted from a given timezone Date', () => {
			const actual = utils.timezoneToUtc(new Date(testQueenslandIsoString), timezone)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return correct datetime in utc converted from a given timezone String', () => {
			const actual = utils.timezoneToUtc(testQueenslandFormatted, timezone)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return correct datetime in utc converted from a given timezone Number', () => {
			const actual = utils.timezoneToUtc(new Date(testQueenslandIsoString).getTime(), timezone)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return utc datetime if no timezone provided', () => {
			const actual = utils.timezoneToUtc(testQueenslandIsoString)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return datetime in correct format if it is set', () => {
			const actual = utils.timezoneToUtc(testQueenslandFormatted, timezone, 'YYYY-MM-DD HH:mm')

			actual.should.be.equal(testUtcFormattedCutSeconds)
		})
	})

	describe('#localToTimezone', () => {
		it('should return correct datetime in a given timezone converted from a local Date', () => {
			const actual = utils.localToTimezone(new Date(testLocalIsoString), timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return correct datetime in a given timezone converted from a local String', () => {
			const actual = utils.localToTimezone(testLocalIsoString, timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return correct datetime in a given timezone converted from a local Number', () => {
			const actual = utils.localToTimezone(new Date(testLocalIsoString).getTime(), timezone)

			actual.should.be.equal(testQueenslandFormatted)
		})

		it('should return datetime in correct format if it is set', () => {
			const actual = utils.localToTimezone(testLocalIsoString, timezone, 'YYYY-MM-DD HH:mm')

			actual.should.be.equal(testQueenslandFormattedCutSeconds)
		})
	})

	describe('#localToUtc', () => {
		it('should return correct utc datetime of given moment (js Date)', () => {
			const actual = utils.localToUtc(new Date(testLocalIsoString))

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return correct utc datetime from a local String', () => {
			const actual = utils.localToUtc(testLocalIsoString)

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return correct datetime from a local Number', () => {
			const actual = utils.localToUtc(new Date(testLocalIsoString).getTime())

			actual.should.be.equal(testUtcFormatted)
		})

		it('should return utc datetime in correct format if it is set (js Date)', () => {
			const actual = utils.localToUtc(new Date(testLocalIsoString), 'YYYY-MM-DD HH:mm')

			actual.should.be.equal(testUtcFormattedCutSeconds)
		})
	})
})

