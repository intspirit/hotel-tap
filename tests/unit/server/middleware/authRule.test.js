'use strict'

import sinon from 'sinon'
import should from 'should'
import {mockReq, mockRes} from 'sinon-express-mock'
import authRule from '../../../../server/middleware/authRule'
import constants from '../../../../shared/constants.js'

const sandbox = sinon.sandbox.create()
should.equal(true, true)

describe('server/middleware/authRule', () => {
	let req = {},
		res = {},
		next = sandbox.spy()

	beforeEach(() => {
		req = mockReq(req)
		res = mockRes()
		next = sandbox.spy()
	})

	afterEach(() => {
		sandbox.restore()
	})

	describe('#idAdmin', () => {
		it('should pass if user is admin', () => {
			req.user = {
				roleId: constants.role.ADMIN
			}

			authRule.isAdmin(req, res, next)

			should(next.calledOnce).equal(true)
		})

		it('should return 403 error if user is NOT admin', () => {
			req.user = {
				roleId: constants.role.EMPLOYEE
			}

			authRule.isAdmin(req, res, next)

			should(next.calledOnce).equal(false)
			should(res.json.calledWith({
				error: {
					code: 403,
					message: 'Access forbidden',
					reason: 2
				}
			})).equal(true)
		})
	})
})
