'use strict'

const
	sinon = require('sinon'),
	should = require('should')

import auth from '../../../../server/middleware/auth.js'

const sandbox = sinon.sandbox.create()

should.equal(true, true)

describe('server/middleware/auth', () => {
	let req = {},
		res = {},
		next = sandbox.spy()

	beforeEach(() => {
		req = {}
		res = {}
		next = sandbox.spy()
	})

	afterEach(() => {
		sandbox.restore()
	})

	//TODO: sk p1 fix UT
	// describe('#authenticate', () => {
	// 	it('should authenticate user', done => {
	// 		//noinspection JSCheckFunctionSignatures
	// 		auth.authenticate(req, res, () => {
	// 			req.user.should.be.ok()
	// 			done()
	// 		})
	//
	// 	})
	// })
})
