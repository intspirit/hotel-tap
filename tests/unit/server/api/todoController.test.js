'use strict'

import should from 'should'
import sinon from 'sinon'
import Promise from 'bluebird'

import todoController from '../../../../server/api/todoController'
import { todoLogic } from '../../../../server/business'
import testUtils from '../../../testUtils'
import {requiredTodoCollectionProperties,
		responseShouldHaveProperties} from '../business/todoLogic.test'
import constants from '../../../../shared/constants'

const sortTypes = constants.todoSortTypes
const groupNames = constants.todoGroupNames

const getRequestMock = (sortType, id) => {
	let query = {
		sortType: sortType || sortTypes.DUE_DATETIME
	}

	const mock = {
		hotelId: 107,
		userId: 485,
		user: {
			id: 485
		},
		query: query
	}

	if (id) {
		mock.params = {
			id: id
		}
	}

	return mock
}

const emptyToDoGroupCollection = {
	count: 0,
	items: []
}

const getToDoGroupFor = name => ({
	name: name,
	title: '',
	collection: emptyToDoGroupCollection
})

const emptyToDoCollectionForSortDueDatetime = {
	totalCount: 0,
	groups: [
		getToDoGroupFor(groupNames.OVERDUE),
		getToDoGroupFor(groupNames.TODAY),
		getToDoGroupFor(groupNames.FUTURE)
	]
}

const emptyToDoCollectionForSortTodoType = {
	totalCount: 0,
	groups: [
		getToDoGroupFor(groupNames.TASKS),
		getToDoGroupFor(groupNames.CHECKLIST_SCHEDULE),
		getToDoGroupFor(groupNames.INSPECTIONS),
		getToDoGroupFor(groupNames.PM)
	]
}

let sandbox = sinon.sandbox.create()

should.equal(true, true)

describe('server/api/todoController', () => {
	describe('#getByDepartmentId', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should be async', () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			todoController.getByDepartmentId(req, res).should.be.a.Promise()
		})

		it('should not change todo logic response when sort type is "due datetime"', async () => {
			const req = getRequestMock(null, 1)
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getByDepartmentId',
				() => Promise.resolve(emptyToDoCollectionForSortDueDatetime))

			const actual = await todoController.getByDepartmentId(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should not change todo logic response when sort type is "todo type"', async () => {
			const req = getRequestMock(sortTypes.TODO_TYPE, 1)
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getByDepartmentId',
				() => Promise.resolve(emptyToDoCollectionForSortTodoType))

			const actual = await todoController.getByDepartmentId(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})
	})

	describe('#getByEmployeeId', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should be async', () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			todoController.getByEmployeeId(req, res).should.be.a.Promise()
		})

		it('should not change todo logic response when sort type is "due datetime"', async () => {
			const req = getRequestMock(null, 1)
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getByEmployeeId',
				() => Promise.resolve(emptyToDoCollectionForSortDueDatetime))

			const actual = await todoController.getByEmployeeId(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should not change todo logic response when sort type is "todo type"', async () => {
			const req = getRequestMock(sortTypes.TODO_TYPE, 1)
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getByEmployeeId',
				() => Promise.resolve(emptyToDoCollectionForSortTodoType))

			const actual = await todoController.getByEmployeeId(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should not call #getOwn handler', async () => {
			const req = getRequestMock(null, 1)
			let res = testUtils.getResponseMock()

			let spy = sandbox.spy(todoController, 'getOwn')

			await todoController.getByEmployeeId(req, res)

			sinon.assert.notCalled(spy)
		})
	})

	describe('#getForAllHotelEmployees', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should be async', () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			todoController.getForAllHotelEmployees(req, res).should.be.a.Promise()
		})

		it('should not change todo logic response when sort type is "due datetime"', async () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getForAllHotelEmployees',
				() => Promise.resolve(emptyToDoCollectionForSortDueDatetime))

			const actual = await todoController.getForAllHotelEmployees(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should not change todo logic response when sort type is "todo type"', async () => {
			const req = getRequestMock(sortTypes.TODO_TYPE)
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getForAllHotelEmployees',
				() => Promise.resolve(emptyToDoCollectionForSortTodoType))

			const actual = await todoController.getForAllHotelEmployees(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})
	})

	describe('#getOwn', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should be async', () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			todoController.getOwn(req, res).should.be.a.Promise()
		})

		it('should not change todo logic response', async () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			sandbox.stub(todoLogic, 'getByEmployeeId',
				() => Promise.resolve(emptyToDoCollectionForSortDueDatetime))

			const actual = await todoController.getOwn(req, res)
			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should not call #getByEmployeeId handler', async () => {
			const req = getRequestMock()
			let res = testUtils.getResponseMock()

			let spy = sandbox.spy(todoController, 'getByEmployeeId')

			await todoController.getOwn(req, res)

			sinon.assert.notCalled(spy)
		})
	})
})
