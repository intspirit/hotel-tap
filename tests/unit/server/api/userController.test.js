'use strict'

let
	rewire = require('rewire'),
	supertest = require('supertest')

import sinon from 'sinon'
import should from 'should'
import fakeData from '../../../../server/data/fakeData'
import db from '../../../../server/db'
import testUtils from '../../../testUtils'
import 'sinon-as-promised'

//noinspection JSUnusedLocalSymbols
let
	server = {},
	request = {},
	authMockEmployee = {
		authenticate(req, res, next) {
			req.user = fakeData.getEmployee()
			req.hotelId = req.user.hotelId
			next()
		}
	},
	authMockAdmin = {
		authenticate(req, res, next) {
			req.user = fakeData.getAdmin()
			req.hotelId = req.user.hotelId
			next()
		}
	}

function initServer(authMock) {
	server = rewire('../../../../server/server')
	//noinspection Eslint
	server.__set__('auth', authMock)

	//noinspection JSUnresolvedFunction
	server.init()
	//noinspection JSUnresolvedFunction
	server.start()
	//noinspection JSUnresolvedVariable
	request = supertest(server.app)
}

describe('server/api/userController', () => {
	let sandbox;

	beforeEach(() => {
		sandbox = sinon.sandbox.create()
	})

	afterEach(() => {
		sandbox.restore()
	})

	describe('#getNav', () => {
		afterEach(() => {
			server.close()
		})

		//TODO: vvs p2 fix when actual user is set in the auth
		it('should return employee navigation for employee', done => {
			initServer(authMockEmployee)
			sandbox.stub(db.departmentDb, 'getByUser').resolves(fakeData.getDepartmentsForEmployee())

			request.get('/api/users/me/nav').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.options.should.be.ok()
					res.body.options.length.should.equal(12)
					res.body.options[1].label.should.be.equal('Javier Lazo')

					return done()
				})
		})

		it('should return admin navigation for admin', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.departmentDb, 'getByHotelId').resolves(fakeData.getDepartmentsForAdmin())

			request.get('/api/users/me/nav').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.options.should.be.ok()
					res.body.options.length.should.equal(15)
					res.body.options[0].label.should.be.equal('Jessica Lorenzo')
					res.body.options[1].label.should.be.equal('Settings')

					return done()
				})
		})
	})

	describe('#getDepartments', () => {
		afterEach(() => {
			server.close()
		})

		it('should return departments for admin', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.departmentDb, 'getByHotelId').resolves(fakeData.getDepartmentsForAdmin())

			request.get('/api/users/me/departments').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.departments.should.be.ok()
					res.body.departments.length.should.equal(3)

					return done()
				})
		})

		it('should return departments for employee', done => {
			initServer(authMockEmployee)
			sandbox.stub(db.departmentDb, 'getByUser').resolves(fakeData.getDepartmentsForEmployee())

			request.get('/api/users/me/departments').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.departments.should.be.ok()
					res.body.departments.length.should.equal(1)
					return done()
				})
		})
	})

	describe('#getTags', () => {
		afterEach(() => {
			server.close()
		})

		it('should return tags', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.tagDb, 'getByHotelId').resolves(fakeData.getTags())

			request.get('/api/tags').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.tags.should.be.ok()
					res.body.tags.length.should.equal(2)

					return done()
				})
		})
	})

	describe('#getAreas', () => {
		afterEach(() => {
			server.close()
		})

		it('should return areas', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.areaDb, 'getByHotelId').resolves(fakeData.getAreas())

			request.get('/api/areas').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.areas.should.be.ok()
					res.body.areas.length.should.equal(2)

					return done()
				})
		})
	})

	describe('#getMaintenanceTypes', () => {
		afterEach(() => {
			server.close()
		})

		it('should return maintenance types', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.maintenanceTypeDb, 'getMaintenanceTypesByHotelId').resolves(fakeData.getMaintenanceTypes())

			request.get('/api/maintenance').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.mtypes.should.be.ok()
					res.body.mtypes.length.should.equal(2)

					return done()
				})
		})
	})

	describe('#getEquipmentGroups', () => {
		afterEach(() => {
			server.close()
		})

		it('should return equipment groups', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.equipmentGroupDb, 'getEquipmentGroupsByHotelId').resolves(fakeData.getEquipmentGroups())

			request.get('/api/equipmentGroups').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.groups.should.be.ok()
					res.body.groups.length.should.equal(3)

					return done()
				})
		})
	})

	describe('#getEquipments', () => {
		afterEach(() => {
			server.close()
		})

		it('should return maintenance types', done => {
			initServer(authMockAdmin)
			sandbox.stub(db.equipmentDb, 'getEquipmentsByHotelId').resolves(fakeData.getEquipments())

			request.get('/api/equipments').
				expect(200).
				expect('Content-Type', /json/).
				end((err, res) => {
					if (err) return done(err)

					res.body.equipments.should.be.ok()
					res.body.equipments.length.should.equal(2)

					return done()
				})
		})
	})
})
