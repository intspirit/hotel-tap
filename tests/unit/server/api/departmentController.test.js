'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import sinon from 'sinon'
import target from '../../../../server/api/departmentController'
import db from '../../../../server/db'
import testUtils from '../../../testUtils'

should.equal(true, true)

describe('server/api/departmentController', () => {
	let sandbox;

	beforeEach(() => {
		sandbox = sinon.sandbox.create()
	})

	afterEach(() => {
		sandbox.restore()
	})

	describe('#getAll', () => {

		it('should return departments', async () => {
			const req = testUtils.getRequestMock()
			const res = testUtils.getResponseMock()

			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.resolve([{id: 1, name: 'department1'}]))

			const actual = await target.getAll(req, res)
			actual.departments.length.should.equal(1)
			actual.departments[0].id.should.equal(1)
			actual.departments[0].name.should.equal('department1')
		})

		it('should return []', async () => {
			const req = testUtils.getRequestMock()
			const res = testUtils.getResponseMock()

			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.reject({error: 'Db error'}))

			const actual = await target.getAll(req, res)
			actual.departments.length.should.equal(0)
		})
	})
})

