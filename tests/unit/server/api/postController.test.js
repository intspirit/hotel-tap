'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import sinon from 'sinon'
import target from '../../../../server/api/postController'
import db from '../../../../server/db'
import testUtils from '../../../testUtils'

let sandbox = sinon.sandbox.create()

should.equal(true, true)

describe('server/api/postController', () => {

	describe('#getUnreadCount', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should return department & user counts', async () => {
			const req = testUtils.getRequestMock()
			let res = testUtils.getResponseMock()

			sandbox.stub(db.postReadStatusDb,
				'getDepartmentCountsByUserId',
				() => Promise.resolve([{type: 'department', id: 1, count: 10}]))
			sandbox.stub(db.postReadStatusDb,
				'getUserCountsByUserId',
				() => Promise.resolve([{type: 'employee', id: 485, count: 3}]))

			const actual = await target.getUnreadCount(req, res)
			actual.counts.length.should.equal(2)
			actual.counts[1].type.should.equal('employee')
			actual.counts[1].id.should.equal(-10)
		})

		it('should return []', async () => {
			const req = testUtils.getRequestMock()
			let res = testUtils.getResponseMock()

			sinon.stub(db.postReadStatusDb,
				'getDepartmentCountsByUserId',
				() => Promise.reject({error: 'error 253'}))
			sinon.stub(db.postReadStatusDb,
				'getUserCountsByUserId',
				() => Promise.resolve([{type: 'employee', id: 485, count: 3}]))

			const actual = await target.getUnreadCount(req, res)
			actual.counts.length.should.equal(0)
		})
	})
})

