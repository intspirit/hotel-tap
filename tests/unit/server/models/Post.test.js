'use strict'

import should from 'should'

import Post from '../../../../server/models/Post'

describe('server/models/Post', () => {
	describe('#constructor', () => {
		it('should throw if no values provided', () => {
			should.throws(() => new Post())
		})

		it('should not throw if ToDo provided', () => {
			const emptyToDo = {
				id: 1,
				type_id: 'task',
				subject: 'some subject',
				description: 'some description',
				completion_status: 'open',
				due_dtm_utc: '2000-01-01 00:00:00',
				completion_dtm_utc: '2000-01-01 00:00:00'
			}

			should.doesNotThrow(() => new Post(emptyToDo))
		})

		it('should not throw if description is empty or not defined', () => {
			let emptyToDo = {
				id: 1,
				type_id: 'task',
				subject: 'some subject',
				completion_status: 'open',
				due_dtm_utc: '2000-01-01 00:00:00',
				completion_dtm_utc: '2000-01-01 00:00:00'
			}

			should.doesNotThrow(() => new Post(emptyToDo))

			emptyToDo.description = ''

			should.doesNotThrow(() => new Post(emptyToDo))
		})
	})

	describe('#cleanDescription', () => {
		const dataBag = [
			[	// http
				'<a href="http://app.hoteltap.com/something">Test1</a>',
				'<a href="/something">Test1</a>'
			],
			[	// https
				'<a href="https://app.hoteltap.com/something">Test2</a>',
				'<a href="/something">Test2</a>'
			],
			[	// app.
				'<a href="http://app.hoteltap.com/something">Test3</a>',
				'<a href="/something">Test3</a>'
			],
			[	// stage.
				'<a href="http://stage.hoteltap.com/something">Test4</a>',
				'<a href="/something">Test4</a>'
			],
			[	// /index.php
				'<a href="https://stage.hoteltap.com/index.php/something">Test5</a>',
				'<a href="/something">Test5</a>'
			],
			[	// departments board
				'<a href="https://app.hoteltap.com/index.php/hotel/board/46">@Front Desk</a>',
				'<a href="/boards/departments/46">@Front Desk</a>'
			],
			[	// employee board
				'<a href="https://app.hoteltap.com/index.php/hotel/empboard/56">@John Doe</a>',
				'<a href="/boards/employee/56">@John Doe</a>'
			]
		]

		it('should clean description', done => {
			dataBag.forEach(x => {
				const [description, expected] = x
				const actual = Post.cleanDescription(description)
				should(actual).equal(expected)
			})
			done()
		})

	})
})
