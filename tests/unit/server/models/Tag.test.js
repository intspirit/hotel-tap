'use strict'

import should from 'should'

import Tag from '../../../../server/models/Tag'

describe('server/models/Tag', () => {
	describe('#constructor', () => {
		it('should throw if lacks arguments', () => {
			should.throws(() => new Tag())
		})

		it('should add a "roomNumber" property if tag name contains only "room" and digits', () => {
			const fakeData = {
				name: 'room1'
			}

			let tag = new Tag(fakeData)
			tag.should.have.a.property('roomNumber')
			tag.roomNumber.should.be.a.Number
		})

		it('should add a "roomNumber" property if tag name contains "room" in upper case and digits', () => {
			const fakeData = {
				name: 'Room1'
			}

			let tag = new Tag(fakeData)
			tag.should.have.a.property('roomNumber')
			tag.roomNumber.should.be.a.Number
		})

		it('should NOT add a "roomNumber" property if tag name contains "room" and has NO digits', () => {
			const fakeData = {
				name: 'DiningRoom'
			}

			let tag = new Tag(fakeData)
			tag.should.not.have.a.property('roomNumber')
		})

		it('should NOT add a "roomNumber" property if tag name contains NOT only "room" and digits', () => {
			const fakeData = {
				name: 'DiningRoom122'
			}

			let tag = new Tag(fakeData)
			tag.should.not.have.a.property('roomNumber')
		})
	})
})
