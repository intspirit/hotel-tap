'use strict'

import should from 'should'
import {ApiError} from '../../../../server/errors'

should.equal(true, true)

describe('server/errors/ApiError', () => {

	describe('#constructor', () => {

		it('should return an error object', done => {

			const actual = new ApiError('Log message', 500, 1000, 'Something went wrong', 1, 'dbError:201')

			should(actual).ok()
			actual.message.should.equal('Log message')
			actual.statusCode.should.equal(500)
			actual.code.should.equal(1000)
			actual.userMessage.should.equal('Something went wrong')
			actual.reason.should.equal(1)
			actual.meta.should.equal('dbError:201')
			actual.name.should.equal('ApiError')
			done()
		})
	})
})
