'use strict'

import should from 'should'
import {LoginError} from '../../../../server/errors'

should.equal(true, true)

describe('server/errors/LoginError', () => {

	describe('#constructor', () => {

		it('should return an error object', done => {

			const actual = new LoginError('Invalid user name', 'code:1300')

			should(actual).ok()
			actual.message.should.equal('Invalid user name')
			actual.statusCode.should.equal(401)
			actual.code.should.equal(401)
			actual.userMessage.should.equal('Sign in has failed. Please, try again')
			actual.meta.should.equal('code:1300')
			actual.name.should.equal('LoginError')
			done()
		})
	})
})
