'use strict'

import should from 'should'
import {getResponseError, BadRequestError} from '../../../../server/errors'

should.equal(true, true)

describe('server/errors/index', () => {

	describe('#getResponseError', () => {

		it('should return a ServerError result if null is passed', done => {

			const actual = getResponseError()

			should(actual).ok()
			should(actual.error.code).equal(500)
			should(actual.error.message).equal('Server error. Something went wrong. Please try again.')
			should(actual.error.reason).equal('')
			done()
		})

		it('should return a ServerError result if Error is passed', done => {

			const actual = getResponseError(new Error('Generic error'))

			should(actual).ok()
			should(actual.error.code).equal(500)
			should(actual.error.message).equal('Server error. Something went wrong. Please try again.')
			should(actual.error.reason).equal('')
			done()
		})

		it('should return a specific application error result if App Error is passed', done => {

			const actual = getResponseError(new BadRequestError('Invalid parameter'))

			should(actual).ok()
			should(actual.error.code).equal(400)
			should(actual.error.message).equal('Something went wrong. Please try again.')
			should(actual.error.reason).equal('')
			done()
		})
	})
})
