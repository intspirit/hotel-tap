'use strict'

import should from 'should'
import sinon from 'sinon'
import Promise from 'bluebird'
import bcrypt from 'bcrypt-nodejs'

import models from '../../../../server/models'
import userLogic from '../../../../server/business/userLogic'
import constants from '../../../../shared/constants'
import db from '../../../../server/db'
import fakeData from '../../../../server/data/fakeData'

import messages from '../../../../shared/core/messages'
import {BadRequestError} from '../../../../server/errors'
import {userDb} from '../../../../server/db'

const hotelId = 10

should.equal(true, true)

describe('server/business/userLogic', () => {
	let sandbox;

	beforeEach(() => {
		sandbox = sinon.sandbox.create()
	})

	afterEach(() => {
		sandbox.restore()
	})

	describe('#getNavigation', () => {
		it('should return admin navigation for admin', async () => {
			const admin = fakeData.getAdmin()
			const expected = [
				'Jessica Lorenzo',
				'Settings',
				'Hotel',
				'Front Desk',
				'Sales',
				'All Employees',
				'Service Recovery Process',
				'Checklists',
				'Preventive Maintenance',
				'Inspections',
				'Dashboard',
				'Reports',
				'Documents',
				'Creating Notes and Tasks',
				'Run DB Scripts'
			]
			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.resolve(fakeData.getDepartmentsForAdmin()))

			const actual = await userLogic.getNavigation(hotelId, admin)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.label).equal(expected[i]))
		})

		it('should return employee navigation for employee', async () => {
			const employee = fakeData.getEmployee()
			employee.roleId = constants.role.EMPLOYEE
			const expected = [
				'Sales',
				'Javier Lazo',
				'All Employees',
				'Service Recovery Process',
				'Checklists',
				'Preventive Maintenance',
				'Inspections',
				'Dashboard',
				'Reports',
				'Documents',
				'Creating Notes and Tasks',
				'Run DB Scripts'
			]
			sandbox.stub(db.departmentDb, 'getByUser', () => Promise.resolve(fakeData.getDepartmentsForEmployee()))

			const actual = await userLogic.getNavigation(hotelId, employee)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.label).equal(expected[i]))
		})

		it('should return basic navigation in case of error', async () => {
			const employee = fakeData.getEmployee()
			employee.roleId = constants.role.EMPLOYEE
			const expected = [
				'Javier Lazo',
				'All Employees',
				'Service Recovery Process',
				'Checklists',
				'Preventive Maintenance',
				'Inspections',
				'Dashboard',
				'Reports',
				'Documents',
				'Creating Notes and Tasks'
			]
			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.reject(new Error('some error')))
			sandbox.stub(db.departmentDb, 'getByUser', () => Promise.reject(new Error('some error')))

			const actual = await userLogic.getNavigation(hotelId, employee)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.label).equal(expected[i]))
		})
	})

	describe('#getHotels', () => {
		it('should return list of hotels for admin', async () => {
			const admin = fakeData.getAdmin()
			admin.roleId = constants.role.ADMIN
			const expected = [
				'Hilton',
				'Best Western',
				'CALIFORNIA INN AND SUITES'
			]
			sandbox.stub(db.hotelDb, 'getByAdmin', () => Promise.resolve(fakeData.getHotelsForAdmin()))
			const actual = await userLogic.getHotels(admin)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return one hotel for employee', async () => {
			const employee = fakeData.getEmployee()
			employee.roleId = constants.role.EMPLOYEE

			const expected = [
				'Hilton'
			]
			sandbox.stub(db.hotelDb, 'getById', () => Promise.resolve(fakeData.getHotelForEmployee()))

			const actual = await userLogic.getHotels(employee)

			should(actual).ok()
			should(actual.length).equal(1)
			should(actual[0].name).equal(expected[0])
		})

		it('should return empty hotels list in case of error', async () => {
			const user = fakeData.getEmployee()
			sandbox.stub(db.hotelDb, 'getByAdmin', () => Promise.reject(new Error('some error')))
			sandbox.stub(db.hotelDb, 'getById', () => Promise.reject(new Error('some error')))

			const actual = await userLogic.getHotels(user)

			should(actual).ok()
			should(actual.length).equal(0)
		})
	})

	describe('#getHotelsFewDetails', () => {
		it('should return list of hotels with few details for admin', async () => {
			const admin = fakeData.getAdmin()
			admin.roleId = constants.role.ADMIN
			const expected = [
				'Hilton',
				'Best Western',
				'CALIFORNIA INN AND SUITES'
			]

			const expectedKeys = ['id', 'name', 'timeZone']

			sandbox.stub(db.hotelDb, 'getByAdmin', () => Promise.resolve(fakeData.getHotelsForAdmin()))
			const actual = await userLogic.getHotelsFewDetails(admin)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => {
				should(Object.keys(x)).eql(expectedKeys)
				should(x.name).equal(expected[i])
			})
		})

		it('should return one hotel with few details for employee', async () => {
			const employee = fakeData.getEmployee()
			employee.roleId = constants.role.EMPLOYEE

			const expected = [
				'Hilton',
				'Best Western',
				'CALIFORNIA INN AND SUITES'
			]

			const expectedKeys = ['id', 'name', 'timeZone']

			sandbox.stub(db.hotelDb, 'getById', () => Promise.resolve(fakeData.getHotelForEmployee()))
			const actual = await userLogic.getHotelsFewDetails(employee)

			should(actual).ok()
			should(actual.length).equal(1)
			should(Object.keys(actual[0])).eql(expectedKeys)
			should(actual[0].name).equal(expected[0])
		})

		it('should return empty hotels list in case of error', async () => {
			const user = fakeData.getEmployee()
			sandbox.stub(db.hotelDb, 'getByAdmin', () => Promise.reject(new Error('some error')))
			sandbox.stub(db.hotelDb, 'getById', () => Promise.reject(new Error('some error')))

			const actual = await userLogic.getHotelsFewDetails(user)
			should(actual).ok()
			should(actual.length).equal(0)
		})
	})

	describe('#getMe', () => {
		it('should return user data', async () => {
			const user = fakeData.getEmployee()
			const expectedKeys = [
				'id',
				'firstName',
				'fullName',
				'lastName',
				'email',
				'imageUrl',
				'isAdmin',
				'notificationStatus'
			]
			const actual = await userLogic.getMe(user)

			should(actual).ok()
			should(Object.keys(actual)).eql(expectedKeys)
		})
	})

	describe('#getDepartments', () => {

		it('should return departments for admin', async () => {
			const admin = fakeData.getAdmin()
			const expected = [
				'Hotel',
				'Front Desk',
				'Sales'
			]

			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.resolve(fakeData.getDepartmentsForAdmin()))
			const actual = await userLogic.getDepartments(hotelId, admin)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return [] for admin', async () => {
			const admin = fakeData.getAdmin()
			const expected = []

			sandbox.stub(db.departmentDb, 'getByHotelId', () => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getDepartments(hotelId, admin)

			should(actual).eql(expected)
		})

		it('should return departments for employee', async () => {
			const user = fakeData.getEmployee()
			const expected = [
				'Sales'
			]

			sandbox.stub(db.departmentDb, 'getByUser', () => Promise.resolve(fakeData.getDepartmentsForEmployee()))
			const actual = await userLogic.getDepartments(hotelId, user)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return [] for employee', async () => {
			const user = fakeData.getEmployee()
			const expected = []

			sandbox.stub(db.departmentDb, 'getByUser', () => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getDepartments(hotelId, user)

			should(actual).eql(expected)
		})
	})

	describe('#getTags', () => {

		it('should return tags', async () => {
			const expected = [
				'tag1',
				'tag2'
			]

			sandbox.stub(db.tagDb, 'getByHotelId', () => Promise.resolve(fakeData.getTags()))
			const actual = await userLogic.getTags(hotelId)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return []', async () => {
			const expected = []

			sandbox.stub(db.tagDb, 'getByHotelId', () => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getTags(hotelId)

			should(actual).eql(expected)
		})
	})

	describe('#getAreas', () => {

		it('should return areas', async () => {
			const expected = [
				'Rooms',
				'Guest Areas'
			]

			sandbox.stub(db.areaDb, 'getByHotelId', () => Promise.resolve(fakeData.getAreas()))
			const actual = await userLogic.getAreas(hotelId)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return []', async () => {
			const expected = []

			sandbox.stub(db.areaDb, 'getByHotelId', () => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getAreas(hotelId)

			should(actual).eql(expected)
		})
	})

	describe('#getMaintenanceTypes', () => {
		it('should return maintenance types', async () => {
			const expected = [
				'Electrical',
				'Plumbing'
			]

			sandbox.stub(db.maintenanceTypeDb, 'getMaintenanceTypesByHotelId',
				() => Promise.resolve(fakeData.getMaintenanceTypes()))
			const actual = await userLogic.getMaintenanceTypes(hotelId)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return []', async () => {
			const expected = []

			sandbox.stub(db.maintenanceTypeDb, 'getMaintenanceTypesByHotelId',
				() => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getMaintenanceTypes(hotelId)

			should(actual).eql(expected)
		})
	})

	describe('#getEquipmentGroups', () => {
		it('should return equipment groups', async () => {
			const expected = [
				'Laundry Equipment',
				'Office Equipment',
				'Common Area Equipment',
			]

			sandbox.stub(db.equipmentGroupDb, 'getEquipmentGroupsByHotelId',
				() => Promise.resolve(fakeData.getEquipmentGroups()))
			const actual = await userLogic.getEquipmentGroups(hotelId)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return []', async () => {
			const expected = []

			sandbox.stub(db.equipmentGroupDb, 'getEquipmentGroupsByHotelId',
				() => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getEquipmentGroups(hotelId)

			should(actual).eql(expected)
		})
	})

	describe('#getEquipments', () => {
		it('should return equipments', async () => {
			const expected = [
				'40 lbs. Washer',
				'45 lbs. Dryer',
			]

			sandbox.stub(db.equipmentDb, 'getEquipmentsByHotelId',
				() => Promise.resolve(fakeData.getEquipments()))
			const actual = await userLogic.getEquipments(hotelId)

			should(actual).ok()
			should(actual.length).equal(expected.length)
			actual.forEach((x, i) => should(x.name).equal(expected[i]))
		})

		it('should return []', async () => {
			const expected = []

			sandbox.stub(db.equipmentDb, 'getEquipmentsByHotelId',
				() => Promise.reject(new Error('foobar')))
			const actual = await userLogic.getEquipments(hotelId)

			should(actual).eql(expected)
		})
	})

	describe('#updatePassword', () => {
		const fakeUser = {
			id: 1000000,
			validPassword: function () {
				return true
			}
		}

		it('should throw if old password is empty', async () => {
			try {
				sandbox.stub(userDb, 'updatePassword', () => Promise.resolve(null))
				sandbox.stub(userDb, 'getById', () => Promise.resolve(null))
				sandbox.stub(bcrypt, 'hash', () => Promise.resolve(null))

				const fakePasswords = {oldPassword: '', newPassword: '12'}

				const actual = await userLogic.updatePassword(fakeUser.id, fakePasswords, fakeUser)
				actual.should.not.be.ok()
			} catch (error) {
				try {
					error.should.be.instanceOf(BadRequestError)
					error.message.should.equal(messages.error.PASSWORD_UPDATE_FAILED)
				} catch (testingError) {
					throw testingError
				}
			}
		})

		it('should throw if new password is empty', async () => {
			try {
				sandbox.stub(userDb, 'updatePassword', () => Promise.resolve(null))
				sandbox.stub(userDb, 'getById', () => Promise.resolve(null))
				sandbox.stub(bcrypt, 'hash', () => Promise.resolve(null))

				const fakePasswords = {oldPassword: '12', newPassword: ''}

				const actual = await userLogic.updatePassword(fakeUser.id, fakePasswords, fakeUser)
				actual.should.not.be.ok()
			} catch (error) {
				try {
					error.should.be.instanceOf(BadRequestError)
					error.message.should.equal(messages.error.PASSWORD_UPDATE_FAILED)
				} catch (testingError) {
					throw testingError
				}
			}
		})

		it('should throw if old password and new password coincide', async () => {
			try {
				sandbox.stub(userDb, 'updatePassword', () => Promise.resolve(null))
				sandbox.stub(userDb, 'getById', () => Promise.resolve(null))
				sandbox.stub(bcrypt, 'hash', () => Promise.resolve(null))

				const fakePasswords = {oldPassword: '123', newPassword: '123'}

				const actual = await userLogic.updatePassword(fakeUser.id, fakePasswords, fakeUser)
				actual.should.not.be.ok()
			} catch (error) {
				try {
					error.should.be.instanceOf(BadRequestError)
					error.message.should.equal(messages.error.NEW_PASSWORD_SHOULD_NOT_EQUAL_OLD_PASSWORD)
				} catch (testingError) {
					throw testingError
				}
			}
		})
	})
})
