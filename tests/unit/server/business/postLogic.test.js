'use strict'

import should from 'should'
import sinon from 'sinon'
import target from '../../../../server/business/postLogic'
import {
	employeeDepartmentDb,
	postDepartmentDb,
	postReadStatusDb,
	postTagDb,
	postUserDb,
	userDb
} from '../../../../server/db'
import hotelLogic from '../../../../server/business/hotelLogic'
import constants from '../../../../shared/constants'

should.equal(true, true)
let sandbox = sinon.sandbox.create()

describe('server/business/postLogic', () => {

	beforeEach(() => {
		sandbox = sinon.sandbox.create()
	})

	afterEach(() => {
		sandbox.restore()
	})

	describe('#getByDepartmentId', () => {

		it('should return posts', async () => {

			const actual = await target.getByDepartmentId(107, 657, {id: 10, defaultLanguage: 'en'})

			should(actual).ok()
			// should(actual.length).equal(expected.length)
			// actual.forEach((x, i) => should(x.label).equal(expected[i]))
		})
	})

	describe('#insertPostDepartments', () => {
		it('should insert post departments', async () => {
			const now = new Date()
			const args = [
				[107, 100, 10, 0, now],
				[107, 100, 20, 0, now],
				[107, 100, 30, 0, now]
			]
			sandbox.stub(postDepartmentDb, 'insertBulk').
				withArgs(args).
				returns(Promise.resolve(33))

			const actual = await target.insertPostDepartments(107, 100, [10, 20, 30], now)

			should(actual).equal(33)
		})
	})

	describe('#getAllPostDepartments', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should add assignment department if assigned to department', async () => {
			const hotelId = 50
			const departmentIds = [1, 2, 10]
			const post = {
				departments: departmentIds,
				board: 'boards/me'
			}
			const assignmentInfo = {
				type: constants.assignmentTypes.DEPARTMENT,
				id: 50
			}

			const actual = await target.getAllPostDepartments(hotelId, {}, post, assignmentInfo)

			should(actual).eql([1, 2, 10, 50])
		})

		it('should add maintenance department if at least one maintenance types mentioned', async () => {
			const hotelId = 50
			const post = {
				departments: [1, 2, 10],
				maintenanceTypeList: [3, 6]
			}

			const stub = sandbox.stub(hotelLogic,
				'getMaintenanceDepartment',
				() => Promise.resolve({id: 30}))

			const actual = await target.getAllPostDepartments(hotelId, {}, post, {})

			should(actual).eql([1, 2, 10, 30])
		})

		it('should return same list if board is missing', async () => {
			const hotelId = 50
			const departmentIds = [1, 2, 10]
			const post = {
				departments: departmentIds,
				board: ''
			}

			const actual = await target.getAllPostDepartments(hotelId, {}, post, {})

			should(actual).eql(departmentIds)
		})

		it('should return same list if posted from board/me', async () => {
			const hotelId = 50
			const departmentIds = [1, 2, 10]
			const post = {
				departments: departmentIds,
				board: 'boards/me'
			}

			const actual = await target.getAllPostDepartments(hotelId, {}, post, {})

			should(actual).eql(departmentIds)
		})

		it('should return same list if posted from board/tags', async () => {
			const hotelId = 50
			const departmentIds = [1, 2, 10]
			const post = {
				departments: departmentIds,
				board: 'boards/tags'
			}

			const actual = await target.getAllPostDepartments(hotelId, {}, post, {})

			should(actual).eql(departmentIds)
		})

		it('should return extra department if admin posts from another board', async () => {
			const hotelId = 50
			const user = {roleId: constants.role.ADMIN}
			const post = {
				departments: [1, 2, 10],
				board: 'departments/100'
			}

			const actual = await target.getAllPostDepartments(hotelId, user, post, {})

			should(actual).eql([1, 2, 10, 100])
		})

		it('should return same list if admin posts from mentioned department', async () => {
			const hotelId = 50
			const user = {roleId: constants.role.ADMIN}
			const post = {
				departments: [1, 2, 10],
				board: 'departments/10'
			}

			const actual = await target.getAllPostDepartments(hotelId, user, post, {})

			should(actual).eql([1, 2, 10])
		})

		it('should return same list if employee posts from default board', async () => {
			const hotelId = 50
			const user = {id: 10, roleId: constants.role.EMPLOYEE}
			const post = {
				departments: [1, 2, 10],
				board: 'departments/10'
			}

			const stub = sandbox.stub(employeeDepartmentDb,
				'getByUserId',
				() => Promise.resolve([
					{employeeId: 1, departmentId: 10},
					{employeeId: 1, departmentId: 20}
				]))
			const actual = await target.getAllPostDepartments(hotelId, user, post, {})

			should(actual).eql([1, 2, 10])
		})

		it('should return same list if employee posts from default board 2', async () => {
			const hotelId = 50
			const user = {id: 10, roleId: constants.role.EMPLOYEE}
			const post = {
				departments: [1, 2, 10],
				board: 'departments/20'
			}

			const stub = sandbox.stub(employeeDepartmentDb,
				'getByUserId',
				() => Promise.resolve([
					{employeeId: 1, departmentId: 10},
					{employeeId: 1, departmentId: 20}
				]))
			const actual = await target.getAllPostDepartments(hotelId, user, post, {})

			should(actual).eql([1, 2, 10, 20])
		})

		it('should add first default board if employee posts from another board', async () => {
			const hotelId = 50
			const user = {id: 10, roleId: constants.role.EMPLOYEE}
			const post = {
				departments: [1, 2, 10],
				board: 'departments/30'
			}

			const stub = sandbox.stub(employeeDepartmentDb,
				'getByUserId',
				() => Promise.resolve([
					{employeeId: 1, departmentId: 11},
					{employeeId: 1, departmentId: 20}
				]))
			const actual = await target.getAllPostDepartments(hotelId, user, post, {})

			should(actual).eql([1, 2, 10, 11])
		})
	})

	describe('#getAllPostUsers', () => {
		it('should return creator', async () => {
			const expected = [
				{id: 485, category: 'cr'}
			]

			const actual = await target.getAllPostUsers([], 485, {})

			should(actual).eql(expected)
		})

		it('should return creator and cc list users', async () => {
			const expected = [
				{id: 603, category: 'cc'},
				{id: 785, category: 'cc'},
				{id: 485, category: 'cr'}
			]

			const actual = await target.getAllPostUsers([603, 785], 485, {})

			should(actual).eql(expected)
		})

		it('should return creator and assigned user', async () => {
			const assignmentInfo = {
				type: constants.assignmentTypes.EMPLOYEE,
				id: 876
			}
			const expected = [
				{id: 485, category: 'cr'},
				{id: 876, category: 'exe'}
			]

			const actual = await target.getAllPostUsers([], 485, assignmentInfo)

			should(actual).eql(expected)
		})

		it('should return creator and original creator as cc when they are different', async () => {
			const expected = [
				{id: 485, category: 'cr'},
				{id: 913, category: 'cc'}
			]

			const actual = await target.getAllPostUsers([], 485, {}, 913)

			should(actual).eql(expected)
		})

		it('should return creator when creator and original creator the same', async () => {
			const expected = [
				{id: 485, category: 'cr'}
			]

			const actual = await target.getAllPostUsers([], 485, {}, 485)

			should(actual).eql(expected)
		})
	})

	describe('#insertPostUsers', () => {
		it('should insert post users', async () => {
			const postUsers = [
				{id: 485, category: 'cr'},
				{id: 876, category: 'exe'},
				{id: 603, category: 'cc'},
				{id: 785, category: 'cc'},
				{id: 913, category: 'cr'}
			]
			const hotelId = 107
			const postId = 100
			const now = new Date()
			const args = [
				[107, 100, 485, 0, 'cr', now],
				[107, 100, 876, 0, 'exe', now],
				[107, 100, 603, 0, 'cc', now],
				[107, 100, 785, 0, 'cc', now],
				[107, 100, 913, 0, 'cr', now]
			]
			sandbox.stub(postUserDb, 'insertBulk').
				withArgs(args).
				returns(Promise.resolve(133))

			const actual = await target.insertPostUsers(hotelId, postId, postUsers, now)

			should(actual).equal(133)
		})
	})

	describe('#insertPostTags', () => {
		it('should insert post tags', async () => {
			const post = {
				tags: [10, 20, 30]
			}
			const args = [
				[107, 100, 10, 0],
				[107, 100, 20, 0],
				[107, 100, 30, 0]
			]
			sandbox.stub(postTagDb, 'insertBulk').
				withArgs(args).
				returns(Promise.resolve(233))

			const actual = await target.insertPostTags(107, 100, post)

			should(actual).equal(233)
		})

		it('should not insert anything', async () => {
			const post = {
				tags: []
			}
			sandbox.stub(postTagDb, 'insertBulk').
				returns(Promise.resolve(233))

			const actual = await target.insertPostTags(107, 100, post)

			should(actual).eql([])
		})
	})

	describe('#insertReadStatuses', () => {
		it('should insert read statuses', async () => {
			const args = [
				[107, 488, 100, 'u'],
				[107, 489, 100, 'u'],
				[107, 487, 100, 'u'],
				[107, 490, 100, 'u'],
				[107, 485, 100, 'r']
			]
			const postUsers = [487, 490]
			sandbox.stub(postReadStatusDb, 'insertBulk').
				withArgs(args).
				returns(Promise.resolve(333))

			sandbox.stub(employeeDepartmentDb, 'getByDepartmentIds', () => Promise.resolve([
				{userId: 488},
				{userId: 489}
			]))
			sandbox.stub(userDb, 'getByHotelId', () => Promise.resolve([
				{id: 485, roleId: constants.role.ADMIN},
				{id: 486, roleId: constants.role.EMPLOYEE}
			]))

			const actual = await target.insertReadStatuses(107, 100, 485, [10, 20], postUsers)

			should(actual).equal(333)
		})
	})

	describe.skip('#insert', () => {
		it('should insert post', async () => {
			const post = {
				typeId: constants.postTypes.NOTE,
				description: `New post on ${new Date()}`,
				guestComplaint: true,
				tags: [31208, 31230, 31352],
				ccList: [652],
				departments: [660, 661]
			}
			const actual = await target.insert(107, 825, post)

			should(actual).equal(1)
		})
	})

})
