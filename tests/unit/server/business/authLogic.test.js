'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import sinon from 'sinon'
import target from '../../../../server/business/authLogic'
import User from '../../../../server/models/User'
import constants from '../../../../shared/constants'
import {LoginError} from '../../../../server/errors'
import {userDb} from '../../../../server/db'


let sandbox = sinon.sandbox.create()

should.equal(true, true)


describe('server/business/authLogic', () => {
	//noinspection Eslint
	const dbData = {
		user_id: 10,
		hotel_id: 107,
		first_name: 'Joe',
		last_name: 'Doe',
		username: '',
		email: 'joe@doe.com',
		password: '$2y$10$cKxdtTPe0Tl/r9J9vAaGmu9gaKPq6q/E2eD/KPPNl8QClbZvj9jT.',
		role_id: 2,
		status: '1',
		profile_picture: ''
	}
	let user = new User(dbData)
	let loginRequest = {
		userType: constants.userType.ADMIN,
		hotelId: 107,
		userName: 'joe@doe.com',
		remember: true,
		ip: '1.1.1.1'
	}
	const password = 'brad'

	describe('#generateToken', () => {
		it('should generate token', done => {

			const actual = target.generateToken(user, true)

			actual.should.be.ok()
			actual.length.should.be.greaterThan(1)
			done()
		})
	})

	describe('#getLoginResponse', () => {
		it('should return valid response with token and user', async done => {
			const actual = target.getLoginResponse(loginRequest, user, password)

			actual.token.should.be.ok()
			actual.user.should.be.ok()
			actual.token.length.should.be.greaterThan(1)
			actual.user.id.should.equal(user.id)
			done()
		})

		it('should throw LoginError if user not found', async done => {
			try {
				const actual = target.getLoginResponse(loginRequest, null, password)

				actual.should.not.be.ok()
				done()
			} catch (err) {
				try {
					err.should.be.instanceOf(LoginError)
					err.message.should.equal('User Not Found')
					done()
				} catch (e) {
					done(e)
				}
			}
		})

		it('should throw LoginError if password is incorrect', async done => {
			try {
				const actual = target.getLoginResponse(loginRequest, user, 'wrong-password')

				actual.should.not.be.ok()
				done()
			} catch (err) {
				try {
					err.should.be.instanceOf(LoginError)
					err.message.should.equal('Invalid password')
					done()
				} catch (e) {
					done(e)
				}
			}
		})
	})

	describe('#login', () => {
		beforeEach(() => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(() => {
			sandbox.restore()
		})

		it('should return a valid response with token and user for valid employee', async () => {
			loginRequest.userType = constants.userType.EMPLOYEE
			sandbox.stub(userDb,
				'getEmployeeForLogin',
				() => Promise.resolve(user))

			const actual = await target.login({loginRequest, password})

			actual.token.should.be.ok()
			actual.user.should.be.ok()
			actual.token.length.should.be.greaterThan(1)
			actual.user.id.should.equal(user.id)
		})

		it('should throw LoginError if employee not found', async () => {
			loginRequest.userType = constants.userType.EMPLOYEE
			try {
				sandbox.stub(userDb,
					'getEmployeeForLogin',
					() => Promise.resolve(null))

				const actual = await target.login({loginRequest, password})

				actual.should.not.be.ok()
				done()
			} catch (err) {
				try {
					err.should.be.instanceOf(LoginError)
					err.message.should.equal('User Not Found')
				} catch (e) {
					throw e
				}
			}
		})

		//TODO: sk p1 fix ater proper validation of login request parameters
		// it('should throw LoginError if login parameters are wrong', async done => {
		// 	try {
		// 		const invalidLoginRequest = {
		// 			hotelId: 107
		// 		}
		//
		// 		const actual = await target.login({loginRequest: invalidLoginRequest, password: 'wrong-password'})
		//
		// 		actual.should.not.be.ok()
		// 		done()
		// 	} catch (err) {
		// 		try {
		// 			err.should.be.instanceOf(LoginError)
		// 			err.message.should.equal('Invalid parameters')
		// 			done()
		// 		} catch (e) {
		// 			done(e)
		// 		}
		// 	}
		// })

		//TODO: sk p1 add UT - admin not found
		//TODO: sk p1 add UT - admins found, but password doesn't match any
	})
})

