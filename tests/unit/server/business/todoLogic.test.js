'use strict';

import should from 'should'
import sinon from 'sinon'
import Promise from 'bluebird'
import _ from 'lodash'

import todoLogic from '../../../../server/business/todoLogic'
import { postDb } from '../../../../server/db'
import constants from '../../../../shared/constants'

const groupNames = constants.todoGroupNames
const sortTypes = constants.todoSortTypes

export const requiredTodoCollectionProperties = ['groups', 'totalCount']
export const requiredTodoCollectionPropertiesAndError = ['error'].concat(requiredTodoCollectionProperties)
export const requiredGroupProperties = ['name', 'title', 'collection']
export const requiredGroupCollectionProperties = ['count', 'items']

export const responseShouldHaveProperties = (obj, props) => {
	obj.should.be.an.Object
	obj.should.have.properties(props)

	_.forEach(props, prop => {
		if (prop === 'error') {
			obj[prop].should.be.a.String;
		} else if (prop === 'totalCount') {
			obj[prop].should.be.a.Number;
		} else if (prop === 'groups') {
			obj[prop].should.be.an.Array;
			_.forEach(obj[prop], group => {
				group.should.have.properties(requiredGroupProperties)

				if (group === 'collection') {
					group.collection.should.have.properties(requiredGroupCollectionProperties)
				}
			})
		}
	})
}

const calculateTotalCountForDueDatetimeSort = (groups) => {
	let overdueGroup = _.find(groups, {name: groupNames.OVERDUE})
	let todayGroup = _.find(groups, {name: groupNames.TODAY})
	let futureGroup = _.find(groups, {name: groupNames.FUTURE})

	return overdueGroup.collection.count + todayGroup.collection.count + futureGroup.collection.count
}

let sandbox = sinon.sandbox.create()
should.equal(true, true)

describe('server/business/todoLogic', () => {
	describe('#getByDepartmentId', () => {
		beforeEach(async () => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(async () => {
			sandbox.restore()
		})

		it('should return a promise', () =>
			todoLogic.getByDepartmentId(1, 1, {}, sortTypes.DUE_DATETIME).should.be.a.Promise())

		it('should resolve with collection', async () => {
			let actual = await todoLogic.getByDepartmentId(1, 1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should respond on error', async () => {
			sandbox.stub(postDb, 'getToDoItemsByDepartmentId', () => Promise.reject(new Error('Db error')))

			const actual = await todoLogic.getByDepartmentId(1, 1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionPropertiesAndError)
		})

		it('should have correct value of "totalCount" property when sort type is dueDatetime', async () => {
			let actual = await todoLogic.getByDepartmentId(1, 1, {}, sortTypes.DUE_DATETIME)

			actual.totalCount.should.be.equal(calculateTotalCountForDueDatetimeSort(actual.groups))
		})
	})

	describe('#getByEmployeeId', () => {
		beforeEach(async () => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(async () => {
			sandbox.restore()
		})

		it('should return a promise',
			() => todoLogic.getByEmployeeId(1, 1, {}, sortTypes.DUE_DATETIME).should.be.a.Promise())

		it('should resolve with collection', async () => {
			let actual = await todoLogic.getByEmployeeId(1, 1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should respond on error', async () => {
			sandbox.stub(postDb, 'getToDoItemsByEmployeeId', () => Promise.reject(new Error('Db error')))

			const actual = await todoLogic.getByEmployeeId(1, 1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionPropertiesAndError)
		})

		it('should have correct value of "totalCount" property when sort type is dueDatetime', async () => {
			let actual = await todoLogic.getByEmployeeId(1, 1, {}, sortTypes.DUE_DATETIME)

			actual.totalCount.should.be.equal(calculateTotalCountForDueDatetimeSort(actual.groups))
		})
	})

	describe('#getForAllHotelEmployees', () => {
		beforeEach(async () => {
			sandbox = sinon.sandbox.create()
		})

		afterEach(async () => {
			sandbox.restore()
		})

		it('should return a promise',
			() => todoLogic.getForAllHotelEmployees(1, {}, sortTypes.DUE_DATETIME).should.be.a.Promise())

		it('should resolve with collection', async () => {
			let actual = await todoLogic.getForAllHotelEmployees(1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionProperties)
		})

		it('should respond on error', async () => {
			sandbox.stub(postDb, 'getEmployeesToDoItems', () => Promise.reject(new Error('Db error')))

			const actual = await todoLogic.getForAllHotelEmployees(1, {}, sortTypes.DUE_DATETIME)

			responseShouldHaveProperties(actual, requiredTodoCollectionPropertiesAndError)
		})

		it('should have correct value of "totalCount" property when sort type is dueDatetime', async () => {
			let actual = await todoLogic.getForAllHotelEmployees(1, {}, sortTypes.DUE_DATETIME)

			actual.totalCount.should.be.equal(calculateTotalCountForDueDatetimeSort(actual.groups))
		})
	})
})
