'use strict'

import should from 'should'
import target from '../../../../../server/business/validator/postUpdateRequestValidator'

should.equal(true, true)

describe('server/business/validator/postUpdateRequestValidator', () => {

	describe('#validReadStatus', () => {

		it('should return true for a valid status', done => {

			let actual = target.validReadStatus({readStatus: 'r'})

			should(actual).be.true()
			actual = target.validReadStatus({readStatus: 'u'})

			should(actual).be.true()
			done()
		})

		it('should return false for a invalid status', done => {

			let actual = target.validReadStatus({readStatus: 'd'})

			should(actual).be.false()
			done()
		})

		it('should return false for a missing status', done => {

			let actual = target.validReadStatus({})

			should(actual).be.false()
			done()
		})
	})


})
