'use strict'

import should from 'should'
import moment from 'moment'
import {utils} from '../../../../shared/core'


should.equal(true, true)


describe('shared/core/utils', () => {
	describe('#getFullName', () => {

		it('should return full name for user object', done => {
			const user = {
				firstName: 'John',
				lastName: 'Doe'
			}
			const expected = 'John Doe'
			const actual = utils.getFullName(user)

			actual.should.equal(expected)
			done()
		})
	})

	describe('#readers2string', () => {
		const postType = 'note'
		it('should return nothing for no list', done => {

			const actual = utils.readers2string(null, postType)

			should(actual).be.not.ok()
			done()
		})
		it('should return nothing for empty list', done => {

			const actual = utils.readers2string([], postType)

			should(actual).be.not.ok()
			done()
		})

		it('should return You for you only', done => {

			const actual = utils.readers2string(['You'], postType)

			actual.should.equal('You read this note')
			done()
		})

		it('should return You, Joe Doe for you and Joe Doe', done => {

			const actual = utils.readers2string(['You', 'Joe Doe'], postType)

			actual.should.equal('You and Joe Doe read this note')
			done()
		})

		it('should return You, Joe Doe and 1 other for you and Joe Doe and 1 other', done => {

			const actual = utils.readers2string(['You', 'Joe Doe', 'Dean'], postType)

			actual.should.equal('You, Joe Doe and 1 other read this note')
			done()
		})

		it('should return You, Joe Doe and 5 others for you and Joe Doe and 5 other', done => {

			const actual = utils.readers2string(['You', 'Joe Doe', 'Dean', 'Tom', 'Jack', 'Bred', 'Alice'], postType)

			actual.should.equal('You, Joe Doe and 5 others read this note')
			done()
		})

		it('should return Joe Doe for Joe Doe', done => {

			const actual = utils.readers2string(['Joe Doe'], postType)

			actual.should.equal('Joe Doe read this note')
			done()
		})

		it('should return Joe Doe and 1 other for Joe Doe and 1 other', done => {

			const actual = utils.readers2string(['Joe Doe', 'Tom'], postType)

			actual.should.equal('Joe Doe and 1 other read this note')
			done()
		})

		it('should return Joe Doe and 3 others for Joe Doe and 3 other', done => {

			const actual = utils.readers2string(['Joe Doe', 'Tom', 'Jack', 'Alice'], postType)

			actual.should.equal('Joe Doe and 3 others read this note')
			done()
		})
	})

	describe('#isOverdue', () => {
		it('return true if before now', () => {
			const actual = utils.isOverdue('2016-02-19 23:59:59')

			should(actual).equal(true)
		})

		it('return false if right now', () => {
			const actual = utils.isOverdue(moment().add(1, 'second'))

			should(actual).equal(false)
		})

		it('return false if in 1 hour', () => {
			const actual = utils.isOverdue(moment().add(1, 'hour'))

			should(actual).equal(false)
		})
	})
})
