'use strict'

import should from 'should'
import htmlConverter from '../../../../shared/core/htmlConverter'

describe('shared/core/htmlConverter', () => {

	describe('#convertDescription', () => {
		it('should return sanitized description HTML with default params', done => {
			const description = `<span id="6570"><a href="https://app.hoteltap.com/index.php/hotel/board/657" rel="6570" id="657" class="dept" data-dept="0">@Lost &amp; Found</a></span>`

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal('<a>@Lost & Found</a>')
			done()
		})

		it('should return sanitized description HTML with custom tags', done => {
			const description = `<span id="6570"><a href="https://app.hoteltap.com/index.php/hotel/board/657" rel="6570" id="657" class="dept" data-dept="0">@Lost &amp; Found</a></span>`
			const options = {
				allowedTags: ['a', 'span']
			}

			const actual = htmlConverter.convertDescription(description, options)

			should(actual).equal('<span><a>@Lost & Found</a></span>')
			done()
		})

		it('should return sanitized description HTML with custom tag attributes', done => {
			const description = `<span id="6570"><a href="https://app.hoteltap.com/index.php/hotel/board/657" rel="6570" id="657" class="dept" data-dept='0'>@Lost &amp; Found</a></span>`
			const options = {
				allowedAttributes: {
					a: ['href', 'data-*']
				}
			}

			const actual = htmlConverter.convertDescription(description, options)

			should(actual).equal('<a href="https://app.hoteltap.com/index.php/hotel/board/657" data-dept="0">@Lost & Found</a>')
			done()
		})

		it('should return sanitized description HTML with not decoded special chars', done => {
			const description = `<span id="6570"><a href="https://app.hoteltap.com/index.php/hotel/board/657" rel="6570" id="657" class="dept" data-dept="0">@Lost &amp; Found</a></span>`

			const actual = htmlConverter.convertDescription(description, null, true)

			should(actual).equal('<a>@Lost &amp; Found</a>')
			done()
		})

		it('should convert broken HTML with default options', done => {
			const description = `<span "6570"><abc href=https://app.hoteltap.com/index.php/hotel/board/657" rel="6570" id='657' class="dept" data-dept="0">@Lost &amp; Found</a></div>`

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal('@Lost & Found')
			done()
		})

		/*eslint-disable no-irregular-whitespace */
		it('should convert HTML with multiple "@" symbols and "#" symbols', done => {
			const description = `&nbsp;<span></span><span id="470"><a href="https://app.hoteltap.com/index.php/hotel/empboard/47" rel="470" id="47" class="emp" data-dept="0">@Jessica Lorenzo</a></span>&nbsp;&nbsp;<span></span><span id="1745"><a href="https://app.hoteltap.com/index.php/hotel/empboard/174" rel="1745" id="174" class="emp" data-dept="0">@Victor Gonzalez</a></span>&nbsp; will come in today at 10 or 11AM&nbsp;to complete all Past Due Tasks in Maintenance and will let you know if Elidia or girls need to clean or fix anything.&nbsp;<span id="901"><a href="https://app.hoteltap.com/index.php/hotel/mboard/90" rel="901" id="90" class="mtype" data-dept="46">@Other Maintenance</a></span>&nbsp; <span id="52122"><a href="https://app.hoteltap.com/index.php/hotel/search/%23OTHER-Maintenance" rel="52122" id="5212" class="tag" data-type="mtype">#OTHER-Maintenance</a></span>&nbsp; <span id="453"><a href="https://app.hoteltap.com/index.php/hotel/board/45" rel="453" id="45" class="dept" data-dept="0">@House Keeping</a></span>&nbsp; <span id="50274"><a href="https://app.hoteltap.com/index.php/hotel/search/%23OTHER-Housekeeping" rel="50274" id="5027" class="tag" data-type="dept">#OTHER-Housekeeping</a></span>&nbsp;<span id="608"><a href="https://app.hoteltap.com/index.php/hotel/search/%40Staff%20Areas" rel="608" id="60" class="area" data-dept="0">@Staff Areas</a></span>&nbsp; <span id="51259"><a href="https://app.hoteltap.com/index.php/hotel/search/%23MaintenanceRoom" rel="51259" id="5125" class="tag" data-type="area">#MaintenanceRoom</a></span>&nbsp;`
			const expected = ` <a>@Jessica Lorenzo</a>  <a>@Victor Gonzalez</a>  will come in today at 10 or 11AM to complete all Past Due Tasks in Maintenance and will let you know if Elidia or girls need to clean or fix anything. <a>@Other Maintenance</a>  <a>#OTHER-Maintenance</a>  <a>@House Keeping</a>  <a>#OTHER-Housekeeping</a> <a>@Staff Areas</a>  <a>#MaintenanceRoom</a> `

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal(expected)
			done()
		})

		it('should convert HTML with copy&past image', done => {
			const description = `<span style="font-size: 12px; line-height: 17.1429px; background-color: rgb(230, 247, 253);">@@@TESTCOMMENT5&nbsp;</span><span style="font-size: 12px; line-height: 17.1429px; background-color: rgb(229, 229, 229);">with copy&amp;past image</span><img src="http://wallpapersonthe.net/wallpapers/b/3840x2400/3840x2400-minions_despicable_me_2_despicable_me_cgi-22108.jpg" alt="3840x2400-minions_despicable_me_2_despic">`
			const expected = `@@@TESTCOMMENT5 with copy&past image`

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal(expected)
			done()
		})

		it('should convert copy&past HTML', done => {
			const description = `<HTML>
<HEAD>
<TITLE>Your Title Here</TITLE>
</HEAD>
<BODY BGCOLOR="FFFFFF">
<CENTER><IMG SRC="clouds.jpg" ALIGN="BOTTOM"> </CENTER>
<HR>
<a href="http://somegreatsite.com">Link Name</a>
is a link to another nifty site
<H1>This is a Header</H1>
<H2>This is a Medium Header</H2>
Send me mail at <a href="mailto:support@yourcompany.com">
support@yourcompany.com</a>.
<P> This is a new paragraph!
<P> <B>This is a new paragraph!</B>
<BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>
<HR>
</BODY>
</HTML>`
			const expected = `\n\nYour Title Here\n\n\n \n\n<a>Link Name</a>\nis a link to another nifty site\nThis is a Header\nThis is a Medium Header\nSend me mail at <a>\nsupport@yourcompany.com</a>.\n This is a new paragraph!\n This is a new paragraph!\n This is a new sentence without a paragraph break, in bold italics.\n\n\n`

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal(expected)
			done()
		})

		it('should convert copied HTML without empty "<a>" tags', done => {
			const description = `<div class="textarea depted-tags" id="post_div" contenteditable="true" onkeyup="autocompletelist('post_div','0');"><div class="Name-block post-content-wrapper" style="width: 1140px; background-color: rgb(229, 229, 229);"><div class="description-wrapper" style="width: 1140px;"><a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/board/277" class="dept">@Lost &amp; Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Found" class="tag">#Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%40Guest%20Areas" class="area">@Guest Areas</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Bar" class="tag">#Bar</a>&nbsp; Hi All!</div></div><div class="post-images" style="background-color: rgb(229, 229, 229);"><div class="clearfix"></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" style="max-width: 150px;"></a></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" style="max-width: 150px;"></a></div><div><br></div><div class="clearfix"></div></div><div id="" class="comment-box" style="width: 1140px; background-color: rgb(229, 229, 229);"></div></div>`
			const expected = `<a>@Lost & Found</a>  <a>#Found</a>  <a>@Guest Areas</a>  <a>#Bar</a>  Hi All!`

			const actual = htmlConverter.convertDescription(description)

			should(actual).equal(expected)
			done()
		})

		it('should convert HTML with empty "<a>" tags', done => {
			const description = `<div class="textarea depted-tags" id="post_div" contenteditable="true" onkeyup="autocompletelist('post_div','0');"><div class="Name-block post-content-wrapper" style="width: 1140px; background-color: rgb(229, 229, 229);"><div class="description-wrapper" style="width: 1140px;"><a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/board/277" class="dept">@Lost &amp; Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Found" class="tag">#Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%40Guest%20Areas" class="area">@Guest Areas</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Bar" class="tag">#Bar</a>&nbsp; Hi All!</div></div><div class="post-images" style="background-color: rgb(229, 229, 229);"><div class="clearfix"></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" style="max-width: 150px;"></a></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" style="max-width: 150px;"></a></div><div><br></div><div class="clearfix"></div></div><div id="" class="comment-box" style="width: 1140px; background-color: rgb(229, 229, 229);"></div></div>`
			const expected = `<a>@Lost & Found</a>  <a>#Found</a>  <a>@Guest Areas</a>  <a>#Bar</a>  Hi All!<a></a><a></a>`

			const actual = htmlConverter.convertDescription(description, null, null, true)

			should(actual).equal(expected)
			done()
		})

		/*eslint-enable no-irregular-whitespace */
	})

	describe('#convertToText', () => {

		it('should return only text from html', done => {
			const description = `<HTML>
<HEAD>
<TITLE>Your Title Here</TITLE>
</HEAD>
<BODY BGCOLOR="FFFFFF">
<CENTER><IMG SRC="clouds.jpg" ALIGN="BOTTOM"> </CENTER>
<div>&nbsp;Hello!&nbsp;&nbsp;</div>
<HR>
<a href="http://somegreatsite.com">Link Name</a>
is a link to another nifty site
<H1>This is a Header</H1>
<H2>This is a Medium Header</H2>
Send me mail at <a href="mailto:support@yourcompany.com">
support@yourcompany.com</a>.
<P> This is a new paragraph!
<P> <B>This is a new paragraph!</B>
<BR> <B><I>This is a new sentence without a paragraph break, in bold italics.</I></B>
<HR>
</BODY>
</HTML>`
			const expected = `Your Title Here Hello! Link Name is a link to another nifty site This is a Header This is a Medium Header Send me mail at support@yourcompany.com. This is a new paragraph! This is a new paragraph! This is a new sentence without a paragraph break, in bold italics.`

			const actual = htmlConverter.convertToText(description)

			should(actual).equal(expected)
			done()
		})


		it('should convert HTML to text and Remove only mentions and hashtags', done => {
			const description = `<div class="textarea depted-tags" id="post_div" contenteditable="true" onkeyup="autocompletelist('post_div','0');"><div class="Name-block post-content-wrapper" style="width: 1140px; background-color: rgb(229, 229, 229);"><div class="description-wrapper" style="width: 1140px;"><a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/board/277" class="dept">@Lost &amp; Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Found" class="tag">#Found</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%40Guest%20Areas" class="area">@Guest Areas</a>&nbsp;&nbsp;<a href="http://hotels.dev.192.168.1.33.xip.io/index.php/hotel/search/%23Bar" class="tag">#Bar</a><a href="/asd/asd/asd">I am simple link (don't remove me).</a>&nbsp; Hi All!</div></div><div class="post-images" style="background-color: rgb(229, 229, 229);"><div class="clearfix"></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125538_009_apcm_450x450_0015.jpg" style="max-width: 150px;"></a></div><div class="attachment-image"><a href="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/htap-uploads-dev/uploads/080416125542_009_apcm_450x450_0017.jpg" style="max-width: 150px;"></a></div><div><br></div><div class="clearfix"></div></div><div id="" class="comment-box" style="width: 1140px; background-color: rgb(229, 229, 229);"></div></div>`
			const expected = `I am simple link (don't remove me). Hi All!`

			const actual = htmlConverter.convertToText(description)

			should(actual).equal(expected)
			done()
		})
	})
})
