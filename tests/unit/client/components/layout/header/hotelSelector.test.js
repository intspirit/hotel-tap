'use strict'

import should from 'should'
import React from 'react'
import {mount, render, shallow} from 'enzyme'
import HotelSelector from '../../../../../../client/components/layout/header/HotelSelector'

describe('<HotelSelector />', () => {

	// @TODO sk p2 Write correct tests
	it('empty hotels list', done => {
		const hotelSelected = () => {}
		const actual = shallow(<HotelSelector onHotelSelected={hotelSelected}/>)
		actual.setState({
			hotels: []
		})
		done()
	})
})
