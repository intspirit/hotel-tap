'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import NavigationOption from '../../../../../../client/components/layout/navigation/NavigationOption'


describe('<NavigationOption />', () => {

	it('renders option without badge if count is 0', done => {
		const option = {
			id: 1,
			url: 'board/1',
			iconUrl: '',
			iconClass: 'fa-home',
			label: 'Front Desk',
			count: 0
		}
		const optionSelected = () => {}
		const actual = shallow(<NavigationOption option={option} onOptionSelected={optionSelected}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[2]).be.not.ok()

		done()
	})

	it('renders option with badge if count is not 0', done => {
		const option = {
			id: 1,
			url: 'board/1',
			iconUrl: '',
			iconClass: 'fa-home',
			label: 'Front Desk',
			count: 10
		}
		const optionSelected = () => {}
		const actual = shallow(<NavigationOption option={option} onOptionSelected={optionSelected}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[2]).be.ok()
		should(actual.find('.badge').text()).equal('10')
		done()
	})

	it('calls onOptionSelected if clicked on', done => {
		const option = {
			id: 1,
			url: 'board/1',
			iconUrl: '',
			iconClass: 'fa-home',
			label: 'Front Desk',
			count: 10
		}
		let functionIsCalled = false
		const optionSelected = () => { functionIsCalled = true }
		const actual = shallow(<NavigationOption option={option} onOptionSelected={optionSelected}/>)
		actual.simulate('click')

		should(functionIsCalled).be.true()

		done()
	})
})
