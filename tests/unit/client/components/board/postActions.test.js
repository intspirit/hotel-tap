'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import PostActions from '../../../../../client/components/board/PostActions'
import {postTypes, postReadStatus} from '../../../../../shared/constants'

describe('<PostActions />', () => {

	it('actions for note type', done => {
		const props = {
			typeId: postTypes.NOTE
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<PostActions {...props} showCommentsEntry={showCommentsEntry}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(7)
		should(actual.nodes[0].props.children[3]).be.ok()
		should(actual.nodes[0].props.children[4]).be.ok()

		done()
	})

	it.skip('actions for not note type', done => {
		//TODO: sk p2 review the correctness of the test and fix it
		const props = {
			typeId: ''
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<PostActions {...props} showCommentsEntry={showCommentsEntry}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(7)
		should(actual.nodes[0].props.children[3]).be.not.ok()
		should(actual.nodes[0].props.children[4]).be.not.ok()

		done()
	})

	it.skip('calls showCommentsEntry if comment clicked', done => {
		//TODO: sk p2 review the correctness of the test and fix it
		let functionIsCalled = false
		const showCommentsEntry = () => {
			functionIsCalled = true
		}
		const actual = shallow(<PostActions showCommentsEntry={showCommentsEntry}/>)

		actual.find('.fa-comment-o').
				parent().
				simulate('click')
		should(functionIsCalled).be.true()

		done()
	})

	it.skip('read action if post was readed', done => {
		//TODO: sk p2 review the correctness of the test and fix it
		const props = {
			readStatus: postReadStatus.READ
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<PostActions {...props} showCommentsEntry={showCommentsEntry}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(7)
		should(actual.contains(<i className="fa fa-envelope-o" />)).equal(true)
		should(actual.find('.readed').childAt(1).
			text()).equal('Mark as unread')

		done()
	})

	it.skip('flag action if post was flaged', done => {
		//TODO: sk p2 review the correctness of the test and fix it
		const props = {
			flag: {
				'set': true
			}
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<PostActions {...props} showCommentsEntry={showCommentsEntry}/>)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(7)
		should(actual.find('.fa-flag-o').parent().
			childAt(1).
			text()).equal('Unflag')

		done()
	})
})
