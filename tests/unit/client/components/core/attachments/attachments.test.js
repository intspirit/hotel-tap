'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import Attachments from '../../../../../../client/components/core/attachments/Attachments'

describe('<Attachments />', () => {

	it('renders attachments if count is 0', done => {
		const attachments = []
		const actual = shallow(<Attachments attachments={attachments} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children[0].props.files.length).equal(0)
		should(actual.nodes[0].props.children[1].props.images.length).equal(0)

		done()
	})

	it('renders attachments if count is more then 0', done => {
		const attachments = [
			{
				id: 301,
				name: 'Shipping label.jpg',
				type: 'image',
				url: 'http://www.mailcentermemphis.com/wp-content/uploads/2014/10/bow-tie-label.jpg'
			},
			{
				id: 302,
				name: 'Shipping label2.jpg',
				type: 'image',
				url: 'http://www.mailcentermemphis.com/wp-content/uploads/2014/10/bow-tie-label.jpg'
			},
			{
				id: 303,
				name: 'Not-image.jpg',
				type: '',
				url: 'http://www.mailcentermemphis.com/wp-content/uploads/2014/10/bow-tie-label.jpg'
			}
		]
		const actual = shallow(<Attachments attachments={attachments} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children[0].props.files.length).equal(1)
		should(actual.nodes[0].props.children[1].props.images.length).equal(2)

		done()
	})
})
