'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import Comments from '../../../../../../client/components/core/comments/Comments'

describe('<Comments />', () => {
	it('renders comments if count is 0', done => {
		const comments = []
		const actual = shallow(<Comments comments={comments} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[1].length).equal(0)

		done()
	})

	it('renders comments if count is more then 0', done => {
		const comments = [
			{
				id: 101,
				type: 'comment',
				createdBy: {
					id: 101,
					fullName: 'Mike G Gerald',
					imageUrl: 'https://s3-us-west-2.amazonaws.com/htap-files-test/img/profile/56e180e580977.jpg'
				},
				createdDateTime: '2016-06-05T19:25:43.511Z',
				description: 'Ok. I will take care',
				attachments: []
			},
			{
				id: 151,
				type: 'comment',
				createdBy: {
					id: 101,
					fullName: 'Mike G Gerald',
					imageUrl: 'https://s3-us-west-2.amazonaws.com/htap-files-test/img/profile/56e180e580977.jpg'
				},
				createdDateTime: '2016-06-07T19:25:43.511Z',
				description: 'UPSed to guest'
			}
		]
		const actual = shallow(<Comments comments={comments} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[1].length).equal(2)

		done()
	})

	it('renders comments if entry is opened', done => {
		const comments = {
			commentEntry: true,
			commentCount: 5,
			comments: []
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<Comments {...comments} showCommentsEntry={showCommentsEntry}/>)


		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[2]).be.ok()

		done()
	})

	it('renders comments if entry is closed', done => {
		const comments = {
			commentEntry: false,
			commentCount: 5,
			comments: []
		}
		const showCommentsEntry = () => {
			// do nothing.
		}
		const actual = shallow(<Comments {...comments} showCommentsEntry={showCommentsEntry}/>)


		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(3)
		should(actual.nodes[0].props.children[2]).be.not.ok()

		done()
	})
})
