'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import CommentsExpander from '../../../../../../client/components/core/comments/CommentExpander'


describe('<CommentsExpander />', () => {

	it('renders expander if count is more then 2', done => {
		const comments = {
			allComments: false,
			commentCount: 5
		}
		const showAllComments = () => {
			// do nothing.
		}
		const actual = shallow(<CommentsExpander {...comments} showAllComments={showAllComments} />)

		should(actual.length).equal(1)
		should(actual.nodes[0]).be.ok()
		should(actual.nodes[0].props.children.length).equal(4)
		should(actual.find('.comments-expander').text()).equal('View 3 more comments')

		done()
	})

	it('calls showAllComments if clicked on', done => {
		const comments = {
			allComments: false,
			commentCount: 5
		}

		const showAllComments = () => {
			comments.allComments = true
		}
		const actual = shallow(<CommentsExpander {...comments} showAllComments={showAllComments} />)
		actual.simulate('click')

		should(comments.allComments).be.true()

		done()
	})

	it('hide expander if allComments', done => {
		const comments = {
			allComments: true,
			commentCount: 5
		}

		const showAllComments = () => {
			// do nothing.
		}
		const actual = shallow(<CommentsExpander {...comments} showAllComments={showAllComments} />)
		should(actual.length).equal(1)
		should(actual.nodes[0]).be.not.ok()

		done()
	})

	it('expander message if diff is 1', done => {
		const comments = {
			allComments: false,
			commentCount: 3
		}
		const showAllComments = () => {
			// do nothing.
		}
		const actual = shallow(<CommentsExpander {...comments} showAllComments={showAllComments} />)


		should(actual.length).equal(1)
		should(actual.nodes[0]).be.ok()
		should(actual.nodes[0].props.children.length).equal(4)
		should(actual.find('.comments-expander').text()).equal('View 1 more comment')

		done()
	})
})
