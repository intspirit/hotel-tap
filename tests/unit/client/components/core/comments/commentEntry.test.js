'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import CommentEntry from '../../../../../../client/components/core/comments/CommentEntry'

describe('<CommentEntry />', () => {

    it.skip('calls showCommentsEntry if comment clicked', done => {
        let functionIsCalled = false
        const showCommentsEntry = () => { functionIsCalled = true }
        const actual = shallow(<CommentEntry showCommentsEntry={showCommentsEntry}/>)
        actual.addComment = () => {showCommentsEntry()}

        actual.find('.send').simulate('click')
        should(functionIsCalled).be.true()

        done()
    })
})
