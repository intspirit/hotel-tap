'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import Readers from '../../../../../client/components/core/Readers'
import {postReadStatus, postTypes} from '../../../../../shared/constants'


describe('<Readers />', () => {

	it('show readers if count is less then 2', done => {
		const props = {
			readBy: []
		}
		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.node).be.not.ok()

		done()
	})

	it('show readers if count is 2 and first Reader is "You"', done => {
		const props = {
			readBy: ['You', 'reader'],
			typeId: postTypes.NOTE
		}
		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(2)
		should(actual.nodes[0].props.children[0]).be.not.ok()
		should(actual.find('a').text()).equal('You, reader read this note')

		done()
	})

	it('show readers if count is 3', done => {
		const props = {
			readBy: [null, 'reader1', 'reader2'],
			typeId: postTypes.NOTE,
			readStatus: postReadStatus.UNREAD
		}
		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.nodes[0]).be.ok()
		should(actual.nodes[0].props.children.length).equal(2)
		should(actual.nodes[0].props.children[0]).be.not.ok()
		should(actual.find('a').text()).equal('reader1 and 1 other read this note')

		done()
	})

	it('show readers if count is more than 3', done => {
		const props = {
			readBy: [null, 'reader1', 'reader2', 'reader3', 'reader4'],
			typeId: postTypes.NOTE,
			readStatus: postReadStatus.UNREAD
		}
		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.nodes[0]).be.ok()
		should(actual.nodes[0].props.children.length).equal(2)
		should(actual.nodes[0].props.children[0]).be.not.ok()
		should(actual.find('a').text()).equal('reader1 and 3 others read this note')

		done()
	})

	it('calls showAllReaders if clicked on', done => {
		const props = {
			readBy: ['reader1', 'reader2'],
			typeId: postTypes.NOTE,
			readStatus: postReadStatus.UNREAD
		}

		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.state().allReaders).be.false()
		actual.find('a').simulate('click')
		should(actual.state().allReaders).be.true()

		done()
	})

	it('show ReadersList if clicked on', done => {
		const props = {
			readBy: [null, 'reader1', 'reader2', 'reader3'],
			typeId: postTypes.NOTE,
			readStatus: postReadStatus.UNREAD
		}

		const actual = shallow(<Readers {...props} />)

		should(actual.length).equal(1)
		should(actual.nodes[0].props.children.length).equal(2)
		should(actual.nodes[0].props.children[0]).be.not.ok()
		actual.find('a').simulate('click')
		should(actual.nodes[0].props.children[0]).be.ok()

		done()
	})

})
