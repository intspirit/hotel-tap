'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import sinon from 'sinon'
import {taskStatus} from '../../../../../shared/constants'
import PMLocationMonthSchedule from '../../../../../client/components/pm/PMLocationMonthSchedule'

(true).should.be.true()

describe('<PMLocationMonthSchedule />', () => {

	describe('canSchedule', () => {

		/*
		 [x] - 1. no schedule, past month
		 [x] - 2. no schedule, not past month
		 [x] - 3. schedule, open, past month
		 [x] - 4. schedule, open, not past month
		 [x] - 5. schedule, due past month, completed in the past month, past month
		 [] - 6. schedule, due past month, completed in this month, past month
		 [] - 7. schedule, due past month, completed in this month, not past month
		 [] - 8. schedule, due not past month, completed in past month, past month
		 [] - 9. schedule, due not past month, completed in this month, past month
		 [] - 10. schedule, due not past month, completed in this month, not past month
		 */

		it('should return false if no schedule and past month - 1', () => {
			const wrapper = shallow(<PMLocationMonthSchedule isPastMonth={true}/>)
			const actual = wrapper.find('input')

			should(actual.length).equal(0)
		})

		it('should return true if no schedule and not past month - 2', () => {
			const wrapper = shallow(<PMLocationMonthSchedule isPastMonth={false}/>)
			const actual = wrapper.find('input')

			should(actual.length).equal(1)

		})

		it('should return true if schedule and open and past month - 3', () => {
			const schedule = {
				dueDateTime: '2016-10-10 23:59:59',
				completion: {
					status: taskStatus.OPEN
				}
			}
			const wrapper = shallow(<PMLocationMonthSchedule schedule={schedule} isPastMonth={true}/>)
			const actual = wrapper.find('input')

			should(actual.length).equal(1)

		})

		it('should return true if schedule and open and not past month - 4', () => {
			const schedule = {
				dueDateTime: '2016-10-10 23:59:59',
				completion: {
					status: taskStatus.OPEN
				}
			}
			const wrapper = shallow(<PMLocationMonthSchedule schedule={schedule} isPastMonth={false}/>)
			const actual = wrapper.find('input')

			should(actual.length).equal(1)

		})

		it('should return false if schedule and done and past month - 5', () => {
			const schedule = {
				dueDateTime: '2016-09-10 23:59:59',
				completion: {
					dateTime: '2016-09-22 10:18:47',
					status: taskStatus.DONE
				}
			}
			const wrapper = shallow(<PMLocationMonthSchedule schedule={schedule} isPastMonth={true}/>)
			const actual = wrapper.find('input')

			should(actual.length).equal(0)

		})

		it('should return false if schedule and done and past month - 6', () => {
			const schedule = {
				dueDateTime: '2016-09-10 23:59:59',
				completion: {
					dateTime: '2016-10-22 10:18:47',
					status: taskStatus.DONE
				}
			}
			const wrapper = shallow(<PMLocationMonthSchedule schedule={schedule} isPastMonth={true}/>)
			console.log(wrapper.debug())
			const actual = wrapper.find('input')

			should(actual.length).equal(0)


		})
		it.skip('should return false if schedule and done and past month - 6-1', () => {
			const schedule = {
				dueDateTime: '2016-09-10 23:59:59',
				completion: {
					dateTime: '2016-10-22 10:18:47',
					status: taskStatus.DONE
				}
			}
			const wrapper = shallow(<PMLocationMonthSchedule schedule={schedule} isPastMonth={false}/>)
			console.log(wrapper.debug())
			const actual = wrapper.find('input')

			should(actual.length).equal(1)

		})
	})

})
