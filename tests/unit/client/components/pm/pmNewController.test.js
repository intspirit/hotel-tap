'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import sinon from 'sinon'
import PMNewController from '../../../../../client/components/pm/PMNewController'
import PMNew from '../../../../../client/components/pm/PMNew'
import {messages, utils} from '../../../../../shared/core'
import {pmService} from '../../../../../shared/services/index'

const event = {
	preventDefault: () => utils.emptyFunc()
}
let browserHistory = {
	push: () => utils.emptyFunc()
}


describe('<PMNewController />', () => {

	let wrapper = null
	let sandbox = null

	beforeEach(() => {
		sandbox = sinon.sandbox.create()
		wrapper = shallow(<PMNewController />)
	})

	afterEach(() => {
		sandbox.restore()
	})

	it('renders PM', () => {
		should(wrapper.find(PMNew).length).equal(1)
	})

	it('sets new name and description in state on change', () => {
		const newName = 'new name'
		const newDescription = 'new description'

		wrapper.instance().onNameDescriptionChanged(newName, newDescription)

		should(wrapper.state('pm').name).equal(newName)
		should(wrapper.state('pm').description).equal(newDescription)
	})

	it('validator returns no error messages if name is not empty', () => {
		wrapper.setState({pm: {name: 'PM'}})

		const actual = wrapper.instance().validate()

		should(actual.length).equal(0)
	})

	it('validator returns error message if name is empty', () => {
		wrapper.setState({pm: {name: '      '}})

		const actual = wrapper.instance().validate()

		should(actual.length).equal(1)
		should(actual[0]).equal(messages.error.NAME_REQUIRED)
	})

	it('onSave stops if there are errors', () => {
		wrapper.setState({pm: {name: '      '}})
		const spy = sinon.spy(wrapper.instance(), 'save')
		wrapper.update()

		wrapper.instance().onSave(event)

		should(wrapper.state('errorMessages').length).equal(1)
		should(wrapper.state('errorMessages')[0]).equal(messages.error.NAME_REQUIRED)
		should(spy.notCalled).be.true()
	})

	it('onSave calls save if there are no errors', () => {
		wrapper.setState({pm: {name: 'PM'}})
		const stub = sinon.stub(wrapper.instance(), 'save')
		wrapper.update()

		wrapper.instance().onSave(event)

		should(stub.calledOnce).be.true()
	})

	it('redirects to PM edit form after PM is added', async () => {
		const pmServiceStub = sandbox.stub(pmService, 'insert', () => Promise.resolve({newId: 5}))

		const browserHistoryStub = sandbox.stub(browserHistory, 'push')

		await wrapper.instance().save(browserHistory)

		should(pmServiceStub.calledOnce).be.true()
		should(browserHistoryStub.calledOnce).be.true()
		should(browserHistoryStub.getCalls(0)[0].args[0]).equal('/pm/5')
	})

})
