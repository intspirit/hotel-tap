'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import PMTasks from '../../../../../client/components/pm/PMTasks'
import PMTask from '../../../../../client/components/pm/PMTask'

describe('<PMTasks />', () => {
	const tasks = [
		{id: 1, subject: 'Task #1'},
		{id: 2, subject: 'Task #2'},
		{id: 3, subject: 'Task #3'},
		{id: 4, subject: 'Task #4'}
	]

	it('renders tasks', () => {
		const actual = shallow(<PMTasks tasks={tasks}/>)

		should(actual.find(PMTask).length).equal(tasks.length)
	})


	//TODO: vvs p2 UNDONE
	it.skip('marks task as deleted', () => {
		// const actual = shallow(<PMTasks tasks={tasks}/>)

		// const d = actual.find('tbody').children(PMTask).last()
		// should(d.find('input').length).equal(1)
		// d.find('a').first().simulate('click')
		// should(d.find('input').length).equal(0)
		true.should.be.true()
	})

})
