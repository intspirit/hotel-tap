'use strict'

import should from 'should'
import React from 'react'
import {shallow} from 'enzyme'
import sinon from 'sinon'
import PMTask from '../../../../../client/components/pm/PMTask'
import constants from '../../../../../shared/constants'
import appController from '../../../../../client/core/appController'

describe('<PMTask />', () => {
	appController.user = {
		isAdmin: true
	}
	const task = {
		id: 1,
		subject: 'Task #1'
	}

	it('renders task with subject', () => {
		const actual = shallow(<PMTask task={task}/>)

		should(actual.find('input').props().value).equal(task.subject)
		should(actual.find('i').props().id).equal(task.id)
	})

	it('calls parent onChange if subject changed AND blur', () => {
		const onChange = sinon.spy()
		const newSubject = 'Changed task'

		const actual = shallow(<PMTask task={task} onChange={onChange}/>)
		const input = actual.find('input').first()
		input.simulate('change', {target: {value: newSubject}})
		input.simulate('blur')

		should(actual.find('input').props().value).equal(newSubject)
		should(onChange.called).be.true()
	})

	it('does not calls parent onChange if Enter is pressed BUT subject is the same', () => {
		const onChange = sinon.spy()

		const actual = shallow(<PMTask task={task} onChange={onChange}/>)
		const input = actual.find('input').first()
		input.simulate('keyDown', {keyCode: 13})

		should(input.props().value).equal(task.subject)
		should(onChange.called).be.false()
	})

	it('calls parent onDelete if delete icon clicked', () => {
		const onDelete = sinon.spy()

		const actual = shallow(<PMTask task={task} onDelete={onDelete}/>)
		actual.find('a').first().
		simulate('click')

		should(onDelete.called).be.true()
	})

	it.skip('does not render deleted', () => {
		task.changed = constants.changeStatus.DELETE
		const actual = shallow(<PMTask task={task}/>)

		should(actual.find('input').length).equal(0)
		task.changed = null
	})

})
