'use strict'

import 'mochawait'
import should from 'should'
import target from '../../../../server/db/attachmentDb'

should.equal(true, true)

describe('server/db/attachmentDb', () => {

	describe('#getByPostIds', () => {
		it('should return array of attachments for valid post ids', async () => {
			const actual = await target.getByPostIds([590, 591])

			should(actual).ok()
			should(actual.length).equal(3)
		})
	})

	describe('#getByPostIds', () => {
		it('should return empty array of attachments for posts w/o attachments', async () => {
			const actual = await target.getByPostIds([-10, -20])

			should(actual).ok()
			should(actual.length).equal(0)
		})
	})
})

