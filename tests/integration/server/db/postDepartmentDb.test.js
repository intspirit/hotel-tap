'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import target from '../../../../server/db/postDepartmentDb'

should.equal(true, true)

describe.skip('server/db/postDepartmentDb', () => {

	describe('#insertBulk', () => {
		it('should insert', async() => {
			const rows = [
				[107, 23308, 660, '2016-08-22 15:49:55.066'],
				[107, 23308, 661, '2016-08-22 15:49:55.066']]
			const actual = await target.insertBulk(rows)

			should(actual).be.ok()
		})

	})
})

