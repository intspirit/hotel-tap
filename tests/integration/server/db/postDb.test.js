'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import target from '../../../../server/db/postDb'
import constants from '../../../../shared/constants'
import Post from '../../../../server/models/Post'

should.equal(true, true)

describe('server/db/postDb', () => {

	describe.skip('#getbyDepartmentId', () => {
		it('should return department posts', async() => {
			const actual = await target.getByDepartmentId(107, 655, 0, 10, ['note', 'task'])

			should(actual).be.Array()
			should(actual.length).equal(10)
		})

	})

	describe.skip('#updateTaskCompletionStatus', () => {
		it('should update status for done', async() => {
			const statusUpdate = {
				status: constants.taskStatus.DONE,
				completionDateTime: new Date(),
				completionDateTimeUtc: new Date()
			}

			const actual = await target.updateTaskCompletionStatus(107, 485, 29988, statusUpdate)

			should(actual).equal(1)
		})

		it('should update status for undone', async () => {
			const statusUpdate = {
				status: constants.taskStatus.OPEN,
				completionDateTime: null,
				completionDateTimeUtc: null
			}

			const actual = await target.updateTaskCompletionStatus(107, null, 29988, statusUpdate)

			should(actual).equal(1)
		})

		it('should not update for wrong data', async () => {
			const statusUpdate = {
				status: constants.taskStatus.DONE,
				completionDateTime: new Date(),
				completionDateTimeUtc: new Date()
			}

			const actual = await target.updateTaskCompletionStatus(99999, 485, 68892, statusUpdate)

			should(actual).equal(0)
		})

	})

	describe('#insertComment', () => {
		it('should insert comment', async() => {
			const newComment = Post.getNewComment(107, 485, 68892, `New comment - ${new Date()}`, 'America/Los_Angeles')
			const actual = await target.insertComment(107, 485, 68892, newComment)

			should(actual).be.greaterThan(68892)
		})

	})
})

