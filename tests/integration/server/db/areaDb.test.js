'use strict'

import should from 'should'
import target from '../../../../server/db/areaDb'

should.equal(true, true)

describe('server/db/areaDb', () => {

	describe('#getAreasByHotelId', () => {
		it('should return areas ', done => {
			target.getByHotelId(51).then(data => {
				should(data).be.ok().and.be.an.Array()
				should(data.length).greaterThan(0)
				done()
			}).catch(done)
		})
	})
})
