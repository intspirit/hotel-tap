'use strict'

import 'mochawait'
import should from 'should'
import target from '../../../../server/db/userDb'

should.equal(true, true)

describe('server/db/userDb', () => {

	describe('#geById', () => {
		it('should return a user for existing id', async() => {
			const actual = await target.getById(5)

			should.ok(actual)
			should.equal(actual.firstName, 'Parry Bir')
		})


		it('should return null for non-existing id', async() => {
			const actual = await target.getById(-5)

			should.not.exist(actual)
		})
	})

	describe('#geByIds', () => {
		it('should return a list of users for existing ids', async() => {
			const actual = await target.getByIds([486, 40])

			should.ok(actual)
			should.equal(actual.length, 2)
			should.equal(actual[0].firstName, 'Sandip')
		})


		it('should return empty array for non-existing ids', async() => {
			const actual = await target.getByIds([-10])

			should.ok(actual)
			should.equal(actual.length, 0)
		})
	})
})

