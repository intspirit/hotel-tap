'use strict'

import 'mochawait'
import should from 'should'
import target from '../../../../server/db/departmentDb'

should.equal(true, true)

describe('server/db/departmentDb', () => {

	describe('#geById', () => {
		it('should return a department for valid id', async () => {
			const actual = await target.getById(1)

			should(actual).ok()
			should(actual.name).equal('Front Desk')
		})
	})

	describe('#geByUserId', () => {
		it('should return departments for user', async () => {
			const actual = await target.getByUser(107, 488)

			should(actual).ok()
			should(actual.length).greaterThan(0)
			should(actual[0].name).equal('Front Desk')
		})
	})

	describe('#getByHotelId', () => {
		it('should return departments for a hotel', async () => {
			const actual = await target.getByHotelId(5)

			should(actual).ok()
			should(actual.length).greaterThan(0)
		})
	})
})

