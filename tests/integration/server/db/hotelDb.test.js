'use strict'

import should from 'should'
import target from '../../../../server/db/hotelDb'

should.equal(true, true)

describe('server/db/hotelDb', () => {

	describe('#geById', () => {
		it('should return a hotel for valid id', done => {
			target.getById(13).then(x => {
				should(x).ok()
				should(x.name).equal('Hawthorn Suites by Wyndham - Oakland/Alameda')
				done()
			}).
			catch(error => done(error))
		})

		it('should return null for non-existing id', done => {
			target.getById(-1).then(x => {
				should(x).null()
				done()
			}).
			catch(error => done(error))
		})
	})

	describe('#getByHotelId', () => {
		it('should return hotels for an admin', done => {
			target.getByAdmin(40).then(x => {
				should(x).ok()
				should(x.length).greaterThan(0)
				done()
			}).
			catch(error => done(error))
		})
	})
})
