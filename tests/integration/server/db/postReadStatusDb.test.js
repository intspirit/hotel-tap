'use strict'

//noinspection JSUnresolvedVariable
import should from 'should'
import target from '../../../../server/db/postReadStatusDb'
import constants from '../../../../shared/constants'

should.equal(true, true)

describe('server/db/postReadStatusDb', () => {

	describe('#getDepartmentCountsByUserId', () => {
		it('should return department counts', async () => {
			const actual = await target.getDepartmentCountsByUserId(107, 485, constants.postReadStatus.UNREAD)

			should(actual).be.Array()
			should(actual.length).greaterThan(0)
		})

		it('should return 0 length array when there are no', async () => {
			const nonExistingHotel = -1

			const actual = await target.getDepartmentCountsByUserId(nonExistingHotel, 485, constants.postReadStatus.UNREAD)

			should(actual).be.Array()
			should(actual.length).equal(0)
		})
	})

	describe('#getUserCountsByUserId', () => {
		it('should return user count', async () => {
			const actual = await target.getUserCountsByUserId(107, 485, constants.postReadStatus.UNREAD)

			should(actual).be.Array()
			should(actual.length).greaterThan(0)
		})

		it('should return 0 length array when there are no', async () => {
			const nonExistingHotel = -1

			const actual = await target.getUserCountsByUserId(nonExistingHotel, 485, constants.postReadStatus.UNREAD)

			should(actual).be.Array()
			should(actual.length).equal(1)
			should(actual[0].count).equal(0)
		})
	})

	describe('#getByPostIds', () => {
		it.skip('should return users when data exists', async () => {
			// const actual = await target.getByPostIds(107, [20893, 23763], constants.postReadStatus.READ)
			const actual = await target.getByPostIds(107, [30018, 30019], constants.postReadStatus.READ)

			should(actual).be.Array()
			should(actual.length).greaterThan(0)
		})

		it('should return 0 length array when data not found', async () => {
			const actual = await target.getByPostIds(107, [-1, -2], constants.postReadStatus.UNREAD)

			should(actual).be.Array()
			should(actual.length).equal(0)
		})
	})

	describe('#update', () => {
		it('should return 1 for valid update request', async () => {
			const actual = await target.update(1, 1, 2, constants.postReadStatus.READ)

			should(actual).be.ok()
			console.log(actual)
		})

		it('should return 0 if post & user is not found', async () => {
			const actual = await target.update(-5, 1, 2, constants.postReadStatus.READ)

			console.log(actual)
			should.equal(actual, 0)
		})
	})
})

