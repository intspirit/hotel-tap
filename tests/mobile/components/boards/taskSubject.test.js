'use strict'

import React from 'react'
import {View} from 'react-native'
import {shallow} from 'enzyme'
import should from 'should'
import TaskSubject from '../../../../mobile/components/boards/TaskSubject'
import constants from '../../../../shared/constants'


describe('<TaskSubject />', () => {
	let taskDonePressed = () => null
	let post = {
		createdBy: {
			fullName: 'Joe Doe'
		},
		completion: {
			completedBy: {
				fullName: 'Jane Fonda',
				id: 33,
				imageUrl: 'jane_fonda.html',
			},
			dateTime: '2015-08-21T20:59:40.000Z',
			status: constants.taskStatus.DONE
		},
		typeId: constants.postTypes.TASK,
		createdDateTime: '2016-06-17T21:11:32.000Z'
	}

	it('renders nothing if post is not a task', () => {
		post.typeId = constants.postTypes.NOTE
		const actual = shallow(<TaskSubject post={post} taskDonePressed={taskDonePressed}/>)

		should(actual.find(View).length).equal(0)
	})

	it('renders View if post is a task', () => {
		post.typeId = constants.postTypes.TASK
		const actual = shallow(<TaskSubject post={post} taskDonePressed={taskDonePressed}/>)

		should(actual.type()).equal(View)
		should(actual.find(View).length).equal(1)
	})
})
