'use strict'

import React from 'react'
import {shallow} from 'enzyme'
import should from 'should'
import Board from '../../../../mobile/components/boards/Board'
import Post from '../../../../mobile/components/boards/Post'


describe('<Board />', () => {
	it('renders no posts for board with no posts', () => {
		const actual = shallow(<Board />)

		should(actual.find(Post).length).equal(0)
	})
})
