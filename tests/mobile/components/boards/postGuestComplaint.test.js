'use strict'

import React from 'react'
import {View} from 'react-native'
import {shallow} from 'enzyme'
import should from 'should'
import PostGuestComplaint from '../../../../mobile/components/boards/PostGuestComplaint'

describe('<PostGuestComplaint />', () => {
	it('renders nothing if it is not guest complaint', () => {
		const actual = shallow(<PostGuestComplaint guestComplaint={false}/>)

		should(actual.find(View).length).equal(0)
	})

	it('renders Guest Complain if it is a guest complaint', () => {
		const actual = shallow(<PostGuestComplaint guestComplaint={true}/>)

		should(actual.find(View).length).equal(1)
	})

})
