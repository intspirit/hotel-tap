'use strict'

import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {shallow} from 'enzyme'
import should from 'should'
import PostHeader from '../../../../mobile/components/boards/PostHeader'

describe('<PostHeader />', () => {
	let actual = {}
	let post = {
		createdBy: {
			fullName: 'Joe Doe'
		},
		typeId: 'note',
		createdDateTime: '2016-06-17T21:11:32.000Z'
	}
	const styles = StyleSheet.create({
		header: {
			flexDirection: 'row'
		},
		image: {
			width: 25,
			height: 25,
			overflow: 'visible',
			marginRight: 5
		},
		headerText: {},
		defaultFont: {
			fontSize: 16
		},
		smallFont: {
			fontSize: 8
		},
		grey: {
			color: '#C7C7CD'
		}
	})


	beforeEach(done => {
		actual = shallow(<PostHeader post={post}/>)
		done()
	})

	it('renders three <View /> components', () => {
		should(actual.find(View).length).equal(3)
	})

	it('renders poster name', () => {
		should(actual.contains(<Text style={[styles.defaultFont, {fontWeight: 'bold'}]}>Joe Doe</Text>)).be.ok()
	})

	it('renders added a note for note', () => {
		should(actual.contains(' added a ')).be.ok()
		should(actual.contains('note')).be.ok()
	})

	it('renders added a task for task', () => {
		post.typeId = 'task'

		actual = shallow(<PostHeader post={post}/>)

		should(actual.contains(' added a ')).be.ok()
		should(actual.contains('task')).be.ok()
	})

	it('renders post date and time', () => {
		should(actual.find(Text).at(2).
			contains(<Text style={[styles.smallFont, styles.grey]}>June 17 at 9:11pm</Text>)).be.ok()
	})
})
