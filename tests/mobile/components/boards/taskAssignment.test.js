'use strict'

import React from 'react'
import {View, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {shallow} from 'enzyme'
import should from 'should'
import TaskAssignment from '../../../../mobile/components/boards/TaskAssignment'
import constants from '../../../../shared/constants'


describe('<TaskSubject />', () => {
	let post = {}

	beforeEach(() => {
		post = {
			createdBy: {
				fullName: 'Joe Doe'
			},
			assignmentTo: {
				fullName: 'Jane Doe',
				id: 10,
				imageUr: 'url1',
				type: constants.assignmentTypes.EMPLOYEE
			},
			typeId: constants.postTypes.TASK,
			createdDateTime: '2016-06-17T21:11:32.000Z',
			completion: {
				completedBy: {
					fullName: '',
					id: -1,
					imageUrl: ''
				},
				dateTime: null,
				status: 'open'
			},
			dueDateTime: null
		}
	})

	it('renders noting for a note', () => {
		post.typeId = constants.postTypes.NOTE
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(0)
	})

	it('renders view', () => {
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(2)
	})

	it('renders due date if it is set', () => {
		post.dueDateTime = '2016-07-21T20:59:40.000Z'
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)
	})

	it('renders assigned to if not completed', () => {
		post.dueDateTime = '2016-07-21T20:59:40.000Z'
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)

		const assignmentText = actual.find(Text).at(0)
		should(assignmentText.props().children).equal('Jane Doe')
	})

	it('renders completed by name if completed', () => {
		post.dueDateTime = '2016-07-21T20:59:40.000Z'
		post.completion = {
			completedBy: {
				fullName: 'Jane Fonda',
				id: 33,
				imageUrl: 'jane_fonda.html'
			},
			dateTime: '2016-08-21T20:59:40.000Z',
			status: constants.taskStatus.DONE
		}
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)

		const assignmentText = actual.find(Text).at(0)
		should(assignmentText.props().children).equal('Jane Fonda')
	})

	it('renders not overdue due date', () => {
		post.dueDateTime = '2030-07-21T20:59:40.000Z'
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)

		const calendarIcon = actual.find(Icon).at(0)
		should(calendarIcon.props().color).equal('#C7C7CD')
		should(calendarIcon.props().name).equal('calendar')

		const dueText = actual.find(Text).at(1)
		should(dueText.props().children).equal('Jul 21 at 8:59pm')
		should(dueText.props().style[1].color).equal('#C7C7CD')
	})

	it('renders overdue due date', () => {
		post.dueDateTime = '2010-07-21T20:59:40.000Z'
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)

		const calendarIcon = actual.find(Icon).at(0)
		should(calendarIcon.props().color).equal('red')
		should(calendarIcon.props().name).equal('calendar')

		const dueText = actual.find(Text).at(1)
		should(dueText.props().children).equal('Jul 21 at 8:59pm')
		should(dueText.props().style[1].color).equal('red')
	})

	it('renders completion data if it is completed', () => {
		post.dueDateTime = '2010-07-21T20:59:40.000Z'
		post.completion = {
			completedBy: {
				fullName: 'Jane Fonda',
				id: 33,
				imageUrl: 'jane_fonda.html',
			},
			dateTime: '2015-08-21T20:59:40.000Z',
			status: constants.taskStatus.DONE
		}
		post.completion.status = constants.taskStatus.DONE
		const actual = shallow(<TaskAssignment post={post}/>)

		should(actual.find(View).length).equal(3)
		should(actual.find(Icon).length).equal(1)

		const calendarIcon = actual.find(Icon).at(0)
		should(calendarIcon.props().color).equal('#C7C7CD')
		should(calendarIcon.props().name).equal('calendar-check-o')

		const dueText = actual.find(Text).at(1)
		should(dueText.props().children).equal('Aug 21 at 8:59pm')
		should(dueText.props().style[1].color).equal('#C7C7CD')
	})

})
