'use strict'

import React from 'react'
import {View, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {shallow} from 'enzyme'
import should from 'should'
import PostDetails from '../../../../mobile/components/boards/PostDetails'
import constants from '../../../../shared/constants'


describe('<PostDetails />', () => {
	let post = {
		createdBy: {
			fullName: 'Joe Doe'
		},
		typeId: constants.postTypes.TASK,
		createdDateTime: '2016-06-17T21:11:32.000Z'
	}

	it('renders post', () => {
		post.typeId = constants.postTypes.NOTE
		const actual = shallow(<PostDetails post={post}/>)

		should(actual.find(View).length).equal(1)
	})

})
