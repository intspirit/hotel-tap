'use strict'

//noinspection JSUnresolvedVariable
import React from 'react'
//noinspection JSUnresolvedVariable
import should from 'should'
import {View, Text} from 'react-native'
import {shallow} from 'enzyme'
import {TagSelector, TagSelectorGroup} from '../../../../mobile/components/tags/TagSelector'
import constants from '../../../../shared/constants'


describe('<TagSelector />', () => {

	//Department w/o tags
	//Maintenance

	it.skip('renders', () => {
		const actual = shallow(<TagSelector/>)

		should(actual.find(View).length).equal(5)
	})

	// it('renders 1', () => {
	// 	const actual = shallow(<TagSelector/>)
	//
	// 	should(actual.state().departmentGroupedTags).be.ok()
	// 	// should(actual.find(TagSelectorGroup).length).equal(4)
	// 	should(actual.find(View).length).equal(3)
	// 	// console.log(actual.childAt(0).debug())
	// })
})
