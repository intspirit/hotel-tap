'use strict'

import React from 'react'
import {View, Text} from 'react-native'
import {shallow} from 'enzyme'
import should from 'should'
import Readers from '../../../../mobile/components/core/Readers'


describe('<Readers />', () => {
	it('renders nothing readers are null', () => {
		const actual = shallow(<Readers readers={null}/>)

		should(actual.find(View).length).equal(0)
	})

	it('renders nothing for empty reader list', () => {
		const actual = shallow(<Readers readers={[]}/>)

		should(actual.find(View).length).equal(0)
	})

	it('renders comma separated list of the names for a few readers', () => {
		const actual = shallow(<Readers readers={['Joe Doe', 'Jane Doe']}/>)

		should(actual.find(View).length).equal(1)
		should(actual.find(Text).length).equal(2)
		should(actual.find(View).childAt(1).props().children).equal('Joe Doe, Jane Doe')
	})

	it('renders name for a single reader', () => {
		const actual = shallow(<Readers readers={['Joe Doe']}/>)

		should(actual.find(View).length).equal(1)
		should(actual.find(Text).length).equal(2)
		should(actual.find(View).childAt(1).props().children).equal('Joe Doe')
	})
})
