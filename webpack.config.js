'use strict'

const
	Webpack = require('webpack'),
	environment = process.env.NODE_ENV || 'development'

//noinspection JSUnresolvedFunction
let plugins = [
	new Webpack.optimize.CommonsChunkPlugin({
		names: ['vendors'],
		minChunks: Infinity
	})
]
if (environment === 'development') {
	const WebpackNotifier = require('webpack-notifier')
	plugins.push(new WebpackNotifier({title: 'Webpack', alwaysNotify: false}))
}

//noinspection JSUnresolvedFunction
module.exports = {
	devtool: 'source-map',
	context: `${__dirname}/client`,
	entry: {
		bundle: './components/Index.js',
		vendors: [
			'react',
			'react-router',
			'react-bootstrap',
			'moment',
			'moment-timezone',
			'lodash'
		]
	},
	output: {
		path: `${__dirname}/client/public/js`,
		filename: '[name].js'
	},
	module: {
		loaders: [
			{
				test: /.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules|bower_components|api-docs)/,
				loaders: [
					'react-hot',
					'babel?presets[]=stage-0,presets[]=react,presets[]=es2015'
				]
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /(datepicker|loader|datetime)\.css$/,
				loaders: [
					'style', 'css'
				]
			}
		]
	},

	resolve: {
		extensions: ['', '.js', '.jsx', '.json']
	},
	plugins: plugins,
	node: {
		net: 'empty',
		tls: 'empty',
		dns: 'empty'
	}
}
