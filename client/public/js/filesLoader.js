// Here we actually invoke Fallback JS to retrieve the following libraries for the page.
fallback.load({
	// The first will fail, therefore Fallback JS will load the second!
	jQuery: [
		'//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js',
		'/js/libs/jquery.min.js'
	],

	'jQuery.ui': [
		'//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
		'/js/libs/jquery-ui.min.js'
	],

	// fallback checks for a window object named 'bootstrap' which isn't defined by bootstrap.js
	// see https://github.com/dolox/fallback/issues/38
	'$.fn.modal': [
		'//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
		'/js/libs/bootstrap.min.js'
	],

	UserVoice: [
		'/js/userVoice.js'
	],

	// using window.webpackJsonp to detect vendors.js
	'webpackJsonp': ['/js/vendors.js'],
	'bundle': ['/js/bundle.js'],

	// using window.root to detect app.config.seed.js
	'root': ['/js/app.config.seed.js'],
	'app.seed': ['/js/app.seed.js']
}, {
	/*
	* Shim `keyNamed`-module so that it will only load after
	* `valueNamed`-module has completed!
	*/
	shim: {
		// JQuery UI after jQuery
		'jQuery.ui': ['jQuery'],

		// Bootstrap after jQuery
		'$.fn.modal': ['jQuery'],

		// bundle.js after vendor.js
		'bundle': ['webpackJsonp'],

		// app.config.seed.js after jQuery
		'root': ['jQuery'],

		// app.seed.js after app.config.seed.js
		'app.seed': ['root']
	}
});
