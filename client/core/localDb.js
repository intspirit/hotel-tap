'use strict'

import cookie from 'react-cookie'

const localDb = {
	jwtKey: 'jwtToken',
	userKey: 'user',
	hotelKey: 'hotel',
	rememberKey: 'rememberFlag',

	setItem(key, value) {
		this.removeItem(key)

		if (this.remember) {
			localStorage.setItem(key, value)

			return
		}

		cookie.save(key, value, {path: '/'})
	},

	getItem(key, parse) {
		const item = localStorage.getItem(key) || cookie.load(key)

		if (!parse || typeof item === 'object') {
			return item
		}

		try {
			return JSON.parse(item)
		} catch (err) {
			return null
		}
	},

	removeItem(key) {
		localStorage.removeItem(key)
		cookie.remove(key)
	},

	setUser(user) {
		this.setItem(this.userKey, JSON.stringify(user))
	},

	getUser() {
		return this.getItem(this.userKey, true)
	},

	deleteUser() {
		this.removeItem(this.userKey)
	},

	setJwt(jwt) {
		this.setItem(this.jwtKey, jwt)
	},

	getJwt() {
		return this.getItem(this.jwtKey)
	},

	deleteJwt() {
		this.removeItem(this.jwtKey)
	},

	setHotel(hotel) {
		this.setItem(this.hotelKey, JSON.stringify(hotel))
	},

	getHotel() {
		return this.getItem(this.hotelKey, true)
	},

	deleteHotel() {
		this.removeItem(this.hotelKey)
	},

	setRemember(remember) {
		localStorage.setItem(this.rememberKey, remember)
	},

	get remember() {
		return localStorage.getItem(this.rememberKey) === 'true'
	}
}

export default localDb
