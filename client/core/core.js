'use strict'

const core = {
	constant: {
		environment: {
			DEVELOPMENT: 'development',
			TESTING: 'testing',
			PRODUCTION: 'production'
		}
	}
}

if (typeof module !== 'undefined') module.exports = core

