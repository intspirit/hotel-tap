'use strict'

import {scroller, animateScroll} from 'react-scroll'

/**
 * Scroller.
 */
const Scroller = {

	scrollTo(component, options) {
		const defaultOptions = {
			duration: 500,
			delay: 350,
			smooth: true,
			offset: -20
		}

		scroller.register('element', component)
		scroller.scrollTo('element', Object.assign(defaultOptions, options || {}))
	},

	scrollToComponentBottom(component, options) {
		const bottomOffset = parseInt((options || {}).bottomOffset, 10)
		const offset = 0 - (window.innerHeight - bottomOffset)

		this.scrollTo(component, Object.assign(options, {offset}))
	},

	scrollToTop() {
		animateScroll.scrollToTop()
	}
}

export default Scroller
