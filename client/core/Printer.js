'use strict'

import React from 'react'

/**
 * Printer.
 * @constructor
 */
class Printer extends React.Component {

	print(element) {
		this.printElement(element)
		window.print()
	}

	printElement(elem, append, delimiter) {
		const domClone = elem.cloneNode(true)

		let printSectionElement = document.getElementById('printSection')

		if (!printSectionElement) {
			printSectionElement = document.createElement('div')
			printSectionElement.id = 'printSection'
			document.body.appendChild(printSectionElement)
		}

		if (append === true) {
			if (typeof delimiter === 'string') {
				printSectionElement.innerHTML += delimiter
			} else if (typeof delimiter === 'object') {
				printSectionElement.appendChlid(delimiter)
			}
		} else {
			printSectionElement.innerHTML = ''
		}

		printSectionElement.appendChild(domClone)
	}
}

export default Printer
