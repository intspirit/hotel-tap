'use strict'

import _ from 'lodash'
import sharedAppController from '../../shared/core/appController'
import localDb from './localDb'
import {identifyUserVoice} from '../functions'

/**
 * Application controller - controls application state and interaction among various components.
 */

const appController = {

	taskChanged(taskId) {
		if (this.postComponent) this.postComponent.refreshPost()
		if (this.postsComponent) this.postsComponent.refreshPost(taskId)
	},

	postCreated(postId) {
		if (this.postsComponent) this.postsComponent.getPosts(null, {
			focusedId: postId
		})
		if (this.todoComponent) this.todoComponent.refreshTodos()
	},

	postConvertedToTask(postId) {
		if (this.todoComponent) this.todoComponent.refreshTodos()
		if (this.postComponent) this.postComponent.refreshPost()
		if (this.postsComponent) this.postsComponent.refreshPost(postId, {
			reorder: true
		})
	},

	postReadStatusChanged(postId) {
		if (this.navigationComponent) this.navigationComponent.getUnreadCount()
		if (this.postsComponent) this.postsComponent.refreshPost(postId)
	},

	postFlagStatusChanged(postId) {
		if (this.postsComponent) this.postsComponent.refreshPost(postId, {
			reorder: true,
			focusedId: postId
		})
	},

	selectTask(taskId) {
		if (this.postsComponent) this.postsComponent.getPost(taskId)
	},

	disactivateSelectedNavigationOptionUntilUserAction() {
		if (this.navigationComponent) this.navigationComponent.disactivateNavigationOptionsUntilUserAction()
	},

	refreshTodos() {
		if (this.todoComponent) this.todoComponent.refreshTodos()
	},

	commentCreated(postId) {
		if (this.postComponent) this.postComponent.refreshPost()
		if (this.postsComponent) this.postsComponent.refreshPost(postId)
	},

	userProfileUpdated(user) {
		localDb.setUser(user)
		sharedAppController.setUser(user)
		identifyUserVoice(user)
		if (this.postsComponent) this.postsComponent.getPosts()
		if (this.navigationComponent) this.navigationComponent.getData()
	},

	hotelActivated(hotel) {
		sharedAppController.setHotel(hotel)
	},

	tagCreated(tag, group) {
		sharedAppController.resetTagsCache()
		if (this.tagsSelectorComponent) this.tagsSelectorComponent.tagAdded(tag, group)
	}
}

export default _.merge(sharedAppController, appController)
