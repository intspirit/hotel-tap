'use strict'

import {authService, userService} from '../../shared/services'
import appController from '../../shared/core/appController'
import localDb from './localDb'

/**
 * Authentication controller
 */
export default {

	async login(credentials) {
		credentials.storedHotel = localDb.getHotel() || {}

		const response = await authService.login(credentials)
		if (!response.token || !response.user) throw Error('Something went wrong. Please try again.')


		appController.jwtToken = response.token
		appController.authenticated = true
		await localDb.setRemember(credentials.remember)
		await localDb.setJwt(response.token)
		await localDb.setUser(response.user)
		await appController.setUser(response.user)

		return response
	},

	async loginToHotel(hotelId) {
		const response = await userService.switchHotel(hotelId)
		appController.jwtToken = response.token
		appController.authenticated = true
		await localDb.setJwt(response.token)
		await localDb.setUser(response.user)
		await appController.setUser(response.user)
		appController.resetCache()
	},

	async logout() {
		appController.jwtToken = null
		await localDb.deleteJwt()
		await localDb.deleteUser()
		await localDb.deleteHotel()
		appController.removeUser()
		appController.removeHotel()
		appController.resetCache()
	}
}
