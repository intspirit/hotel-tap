'use strict'

import React from 'react'
import pluralize from 'pluralize'
import ReadersList from './ReadersList'
import {readers} from '../../../shared/constants'

/**
 * Readers component.
 * @param {Object} props
 * @param {Array} props.readBy
 * @param {string} props.typeId
 * @constructor
 */
class Readers extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			allReaders: false
		}
		this.showAllReaders = this.showAllReaders.bind(this)
	}

	showAllReaders(e) {
		if (e) {
			e.preventDefault()
		}
		this.setState({
			allReaders: !this.state.allReaders
		})
	}

	render() {
		const diff = this.props.readBy.length - readers.DEFAULT_READERS_COUNT

		return this.props.readBy.length > 1 ?
			<div className="readers">
				{this.state.allReaders ?
					<ReadersList readers={this.props.readBy} showAllReaders={this.showAllReaders}/> :
					null
				}
				<a href="#" onClick={this.showAllReaders}>
					{this.props.readBy[0] === 'You' ? 'You, ' : null}
					{this.props.readBy[1]}
					{diff >= 1 && ` and ${diff} ${pluralize('other', diff)}`}
					{` read this ${this.props.typeId}`}
				</a>
			</div> :
			null
	}
}

Readers.propTypes = {
	readBy: React.PropTypes.array,
	typeId: React.PropTypes.string
}

Readers.defaultProps = {
	readBy: [],
	typeId: ''
}

export default Readers
