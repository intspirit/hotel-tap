'use strict'

import React from 'react'

/**
 * Subject component.
 * @param {Object} props
 * @param {string} props.subject
 * @constructor
 */
const Subject = props => <div className={`subject ${props.status}`}>
	<i className="fa fa-check-circle-o" onClick={props.changeStatus} /> <span>{props.subject}</span>
</div>

Subject.propTypes = {
	subject: React.PropTypes.string,
	status: React.PropTypes.string,
	changeStatus: React.PropTypes.func
}

Subject.defaultProps = {
	subject: ''
}

export default Subject
