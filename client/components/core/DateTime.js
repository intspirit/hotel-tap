'use strict'

import React from 'react'
import utils from '../../../shared/core/utils'
import appController from '../../core/appController'

/**
 * Date time component.
 * @param {Object} props
 * @param {string} props.createdDateTime
 * @constructor
 */
const DateTime = (props) =>
	<span className="from">
		{utils.formatDateTime(props.createdDateTime, appController.hotel.timeZone)}
	</span>

//TODO: ak p3 rename createdDateTime to dateTime
DateTime.propTypes = {
	createdDateTime: React.PropTypes.string
}

DateTime.defaultProps = {
	createdDateTime: ''
}

export default DateTime
