'use strict'

import React from 'react'
import {Link} from 'react-router'

/**
 * Renders post tags list.
 *
 * @param {Object} props
 * @param {{id: int, name: string, typeId: int, typeName: string}[]} props.tags
 * @constructor
 */
const PostTags = props => props.tags.length > 0 ? //eslint-disable-line no-confusing-arrow
	<div className="text">
		{props.showLabel ? <i className="fa fa-tags margin-right-13"/> : null}
		{props.tags.map(x =>
			<Link key={x.id} className="margin-right-13" to={`/boards/tags/${x.id}`}>{`@${x.typeName} #${x.name}`}</Link>)}

	</div> :
	null


PostTags.propTypes = {
	tags: React.PropTypes.array,
	showLabel: React.PropTypes.bool
}
PostTags.defaultProps = {
	tags: [],
	showLabel: true
}

export default PostTags
