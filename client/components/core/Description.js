'use strict'

import React from 'react'
import classnames from 'classnames'
import constants from '../../../shared/constants'
import linkifyHtml from 'linkifyjs/html'


/**
 * Check whether this si a description of the completed task.
 * @param {Object} props
 * @return {boolean}
 */
const isCompletedTask = props => props.post.typeId === constants.postTypes.TASK &&
		props.post.completion.status === constants.taskStatus.DONE &&
		!props.details

const getClassName = props => classnames('inner-text', {
	'comment-description': props.post.typeId === constants.postTypes.COMMENT
})

/**
 * Description component.
 * @param {object} props
 * @param {object} props.post
 * @param {string} props.post.description
 * @return {object}
 */
const Description = props => {
	if (props.post.description === '' || isCompletedTask(props)) return null

	/* eslint-disable */
	return <div className="text">
		<p className={getClassName(props)} dangerouslySetInnerHTML={{__html: linkifyHtml(props.post.description)}} />
	</div>
	/* eslint-enable */
}

Description.propTypes = {
	post: React.PropTypes.shape({
		description: React.PropTypes.string,
		typeId: React.PropTypes.string,
		completion: React.PropTypes.object
	}),
	details: React.PropTypes.bool
}
Description.defaultProps = {
	post: {
		description: '',
		typeId: constants.postTypes.NOTE,
		completion: {status: constants.taskStatus.OPEN}
	},
	details: false
}

export default Description
