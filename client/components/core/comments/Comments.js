'use strict'

import React from 'react'
import CommentExpander from './CommentExpander.js'
import Comment from './Comment.js'
import CommentEntry from './CommentEntry.js'
import {comments} from '../../../../shared/constants'

/**
 * Comments component.
 * @param {Object} props
 * @param {Array} props.comments
 * @param {int} props.commentCount
 * @param {func} props.showCommentsEntry
 * @param {boolean} props.commentEntry
 * @constructor
 */
class Comments extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			allComments: false
		}
	}

	showAllComments(e) {
		e.preventDefault()
		//TODO: md p3 request to get all comments
		this.setState({
			allComments: true
		})
	}

	get comments() {
		const allComments = this.props.comments
		const lastComments = allComments.
			slice(allComments.length - comments.DEFAULT_COMMENT_COUNT).
			map(comment => <Comment comment={comment} key={comment.id} />)

		return this.state.allComments ?
			allComments.map(comment => <Comment comment={comment} key={comment.id} />) :
			lastComments
	}

	render() {
		return <ul className="comments">
				<CommentExpander
					allComments={this.state.allComments}
					showAllComments={this.showAllComments.bind(this)}
					commentCount={this.props.commentCount}
				/>
				{this.comments}
				{
					this.props.commentEntry ?
						<CommentEntry
							postId={this.props.id}
							showCommentsEntry={this.props.showCommentsEntry}
						/> :
						null
				}
		</ul>
	}
}

Comments.propTypes = {
	id: React.PropTypes.number.isRequired,
	comments: React.PropTypes.array,
	commentCount: React.PropTypes.number,
	showCommentsEntry: React.PropTypes.func,
	commentEntry: React.PropTypes.bool
}

Comments.defaultProps = {
	id: 0,
	comments: [],
	commentCount: 0,
	showCommentsEntry: () => {
		// do nothing.
	},
	commentEntry: false
}

export default Comments
