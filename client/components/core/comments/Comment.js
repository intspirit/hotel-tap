'use strict'

import React from 'react'
import {browserHistory} from 'react-router'
import appController from '../../../core/appController'
import Attachments from '../attachments/Attachments'
import UserImage from '../UserImage'
import DateTime from '../DateTime'
import Description from '../Description'
import PostTags from '../PostTags'
import PostUsers from '../PostUsers'

/**
 * Comment component.
 * @param {object} props.comment
 */
class Comment extends React.Component {

	/**
	 * Comment component.
	 * @param {number} id
	 * @returns {*}
	 */
	redirectTo = (id) => {
		if (id && id === appController.user.id) {
			browserHistory.push(`/boards/me`)
		} else if (id) {
			browserHistory.push(`/boards/employee/${id}`)
		}
	}

	render() {
		return <li className="who clearfix">
			<div
				className="post-avatar-wrapper clickable"
				onClick={this.redirectTo.bind(this, this.props.comment.createdBy.id)}
			>
				<UserImage
					imageUrl={this.props.comment.createdBy && this.props.comment.createdBy.imageUrl}
					fullName={this.props.comment.createdBy && this.props.comment.createdBy.fullName}
				/>
			</div>
			<div className="comment-form">
				<span className="name" onClick={this.redirectTo.bind(this, this.props.comment.createdBy.id)}>
					<b>{`${this.props.comment.createdBy.fullName} `}</b>
				</span>
				<DateTime createdDateTime={this.props.comment.createdDateTime} />
				<PostTags tags={this.props.comment.tags}/>
				<PostUsers users={this.props.comment.ccUsers}/>
				<Description post={this.props.comment} />
			</div>
			<Attachments attachments={this.props.comment.attachments} />
		</li>
	}
}

Comment.propTypes = {
	comment: React.PropTypes.object
}

Comment.defaultProps = {
	comment: {}
}

export default Comment
