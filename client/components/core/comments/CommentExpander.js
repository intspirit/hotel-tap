'use strict'

import React from 'react'
import pluralize from 'pluralize'
import {comments} from '../../../../shared/constants'

/**
 * Comment expander component.
 * @param {Object} props
 * @param {func} props.showAllCommentsClicked
 * @param {int} props.commentCount
 * @param {boolean} props.allComments
 * @constructor
 */
const CommentsExpander = (props) => {
	const diff = props.commentCount - comments.DEFAULT_COMMENT_COUNT

	return !props.allComments && diff > 0 ?
		<a href="#" className="comments-expander" onClick={props.showAllComments}>
			View {diff} more {pluralize('comment', diff)}
		</a> :
		null
}

CommentsExpander.propTypes = {
	commentCount: React.PropTypes.number,
	showAllComments: React.PropTypes.func,
	allComments: React.PropTypes.bool
}

CommentsExpander.getDefaultProps = {
	commentCount: 0,
	showAllComments: () => {
		// do nothing.
	},
	allComments: false
}

export default CommentsExpander
