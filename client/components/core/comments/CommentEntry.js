'use strict'

import React from 'react'
import {Button} from 'react-bootstrap'
import appController from '../../../core/appController'
import Scroller from '../../../core/scroller'
import AttachmentEntry from '../attachments/AttachmentEntry'
import TagsSelector from '../tags/TagsSelector'
import TagsSelectorModal from '../tags/TagsSelectorModal'
import UsersSelector from '../users/UsersSelector'
import UsersSelectorModal from '../users/UsersSelectorModal'
import PostTags from '../PostTags'
import PostUsers from '../PostUsers'
import DescriptionEntry from '../DescriptionEntry'
import CommentEntryComponent from '../../../../shared/components/commentEntry'
import UserImage from '../UserImage'
import classnames from 'classnames'
import Log from 'loglevel'
import {GAEvents} from '../../../../shared/constants'
import {logEventForGA} from '../../../functions'


const mapTags = tags => tags.map(x => (
{
	id: x.id,
	name: x.tagName.replace('#', ''),
	typeId: x.groupId,
	typeName: x.groupName.replace('@', '')
}))

const mapUsers = users => users.map(x => (
{
	userId: x.id,
	fullName: x.name
}))

/**
 * Comment entry component.
 * @param {func} props.showCommentsEntry
 */
class CommentEntry extends React.Component {

	constructor(props) {
		super(props)

		this.commentEntryComponent = new CommentEntryComponent(appController, this)
		this.commentEntryComponent.setPostId(this.props.postId)

		this.state = Object.assign(this.commentEntryComponent.state, {
			selectedTags: this.commentEntryComponent.selectedTags,
			selectedUsers: this.commentEntryComponent.selectedUsers,
			errorMessage: '',
			loadingComment: false,
			isTagsModalVisible: false,
			isUsersModalVisible: false
		})
	}

	componentDidMount() {
		if (!this.node) return

		Scroller.scrollToComponentBottom(this.node, {
			delay: 100,
			bottomOffset: 200
		})
	}

	updateState(newState) {
		this.setState(newState)
	}

	showTagsModal() {
		this.setState({
			isTagsModalVisible: true
		})
	}

	hideTagsModal() {
		this.setState({
			isTagsModalVisible: false
		})
	}

	showUsersModal() {
		this.setState({
			isUsersModalVisible: true
		})
	}

	hideUsersModal() {
		this.setState({
			isUsersModalVisible: false
		})
	}

	resetErrorMessage() {
		this.setState({
			errorMessage: ''
		})
	}

	reset() {
		this.commentEntryComponent.reset()
		this.resetErrorMessage()
		this.setState({loadingComment: false})
	}

	async addComment() {
		const errorMessage = this.commentEntryComponent.validate()

		if (errorMessage) {
			this.setState({errorMessage})

			return
		}

		this.setState({loadingComment: true})
		const success = await this.insertPost()

		if (success) {
			this.reset()
			this.props.showCommentsEntry()
			appController.commentCreated(this.props.postId)
		} else {
			this.setState({
				errorMessage: 'Something wrong with adding comment',
				loadingComment: false
			})
		}
	}

	cancel() {
		this.props.showCommentsEntry()
	}

	onDescriptionChange(ev) {
		this.setState({
			errorMessage: ''
		})
		this.commentEntryComponent.descriptionChanged(ev.target.value)
	}

	onAttachmentsChanged(attachments) {
		this.setState({
			errorMessage: ''
		})
		this.commentEntryComponent.attachmentsChanged(attachments)
	}

	processSelectedTags(selectedTags) {
		this.commentEntryComponent.selectedTags = selectedTags
		this.setState({selectedTags})
	}

	processSelectedUsers(selectedUsers) {
		this.commentEntryComponent.selectedUsers = selectedUsers
		this.setState({selectedUsers})
	}

	async insertPost() {
		try {
			await this.commentEntryComponent.save()
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.COMMENT_ADDED)

			return true
		} catch (err) {
			Log.error(`CommentEntry|insertPost|error:${err}`)

			return false
		}
	}

	get errorMessage() {
		return this.state.errorMessage ?
			<small className="help-block">
				{this.state.errorMessage}
			</small> :
			null
	}

	render() {
		return 	<li ref={node => {
			this.node = node

			return node
		}}>
			<div className="post-avatar-wrapper">
				<UserImage fullName={appController.user.fullName} imageUrl={appController.user.imageUrl} />
			</div>
			<PostTags tags={mapTags(this.state.selectedTags)} />
			<PostUsers users={mapUsers(this.state.selectedUsers)} />
			<div className={classnames('comment-form', {
					'has-error': this.state.errorMessage
				})}>
				<div className="description-wrapper">
					<DescriptionEntry
						className="form-control description-entry"
						value={this.state.description}
						placeholder={'Write a comment...'}
						onChange={this.onDescriptionChange.bind(this)}
					/>
				</div>
				{this.errorMessage}
				<div className="comments-control">
					<AttachmentEntry onAttachmentsChanged={this.onAttachmentsChanged.bind(this)} />
					<TagsSelector onClick={this.showTagsModal.bind(this)} />
					<UsersSelector onClick={this.showUsersModal.bind(this)} />
					<Button
						onClick={this.addComment.bind(this)}
						bsStyle="primary"
						className="send pull-right"
						disabled={this.state.loadingComment}
					>
						Comment
					</Button>
					<Button onClick={this.cancel.bind(this)} bsStyle="default" className="pull-right margin-right-13">
						Cancel
					</Button>
				</div>
			</div>

			{this.state.isTagsModalVisible ?
				<TagsSelectorModal
					show={this.state.isTagsModalVisible}
					selectedTags={this.state.selectedTags}
					onProcessSelected={this.processSelectedTags.bind(this)}
					onHide={this.hideTagsModal.bind(this)}
				/> :
				null
			}

			{this.state.isUsersModalVisible ?
				<UsersSelectorModal
					show={this.state.isUsersModalVisible}
					selectedUsers={this.state.selectedUsers}
					onProcessSelected={this.processSelectedUsers.bind(this)}
					onHide={this.hideUsersModal.bind(this)}
				/> :
				null
			}

		</li>
	}
}

CommentEntry.propTypes = {
	postId: React.PropTypes.number,
	showCommentsEntry: React.PropTypes.func
}

CommentEntry.defaultProps = {
	postId: 0,
	showCommentsEntry: () => null
}

export default CommentEntry
