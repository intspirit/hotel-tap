'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'

/**
 * Tags selector component.
 *
 * @param {Object} props
 * @param {func}   props.onClick
 */
class TagsSelector extends React.Component {

	get tooltip() {
		return <Tooltip id="tag-tooltip"><b>Add tags</b></Tooltip>
	}

	render() {
		return <a className="tags-entry">
			<OverlayTrigger placement="bottom" overlay={this.tooltip}>
				<i onClick={this.props.onClick} className="fa fa-tags fa-fw fa-lg"/>
			</OverlayTrigger>
		</a>
	}
}

TagsSelector.propTypes = {
	onClick: React.PropTypes.func
}

TagsSelector.defaultProps = {
	onClick: () => null
}

export default TagsSelector
