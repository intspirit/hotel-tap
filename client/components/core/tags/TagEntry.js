'use strict'

import React from 'react'
import Log from 'loglevel'
import classnames from 'classnames'
import TagEntryComponent from '../../../../shared/components/tagEntry'
import appController from '../../../core/appController'

/**
 * Tag entry component.
 *
 * @param {Object} props
 * @param {Object} props.group
 * @param {Function} props.onHide
 */
class TagEntry extends React.Component {

	constructor(props) {
		super(props)

		this.tagEntryComponent = new TagEntryComponent(appController, this)
		this.tagEntryComponent.setGroup(this.props.group)

		this.state = Object.assign(this.tagEntryComponent.state, {
			hasError: false,
			errorMessage: ''
		})
	}

	componentDidMount() {
		this.input.focus()
	}

	async onSubmit(e) {
		e.preventDefault()

		this.resetError()
		const errorMessage = this.tagEntryComponent.validate()

		if (errorMessage) {
			this.setError(errorMessage)

			return
		}

		try {
			const serviceResponse = await this.tagEntryComponent.save()
			if (serviceResponse.inserted) {
				this.props.onHide()
				const tag = {
					id: serviceResponse.id,
					name: this.state.name
				}
				appController.tagCreated(tag, this.props.group)
			}
		} catch (err) {
			Log.error(`TagEntry|tagSubmitted|error:${err}`)
		}
	}

	onCancel(e) {
		e.preventDefault()
		this.props.onHide()
	}

	updateState(newState) {
		this.setState(newState)
	}

	setError(errorMessage) {
		this.setState({
			hasError: errorMessage.length > 0,
			errorMessage: errorMessage || ''
		})
	}

	resetError() {
		this.setError('')
	}

	render() {
		return <div className="list-tags-item row">
			<label className={classnames('input col col-9', {
				'has-error': this.state.hasError
			})}>
				<input
					type="text"
					ref={node => {
						this.input = node

						return node
					}}
					className="input-xs"
					placeholder="New tag"
					value={this.state.name}
					onChange={ev => this.tagEntryComponent.nameChanged(ev.target.value) }
				/>
				<small className="help-block">{this.state.errorMessage}</small>
			</label>
			<span className="col col-3 no-padding">
				<a href="" onClick={this.onSubmit.bind(this)}>
					<i className="fa fa-2x fa-check" style={{color: 'green'}} />
				</a>
				<a href="" onClick={this.onCancel.bind(this)}>
					<i className="fa fa-2x fa-close" style={{color: 'grey'}} />
				</a>
			</span>
		</div>
	}
}

TagEntry.propTypes = {
	group: React.PropTypes.object,
	onHide: React.PropTypes.func
}

TagEntry.defaultProps = {
	onHide: () => null
}

export default TagEntry
