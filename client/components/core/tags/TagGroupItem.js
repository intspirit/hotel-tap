'use strict'

import React from 'react'
import classnames from 'classnames'
import {ListGroup, ListGroupItem} from 'react-bootstrap'
import TagEntry from './TagEntry'

/**
 * Group title component.
 *
 * @constructor
 * @param {Object} props
 * @param {String} props.title
 * @param {Function} props.onAddClick
 */
const GroupTitle = props =>
	<header className="list-tags-group">
		<a href="" onClick={e => {
			e.preventDefault()
			props.onAddClick()
		}}>
			<i className="fa fa-plus-circle pull-right group-icon" />
		</a>
		<div className="group-name">
			{props.title}
		</div>
	</header>

GroupTitle.propTypes = {
	title: React.PropTypes.string,
	onAddClick: React.PropTypes.func
}

GroupTitle.defaultProps = {
	onAddClick: () => null
}

/**
 * Tag group item list component.
 *
 * @param {Object} props
 * @param {Array} props.tags
 * @param {Object} props.group
 * @param {Object} props.tagsIndexed
 * @param {Function} props.onTagSelected
 */
class GroupList extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			isTagEntryVisible: false
		}
	}

	showTagEntry() {
		this.setState({
			isTagEntryVisible: true
		})
	}

	hideTagEntry() {
		this.setState({
			isTagEntryVisible: false
		})
	}

	render() {
		return <fieldset className="list-tags-items">
			<section>
				{(this.props.tags || []).map(tag =>
					<div className="list-tags-item" key={tag.uId}>
						<label className={classnames('checkbox', {
							'state-disabled': this.props.tagsIndexed[tag.uId].disabled
						})}>
							<input
								type="checkbox"
								disabled={this.props.tagsIndexed[tag.uId].disabled}
								checked={this.props.tagsIndexed[tag.uId].selected}
								onChange={ev => this.props.onTagSelected(this.props.group.uId, tag.uId, ev.target.checked)}
							/>
							<i />
							{tag.name}
						</label>
					</div>
				)}
				{this.state.isTagEntryVisible ?
					<TagEntry
						group={this.props.group}
						onHide={this.hideTagEntry.bind(this)}
					/> :
					null
				}
			</section>
		</fieldset>
	}
}

GroupList.propTypes = {
	group: React.PropTypes.object,
	tags: React.PropTypes.array,
	tagsIndexed: React.PropTypes.object,
	onTagSelected: React.PropTypes.func
}

GroupList.defaultProps = {
	onTagSelected: () => null
}

/**
 * Tag group item list component.
 *
 * @constructor
 * @param {Object} props
 * @param {Array} props.tags
 * @param {Object} props.group
 * @param {Object} props.tagsIndexed
 * @param {Function} props.onTagSelected
 */
class TagGroupItem extends React.Component {

	onAddClick() {
		this.groupList.showTagEntry()
	}

	render() {
		return this.props.tags && this.props.tags.length > 0 ?
			<div className="fi col col-xs-6 col-sm-6 col-md-4">
				<ListGroup className="list-tags-wrapper">
					<ListGroupItem className="list-tags">
						<GroupTitle
							title={this.props.group.name}
							onAddClick={this.onAddClick.bind(this)}
						/>
						<GroupList
							ref={node => {
								this.groupList = node

								return node
							}}
							group={this.props.group}
							tags={this.props.tags}
							tagsIndexed={this.props.tagsIndexed}
							onTagSelected={this.props.onTagSelected}
						/>
					</ListGroupItem>
				</ListGroup>
			</div> :
			null
	}
}

TagGroupItem.propTypes = {
	tags: React.PropTypes.array,
	tagsIndexed: React.PropTypes.object,
	group: React.PropTypes.object,
	onTagSelected: React.PropTypes.func
}

TagGroupItem.defaultProps = {
	onTagSelected: () => null
}

export default TagGroupItem
