'use strict'

import React from 'react'

class TagGroup extends React.Component {
	render() {
		return (
			<div className="row scrollable">
				<div className="fc col col-xs-12 col-sm-12 col-md-12">
					{this.props.children}
				</div>
			</div>
		)
	}
}

export default TagGroup
