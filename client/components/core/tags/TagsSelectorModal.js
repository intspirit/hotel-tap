'use strict'

import React from 'react'
import update from 'react-addons-update'
import Log from 'loglevel'
import _ from 'lodash'
import classnames from 'classnames'
import {Tabs, Tab, Button, Grid, Row, Col} from 'react-bootstrap'
import appController from '../../../core/appController'
import {tagsMapper, utils} from '../../../../shared/core'
import constants from '../../../../shared/constants'
import TagGroup from './TagGroup'
import TagGroupItem from './TagGroupItem'
import Modal from '../Modal'
import Loader from '../loader/Loader'

/**
 * Post entry component.
 *
 * @constructor
 * @param {Object} props
 * @param {String} props.title
 * @param {Array}  props.tags
 */
const TabTitle = props =>
	<span>
		{props.title}
		{' '}
		{props.tags && props.tags.length > 0 ?
			<span className="txt-color-blue">
				{props.tags.map(x => <span key={x.tagId} className="margin-right-13">{`${x.groupName} ${x.tagName}`}</span>)}
			</span> :
			null
		}
	</span>

TabTitle.propTypes = {
	title: React.PropTypes.string,
	tags: React.PropTypes.array
}

class TagsSelectorModal extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			key: constants.tagLegacyType.DEPARTMENT
		}

		this.selectedTagsIndexed = _.keyBy(props.selectedTags, 'tagId')
		this.departmentsIndexed = {}
		this.areasIndexed = {}
		this.equipmentGroupsIndexed = {}
		this.selectedGroup = {}
		this.groupedTags = {}

		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleSelect = this.handleSelect.bind(this)
		this.hide = this.hide.bind(this)

		appController.tagsSelectorComponent = this
	}

	async build() {
		try {
			let {
				departmentsIndexed,
				groupedTags,
				tagsIndexed,
				areasIndexed,
				equipmentGroupsIndexed
			} = await tagsMapper.map(this.props.selectedTags)

			this.groupedTags = groupedTags
			this.departmentsIndexed = departmentsIndexed
			this.areasIndexed = areasIndexed
			this.equipmentGroupsIndexed = equipmentGroupsIndexed

			_.assign(this.state, {tagsIndexed})
		} catch (err) {
			Log.error(`TagsSelectorModal|build|error:${err}`)
		}
	}

	async componentDidMount() {
		this.setState({
			isLoading: true
		})

		await this.build()

		this.setState({
			tagsIndexed: this.state.tagsIndexed,
			isLoading: false
		})
	}

	tagSelected(group, tagId, value) {
		this.selectedTagsIndexed[tagId] = {
			id: this.state.tagsIndexed[tagId].id,
			type: this.state.tagsIndexed[tagId].type,
			groupId: group.uId,
			groupName: `@${group.name}`,
			tagId: tagId,
			tagName: `#${this.state.tagsIndexed[tagId].name}`,
			selected: value
		}
		const newTagsIndexed = update(this.state.tagsIndexed, {[tagId]: {selected: {$set: value}}})
		this.setState({tagsIndexed: newTagsIndexed})
	}

	tagAdded(tag, group) {
		//TODO: ak p1 take care about rooms tags
		//TODO: ak p1 take care about equipments tags

		const tagUid = tagsMapper.getUuid(group.type, tag.id)
		const groupUid = tagsMapper.getUuid(group.type, group.id)
		const tagIndexed = {
			id: tag.id,
			name: tag.name,
			type: group.type,
			typeId: group.id,
			typeName: group.name,
			uId: tagUid,
			selected: false,
			disabled: false
		}
		const newTagsIndexed = update(this.state.tagsIndexed, {$merge: {[tagUid]: tagIndexed}})
		const newGroupedTags = update(this.groupedTags, {[groupUid]: {$push: [tagIndexed]}})

		this.groupedTags = newGroupedTags
		this.setState({tagsIndexed: newTagsIndexed})
	}

	hide() {
		this.props.onHide()
	}

	handleSubmit() {
		this.props.onProcessSelected(this.selectedTags)
		this.hide()
	}

	handleSelect(key) {
		this.setState({key})
	}

	calculateTitle(title, selectedTags) {
		return selectedTags.length > 0 ?
			`${title} ${selectedTags.map(x => `${x.groupName} ${x.tagName}`).join(', ')}` :
			title
	}

	get selectedTags() {
		return Object.values(this.selectedTagsIndexed).filter(x => x.selected)
	}

	get activeKey() {
		if (this.props.departmentsTabDisabled && this.state.key === constants.tagLegacyType.DEPARTMENT)
			return constants.tagLegacyType.AREA

		return this.state.key
	}

	get selectedDepartmentTags() {
		return this.selectedTags.filter(x =>
			x.type === constants.tagLegacyType.DEPARTMENT ||
			x.type === constants.tagLegacyType.MAINTENANCE
		)
	}

	get selectedLocationTags() {
		return this.selectedTags.filter(x => x.type === constants.tagLegacyType.AREA)
	}

	get selectedEquipmentTags() {
		return this.selectedTags.filter(x => x.type === constants.tagLegacyType.EQUIPMENT)
	}

	get isDepartmentTagRequired() {
		return utils.isDepartmentBoard(this.props.board) && this.selectedDepartmentTags.length === 0
	}

	get isLocationTagRequired() {
		return utils.isDepartmentBoard(this.props.board) && this.selectedLocationTags.length === 0
	}

	get memo() {
		return utils.isDepartmentBoard(this.props.board) ?
			<div className="row">
				<div className="col col-sm-12">
					<p>Please select a Department tag and a Location tag</p>
				</div>
			</div> :
			null
	}

	get body() {
		const renderTagGroup = (groups) =>
			<TagGroup>
				{Object.values(groups).map(group =>
					<TagGroupItem
						key={group.uId}
						group={group}
						tags={this.groupedTags[group.uId]}
						tagsIndexed={this.state.tagsIndexed}
						onTagSelected={ (groupId, tagId, value) => this.tagSelected(group, tagId, value) }
					/>
				)}
			</TagGroup>
		const renderTabTitle = (title, tags) => <TabTitle title={title} tags={tags} />

		return this.state.isLoading ?
			<Loader visible={this.state.isLoading} /> :
			<div>
				{this.memo}
				<form className="smart-form tags-selector">
					<Tabs activeKey={this.activeKey} onSelect={this.handleSelect} id="tags-tabs-list">
						<Tab
							title={renderTabTitle('Department', this.selectedDepartmentTags)}
							eventKey={constants.tagLegacyType.DEPARTMENT}
							disabled={this.props.departmentsTabDisabled}
							tabClassName={classnames({
								'txt-color-red': this.isDepartmentTagRequired
							})}
						>
							{renderTagGroup(this.departmentsIndexed)}
						</Tab>

						<Tab
							title={renderTabTitle('Location', this.selectedLocationTags)}
							eventKey={constants.tagLegacyType.AREA}
							tabClassName={classnames({
								'txt-color-red': this.isLocationTagRequired
							})}
						>
							{renderTagGroup(this.areasIndexed)}
						</Tab>

						<Tab
							title={renderTabTitle('Equipment', this.selectedEquipmentTags)}
							eventKey={constants.tagLegacyType.EQUIPMENT}
						>
							{renderTagGroup(this.equipmentGroupsIndexed)}
						</Tab>
					</Tabs>
				</form>
			</div>
	}

	get footer() {
		return <Grid fluid={true}>
			<Row>
				<Col>
					<Button onClick={this.hide}>Cancel</Button>
					<Button onClick={this.handleSubmit} bsStyle="primary">Save</Button>
				</Col>
			</Row>
		</Grid>
	}

	render() {
		return <Modal
			bsSize="large"
			show={this.props.show}
			onHide={this.hide}
			title="Tags"
			body={this.body}
			footer={this.footer} />
	}
}

TagsSelectorModal.propTypes = {
	board: React.PropTypes.string,
	departmentsTabDisabled: React.PropTypes.bool,
	show: React.PropTypes.bool,
	onHide: React.PropTypes.func,
	selectedTags: React.PropTypes.array,
	onProcessSelected: React.PropTypes.func
}

TagsSelectorModal.defaultProps = {
	board: '',
	departmentsTabDisabled: false,
	show: false,
	onHide: () => null,
	selectedTags: [],
	onProcessSelected: () => null
}

export default TagsSelectorModal
