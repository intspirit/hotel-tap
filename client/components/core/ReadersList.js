import React, {Component} from 'react'

/**
 * Readers list component.
 * @param {Object} props
 * @param {Array} props.readers
 * @param {func} props.showAllReaders
 * @constructor
 */
class ReadersList extends Component {
	componentDidMount() {
		setTimeout(() => {
			this.props.showAllReaders()
		}, 2000)
	}

	render() {
		return (
			<div className={'readers-list-wrapper'}>
				{this.props.readers.length ?
					<div className="readers-list">
						<ul>
							{this.props.readers.map((reader, key) =>
								reader && <li key={key}>{reader}</li>
							)}
						</ul>
					</div> :
					null
				}
			</div>
		)
	}
}

ReadersList.propTypes = {
	readers: React.PropTypes.array,
	showAllReaders: React.PropTypes.func
}

ReadersList.defaultProps = {
	readers: [],
	showAllReaders: () => {
		// do nothing.
	}
}

export default ReadersList
