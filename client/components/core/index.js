'use strict'

import NameDescription from './NameDescription'
import DescriptionEntry from './DescriptionEntry'
import SubjectEntry from './SubjectEntry'
import Assignment from './Assignment'
import ValidationError from './ValidationError'
import Loader from './loader/Loader'

module.exports = {
	NameDescription: NameDescription,
	Assignment: Assignment,
	DescriptionEntry: DescriptionEntry,
	SubjectEntry: SubjectEntry,
	ValidationError: ValidationError,
	Loader: Loader
}
