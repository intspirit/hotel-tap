'use strict'

import React from 'react'
import DatePicker from 'react-datepicker'
import moment from 'moment'
// import moment from 'moment-timezone'
// import dateTimeHelper from '../../../shared/core/dateTimeHelper'

import 'react-datepicker/dist/react-datepicker.css'

/**
 * User image component.
 * @param {Object} props
 * @constructor
 */
class DatePicker2 extends React.Component {

	onChange(date) {
		const preparedDate = moment.utc(date.format('YYYY-MM-DD HH:mm:ss')).endOf('day')

		this.props.onChange(preparedDate)
	}

	render() {
		return <DatePicker {...this.props} onChange={this.onChange.bind(this)} />
	}
}

DatePicker2.propTypes = {
	onChange: React.PropTypes.func
}

export default DatePicker2
