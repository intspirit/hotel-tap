'use strict'

import React, {Component, PropTypes} from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import classnames from 'classnames'
import constants from '../../../shared/constants'
import {utils} from '../../../shared/core'


//TODO: vvs p2 REFACTOR - split into small stateless components.
/**
 * Assignment component - get assignment to employee or department.
 */
class Assignment extends Component {
	static propTypes = {
		assignmentType: PropTypes.string,
		assignmentId: PropTypes.number,
		departments: PropTypes.array,
		employees: PropTypes.array,
		disabled: PropTypes.bool,
		assignmentChanged: PropTypes.func
	}

	static defaultProps = {
		assignmentType: constants.assignmentTypes.DEPARTMENT,
		departments: [],
		employees: [],
		assignmentId: -1,
		disabled: false,
		assignmentChanged: () => null
	}

	constructor(props) {
		super(props)
		this.state = {
			employeeId: props.assignmentType === constants.assignmentTypes.EMPLOYEE ? props.assignmentId : -1,
			departmentId: props.assignmentType === constants.assignmentTypes.DEPARTMENT ? props.assignmentId : -1
		}
	}

	onEmployeeChange(e) {
		this.props.assignmentChanged(constants.assignmentTypes.EMPLOYEE, e.target.value)
		this.setState({
			employeeId: e.target.value,
			departmentId: -1
		})
	}

	onDepartmentChange(department, e) {
		e.preventDefault()
		if (this.props.disabled) return
		this.props.assignmentChanged(constants.assignmentTypes.DEPARTMENT, department.id)
		this.setState({
			employeeId: '-1',
			departmentId: department.id
		})
	}

	render() {
		const employeeOptions = this.props.employees.map(x =>
			<option value={x.id} key={x.id}>{utils.getFullName(x)}</option>)

		const tooltip = text => <Tooltip id="department-picker-tooltip">{text}</Tooltip>
		const getClassName = x => classnames('department-picker-item', {selected: x.id === this.state.departmentId})

		const departmentOptions = <ul className="list-inline department-picker">
			{this.props.departments.map(x =>
				<OverlayTrigger key={x.id} placement="top" overlay={tooltip(x.name)}>
					<li className={getClassName(x)}>
						<a href="" onClick={this.onDepartmentChange.bind(this, x)}>
							<i className="fa fa-lg fa-fw custom-icon">
								<div style={{backgroundImage: `url(${x.iconFullUrl})`}}/>
							</i>
						</a>
					</li>
				</OverlayTrigger>
			)}
		</ul>


		return <fieldset>
				<div className="row">
					<section className="col col-4">
						<label className="select">
							<select value={this.state.employeeId}
								disabled={this.props.disabled ? 'disabled' : ''}
								onChange={this.onEmployeeChange.bind(this)}>
								<option value="-1" key="-1">Employee Name</option>
								{employeeOptions}
							</select>
							<i/>
						</label>

					</section>
					<section className="col col-8">
						{departmentOptions}
					</section>
				</div>
			</fieldset>
	}
}

export default Assignment
