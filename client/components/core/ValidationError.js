'use strict'

import React from 'react'
import {Alert} from 'react-bootstrap'

/**
 * Validation error component - shows validation errors.
 * @param {object} props
 * @param {string[]} props.errorMessages
 * @constructor
 */
const ValidationError = props => props.errorMessages.length > 0 ? //eslint-disable-line no-confusing-arrow
	<Alert bsStyle="danger">
		<ul style={{listStyleType: 'none'}}>
			{props.errorMessages.map((x, i) => <li key={i}>{x}</li>)}
		</ul>
	</Alert> :
	null

ValidationError.propTypes = {
	errorMessages: React.PropTypes.arrayOf(React.PropTypes.string)
}
ValidationError.defaultProps = {
	errorMessages: []
}

export default ValidationError
