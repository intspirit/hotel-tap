'use strict'

import React from 'react'
import './loader.css'

/**
 * LoaderFileUpload component.
 * @param {Object} props
 * @param {Boolean} props.visible
 * @constructor
 */
const LoaderFileUpload = props => props.visible ? // eslint-disable-line no-confusing-arrow
	<div className="loader-wrapper">
		<div className="loader" />
	</div> :
	null

LoaderFileUpload.propTypes = {
	visible: React.PropTypes.bool
}

LoaderFileUpload.defaultProps = {
	visible: false
}

export default LoaderFileUpload
