'use strict'

import React from 'react'

/**
 * Loader component.
 * @param {Object} props
 * @param {Boolean} props.visible
 * @constructor
 */
const Loader = props => props.visible ? // eslint-disable-line no-confusing-arrow
	<div className="col-1">
		<i className="fa fa-spinner fa-spin"/> Loading ...
	</div> :
	null

Loader.propTypes = {
	visible: React.PropTypes.bool
}

Loader.defaultProps = {
	visible: false
}

export default Loader
