'use strict'

import React from 'react'
import {Button, DropdownButton, Form, FormGroup, FormControl, InputGroup, MenuItem, Row} from 'react-bootstrap'
import moment from 'moment-timezone'
import _ from 'lodash'

import {complaintResolutionNames, complaintResolutions} from '../../../shared/constants'
import appController from '../../core/appController'
import {utils} from '../../../shared/core'

const getComplaintResolutionKeyFromAbbreviation = utils.getComplaintResolutionKeyFromAbbreviation
const UNRESOLVED = 'UNRESOLVED'

/**
 * Post guest complaint.
 *
 * @param {Object} props
 * @param {bool}   props.complaint
 * @param {Object} props.complaintResolution
 * @constructor
 */
class PostComplaint extends React.Component {
	constructor(props) {
		super(props)

		this.state = Object.assign({isEditing: false}, this.getStateFromProps(this.props))
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.getStateFromProps(nextProps))
	}

	getStateFromProps(props) {
		return {
			isPanelVisible: this.state && this.state.isEditing || Boolean(props.complaintResolution.resolved)
		}
	}

	getResolutionKey() {
		return this.props.complaintResolution.resolution ?
			getComplaintResolutionKeyFromAbbreviation(this.props.complaintResolution.resolution) :
			UNRESOLVED
	}

	onCostChange (e) {
		this.onChange({cost: parseInt(e.target.value, 10)})
	}

	onPointsGivenChange (e) {
		this.onChange({pointsGiven: parseInt(e.target.value, 10)})
	}

	onResolutionTypeSelect (resolutionKey) {
		this.onChange({resolution: complaintResolutions[resolutionKey]})
	}

	onChange(resolutionChanges) {
		this.props.onChange(resolutionChanges)
	}

	cancelForm() {
		this.setState({isEditing: false}, this.props.cancel)
	}

	showForm() {
		this.setState({isEditing: true, isPanelVisible: true})
	}

	saveComplaintResolution () {
		this.setState({
			isEditing: false,
			isPanelVisible: this.getResolutionKey() !== UNRESOLVED
		}, this.props.resolve)
	}

	renderResolveButton() {
		const isResolved = Boolean(this.props.complaintResolution.resolved)

		return !this.state.isEditing && (!isResolved || appController.user.isAdmin) ?
			<Button
				bsStyle={isResolved ? 'success' : 'danger'}
				className="pull-right"
				onClick={this.showForm.bind(this)}>
					{isResolved ? 'Resolved' : 'Unresolved'}
			</Button> :
			null
	}

	renderComplaintResolution() {
		let resolutionTimeString = this.props.complaintResolution.duration ?
			moment.duration(this.props.complaintResolution.duration).humanize() :
			null

		return (
			<Row>
				<div className="col-xs-12 col-sm-3 col-md-3 col-lg-2">
					Cost: ${this.props.complaintResolution.cost}
				</div>
				<div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					Points given: {this.props.complaintResolution.pointsGiven}
				</div>
				<div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					{complaintResolutionNames[this.getResolutionKey()]}
				</div>
				<div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					Resolution time: {resolutionTimeString}
				</div>
			</Row>
		)
	}

	renderEditForm() {
		return (
			<Form>
				<Row>
					<FormGroup className="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<InputGroup>
							<InputGroup.Addon><i className="fa fa-dollar"/></InputGroup.Addon>
							<FormControl
								type="text"
								value={this.props.complaintResolution.cost || ''}
								placeholder="Cost"
								onChange={this.onCostChange.bind(this)}/>
						</InputGroup>
					</FormGroup>
					<FormGroup className="col-xs-6 col-sm-3 col-md-3 col-lg-3">
						<FormControl
							type="text"
							value={this.props.complaintResolution.pointsGiven || ''}
							placeholder="Points given"
							onChange={this.onPointsGivenChange.bind(this)}/>
					</FormGroup>
					<FormGroup className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<DropdownButton
							title={complaintResolutionNames[this.getResolutionKey()]}
							id="resolution_type_dropdown"
							onSelect={this.onResolutionTypeSelect.bind(this)}>
							{_.map(complaintResolutionNames, (name, key) =>
								<MenuItem eventKey={key} key={key}>{name}</MenuItem>
							)}
						</DropdownButton>
					</FormGroup>
					<FormGroup className="col-xs-12 col-sm-3 col-md-4 col-lg-3">
						<Button type="button" onClick={this.cancelForm.bind(this)}>Cancel</Button>
						{' '}
						<Button
							type="button"
							onClick={this.saveComplaintResolution.bind(this)}
							bsStyle="primary">
							Save
						</Button>
					</FormGroup>
				</Row>
			</Form>
		)
	}

	render() {
		return this.props.complaint ?
			<div>
				<div className="text">
					<i className="fa fa-warning txt-color-red margin-right-13"/>
					<span className="margin-right-13 txt-color-red">Guest Complaint</span>
					{this.renderResolveButton()}
				</div>
				<div className={`text ${this.state.isPanelVisible ? 'show' : 'hidden'}`}>
					{this.state.isEditing ? this.renderEditForm() : this.renderComplaintResolution()}
				</div>
			</div> :
			null
	}
}

PostComplaint.propTypes = {
	complaintResolution: React.PropTypes.object,
	complaint: React.PropTypes.bool,
	resolve: React.PropTypes.func,
	cancel: React.PropTypes.func,
	onChange: React.PropTypes.func
}
PostComplaint.defaultProps = {
	complaint: false,
	complaintResolution: {
		cost: '',
		pointsGiven: '',
		resolution: complaintResolutions[UNRESOLVED],
		resolved: false
	},
	resolve: () => null,
	cancel: () => null,
	onChange: () => null
}

export default PostComplaint

