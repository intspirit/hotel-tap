'use strict'

import React from 'react'

/**
 * Name and description component.
 */
class NameDescription extends React.Component {

	static propTypes = {
		name: React.PropTypes.string,
		description: React.PropTypes.string,
		disabled: React.PropTypes.bool,
		nameHint: React.PropTypes.string,
		onChange: React.PropTypes.func
	}

	static defaultProps = {
		name: '',
		nameHint: '',
		description: '',
		disabled: false,
		onChange: () => null
	}

	constructor(props) {
		super(props)
		this.oldName = props.name
		this.oldDescription = props.description
		this.state = {
			name: props.name,
			description: props.description
		}
	}

	onNameChange(event) {
		this.setState({name: event.target.value})
	}

	onDescriptionChange(event) {
		this.setState({description: event.target.value})
	}

	onChangeCompleted() {
		if (this.props.onChange &&
			(this.oldDescription !== this.state.description || this.oldName !== this.state.name))
			this.props.onChange(this.state.name.trim(), this.state.description.trim())
	}

	render() {
		return <div>
			<section>
				<label className="input">
					<input
						type="text"
						className=""
						placeholder={this.props.nameHint}
						onChange={this.onNameChange.bind(this)}
						onBlur={this.onChangeCompleted.bind(this)}
						value={this.state.name}
						autoFocus={true}
						disabled={this.props.disabled}/>
				</label>

			</section>
			<section>
				<label className="textarea">
					<textarea
						rows="4"
						placeholder="Description"
						onBlur={this.onChangeCompleted.bind(this)}
						onChange={this.onDescriptionChange.bind(this)}
						className="custom-scroll"
						value={this.state.description}
						disabled={this.props.disabled}/>
				</label>
			</section>
		</div>
	}

}

export default NameDescription
