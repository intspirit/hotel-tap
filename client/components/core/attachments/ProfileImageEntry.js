'use strict'

//TODO: vvs p2 REFACTOR - AttachmentEntry / ProfileImageEntry - single component.

import React from 'react'
import {Button} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import S3Upload from 'react-s3-uploader/s3upload'
import _ from 'lodash'
import Log from 'loglevel'
import Loader from '../loader/LoaderFileUpload'
import UserImage from '../../core/UserImage'
import {userImageDimensions, defaultImageFileName} from '../../../../shared/constants'

const AttachmentProgress = props => props.inProgress &&
	<div className="in-progress-wrapper">
		<div className="progress-overlay"/>
		<div className="progress-value">{props.value}%</div>
		<div className="progress-loader-wrapper">
			<Loader visible={true} />
		</div>
	</div>

AttachmentProgress.propTypes = {
	inProgress: React.PropTypes.bool,
	value: React.PropTypes.number
}

AttachmentProgress.defaultProps = {
	inProgress: false,
	value: 0
}


/**
 * Profile image entry component.
 *
 * @param {func} props.onAttachmentsChanged
 * @param {object} props.profile
 * @param {string} props.profile.fullName
 * @param {string} props.profile.imageUrl
 */

class ProfileImageEntry extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			profileImage: {preview: props.profile.imageUrl}
		}

		this.onOpenClick = this.onOpenClick.bind(this)
		this.onDrop = this.onDrop.bind(this)
		this.onRemoveClick = this.onRemoveClick.bind(this)
	}

	onProgress(file, progress) {
		let profileImage = this.state.profileImage
		profileImage.progress = progress
		this.setState({profileImage})
	}

	onError(file, err) {
		this.reset()
		Log.error(`ProfileImageEntry|onError|error:${err}`)
	}

	onFinish(file, info) {
		let profileImage = this.state.profileImage
		profileImage.inProgress = false
		profileImage.tmpFilePath = info.filename

		this.setState({profileImage})
		this.props.onAttachmentsChanged(profileImage.tmpFilePath)
	}

	onDrop(files) {
		files.map(file => {
			file.id = _.uniqueId()
			file.inProgress = true
			file.progress = 0

			return file
		})

		files.forEach(file => {
			const S3Uploader = new S3Upload({
				files: [file],
				signingUrl: '/s3/sign',
				signingUrlQueryParams: {},
				onProgress: this.onProgress.bind(this, file),
				onFinishS3Put: this.onFinish.bind(this, file),
				onError: this.onError.bind(this, file),
				uploadRequestHeaders: {'x-amz-acl': 'public-read'},
				contentDisposition: file.type,
				server: ''
			})

			return S3Uploader
		})

		const profileImage = files[0]
		this.setState({profileImage})
	}

	onOpenClick() {
		this.refs.avatar.open()
	}

	onRemoveClick(event) {
		event.preventDefault()
		this.removeAvatar()
		this.props.onAttachmentsChanged(null)

		return false
	}

	removeAvatar() {
		this.setState({
			profileImage: {preview: null}
		})
	}

	reset() {
		this.setState({
			profileImage: {preview: this.props.profile.imageUrl}
		})
	}

	get isProfileImageExists() {
		const defaultImageName = `/profile/${defaultImageFileName.EMPLOYEE}`
		const currentImageName = this.state.profileImage.preview || ''

		return !currentImageName.endsWith(defaultImageName) &&
			this.state.profileImage.preview !== null
	}

	render() {
		if (!this.props.profile.fullName) return null

		const avatar = this.state.profileImage

		return <div className="attachments-entry-wrapper">
			<div className="attachments-preview">
				<div className="attachment-preview" key="profile-image">
					<div className="attachment">
						<UserImage
							fullName={this.props.profile.fullName}
							imageUrl={avatar.preview}
							dimension={userImageDimensions.LARGE}
						/>
					</div>
					<AttachmentProgress
						inProgress={avatar.inProgress}
						value={avatar.progress}
					/>
					{this.isProfileImageExists &&
						<div className="attachment-actions">
						<span
							className="remove-attachment"
							onClick={this.onRemoveClick}>Remove
						</span>
						</div>
					}
				</div>
			</div>
			<a className="attachments-entry">
				<Dropzone
					ref="avatar"
					onDrop={this.onDrop}
					multiple={false}
					className="hidden"
				/>
				<Button
					onClick={this.onOpenClick}
					bsStyle="default"
					bsSize="xsmall"
					className="pull-left"
				>
					Upload Image
				</Button>
			</a>
		</div>
	}
}

ProfileImageEntry.propTypes = {
	profile: React.PropTypes.object,
	onAttachmentsChanged: React.PropTypes.func
}

ProfileImageEntry.defaultProps = {
	profile: {},
	onAttachmentsChanged: () => {
		// do nothing.
	}
}

export default ProfileImageEntry
