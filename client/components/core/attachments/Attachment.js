'use strict'

import React from 'react'
import {attachmentTypes} from '../../../../shared/constants'

/*eslint-disable no-confusing-arrow */
//noinspection Eslint
/**
 * @param {Object} props
 * @param {Object} props.attachment
 * @constructor
 */
const Attachment = (props) => (props.attachment.type || '').includes(attachmentTypes.IMAGE) ?
	<div className="image">
		<a href={props.attachment.url} target="_blank">
			<img src={props.attachment.url} alt="img"/>
		</a>
	</div> :
	<div className="file">
		<span>
			<i className="fa fa-file-o"/>
			<a href={props.attachment.url} target="_blank">{props.attachment.name}</a>
		</span>
	</div>

/*eslint-enable no-confusing-arrow */


Attachment.propTypes = {
	attachment: React.PropTypes.object
}

Attachment.defaultProps = {
	attachment: {}
}

export default Attachment
