'use strict'

import React from 'react'
import Attachment from './Attachment'
import {attachmentTypes} from '../../../../shared/constants'

/**
 * Attachments component for non-image files.
 * @param {Object} props
 * @param {Array} props.files
 * @constructor
 */
const AttachmentsFiles = (props) => props.files && props.files.length !== 0 &&
	<div className="files">
		{props.files.map(file =>
			<Attachment attachment={file} key={file.id} />
		)}
		<div className="clearfix" />
	</div>

AttachmentsFiles.propTypes = {
	files: React.PropTypes.array
}

AttachmentsFiles.defaultProps = {
	files: []
}

/**
 * Attachments component for images.
 * @param {Object} props
 * @param {Array} props.images
 * @constructor
 */
const AttachmentsImages = (props) => props.images && props.images.length !== 0 &&
	<div className="files">
		{props.images.map(image =>
			<Attachment attachment={image} key={image.id} />
		)}
		<div className="clearfix" />
	</div>

AttachmentsImages.propTypes = {
	images: React.PropTypes.array
}

AttachmentsImages.defaultProps = {
	images: []
}


/**
 * Attachments component.
 * @param {Object} props
 * @param {Array} props.attachments
 * @constructor
 */
class Attachments extends React.Component {
	constructor(props) {
		super(props)

		let model = {
			images: [],
			files: []
		}

		this.props.attachments.forEach(attachment => {
			if (this.isImage(attachment)) {
				model.images.push(attachment)

				return
			}

			model.files.push(attachment)
		})

		this.state = {
			model: model
		}
	}

	isImage(attachment) {
		return (attachment.type || '').includes(attachmentTypes.IMAGE)
	}

	render() {
		return <div className="attachments">
			<AttachmentsFiles files={this.state.model.files} />
			<AttachmentsImages images={this.state.model.images} />
		</div>
	}
}

Attachments.propTypes = {
	attachments: React.PropTypes.array
}

Attachments.defaultProps = {
	attachments: []
}

export default Attachments
