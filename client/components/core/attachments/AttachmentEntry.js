'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import S3Upload from 'react-s3-uploader/s3upload'
import _ from 'lodash'
import Log from 'loglevel'
import Loader from '../loader/LoaderFileUpload'

const AttachmentPreview = (props) => props.isImage ? // eslint-disable-line no-confusing-arrow
	<img src={props.attachment.preview}/> :
	<div className="attachment-non-image">
		<div className="fa fa-file-text-o attachment-icon" />
		<div className="attachment-name">{props.attachment.name}</div>
	</div>

AttachmentPreview.propTypes = {
	isImage: React.PropTypes.bool,
	attachment: React.PropTypes.object
}

AttachmentPreview.defaultProps = {
	isImage: false,
	attachment: {}
}

const AttachmentProgress = (props) => props.inProgress ? // eslint-disable-line no-confusing-arrow
	<div className="in-progress-wrapper">
		<div className="progress-overlay"/>
		<div className="progress-value">{props.value}%</div>
		<div className="progress-loader-wrapper">
			<Loader visible={true} />
		</div>
	</div> :
	null

AttachmentProgress.propTypes = {
	inProgress: React.PropTypes.bool,
	value: React.PropTypes.number
}

AttachmentProgress.defaultProps = {
	inProgress: false,
	value: 0
}


/**
 * Attachment entry component.
 * @param {Object} props
 * @param {func}   props.onAttachmentsChanged
 * @param {Array}  props.attachments
 */

class AttachmentEntry extends React.Component {
	constructor(props) {
		super(props)

		this.state = this.getDefaultState()

		this.files = []

	}

	getDefaultState() {
		return {
			attachments: []
		}
	}

	onProgress(file, progress) {
		const attachments = this.state.attachments
		attachments.map(x => {
			if (x.id === file.id) {
				x.progress = progress
			}

			return x
		})
		this.setState({attachments})
	}

	onError(file, err) {
		const attachments = this.state.attachments
		_.remove(attachments, x => x.id === file.id)
		this.setState({attachments})
		Log.error(`AttachmentEntry|onError|error:${err}`)
	}

	onFinish(file, info) {
		const attachments = this.state.attachments
		attachments.map(x => {
			if (x.id === file.id) {
				x.inProgress = false
				x.tmpFilePath = info.filename
			}

			return x
		})
		this.setState({attachments})
	}

	onDrop(files) {
		files.map(file => {
			file.id = _.uniqueId()
			file.inProgress = true
			file.progress = 0

			return file
		})

		files.forEach(file => {
			const S3Uploader = new S3Upload({
				files: [file],
				signingUrl: '/s3/sign',
				signingUrlQueryParams: {},
				onProgress: this.onProgress.bind(this, file),
				onFinishS3Put: this.onFinish.bind(this, file),
				onError: this.onError.bind(this, file),
				uploadRequestHeaders: {'x-amz-acl': 'public-read'},
				contentDisposition: file.type,
				server: ''
			})

			return S3Uploader
		})

		const attachments = this.state.attachments.concat(files)
		this.setState({attachments})
		this.props.onAttachmentsChanged(attachments)
	}

	onOpenClick() {
		this.refs.dropzone.open()
	}

	onRemoveClick(attachment, event) {
		event.preventDefault()

		const attachments = this.state.attachments.filter((x) => attachment.id !== x.id)

		this.setState({attachments})
		this.props.onAttachmentsChanged(attachments)

		return false
	}

	reset() {
		const state = this.getDefaultState()
		this.setState(state)
	}

	get tooltip() {
		return <Tooltip id="attachment-tooltip"><b>Add an attachment</b></Tooltip>
	}

	render() {
		return <div className="attachments-entry-wrapper">
			<div className="attachments-preview">
				{this.state.attachments.map(attachment => <div
						key={`attachment_${attachment.id}`}
						className="attachment-preview">
						<div className="attachment">
							<AttachmentPreview
								attachment={attachment}
								isImage={attachment.type.indexOf('image') > -1}
							/>
						</div>
						<AttachmentProgress
							inProgress={attachment.inProgress}
							value={attachment.progress}
						/>

						<div className="attachment-actions">
							<span
								className="remove-attachment"
								onClick={this.onRemoveClick.bind(this, attachment)}>Remove
							</span>
						</div>
					</div>
				)}
			</div>
			<a className="attachments-entry">
				<Dropzone ref="dropzone" onDrop={this.onDrop.bind(this)} className="hidden">
					<div>Try dropping some files here, or click to select files to upload.</div>
				</Dropzone>
				<OverlayTrigger placement="bottom" overlay={this.tooltip}>
					<i onClick={this.onOpenClick.bind(this)} className="fa fa-paperclip fa-fw fa-lg"/>
				</OverlayTrigger>
			</a>
		</div>
	}
}

AttachmentEntry.propTypes = {
	attachments: React.PropTypes.array,
	onAttachmentsChanged: React.PropTypes.func
}

AttachmentEntry.defaultProps = {
	onAttachmentsChanged: () => {
		// do nothing.
	}
}

export default AttachmentEntry
