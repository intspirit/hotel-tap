'use strict'

import React from 'react'

/*eslint-disable no-confusing-arrow */
const propFactory = prop => obj => typeof prop === 'function' ? prop(obj) : obj[prop]

/**
 * Select component.
 *
 * @param {Object} props
 * @param {Array} props.items
 * @param {String} props.selected
 * @param {Function} props.onChange
 * @param {String} props.placeHolderText
 * @param {Function|String} props.dataTextField
 * @param {Function|String} props.dataValueField
 *
 * @constructor
 */
const Select = props => {
	const getValue = propFactory(props.dataValueField)
	const getText = propFactory(props.dataTextField)

	return <label className="select">
		<select role="button"
			value={props.selected}
			onChange={props.onChange}>
			{
				props.showPlaceHolder ?
					<option value={props.placeHolderValue} disabled>{props.placeHolderText}</option> :
					null
			}
			{props.items.map(x => <option key={getValue(x)} value={getValue(x)}>{getText(x)}</option>)}
		</select>
		<i role="button"/>
	</label>
}

Select.propTypes = {
	showPlaceHolder: React.PropTypes.bool,
	placeHolderText: React.PropTypes.string,
	placeHolderValue: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.number
	]),
	dataTextField: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.func
	]),
	dataValueField: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.func
	]),
	selected: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.number
	]),
	items: React.PropTypes.array,
	onChange: React.PropTypes.func
}

Select.defaultProps = {
	showPlaceHolder: true,
	placeHolderText: '',
	placeHolderValue: -1,
	dataTextField: 'text',
	dataValueField: 'value',
	selected: '',
	items: [],
	onChange: () => null
}

export default Select
