'use strict'

import React from 'react'
import classnames from 'classnames'
import UserImage from '../UserImage'

class UsersListItem extends React.Component {
	static propTypes = {
		selected: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
		user: React.PropTypes.object,
		onUserSelected: React.PropTypes.func
	}

	static defaultProps = {
		selected: false,
		disabled: false,
		onUserSelected: () => null
	}

	render() {
		const getClassName = () => classnames('checkbox', {
			'state-disabled': this.props.disabled
		})

		return <div className="checkbox list-users-item">
			<label className={getClassName()}>
				<input
					type="checkbox"
					disabled={this.props.disabled}
					checked={this.props.selected}
					onChange={ev => this.props.onUserSelected(this.props.user.id, ev.target.checked)}
				/>
				<i/>
				<UserImage imageUrl={this.props.user.imageUrl} fullName={this.props.user.fullName} />
				<span className="text">{this.props.user.fullName}</span>
			</label>
		</div>
	}
}

export default UsersListItem
