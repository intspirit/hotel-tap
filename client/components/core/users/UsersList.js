'use strict'

import React from 'react'

class UsersList extends React.Component {

	render() {
		return <div className="row scrollable users-selector">
			<div className="fc col col-xs-12 col-sm-12 col-md-12">
				<ul className="list-group">
					<li className="list-group-item">
						<fieldset>
							<section className="list-users">
								{this.props.children}
							</section>
						</fieldset>
					</li>
				</ul>
			</div>
		</div>
	}
}

export default UsersList
