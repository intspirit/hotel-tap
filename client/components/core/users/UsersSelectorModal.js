'use strict'

import React from 'react'
import Log from 'loglevel'
import _ from 'lodash'
import update from 'react-addons-update'
import {Button, Grid, Row, Col} from 'react-bootstrap'
import {utils, db} from '../../../../shared/core'
import UsersList from './UsersList'
import UsersListItem from './UsersListItem'
import Modal from '../Modal'
import Loader from '../loader/Loader'

class UsersSelectorModal extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			usersIndexed: {}
		}

		this.selectedUsers = _.keyBy(props.selectedUsers, 'userId')

		this.handleSubmit = this.handleSubmit.bind(this)
		this.hide = this.hide.bind(this)
	}

	async componentDidMount() {
		this.setState({
			isLoading: true
		})

		await this.build()

		this.setState({
			usersIndexed: this.state.usersIndexed,
			isLoading: false
		})
	}

	async build() {
		try {
			const users = await db.getOtherUsers()
			const usersIndexed = _.chain(users).
				map(x => _.assign(x, {
					selected: false,
					disabled: false,
					fullName: utils.getFullName(x)
				})).
				keyBy('id').
				value()

			this.props.selectedUsers.forEach(x => {
				usersIndexed[x.userId].selected = true
				usersIndexed[x.userId].disabled = x.disabled
			})

			_.assign(this.state, {usersIndexed})

		} catch (err) {
			Log.error(`UsersSelectorModal|build|error:${err}`)
		}
	}

	userSelected(userId, selected) {
		this.selectedUsers[userId] = {
			id: this.state.usersIndexed[userId].id,
			name: this.state.usersIndexed[userId].fullName,
			userId: userId,
			selected: selected
		}
		const newUsersIndexed = update(this.state.usersIndexed, {[userId]: {selected: {$set: selected}}})
		this.setState({usersIndexed: newUsersIndexed})
	}

	hide() {
		this.props.onHide()
	}

	handleSubmit() {
		this.props.onProcessSelected(Object.values(this.selectedUsers).filter(x => x.selected))
		this.hide()
	}

	handleEnter() {
		this.selectedUsers = _.keyBy(this.props.selectedUsers, 'userId')
		this.build()
	}

	get users() {
		return _.chain(this.state.usersIndexed).
			values().
			sortBy(x => x.fullName).
			value()
	}

	get body() {
		return this.state.isLoading ?
			<Loader visible={this.state.isLoading} /> :
			<form className="smart-form">
				<UsersList>
					{this.users.map(x => <UsersListItem
						key={x.id}
						user={x}
						selected={x.selected}
						disabled={x.disabled}
						onUserSelected={this.userSelected.bind(this)}
					/>)}
				</UsersList>
			</form>
	}

	get footer() {
		return <Grid fluid={true}>
			<Row>
				<Col>
					<Button onClick={this.hide}>Cancel</Button>
					<Button onClick={this.handleSubmit} bsStyle="primary">Save</Button>
				</Col>
			</Row>
		</Grid>
	}

	render() {
		return <Modal
			className="users-selector-modal"
			show={this.props.show}
			onHide={this.hide}
			title="Users"
			body={this.body}
			footer={this.footer} />
	}
}

UsersSelectorModal.propTypes = {
	selectedUsers: React.PropTypes.array,
	show: React.PropTypes.bool,
	onHide: React.PropTypes.func,
	onProcessSelected: React.PropTypes.func
}

UsersSelectorModal.defaultProps = {
	selectedUsers: [],
	show: false,
	onHide: () => null,
	onProcessSelected: () => null
}

export default UsersSelectorModal
