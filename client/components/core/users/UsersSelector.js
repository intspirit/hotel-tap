'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'

/**
 * Users selector component.
 *
 * @param {Object} props
 * @param {func}   props.onClick
 */

class UsersSelector extends React.Component {

	get tooltip() {
		return <Tooltip id="tag-tooltip"><b>Add users</b></Tooltip>
	}

	render() {
		return <a className="tags-entry">
			<OverlayTrigger placement="bottom" overlay={this.tooltip}>
				<i onClick={this.props.onClick} className="fa fa-user fa-fw fa-lg"/>
			</OverlayTrigger>
		</a>
	}
}

UsersSelector.propTypes = {
	onClick: React.PropTypes.func
}

UsersSelector.defaultProps = {
	onClick: () => null
}

export default UsersSelector
