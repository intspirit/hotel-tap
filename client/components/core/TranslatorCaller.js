'use strict'

import React from 'react'

/**
 * component.
 */
class TranslatorCaller extends React.Component {
	render() {
		return <a className="translator-caller" href="#" onClick={e => e.preventDefault()}>
			See Translation
		</a>
	}
}

export default TranslatorCaller
