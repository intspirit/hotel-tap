'use strict'

import React from 'react'
import Avatar from 'react-avatar'
import {userImageDimensions} from '../../../shared/constants'

/**
 * User image component.
 * @param {Object} props
 * @param {string} props.imageUrl
 * @param {string} props.fullName
 * @param {int} props.dimension
 * @constructor
 */
const UserImage = (props) => <Avatar name={props.fullName} size={props.dimension} src={props.imageUrl} />


UserImage.propTypes = {
	imageUrl: React.PropTypes.string,
	fullName: React.PropTypes.string.isRequired,
	dimension: React.PropTypes.number
}

UserImage.defaultProps = {
	imageUrl: '',
	fullName: '',
	dimension: userImageDimensions.NORMAL
}

export default UserImage
