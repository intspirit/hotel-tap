'use strict'

import React from 'react'
import {Link} from 'react-router'

/**
 * Renders post cc list.
 *
 * @param {Object} props
 * @param {{userId: int, fullName: string}[]} props.users
 * @constructor
 */
const PostUsers = props => props.users.length > 0 ? //eslint-disable-line no-confusing-arrow
	<div className="text">
		{props.showLabel ? <i className="fa fa-user margin-right-13"/> : null}
		{props.users.map(x =>
			<Link key={x.userId} className="margin-right-13" to={`/boards/employee/${x.userId}`}>{x.fullName}</Link>)}
	</div> :
	null


PostUsers.propTypes = {
	users: React.PropTypes.array,
	showLabel: React.PropTypes.bool
}
PostUsers.defaultProps = {
	users: [],
	showLabel: true
}

export default PostUsers
