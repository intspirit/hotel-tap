'use strict'

import React from 'react'

/**
 * Empty list
 *
 * @param {Object} props
 * @param {String} props.title
 * @constructor
 */
const EmptyList = props => <div className="empty-list">{props.title}</div>

EmptyList.propTypes = {
	title: React.PropTypes.string
}

EmptyList.defaultProps = {
	title: 'List is Empty'
}

export default EmptyList
