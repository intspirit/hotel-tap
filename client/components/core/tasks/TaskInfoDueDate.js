'use strict'

import React from 'react'
import DateTime from '../DateTime'
import {emptyDueDateTimeMessage} from '../../../../shared/constants'

/**
 * TaskInfoDueDate component.
 *
 * @param {Object}  props
 * @param {Boolean} props.overdue
 * @param {String}  props.dateTime
 * @constructor
 */
const TaskInfoDueDate = (props) => props.dateTime ? //eslint-disable-line no-confusing-arrow
	<span className={props.overdue ? 'overdue' : ''}>
		Due <DateTime createdDateTime={props.dateTime}/>
	</span> :
	<span className="text-muted">
		{emptyDueDateTimeMessage}
	</span>

TaskInfoDueDate.propTypes = {
	overdue: React.PropTypes.bool,
	dateTime: React.PropTypes.string
}

TaskInfoDueDate.defaultProps = {
	overdue: false,
	dateTime: null
}

export default TaskInfoDueDate
