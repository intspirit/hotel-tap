'use strict'

import React from 'react'
import {Alert} from 'react-bootstrap'
import moment from 'moment'
import DateTimePicker from '../DateTimePicker'
import {dateTimeHelper} from '../../../../shared/core'

/**
 * TaskInfoDueDateEdit component.
 *
 * @param {Object}    props
 * @param {String}    props.dueDateTime
 * @param {Function}  props.changeDueDate
 * @param {Function}  props.cancelEditTask
 * @constructor
 */
class TaskInfoDueDateEdit extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			dueDateTime: props.dueDateTime,
			errorMessage: ''
		}

		this.save = this.save.bind(this)
		this.handleSaveClick = this.handleSaveClick.bind(this)
		this.handleCancelClick = this.handleCancelClick.bind(this)
		this.setErrorMessage = this.setErrorMessage.bind(this)
		this.resetErrorMessage = this.resetErrorMessage.bind(this)
		this.handleAlertDismiss = this.handleAlertDismiss.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.dueDateTime !== this.props.dueDateTime) {
			this.setState({dueDateTime: nextProps.dueDateTime})
		}
	}

	save() {
		this.props.changeDueDateTime(this.state.dueDateTime)
	}

	handleSaveClick() {
		if (this.invalidDueDateTime(this.state.dueDateTime)) {
			this.setErrorMessage()

			return
		}
		this.resetErrorMessage()
		this.save()
	}

	handleCancelClick() {
		this.props.cancelEditTask()
	}

	dueDateTimeChanged(dueDateTime) {
		this.setState({dueDateTime})
	}

	invalidDueDateTime(dueDateTime) {
		return moment(dueDateTime).isBefore(moment().seconds(0))
	}

	setErrorMessage() {
		this.setState({errorMessage: 'Please enter a Due date and Time in the future'})
	}

	resetErrorMessage() {
		this.setState({errorMessage: ''})
	}

	handleAlertDismiss() {
		this.resetErrorMessage()
	}

	render() {
		return <form className="smart-form due-date-form pull-left">
			{this.state.errorMessage && <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
				{this.state.errorMessage}
			</Alert>}
			<label className="input pull-left">
				<DateTimePicker
					timeFormat={false}
					closeOnSelect={true}
					inputProps={{
						placeholder: 'Due Date'
					}}
					value={this.state.dueDateTime}
					onChange={::this.dueDateTimeChanged}
					isValidDate={dateTime => dateTimeHelper.validateDueDateTime(dateTime)}
				/>
				<i role="button" className="icon-append fa fa-calendar"/>
			</label>
			<label className="input pull-left time-picker">
				<DateTimePicker
					dateFormat={false}
					closeOnSelect={true}
					inputProps={{
						placeholder: 'Due Time'
					}}
					value={this.state.dueDateTime}
					onChange={::this.dueDateTimeChanged}
					isValidDate={dateTime => dateTimeHelper.validateDueDateTime(dateTime)}
				/>
				<i role="button" className="icon-append fa fa-clock-o"/>
			</label>
			<button type="button" className="btn btn-primary btn-sm" onClick={this.handleSaveClick}>Save</button>
			<button type="button" className="btn btn-default btn-sm" onClick={this.handleCancelClick}>Cancel</button>
		</form>
	}
}

TaskInfoDueDateEdit.propTypes = {
	dueDateTime: React.PropTypes.string,
	changeDueDateTime: React.PropTypes.func,
	cancelEditTask: React.PropTypes.func
}

TaskInfoDueDateEdit.defaultProps = {
	changeDueDateTime: () => null,
	cancelEditTask: () => null
}

export default TaskInfoDueDateEdit
