'use strict'

import React from 'react'
import DateTime from '../DateTime'
import UserImage from '../UserImage'
import {userImageDimensions} from '../../../../shared/constants'

/**
 * TaskCompletionInfo component.
 *
 * @param {Object} props
 * @param {Object} props.completion
 * @constructor
 */
const TaskCompletionInfo = (props) => props.completion && props.completion.completedBy ? //eslint-disable-line no-confusing-arrow
	<div>
		<span className="completion">
			{props.completion.completedBy.fullName} completed this task
		</span>
		<DateTime createdDateTime={props.completion.dateTime}/>
		<div className="assignment-avatar-wrapper pull-left">
			<UserImage
				imageUrl={props.completion.completedBy.imageUrl}
				fullName={props.completion.completedBy.fullName}
				dimension={userImageDimensions.SMALL}
			/>
		</div>
	</div> :
	null

TaskCompletionInfo.propTypes = {
	completion: React.PropTypes.object
}

TaskCompletionInfo.defaultProps = {
	completion: {
		status: ''
	}
}

export default TaskCompletionInfo
