'use strict'

import React from 'react'
import UserImage from '../UserImage'
import {userImageDimensions, emptyTaskAssignmentMessage} from '../../../../shared/constants'

/**
 * TaskInfoAssignment component.
 *
 * @param {Object}  props
 * @param {Boolean} props.overdue
 * @param {String}  props.dateTime
 * @constructor
 */
const TaskInfoAssignment = (props) => props.assignmentTo && props.assignmentTo.id !== -1 ? //eslint-disable-line no-confusing-arrow
	<div className="pull-right">
		<div className="assignment-avatar-wrapper pull-right">
			<UserImage
				imageUrl={props.assignmentTo.imageUrl}
				fullName={props.assignmentTo.fullName}
				dimension={userImageDimensions.SMALL}
			/>
		</div>
		<span className="pull-right assignment">
			Assigned to {props.assignmentTo.fullName}
		</span>
	</div> :
	<div className="pull-right text-danger">
		{emptyTaskAssignmentMessage}
	</div>

TaskInfoAssignment.propTypes = {
	assignmentTo: React.PropTypes.object
}

TaskInfoAssignment.defaultProps = {
	assignmentTo: null
}

export default TaskInfoAssignment
