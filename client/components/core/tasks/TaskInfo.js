'use strict'

import React from 'react'
import TaskCompletionInfo from './TaskCompletionInfo'
import TaskIncompletedInfo from './TaskIncompletedInfo'
import TaskInfoAssignment from './TaskInfoAssignment'
import TaskInfoDueDateEdit from './TaskInfoDueDateEdit'
import {taskStatus} from '../../../../shared/constants'

/**
 * TaskViewableInfo component.
 *
 * @param {Object}  props
 * @param {Object}  props.assignmentTo
 * @param {Object}  props.completion
 * @param {String}  props.dueDateTime
 * @constructor
 */
const TaskViewableInfo = (props) => props.completion.status === taskStatus.DONE ? //eslint-disable-line no-confusing-arrow
	<TaskCompletionInfo completion={props.completion} /> :
	<TaskIncompletedInfo assignmentTo={props.assignmentTo} dueDateTime={props.dueDateTime} />

TaskViewableInfo.propTypes = {
	assignmentTo: React.PropTypes.object,
	completion: React.PropTypes.object,
	dueDateTime: React.PropTypes.string
}

/**
 * TaskInfo component.
 *
 * @param {Object}  props
 * @param {Object}  props.assignmentTo
 * @param {Object}  props.completion
 * @param {String}  props.subject
 * @param {String}  props.dueDateTime
 * @param {Boolean} props.editMode
 * @constructor
 */
const TaskInfo = (props) => props.editMode ? //eslint-disable-line no-confusing-arrow
	<div className="task-info">
		<div className="clearfix">
			<TaskInfoDueDateEdit
				dueDateTime={props.dueDateTime}
				changeDueDateTime={props.changeDueDateTime}
				cancelEditTask={props.cancelEditTask}
			/>
			<TaskInfoAssignment assignmentTo={props.assignmentTo} />
		</div>
	</div> :
	<div className="task-info">
		<TaskViewableInfo
			completion={props.completion}
			assignmentTo={props.assignmentTo}
			dueDateTime={props.dueDateTime}
		/>
	</div>

TaskInfo.propTypes = {
	assignmentTo: React.PropTypes.object,
	cancelEditTask: React.PropTypes.func,
	changeDueDateTime: React.PropTypes.func,
	completion: React.PropTypes.object,
	dueDateTime: React.PropTypes.string,
	editMode: React.PropTypes.bool,
	subject: React.PropTypes.string
}

TaskInfo.defaultProps = {
	assignmentTo: {
		imageUrl: ''
	},
	cancelEditTask: () => null,
	changeDueDateTime: () => null,
	completion: {
		status: ''
	},
	dueDateTime: '',
	editMode: false,
	subject: ''
}

export default TaskInfo
