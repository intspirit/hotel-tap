'use strict'

import React from 'react'
import {dateTimeHelper} from '../../../../shared/core'
import TaskInfoDueDate from './TaskInfoDueDate'
import TaskInfoAssignment from './TaskInfoAssignment'

/**
 * TaskIncompletedInfo component.
 *
 * @param {Object} props
 * @param {Object} props.assignmentTo
 * @param {String} props.dueDateTime
 * @constructor
 */
const TaskIncompletedInfo = (props) => {
	const overdue = dateTimeHelper.getHotelNow().isAfter(dateTimeHelper.getDateTime(props.dueDateTime))

	return <div>
			<TaskInfoDueDate dateTime={props.dueDateTime} overdue={overdue} />
			<TaskInfoAssignment assignmentTo={props.assignmentTo} />
		</div>
}

TaskIncompletedInfo.propTypes = {
	assignmentTo: React.PropTypes.object,
	dueDateTime: React.PropTypes.string
}

TaskIncompletedInfo.defaultProps = {
	assignmentTo: {
		imageUrl: ''
	}
}

export default TaskIncompletedInfo
