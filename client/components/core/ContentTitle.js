'use strict'

import React, {PropTypes} from 'react'
import {boardTypes} from '../../../shared/constants'

const boardTypesIconClasses = {
	[boardTypes.HOME]: 'fa-home',
	[boardTypes.EMPLOYEE]: 'fa-user',
	[boardTypes.DEPARTMENT]: 'fa-cube',
	[boardTypes.PM]: 'fa-tasks',
	[boardTypes.CHECKLIST_SCHEDULE]: 'fa-tasks',
	[boardTypes.INSPECTIONS]: 'fa-tasks',
	[boardTypes.DASHBOARD]: 'fa-dashboard',
	[boardTypes.REPORTS]: 'fa-bar-chart',
	[boardTypes.DOCUMENTS]: 'fa-file-text-o',
	[boardTypes.HELP]: 'fa-file-video-o'
}

/**
 * Content (feed, task, ...) title component.
 * @param {Object} props
 * @param {string} props.title
 * @param {string} props.icon
 * @constructor
 */
const ContentTitle = props =>
	<h1 className="page-title txt-color-blueDark">
		{props.loaded ?
			<div>
				{props.icon ?
					<i className="fa fa-fw custom-icon">
						<div style={{backgroundImage: `url(${props.icon})`}} />
					</i> :
					<i className={`fa fa-fw ${boardTypesIconClasses[props.type] || ''}`}/>
				}
				{props.title}
				{props.children}
			</div> :

			<div className="col-1">
				<i className="fa fa-spinner fa-spin"/> Loading ...
			</div>
		}
	</h1>

ContentTitle.propTypes = {
	title: PropTypes.string.isRequired,
	icon: PropTypes.string,
	type: PropTypes.string,
	loaded: PropTypes.bool
}
ContentTitle.defaultProps = {
	title: '',
	icon: '',
	type: '',
	loaded: true
}

export default ContentTitle
