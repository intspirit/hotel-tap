'use strict'

import React from 'react'
import DateTimePicker from 'react-datetime'

import 'react-datetime/css/react-datetime.css'

import moment from 'moment'

const dateTimePickerReference = 'dateTimePicker'

class DateTimePicker2 extends React.Component {
	constructor(props) {
		super(props)

		this.state = {value: props.value}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.value !== this.props.value) {
			this.setState({value: nextProps.value})
		}
	}

	getDateTimeFormat() {
		return this.props.dateTimeFormat || 'YYYY-MM-DD HH:mm:ss'
	}

	getDateTimeString(dateTime) {
		const dateTimeMoment = moment(dateTime)

		return dateTimeMoment.isValid() ? dateTimeMoment.format(this.getDateTimeFormat()) : ''
	}

	onFocus() {
		if (!this.state.value) {
			const startOfDay = this.getDateTimeString(moment().startOf('day'))
			this.setState({value: startOfDay},
				() => {
					this.refs[dateTimePickerReference].openCalendar()
					if (this.props.onFocus) {
						this.props.onFocus()
					}
				})
		} else if (this.props.onFocus) {
			this.props.onFocus()
		}
	}

	onBlur(dateTime) {
		if (this.props.onBlur) {
			this.props.onBlur(this.getDateTimeString(dateTime))
		} else if (!this.props.value) {
			this.onChange(dateTime)
		}
	}

	onChange(dateTime) {
		if (this.props.onChange) {
			this.props.onChange(this.getDateTimeString(dateTime))
		}
	}

	render() {
		let dateTimeValue = null
		if (this.state.value) {
			const dateTime = moment(this.state.value)
			if (dateTime.isValid()) {
				dateTimeValue = dateTime
			}
		}

		const dateTimePickerProps = Object.assign({}, this.props, {
			onChange: dateTime => this.onChange(dateTime),
			onFocus: () => this.onFocus(),
			onBlur: dateTime => this.onBlur(dateTime),
			value: dateTimeValue
		})

		return <DateTimePicker {...dateTimePickerProps} ref={dateTimePickerReference}/>
	}
}

DateTimePicker2.propTypes = {
	dateTimeFormat: React.PropTypes.string,
	value: React.PropTypes.any,
	onChange: React.PropTypes.func,
	onFocus: React.PropTypes.func,
	onBlur: React.PropTypes.func
}

export default DateTimePicker2
