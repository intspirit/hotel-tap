'use strict'

import React from 'react'
import ContentEditable from 'react-contenteditable'

/**
 * Description placeholder component.
 * @param {Object} props
 * @param {string} props.placeholder
 * @param {boolean} props.visible
 * @param {func} props.onClick
 * @constructor
 */
const DescriptionPlaceholder = props => props.placeholder && props.visible ? //eslint-disable-line no-confusing-arrow
	<div
		className="description-placeholder text-muted"
		onClick={props.onClick}
		dangerouslySetInnerHTML={{__html: props.placeholder}} /> :
	null

DescriptionPlaceholder.propTypes = {
	placeholder: React.PropTypes.string,
	visible: React.PropTypes.bool,
	onClick: React.PropTypes.func
}
DescriptionPlaceholder.defaultProps = {
	placeholder: '',
	visible: false,
	onClick: () => null
}

/**
 * Description entry component.
 * @param {Object} props
 * @param {string} props.value
 * @param {func} props.onChange
 * @param {string} props.className
 * @constructor
 */
class DescriptionEntry extends React.Component {
	onPlaceholderClick(e) {
		e.preventDefault()
		this.refs.ce.htmlEl.focus()
	}

	render() {
		return <section className="description-wrapper">
			<label className="textarea">
				<DescriptionPlaceholder
					placeholder={this.props.placeholder}
					visible={!this.props.value}
					onClick={this.onPlaceholderClick.bind(this)}
				/>
				<ContentEditable
					ref="ce"
					className={`ce ${this.props.className ? this.props.className : ''}`}
					html={this.props.value}
					onChange={this.props.onChange}/>
			</label>
		</section>
	}
}

DescriptionEntry.propTypes = {
	value: React.PropTypes.string,
	placeholder: React.PropTypes.string,
	onChange: React.PropTypes.func
}
DescriptionEntry.defaultProps = {
	value: '',
	placeholder: '',
	onChange: () => null
}

export default DescriptionEntry
