'use strict'

import React from 'react'
import {Modal} from 'react-bootstrap'

const isElementProps = (propName) =>
	propName === 'show' ||
	propName === 'onHide' ||
	propName === 'onEnter' ||
	propName === 'bsSize'

const getElementProps = (props) => {
	const elementProps = {}

	Object.entries(props).forEach(([propName, propValue]) => {
		if (isElementProps(propName)) {
			elementProps[propName] = propValue
		}
	})

	return elementProps
}

/**
 * Modal window component.
 * @param {Object} props
 * @constructor
 */
class Modal2 extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			title: this.props.title,
			body: this.props.body,
			footer: this.props.footer
		}
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.show) return

		this.setState({
			title: nextProps.title,
			body: nextProps.body,
			footer: nextProps.footer
		})
	}

	render() {
		const {className, ...props} = this.props
		const elementProps = getElementProps(props)

		return <Modal {...elementProps} className={className}>
			<Modal.Header closeButton>
				<Modal.Title>{this.state.title}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				{this.state.body}
			</Modal.Body>
			{this.props.footer &&
				<Modal.Footer >
					{this.state.footer}
				</Modal.Footer>
			}
		</Modal>
	}
}

Modal2.propTypes = {
	show: React.PropTypes.bool,
	onHide: React.PropTypes.func,
	onEnter: React.PropTypes.func,
	bsSize: React.PropTypes.string,
	className: React.PropTypes.string,
	title: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.element
	]),
	body: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.element
	]),
	footer: React.PropTypes.element
}

Modal2.defaultProps = {
	className: ''
}

export default Modal2
