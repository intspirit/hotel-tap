'use strict'

import React from 'react'


/**
 * Subject entry component.
 * @param {Object} props
 * @param {string} props.value
 * @param {string} props.nameHint
 * @param {func} props.onChange
 * @constructor
 */
const SubjectEntry = props => <section>
	<label className="input">
		<input
			type="text"
			placeholder={props.hint}
			onChange={props.onChange}
			value={props.value}
			autoFocus={true}/>
	</label>
</section>

SubjectEntry.propTypes = {
	value: React.PropTypes.string,
	hint: React.PropTypes.string,
	onChange: React.PropTypes.func
}
SubjectEntry.defaultProps = {
	value: '',
	hint: '',
	onChange: () => null
}

export default SubjectEntry
