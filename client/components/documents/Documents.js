'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'
import Log from 'loglevel'
import ContentTitle from '../core/ContentTitle'
import {boardTypes} from '../../../shared/constants'
import documentsService from '../../../shared/services/documentsService'
import Scroller from '../../core/scroller'


/**
 * Header for Documents.
 * @constructor
 */
const DocumentsHeader = () => <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<ContentTitle
		title="Documents"
		type={boardTypes.DOCUMENTS}
	/>
</div>


/**
 * Document.
 * @param {Object} props
 * @return {void}
 */
const Document = props => <tr>
	<td>
		<a className="fa fa-file-text-o" href={props.doc.documentUrl} target="_blank"/>
	</td>
	<td>
		{props.doc.title}
	</td>
</tr>

Document.propTypes = {
	doc: React.PropTypes.shape({
		id: React.PropTypes.number,
		title: React.PropTypes.string,
		documentUrl: React.PropTypes.string
	})
}


/**
 * Documents component.
 */
class Documents extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			documents: []
		}
	}

	async componentDidMount() {
		this.getDocuments()
		Scroller.scrollToTop()
	}

	async getDocuments() {
		try {
			const documents = await documentsService.getAll()
			this.setState(documents)
		} catch (err) {
			Log.error(`Documents|getDocuments|error:${err}`)
		}
	}

	render() {
		return <Row>
			<div className="col-sm-12">
				<DocumentsHeader />
			</div>

			<article className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div className="table-responsive col-sm-12">
					<table className="table table-bordered table-hover whiteBackground">
						<thead>
							<tr className="tableHeaderBackground">
								<th className="col-sm-1">File</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							{this.state.documents.map(x => <Document key={x.id} doc={x} />)}
						</tbody>
					</table>
				</div>
			</article>
		</Row>
	}
}

export default Documents
