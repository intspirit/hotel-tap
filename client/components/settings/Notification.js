'use strict'

import React from 'react'
import {Button} from 'react-bootstrap'

/**
 * Notification component.
 *
 * @param {Object} props
 * @param {boolean} props.notification
 * @param {Function} props.onCheck
 * @param {Function} props.hide
 */
class Notification extends React.Component {
	onNotificationChange() {
		const notification = !this.props.notification
		this.props.onCheck({notification})
	}

	render() {
		return <div className="widget-body">
			<form className="margin-top-10 form-horizontal">
				<div className="form-group no-margin">
					<label className="col-md-2 control-label" htmlFor="notification-status">
						Notifications
					</label>
					<div className="col-md-10">
						<span className="onoffswitch">
							<input
								type="checkbox"
								id="notification-status"
								className="onoffswitch-checkbox"
								checked={this.props.notification}
								onChange={this.onNotificationChange.bind(this)}
							/>
							<label className="onoffswitch-label" htmlFor="notification-status">
								<span className="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO"/>
								<span className="onoffswitch-switch"/>
							</label>
						</span>
					</div>
				</div>
				<div className="modal-footer">
					<Button
						onClick={this.props.hide}
						className="margin-right-13"
					>
						Cancel
					</Button>
				</div>
			</form>
		</div>
	}
}

Notification.propTypes = {
	notification: React.PropTypes.bool,
	onCheck: React.PropTypes.func,
	hide: React.PropTypes.func
}

Notification.defaultProps = {
	onCheck: () => null,
	hide: () => null
}

export default Notification
