'use strict'

import React from 'react'
import {Button} from 'react-bootstrap'
import ProfileImageEntry from '../core/attachments/ProfileImageEntry'

/**
 * Profile component.
 *
 * @param {Object} props
 * @param {Object} props.user
 * @param {Function} props.hide
 * @param {Function} props.onSubmit
 */
class Profile extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			firstName: props.user.firstName,
			lastName: props.user.lastName,
			validatorPassed: true
		}

		this.onFirstNameChange = this.onFirstNameChange.bind(this)
		this.onLastNameChange = this.onLastNameChange.bind(this)
		this.onProfileImageChange = this.onProfileImageChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}

	validate() {
		const firstName = this.state.firstName
			this.setState({
				validatorPassed: Boolean(firstName.trim())
			})
	}

	onFirstNameChange(e) {
		this.setState({
			firstName: e.target.value
		}, this.validate)
	}

	onLastNameChange(e) {
		this.setState({
			lastName: e.target.value
		})
	}

	onProfileImageChange(imageFileName) {
		this.setState({imageFileName})
	}

	handleSubmit() {
		const state = this.state
		const firstName = state.firstName.trim()
		const lastName = state.lastName.trim() || null
		const profileData = {firstName, lastName}

		if (state.hasOwnProperty('imageFileName'))
			profileData.imageFileName = state.imageFileName

		this.props.onSubmit(profileData)
	}

	render() {
		return <div className="widget-body">
			<form className="margin-top-10 form-horizontal">
				<fieldset>
					<div className={`form-group ${this.state.validatorPassed ? '' : 'has-error'}`}>
						<label className="col-md-2 control-label">First Name</label>
						<div className="col-md-10">
							<input
								className="form-control"
								placeholder="First Name"
								type="text"
								value={this.state.firstName}
								onChange={this.onFirstNameChange}
							/>
						</div>
						{!this.state.validatorPassed &&
							<span className="help-block col-md-offset-2">
							<i className="fa fa-warning margin-right-5"/>
								First name cannot be empty
							</span>
						}
					</div>
					<div className="form-group">
						<label className="col-md-2 control-label">Last Name</label>
						<div className="col-md-10">
							<input
								className="form-control"
								placeholder="Last Name"
								type="text"
								value={this.state.lastName}
								onChange={this.onLastNameChange}
							/>
						</div>
					</div>
					<div className="form-group">
						<label className="col-md-2 control-label">Email</label>
						<div className="col-md-10">
							<span className="form-control">{this.props.user.email}</span>
						</div>
					</div>
					<div className="form-group">
						<label className="col-md-2 control-label">Profile Image</label>
						<div className="col-md-10">
							<ProfileImageEntry
								profile={this.props.user}
								onAttachmentsChanged={this.onProfileImageChange}
							/>
						</div>
					</div>
				</fieldset>
				<div className="modal-footer">
					<Button
						onClick={this.props.hide}
						className="margin-right-13"
					>
						Cancel
					</Button>
					<Button
						disabled={!this.state.validatorPassed}
						onClick={this.handleSubmit}
						bsStyle="primary"
						className={`${this.state.validatorPassed ? '' : 'disabled'}`}
					>
						Update Profile
					</Button>
				</div>
			</form>
		</div>
	}
}

Profile.propTypes = {
	user: React.PropTypes.object,
	onSubmit: React.PropTypes.func,
	hide: React.PropTypes.func
}

Profile.defaultProps = {
	user: {},
	onSubmit: () => null,
	hide: () => null
}

export default Profile
