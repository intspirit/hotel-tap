'use strict'

import React from 'react'
import {Button} from 'react-bootstrap'
import validator from 'validator'
import _ from 'lodash'

import ValidationError from '../core/ValidationError'
import {messages} from '../../../shared/core'

function validate (state) {
	let errorMessages = []

	const oldPasswordCleared = state.oldPassword.trim()
	const newPasswordCleared = state.newPassword.trim()
	const confirmNewPasswordCleared = state.confirmNewPassword.trim()

	if (validator.isEmpty(oldPasswordCleared)) errorMessages.push(messages.error.OLD_PASSWORD_REQUIRED)

	if (validator.isEmpty(newPasswordCleared)) errorMessages.push(messages.error.NEW_PASSWORD_REQUIRED)

	if (validator.isEmpty(confirmNewPasswordCleared)) {
		errorMessages.push(messages.error.NEW_PASSWORD_CONFIRMATION_REQUIRED)
	} else if (!validator.isEmpty(newPasswordCleared) &&
				!validator.equals(newPasswordCleared, confirmNewPasswordCleared)) {
		errorMessages.push(messages.error.MATCH_PASSWORD_AND_CONFIRMATION)
	}

	return errorMessages
}

/**
 * Account component.
 *
 * @param {Object} props
 * @param {Function} props.onSubmit
 * @param {Function} props.hide
 */
class Account extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			oldPassword: '',
			newPassword: '',
			confirmNewPassword: '',
			validationErrors: this.props.validationErrors || []
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.validationErrors && nextProps.validationErrors.length) {
			const newValidationErrors = _.differenceBy(nextProps.validationErrors, this.state.validationErrors)
			this.setState({
				validationErrors: newValidationErrors.concat(this.state.validationErrors)
			})
		}
	}

	onOldPasswordChange(e) {
		this.setState({
			oldPassword: e.target.value
		})
	}

	onNewPasswordChange(e) {
		this.setState({
			newPassword: e.target.value
		})
	}

	onConfirmNewPasswordChange(e) {
		this.setState({
			confirmNewPassword: e.target.value
		})
	}

	handleSubmit() {
		const validationErrors = validate(this.state)

		if (validationErrors.length) {
			this.setState({
				validationErrors: validationErrors
			})
		} else {
			this.props.onSubmit({
				oldPassword: this.state.oldPassword.trim(),
				newPassword: this.state.newPassword.trim()
			})
		}
	}

	render() {
		return <div className="widget-body">
			<form className="margin-top-10 form-horizontal">
				<fieldset>
					<ValidationError errorMessages={this.state.validationErrors}/>
					<div className={`form-group ${this.state.validationErrors.oldPassword ? 'has-error' : ''}`}>
						<label className="col-md-3 control-label">Old Password</label>
						<div className="col-md-9">
							<input
								className="form-control"
								placeholder="Old Password"
								type="password"
								value={this.state.oldPassword}
								onChange={this.onOldPasswordChange.bind(this)}
							/>
						</div>
					</div>
					<div className={`form-group ${this.state.validationErrors.newPassword ? 'has-error' : ''}`}>
						<label className="col-md-3 control-label">New Password</label>
						<div className="col-md-9">
							<input
								className="form-control"
								placeholder="New Password"
								type="password"
								value={this.state.newPassword}
								onChange={this.onNewPasswordChange.bind(this)}
							/>
						</div>
					</div>
					<div className={`form-group ${this.state.validationErrors.confirmNewPassword ? 'has-error' : ''}`}>
						<label className="col-md-3 control-label">Confirm Password</label>
						<div className="col-md-9">
							<input
								className="form-control"
								placeholder="Confirm New Password"
								type="password"
								value={this.state.confirmNewPassword}
								onChange={this.onConfirmNewPasswordChange.bind(this)}
							/>
						</div>
					</div>
				</fieldset>
				<div className="modal-footer">
					<Button
						onClick={this.props.hide}
						className="margin-right-13"
					>
						Cancel
					</Button>
					<Button
						onClick={this.handleSubmit.bind(this)}
						bsStyle="primary"
					>
						Update Profile
					</Button>
				</div>
			</form>
		</div>
	}
}

Account.propTypes = {
	onSubmit: React.PropTypes.func,
	hide: React.PropTypes.func,
	validationErrors: React.PropTypes.arrayOf(React.PropTypes.string)
}

Account.defaultProps = {
	onSubmit: () => null,
	hide: () => null
}

export default Account
