'use strict'

import React from 'react'
import Log from 'loglevel'
import {Tabs, Tab} from 'react-bootstrap'
import constants from '../../../shared/constants'
import Modal from '../core/Modal'
import Loader from '../core/loader/Loader'
import Profile from './Profile'
import Account from './Account'
import Notification from './Notification'
import userService from '../../../shared/services/userService'
import appController from '../../core/appController'

/**
 * User profile settings component.
 *
 * @param {Object} props
 * @param {boolean} props.isVisible
 * @param {Function} props.onHide
 */
class ProfileSettingsModal extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			user: {},
			isLoading: false,
			tabKey: constants.userSettingsType.PROFILE,
			updateAccountErrors: []
		}

		this.hide = this.hide.bind(this)
		this.handleTabSelect = this.handleTabSelect.bind(this)
		this.handleFormUpdate = this.handleFormUpdate.bind(this)
	}

	async componentDidMount() {
		this.build()
	}

	async build() {
		this.setState({
			isLoading: true
		})

		try {
			let user = await userService.getMe()
			this.setState(user)
		} catch (err) {
			Log.error(`ProfileSettingsModal|build|error:${err}`)
		}

		this.setState({
			isLoading: false
		})
	}

	hide() {
		this.props.onHide()
	}

	handleTabSelect(tabKey) {
		this.setState({tabKey})
	}

	async handleFormUpdate(data) {
		this.setState({
			isLoading: true
		})

		try {
			await userService.update(data).
				then(user => {
				appController.userProfileUpdated(user)
				this.setState({
					user,
					isLoading: false
				})
			})

			if (this.state.tabKey !== constants.userSettingsType.NOTIFICATIONS)
				this.hide()
		} catch (err) {
			Log.error(`ProfileSettingsModal|handleProfileUpdate|error:${err}`)

			const state = this.state
			state.isLoading = false

			if (this.state.tabKey === constants.userSettingsType.ACCOUNT) {
				if (err.response && err.response.error && err.response.error.message) {
					state.updateAccountErrors = [err.response.error.message]
				}
			}
			this.setState(state)
		}
	}

	get activeKey() {
		return this.state.tabKey
	}

	get body() {
		return this.state.isLoading ?
			<Loader visible={this.state.isLoading} /> :
			<div className="widget-body">
				<Tabs activeKey={this.activeKey} onSelect={this.handleTabSelect} id="user-settings">
					<Tab eventKey={constants.userSettingsType.PROFILE} title="Profile">
						<Profile
							user={this.state.user}
							onSubmit={this.handleFormUpdate}
							hide={this.hide}
						/>
					</Tab>

					<Tab eventKey={constants.userSettingsType.ACCOUNT} title="Account">
						<Account
							onSubmit={this.handleFormUpdate}
							hide={this.hide}
							validationErrors={this.state.updateAccountErrors}
						/>
					</Tab>

					<Tab eventKey={constants.userSettingsType.NOTIFICATIONS} title="Notifications">
						<Notification
							notification={this.state.user.notificationStatus}
							onCheck={this.handleFormUpdate}
							hide={this.hide}
						/>
					</Tab>
				</Tabs>
			</div>
	}

	render() {
		return <Modal
			dialogClassName="col-md-6"
			show={this.props.isVisible}
			onHide={this.hide}
			title="My Profile Settings"
			body={this.body} />
	}
}

ProfileSettingsModal.propTypes = {
	isVisible: React.PropTypes.bool,
	onHide: React.PropTypes.func
}

ProfileSettingsModal.defaultProps = {
	isVisible: false,
	onHide: () => null
}

export default ProfileSettingsModal
