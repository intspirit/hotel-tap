'use strict'

import React from 'react'

import Attachments from '../core/attachments/Attachments'
import Comments from '../core/comments/Comments'
import PostUsers from '../core/PostUsers'
import PostTags from '../core/PostTags'
import PostComplaint from '../core/PostComplaint'
import Description from '../core/Description'
import PostHeader from './PostHeader'
import PostActions from './PostActions'
import Readers from '../core/Readers'
import {postReadStatus} from '../../../shared/constants'


/**
 * Note component.
 * @param {Object} props
 * @param {Object} props.note
 */
class Note extends React.Component {
	render() {
		let readStatusClass = this.props.readStatus === postReadStatus.READ ? 'readed' : 'unreaded'

		return <div className={`panel panel-default post-note ${readStatusClass}`}>
			<PostHeader
				{...this.props.note}
				readStatus={this.props.readStatus}
				updateFlagDueDate={this.props.updateFlagDueDate}
				flag={this.props.flag}
			/>
			<PostTags tags={this.props.note.tags}/>
			<PostUsers users={this.props.note.ccUsers}/>
			<PostComplaint
				complaint={this.props.note.guestComplaint}
				complaintResolution={this.props.complaintResolution}
				resolve={this.props.resolveComplaint}
				cancel={this.props.cancelComplaintResolution}
				onChange={this.props.onComplaintResolutionChange}
			/>
			<Description post={this.props.note} />
			<Attachments attachments={this.props.note.attachments} />
			<PostActions
				{...this.props.note}
				showCommentsEntry={this.props.showCommentsEntry}
				changeReadStatus={this.props.changeReadStatus}
				changeFlagStatus={this.props.changeFlagStatus}
				commentEntry={this.props.commentEntry}
				readStatus={this.props.readStatus}
				flag={this.props.flag}
				onConvertAction={this.props.showConvertTaskEntry}
			/>
			<div className="note-footer">
				<Readers typeId={this.props.note.typeId} readBy={this.props.readers}/>
				<Comments
					{...this.props.note}
					commentEntry={this.props.commentEntry}
					showCommentsEntry={this.props.showCommentsEntry}
				/>
			</div>
		</div>
	}
}

Note.propTypes = {
	note: React.PropTypes.object.isRequired,
	readers: React.PropTypes.array,
	readStatus: React.PropTypes.string,
	commentEntry: React.PropTypes.bool,
	flag: React.PropTypes.object,
	complaintResolution: React.PropTypes.object,
	changeReadStatus: React.PropTypes.func,
	showCommentsEntry: React.PropTypes.func,
	showConvertTaskEntry: React.PropTypes.func,
	changeFlagStatus: React.PropTypes.func,
	updateFlagDueDate: React.PropTypes.func,
	resolveComplaint: React.PropTypes.func,
	cancelComplaintResolution: React.PropTypes.func,
	onComplaintResolutionChange: React.PropTypes.func
}

Note.defaultProps = {
	note: {}
}

export default Note
