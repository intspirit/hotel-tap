'use strict'

import React from 'react'
import {Button} from 'react-bootstrap'
import DatePicker from '../core/DatePicker'
import Modal from '../core/Modal'
import postService from '../../../shared/services/postService'
import dateTimeHelper from '../../../shared/core/dateTimeHelper'
import {GAEvents} from '../../../shared/constants'
import {logEventForGA} from '../../functions'
import Log from 'loglevel'

/**
 * Post Flag changer component.
 * @param {Object} props
 * @param {Object} props.post
 * @constructor
 */
class PostFlagChanger extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			post: props.post,
			flag: props.post.flag,
			flagDueDate: dateTimeHelper.getUtcDateTime(props.post.flag.expirationDateTime, 'YYYY-MM-DD HH:mm:ss') || null
		}

		this.onFlagDueChange = this.onFlagDueChange.bind(this)
		this.hide = this.hide.bind(this)
	}

	async setFlag() {
		const flagProperties = this.state.flag.set && !this.state.isUpdate ?
			{flag: false, flagDue: false} :
			{flag: true, flagDue: this.state.flagDueDate}

		const flagLabelForGA = this.state.flag.set && !this.state.isUpdate ?
			GAEvents.labels.UNFLAGGED :
			GAEvents.labels.FLAGGED

		try {
			let {flag} = await postService.update(this.props.post.id, flagProperties)
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.FLAG, flagLabelForGA)

			this.hide()
			this.setState({
				flag: flag,
				flagDueDate: dateTimeHelper.getUtcDateTime(flag.expirationDateTime, 'YYYY-MM-DD HH:mm:ss') || null
			}, this.props.onFlagSet(flag))
		} catch (err) {
			Log.error(`PostFlagChanger|setFlag|error:${err}`)
		}
	}

	show(e) {
		e.preventDefault()
		this.setState({
			isVisible: true,
			isUpdate: false
		})
	}

	showForUpdate(e) {
		e.preventDefault()
		this.setState({
			isVisible: true,
			isUpdate: true
		})
	}

	hide() {
		this.setState({
			isVisible: false,
			isUpdate: false
		})
	}

	onFlagDueChange(date) {
		this.setState({
			flagDueDate: date
		})
	}

	get flagModalTitle() {
		return <span>Flag</span>
	}

	get flagModalBody() {
		return this.state.flag.set && !this.state.isUpdate ?
			`Do you really want to unflag this ${this.state.post.typeId}` :
			<span>Flag Expiry Date:
				<DatePicker
					selected={this.state.flagDueDate}
					minDate={dateTimeHelper.getHotelNow()}
					className="flag-due-input"
					onChange={this.onFlagDueChange} />
			</span>
	}

	get flagModalFooter() {
		return <span>
			<Button bsSize="small" onClick={this.setFlag.bind(this)} bsStyle="primary">
				Save
			</Button>
			<Button bsSize="small" onClick={this.hide}>Cancel</Button>
		</span>
	}

	render() {
		return <Modal
				bsSize="small"
				className="flag-modal"
				show={this.state.isVisible}
				onHide={this.hide}
				title={this.flagModalTitle}
				body={this.flagModalBody}
				footer={this.flagModalFooter}
			/>
	}
}

PostFlagChanger.propTypes = {
	post: React.PropTypes.object,
	onFlagSet: React.PropTypes.func
}

export default PostFlagChanger
