'use strict'

import React from 'react'
import {
	postReadStatus,
	postTypes,
	taskStatus,
	GAEvents} from '../../../shared/constants'
import appController from '../../core/appController'
import Scroller from '../../core/scroller'
import Note from './Note'
import Task from './Task'
import EditTask from './EditTask'
import postService from '../../../shared/services/postService'
import PostFlagChanger from './PostFlagChanger'
import Log from 'loglevel'
import {utils} from '../../../shared/core'
import {logEventForGA} from '../../functions'

//noinspection Eslint
/**
 * Post component (task or note decider).
 * @param {object} props
 * @param {object} props.post
 * @param {string} props.post.typeId
 * @param {int} props.post.id
 */
class Post extends React.Component {
	static propTypes = {
		boardId: React.PropTypes.number,
		boardType: React.PropTypes.string,
		post: React.PropTypes.shape({
			assignmentTo: React.PropTypes.object,
			completion: React.PropTypes.object,
			flag: React.PropTypes.object,
			typeId: React.PropTypes.string,
			id: React.PropTypes.number,
			readBy: React.PropTypes.Array,
			readStatus: React.PropTypes.string,
			taskDescription: React.PropTypes.object,
			createdBy: React.PropTypes.object,
			complaintResolution: React.PropTypes.object
		})
	}

	constructor(props) {
		super(props)
		const readers = this.props.post.readBy
		this.state = {
			commentEntry: false,
			isConvertTaskEntryVisible: false,
			readers: readers[0] === 'You' ? readers : [null, ...readers],
			readStatus: this.props.post.readStatus,
			flag: this.props.post.flag,
			expiryDateModal: false,
			complaintResolution: this.props.post.complaintResolution
		}

		this.changeReadStatus = this.changeReadStatus.bind(this)
		this.showCommentsEntry = this.showCommentsEntry.bind(this)
		this.showConvertTaskEntry = this.showConvertTaskEntry.bind(this)
		this.hideConvertTaskEntry = this.hideConvertTaskEntry.bind(this)
		this.changeFlagStatus = this.changeFlagStatus.bind(this)
		this.updateFlagDueDate = this.updateFlagDueDate.bind(this)
		this.changeCompletionStatus = this.changeCompletionStatus.bind(this)
		this.resolveComplaint = this.resolveComplaint.bind(this)
		this.cancelComplaintResolution = this.cancelComplaintResolution.bind(this)
		this.onComplaintResolutionChange = this.onComplaintResolutionChange.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		const newPost = nextProps.post
		let state = Object.assign({}, this.state)

		const readers = newPost.readBy
		state.readers = readers[0] === 'You' ? readers : [null, ...readers]

		state.readStatus = newPost.readStatus
		state.flag = newPost.flag

		const currentPost = this.props.post
		if (newPost.complaintResolution && currentPost.complaintResolution &&
			(newPost.complaintResolution.resolved !== currentPost.complaintResolution.resolved ||
			newPost.complaintResolution.resolution !== currentPost.complaintResolution.resolution ||
			newPost.complaintResolution.cost !== currentPost.complaintResolution.cost ||
			newPost.complaintResolution.pointsGiven !== currentPost.complaintResolution.pointsGiven)) {
			state.complaintResolution = newPost.complaintResolution
		}

		this.setState(state)
	}

	async changeReadStatus(e) {
		if (e) {
			e.preventDefault()
		}

		const newReadStatus = this.state.readStatus === postReadStatus.READ ?
			postReadStatus.UNREAD :
			postReadStatus.READ

		const readLabelForGA = this.state.readStatus === postReadStatus.READ ?
			GAEvents.labels.UNREAD :
			GAEvents.labels.READ

		try {
			const data = await postService.update(this.props.post.id, {readStatus: newReadStatus})
			const isAuthor = appController.user.id === this.props.post.createdBy.id
			const firstReader = data.readStatus === postReadStatus.READ && !isAuthor ? 'You' : null
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.MARK_READ, readLabelForGA)

			this.setState({
				readStatus: data.readStatus,
				readers: [firstReader, ...this.state.readers.splice(1)]
			}, appController.postReadStatusChanged(this.props.post.id))
		} catch (err) {
			Log.error(`Note|getPosts|changeReadStatus:${err}`)
		}
	}

	setFlag(flag) {
		this.setState({
			flag: flag,
			readers: []
		}, appController.postFlagStatusChanged(this.props.post.id))
	}

	changeFlagStatus(e) {
		e.preventDefault()
		this.refs.flagChanger.show(e)
	}

	updateFlagDueDate(e) {
		e.preventDefault()
		this.refs.flagChanger.showForUpdate(e)
	}

	async changeCompletionStatus(e) {
		if (e) {
			e.preventDefault()
		}
		const post = this.props.post
		const postId = post.id
		const isTaskUnread = this.state.readStatus === postReadStatus.UNREAD
		const newCompletionStatus = post.completion.status === taskStatus.OPEN ?
			taskStatus.DONE :
			taskStatus.OPEN
		const completionLabelForGA = post.completion.status === taskStatus.OPEN ?
			GAEvents.labels.DONE :
			GAEvents.labels.NOT_DONE
		try {
			const serviceResponse = await postService.update(postId, {completionStatus: newCompletionStatus})
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.TASK_DONE, completionLabelForGA)

			if (serviceResponse.status === newCompletionStatus) {
				appController.taskChanged(postId)
				appController.refreshTodos()
				if (isTaskUnread) this.changeReadStatus()
			} else {
				throw new Error(`Completion status was not updated`)
			}
		} catch (error) {
			Log.error(`Post|changeCompletionStatus|${error}`)
		}
	}

	async resolveComplaint() {
		const postId = this.props.post.id

		try {
			try {
				await postService.update(postId, {complaintResolution: {
					cost: this.state.complaintResolution.cost,
					pointsGiven: this.state.complaintResolution.pointsGiven,
					resolution: this.state.complaintResolution.resolution
				}})
			} catch (postServiceError) {
				Log.error(`Post|resolveComplaint|postService|${postServiceError}`)
			}
			appController.taskChanged(postId)
		} catch (error) {
			Log.error(`Post|resolveComplaint|${error}`)
		}
	}

	cancelComplaintResolution() {
		this.setState({complaintResolution: this.props.post.complaintResolution})
	}

	onComplaintResolutionChange(updatedResolution) {
		this.setState({complaintResolution: Object.assign({}, this.state.complaintResolution, updatedResolution)})
	}

	showCommentsEntry() {
		if (!this.state.commentEntry)
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.COMMENT)
		this.setState({
			commentEntry: !this.state.commentEntry
		})
	}

	showConvertTaskEntry() {
		logEventForGA(GAEvents.categories.POST, GAEvents.actions.CONVERT_TO_TASK)
		this.setState({
			isConvertTaskEntryVisible: true
		})
	}

	hideConvertTaskEntry() {
		logEventForGA(GAEvents.categories.POST, GAEvents.actions.CONVERT_TASK_CANCEL)
		this.setState({
			isConvertTaskEntryVisible: false
		})
	}

	scrollToTop() {
		if (!this.node) return

		Scroller.scrollTo(this.node)
	}

	renderTask() {
		return <Task
			task={this.props.post}
			key={this.props.post.id}
			completion={this.props.post.completion}
			readers={this.state.readers}
			readStatus={this.state.readStatus}
			commentEntry={this.state.commentEntry}
			flag={this.state.flag}
			complaintResolution={this.state.complaintResolution}
			taskDescription={this.props.post.taskDescription}
			assignmentTo={this.props.post.assignmentTo}
			showCommentsEntry={this.showCommentsEntry}
			changeReadStatus={this.changeReadStatus}
			changeFlagStatus={this.changeFlagStatus}
			changeCompletionStatus={this.changeCompletionStatus}
			resolveComplaint={this.resolveComplaint}
			cancelComplaintResolution={this.cancelComplaintResolution}
			onComplaintResolutionChange={this.onComplaintResolutionChange}
		/>
	}

	renderNote() {
		return <Note
			note={this.props.post}
			key={this.props.post.id}
			readers={this.state.readers}
			readStatus={this.state.readStatus}
			commentEntry={this.state.commentEntry}
			complaintResolution={this.state.complaintResolution}
			flag={this.state.flag}
			showCommentsEntry={this.showCommentsEntry}
			showConvertTaskEntry={this.showConvertTaskEntry}
			changeReadStatus={this.changeReadStatus}
			changeFlagStatus={this.changeFlagStatus}
			updateFlagDueDate={this.updateFlagDueDate}
			resolveComplaint={this.resolveComplaint}
			cancelComplaintResolution={this.cancelComplaintResolution}
			onComplaintResolutionChange={this.onComplaintResolutionChange}
		/>
	}

	renderEditTask() {
		return <EditTask
			board={utils.assignBoard(this.props.boardType, this.props.boardId)}
			task={this.props.post}
			onClose={this.hideConvertTaskEntry}
		/>
	}

	renderPost() {
		if (this.props.post.typeId === postTypes.TASK) {
			return this.renderTask()
		}

		return this.state.isConvertTaskEntryVisible ?
			this.renderEditTask() :
			this.renderNote()
	}

	render() {
		return <div ref={node => {
			this.node = node

			return node
		}}>
			<PostFlagChanger
				ref="flagChanger"
				post={this.props.post}
				onFlagSet={this.setFlag.bind(this)}
			/>
			{this.renderPost()}
		</div>
	}
}

export default Post
