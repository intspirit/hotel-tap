'use strict'

import React from 'react'
import appController from '../../core/appController'
import PostEntry from '../post/PostEntry'
import constants from '../../../shared/constants'
import {tagsMapper} from '../../../shared/core'

const getUuid = tagsMapper.getUuid

const mapTags = tags => tags.map(x =>
({
	id: x.id,
	type: x.type,
	tagId: getUuid(x.type, x.id),
	tagName: `#${x.name}`,
	groupId: getUuid(x.type, x.typeId),
	groupName: `@${x.typeName}`,
	selected: true,
	disabled: true
}))

/*eslint-disable no-confusing-arrow */
const mapUsers = (users, originallyCreatedBy) => users.map(x => x.userId === appController.user.id ?
	{
		id: originallyCreatedBy.id,
		name: originallyCreatedBy.fullName,
		userId: originallyCreatedBy.id,
		selected: true,
		disabled: true
	} :
	{
		id: x.userId,
		name: x.fullName,
		userId: x.userId,
		selected: true,
		disabled: true
	}
)

/*eslint-enable no-confusing-arrow */

/**
 * EditTask component.
 *
 * @param {Object}   props
 * @param {Object}   props.task
 * @param {String}   props.board
 * @param {Function} props.onClose
 */
class EditTask extends React.Component {

	render() {
		return <PostEntry
			board={this.props.board}
			id={this.props.task.id}
			typeId={constants.postTypes.TASK}
			tags={mapTags(this.props.task.tags)}
			users={mapUsers(this.props.task.ccUsers, this.props.task.createdBy)}
			guestComplaint={this.props.task.guestComplaint}
			privatePost={this.props.task.private}
			assignmentType={this.props.task.assignmentTo.type}
			assignmentId={this.props.task.assignmentTo.id}
			subject={this.props.task.taskDescription.subject}
			dueDateTime={this.props.task.taskDescription.dueDateTime}
			description={this.props.task.descriptionText}
			attachments={this.props.task.attachments}
			createdBy={this.props.task.createdBy.id}
			onClose={this.props.onClose}
		/>
	}
}

EditTask.propTypes = {
	task: React.PropTypes.object,
	board: React.PropTypes.string,
	onClose: React.PropTypes.func
}

EditTask.defaultProps = {
	onClose: () => null
}

export default EditTask
