'use strict'

import React from 'react'
import Log from 'loglevel'
import {Button} from 'react-bootstrap'
import _ from 'lodash'
import {setSortCriteria} from '../../../shared/functions'
import appController from '../../core/appController'
import Scroller from '../../core/scroller'
import postService from '../../../shared/services/postService'
import Post from './Post'
import Loader from '../core/loader/Loader'
import constants from '../../../shared/constants'

//TODO: VVS p2 REFACTOR
const fetchPost = async (postId) => {
	let post = null
	const serviceResponse = await postService.getPostById(postId)

	if (serviceResponse.hasError) {
		throw new Error('Post service response contains error.')
	} else if (serviceResponse.post) {
		post = serviceResponse.post
	} else {
		throw new Error('No post received.')
	}

	return post
}

/**
 * Posts component.
 */
class Posts extends React.Component {
	static propTypes = {
		assignment: React.PropTypes.string,
		id: React.PropTypes.number,
		postsLoaded: React.PropTypes.func,
		postsLoading: React.PropTypes.func
	}

	constructor(props) {
		super(props)
		this.state = {
			posts: [],
			hasMore: false,
			isLoading: true,
			isLoadingMore: false
		}

		this.fetchPostsIndexed = _.fromPairs([
			[constants.boardTypes.DEPARTMENT, postService.getByDepartmentId],
			[constants.boardTypes.EMPLOYEE, postService.getByEmployeeId],
			[constants.boardTypes.TAG, postService.getByTagId],
			[constants.boardTypes.SRP, postService.getComplaints]
		])

		appController.postsComponent = this
	}

	async getBoardPosts(assignment, id, nextPageToken) {
		// @TODO p1 get by board type
		return assignment === constants.boardTypes.SRP ?
			await this.fetchPostsIndexed[assignment](nextPageToken) :
			await this.fetchPostsIndexed[assignment](id, nextPageToken)
	}

	async getPosts(nextProps, options) {
		try {
			this.props.postsLoading()
			this.setState({
				isLoading: true
			})
			const props = nextProps || this.props
			const data = await this.getBoardPosts(props.assignment, props.id)
			const boardData = {
				boardName: data.boardName,
				boardType: data.boardType,
				boardIconUrl: data.boardIconUrl
			}
			this.setState({
				posts: data.posts.sort(setSortCriteria(constants.sortCriteria.BY_FLAG_STATUS, true)),
				hasMore: data.hasMore,
				nextPageToken: data.nextPageToken,
				boardData: boardData,
				isLoading: false,
				isShowingSpecificPost: false,
				focusedId: (options || {}).focusedId || null
			})
			this.props.postsLoaded(boardData)
		} catch (err) {
			this.setState({
				isLoading: false
			})
			Log.error(`Posts|getPosts|error:${err}`)
		}
	}

	async getMore() {
		try {
			this.setState({
				isLoadingMore: true
			})

			const data = await this.getBoardPosts(this.props.assignment, this.props.id, this.state.nextPageToken)

			this.setState({
				posts: this.state.posts.concat(data.posts),
				hasMore: data.hasMore,
				nextPageToken: data.nextPageToken,
				isLoadingMore: false,
				isShowingSpecificPost: false
			})
		} catch (err) {
			this.setState({
				isLoadingMore: false
			})
			Log.error(`Posts|getMore|error:${err}`)
		}
	}

	async getPost(postId) {
		try {
			const post = await fetchPost(postId)
			if (post) {
				let state = this.state
				state.posts = [post]
				state.hasMore = false
				state.isShowingSpecificPost = true
				this.setState(state)
			}
		} catch (error) {
			Log.error(`Posts|getPost|error:${error}`)
		}
	}

	async refreshPost(postId, options) {
		try {
			let state = this.state
			let posts = state.posts

			let postPositionInList = _.findIndex(posts, {id: postId})
			if (postPositionInList > -1) {
				const post = await fetchPost(postId)

				if (post) {
					posts[postPositionInList] = post

					if (options && options.reorder) {
						posts = _.orderBy(posts, ['flag.expirationDateTime', 'orderDateTimeUtc'], ['asc', 'desc'])
					}

					this.setState({
						posts: posts,
						focusedId: (options || {}).focusedId || null
					})
				}
			}
		} catch (error) {
			Log.error(`Posts|refreshPost|error:${error}`)
		}
	}

	componentDidUpdate() {
		if (this.state.focusedId) {
			const element = this.refs[`post_${this.state.focusedId}`]
			if (element) {
				element.scrollToTop()
			}
		}
	}

	//noinspection JSUnusedGlobalSymbols
	componentDidMount() {
		this.getPosts()
		Scroller.scrollToTop()
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.id !== nextProps.id || this.state.isShowingSpecificPost) {
			this.getPosts(nextProps)
			Scroller.scrollToTop()
		}
	}

	get posts() {
		return this.state.isLoading ?
			null :
			this.state.posts.map(post =>
				<Post
					post={post}
					key={post.id}
					ref={`post_${post.id}`}
					boardId={this.props.id}
					boardType={this.props.assignment}
				/>)
	}

	get moreButton() {
		return this.state.hasMore && !this.state.isLoading && !this.state.isLoadingMore ?
			<Button onClick={this.getMore.bind(this)} className="pull-right">Get more</Button> :
			null
	}

	get loader() {
		return this.state.isLoadingMore ?
			<div className="more-posts-loader-wrapper">
				<Loader visible={true} />
			</div> :
			null
	}

	render() {
		return <div>
			{this.posts}
			{this.loader}
			{this.moreButton}
		</div>
	}
}

Posts.propTypes = {
}

export default Posts
