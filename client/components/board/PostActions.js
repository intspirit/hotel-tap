'use strict'

import React from 'react'
import {postTypes, postReadStatus, taskStatus} from '../../../shared/constants'
import appController from '../../core/appController'

const ReadAction = (props) => <li>
	{props.readStatus === postReadStatus.READ ?
		<span onClick={props.changeReadStatus} className="readed">
			<i className="fa fa-envelope-o"/>
			<span className="hidden-sm action-title">
				Mark as unread
			</span>
		</span> :
		<span onClick={props.changeReadStatus} className="unreaded">
			<i className="fa fa-envelope"/>
			<span className="hidden-sm action-title">
				Mark as read
			</span>
		</span>
	}
</li>

ReadAction.propTypes = {
	readStatus: React.PropTypes.string,
	changeReadStatus: React.PropTypes.func
}

const CommentAction = props => <li>
	<span onClick={props.showCommentsEntry}>
		<i className="fa fa-comment-o"/>
		<span className="action-title">
			{props.title || 'Comment'}
		</span>
	</span>
</li>

CommentAction.propTypes = {
	title: React.PropTypes.string,
	showCommentsEntry: React.PropTypes.func
}

const FlagAction = props => props.available && <li>
	<span onClick={props.changeFlagStatus}>
		<i className="fa fa-flag-o"/>
		<span className="action-title">
			{props.flag.set ? 'Unflag' : 'Flag'}
		</span>
	</span>
</li>

FlagAction.propTypes = {
	changeFlagStatus: React.PropTypes.func,
	flag: React.PropTypes.object
}

/*eslint-disable no-confusing-arrow */
const ConvertAction = props => props.typeId === postTypes.NOTE && !props.private ?
	<li>
		<span onClick={() => props.onAction()}>
			<i className="fa fa-check"/>
			Convert to task
		</span>
	</li> :
	null

/*eslint-enable no-confusing-arrow */

ConvertAction.propTypes = {
	onAction: React.PropTypes.func,
	typeId: React.PropTypes.string,
	private: React.PropTypes.bool
}

ConvertAction.defaultProps = {
	onAction: () => null
}

/*eslint-disable no-confusing-arrow */
const ArchiveAction = props => props.typeId === postTypes.TASK && props.completion.status !== taskStatus.DONE ?
	null :
	<li>
		<span className="pull-right">
			<i className="fa fa-archive"/>
		</span>
	</li>

/*eslint-enable no-confusing-arrow */

ArchiveAction.propTypes = {
	typeId: React.PropTypes.string,
	completion: React.PropTypes.object
}

const EditAction = props => props.typeId === postTypes.TASK && props.completion.status !== taskStatus.DONE &&
<li>
	<span onClick={() => props.onAction()}>
		<i className="fa fa-edit"/>
		Edit
	</span>
</li>

EditAction.propTypes = {
	onAction: React.PropTypes.func,
	completion: React.PropTypes.object,
	typeId: React.PropTypes.string
}

EditAction.defaultProps = {
	onAction: () => null
}

/*eslint-disable no-confusing-arrow */
const DetailsActions = props => props.typeId === postTypes.TASK && props.completion.status === taskStatus.DONE ?
	<li>
		<span className="pull-right">
			View details
		</span>
	</li> :
	null

/*eslint-enable no-confusing-arrow */

DetailsActions.propTypes = {
	typeId: React.PropTypes.string,
	completion: React.PropTypes.object
}

/**
 * Post actions component.
 *
 * @param {Object} props
 * @param {Object} props.flag
 * @param {Object} props.completion
 * @param {String} props.readStatus
 * @param {Function} props.showCommentsEntry
 * @param {Function} props.changeReadStatus
 * @param {Function} props.changeFlagStatus
 * @param {String} props.typeId
 * @param {Boolean} props.commentEntry
 * @param {Function} props.onEditAction
 * @param {Boolean} props.editMode
 */
class PostActions extends React.Component {
	render() {
		return <ul className={`links ${this.props.completion.status}`}>
			<ReadAction
				completion={this.props.completion}
				changeReadStatus={this.props.changeReadStatus}
				readStatus={this.props.readStatus}
			/>
			<CommentAction
				title={this.props.complaintResolution ? 'Comment/Action' : 'Comment'}
				showCommentsEntry={this.props.showCommentsEntry}/>
			<FlagAction
				completion={this.props.completion}
				changeFlagStatus={this.props.changeFlagStatus}
				flag={this.props.flag}
				available={appController.user.isAdmin && this.props.typeId === postTypes.NOTE}
			/>
			<ConvertAction
				private={this.props.private}
				typeId={this.props.typeId}
				onAction={this.props.onConvertAction}
			/>
			{this.props.editMode ?
				null :
				<EditAction
					typeId={this.props.typeId}
					completion={this.props.completion}
					onAction={this.props.onEditAction}
				/>
			}
			<DetailsActions completion={this.props.completion}/>
			<ArchiveAction
				typeId={this.props.typeId}
				completion={this.props.completion}
			/>
		</ul>
	}
}

PostActions.propTypes = {
	completion: React.PropTypes.object,
	flag: React.PropTypes.object,
	readStatus: React.PropTypes.string,
	showCommentsEntry: React.PropTypes.func,
	changeReadStatus: React.PropTypes.func,
	changeFlagStatus: React.PropTypes.func,
	typeId: React.PropTypes.string,
	commentEntry: React.PropTypes.bool,
	onEditAction: React.PropTypes.func,
	editMode: React.PropTypes.bool,
	private: React.PropTypes.bool,
	onConvertAction: React.PropTypes.func,
	complaintResolution: React.PropTypes.object
}

PostActions.defaultProps = {
	completion: {},
	flag: {},
	readStatus: postReadStatus.UNREAD,
	showCommentsEntry: () => null,
	changeReadStatus: () => null,
	changeFlagStatus: () => null,
	typeId: postTypes.NOTE,
	commentEntry: false,
	onEditAction: () => null,
	editMode: false,
	private: false,
	onConvertAction: () => null
}

export default PostActions
