'use strict'

import React from 'react'
import {browserHistory} from 'react-router'
import {postReadStatus} from '../../../shared/constants'
import appController from '../../core/appController'
import DateTime from '../core/DateTime'
import PostFlag from './PostFlag'
import PostTitle from './PostTitle'
import UserImage from '../core/UserImage'


/*eslint-disable no-confusing-arrow */
/**
 * Private post label.
 * @param {Object} props
 * @param {Boolean} props.isPrivate
 * @constructor
 */
const PostPrivate = (props) => props.isPrivate ?
	<span className="txt-color-red post-private-label">*Private</span> :
	null

/*eslint-enable no-confusing-arrow */

PostPrivate.propTypes = {
	isPrivate: React.PropTypes.bool
}

PostPrivate.defaultProps = {
	isPrivate: false
}

/**
 * Note header component.
 * @param {Object} props
 * @param {Object} props.createdBy
 * @param {Object} props.flag
 * @param {Function} props.updateFlagDueDate
 * @param {string} props.createdDateTime
 * @param {string} props.readStatus
 * @param {string} props.typeId
 * @constructor
 */
const PostHeader = (props) => {
	let readStatusClass = props.readStatus === postReadStatus.READ ? 'readed' : 'unreaded'

	const redirectTo = () => {
		if (props.createdBy.id && props.createdBy.id === appController.user.id) {
			browserHistory.push(`/boards/me`)
		} else if (props.createdBy.id) {
			browserHistory.push(`/boards/employee/${props.createdBy.id}`)
		}
	}

	return <div className={`who clearfix ${readStatusClass}`}>
		<div className="post-avatar-wrapper clickable" onClick={redirectTo}>
			<UserImage imageUrl={props.createdBy.imageUrl} fullName={props.createdBy.fullName} />
		</div>
		<PostTitle createdBy={props.createdBy} typeId={props.typeId} />
		<PostPrivate isPrivate={props.private} />
		<PostFlag updateFlagDueDate={props.updateFlagDueDate} flag={props.flag} />
		<DateTime createdDateTime={props.createdDateTime} />
	</div>
}

PostHeader.propTypes = {
	createdBy: React.PropTypes.object,
	flag: React.PropTypes.object,
	createdDateTime: React.PropTypes.string,
	readStatus: React.PropTypes.string,
	typeId: React.PropTypes.string,
	private: React.PropTypes.bool,
	updateFlagDueDate: React.PropTypes.func
}

PostHeader.defaultProps = {
	createdBy: {},
	flag: {},
	createdDateTime: '',
	readStatus: postReadStatus.UNREAD,
	typeId: '',
	private: false,
	changeFlagStatus: () => null
}

export default PostHeader
