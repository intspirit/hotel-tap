'use strict'

import React from 'react'
import appController from '../../core/appController'
import {browserHistory} from 'react-router'

/**
 * Post (note, task, ...) title component.
 * @param {Object} props
 * @param {Object} props.createdBy
 * @param {string} props.typeId
 * @constructor
 */
const PostTitle = (props) => {
	const redirectTo = () => {
		if (props.createdBy.id && props.createdBy.id === appController.user.id) {
			browserHistory.push(`/boards/me`)
		} else if (props.createdBy.id) {
			browserHistory.push(`/boards/employee/${props.createdBy.id}`)
		}
	}

	return <span className="name">
		<b onClick={redirectTo}>{props.createdBy.fullName}</b> added a {props.typeId}
	</span>
}

PostTitle.propTypes = {
	createdBy: React.PropTypes.object,
	typeId: React.PropTypes.string
}

PostTitle.defaultProps = {
	createdBy: {},
	typeId: ''
}

export default PostTitle
