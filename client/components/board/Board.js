'use strict'

import React from 'react'

import Posts from './Posts'
import ContentTitle from '../core/ContentTitle'
import PostEntry from '../post/PostEntry'
import appController from '../../core/appController'
import {utils} from '../../../shared/core'

/**
 * Board component.
 */
class Board extends React.Component {

	static propTypes = {
		assignment: React.PropTypes.string,
		id: React.PropTypes.number,
		children: React.PropTypes.node
	}

	/**
	 * Initializes a new instance of the Feed class.
	 * @param {Object} props
	 * @param {int|null} props.id
	 */
	constructor(props) {
		super(props)
		this.state = {
			boardName: '',
			boardIconUrl: '',
			boardType: '',
			loaded: false
		}
		appController.feedComponent = this
	}

	postsLoaded(boardData) {
		this.setState({
			boardName: boardData.boardName,
			boardIconUrl: boardData.boardIconUrl,
			boardType: boardData.boardType,
			loaded: true
		})
	}

	postsLoading() {
		this.setState({
			loaded: false
		})
	}

	render() {
		const postsContainerClass = `col-xs-12 col-sm-12 ${this.props.children ? 'col-md-8 col-lg-8' :
																					'col-md-12 col-lg-12'}`

		return (
			<div className="col-sm-12">
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ContentTitle
						title={this.state.boardName}
						type={this.state.boardType}
						icon={this.state.boardIconUrl}
						loaded={this.state.loaded}
					/>
				</div>
				{this.props.children ?
					<div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 pull-right">{this.props.children}</div> :
					null}
				<article className={postsContainerClass}>
					<div className="feed panel">
						<div className="panel-body status">
							<PostEntry board={utils.assignBoard(this.props.assignment, this.props.id)} />
							{this.props.id ?
								<Posts {...this.props}
									postsLoaded={this.postsLoaded.bind(this)}
									postsLoading={this.postsLoading.bind(this)}
								/> :
								null}
						</div>
					</div>
				</article>
			</div>
		)
	}
}

export default Board
