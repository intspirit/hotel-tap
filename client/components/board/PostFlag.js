'use strict'

import React from 'react'
import utils from '../../../shared/core/utils'

/**
 * Post flag component.
 * @param {Object} props
 * @param {Object} props.flag
 * @param {Function} props.updateFlagDueDate
 * @constructor
 */
const PostFlag = (props) =>
	<span className="pull-right txt-color-red clickable" onClick={props.updateFlagDueDate}>
		<i className={props.flag.set ? 'fa fa-lg fa-flag-o' : ''}>
			<span className="post-flag-date">
				{props.flag.expirationDateTime && utils.formatDateTime(props.flag.expirationDateTime, 'UTC', true)}
			</span>
		</i>
	</span>

PostFlag.propTypes = {
	flag: React.PropTypes.object,
	updateFlagDueDate: React.PropTypes.func
}

PostFlag.defaultProps = {
	flag: {},
	updateFlagDueDate: () => null
}

export default PostFlag
