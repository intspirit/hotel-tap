'use strict'

import React from 'react'
import Log from 'loglevel'
import appController from '../../core/appController'
import postService from '../../../shared/services/postService'
import Attachments from '../core/attachments/Attachments.js'
import Comments from '../core/comments/Comments.js'
import Description from '../core/Description.js'
import PostHeader from './PostHeader.js'
import PostUsers from '../core/PostUsers'
import PostTags from '../core/PostTags'
import PostActions from './PostActions.js'
import PostComplaint from '../core/PostComplaint'
import Readers from '../core/Readers.js'
import Subject from '../core/Subject'
import TaskInfo from '../core/tasks/TaskInfo'

/**
 * Task component.
 *
 * @param {Object} props
 * @param {Object} props.task
 */
class Task extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			editMode: false
		}
	}

	showEditTask() {
		this.setState({
			editMode: true
		})
	}

	closeEditTask() {
		this.setState({
			editMode: false
		})
	}

	async changeDueDateTime(dueDateTime) {
		const task = this.props.task
		const taskId = task.id

		try {
			const serviceResponse = await postService.update(taskId, {dueDateTime: dueDateTime})

			if (serviceResponse.dueDateTime === dueDateTime) {
				this.closeEditTask()
				appController.taskChanged(taskId)
				appController.refreshTodos()

			} else {
				throw new Error('Due date was not updated')
			}
		} catch (error) {
			Log.error(`Task|changeDueDateTime|${error}`)
		}
	}

	render() {
		return <div className={`panel panel-default post-task ${this.props.completion.status}`}>
			<PostHeader {...this.props.task} readStatus={this.props.readStatus} flag={this.props.flag}/>
			<Subject
				subject={this.props.taskDescription.subject}
				status={this.props.completion.status}
				changeStatus={this.props.changeCompletionStatus}
			/>
			<PostTags tags={this.props.task.tags}/>
			<PostUsers users={this.props.task.ccUsers}/>
			<PostComplaint
				complaint={this.props.task.guestComplaint}
				complaintResolution={this.props.complaintResolution}
				resolve={this.props.resolveComplaint}
				cancel={this.props.cancelComplaintResolution}
				onChange={this.props.onComplaintResolutionChange}
			/>
			<Description post={this.props.task} />
			<TaskInfo
				assignmentTo={this.props.assignmentTo}
				dueDateTime={this.props.taskDescription.taskDueDateTime}
				completion={this.props.completion}
				changeDueDateTime={::this.changeDueDateTime}
				cancelEditTask={::this.closeEditTask}
				editMode={this.state.editMode}
			/>
			<Attachments attachments={this.props.task.attachments} />
			<PostActions
				{...this.props.task}
				completion={this.props.completion}
				showCommentsEntry={this.props.showCommentsEntry}
				changeReadStatus={this.props.changeReadStatus}
				changeFlagStatus={this.props.changeFlagStatus}
				commentEntry={this.props.commentEntry}
				readStatus={this.props.readStatus}
				flag={this.props.flag}
				editMode={this.state.editMode}
				onEditAction={::this.showEditTask}
			/>
			<div className="task-footer">
			<Readers typeId={this.props.task.typeId} readBy={this.props.readers}/>
				<Comments
					{...this.props.task}
					commentEntry={this.props.commentEntry}
					showCommentsEntry={this.props.showCommentsEntry}
				/>
			</div>
		</div>
	}
}

Task.propTypes = {
	assignmentTo: React.PropTypes.object,
	completion: React.PropTypes.object,
	task: React.PropTypes.object.isRequired,
	readers: React.PropTypes.array,
	readStatus: React.PropTypes.string,
	commentEntry: React.PropTypes.bool,
	flag: React.PropTypes.object,
	taskDescription: React.PropTypes.object,
	complaintResolution: React.PropTypes.object,
	changeReadStatus: React.PropTypes.func,
	showCommentsEntry: React.PropTypes.func,
	changeFlagStatus: React.PropTypes.func,
	changeCompletionStatus: React.PropTypes.func,
	resolveComplaint: React.PropTypes.func,
	cancelComplaintResolution: React.PropTypes.func,
	onComplaintResolutionChange: React.PropTypes.func
}

Task.defaultProps = {
	task: {}
}

export default Task
