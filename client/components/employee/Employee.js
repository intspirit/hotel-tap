'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'
import appController from '../../core/appController'
import Board from '../board/Board'
import constants from '../../../shared/constants'
import TodoList from '../todo/TodoList'

/**
 * Employee component.
 */
class Employee extends React.Component {
	static propTypes = {
		params: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <Row>
			<Board
				assignment={constants.boardTypes.EMPLOYEE}
				id={this.props.params.id ? parseInt(this.props.params.id, 10) : appController.user.id}>
				<TodoList assignment={constants.boardTypes.EMPLOYEE} id={this.props.params.id}/>
			</Board>
		</Row>
	}
}

export default Employee
