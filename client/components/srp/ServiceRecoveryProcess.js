'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'

import Board from '../board/Board'

import appController from '../../core/appController'
import constants from '../../../shared/constants'

class ServiceRecoveryProcess extends React.Component {

	render() {
		return (
			<Row>
				<Board assignment={constants.boardTypes.SRP} id={appController.user.id} />
			</Row>
		)
	}
}

export default ServiceRecoveryProcess
