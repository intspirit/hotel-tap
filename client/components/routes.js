'use strict'

import React from 'react'
import {Route, Redirect, browserHistory} from 'react-router'
import Login from './login/Login'
import DashboardMain from './dashboard/DashboardMain'
import Department from './department/Department'
import Documents from './documents/Documents'
import Employee from './employee/Employee'
import NotesAndTasks from './help/NotesAndTasks'
import ReportsList from './reports/ReportsList'
import PmReport from './reports/PmReport'
import ChecklistReport from './reports/ChecklistReport'
import Tag from './tag/Tag'
import PostView from './post/PostView'
import Layout from './layout/Layout'
import Development from './Development'
import sac from '../../shared/core/appController'
import appController from '../core/appController'
import authController from '../core/authController'
import AdminRoute from './routes/AdminRoute'
import SettingsRoute from './routes/admin/SettingsRoute'
import {PMList, PMNewController, PMController} from './pm'
import ServiceRecoveryProcess from './srp/ServiceRecoveryProcess'
import {InspectionsList, Inspection, AddInspection} from './inspections'
import {closeUserVoiceWidget} from '../functions'

function decide(nextState, replace) {
	if (!sac.authenticated) {
		replace({
			pathname: '/login',
			state: {nextPathname: nextState.location.pathname}
		})

		return
	}

	if (nextState.location.pathname === '/') {
		const url = appController.settings.defaultBoardUrl || '/boards/me'
		replace({
			pathname: sac.authenticated ? url : '/login',
			state: {nextPathname: nextState.location.pathname}
		})
	}
}

function onRouteChange() {
	closeUserVoiceWidget()
}

function logout() {
	authController.logout(true)
	browserHistory.push('/')
}
// IndexRoute
export default <Route>
	<Route path="/" component={Layout} onEnter={decide} onChange={onRouteChange}>

		<AdminRoute path="admin">
			<SettingsRoute path="settings"/>
		</AdminRoute>

		<Route path="/boards/departments" component={Department}/>
		<Route path="/boards/departments/:id" component={Department}/>
		<Route path="/boards/employee/:id" component={Employee}/>
		<Route path="/boards/tags/:id" component={Tag}/>
		<Route path="/boards/me" component={Employee}/>
		<Route path="/boards/srp" component={ServiceRecoveryProcess} />
		<Route path="/reports/pm/:id" component={PmReport}/>
		<Route path="/reports/check-lists/:id" component={ChecklistReport}/>
		<Route path="/reports" component={ReportsList}/>
		<Route path="/posts/:id" component={PostView}/>

		<Route path="dashboard">
			<Route path="main" component={DashboardMain}/>
			<Redirect from="/" to="/main"/>
		</Route>

		<Route path="pm/new" component={PMNewController}/>
		<Route path="pm/:id" component={PMController}/>
		<Route path="pm" component={PMList}/>


		<Route path="documents" component={Documents}/>

		<Route path="help" component={NotesAndTasks}/>

		<Route path="inspections/new" component={AddInspection}/>
		<Route path="inspections/:id" component={Inspection} />
		<Route path="inspections" component={InspectionsList} />

		<Route path="/rundbscripts" component={Development} />

	</Route>

	<Route path="/login" component={Login}/>
	<Route path="/logout" onEnter={logout}/>
</Route>

