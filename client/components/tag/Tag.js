'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'
import Board from '../board/Board'
import constants from '../../../shared/constants'

/**
 * Tag component.
 */
class Tag extends React.Component {
	static propTypes = {
		params: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <Row>
			<Board
				assignment={constants.boardTypes.TAG}
				id={this.props.params.id ? parseInt(this.props.params.id, 10) : 0}
			/>
		</Row>
	}
}

export default Tag
