'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'
import Board from '../board/Board.js'
import TodoList from '../todo/TodoList.js'
import constants from '../../../shared/constants'

/**
 * component.
 */
class Department extends React.Component {
	static propTypes = {
		params: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <Row>
			<Board
				assignment={constants.boardTypes.DEPARTMENT}
				id={parseInt(this.props.params.id, 10)}>
				<TodoList
					assignment={constants.boardTypes.DEPARTMENT}
					id={this.props.params.id}/>

			</Board>
		</Row>
	}
}

export default Department
