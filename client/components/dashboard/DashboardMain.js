'use strict'

import React from 'react'

/**
 * component.
 */
class DashboardMain extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <div>Dashboard Main</div>
	}
}

DashboardMain.propTypes = {}
DashboardMain.defaultProps = {}

export default DashboardMain
