'use strict'

import React from 'react'
import serviceBase from '../../shared/services/serviceBase'
import Loader from './core/loader/Loader'

/**
 * Development component.
 */
class Development extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			text: '',
			inProcess: false
		}
	}

	async componentDidMount() {
		try {
			this.setState({
				text: 'In progress...',
				inProcess: true
			})

			const result = await serviceBase.get(`/dev/rundbscripts`)

			this.setState({
				text: `Completed. Processed ${result.processedCount} items`,
				inProcess: false
			})
		} catch (err) {
			this.setState({
				text: `Failed`,
				inProcess: false
			})
		}
	}

	render() {
		return <div className="col-sm-12">
			<h1>{this.state.text}</h1>
			<Loader visible={this.state.inProcess} />
		</div>
	}
}

export default Development
