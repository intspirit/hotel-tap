'use strict'

import React from 'react'
import ReactDOM from 'react-dom'
import {Router, browserHistory} from 'react-router'
import appController from '../../shared/core/appController'
import routes from './routes'
import localDb from '../core/localDb'
import {identifyUserVoice, logPageViewForGA} from '../functions'
import ReactGA from 'react-ga'
require('es6-shim')


function init() {

	ReactGA.initialize('UA-59782064-1')
	appController.localDb = localDb
	const jwtToken = localDb.getJwt()
	if (jwtToken) {
		appController.jwtToken = jwtToken
		const user = localDb.getUser()
		appController.setUser(user)
		identifyUserVoice(user)
	}

	ReactDOM.render(
		<Router history={browserHistory} onUpdate={logPageViewForGA}>
			{routes}
		</Router>,
		document.getElementById('react')
	)
}

init()
