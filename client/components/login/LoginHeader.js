'use strict'

import React from 'react'

/**
 * Login header component.
 * @constructor
 */
const LoginHeader = () => <header id="header">
	<div id="logo-group">
		<span id="logo">
			<img src="/img/header-logo.png" alt="HotelTap"/>
		</span>
	</div>

	<span id="extr-page-header-space">
		<span className="hidden-mobile hiddex-xs">Need an account?</span>
		<a href="/register" className="btn btn-danger">Create account</a>
	</span>

</header>

export default LoginHeader
