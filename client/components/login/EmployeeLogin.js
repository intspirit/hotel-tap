'use strict'

import React from 'react'
import {browserHistory} from 'react-router'
import Log from 'loglevel'
import authController from '../../core/authController'
import constants from '../../../shared/constants'
import {messages} from '../../../shared/core'


/**
 * component.
 */
class EmployeeLogin extends React.Component {
	static propTypes = {
		setAdminLogin: React.PropTypes.func,
		validate: React.PropTypes.func,
		showError: React.PropTypes.func,
		resetError: React.PropTypes.func
	}

	constructor(props) {
		super(props)
		this.state = {
			hotelId: '',
			username: '',
			password: '',
			remember: true
		}
	}

	onHotelIdChange(event) {
		this.props.resetError()
		this.setState({
			hotelId: event.target.value
		})
	}

	onUsernameChange(event) {
		this.props.resetError()
		this.setState({
			username: event.target.value
		})
	}

	onPasswordChange(event) {
		this.props.resetError()
		this.setState({
			password: event.target.value
		})
	}

	onRememberChange() {
		this.setState({
			remember: !this.state.remember
		})
	}

	async onSubmit(e) {
		e.preventDefault()
		//noinspection Eslint
		const credentials = {
			userType: constants.userType.EMPLOYEE,
			hotelId: this.state.hotelId,
			userName: this.state.username,
			password: this.state.password,
			remember: this.state.remember
		}

		const validationFailed = this.props.validate({
			hotelId: credentials.hotelId,
			userName: credentials.userName,
			password: credentials.password
		})

		if (validationFailed) return

		try {
			await authController.login(credentials)
			browserHistory.push('/')
		} catch (err) {
			Log.error(`Guest|Login|onSubmit|err:${err}`)
			this.props.showError(messages.error.INVALID_LOGIN)
		}
	}

	render() {
		return <div className="well no-padding">
			<form action="index.html" id="login-form" className="smart-form client-form" noValidate>
				<header>
					Sign In
					<a href="#" onClick={this.props.setAdminLogin} className="pull-right">I am Admin</a>
				</header>

				<fieldset>

					<section>
						<label className="label">Hotel ID</label>
						<label className="input">
							<i className="icon-append fa fa-hotel"/>
							<input type="text"
								value={this.state.hotelId}
								onChange={this.onHotelIdChange.bind(this)}/>
							<b className="tooltip tooltip-top-right">
								<i className="fa fa-hotel txt-color-teal"/>
								&nbsp;
								Please enter hotel ID
							</b>
						</label>
					</section>

					<section>
						<label className="label">Username</label>
						<label className="input">
							<i className="icon-append fa fa-user"/>
							<input type="text"
								value={this.state.username}
								onChange={this.onUsernameChange.bind(this)} />
							<b className="tooltip tooltip-top-right">
								<i className="fa fa-user txt-color-teal"/>
								&nbsp;
								Please enter username
							</b>
						</label>
					</section>

					<section>
						<label className="label">Password</label>
						<label className="input"> <i className="icon-append fa fa-lock"/>
							<input type="password"
								value={this.state.password}
								onChange={this.onPasswordChange.bind(this)} />
							<b className="tooltip tooltip-top-right">
								<i className="fa fa-lock txt-color-teal"/>
								&nbsp;
								Enter your password
							</b>
						</label>
					</section>

					<section>
						<label className="checkbox">
							<input type="checkbox"
								checked={this.state.remember}
								onChange={this.onRememberChange.bind(this)} />
							<i/>Stay signed in</label>
					</section>
				</fieldset>
				<footer>
					<button type="submit" className="btn btn-primary" onClick={this.onSubmit.bind(this)}>
						Sign in
					</button>
				</footer>
			</form>

		</div>
	}
}

export default EmployeeLogin
