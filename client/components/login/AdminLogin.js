'use strict'

import React from 'react'
import {Link, browserHistory} from 'react-router'
import Log from 'loglevel'
import authController from '../../core/authController'
import constants from '../../../shared/constants'
import {messages} from '../../../shared/core'

/**
 * Employee login component.
 */
class AdminLogin extends React.Component {
	static propTypes = {
		setEmployeeLogin: React.PropTypes.func,
		validate: React.PropTypes.func,
		showError: React.PropTypes.func,
		resetError: React.PropTypes.func
	}

	constructor(props) {
		super(props)
		this.state = {
			email: '',
			password: '',
			remember: true
		}
	}

	onEmailChange(event) {
		this.props.resetError()
		this.setState({
			email: event.target.value
		})
	}

	onPasswordChange(event) {
		this.props.resetError()
		this.setState({
			password: event.target.value
		})
	}

	onRememberChange() {
		this.setState({
			remember: !this.state.remember
		})
	}


	async onSubmit(e) {
		e.preventDefault()
		//noinspection Eslint
		const credentials = {
			userType: constants.userType.ADMIN,
			userName: this.state.email,
			password: this.state.password,
			remember: this.state.remember
		}

		const validationFailed = this.props.validate({
			userName: credentials.userName,
			password: credentials.password
		})

		if (validationFailed) return

		try {
			await authController.login(credentials)
			browserHistory.push('/')
		} catch (err) {
			Log.error(`LoginAdmin|onSubmit|err:${err}`)
			this.props.showError(messages.error.INVALID_LOGIN)
		}
	}

	render() {
		return <div className="well no-padding">
			<form id="login-form" className="smart-form client-form" noValidate>
				<header>
					Admin Sign In
					<a href="#" onClick={this.props.setEmployeeLogin} className="pull-right">I am employee</a>
				</header>

				<fieldset>
					<section>
						<label className="label">E-mail</label>
						<label className="input">
							<i className="icon-append fa fa-user"/>
							<input type="email"
								value={this.state.email}
								onChange={this.onEmailChange.bind(this)}/>
							<b className="tooltip tooltip-top-right">
								<i className="fa fa-user txt-color-teal"/>
								&nbsp;
								Please enter email
							</b>
						</label>
					</section>

					<section>
						<label className="label">Password</label>
						<label className="input"> <i className="icon-append fa fa-lock"/>
							<input type="password"
								value={this.state.password}
								onChange={this.onPasswordChange.bind(this)}/>
							<b className="tooltip tooltip-top-right">
								<i className="fa fa-lock txt-color-teal"/>
								&nbsp;
								Enter your password
							</b>
						</label>
						<div className="note">
							<Link to="/forgotpassword">Forgot password?</Link>
						</div>
					</section>

					<section>
						<label className="checkbox">
							<input type="checkbox"
								checked={this.state.remember}
								onChange={this.onRememberChange.bind(this)}/>
							<i/>Stay signed in</label>
					</section>
				</fieldset>
				<footer>
					<button type="submit" className="btn btn-primary" onClick={this.onSubmit.bind(this)}>
						Sign in
					</button>
				</footer>
			</form>

		</div>
	}
}

export default AdminLogin
