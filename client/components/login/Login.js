import React from 'react'
import validator from 'validator'
import LoginHeader from './LoginHeader'
import LoginInfo from './LoginInfo'
import EmployeeLogin from './EmployeeLogin'
import AdminLogin from './AdminLogin'
import ValidationError from '../core/ValidationError'
import {messages} from '../../../shared/core'

/**
 * component.
 */
class Login extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			mode: 'employee',
			error: []
		}
	}

	setAdminLogin() {
		this.resetError()
		this.setState({mode: 'admin'})
	}

	setEmployeeLogin() {
		this.resetError()
		this.setState({mode: 'employee'})
	}

	validate(fields) {
		this.resetError()
		let error = false
		Object.values(fields).some(field => {
			if (validator.isEmpty(field)) {
				error = messages.error.NO_CREDENTIALS

				return true
			}

			return false
		})

		if (error) {
			this.showError(error)

			return true
		}

		return false
	}

	showError(errorMessage) {
		this.setState({error: [errorMessage]})
	}

	resetError() {
		this.setState({error: []})
	}

	render() {
		return <div id="extr-page">
			<LoginHeader />
			<div id="main" role="main">
				<div id="content" className="container">
					<div className="row">
						<div className="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
							<LoginInfo />
						</div>
						<div className="col-xs-12 col-sm-12 col-md-5 col-lg-4">
							{this.state.mode === 'employee' ?
								<EmployeeLogin
									setAdminLogin={::this.setAdminLogin}
									showError={::this.showError}
									resetError={::this.resetError}
									validate={::this.validate}/> :
								<AdminLogin
									setEmployeeLogin={::this.setEmployeeLogin}
									showError={::this.showError}
									resetError={::this.resetError}
									validate={::this.validate}/>}
							<ValidationError errorMessages={this.state.error}/>
						</div>
					</div>
				</div>
			</div>
		</div>
	}
}

export default Login
