'use strict'

import React from 'react'
import moment from 'moment-timezone'
import _ from 'lodash'
import {utils} from '../../../shared/core'
import constants from '../../../shared/constants'

/**
 * Inspection Schedule Filter
 */
class InspectionScheduleFilter extends React.Component {
	static propTypes = {
		isVisible: React.PropTypes.bool,
		onFilterChanged: React.PropTypes.func,
		employees: React.PropTypes.array,
		rooms: React.PropTypes.array
	}

	static defaultProps = {
		onFilterChanged: () => null,
		employees: [],
		rooms: []
	}

	constructor(props) {
		super(props)

		this.years = _.map(_.range(3), number => moment().add(number, 'y').
													format('Y'))
		this.months = _.map(_.range(12), index => moment().month(index).
													format('MMMM'))

		this.state = {
			filters: this.getDefaultFilters(),
			isVisible: props.isVisible
		}
	}

	getDefaultFilters() {
		return {
			dueYear: moment().format('Y'),
			dueMonth: moment().format('MMMM'),
			employeeId: -1,
			supervisorId: -1,
			roomId: -1,
			completionStatus: -1,
			inspectionResult: -1
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({isVisible: nextProps.isVisible})
	}

	onFilterChange(newFilters, reset) {
		const outFilters = reset ? {} : _.pickBy(newFilters, f => f !== -1)
		if (outFilters.hasOwnProperty('completionStatus')) {
			outFilters.completionStatus = outFilters.completionStatus ?
											constants.taskStatus.DONE :
											constants.taskStatus.OPEN
		}
		if (outFilters.hasOwnProperty('inspectionResult')) {
			outFilters.inspectionResult = Boolean(outFilters.inspectionResult)
		}

		this.setState({filters: newFilters},
			() => this.props.onFilterChanged(outFilters))
	}

	onYearChange(event) {
		this.onFilterChange({...this.state.filters, dueYear: parseInt(event.target.value, 10)})
	}

	onMonthChange(event) {
		this.onFilterChange({...this.state.filters, dueMonth: event.target.value})
	}

	onEmployeeChange(event) {
		this.onFilterChange({...this.state.filters, employeeId: parseInt(event.target.value, 10)})
	}

	onSupervisorChange(event) {
		this.onFilterChange({...this.state.filters, supervisorId: parseInt(event.target.value, 10)})
	}

	onRoomChange(event) {
		this.onFilterChange({...this.state.filters, roomId: parseInt(event.target.value, 10)})
	}

	onCompletionStatusChange(event) {
		this.onFilterChange({...this.state.filters, completionStatus: parseInt(event.target.value, 10)})
	}

	onInspectionResultChange(event) {
		this.onFilterChange({...this.state.filters, inspectionResult: parseInt(event.target.value, 10)})
	}

	reset() {
		this.onFilterChange(this.getDefaultFilters(), true)
	}

	render() {
		return this.state.isVisible ?
			<tr className="filterRow info">
				<td>
					<div className="row">
						<section className="col col-6">
							<label className="select">
								<select id="filterYear"
										value={this.state.filters.dueYear}
										onChange={this.onYearChange.bind(this)}>
									{this.years.map(year => <option key={year} value={year}>{year}</option>)}
								</select>
								<i/>
							</label>
						</section>
						<section className="col col-6">
							<label className="select">
								<select id="filterMonth"
										value={this.months[this.state.filters.dueMonth]}
										onChange={this.onMonthChange.bind(this)}>
									{this.months.map(month => <option key={month} value={month}>{month}</option>)}
								</select>
								<i/>
							</label>
						</section>
					</div>
				</td>
				<td>
					<label className="select">
						<select id="filterEmployeeId"
								value={this.state.filters.employeeId}
								onChange={this.onEmployeeChange.bind(this)}>
							<option value="-1" >Employee</option>
							{this.props.employees.map(employee =>
								<option value={employee.id} key={employee.id}>{utils.getFullName(employee)}</option>)}
						</select>
						<i/>
					</label>
				</td>
				<td>
					<label className="select">
						<select id="filterSupervisorId"
								value={this.state.filters.supervisorId}
								onChange={this.onSupervisorChange.bind(this)}>
							<option value="-1">Supervisor</option>
							{this.props.employees.map(supervisor =>
								<option value={supervisor.id} key={supervisor.id}>
									{utils.getFullName(supervisor)}
								</option>)}
						</select>
						<i/>
					</label>
				</td>
				<td>
					<label className="select">
						<select id="filterRoomId"
								value={this.state.filters.roomId}
								onChange={this.onRoomChange.bind(this)}>
							<option value="-1">Location</option>
							{this.props.rooms.map(room =>
								<option value={room.id} key={room.id}>{room.name}</option>)}
						</select>
						<i/>
					</label>
				</td>
				<td>
					<label className="select">
						<select id="filterStatus"
								value={this.state.filters.completionStatus}
								onChange={this.onCompletionStatusChange.bind(this)}>
							<option key="-1" value="-1">Status</option>
							<option key="0" value="0">Assigned</option>
							<option key="1" value="1">Completed</option>
						</select>
						<i/>
					</label>
				</td>
				<td/>
				<td>
					<label className="select">
						<select id="filterResult"
								value={this.state.filters.inspectionResult}
								onChange={this.onInspectionResultChange.bind(this)}>
							<option key="-1" value="-1">Result</option>
							<option key="0" value="0">Failed</option>
							<option key="1" value="1">Passed</option>
						</select>
						<i/>
					</label>
				</td>
				<td width="10%">
					<button
						type="button"
						className="btn btn-default btn-sm"
						onClick={this.reset.bind(this)}>
							Clear Filter
					</button>
				</td>
			</tr> :
			null
	}
}

export default InspectionScheduleFilter
