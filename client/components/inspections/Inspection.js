'use strict'

import React from 'react'
import Promise from 'bluebird'
import update from 'react-addons-update'
import validator from 'validator'
import Log from 'loglevel'
import {browserHistory} from 'react-router'

import ContentTitle from '../core/ContentTitle'
import {NameDescription, ValidationError} from '../core'
import InspectionTasks from './InspectionTasks'
import InspectionSchedules from './InspectionSchedules'
import {inspectionService} from '../../../shared/services'
import {db, messages} from '../../../shared/core'
import {boardTypes, inspectionPassingScore} from '../../../shared/constants'
import appController from '../../core/appController'

/**
 * A view header for an inspection
 * @constructor
 */
const InspectionViewHeader = () =>
	<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ContentTitle title="Inspection" type={boardTypes.INSPECTIONS}/>
	</div>

/**
 * Inspection component
 */
class Inspection extends React.Component {
	static propTypes = {
		routeParams: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {
			isLoaded: false,
			schedules: []
		}

		this.inspectionChanged = false
		this.tasksChanged = false
		this.schedulesChanged = false
	}

	async componentDidMount() {
		this.getData(this.props.routeParams.id)
	}

	async getData(inspectionId) {
		const [inspection, {schedules}, employees] =
			await Promise.all([
				inspectionService.getById(inspectionId),
				inspectionService.getSchedules(inspectionId),
				db.getUsers()
			])

		this.setState({
			isLoaded: true,
			inspection: inspection,
			employees: employees,
			rooms: inspection.rooms,
			schedules: schedules
		})
	}

	onNameDescriptionChanged(name, description) {
		const newInspection = update(this.state.inspection, {name: {$set: name}, description: {$set: description}})
		this.inspectionChanged = true
		this.setState({inspection: newInspection})
	}

	onPassingScoreChange(passingScore) {
		const newInspection = update(this.state.inspection, {inspectionPoints: {$set: passingScore}})
		this.inspectionChanged = true
		this.setState({inspection: newInspection})
	}

	onTasksChange(tasks) {
		this.tasksChanged = true
		const state = update(this.state, {inspection: {tasks: {$set: tasks}}})
		this.setState({inspection: state.inspection})
	}

	onSchedulesChange(schedules) {
		this.schedulesChanged = true
		this.setState({schedules})
	}

	async onSaveTemplate() {
		const errorMessages = this.validate()

		if (errorMessages.length) {
			this.setState({errorMessages: errorMessages})
		} else if (this.inspectionChanged || this.tasksChanged || this.schedulesChanged) {
			const saveResult = await this.saveChanges()
			if (saveResult) {
				browserHistory.push(`/inspections`)
			} else {
				this.setState({errorMessages: ['Inspection was not updated. Something went wrong']})
			}
		}
	}

	async saveChanges() {
		let request = {
			id: this.state.inspection.id
		}

		if (this.inspectionChanged) {
			request.name = this.state.inspection.name
			request.description = this.state.inspection.description
			request.inspectionPoints = this.state.inspection.inspectionPoints
		}

		if (this.tasksChanged) {
			request.tasks = this.state.inspection.tasks.filter(task => task.changeStatus)
		}

		if (this.schedulesChanged) {
			request.schedules = this.state.schedules.filter(schedule => schedule.changeStatus)
		}

		let response = null
		try {
			response = await inspectionService.update(this.state.inspection.id, request)
		} catch (error) {
			Log.error(`Inspection|saveChanges|${error}`)
		}

		return response
	}

	validate() {
		const errorMessages = []
		if (validator.isEmpty(this.state.inspection.name.trim())) errorMessages.push(messages.error.NAME_REQUIRED)

		return errorMessages
	}

	renderInspection() {
		return (
			<div className="widget-body no-padding">
				<form className="smart-form">
					<fieldset>
						<NameDescription
							name={this.state.inspection.name}
							description={this.state.inspection.description}
							onChange={this.onNameDescriptionChanged.bind(this)}
							disabled={!appController.user.isAdmin}/>
					</fieldset>
					<InspectionTasks
						tasks={this.state.inspection.tasks}
						passingScore={this.state.inspection.inspectionPoints || inspectionPassingScore.DEFAULT}
						onPassingScoreChange={this.onPassingScoreChange.bind(this)}
						onTasksChange={this.onTasksChange.bind(this)}
						disabled={!appController.user.isAdmin}/>
					<fieldset>
						<button
							type="button"
							className="btn btn-primary btn-sm"
							onClick={this.onSaveTemplate.bind(this)}>
							Save Template
						</button>
					</fieldset>
					<InspectionSchedules
						employees={this.state.employees}
						schedules={this.state.schedules}
						rooms={this.state.rooms}
						onChange={this.onSchedulesChange.bind(this)}/>
					{this.state.errorMessages && this.state.errorMessages.length ?
						<footer>
							<ValidationError errorMessages={this.state.errorMessages} />
						</footer> :
						null}
				</form>
			</div>
		)
	}

	render() {
		return (
			<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<InspectionViewHeader loaded={this.state.isLoaded}/>
				{this.state.isLoaded ? this.renderInspection() : null}
			</div>
		)
	}
}

export default Inspection
