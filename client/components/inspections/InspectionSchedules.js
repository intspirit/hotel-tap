'use strict'

import React from 'react'
import update from 'react-addons-update'
import InspectionScheduleFilter from './InspectionScheduleFilter'
import InspectionSchedule from './InspectionSchedule'
import InspectionScheduleEditor from './InspectionScheduleEditor'
import constants from '../../../shared/constants'
import _ from 'lodash'
import moment from 'moment'

const sortDirections = {
	ASC: 'asc',
	DESC: 'desc'
}

// TODO: alytyuk p2 move to shared/constants if mobile has same functionality
function filterAndSortSchedules(schedules, filters, sortKey, sortDirection) {
	let filteredSchedules = _.slice(schedules)

	if (!_.isEmpty(filters)) {
		const filtersWithoutDate = _.omit(filters, ['dueYear', 'dueMonth'])

		if (filters.dueYear && filters.dueMonth) {
			filteredSchedules = _.filter(filteredSchedules, schedule =>
				moment(schedule.dueDateTime).isSame(moment().year(filters.dueYear).
																month(filters.dueMonth), 'month')
			)
		}

		filteredSchedules = _.filter(filteredSchedules, filtersWithoutDate)
	}

	return _.orderBy(filteredSchedules, [sortKey], [sortDirection])
}

// TODO: alytyuk p2 move to shared/constants if mobile has same table
const sortNames = {
	dueDateTime: 'Date',
	employeeName: 'Employee',
	supervisorName: 'Supervisor',
	roomName: 'Location',
	completionDateTime: 'Status',
	scorePercentage: 'Score, %',
	inspectionResult: 'Result'
}

/**
 * Inspection Schedules Table Header with ability to sort table columns
 * @param {object} props
 * @returns {Component}
 */
const SchedulesTableHeader = props =>
	<thead>
		<tr className="tableHeaderBackground">
			{Object.keys(sortNames).
				map(key =>
					<th
						key={key}
						onClick={() => props.onSortChanged(key)}>
							{sortNames[key]}
							<span className={`${props.sortKey === key ? 'pull-right' : 'hidden'}`}>
								<i className={`fa fa-arrow-${props.sortDirection === 'desc' ? 'down' : 'up'}`} />
							</span>
					</th>)}
			<th>Options</th>
		</tr>
	</thead>

SchedulesTableHeader.propTypes = {
	sortKey: React.PropTypes.string,
	sortDirection: React.PropTypes.string,
	onSortChanged: React.PropTypes.func
}

SchedulesTableHeader.defaultProps = {
	onSortChanged: () => null
}

/**
 * List of inspection schedules
 */
class InspectionSchedules extends React.Component {
	static propTypes = {
		schedules: React.PropTypes.array,
		employees: React.PropTypes.array,
		rooms: React.PropTypes.array,
		onChange: React.PropTypes.func
	}

	static defaultProps = {
		onChange: () => null,
		schedules: []
	}

	constructor(props) {
		super(props)

		this.state = {
			filters: {},
			sortKey: '',
			sortDirection: sortDirections.ASC,
			isFilterVisible: false
		}

		this.newScheduleId = -1
	}

	toggleFilter() {
		this.setState({isFilterVisible: !this.state.isFilterVisible})
	}

	onFilterChanged(newFilters) {
		this.setState({filters: newFilters})
	}

	onSortChanged(key) {
		this.setState({
			sortKey: key,
			sortDirection: this.state.sortDirection === sortDirections.ASC && this.state.sortKey === key ?
								sortDirections.DESC :
								sortDirections.ASC
		})
	}

	onScheduleAdded(schedule) {
		const newSchedule = Object.assign({}, schedule, {
			id: this.newScheduleId,
			changeStatus: constants.changeStatus.INSERT
		})

		this.newScheduleId -= 1
		this.props.onChange(update(this.props.schedules, {$push: [newSchedule]}))
	}

	onScheduleChanged(id, scheduleChanges) {
		const scheduleIndex = _.findIndex(this.props.schedules, schedule => schedule.id === id)
		if (scheduleIndex !== -1) {
			const newChangeStatus = this.props.schedules[scheduleIndex].changeStatus === constants.changeStatus.INSERT ?
				constants.changeStatus.INSERT :
				constants.changeStatus.UPDATE
			const updatedSchedule = Object.assign({}, scheduleChanges, {changeStatus: newChangeStatus})
			this.props.onChange(update(this.props.schedules, {[scheduleIndex]: {$merge: updatedSchedule}}))
		}
	}

	render() {
		const schedules = filterAndSortSchedules(this.props.schedules, this.state.filters, this.state.sortKey,
													this.state.sortDirection)

		return (
			<fieldset>
				<h3>
					Inspections
					<span
						className="btn btn-link pull-right"
						onClick={this.toggleFilter.bind(this)}>
							{this.state.isFilterVisible ? 'Hide Filter' : 'Show Filter'}
					</span>
				</h3>
				<section>
					<div className="table-responsive form-group">
						<table className="table table-bordered whiteBackground">
							<SchedulesTableHeader
								sortKey={this.state.sortKey}
								sortDirection={this.state.sortDirection}
								onSortChanged={this.onSortChanged.bind(this)}/>
							<tbody>
								<InspectionScheduleFilter
									isVisible={this.state.isFilterVisible}
									onFilterChanged={this.onFilterChanged.bind(this)}
									employees={this.props.employees}
									rooms={this.props.rooms}/>
								<InspectionScheduleEditor
									new={true}
									onSave={this.onScheduleAdded.bind(this)}
									employees={this.props.employees}
									rooms={this.props.rooms}/>
								{schedules.map(schedule =>
									<InspectionSchedule
										key={schedule.id}
										schedule={schedule}
										onChange={this.onScheduleChanged.bind(this)}
										employees={this.props.employees}
										rooms={this.props.rooms}/>
								)}
							</tbody>
						</table>
					</div>
				</section>
			</fieldset>
		)
	}
}

export default InspectionSchedules


