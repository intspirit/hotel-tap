'use strict'

import React from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import {utils} from '../../../shared/core'

/**
 * Inspection Schedule Edit form
 */
class InspectionScheduleEditor extends React.Component {
	static propTypes = {
		schedule: React.PropTypes.object,
		onSave: React.PropTypes.func,
		onCancel: React.PropTypes.func,
		employees: React.PropTypes.array,
		rooms: React.PropTypes.array,
		new: React.PropTypes.bool
	}

	static defaultProps = {
		onScheduleSave: () => null,
		onScheduleCancel: () => null,
		employees: [],
		rooms: [],
		schedule: {
			id: -1,
			employeeId: -1,
			employeeName: '',
			supervisorId: -1,
			supervisorName: '',
			roomId: -1,
			room: '',
			dueDateTime: null
		},
		new: false
	}

	constructor(props) {
		super(props)
		this.state = this.getStateFromProps(this.props)
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.getStateFromProps(nextProps))
	}

	getStateFromProps(props) {
		return {
			id: props.schedule.id,
			dateMoment: props.schedule.dueDateTime ? moment(props.schedule.dueDateTime, 'YYYY-MM-DD') : moment(),
			employeeId: props.schedule.employeeId,
			employeeName: props.schedule.employeeName,
			supervisorId: props.schedule.supervisorId,
			supervisorName: props.schedule.supervisorName,
			roomId: props.schedule.roomId,
			room: props.schedule.room,
			errors: {}
		}
	}

	onDateMomentChange(date) {
		this.setState({
			dateMoment: date,
			errors: {...this.state.errors, date: false}
		})
	}

	onEmployeeChange(event) {
		this.setState({
			employeeId: parseInt(event.target.value, 10),
			employeeName: event.target.selectedOptions[0].label,
			errors: {...this.state.errors, employeeId: false}
		})
	}

	onSupervisorChange(event) {
		this.setState({
			supervisorId: parseInt(event.target.value, 10),
			supervisorName: event.target.selectedOptions[0].label,
			errors: {...this.state.errors, supervisorId: false}
		})
	}

	onRoomChange(event) {
		this.setState({
			roomId: parseInt(event.target.value, 10),
			room: event.target.selectedOptions[0].label,
			errors: {...this.state.errors, roomId: false}
		})
	}

	validate() {
		let isValid = true
		let errors = {}

		if (!this.state.roomId || this.state.roomId === -1) {
			errors.roomId = true
			isValid = false
		}

		if (!this.state.employeeId || this.state.employeeId === -1) {
			errors.employeeId = true
			isValid = false
		}

		if (!this.state.supervisorId || this.state.supervisorId === -1) {
			errors.supervisorId = true
			isValid = false
		}

		return isValid ? null : errors
	}

	cancel() {
		this.props.onCancel()
		this.setState(this.getStateFromProps(this.props))
	}

	save() {
		const errors = this.validate()
		if (errors) {
			this.setState({errors})
		} else {
			this.props.onSave({
				id: this.state.id,
				dueDateTime: this.state.dateMoment.format('YYYY-MM-DD'),
				employeeId: this.state.employeeId,
				employeeName: this.state.employeeName,
				supervisorId: this.state.supervisorId,
				supervisorName: this.state.supervisorName,
				roomId: this.state.roomId,
				roomName: this.state.room
			})
			this.setState(this.getStateFromProps(this.props))
		}
	}

	render() {
		return (
			<tr className="newRow" key={this.state.id}>
				<td>
					<label className="input">
						<DatePicker
							selected={this.state.dateMoment}
							onChange={this.onDateMomentChange.bind(this)}/>
					</label>
				</td>
				<td>
					<label className={`select ${this.state.errors.employeeId ? 'state-error' : ''}`}>
						<select
							value={this.state.employeeId}
							onChange={this.onEmployeeChange.bind(this)}>
							<option value="-1" disabled="disabled">Employee</option>
							{this.props.employees.map(employee =>
								<option value={employee.id} key={employee.id}>{utils.getFullName(employee)}</option>)}
						</select>
						<i/>
					</label>
				</td>
				<td>
					<label className={`select ${this.state.errors.supervisorId ? 'state-error' : ''}`}>
						<select
							value={this.state.supervisorId}
							onChange={this.onSupervisorChange.bind(this)}>
							<option value="-1" disabled="disabled">Supervisor</option>
							{this.props.employees.map(supervisor =>
								<option value={supervisor.id} key={supervisor.id}>
									{utils.getFullName(supervisor)}</option>)
							}
						</select>
						<i/>
					</label>
				</td>
				<td>
					<label className={`select ${this.state.errors.roomId ? 'state-error' : ''}`}>
						<select
							value={this.state.roomId}
							onChange={this.onRoomChange.bind(this)}>
							<option value="-1" disabled="disabled">Location</option>
							{this.props.rooms.map(room => <option value={room.id} key={room.id}>{room.name}</option>)}
						</select>
						<i/>
					</label>
				</td>
				<td/>
				<td/>
				<td/>
				<td width="10%">
					<button type="button" className="btn btn-link" onClick={this.save.bind(this)}>
						{this.props.new ? 'Add' : 'Save'}
					</button>
					{this.props.new ?
						null :
						<button type="button" className="btn btn-link" onClick={this.cancel.bind(this)}>
							Cancel
						</button>}
				</td>
			</tr>
		)
	}
}

export default InspectionScheduleEditor
