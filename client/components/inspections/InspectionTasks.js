'use strict'

import React from 'react'
import update from 'react-addons-update'
import validator from 'validator'
import _ from 'lodash'
import constants from '../../../shared/constants'
import InspectionTask from './InspectionTask'
import InspectionTaskNew from './InspectionTaskNew'

// TODO: alytyuk p2 seems should be moved to shared functions if mobile part has same functionality
function calculateInspectionPoints(tasks, passingScore) {
	let totalPoints = 0
	let passingPoints = 0

	if (tasks) {
		totalPoints = _.sumBy(tasks, task => {
			let points = 0
			if (task.changeStatus !== constants.changeStatus.DELETE) {
				points = parseInt(task.inspectionPoints, 10)
			}

			return points
		})
		passingPoints = totalPoints * passingScore / 100
	}

	return {totalPoints, passingPoints}
}

/**
 * List of inspection tasks
 */
class InspectionTasks extends React.Component {
	static propTypes = {
		tasks: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		passingScore: React.PropTypes.number,
		onPassingScoreChange: React.PropTypes.func,
		onTasksChange: React.PropTypes.func
	}

	static defaultProps = {
		tasks: [],
		passingScore: constants.inspectionPassingScore.DEFAULT,
		disabled: false,
		onPassingScoreChange: () => null,
		onTasksChange: () => null
	}

	constructor(props) {
		super(props)

		const passingScore = this.props.passingScore
		const points = calculateInspectionPoints(this.props.tasks, passingScore)
		this.state = {
			tasks: props.tasks,
			totalPoints: points.totalPoints,
			passingPoints: points.passingPoints,
			passingScore: passingScore
		}

		this.newTaskId = -1
		this.newTaskOrder = props.tasks.length > 0 ? Math.max(...props.tasks.map(task => task.sortOrder)) : 1
	}

	onTaskAdded(task) {
		if (!validator.isEmpty(task.subject.trim())) {
			const newTask = Object.assign({}, task, {
				id: this.newTaskId,
				sortOrder: this.newTaskOrder,
				changeStatus: constants.changeStatus.INSERT
			})

			this.newTaskId -= 1
			this.newTaskOrder += 1

			this.updateTasks(update(this.state.tasks, {$push: [newTask]}))
		}
	}

	onTaskChanged(id, taskChanges) {
		if (!this.props.disabled) {
			const taskIndex = _.findIndex(this.state.tasks, task => task.id === id)
			if (taskIndex !== -1) {
				const newChangeStatus = this.state.tasks[taskIndex].changeStatus === constants.changeStatus.INSERT ?
					constants.changeStatus.INSERT :
					constants.changeStatus.UPDATE


				const taskUpdate = Object.assign({}, taskChanges, {
					changeStatus: newChangeStatus
				})

				this.updateTasks(update(this.state.tasks, {[taskIndex]: {$merge: taskUpdate}}))
			}
		}
	}

	onTaskDelete(id) {
		if (!this.props.disabled) {
			const taskIndex = this.state.tasks.findIndex(x => x.id === id)
			if (taskIndex !== -1) {
				const updatedTasks =
					this.state.tasks[taskIndex].changeStatus === constants.changeStatus.INSERT ?
						update(this.state.tasks, {$splice: [[taskIndex, 1]]}) :
						update(this.state.tasks, {$splice: [[taskIndex, 1,
							update(this.state.tasks[taskIndex],
									{changeStatus: {$set: constants.changeStatus.DELETE}})]]})

				this.updateTasks(updatedTasks)
			}
		}
	}

	updateTasks(tasks) {
		const points = calculateInspectionPoints(tasks, this.state.passingScore)
		this.props.onTasksChange(tasks)
		this.setState({
			tasks: tasks,
			totalPoints: points.totalPoints,
			passingPoints: points.passingPoints
		})
	}

	onPassingScoreChange(e) {
		let passingScore = parseInt(e.target.value, 10)
		if (passingScore < constants.inspectionPassingScore.MIN) {
			passingScore = constants.inspectionPassingScore.MIN
		} else if (passingScore > constants.inspectionPassingScore.MAX) {
			passingScore = constants.inspectionPassingScore.MAX
		}
		this.setState({
			passingPoints: this.state.totalPoints * passingScore / 100,
			passingScore: passingScore
		})
	}

	onPassingScoreBlur() {
		this.props.onPassingScoreChange(this.state.passingScore)
	}

	renderTotalScoreRow() {
		return (
			<tr key="totalScoreRow" className="newRow">
				<td>
					<span className="pull-right">TOTAL:</span>
				</td>
				<td colSpan="3">
					<span>{this.state.totalPoints}</span>
				</td>
			</tr>
		)
	}

	renderPassingScoreRow() {
		return (
			<tr key="passingScoreRow" className="newRow">
				<td>
					<span className="pull-right">PASSING SCORE:</span>
				</td>
				<td>
					<input
						type="number"
						min={constants.inspectionPassingScore.MIN}
						max={constants.inspectionPassingScore.MAX}
						step={constants.inspectionPassingScore.STEP}
						disabled={this.props.disabled}
						onBlur={this.onPassingScoreBlur.bind(this)}
						onChange={this.onPassingScoreChange.bind(this)}
						value={this.state.passingScore}/>{' %'}
				</td>
				<td colSpan="2">
					<span>{`${this.state.passingPoints} points`}</span>
				</td>
			</tr>
		)
	}

	render() {
		return (
			<fieldset>
				<h3>Tasks</h3>
				<section>
					<div className="table-responsive form-group">
						<table className="table table-bordered whiteBackground">
							<thead>
								<tr className="tableHeaderBackground">
									<th>Task</th>
									<th>
										{`Points (${constants.inspectionPoints.MIN} - ${constants.inspectionPoints.MAX})`}
									</th>
									<th>Fail All</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
								{this.state.tasks.map(task =>
									<InspectionTask
										key={task.id}
										task={task}
										disabled={this.props.disabled}
										onChange={this.onTaskChanged.bind(this)}
										onDelete={this.onTaskDelete.bind(this)} />)}
								{this.props.disabled ?
									null :
									<InspectionTaskNew onTaskAdded={this.onTaskAdded.bind(this)}/>}
								{this.renderTotalScoreRow()}
								{this.renderPassingScoreRow()}
							</tbody>
						</table>
					</div>
				</section>
			</fieldset>
		)
	}
}

export default InspectionTasks
