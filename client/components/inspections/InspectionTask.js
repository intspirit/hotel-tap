'use strict'

import React from 'react'
import constants from '../../../shared/constants'

/**
 * Inspection task
 */
class InspectionTask extends React.Component {
	static propTypes = {
		task: React.PropTypes.shape({
			id: React.PropTypes.number,
			subject: React.PropTypes.string,
			inspectionPoints: React.PropTypes.number,
			failsAll: React.PropTypes.bool,
			changeStatus: React.PropTypes.string
		}),
		disabled: React.PropTypes.bool,
		onChange: React.PropTypes.func,
		onDelete: React.PropTypes.func
	}

	static defaultProps = {
		onChange: () => null,
		onDelete: () => null,
		task: {
			subject: '',
			inspectionPoints: constants.inspectionPoints.MIN,
			failsAll: false
		},
		disabled: false
	}

	constructor(props) {
		super(props)
		this.state = this.getStateFromProps(this.props)
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.getStateFromProps(nextProps))
	}

	getStateFromProps(props) {
		const task = props.task

		return {
			subject: task.subject,
			inspectionPoints: task.inspectionPoints,
			failsAll: task.failsAll
		}
	}

	onSubjectChange(e) {
		this.setState({subject: e.target.value})
	}

	onSubjectKeyDown(e) {
		if (e.keyCode === constants.keyboardKey.ENTER) {
			this.props.onChange(this.props.task.id, {subject: this.state.subject.trim()})
		}
	}

	onInspectionPointsChange(e) {
		this.setState({inspectionPoints: e.target.value}, () =>
			this.props.onChange(this.props.task.id, {inspectionPoints: parseInt(this.state.inspectionPoints, 10)}))
	}

	onFailsAllChange(e) {
		this.setState({failsAll: e.target.checked}, () =>
			this.props.onChange(this.props.task.id, {failsAll: this.state.failsAll}))
	}

	onDelete() {
		if (!this.props.disabled) {
			this.props.onDelete(this.props.task.id)
		}
	}

	render() {
		if (this.props.task.changeStatus === constants.changeStatus.DELETE) return null

		return (
			<tr>
				<td>
					<input
						type="text"
						value={this.state.subject}
						onChange={this.onSubjectChange.bind(this)}
						onKeyDown={this.onSubjectKeyDown.bind(this)}
						disabled={this.props.disabled}
						className="transparentInput form-control"/>
				</td>
				<td>
					<input
						type="number"
						min={constants.inspectionPoints.MIN}
						max={constants.inspectionPoints.MAX}
						value={this.state.inspectionPoints}
						onChange={this.onInspectionPointsChange.bind(this)}
						disabled={this.props.disabled}
						className="transparentInput form-control"/>
				</td>
				<td>
					<label className="checkbox">
						<input
							type="checkbox"
							checked={this.state.failsAll}
							onChange={this.onFailsAllChange.bind(this)}
							disabled={this.props.disabled}/>
						<i/>
					</label>
				</td>
				<td className="text-center">
					<a onClick={this.onDelete.bind(this)}>
						<i className="fa fa-trash"/>
					</a>
				</td>
			</tr>
		)
	}
}

export default InspectionTask
