'use strict'

import React from 'react'
import constants from '../../../shared/constants'

/**
 * New Inspection Task form
 */
class InspectionTaskNew extends React.Component {
	static propTypes = {
		id: React.PropTypes.number,
		onTaskAdded: React.PropTypes.func
	}

	static defaultProps = {
		onTaskAdded: () => null
	}

	constructor(props) {
		super(props)
		this.state = Object.assign(this.getDefaultState(), {id: this.props.id})
	}

	getDefaultState() {
		return {
			subject: '',
			inspectionPoints: constants.inspectionPoints.MIN,
			failsAll: false
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({id: nextProps.id})
	}

	onSubjectChange(e) {
		this.setState({subject: e.target.value})
	}

	onInspectionPointsChange(e) {
		this.setState({inspectionPoints: e.target.value})
	}

	onFailsAllChange(e) {
		this.setState({failsAll: e.target.checked})
	}

	save() {
		this.props.onTaskAdded({
			subject: this.state.subject,
			inspectionPoints: parseInt(this.state.inspectionPoints, 10),
			failsAll: this.state.failsAll
		})
		this.setState(this.getDefaultState())
	}

	render() {
		return (
			<tr key={this.state.id}>
				<td>
					<label className="input">
						<input
							type="text"
							value={this.state.subject}
							onChange={this.onSubjectChange.bind(this)}/>
					</label>
				</td>
				<td>
					<label className="input">
						<input
							type="number"
							min={constants.inspectionPoints.MIN}
							max={constants.inspectionPoints.MAX}
							value={this.state.inspectionPoints}
							onChange={this.onInspectionPointsChange.bind(this)}/>
					</label>
				</td>
				<td>
					<label className="checkbox">
						<input
							type="checkbox"
							checked={this.state.failsAll}
							value="failAll"
							onChange={this.onFailsAllChange.bind(this)}/>
						<i/>
					</label>
				</td>
				<td className="text-center">
					<button type="button" className="btn btn-link" onClick={this.save.bind(this)}>Add</button>
				</td>
			</tr>
		)
	}
}

export default InspectionTaskNew
