'use strict'

import React from 'react'
import {Link} from 'react-router'
import ContentTitle from '../core/ContentTitle'
import {inspectionService} from '../../../shared/services'
import {boardTypes} from '../../../shared/constants'

/**
 * View header for a list of inspections
 * @param {Object} props
 * @constructor
 */
const InspectionsViewHeader = props =>
	<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<ContentTitle title="Inspections" type={boardTypes.INSPECTIONS} loaded={props.loaded}>
			<div className="pull-right">
				<Link className="btn btn-primary" to="/inspections/new">Add Inspection</Link>
			</div>
		</ContentTitle>
	</div>

InspectionsViewHeader.propTypes = {
	loaded: React.PropTypes.bool
}

/**
 * An inspection in a list of inspections
 * @param {Object} props
 * @returns {void}
 */
const InspectionListItem = props =>
	<tr>
		<td>{props.inspection.subject}</td>
		<td>
			<Link className="fa fa-edit" to={`/inspections/${props.inspection.id}`} />
		</td>
	</tr>
InspectionListItem.propTypes = {
	inspection: React.PropTypes.shape({
		id: React.PropTypes.number,
		subject: React.PropTypes.string
	})
}

/**
 * A list of inspections
 */
class InspectionsList extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isLoaded: false,
			inspections: []
		}
	}

	async componentDidMount() {
		this.getData()
	}

	async getData() {
		const serviceResponse = await inspectionService.getAll()
		this.setState({
			isLoaded: true,
			inspections: serviceResponse.inspections
		})
	}

	render() {
		return (
			<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<InspectionsViewHeader loaded={this.state.isLoaded} />
				{this.state.isLoaded ?
					<article className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div className="table-responsive">
							<table className="table table-bordered whiteBackground">
								<thead>
									<tr className="tableHeaderBackground">
										<th>Inspection Name</th>
										<th className="col-sm-1">Options</th>
									</tr>
								</thead>
								<tbody>
									{this.state.inspections.map(inspection =>
										<InspectionListItem key={inspection.id} inspection={inspection} />)}
								</tbody>
							</table>
						</div>
					</article> :
					null}
			</div>
		)
	}
}

export default InspectionsList
