'use strict'

import React from 'react'
import {browserHistory, Link} from 'react-router'
import update from 'react-addons-update'
import validator from 'validator'
import Log from 'loglevel'

import {NameDescription, ValidationError} from '../core'
import ContentTitle from '../core/ContentTitle'
import {boardTypes} from '../../../shared/constants'
import {messages} from '../../../shared/core'
import {inspectionService} from '../../../shared/services'

/**
 * A view header for an inspection
 * @constructor
 */
const AddInspectionViewHeader = () =>
	<ContentTitle title="Add Inspection" type={boardTypes.INSPECTIONS}>
		<div className="pull-right">
			<Link className="btn btn-primary" to="/inspections">Back to Inspections</Link>
		</div>
	</ContentTitle>

/**
 * Add inspection form
 */
class AddInspection extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			name: '',
			description: '',
			errorMessages: []
		}
	}

	onNameDescriptionChanged(name, description) {
		this.setState(update(this.state, {name: {$set: name}, description: {$set: description}}))
	}

	validate() {
		let errorMessages = []
		if (validator.isEmpty(this.state.name.trim())) errorMessages.push(messages.error.NAME_REQUIRED)

		return errorMessages
	}

	async save() {
		const errorMessages = this.validate()
		if (errorMessages.length) {
			this.setState({errorMessages: errorMessages})
		} else {
			const saveResult = await this.saveChanges()
			if (saveResult && saveResult.id) {
				browserHistory.push(`/inspections/${saveResult.id}`)
			} else {
				this.setState({errorMessages: ['Inspection was not saved']})
			}
		}
	}

	async saveChanges() {
		let response = null
		try {
			response = await inspectionService.insert({
				name: this.state.name.trim(),
				description: this.state.description.trim()
			})
		} catch (error) {
			Log.error(`AddInspection|saveChanges|${error}`)
		}

		return response
	}

	render() {
		return (
			<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<AddInspectionViewHeader />
				<div className="widget-body no-padding">
					<form className="smart-form">
						{this.state.errorMessages && this.state.errorMessages.length ?
							<fieldset>
								<ValidationError errorMessages={this.state.errorMessages} />
							</fieldset> :
							null}
						<fieldset>
							<NameDescription
								name={this.state.name}
								description={this.state.description}
								onChange={this.onNameDescriptionChanged.bind(this)}/>
						</fieldset>
						<footer>
							<button
								type="button"
								className="btn btn-primary"
								onClick={this.save.bind(this)}>
								Save
							</button>
						</footer>
					</form>
				</div>
			</div>
		)
	}
}

export default AddInspection
