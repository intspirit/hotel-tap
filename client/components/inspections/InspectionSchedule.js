'use strict'

import React from 'react'
import _ from 'lodash'
import InspectionScheduleEditor from './InspectionScheduleEditor'
import {utils} from '../../../shared/core'
import constants from '../../../shared/constants'

/**
 * Inspection Schedule
 */
class InspectionSchedule extends React.Component {
	static propTypes = {
		schedule: React.PropTypes.object,
		onChange: React.PropTypes.func,
		employees: React.PropTypes.array,
		rooms: React.PropTypes.array
	}

	static defaultProps = {
		onChange: () => null
	}

	constructor(props) {
		super(props)

		this.state = {isEditing: false}
		this.setInnerProperties(this.props)

		this.toggleEdit = this.toggleEdit.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		this.setInnerProperties(nextProps)
	}

	setInnerProperties(props) {
		this.employees = _.keyBy(props.employees, 'id')
		this.rooms = _.keyBy(props.rooms, 'id')
	}

	toggleEdit() {
		this.setState({isEditing: !this.state.isEditing})
	}

	onSave(schedule) {
		this.props.onChange(schedule.id, schedule)
		this.toggleEdit()
	}

	renderViewRow() {
		const employee = this.employees[this.props.schedule.employeeId]
		const supervisor = this.employees[this.props.schedule.supervisorId]
		const room = this.rooms[this.props.schedule.roomId] || {name: 'Location deactivated'}

		let completionStatus = 'Assigned'
		let inspectionStatus = ''
		let editBtnTitle = 'Edit'
		if (this.props.schedule.completionStatus === constants.taskStatus.DONE) {
			completionStatus = 'Completed'
			inspectionStatus = this.props.schedule.failsAll ? 'Pass' : 'Fail'
			editBtnTitle = 'View'
		}

		return (
			<tr key={this.props.schedule.id} className="warning">
				<td>{this.props.schedule.dueDateTime}</td>
				<td>{employee ? utils.getFullName(employee) : 'Unassigned'}</td>
				<td>{supervisor ? utils.getFullName(supervisor) : 'Unassigned'}</td>
				<td>{room.name}</td>
				<td>{completionStatus}</td>
				<td>{this.props.schedule.inspectionPoints}</td>
				<td>{inspectionStatus}</td>
				<td><a className="btn btn-link" onClick={this.toggleEdit}>{editBtnTitle}</a></td>
			</tr>
		)
	}

	renderEditRow() {
		return (
			<InspectionScheduleEditor
				key={this.props.schedule.id}
				schedule={this.props.schedule}
				employees={this.props.employees}
				rooms={this.props.rooms}
				onSave={this.onSave.bind(this)}
				onCancel={this.toggleEdit}/>
		)
	}

	render() {
		return this.state.isEditing ? this.renderEditRow() : this.renderViewRow()
	}
}

export default InspectionSchedule
