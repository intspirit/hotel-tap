'use strict'

import React from 'react'

/**
 * component.
 */
class Footer extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <div className="page-footer">
			<div className="row">
				<div className="col-xs-12 col-sm-12">
					<span className="txt-color-white">HotelTap
						<span className="hidden-xs"> - hotel communication, task completion, maintenance</span>
						{` © 2014-${new Date().getFullYear()}`}
					</span>
				</div>
			</div>
		</div>
	}
}

Footer.propTypes = {}
Footer.defaultProps = {}

export default Footer
