'use strict'

import React from 'react'
import Log from 'loglevel'

import appController from '../../../core/appController.js'
import {postService, userService} from '../../../../shared/services/index'
import NavigationOption from './NavigationOption'
import constants from '../../../../shared/constants'
import navigationLogic from '../../../../shared/navigationLogic'


/**
 * Shrinks the navigation menu - shows icons only.
 * @return {Object}
 */
const MenuMinifier = () => <span className="minifyme" data-action="minifyMenu">
	<i className="fa fa-arrow-circle-left hit"/>
</span>


/**
 * component.
 */
class Navigation extends React.Component {
	constructor(props) {
		super(props)
		//noinspection JSUnresolvedVariable
		this.state = {
			options: [],
			unreadCounts: []
		}
		appController.navigationComponent = this
	}

	async getData() {
		try {
			const data = await userService.getNavigation()
			await this.setState({options: data.options})

			this.getUnreadCount()
		} catch (err) {
			Log.error(`Navigation|getData|error:${err}`)
		}
	}

	async getUnreadCount() {
		try {
			const data = await postService.getUnreadCount()
			const optionsWithCounts = navigationLogic.setUnreadCounts(this.state.options, data.counts)
			this.setState({
				options: optionsWithCounts,
				unreadCounts: data.counts
			})
		} catch (err) {
			Log.error(`Navigation|getUnreadCount|error:${err}`)
		}
	}

	disactivateNavigationOptionsUntilUserAction() {
		let state = this.state
		state.isDisactivatedUntilUserAction = true
		this.setState(state)
	}

	componentDidMount() {
		this.getData()
		this.refreshEveryMinute()
	}

	refreshEveryMinute() {
		this.timer = setInterval(this.getUnreadCount.bind(this), constants.interval.ONE_MINUTE)
	}

	componentWillReceiveProps() {
		let state = this.state
		if (state.isDisactivatedUntilUserAction) {
			Reflect.deleteProperty(state, 'isDisactivatedUntilUserAction')
			this.setState(state)
		}
	}

	componentWillUnmount() {
		clearInterval(this.timer)
	}

	onOptionSelected() {
		//TODO: sk p1 some action for selected option.
	}

	render() {
		if (this.state.options.length === 0) return null

		return <aside id="left-panel">
			<nav>
				<ul>
					{this.state.options.map(x => <NavigationOption
						key={x.id}
						option={x}
						isDisactivated={this.state.isDisactivatedUntilUserAction}
						onOptionSelected={this.onOptionSelected.bind(this)}/>
					)}
				</ul>
			</nav>
			<MenuMinifier />
		</aside>
	}
}

export default Navigation
