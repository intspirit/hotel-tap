'use strict'

import React from 'react'
import activeComponent from 'react-router-active-component'

const NavItem = activeComponent('li')

/**
 *  * Navigation option component.
 * component.
 * @param {Object} props
 * @param {boolean} props.isActive
 * @param {number} props.id
 * @param {func} props.onOptionSelected
 * @param {string} props.url
 * @param {string} props.iconUrl
 * @param {string} props.iconClass
 * @param {number} props.count
 * @oaram {boolean} props.isDisactivated removes 'active' ui state
 * @constructor
 * @return {Object}
 */
const NavigationOption = props => <NavItem
	to={props.option.url}
	key={props.option.id}
	onClick={props.onOptionSelected}
	activeClassName={props.isDisactivated ? `` : `active`}>

	{props.option.iconUrl ?
		<i className="fa fa-lg fa-fw custom-icon">
			<div style={{backgroundImage: `url(${props.option.iconUrl})`}} />
		</i> :
		<i className={`fa fa-lg fa-fw ${props.option.iconClass}`}/>
	}

	<span className="menu-item-parent">{props.option.label}</span>

	{props.option.count === 0 ?
		null :
		<span className="badge pull-right inbox-badge margin-right-13">{props.option.count}</span>
	}
</NavItem>


NavigationOption.propTypes = {
	option: React.PropTypes.object,
	onOptionSelected: React.PropTypes.func,
	isDisactivated: React.PropTypes.bool
}

NavigationOption.defaultProps = {
	option: {},
	onOptionSelected: () => null
}

export default NavigationOption
