'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import UserImage from '../../core/UserImage'
import {userImageDimensions} from '../../../../shared/constants'
import appController from '../../../core/appController'
import ProfileSettingsModal from '../../settings/Settings'


/**
 * User menu component.
 */
class UserMenu extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			isProfileModalVisible: false,
			userFullName: appController.user.fullName,
			userImage: appController.user.imageUrl
		}

		this.showProfileModal = this.showProfileModal.bind(this)
		this.hideProfileModal = this.hideProfileModal.bind(this)
	}

	showProfileModal(e) {
		e.preventDefault()
		this.setState({
			isProfileModalVisible: true
		})
	}

	hideProfileModal() {
		this.setState({
			isProfileModalVisible: false,
			userFullName: appController.user.fullName,
			userImage: appController.user.imageUrl
		})
	}

	get tooltip() {
		return <Tooltip id="menu-tooltip"><b>My Profile Settings</b></Tooltip>
	}

	render() {
		if (!appController.user) return null

		return <ul id="mobile-profile-img" className="header-dropdown-list hidden-xs padding-5">
			<OverlayTrigger placement="bottom" overlay={this.tooltip}>
				<li className="">
					<a href="#" className="no-margin userdropdown" onClick={this.showProfileModal}>
						<div className="header-avatar-wrapper">
								<UserImage
									imageUrl={this.state.userImage}
									fullName={this.state.userFullName}
									dimension={userImageDimensions.SMALL}
								/>
						</div>
					</a>
				</li>
			</OverlayTrigger>
			{this.state.isProfileModalVisible &&
				<ProfileSettingsModal
					isVisible={this.state.isProfileModalVisible}
					onHide={this.hideProfileModal}
				/>
			}
		</ul>
	}
}

UserMenu.propTypes = {}
UserMenu.defaultProps = {}

export default UserMenu
