'use strict'

import React from 'react'
import _ from 'lodash'
import userService from '../../../../shared/services/userService'
import localDb from '../../../core/localDb'
import appController from '../../../core/appController'
import Log from 'loglevel'
import pluralize from 'pluralize'

/**
 * Hotel selector component.
 */
class HotelSelector extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			hotels: [],
			activeHotel: {}
		}
	}

	async getData() {
		try {
			const data = await userService.getHotelsFewDetails()
			const activeHotel = this.getActiveHotel(data.hotels)
			localDb.setHotel(activeHotel)
			appController.hotelActivated(activeHotel)

			this.setState({
				hotels: data.hotels,
				activeHotel: activeHotel
			})

		} catch (err) {
			Log.error(`HotelSelector|getData|error:${err}`)
		}
	}

	// @TODO move to global
	getActiveHotel(hotels) {
		//UT: server returns nothing
		//UT: nothing in the localStorage
		//UT: something in the localStorage, and it's in the state.hotels
		//UT: something in the localStorage, but not in the state.hotels
		let hotel = {}
		try {
			hotel = localDb.getHotel() || {}
			//What if hotel is not found in the localStorage
			//What if hotel is found, but user doesn't have access to it anymore - hotel is not in the state.hotels?
			//These are cases for UT.
		} catch (err) {
			Log.error(`HotelSelector|getActiveHotel|error:${err}`)
		}

		hotel = hotels.find(x => x.id === hotel.id) || hotels[0]

		return hotel
	}

	setActiveHotel(hotel) {
		this.setState({
			activeHotel: hotel
		})

		localDb.setHotel(hotel)

		this.props.onHotelSelected(hotel)
	}

	componentDidMount() {
		this.getData()
	}

	get hotelsDropdown() {
		return <span>
			<span className="project-selector dropdown-toggle"
				data-toggle="dropdown"
				aria-expanded="false">
				{this.state.activeHotel.name}
				<i className="fa fa-angle-down"/>
			</span>
			<ul className="dropdown-menu">
				{_.orderBy(this.state.hotels, ['name']).
					filter(x => x.id !== this.state.activeHotel.id).
					map(x => <li key={x.id} onClick={this.setActiveHotel.bind(this, x)}>
							<a href="#">{x.name}</a>
						</li>
					)
				}
			</ul>
		</span>
	}

	get label() {
		return pluralize('Hotel', this.state.hotels.length)
	}

	get hotelSelector() {
		return <span className="project-selector">
			{this.state.activeHotel.name}
		</span>
	}

	render() {
		//UT - noting passed => noting rendered
		if (!this.state.hotels.length) return null

		return <div className="project-context hidden-xs">
			<span className="label">{this.label}</span>
			{this.state.hotels.length > 1 ? this.hotelsDropdown : this.hotelSelector}
		</div>
	}
}

HotelSelector.propTypes = {
	onHotelSelected: React.PropTypes.func
}
HotelSelector.defaultProps = {}

export default HotelSelector
