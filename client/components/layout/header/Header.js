'use strict'

import React from 'react'
import Log from 'loglevel'
import {browserHistory} from 'react-router'

import HotelSelector from './HotelSelector'
import SearchBox from './SearchBox'
import UserMenu from './UserMenu'
import authController from '../../../core/authController'

const Logo = () => <div id="logo-group">
	<span id="logo" onClick={() => browserHistory.push('/boards/me')}>
		<img src="/img/header-logo.png" alt="HotelTap" />
	</span>
</div>


const SupportInfo = () => <div className="pull-right support-info">
	<span className="hidden-xs">
		24/7 SUPPORT 1-844-381-7221
	</span>
	<span className="dropdown visible-xs">
		<span className="dropdown-toggle"
			data-toggle="dropdown"
			aria-expanded="false">
			<i className="fa fa-support"/>
		</span>
		<ul className="dropdown-menu support-info-xs">
			24/7 SUPPORT <a href="tel:1-844-381-7221">1-844-381-7221</a>
		</ul>
	</span>
</div>


const Logout = props => <div id="logout"
							className="btn-header transparent pull-right"
							onClick={props.onLogout}>
	<span>
		<a href="#"
			title="Sign out">
			<i className="fa fa-sign-out"/>
		</a>
	</span>
</div>

Logout.propTypes = {
	onLogout: React.PropTypes.func
}

const NavigationVisibilitySwitcher = () => <div id="hide-menu" className="btn-header pull-right">
	<span>
		<a href="#" data-action="toggleMenu" title="Collapse Menu">
			<i className="fa fa-reorder"/>
		</a>
	</span>
</div>


/**
 * Header component.
 */
class Header extends React.Component {
	static async onHotelSelected(hotel) {
		try {
			const hotelId = hotel.id
			await authController.loginToHotel(hotelId)
			window.location.href = '/'
		} catch (err) {
			Log.error(`Header|onHotelSelected|error:${err}`)
		}
	}

	static onLogout() {
		authController.logout()
		browserHistory.push('/login')
	}

	render() {
		return <header id="header">
			<Logo />
			<HotelSelector onHotelSelected={Header.onHotelSelected.bind(this)}/>
			<div className="pull-right">
				<NavigationVisibilitySwitcher />
				<Logout onLogout={Header.onLogout.bind(this)} />
				<UserMenu />
				<SearchBox />
				<SupportInfo />
			</div>
		</header>
	}
}

Header.propTypes = {}
Header.defaultProps = {}

export default Header
