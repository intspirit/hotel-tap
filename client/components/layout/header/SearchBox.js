'use strict'

import React from 'react'

/**
 * Search box component.
 */
class SearchBox extends React.Component {
	constructor(props) {
		super(props)
		this.state = {message: 'Search Box'}
	}

	render() {
		return <form action="#ajax/search.html"className="header-search pull-right">
			<input id="search-fld" type="text" name="param" placeholder="Search"/>
			<button type="submit">
				<i className="fa fa-search"/>
			</button>
			<a href="#" id="cancel-search-js" title="Cancel Search">
				<i className="fa fa-times"/>
			</a>
		</form>
	}
}

SearchBox.propTypes = {}
SearchBox.defaultProps = {}

export default SearchBox
