'use strict'

import React from 'react'
import Navigation from './navigation/Navigation'
import Header from './header/Header'
import Footer from './Footer'

/**
 * Main layout component.
 */
class Layout extends React.Component {
	static propTypes = {
		children: React.PropTypes.object
	}

	static defaultProps = {
		children: {}
	}

	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return <div>
			<Header />
			<Navigation />
			<div id="main" role="main">
				<div id="content">
					{this.props.children}
				</div>
			</div>
			<Footer />
		</div>
	}
}

export default Layout
