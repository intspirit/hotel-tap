'use strict'

import React, {PropTypes, Component} from 'react'
import _ from 'lodash'
import PMLocationSchedule from './PMLocationSchedule'
import {pmService} from '../../../shared/services'
import constants from '../../../shared/constants'

/**
 * Gets a key based on location and date.
 * @param {int} locationId
 * @param {string} date
 * @return {string}
 */
const getKey = (locationId, date) => `${locationId}-${(new Date(date)).getMonth() + 1}`

/**
 * PM Schedule component.
 */
class PMSchedules extends Component {
	constructor(props) {
		super(props)
		this.state = {
			year: (new Date()).getFullYear(),
			schedules: [],
			completions: [],
			pmStateTimestamp: new Date(),
			canChangeYear: true
		}
		this.newId = -1
	}

	async getData(year = this.state.year) {
		const schedules = await pmService.getSchedule(this.props.id, year)
		const indexedSchedules = _.chain(schedules).
			filter(x => new Date(x.dueDateTime).getFullYear() === year).
			keyBy(x => getKey(x.location.id, x.dueDateTime)).
			value()

		const completionsWithKey = schedules.
			filter(x => x.completion.dateTime).
			map(x => Object.assign(x, {completionKey: getKey(x.location.id, x.completion.dateTime)}))
		const indexedCompletions = _.groupBy(completionsWithKey, 'completionKey')

		this.setState({
			pmStateTimestamp: this.props.stateTimestamp,
			year: year,
			schedules: indexedSchedules,
			completions: indexedCompletions,
			canChangeYear: true
		})
	}

	componentDidMount() {
		const yearNow = new Date().getFullYear()
		this.getData(yearNow)
	}

	onPreviousYearClicked(e) {
		e.preventDefault()
		//TODO: vvs p2 unsaved changes?
		this.getData(this.state.year <= 2014 ? 2014 : this.state.year - 1)
	}

	onNextYearClicked(e) {
		e.preventDefault()
		//TODO: vvs p2 unsaved changes?
		this.getData(this.state.year >= 2020 ? 2020 : this.state.year + 1)
	}

	componentWillReceiveProps(nextProp) {
		if (this.state.pmStateTimestamp === nextProp.stateTimestamp) return

		this.getData()
	}

	/**
	 * Get a proper due date time by day and month - due at the end of the day.
	 * @param {int} month
	 * @param {int} date
	 * @return {Date}
	 */
	getDueDateTime(month, date) {
		return new Date(this.state.year, month - 1, date, 23, 59, 59)
	}

	onChange(scheduleId, locationId, month, dueDate) {
		//TODO: vvs p3 REFACTOR - use the same `getKey`
		const key = `${locationId}-${month}`
		let schedule = this.state.schedules[key]

		if (scheduleId > 0) {
			if (dueDate) {
				schedule.changeStatus = constants.changeStatus.UPDATE
				schedule.dueDateTime = this.getDueDateTime(month, dueDate)
			} else {
				schedule.changeStatus = constants.changeStatus.DELETE
				schedule.dueDateTime = null
			}
		} else if (schedule) {
			if (dueDate)
				schedule.dueDateTime = this.getDueDateTime(month, dueDate)
			else
				Reflect.deleteProperty(this.state.schedules, key)
		} else {
			//TODO: vvs p1 temp
			/* eslint-disable */
			this.newId -= 1
			this.state.schedules[key] = {
				id: this.newId,
				parentId: this.props.id,
				location: {
					id: locationId,
					type: this.props.locationType
				},
				completion: {
					dateTime: null,
					status: constants.taskStatus.OPEN
				},
				changeStatus: constants.changeStatus.INSERT,
				dueDateTime: this.getDueDateTime(month, dueDate)
			}
			/* eslint-enable */
		}
		this.setState({canChangeYear: false})

		this.props.onChange(this.state.schedules)
	}

	shouldComponentUpdate(nextProps) {
		return _.isInteger(nextProps.id)
	}

	render() {
		return <fieldset>
			<h3>
				Schedules
				<div className="pull-right">
					{this.state.canChangeYear ?
						<a href="#" onClick={this.onPreviousYearClicked.bind(this)}>
							<i className="fa fa-chevron-left"/>
						</a> :
						null
					}
					<span>{this.state.year}</span>
					{this.state.canChangeYear ?
						<a href="#" onClick={this.onNextYearClicked.bind(this)}>
							<i className="fa fa-chevron-right"/>
						</a> :
						null
					}
				</div>
			</h3>
			<section>
				<div className="table-responsive form-group">

					<table className="table table-bordered whiteBackground">
						<thead>
						<tr className="tableHeaderBackground">
							<th>{this.props.locationType === constants.locationType.ROOM ? 'Room' : 'Area'}</th>
							<th>Jan</th>
							<th>Feb</th>
							<th>Mar</th>
							<th>Apr</th>
							<th>May</th>
							<th>Jun</th>
							<th>Jul</th>
							<th>Aug</th>
							<th>Sep</th>
							<th>Oct</th>
							<th>Nov</th>
							<th>Dec</th>
						</tr>
						</thead>
						<tbody>
						{this.props.locations.map(x =>
							<PMLocationSchedule
								key={x.id}
								location={x}
								year={this.state.year}
								onChange={this.onChange.bind(this)}
								schedules={this.state.schedules}
								completions={this.state.completions}
							/>)}
						</tbody>
					</table>

				</div>
			</section>
		</fieldset>
	}

}
PMSchedules.propTypes = {
	stateTimestamp: PropTypes.instanceOf(Date),
	locationType: PropTypes.string,
	locations: PropTypes.array,
	onChange: PropTypes.func,
	id: PropTypes.number
}
PMSchedules.defaultTypes = {
	stateTimestamp: new Date(),
	locationType: constants.locationType.ROOM,
	locations: [],
	onChange: () => null,
	id: -1
}

export default PMSchedules
