'use strict'

import React from 'react'
import {browserHistory} from 'react-router'
import update from 'react-addons-update'
import validator from 'validator'

import {messages} from '../../../shared/core'
import {pmService} from '../../../shared/services'
import {locationType} from '../../../shared/constants'
import PMNew from './PMNew'

/**
 * PM New Controller component.
 */
class PMNewController extends React.Component {
	static propTypes = {
		route: React.PropTypes.object,
		routeParams: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {
			errorMessages: [],
			pm: {
				name: '',
				description: '',
				locationType: locationType.ROOM
			}
		}
	}

	onNameDescriptionChanged(name, description) {
		const state = update(this.state, {
			pm: {
				name: {$set: name},
				description: {$set: description}
			}
		})
		this.setState({pm: state.pm})
	}

	onLocationTypeChanged(e) {
		const state = update(this.state, {
			pm: {
				locationType: {$set: e.target.value}
			}
		})
		this.setState({pm: state.pm})
	}

	async onSave(e) {
		e.preventDefault()
		const errorMessages = this.validate()
		if (errorMessages.length > 0) {
			this.setState({errorMessages: errorMessages})

			return
		}

		await this.save()
	}

	/**
	 * Saves the PM.
	 * @param {Object} bh
	 * @description browserHistory is converted to bh so that it can be stubbed in UT
	 * (otherwise sinon has problem with it).
	 * @return {void}
	 */
	async save(bh = browserHistory) {
		const response = await pmService.insert({
			name: this.state.pm.name,
			description: this.state.pm.description || '',
			locationType: this.state.pm.locationType
		})
		bh.push(`/pm/${response.newId}`)
	}

	validate() {
		let errorMessages = []
		if (validator.isEmpty(this.state.pm.name.trim())) errorMessages.push(messages.error.NAME_REQUIRED)

		return errorMessages
	}

	render() {
		return <PMNew
			pm={this.state.pm}
			onLocationTypeChanged={::this.onLocationTypeChanged}
			onNameDescriptionChanged={::this.onNameDescriptionChanged}
			onSave={::this.onSave}/>
	}
}

export default PMNewController
