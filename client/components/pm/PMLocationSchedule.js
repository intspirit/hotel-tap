'use strict'

import React, {PropTypes, Component} from 'react'
import moment from 'moment'
import PMLocationMonthSchedule from './PMLocationMonthSchedule'

function getKey(locationId, month) {
	return `${locationId}-${month}`
}

const now = new Date()
const thisMonthFirstDate = moment([now.getFullYear(), now.getMonth() + 1]).add(-1, 'month')

/**
 * Checks whether a it's a past month.
 * @param {int} year
 * @param {int} month
 * @return {boolean}
 */
function isPastMonth(year, month) {
	const someDate = moment([year, month, 0, 0, 0]).add(-1, 'month')

	return someDate.toDate() < thisMonthFirstDate.toDate()
}

/**
 * PM schedules for a location.
 */
class PMLocationSchedules extends Component {
	static monthNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

	render() {
		return <tr>
			<td>{this.props.location.areaName ?
				`${this.props.location.areaName} - ${this.props.location.name}` :
				this.props.location.name}</td>
			{PMLocationSchedules.monthNumbers.map(x =>
				<PMLocationMonthSchedule
					key={getKey(this.props.location.id, x)}
					month={x}
					isPastMonth={isPastMonth(this.props.year, x)}
					year={this.props.year}
					locationId={this.props.location.id}
					onChange={this.props.onChange}
					schedule={this.props.schedules[getKey(this.props.location.id, x)] || null}
					completion={this.props.completions[getKey(this.props.location.id, x)] || null}
				/>)}
		</tr>
	}

}

PMLocationSchedules.propTypes = {
	location: PropTypes.object,
	year: PropTypes.number,
	onChange: PropTypes.func,
	schedules: PropTypes.any,
	completions: PropTypes.any
}

export default PMLocationSchedules
