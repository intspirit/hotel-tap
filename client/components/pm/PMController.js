'use strict'

import React from 'react'
import update from 'react-addons-update'
import validator from 'validator'
import moment from 'moment'
import {messages, db} from '../../../shared/core'
import {pmService} from '../../../shared/services'
import PM from './PM'
import constants from '../../../shared/constants'

/**
 * Parses room number out of the room tag.
 * @param {Object} location
 * @param {String} location.name
 * @return {int}
 */
const getRoomNumber = (location) => parseInt(location.name.substr(4), 10) || 0

/**
 * PM Controller component.
 */
class PMController extends React.Component {
	static propTypes = {
		route: React.PropTypes.object,
		routeParams: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.state = {
			loaded: false,
			errorMessages: [],
			stateTimestamp: new Date()
		}
		this.changed = false
		this.tasksChanged = false
		this.schedulesChanged = false
		this.schedules = []
	}

	onNameDescriptionChanged(name, description) {
		const state = update(this.state, {pm: {name: {$set: name}, description: {$set: description}}})
		this.changed = true
		this.setState({pm: state.pm})
	}

	assignmentChanged(assignmentType, assignmentId) {
		const state = update(this.state, {
			pm: {
				assignmentType: {$set: assignmentType},
				assignmentId: {$set: parseInt(assignmentId, 10)}
			}
		})
		this.changed = true
		this.setState({pm: state.pm})
	}

	onTasksChange(tasks) {
		this.tasksChanged = true
		const state = update(this.state, {pm: {tasks: {$set: tasks}}})
		this.setState({pm: state.pm})
	}

	onScheduleChange(schedules) {
		this.schedulesChanged = true
		this.schedules = schedules
	}

	async onSave(e) {
		e.preventDefault()
		const errorMessages = this.validate()
		if (errorMessages.length > 0) {
			this.setState({errorMessages: errorMessages})

			return
		}

		await this.saveChanges()
	}

	async saveChanges() {
		if (!this.changed && !this.tasksChanged && !this.schedulesChanged) return

		let request = {
			id: this.state.pm.id
		}

		if (this.changed) {
			request.name = this.state.pm.name
			request.description = this.state.pm.description
			request.assignmentType = this.state.pm.assignmentType
			request.assignmentId = this.state.pm.assignmentId
		}

		if (this.tasksChanged)
			request.tasks = this.state.pm.tasks.filter(x => x.changeStatus)


		if (this.schedulesChanged)
			request.schedules = Object.values(this.schedules).
				filter(x => x.changeStatus).
			map(x => Object.assign(x, {dueDateTimeString: moment(x.dueDateTime).format('YYYY-MM-DD HH:mm:ss')}))

		try {
			await pmService.update(this.state.pm.id, request)

			this.changed = false
			this.tasksChanged = false
			this.schedulesChanged = false
			this.schedules = []
			await this.getData(this.state.pm.id)
		} catch (error) {
			this.setState({
				errorMessages: [error.response.error.message]
			})
		}

	}

	hasNoAssignment() {
		return !this.state.pm.assignmentId || !validator.isInt(this.state.pm.assignmentId.toString(), [{min: 0}])
	}

	validate() {
		let errorMessages = []
		if (validator.isEmpty(this.state.pm.name.trim())) errorMessages.push(messages.error.NAME_REQUIRED)
		if (this.hasNoAssignment()) errorMessages.push(messages.error.ASSIGNMENT_REQUIRED)

		return errorMessages
	}

	async getData(id) {
		const [pm, departments, users] = await Promise.all(
			[pmService.getById(id), db.getDepartments(), db.getUsers()])
		if (pm.locationType === constants.locationType.ROOM) {
			pm.locations = pm.locations.sort((x, y) => getRoomNumber(x) <= getRoomNumber(y) ? -1 : 1) //eslint-disable-line no-confusing-arrow
		}
		this.setState({
			loaded: true,
			pm: pm,
			departments: departments,
			employees: users,
			stateTimestamp: new Date()
		})
	}

	async componentDidMount() {
		this.getData(this.props.routeParams.id)
	}

	render() {
		return <PM
			pm={this.state.pm}
			stateTimestamp={this.state.stateTimestamp}
			loaded={this.state.loaded}
			errorMessages={this.state.errorMessages}
			employees={this.state.employees}
			departments={this.state.departments}
			assignmentChanged={::this.assignmentChanged}
			onTasksChange={::this.onTasksChange}
			onScheduleChange={::this.onScheduleChange}
			onNameDescriptionChanged={::this.onNameDescriptionChanged}
			onSave={::this.onSave}/>
	}
}

export default PMController
