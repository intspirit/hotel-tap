'use strict'

import PMList from './PMList'
import PM from './PM'
import PMController from './PMController'
import PMNewController from './PMNewController'

export {
	PMList,
	PM,
	PMController,
	PMNewController
}


//TODO


/**
 * Name, description
 * [] - no name
 * [] - description html ?
 *
 * Tasks
 * [x] - change
 * [] - change & delete
 * [] - add & change
 *
 * Schedule
 * [] - overdue
 * [] - can't schedule - in the past
 * [x] - change existing
 * [x] - remove existing
 * [x] - add new
 * [x] - change new
 * [x] - remove new
 */
