'use strict'

import React, {PropTypes, Component} from 'react'
import moment from 'moment'
import _ from 'lodash'
import classNames from 'classnames'
import constants from '../../../shared/constants'
import utils from '../../../shared/core/utils'

//eslint-disable
//TODO: vvs p2 REFACTOR legacy function, but works better than 3rd party validators.
const validDays = _.range(1, 32).map(x => String(x))

const invalidDate = (year, month, value) => {
	if (value === '') return false

	if (validDays.indexOf(value) === -1) return true
	const day = parseInt(value, 10)
	const date = new Date(year, month - 1, day)
	if (date.getDate() !== day) return true

	return false
}
//eslint-enable

/**
 * Pm schedule for a specific location and month component.
 * @param {Object} schedule
 * @return {React.Component}
 */
class PMLocationMonthSchedule extends Component {
	constructor(props) {
		super(props)
		this.state = {
			value: ''
		}
		this.oldValue = ''
		this.dateTimeValue = null
	}

	isUndone = (schedule) => schedule && schedule.completion && schedule.completion.status === 'open'

	componentWillReceiveProps(nextProp) {
		const value = this.isUndone(nextProp.schedule) &&
			nextProp.schedule.changeStatus !== constants.changeStatus.DELETE ?
				new Date(nextProp.schedule.dueDateTime).getDate() :
				''
		this.oldValue = value
		this.dateTimeValue = value === '' ? null : new Date(nextProp.schedule.dueDateTime)
		this.setState({value})
	}

	onFocus(e) {
		this.oldValue = e.target.value
	}

	onBlur() {
		let trimmedValue = this.state.value && _.isString(this.state.value) ? this.state.value.trim() : this.state.value
		if (trimmedValue === this.oldValue) return

		//TODO: vvs p1 UNDONE - handle passt
		// if (utils.isBeforeToday(this.dateTimeValue)) {
		// 	// trimmedValue = new Date().getDate()
		// 	// alert('PM cannot be scheduled in the past') //eslint-disable no-alert
		// }

		this.props.onChange(
			this.props.schedule ? this.props.schedule.id : -1,
			this.props.locationId,
			this.props.month,
			trimmedValue)
	}

	onChange(e) {
		const trimmedValue = e.target.value.trim()
		if (invalidDate(this.props.year, this.props.month, trimmedValue)) return

		const dateString = `${this.props.year}-${this.props.month}-${trimmedValue} 23:59:59`
		this.dateTimeValue = null

		this.dateTimeValue = new Date(dateString)
		this.setState({
			value: e.target.value
		})
	}

	onClick(e) {
		e.preventDefault()
		console.log('click')//eslint-disable-line
	}

	getCompletionLink(date) {
		const dateOfMonth = moment(date).date()

		//TODO: vvs p1 proper link
		return <a
			key={`${_.uniqueId()}`}
			href={`${this.props.month}/${dateOfMonth}`}
			className="txt-color-green margin-right-5"
			onClick={this.onClick.bind(this)}>
			{dateOfMonth}
		</a>
	}

	canSchedule() {
		const undone = Boolean(this.props.schedule) &&
			this.props.schedule.completion &&
			this.props.schedule.completion.status === constants.taskStatus.OPEN
		const unscheduledAndNotInPastMonths = !this.props.schedule &&
			!this.props.isPastMonth

		return undone || unscheduledAndNotInPastMonths
	}

	//TODO: vvs p3 style to css
	render() {
		const overdue = utils.isOverdue(this.dateTimeValue)
		const classes = classNames('transparentInput1', 'cell', {'txt-color-red': overdue})


		return <td>
			{this.props.completion ?
				this.props.completion.map(x => this.getCompletionLink(x.completion.dateTime)) :
				null}

			{this.canSchedule() ?
				<input
					className={classes}
					style={{outline: 'none', border: '1px solid #9ecaed', boxShadow: '0 0 10px #9ecaed'}}
					value={this.state.value}
					onFocus={::this.onFocus}
					onBlur={::this.onBlur}
					onChange={::this.onChange}/> :
				null
			}
		</td>
	}

}
PMLocationMonthSchedule.propTypes = {
	onChange: PropTypes.func,
	year: PropTypes.number,
	month: PropTypes.number,
	locationId: PropTypes.number,
	isPastMonth: PropTypes.bool,
	schedule: PropTypes.object,
	completion: PropTypes.array
}
PMLocationMonthSchedule.defaultTypes = {
	year: 0,
	month: 0,
	locationId: 0,
	isPastMonth: true,
	schedule: {
		id: -1,
		location: {
			type: constants.locationType.ROOM,
			id: 0
		},
		dueDateTime: null,
		completion: null
	},
	completion: null
}

export default PMLocationMonthSchedule
