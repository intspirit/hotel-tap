'use strict'

import React, {PropTypes} from 'react'
import ContentTitle from '../core/ContentTitle'
import {boardTypes, locationType} from '../../../shared/constants'
import {ValidationError, NameDescription} from '../core'
import {utils} from '../../../shared/core'

/**
 * PM component.
 * @param {Object} props
 * @return {Component}
 */
const PMNew = props => <div className="col-sm-12">
	<ContentTitle
		title={'Preventive Maintenance'}
		type={boardTypes.PM}/>

	<div className="widget-body no-padding">
		<form className="smart-form">
			<fieldset>
				<NameDescription
					name={props.pm.name}
					nameHint={'PM name ...'}
					description={props.pm.description}
					onChange={props.onNameDescriptionChanged}/>
				<section>
					<label className="select">
						<select value={props.pm.locationType} onChange={props.onLocationTypeChanged}>
							<option value={locationType.ROOM}>Rooms</option>
							<option value={locationType.AREA}>Areas</option>
						</select>
						<i/>
					</label>
				</section>
			</fieldset>

			<footer>
				<ValidationError errorMessages={props.errorMessages}/>
				<button
					className="btn btn-primary"
					onClick={props.onSave}>
					Save
				</button>

			</footer>
		</form>
	</div>
</div>

PMNew.propTypes = {
	pm: PropTypes.shape({
		name: PropTypes.string,
		description: PropTypes.string,
		locationType: PropTypes.string

	}),
	errorMessages: PropTypes.arrayOf(PropTypes.string),
	onNameDescriptionChanged: PropTypes.func,
	onLocationTypeChanged: PropTypes.func,
	onSave: PropTypes.func
}

PMNew.defaultProps = {
	pm: {
		name: '',
		description: '',
		locationType: locationType.ROOM
	},
	errorMessages: [],
	onNameDescriptionChanged: utils.emptyFunc,
	onLocationTypeChanged: utils.emptyFunc,
	onSave: utils.emptyFunc
}

export default PMNew
