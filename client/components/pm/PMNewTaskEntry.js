'use strict'

import React from 'react'
import constants from '../../../shared/constants'

/**
 * PM task entry form.
 * @constructor
 */
class PMNewTaskEntry extends React.Component {
	static propTypes = {
		onNewTaskAdded: React.PropTypes.func
	}
	static defaultProps = {
		onNewTaskAdded: () => null
	}

	constructor(props) {
		super(props)
		this.state = {
			value: ''
		}
	}

	onValueChange(event) {
		this.setState({value: event.target.value})
	}

	onKeyDown(event) {
		if (event.keyCode !== constants.keyboardKey.ENTER) return

		event.preventDefault()
		this.props.onNewTaskAdded(this.state.value.trim())
		this.setState({
			value: ''
		})
	}

	render() {
		return <label className="input">
			<input
				type="text"
				value={this.state.value}
				onChange={this.onValueChange.bind(this)}
				onKeyDown={this.onKeyDown.bind(this)}
				className="form-control"
				placeholder="Add task and press [Enter]"/>
		</label>
	}
}

export default PMNewTaskEntry
