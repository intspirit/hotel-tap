'use strict'

import React, {PropTypes} from 'react'

import ContentTitle from '../core/ContentTitle'
import {boardTypes} from '../../../shared/constants'
import {Assignment, ValidationError, NameDescription} from '../core'
import {utils} from '../../../shared/core'
import PMTasks from './PMTasks'
import PMSchedules from './PMSchedules'
import constants from '../../../shared/constants.js'
import appController from '../../core/appController'


/**
 * PM component.
 * @param {Object} props
 * @return {Component}
 */
const PM = props => <div className="col-sm-12">
	<ContentTitle
		title={'Preventive Maintenance'}
		loaded={props.loaded}
		type={boardTypes.PM}/>

	{props.loaded ?
		<div className="widget-body no-padding">
			<form className="smart-form">
				<fieldset>
					<NameDescription
						name={props.pm.name}
						nameHint={'PM name ...'}
						description={props.pm.description}
						disabled={!appController.user.isAdmin}
						onChange={props.onNameDescriptionChanged}/>
				</fieldset>

				<Assignment
					assignmentChanged={props.assignmentChanged}
					assignmentType={props.pm.assignmentType}
					assignmentId={props.pm.assignmentId}
					employees={props.employees}
					disabled={!appController.user.isAdmin}
					departments={props.departments}/>

				<PMTasks tasks={props.pm.tasks} onChange={props.onTasksChange}/>

				<PMSchedules
					id={props.pm.id}
					stateTimestamp={props.stateTimestamp}
					locationType={props.pm.locationType}
					locations={props.pm.locations}
					onChange={props.onScheduleChange}/>

				<footer>
					<ValidationError errorMessages={props.errorMessages}/>
					<button
						className="btn btn-primary"
						onClick={props.onSave}>
						Save
					</button>

				</footer>
				<footer>&nbsp;</footer>

			</form>
		</div> :
		null
	}
</div>

PM.propTypes = {
	pm: PropTypes.object,
	stateTimestamp: PropTypes.instanceOf(Date),
	loaded: PropTypes.bool,
	errorMessages: PropTypes.arrayOf(PropTypes.string),
	employees: PropTypes.array,
	departments: PropTypes.array,
	assignmentChanged: PropTypes.func,
	onTasksChange: PropTypes.func,
	onScheduleChange: PropTypes.func,
	onNameDescriptionChanged: PropTypes.func,
	onSave: PropTypes.func
}

PM.defaultProps = {
	pm: {
		name: '',
		description: '',
		locationType: constants.locationType.ROOM,
		assignmentType: constants.assignmentTypes.EMPLOYEE,
		assignmentId: -1,
		task: [],
		schedules: []
	},
	stateTimestamp: new Date(),
	employees: [],
	departments: [],
	loaded: true,
	errorMessages: [],
	assignmentChanged: utils.emptyFunc,
	onTasksChange: utils.emptyFunc,
	onScheduleChange: utils.emptyFunc,
	onNameDescriptionChanged: utils.emptyFunc,
	onSave: utils.emptyFunc
}

export default PM
