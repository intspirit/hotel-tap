'use strict'

import React from 'react'
import {Link} from 'react-router'
import ContentTitle from '../core/ContentTitle'
import {boardTypes} from '../../../shared/constants'
import {pmService} from '../../../shared/services'
import appController from '../../core/appController'

/**
 * View header for PM list.
 * @constructor
 */
const PMListViewHeader = () => <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<ContentTitle
		title={'Preventive Maintenance'}
		type={boardTypes.PM}>
		{appController.user.isAdmin ?
			<div className="pull-right">
				<Link className="btn btn-primary" to={`/pm/new`}>Add Maintenance</Link>
			</div> :
			null
		}
	</ContentTitle>
</div>


/**
 * PM list item.
 * @param {Object} props
 * @return {void}
 */
const PMListItem = props => <tr>
	<td>{props.pm.subject}</td>
	<td>
		<Link className="fa fa-edit" to={`/pm/${props.pm.id}`}/>
	</td>
</tr>
PMListItem.propTypes = {
	pm: React.PropTypes.shape({
		subject: React.PropTypes.string,
		id: React.PropTypes.number
	})
}


/**
 * PM list.
 */
class PMList extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			pmList: []
		}
	}

	async componentDidMount() {
		const response = await pmService.getAll()
		this.setState({
			pmList: response.pmList
		})
	}

	render() {
		return <div className="col-sm-12">
			<PMListViewHeader />

			<article className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div className="table-responsive">

					<table className="table table-bordered whiteBackground">
						<thead>
						<tr className="tableHeaderBackground">
							<th>Maintenance Name</th>
							<th className="col-sm-1">Options</th>
						</tr>
						</thead>
						<tbody>
						{this.state.pmList.map(x => <PMListItem key={x.id} pm={x} />)}
						</tbody>
					</table>

				</div>
			</article>
		</div>
	}
}

export default PMList
