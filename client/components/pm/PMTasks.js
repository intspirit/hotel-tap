'use strict'

import React from 'react'
import update from 'react-addons-update'
import constants from '../../../shared/constants'
import PMTask from './PMTask'
import PMNewTaskEntry from './PMNewTaskEntry'
import appController from '../../core/appController'


/**
 * PM tasks.
 */
class PMTasks extends React.Component {

	static propTypes = {
		tasks: React.PropTypes.array,
		onChange: React.PropTypes.func
	}
	static defaultProps = {
		tasks: [],
		onChange: () => null
	}

	constructor(props) {
		super(props)
		this.state = {
			tasks: props.tasks
		}
		this.newTaskId = -1
		this.newTaskOrder = props.tasks.length > 0 ? Math.max(...props.tasks.map(x => x.sortOrder)) + 1 : 1

	}

	onNewTaskAdded(subject) {
		if (subject === '') return

		const newTask = {
			id: this.newTaskId,
			subject: subject,
			sortOrder: this.newTaskOrder,
			changeStatus: constants.changeStatus.INSERT
		}
		const updatedTasks = update(this.state.tasks, {$push: [newTask]})
		this.props.onChange(updatedTasks)
		this.setState({tasks: updatedTasks})

		this.newTaskId -= 1
		this.newTaskOrder += 1
	}

	/**
	 * Handles task's subject change.
	 * @param {int} id
	 * @param {string} subject
	 * @return {void}
	 * @description see comments in the onDelete.
	 */
	onTaskChanged(id, subject) {
		const taskIndex = this.state.tasks.findIndex(x => x.id === id)
		const newChangeStatus = this.state.tasks[taskIndex].changeStatus === constants.changeStatus.INSERT ?
			constants.changeStatus.INSERT :
			constants.changeStatus.UPDATE

		const updatedTasks = update(this.state, {
			tasks: {
				[taskIndex]: {
					subject: {$set: subject},
					changeStatus: {$set: newChangeStatus}
				}
			}
		})
		this.props.onChange(updatedTasks.tasks)
		this.setState({tasks: updatedTasks.tasks})
	}

	/**
	 * Handles task delete request.
	 * @param {Object} e
	 * @return {void}
	 * @description This func uses React update for a better better performance
	 * to set `changeState` for task to 'd' or removes if it's just added task.
	 * See:
	 * - https://facebook.github.io/react/docs/update.html
	 * - http://stackoverflow.com/questions/28121272/whats-the-best-way-to-update-an-object-in-an-array-in-reactjs
	 */
	onDelete(e) {
		e.preventDefault()
		const taskIndex = this.state.tasks.findIndex(x => x.id === parseInt(e.target.id, 10))
		const updatedTasks = this.state.tasks[taskIndex].changeStatus === constants.changeStatus.INSERT ?
			update(this.state.tasks, {$splice: [[taskIndex, 1]]}) :
			update(this.state.tasks, {
				$splice: [[taskIndex, 1,
					update(this.state.tasks[taskIndex], {changeStatus: {$set: constants.changeStatus.DELETE}})]]
			})
		this.props.onChange(updatedTasks)
		this.setState({tasks: updatedTasks})
	}

	render() {
		return <fieldset>
			<h3>Tasks</h3>
			<section>
				<div className="table-responsive form-group">

					<table className="table table-bordered whiteBackground">
						<thead>
						<tr className="tableHeaderBackground">
							<th>Task</th>
							<th className="col-sm-1 text-center"><i className="fa fa-trash"/></th>
						</tr>
						</thead>
						<tbody>
						{this.state.tasks.map(x =>
							<PMTask
								key={x.id}
								task={x}
								onChange={::this.onTaskChanged}
								onDelete={::this.onDelete}/>)}
						</tbody>
					</table>

				</div>
			</section>
			{appController.user.isAdmin ?
				<section>
					<PMNewTaskEntry onNewTaskAdded={this.onNewTaskAdded.bind(this)}/>
				</section> :
				null
			}
		</fieldset>
	}
}

export default PMTasks
