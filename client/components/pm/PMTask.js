'use strict'

import React from 'react'
import constants from '../../../shared/constants'
import appController from '../../core/appController'

// WIP
//eslint-disable
/**
 * PM task.
 * @param {Object} props
 * @return {void}
 */
class PMTask extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			value: props.task.subject
		}
	}

	onChange(e) {
		if (!appController.user.isAdmin) return

		this.setState({value: e.target.value})
	}

	onBlur() {
		const newValue = this.state.value.trim()
		if (newValue === this.props.task.subject) return

		this.props.onChange(this.props.task.id, newValue)
	}

	render() {
		if (this.props.task.changeStatus === constants.changeStatus.DELETE) return null

		return <tr>
			<td>
				<input
					type="text"
					className="transparentInput col-md-12"
					onChange={::this.onChange}
					onBlur={::this.onBlur}
					value={this.state.value}/>
			</td>
			<td className="text-center">
				{appController.user.isAdmin ?
					<a href="#" onClick={this.props.onDelete}>
						<i id={this.props.task.id} className="fa fa-trash"/>
					</a> :
					null}
			</td>
		</tr>
	}
}

PMTask.propTypes = {
	task: React.PropTypes.shape({
		subject: React.PropTypes.string,
		id: React.PropTypes.number,
		changeStatus: React.PropTypes.string
	}),
	onDelete: React.PropTypes.func,
	onChange: React.PropTypes.func
}
PMTask.defaultProps = {
	task: {id: -333, subject: ''},
	onDelete: () => null,
	onChange: () => null,
	changeStatus: ''
}

export default PMTask
