'use strict'

import React from 'react'

const GroupHeader = props => {
	const {title, numberOfTodos, uiOptions, changeGroupItemsVisibility, isGroupVisible} = props

	let groupTitleClass = null
	let groupIconClass = null
	if (uiOptions) {
		groupTitleClass = uiOptions.groupTitleClass || ''
		groupIconClass = uiOptions.groupIconClass || ''
	}

	const renderVisibilityControl = () => {
		const iconName = isGroupVisible ? 'caret-down' : 'caret-right'

		return (
			<div className="jarviswidget-ctrls">
				<a className="collapse-btn"
						onClick={(ev) => {
							ev.preventDefault()
							changeGroupItemsVisibility()
						} }>
					<i className={`fa fa-${iconName} not-printable`}/>
				</a>
			</div>
		)
	}

	return (
		<h5 className={`todo-group-title ${groupTitleClass}`}>
			<i className={ `fa ${groupIconClass}` }/>
			{ ` ${title} ` }
			<span className="pull-right counter">{ numberOfTodos }</span>
			{changeGroupItemsVisibility ? renderVisibilityControl() : null}
		</h5>
	)
}

GroupHeader.propTypes = {
	title: React.PropTypes.string.isRequired,
	numberOfTodos: React.PropTypes.number.isRequired,
	uiOptions: React.PropTypes.object,
	changeGroupItemsVisibility: React.PropTypes.func,
	isGroupVisible: React.PropTypes.bool
}

export default GroupHeader
