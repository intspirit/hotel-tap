'use strict'

import React from 'react'
import {Panel} from 'react-bootstrap'

import GroupHeader from './GroupHeader'
import GroupItems from './GroupItems'

class TodoGroup extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			open: true
		}
	}

	toggleItemsVisibility() {
		this.setState({open: !this.state.open})
	}

	render() {
		const {group, title, uiOptions, showTodoFullInfo, onTodoCompletionStatusChanged, isCollapsible} = this.props

		let changeGroupItemsVisibility = null
		let isGroupVisible = null
		if (isCollapsible) {
			changeGroupItemsVisibility = () => this.toggleItemsVisibility()
			isGroupVisible = this.state.open
		}

		const renderItems = () =>
			<GroupItems todos={group.items}
						uiOptions={uiOptions}
						showTodoFullInfo={showTodoFullInfo}
						onTodoCompletionStatusChanged={onTodoCompletionStatusChanged}/>

		const renderCollapsiblePanel = () =>
			<Panel collapsible={isCollapsible} expanded={this.state.open} className="collapsible-panel">
				<div className="clearfix" />
				{renderItems()}
			</Panel>

		return group && group.count ?
			<div className="todo-group">
				<GroupHeader title={title} numberOfTodos={group.count} uiOptions={uiOptions}
								changeGroupItemsVisibility={changeGroupItemsVisibility}
								isGroupVisible={isGroupVisible}/>
				<div className="clearfix"/>
				{isCollapsible ? renderCollapsiblePanel() : renderItems()}
			</div> :
			null
	}
}

TodoGroup.propTypes = {
	group: React.PropTypes.object.isRequired,
	title: React.PropTypes.string.isRequired,
	showTodoFullInfo: React.PropTypes.func,
	onTodoCompletionStatusChanged: React.PropTypes.func,
	uiOptions: React.PropTypes.object,
	isCollapsible: React.PropTypes.bool
}

export default TodoGroup
