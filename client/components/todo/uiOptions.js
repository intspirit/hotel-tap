import constants from '../../../shared/constants'

const todoTypesIconClasses = {
	rl: 'fa-pencil',
	cl: 'fa-check-circle',
	insp: 'fa-search'
}

const overdueTodoTextClassName = 'txt-color-red'
const todayTodoTextClassName = 'txt-color-blue'

const baseTodoTypeUiOptions = {
	groupIconClass: 'fa-exclamation',
	listItemClass: 'background-clear',
	overdueTodoClassName: overdueTodoTextClassName,
	todayTodoClassName: todayTodoTextClassName
}

const UIOptions = {
	[constants.todoGroupNames.TODAY]: {
		dateTimeFormat: 'LT',
		groupIconClass: 'fa-exclamation',
		groupTitleClass: todayTodoTextClassName,
		listItemClass: 'background-clear',
		todoItemTypeClasses: todoTypesIconClasses
	},
	[constants.todoGroupNames.OVERDUE]: {
		groupIconClass: 'fa-warning',
		groupTitleClass: overdueTodoTextClassName,
		listItemClass: 'background-clear',
		todoItemTypeClasses: todoTypesIconClasses
	},
	[constants.todoGroupNames.FUTURE]: {
		groupIconClass: 'fa-arrow-up',
		groupTitleClass: 'txt-color-black',
		listItemClass: 'background-clear',
		todoItemTypeClasses: todoTypesIconClasses
	},

	[constants.todoGroupNames.TASKS]: Object.assign({}, baseTodoTypeUiOptions),
	[constants.todoGroupNames.CHECKLIST_SCHEDULE]: Object.assign({}, baseTodoTypeUiOptions),
	[constants.todoGroupNames.INSPECTIONS]: Object.assign({}, baseTodoTypeUiOptions),
	[constants.todoGroupNames.PM]: Object.assign({}, baseTodoTypeUiOptions)
}

export default UIOptions
