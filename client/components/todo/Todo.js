'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'

import constants from '../../../shared/constants'
import utils from '../../../shared/core/utils'
import appController from '../../core/appController'

import Description from '../core/Description'


const TodoDescription = props =>
	<Tooltip id={`todo_tooltip_${props.todo.id}`}>
		<Description post={props.todo}/>
	</Tooltip>

TodoDescription.propTypes = {
	todo: React.PropTypes.object.isRequired
}


const Todo = props => {
	const {todo, showFullInfo, onCompletionStatusChanged, uiOptions, isCheckable} = props

	const listItemClass = uiOptions ? uiOptions.listItemClass : null
	const todoIcon = uiOptions ? uiOptions.todoIcon : null

	let todoInfoTextStyle = ''
	if (uiOptions) {
		if (todo.isOverdue) {
			todoInfoTextStyle = uiOptions.overdueTodoClassName || todoInfoTextStyle
		} else if (todo.isToday) {
			todoInfoTextStyle = uiOptions.todayTodoClassName || todoInfoTextStyle
		}
	}
	const todoInfoClassName = `todo-info ${todoInfoTextStyle}`

	const isChecked = todo.completion.status === constants.taskStatus.DONE

	let onCheckboxStateChange = null
	if (onCompletionStatusChanged) {
		onCheckboxStateChange = () => onCompletionStatusChanged()
	}

	let onTodoSubjectClick = null
	if (showFullInfo) {
		onTodoSubjectClick = () => showFullInfo()
	}

	const renderCheckbox = () =>
			<label className="checkbox">
				<input type="checkbox" name="checkbox-inline" checked={isChecked} onChange={onCheckboxStateChange}/>
				<i/>
			</label>

	const dueDateTime = todo.dueDateTime ?
							utils.formatDateTime(todo.dueDateTime, appController.hotel.timeZone) :
							constants.emptyDueDateTimeMessage

	return (
		<OverlayTrigger placement="bottom" overlay={<TodoDescription todo={todo} />}>
			<li className={listItemClass}>
					<span className="handle">
						{isCheckable ? renderCheckbox() : null}
					</span>
				<p className={todoInfoClassName} onClick={onTodoSubjectClick}>
					<a className="todo-subject"><i className={todoIcon} />{todo.subject}</a>
					<span className="date">{dueDateTime}</span>
				</p>
			</li>
		</OverlayTrigger>
	)
}

Todo.propTypes = {
	todo: React.PropTypes.object.isRequired,
	showFullInfo: React.PropTypes.func,
	onCompletionStatusChanged: React.PropTypes.func,
	uiOptions: React.PropTypes.object,
	isCheckable: React.PropTypes.bool
}

export default Todo
