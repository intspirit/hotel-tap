import React from 'react'
import Log from 'loglevel'
import {DropdownButton, MenuItem} from 'react-bootstrap'

import todoService from '../../../shared/services/todoService'
import postService from '../../../shared/services/postService'

import appController from '../../core/appController'
import {
	GAEvents,
	todoSortTypes,
	navigationOptionType,
	defaultUserId,
	interval,
	taskStatus,
	postReadStatus} from '../../../shared/constants'
import {logEventForGA} from '../../functions'

import TodoGroup from './TodoGroup'
import Printer from '../../core/Printer'

import uiOptions from './uiOptions'

const sortDropdownTitles = {
	[todoSortTypes.DUE_DATETIME]: 'Sort by date',
	[todoSortTypes.TODO_TYPE]: 'Sort by type'
}

const SortTypeSelector = props =>
	<DropdownButton id="todo_sort_type_selector" title={props.activeSortType} onSelect={props.onChange}>
		{Object.keys(sortDropdownTitles).map(sortType =>
			<MenuItem eventKey={sortType} key={`sort_type_${sortType}`}>{sortDropdownTitles[sortType]}</MenuItem>
		)}
	</DropdownButton>

SortTypeSelector.propTypes = {
	activeSortType: React.PropTypes.string.isRequired,
	onChange: React.PropTypes.func.isRequired
}

/**
 * To do list component.
 */
class TodoList extends React.Component {
	constructor(props) {
		super(props)
		//noinspection JSUnresolvedVariable
		this.state = TodoList.defaultState
		appController.todoComponent = this
	}

	static get defaultState() {
		return {
			todoItems: {
				totalCount: 0,
				groups: []
			},
			sortType: todoSortTypes.DUE_DATETIME
		}
	}

	getRequestParams(assignmentType, id) {
		const typeParam = assignmentType === navigationOptionType.DEPARTMENT ?
			'departments' :
			'employees'

		let idParam = ''
		if (assignmentType === navigationOptionType.DEPARTMENT) {
			idParam = id
		} else if (id && id !== defaultUserId) {
			idParam = id
		} else {
			idParam = defaultUserId
		}

		return {typeParam, idParam}
	}

	async getData(assignmentType, id) {
		let state = TodoList.defaultState
		try {
			//TODO: vvs p1 authorize
			let {typeParam, idParam} = this.getRequestParams(assignmentType, id)

			const todoItems = await todoService.getList(typeParam, idParam, this.state.sortType)
			state = {todoItems: todoItems, sortType: this.state.sortType}
		} catch (error) {
			Log.error(`ToDo|getData|error:${error}`)

		}
		await this.setState(state)
	}

	startRefreshingDataEveryMinute() {
		this.timer = setInterval(() => this.getData(this.props.assignment, this.props.id), interval.ONE_MINUTE)
	}

	stopRefreshingDataEveryMinute() {
		if (this.timer) {
			clearInterval(this.timer)
		}
	}

	//noinspection JSUnusedGlobalSymbols
	componentDidMount() {
		this.getData(this.props.assignment, this.props.id)
		this.startRefreshingDataEveryMinute()
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.assignment !== this.props.assignment || nextProps.id.toString() !== this.props.id.toString()) {
			this.stopRefreshingDataEveryMinute()
			this.getData(nextProps.assignment, nextProps.id)
			this.startRefreshingDataEveryMinute()
		}
	}

	componentWillUnmount() {
		this.stopRefreshingDataEveryMinute()
	}

	setDoneTodoState(todoGroupName, positionInGroup) {
		let state = this.state

		if (state.sortType === todoSortTypes.DUE_DATETIME) {
			let groups = state.todoItems.groups

			let currentGroup = groups.find(group => group.name === todoGroupName)
			currentGroup.collection.items.splice(positionInGroup, 1)
			currentGroup.collection.count -= 1

			state.todoItems.totalCount -= 1

			this.setState(state)
		}
	}

	async onCompletionChanged(groupName, todo, positionInGroup) {
		try {
			const currentStatus = todo.completion.status
			if (currentStatus === taskStatus.DONE) {
				throw new Error('Completion status is already "DONE"')
			} else {
				const newStatus = taskStatus.DONE

				const serviceCompletionResponse = await postService.update(todo.id, {completionStatus: newStatus})
				if (serviceCompletionResponse.status === newStatus) {
					todo.completion.status = newStatus
					this.setDoneTodoState(groupName, positionInGroup)
					logEventForGA(GAEvents.categories.TODO, GAEvents.actions.TODO_COMPLETED)
				} else {
					throw new Error(`Completion status not updated; value: ${serviceCompletionResponse.status}`)
				}

				const serviceReadResponse = await postService.update(todo.id, {readStatus: postReadStatus.READ})

				if (serviceReadResponse.readStatus === postReadStatus.READ) {
					appController.taskChanged(todo.id)
					appController.postReadStatusChanged()
				} else {
					throw new Error(`Read status not updated; value: ${serviceReadResponse.status}`)
				}
			}
		} catch (error) {
			Log.error(`ToDo|onTodoChanged|error:${error}`)
		}
	}

	selectTodoFullInfo(todoId) {
		appController.selectTask(todoId)
		appController.disactivateSelectedNavigationOptionUntilUserAction()
	}

	refreshTodos() {
		this.stopRefreshingDataEveryMinute()
		this.getData(this.props.assignment, this.props.id)
		this.startRefreshingDataEveryMinute()
	}

	changeSortType(newSortType) {
		let state = this.state
		state.sortType = newSortType
		const todoSortLabelForGA = newSortType === todoSortTypes.DUE_DATETIME ?
			GAEvents.labels.BY_DATE :
			GAEvents.labels.BY_TYPE

		logEventForGA(GAEvents.categories.TODO, GAEvents.actions.TODO_SORT_CHANGED, todoSortLabelForGA)
		this.setState(state, () => this.getData(this.props.assignment, this.props.id))
	}

	print() {
		if (!this.node) return

		const printer = new Printer()
		printer.print(this.node)
	}

	get todoGroups() {
		const todoItems = this.state.todoItems

		return todoItems.totalCount ?
			todoItems.groups.map(group =>
				<TodoGroup group={group.collection}
							title={group.title}
							uiOptions={uiOptions[group.name]}
							showTodoFullInfo={(todoId) => this.selectTodoFullInfo(todoId)}
							onTodoCompletionStatusChanged={(todo, position) =>
								this.onCompletionChanged(group.name, todo, position)}
							isCollapsible={true}
							key={`todo_group_${group.name}`}/>
			) :
			<h4 className="text-center text-muted empty-list">List is Empty</h4>
	}

	get printButton() {
		return this.state.todoItems.totalCount ?
			<span className="widget-icon print-todo not-printable" onClick={this.print.bind(this)}>
				<i className="fa fa-print txt-color-white"/>
			</span> :
			null
	}

	render() {
		let todoItems = this.state.todoItems

		return (
			<section ref={node => {
					this.node = node

					return node
				}}
				id="widget-grid"
				className="">
				<div className="row">
					<article className="col-sm-12 col-md-12 col-lg-12">
						<div
							className="jarviswidget jarviswidget-color-blue todo-list"
							id="todo-list"
							data-widget-editbutton="false"
							data-widget-colorbutton="false">
							<header>
								<span className="widget-icon"> <i className="fa fa-check txt-color-white"/> </span>
								<h2> To - Do List </h2>
								{this.printButton}
								<div className="widget-toolbar no-border todo-counter no-pointer">
									{ todoItems.totalCount }
								</div>
								<div className="widget-toolbar no-border not-printable">
									<SortTypeSelector activeSortType={sortDropdownTitles[this.state.sortType]}
														onChange={(sortType) => this.changeSortType(sortType)} />

								</div>
							</header>
							<div>
								<div className="widget-body no-padding smart-form">
									{this.todoGroups}
								</div>
							</div>
						</div>
					</article>
				</div>
			</section>
		)
	}
}

TodoList.propTypes = {
	assignment: React.PropTypes.string.isRequired,
	id: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.number
	])
}
TodoList.defaultProps = {
	assignment: navigationOptionType.EMPLOYEE,
	id: defaultUserId
}

export default TodoList
