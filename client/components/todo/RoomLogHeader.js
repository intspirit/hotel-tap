import React from 'react'

const RoomLogHeader = props => {
	const {title, numberOfTodos, uiOptions, toggleRoomLogItemsVisibility, areRoomLogsItemsVisible} = props

	const listItemClass = `room-log-header ${uiOptions && uiOptions.listItemClass || ''}`


	const renderRoomVisibilitySwitcher = () => {
		const iconName = areRoomLogsItemsVisible ? 'caret-down' : 'caret-right'

		const roomVisibilitySwitchHandler = (ev) => {
			ev.preventDefault()
			toggleRoomLogItemsVisibility()
		}

		return (
			<a className="collapse-btn margin-right-5" onClick={roomVisibilitySwitchHandler}>
				<i className={`fa fa-${iconName}`} />
			</a>
		)
	}

	return (
		<li className={listItemClass}>
			<span className="handle" />
			<p className="todo-info">
				{toggleRoomLogItemsVisibility ? renderRoomVisibilitySwitcher() : null}
				{title}
			</p>
			<span className="pull-right counter">{numberOfTodos}</span>
		</li>
	)


}

RoomLogHeader.propTypes = {
	title: React.PropTypes.string.isRequired,
	numberOfTodos: React.PropTypes.number.isRequired,
	uiOptions: React.PropTypes.object,
	toggleRoomLogItemsVisibility: React.PropTypes.func,
	areRoomLogsItemsVisible: React.PropTypes.bool
}

export default RoomLogHeader
