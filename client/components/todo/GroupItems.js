'use strict'

import React from 'react'
import RoomLogHeader from './RoomLogHeader'
import Todo from './Todo'

import constants from '../../../shared/constants'

const todoTypes = constants.postTypes

const buildTodoItemsListToRender = (todos, visibleRoomLogs) => {
	let resultList = []

	todos.forEach(todo => {
		if (todo.typeId === todoTypes.CHECKLIST_SCHEDULE ||
			todo.typeId === todoTypes.INSPECTION_SCHEDULE ||
			todo.typeId === todoTypes.PM_SCHEDULE) {
			resultList.push(todo)
			if (visibleRoomLogs[todo.id]) {
				if (todo.collection && todo.collection.count) {
					todo.collection.items.forEach(roomTodo => {
						resultList.push(roomTodo)
					})
				}
			}
		} else {
			resultList.push(todo)
		}
	})

	return resultList
}

class GroupItems extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			visibleSchedules: {}
		}
	}

	toggleSchedulesVisibility(scheduleId) {
		let state = this.state

		if (state.visibleSchedules[scheduleId]) {
			Reflect.deleteProperty(state.visibleSchedules, scheduleId)
		} else {
			state.visibleSchedules[scheduleId] = true
		}

		this.setState(state)
	}

	render() {
		const {todos, showTodoFullInfo, onTodoCompletionStatusChanged, uiOptions} = this.props

		const visibleSchedules = this.state.visibleSchedules

		const todosToRender = buildTodoItemsListToRender(todos, visibleSchedules)

		return (
			<ul id="sortable1" className="todo">
				{ todosToRender.map((todo, index) => {
					let ui = null
					let key = `todo_${todo.id}`

					// TODO: alytyuk p2 refactor code below to distinguish room logs, rooms and associated room tasks
					if ((todo.typeId === todoTypes.PM_SCHEDULE ||
						todo.typeId === todoTypes.INSPECTION_SCHEDULE ||
						todo.typeId === todoTypes.CHECKLIST_SCHEDULE) &&
						todo.collection) {
						ui =
							<RoomLogHeader title={todo.subject}
											key={key}
											numberOfTodos={todo.collection.count}
											uiOptions={uiOptions}
											toggleRoomLogItemsVisibility={() => this.toggleSchedulesVisibility(todo.id)}
											areRoomLogsItemsVisible={visibleSchedules[todo.id]}/>
					} else {
						ui =
							<Todo todo={todo}
									isCheckable={todo.typeId === todoTypes.TASK}
									uiOptions={uiOptions}
									showFullInfo={showTodoFullInfo ? () => showTodoFullInfo(todo.id) : null}
									onCompletionStatusChanged={onTodoCompletionStatusChanged ?
										() => onTodoCompletionStatusChanged(todo, index) :
										null
									}
									key={key}/>
					}

					return ui
				})}
			</ul>
		)
	}
}

GroupItems.propTypes = {
	todos: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
	showTodoFullInfo: React.PropTypes.func,
	onTodoCompletionStatusChanged: React.PropTypes.func,
	uiOptions: React.PropTypes.object
}

export default GroupItems
