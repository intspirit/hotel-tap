'use strict'

import React from 'react'
import AttachmentEntry from '../core/attachments/AttachmentEntry'
import {postTypes} from '../../../shared/constants'

const btnPostLabel = {
	[postTypes.TASK]: 'Post a task',
	[postTypes.NOTE]: 'Post a note'
}

const btnToggleLabel = {
	[postTypes.TASK]: 'Cancel',
	[postTypes.NOTE]: 'Convert to task'
}

class PostEntryActions extends React.Component {
	resetAttachmentsState() {
		this.refs.attachments.reset()
	}

	render() {
		return <div>
			<AttachmentEntry
				ref="attachments"
				attachments={this.props.attachments}
				onAttachmentsChanged={this.props.onAttachmentsChanged}
			/>

			{this.props.showPostButton ?
				<button
					type="button"
					className="btn pull-right btn-primary btn-sm"
					onClick={this.props.onPost}
					disabled={this.props.disabledSubmit}
				>
					{btnPostLabel[this.props.typeId]}
				</button> :
				null
			}

			{this.props.showToggleButton ?
				<button type="button" className="btn pull-right btn-default btn-sm" onClick={this.props.onToggle}>
					{btnToggleLabel[this.props.typeId]}
				</button> :
				null
			}

			{this.props.showCancelButton ?
				<button type="button" className="btn pull-right btn-default btn-sm" onClick={this.props.onCancel}>
					Cancel
				</button> :
				null
			}
		</div>
	}
}

PostEntryActions.propTypes = {
	attachments: React.PropTypes.array,
	typeId: React.PropTypes.string,
	onAttachmentsChanged: React.PropTypes.func,
	onPost: React.PropTypes.func,
	onToggle: React.PropTypes.func,
	onCancel: React.PropTypes.func,
	showPostButton: React.PropTypes.bool,
	showToggleButton: React.PropTypes.bool,
	showCancelButton: React.PropTypes.bool,
	disabledSubmit: React.PropTypes.bool
}

PostEntryActions.defaultProps = {
	onAttachmentsChanged: () => null,
	onPost: () => null,
	onToggle: () => null,
	onCancel: () => null,
	typeId: postTypes.NOTE,
	showPostButton: true,
	showToggleButton: true,
	showCancelButton: false,
	disabledSubmit: false
}

export default PostEntryActions
