'use strict'

import React from 'react'
import {Button, Grid, Row, Col} from 'react-bootstrap'
import Modal from '../core/Modal'

class ConvertToTaskModal extends React.Component {

	constructor(props) {
		super(props)

		this.handleConvertToTaskClick = this.handleConvertToTaskClick.bind(this)
		this.handlePostNoteClick = this.handlePostNoteClick.bind(this)
		this.hide = this.hide.bind(this)
	}

	hide() {
		this.props.onHide()
	}

	handleConvertToTaskClick() {
		this.props.onConvertToTask()
		this.hide()
	}

	handlePostNoteClick() {
		this.props.onPostNote()
		this.hide()
	}

	get body() {
		return <h5>Since this is a maintenance issue, would you like to create a task?</h5>
	}

	get footer() {
		return <Grid fluid={true}>
			<Row>
				<Col>
					<Button
						className="pull-left"
						bsStyle="primary"
						onClick={this.handleConvertToTaskClick}>Convert to Task</Button>

					<Button
						className="pull-left"
						bsStyle="link"
						onClick={this.handlePostNoteClick}>Post Note</Button>

					<Button onClick={this.hide}>Cancel</Button>
				</Col>
			</Row>
		</Grid>
	}

	render() {
		return <Modal
			className="convert-to-task-modal"
			show={this.props.show}
			onHide={this.hide}
			body={this.body}
			footer={this.footer} />
	}
}

ConvertToTaskModal.propTypes = {
	show: React.PropTypes.bool,
	onHide: React.PropTypes.func,
	onConvertToTask: React.PropTypes.func,
	onPostNote: React.PropTypes.func
}

ConvertToTaskModal.defaultProps = {
	show: false,
	onHide: () => null,
	onConvertToTask: () => null,
	onPostNote: () => null
}

export default ConvertToTaskModal
