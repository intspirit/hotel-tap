'use strict'

import React from 'react'
import Log from 'loglevel'
import _ from 'lodash'
import {Panel, Collapse, Alert} from 'react-bootstrap'
import {DescriptionEntry} from '../core'
import TagsSelectorModal from '../core/tags/TagsSelectorModal'
import UsersSelectorModal from '../core/users/UsersSelectorModal'
import appController from '../../core/appController'
import PostEntryComponent from '../../../shared/components/postEntry'
import {GAEvents, postTypes, assignmentTypes} from '../../../shared/constants'
import {logEventForGA} from '../../functions'
import db from '../../../shared/core/db'
import Select from '../core/Select'
import DepartmentPicker from './DepartmentPicker'
import ConvertToTaskModal from './ConvertToTaskModal'
import PostEntryActions from './PostEntryActions'
import EditTags from './EditTags'
import EditUsers from './EditUsers'
import {utils, dateTimeHelper} from '../../../shared/core'
import DateTimePicker from '../core/DateTimePicker'
import Scroller from '../../core/scroller'

const assignFullName = (user) => (
{
	id: user.id,
	name: utils.getFullName(user)
})

/**
 * Post entry component.
 *
 * @param {Object}   props
 * @param {String}   props.board
 * @param {Number}   props.id
 * @param {String}   props.typeId
 * @param {Array}    props.tags
 * @param {Array}    props.users
 * @param {Boolean}  props.guestComplaint
 * @param {String}   props.assignmentType
 * @param {Number}   props.assignmentId
 * @param {String}   props.subject
 * @param {String}   props.dueDateTime
 * @param {String}   props.description
 * @param {Array}    props.attachments
 * @param {Number}   props.createdBy
 */
class PostEntry extends React.Component {

	constructor(props) {
		super(props)

		this.postEntryComponent = new PostEntryComponent(appController, this, this.props)

		this.state = Object.assign(this.postEntryComponent.state, {
			selectedUsers: this.postEntryComponent.selectedUsers,
			selectedTags: this.postEntryComponent.selectedTags,
			isTagsModalVisible: false,
			isUsersModalVisible: false,
			isConvertToTaskModalVisible: false,
			departments: [],
			employees: [],
			selectedEmployeeId: 0,
			selectedDepartmentId: 0,
			postLoading: false,
			errorMessage: ''
		})

		this.handleAlertDismiss = this.handleAlertDismiss.bind(this)
		this.handleCreatePost = this.handleCreatePost.bind(this)
		this.handleTogglePostType = this.handleTogglePostType.bind(this)
		this.handlePostNote = this.save.bind(this)
		this.processSelectedTags = this.processSelectedTags.bind(this)
		this.processSelectedUsers = this.processSelectedUsers.bind(this)
		this.showTagsModal = this.showTagsModal.bind(this)
		this.showUsersModal = this.showUsersModal.bind(this)
		this.hideTagsModal = this.hideTagsModal.bind(this)
		this.hideUsersModal = this.hideUsersModal.bind(this)
		this.hideConvertToTaskModal = this.hideConvertToTaskModal.bind(this)
	}

	showTagsModal() {
		this.setState({
			isTagsModalVisible: true
		})
	}

	hideTagsModal() {
		this.setState({
			isTagsModalVisible: false
		})
	}

	showUsersModal() {
		this.setState({
			isUsersModalVisible: true
		})
	}

	hideUsersModal() {
		this.setState({
			isUsersModalVisible: false
		})
	}

	showConvertToTaskModal() {
		this.setState({
			isConvertToTaskModalVisible: true
		})
	}

	hideConvertToTaskModal() {
		this.setState({
			isConvertToTaskModalVisible: false
		})
	}

	componentDidMount() {
		this.getUsers()
		this.getDepartments()
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.board !== nextProps.board) {
			this.postEntryComponent.setBoard(nextProps.board)
		}
	}

	async getUsers() {
		try {
			const users = await db.getUsers()
			const employees = _.chain(users).
				map(assignFullName).
				sortBy(x => x.name).
				value()

			this.setState({
				employees
			})
		} catch (err) {
			Log.error(`PostEntry|getUsers|error:${err}`)
		}
	}

	//TODO: vvs p2 REFACTOR - this should not get loaded for every board when no entry will be made
	async getDepartments() {
		try {
			const departments = await db.getDepartments()
			this.setState({
				departments
			})
		} catch (err) {
			Log.error(`PostEntry|getDepartments|error:${err}`)
		}
	}

	processSelectedTags(selectedTags) {
		this.postEntryComponent.selectedTags = selectedTags
		this.setState({
			selectedTags,
			tagNames: this.postEntryComponent.tagNames
		})
	}

	processSelectedUsers(selectedUsers) {
		this.postEntryComponent.selectedUsers = selectedUsers
		this.setState({
			selectedUsers,
			userNames: this.postEntryComponent.userNames
		})
	}

	updateState(newState) {
		if (newState.assignmentType) {
			this.setAssignmentState(newState)
		} else {
			this.setState(newState)
		}
	}

	setAssignmentState(newState) {
		const state = newState.assignmentType === assignmentTypes.DEPARTMENT ?
			{
				selectedEmployeeId: 0,
				selectedDepartmentId: newState.assignmentId
			} :
			{
				selectedEmployeeId: newState.assignmentId,
				selectedDepartmentId: 0
			}

		this.setState(state)
	}

	updateSelectedTags(selectedTags) {
		this.processSelectedTags(selectedTags)
	}

	setErrorMessage(errorMessage) {
		this.scrollToTop()
		this.setState({errorMessage})
	}

	resetErrorMessage() {
		this.setState({
			errorMessage: ''
		})
	}

	resetAttachments() {
		this.refs['post-entry-actions'].resetAttachmentsState()
	}

	reset() {
		this.postEntryComponent.reset()
		this.resetErrorMessage()
		this.resetAttachments()
		this.setState({
			selectedUsers: [],
			selectedTags: [],
			selectedEmployeeId: 0,
			selectedDepartmentId: 0
		})
	}

	scrollToTop() {
		if (!this.node) return

		Scroller.scrollTo(this.node)
	}

	async save() {
		const errorMessage = this.postEntryComponent.validate()
		if (errorMessage) {
			this.setErrorMessage(errorMessage)

			return
		}

		try {
			this.startLoading()

			const serviceResponse = await this.postEntryComponent.save()

			this.endLoading()

			if (serviceResponse.inserted) {
				if (this.state.typeId === postTypes.NOTE) {
					logEventForGA(GAEvents.categories.POST, GAEvents.actions.POST_NOTE)
				} else {
					logEventForGA(GAEvents.categories.POST, GAEvents.actions.POST_TASK)
				}
				this.reset()
				appController.postCreated(serviceResponse.postId)
			}

			if (serviceResponse.updated) {
				logEventForGA(GAEvents.categories.POST, GAEvents.actions.NOTE_TO_TASK)
				appController.postConvertedToTask(serviceResponse.postId)
			}
		} catch (err) {
			this.setErrorMessage('Something wrong with saving post')
			this.endLoading()
			Log.error(`PostEntry|save|error:${err}`)
		}
	}

	async handleCreatePost() {
		if (this.postEntryComponent.isMaintenanceIssue) {
			this.showConvertToTaskModal()

			return
		}

		await this.save()
	}

	startLoading() {
		this.setState({postLoading: true})
	}

	endLoading() {
		this.setState({postLoading: false})
	}

	handleTogglePostType() {
		const typeId = this.state.typeId === postTypes.NOTE ? postTypes.TASK : postTypes.NOTE

		if (typeId === postTypes.TASK) {
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.CONVERT_TO_TASK)
		} else {
			logEventForGA(GAEvents.categories.POST, GAEvents.actions.CONVERT_TASK_CANCEL)
		}

		this.postEntryComponent.typeIdChanged(typeId)
	}

	handleAlertDismiss() {
		this.resetErrorMessage()
	}

	isPrivatePostVisible() {
		return this.state.typeId === postTypes.NOTE && !utils.isDepartmentBoard(this.props.board)
	}

	isTogglePostButtonVisible() {
		return this.props.id === -1
	}

	isCancelButtonVisible() {
		return this.props.id > -1
	}

	render() {
		//TODO: vvs p2 REFACTOR : throw & re-write: big, why core not npm?
		return (
			<Panel className="post-form-wrapper">
				<form className="post-form smart-form" ref={node => {
					this.node = node

					return node
				}}>
					{this.state.errorMessage ? <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
						{this.state.errorMessage}
					</Alert> : null}
					<fieldset className="form-elements-warpper">
						<section>
							<EditTags
								board={this.props.board}
								tags={this.state.selectedTags}
								onEdit={this.showTagsModal} />
						</section>

						<section>
							<EditUsers
								users={this.state.selectedUsers}
								onEdit={this.showUsersModal} />
						</section>

						<section>
							<div className="inline-group">
								<label className="checkbox">
									<input
										type="checkbox"
										checked={this.state.guestComplaint}
										onChange={ev => this.postEntryComponent.guestComplaintChanged(ev.target.checked)} />
									<i />
									Guest complaint
								</label>

								{this.isPrivatePostVisible() ?
									<label className="checkbox pull-right">
										<input
											type="checkbox"
											checked={this.state.privatePost}
											onChange={ev => this.postEntryComponent.privatePostChanged(ev.target.checked)} />
										<i />
										Private
									</label> :
									null
								}
							</div>
						</section>

						<Collapse in={this.state.typeId === postTypes.TASK}>
							<div>
								<section style={{marginBottom: 0}}>
									<label className="label" style={{marginBottom: 0}}>Assign to:</label>
								</section>
								<div className="row">
									<section className="col col-4">
										<Select
											placeHolderText="Employee"
											dataValueField="id"
											dataTextField="name"
											items={this.state.employees}
											selected={this.state.selectedEmployeeId}
											onChange={ev => this.postEntryComponent.employeeIdChanged(ev.target.value)} />
									</section>

									<section className="col col-8">
										<DepartmentPicker
											departments={this.state.departments}
											selected={this.state.selectedDepartmentId}
											onChange={value => this.postEntryComponent.departmentIdChanged(value)} />
									</section>
								</div>

								<section>
									<label className="input">
										<input type="text"
											placeholder="Subject"
											value={this.state.subject}
											onChange={ev => this.postEntryComponent.subjectChanged(ev.target.value)} />
									</label>
								</section>

								<section className="clearfix">
									<label className="input pull-left">
										<DateTimePicker
											timeFormat={false}
											closeOnSelect={true}
											inputProps={{
												placeholder: 'Due Date'
											}}
											value={this.state.dueDate}
											isValidDate={dateTime => dateTimeHelper.validateDueDateTime(dateTime)}
											onChange={::this.postEntryComponent.dueDateChanged} />
										<i role="button" className="icon-append fa fa-calendar"/>
									</label>
									<label className="input pull-left time-picker">
										<DateTimePicker
											dateFormat={false}
											inputProps={{
												placeholder: 'Due Time'
											}}
											value={this.state.dueTime}
											onChange={::this.postEntryComponent.dueTimeChanged} />
										<i role="button" className="icon-append fa fa-clock-o"/>
									</label>
								</section>
							</div>
						</Collapse>

						<section>
							<label className="textarea textarea-resizable">
								<DescriptionEntry
									className="form-control description-entry"
									value={this.state.description}
									placeholder={`Write a note or create a task by selecting one Department tag and one Location tag.
										<br/>
										If this is a guest complaint, check off the Guest Complaint box`}
									onChange={ev => this.postEntryComponent.descriptionChanged(ev.target.value)} />
							</label>
						</section>

						<section className="post-controls">
							<PostEntryActions
								ref="post-entry-actions"
								typeId={this.state.typeId}
								attachments={this.state.attachments}
								showToggleButton={this.isTogglePostButtonVisible()}
								showCancelButton={this.isCancelButtonVisible()}
								onAttachmentsChanged={attachments => this.postEntryComponent.attachmentsChanged(attachments)}
								onPost={this.handleCreatePost}
								onToggle={this.handleTogglePostType}
								onCancel={this.props.onClose}
								disabledSubmit={this.state.postLoading}
							/>
						</section>
					</fieldset>
				</form>

				{this.state.isTagsModalVisible ?
					<TagsSelectorModal
						board={this.props.board}
						show={this.state.isTagsModalVisible}
						departmentsTabDisabled={this.state.privatePost}
						selectedTags={this.state.selectedTags}
						onProcessSelected={this.processSelectedTags}
						onHide={this.hideTagsModal}
					/> :
					null
				}

				{this.state.isUsersModalVisible ?
					<UsersSelectorModal
						show={this.state.isUsersModalVisible}
						selectedUsers={this.state.selectedUsers}
						onProcessSelected={this.processSelectedUsers}
						onHide={this.hideUsersModal}
					/> :
					null
				}

				{this.state.isConvertToTaskModalVisible ?
					<ConvertToTaskModal
						show={this.state.isConvertToTaskModalVisible}
						onConvertToTask={this.handleTogglePostType}
						onPostNote={this.handlePostNote}
						onHide={this.hideConvertToTaskModal}
					/> :
					null
				}

			</Panel>
		)
	}
}

PostEntry.propTypes = {
	board: React.PropTypes.string,
	id: React.PropTypes.number,
	typeId: React.PropTypes.string,
	tags: React.PropTypes.array,
	users: React.PropTypes.array,
	guestComplaint: React.PropTypes.bool,
	privatePost: React.PropTypes.bool,
	assignmentType: React.PropTypes.string,
	assignmentId: React.PropTypes.number,
	subject: React.PropTypes.string,
	dueDateTime: React.PropTypes.string,
	description: React.PropTypes.string,
	attachments: React.PropTypes.array,
	onClose: React.PropTypes.func,
	createdBy: React.PropTypes.number
}

PostEntry.defaultProps = {
	board: '',
	id: -1,
	onClose: () => null
}

export default PostEntry
