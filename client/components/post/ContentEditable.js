'use strict'

import React from 'react'

//TODO: vvs p2 REFACTOR - bad name, no need.
/**
 * Content editable component
 */
class ContentEditable extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			content: this.props.content
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			content: nextProps.content
		})
	}

	render() {
		return <label className="label contenteditable">
			<i className={`icon-prepend fa ${this.props.icon}`}/>
			<div className="content">
				{this.state.content}
			</div>
			<i role="button" className="icon-append fa fa-pencil-square-o" onClick={this.props.onEdit}/>
		</label>
	}
}

ContentEditable.propTypes = {
	content: React.PropTypes.oneOfType([
		React.PropTypes.string,
		React.PropTypes.element
	]),
	icon: React.PropTypes.string,
	onEdit: React.PropTypes.func
}

ContentEditable.defaultProps = {
	content: '',
	icon: '',
	onEdit: () => null
}

export default ContentEditable
