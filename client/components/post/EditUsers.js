'use strict'

import React from 'react'
import PostUsers from '../core/PostUsers'
import ContentEditable from './ContentEditable'

/**
 * Renders a form component to view / edit users
 */
class EditUsers extends React.Component {
	map(users) {
		return users.map(x => (
			{
				userId: x.id,
				fullName: x.name
			}
		))
	}

	get content() {
		return this.props.users.length > 0 ?
			<PostUsers users={this.map(this.props.users)} showLabel={false} /> :
			<span className="text-muted">Click edit to copy to...</span>
	}

	render() {
		return <ContentEditable icon="fa-users" content={this.content} onEdit={this.props.onEdit} />
	}
}

EditUsers.propTypes = {
	users: React.PropTypes.array,
	onEdit: React.PropTypes.func
}

EditUsers.defaultProps = {
	onEdit: () => null
}

export default EditUsers
