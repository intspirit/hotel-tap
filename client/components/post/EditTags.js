'use strict'

import React from 'react'
import PostTags from '../core/PostTags'
import ContentEditable from './ContentEditable'
import {utils} from '../../../shared/core'

/**
 * Renders a form component to view / edit tags
 */
class EditTags extends React.Component {
	map(tags) {
		return tags.map(x => (
			{
				id: x.id,
				name: x.tagName.replace('#', ''),
				typeId: x.groupId,
				typeName: x.groupName.replace('@', '')
			}
		))
	}

	get placeholder() {
		return `Click edit to select tags${utils.isDepartmentBoard(this.props.board) ?
			'. Department and locations tags are required...' :
			''}`
	}

	get content() {
		return this.props.tags.length > 0 ?
			<PostTags tags={this.map(this.props.tags)} showLabel={false} /> :
			<span className="text-muted">{this.placeholder}</span>
	}

	render() {
		return <ContentEditable icon="fa-tags" content={this.content} onEdit={this.props.onEdit} />
	}
}

EditTags.propTypes = {
	board: React.PropTypes.string,
	tags: React.PropTypes.array,
	onEdit: React.PropTypes.func
}

EditTags.defaultProps = {
	board: '',
	tags: []
}

export default EditTags
