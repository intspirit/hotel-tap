'use strict'

import React from 'react'
import {OverlayTrigger, Tooltip} from 'react-bootstrap'
import classnames from 'classnames'

class DepartmentPicker extends React.Component {
	handleClick(department, e) {
		e.preventDefault()
		this.props.onChange(department.id)
	}

	render() {
		const tooltip = text => <Tooltip id="department-picker-tooltip">{text}</Tooltip>
		const getClassName = x => classnames(
			'department-picker-item',
			{
				selected: x.id === this.props.selected
			}
		)

		return <ul className="list-inline department-picker">
			{this.props.departments.map(x =>
				<OverlayTrigger key={x.id} placement="top" overlay={tooltip(x.name)}>
					<li className={getClassName(x)}>
						<a href="" onClick={this.handleClick.bind(this, x)}>
							<i className="fa fa-lg fa-fw custom-icon">
								<div style={{backgroundImage: `url(${x.iconFullUrl})`}}/>
							</i>
						</a>
					</li>
				</OverlayTrigger>
			)}
		</ul>
	}
}

DepartmentPicker.propTypes = {
	selected: React.PropTypes.number,
	departments: React.PropTypes.array,
	onChange: React.PropTypes.func
}

export default DepartmentPicker
