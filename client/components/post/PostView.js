'use strict'

import React from 'react'
import Log from 'loglevel'
import postService from '../../../shared/services/postService'
import appController from '../../core/appController'
import Post from '../board/Post'
import ContentTitle from '../core/ContentTitle'


/**
 * Post view component.
 */
class PostView extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			post: {},
			isLoaded: false
		}

		appController.postComponent = this
	}

	componentDidMount() {
		this.getPost()
	}

	async getPost() {
		try {
			this.setState({
				isLoaded: false
			})

			const postId = parseInt(this.props.params.id, 10)
			const serviceResponse = await postService.getPostById(postId)

			this.setState({
				isLoaded: true,
				post: serviceResponse.post
			})
		} catch (err) {
			Log.error(`PostView|getPost|error:${err}`)
		}
	}

	refreshPost() {
		this.getPost()
	}

	get content() {
		return this.state.isLoaded ?
			<Post post={this.state.post} /> :
			null
	}

	render() {
		return <div className="row">
			<div className="col-sm-12">
				<ContentTitle
					title="Post view"
					loaded={this.state.isLoaded}
				/>
				<article>
					<div className="feed panel">
						<div className="panel-body status">
							{this.content}
						</div>
					</div>
				</article>
			</div>
		</div>
	}
}

PostView.propTypes = {
	params: React.PropTypes.object
}

export default PostView
