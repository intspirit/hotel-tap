'use strict'

import {Route} from 'react-router'

export default class AdminRoute extends Route {
	static defaultProps = {
		onEnter(nextState, replaceState, callback) {
			if (typeof window === 'undefined') {
				callback()

				return
			}
			// @TODO check user for role Admin
			callback()
		}
	}
}

