'use strict'

import {Route, browserHistory} from 'react-router'
import authService from '../../../../shared/services/authService'

export default class SettingsRoute extends Route {
	static defaultProps = {
		async onEnter() {
			try {
				const response = await authService.loginAdmin()
				const url = `${response.adminUrl}/admin/forcesignin/${response.accessToken}`

				window.location.href = url
			} catch (error) {
				browserHistory.push('/')
			}
		}
	}
}

