'use strict'

import React from 'react'
import {Row} from 'react-bootstrap'
import ContentTitle from '../core/ContentTitle'
import {boardTypes} from '../../../shared/constants'
import Scroller from '../../core/scroller'


/**
 * Header for NotesAndTasksHeader component.
 * @constructor
 */
const NotesAndTasksHeader = () => <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<ContentTitle
		title="Creating Notes and Tasks"
		type={boardTypes.HELP}
	/>
</div>


/**
 * NotesAndTasks component.
 */
class NotesAndTasks extends React.Component {
	constructor(props) {
		super(props)

		// TODO: nd find the way to fetch video source
		this.state = {
			src: 'https://s3-us-west-2.amazonaws.com/htap-files-test/videos/note_and_task.mp4'
		}
	}

	componentDidMount() {
		Scroller.scrollToTop()
	}

	render() {
		return <Row>
			<div className="col-sm-12">
				<NotesAndTasksHeader />
			</div>

			<article className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div className="embed-responsive">
					<video controls className="col-lg-9 col-md-12 col-xs-12">
						<source src={this.state.src} type="video/mp4"/>
					</video>
				</div>
			</article>
		</Row>
	}
}

export default NotesAndTasks
