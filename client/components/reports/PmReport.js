'use strict'

import React from 'react'
import Log from 'loglevel'
import _ from 'lodash'
import {reportService} from '../../../shared/services'
import {dateTimeHelper} from '../../../shared/core'
import Report from './Report'
import GroupTitle from './GroupTitle'
import EmptyList from '../core/EmptyList'

/**
 * Checklist report group log item component.
 *
 * @param {Object} props
 * @param {String} props.title
 * @param {Array} props.items
 * @constructor
 */
const PmReportGroupLogItem = (props) => <div>
	<GroupTitle title={props.title} />
	<div className="table-responsive">
		<table className="table table-bordered whiteBackground">
			<thead>
				<tr className="tableHeaderBackground">
					<th className="col-sm-6">Room</th>
					<th className="col-sm-6">Completed By</th>
				</tr>
			</thead>
			<tbody>
				{props.items.map(x =>
					<tr key={x.id}>
						<td>{x.location.name}</td>
						<td>{Report.formatCompletedBy(x.completion)}</td>
					</tr>
				)}
			</tbody>
		</table>
	</div>
</div>

PmReportGroupLogItem.propTypes = {
	title: React.PropTypes.string,
	items: React.PropTypes.array
}

/**
 * Preventive maintenance report component.
 */
class PmReport extends React.Component {

	constructor(props) {
		super(props)

		this.state = {
			report: {},
			groupedLogs: {}
		}
	}

	componentDidMount() {
		this.getLogs()
	}

	async getLogs() {
		try {
			const reportId = parseInt(this.props.params.id, 10)
			const {report, logs} = await reportService.getPreventiveMaintenanceLogsByReportId(reportId)
			const groupedLogs = _.groupBy(logs, x =>
				dateTimeHelper.getDateTimeFormatted(x.completion.dateTime, 'MMM DD, YYYY'))

			this.setState({report, groupedLogs})
		} catch (err) {
			Log.error(`PmReport|getLogs|error:${err}`)
		}
	}

	render() {
		const keys = Object.keys(this.state.groupedLogs)

		return <Report title={`Preventive Maintenance Report - ${this.state.report.subject}`}>
			{keys.length > 0 ?
				Object.keys(this.state.groupedLogs).map(x =>
					<PmReportGroupLogItem
						key={x}
						title={x}
						items={this.state.groupedLogs[x]}
					/>
				) :
				<EmptyList />
			}
		</Report>
	}
}

PmReport.propTypes = {
	params: React.PropTypes.object
}

export default PmReport
