'use strict'

import React from 'react'
import {boardTypes} from '../../../shared/constants'
import ContentTitle from '../core/ContentTitle'
import Printer from '../../core/Printer'
import Scroller from '../../core/scroller'
import {dateTimeHelper} from '../../../shared/core'

/**
 * Report component.
 */
class Report extends React.Component {

	constructor(props) {
		super(props)

		this.handlePrintClick = this.handlePrintClick.bind(this)
	}

	static formatCompletedBy(completion) {
		const completedBy = dateTimeHelper.getDateTimeFormatted(completion.dateTime, 'h:mm a')

		return `${completion.completedBy.fullName} at ${completedBy}`
	}

	componentDidMount() {
		Scroller.scrollToTop()
	}

	handlePrintClick() {
		const printer = new Printer()
		printer.print(this.node)
	}

	render() {
		return <div className="row" ref={node => {
			this.node = node

			return node
		}}>
			<div className="col-sm-12">
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<button
						type="button"
						className="btn pull-right btn-primary btn-sm btn-print not-printable"
						onClick={this.handlePrintClick}>Print</button>
					<ContentTitle type={boardTypes.REPORTS} title={this.props.title} />
				</div>
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<article>
						<div className="report panel panel-default">
							<div className="panel-body status">
								{this.props.children}
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
	}
}

Report.propTypes = {
	title: React.PropTypes.string
}

export default Report
