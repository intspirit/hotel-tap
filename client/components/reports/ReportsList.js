'use strict'

import React from 'react'
import {Link} from 'react-router'
import Log from 'loglevel'
import {reportService} from '../../../shared/services'
import {boardTypes, postTypes} from '../../../shared/constants'
import ContentTitle from '../core/ContentTitle'
import Scroller from '../../core/scroller'
import GroupTitle from './GroupTitle'

/**
 * BaseReportsList component.
 *
 * @param {Object} props
 * @param {String} props.title
 * @param {String} props.url
 * @param {Array} props.items
 * @constructor
 */
const BaseReportsList = props =>
	<div className="panel panel-default reports-list">
		<div className="panel-body status">
			<GroupTitle title={props.title} />
			<div className="table-responsive">
				<table className="table table-bordered whiteBackground">
					<thead>
						<tr className="tableHeaderBackground">
							<th>Maintenance Name</th>
							<th className="col-sm-1">Criteria</th>
						</tr>
					</thead>
					<tbody>
						{props.items.map(x =>
							<tr key={x.id} className="reports-list-item">
								<td>{x.subject}</td>
								<td className="criteria">
									<Link to={props.url.replace(':id', x.id)}>90 days</Link>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</div>
		</div>
	</div>

BaseReportsList.propTypes = {
	title: React.PropTypes.string,
	url: React.PropTypes.string,
	items: React.PropTypes.array
}

/**
 * PreventiveMaintenanceList component.
 *
 * @param {Object} props
 * @param {Array} props.items
 * @constructor
 */
const PreventiveMaintenanceList = props =>
	<BaseReportsList
		typeId={postTypes.PM}
		title="Preventive Maintenance"
		url="/reports/pm/:id"
		items={props.items.filter(x => x.typeId === postTypes.PM)}
	/>

PreventiveMaintenanceList.propTypes = {
	items: React.PropTypes.array
}

/**
 * ChecklistList component.
 *
 * @param {Object} props
 * @param {Array} props.items
 * @constructor
 */
const ChecklistList = props =>
	<BaseReportsList
		typeId={postTypes.CHECKLIST}
		title="Checklists"
		url="/reports/check-lists/:id"
		items={props.items.filter(x => x.typeId === postTypes.CHECKLIST)}
	/>

ChecklistList.propTypes = {
	items: React.PropTypes.array
}

/**
 * ReportsList component.
 */
class ReportsList extends React.Component {

	constructor(props) {
		super(props)
		this.state = this.getDefaultState()
	}

	getDefaultState() {
		return {
			reports: []
		}
	}

	componentDidMount() {
		this.getReports()
		Scroller.scrollToTop()
	}

	async getReports() {
		try {
			const reports = await reportService.getAll()

			this.setState({reports})
		} catch (err) {
			Log.error(`ReportsList|getReports|error:${err}`)
		}
	}

	render() {
		return <div className="row">
			<div className="col-sm-12">
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<ContentTitle title="Reports" type={boardTypes.REPORTS}/>
				</div>
				<div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<article>
						<div className="feed panel reports">
							<PreventiveMaintenanceList items={this.state.reports} />
							<ChecklistList items={this.state.reports} />
						</div>
					</article>
				</div>
			</div>
		</div>
	}
}

export default ReportsList
