'use strict'

import React from 'react'

/**
 * Group title component for reports
 *
 * @param {Object} props
 * @param {String} props.title
 * @constructor
 */
const GroupTitle = (props) => <div className="who clearfix">
	<h4>{props.title}</h4>
</div>

GroupTitle.propTypes = {
	title: React.PropTypes.string
}

export default GroupTitle


