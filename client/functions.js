'use strict'

import ReactGA from 'react-ga'


/**
 * Function that refactor url by excluding /:id in it
 *
 * @param {string} path
 * @returns {string}
 */
function getUrlWithoutParams(path) {
	let newPath = path
	const paramsIdRegExp = /\/[0-9]+$/
	if (path.search(paramsIdRegExp) > 0)
		newPath = path.substr(0, path.search(paramsIdRegExp))

	return newPath
}

/**
 * Function for identify user in UserVoice feedback service
 *
 * @param {Object} user
 * @param {String} user.email
 * @param {String} user.fullName
 * @returns {void}
 */
export function identifyUserVoice(user) {
	if (!window || !window.UserVoice || !user) return

	window.UserVoice.push(['identify', {
		email: user.email || null,
		name: user.fullName
	}])
}

/**
 * Function for close UserVoice widget
 *
 * @returns {void}
 */
export function closeUserVoiceWidget() {
	if (!window || !window.UserVoice) return

	window.UserVoice.hide()
}

/**
 * Function for logging pages for Google Analytics
 *
 * @returns {void}
 */
export function logPageViewForGA() {
	const path = getUrlWithoutParams(window.location.pathname)
	ReactGA.set({page: path})
	ReactGA.pageview(path)
}

/**
 * Function for logging events for Google Analytics
 *
 * @param {string} category
 * @param {string} action
 * @param {string=} label
 * @returns {void}
 */
export function logEventForGA(category, action, label) {
	if (arguments.length === 2) {
		ReactGA.event({category, action})
	} else {
		ReactGA.event({category, action, label})
	}
}
