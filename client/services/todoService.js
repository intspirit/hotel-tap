'use strict'

import serviceBase from './serviceBase'

export default {

	/**
	 * Gets to do list
	 * @param {string} type - {department || employee}
	 * @param {string} id - {department id || 'me' for the current employee}
	 * @returns {*}
	 */
	get: (type, id = '') => serviceBase.get(`/api/todo/${type}/${id}`)

}
