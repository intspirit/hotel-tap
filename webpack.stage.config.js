'use strict'

const
	Webpack = require('webpack'),
	environment = 'stage'

//noinspection JSUnresolvedFunction
let plugins = [
	new Webpack.DefinePlugin({
		"process.env": {
			NODE_ENV: JSON.stringify(environment)
		}
	}),
	new Webpack.optimize.CommonsChunkPlugin({
		names: ['vendors'],
		minChunks: Infinity
	}),
	new Webpack.optimize.UglifyJsPlugin({
		compress: { warnings: false }
	})
]

//noinspection JSUnresolvedFunction
module.exports = {
	devtool: 'source-map',
	context: `${__dirname}/client`,
	entry: {
		bundle: './components/Index.js',
		vendors: [
			'react',
			'react-router',
			'react-bootstrap',
			'moment',
			'lodash'
		]
	},
	output: {
		path: `${__dirname}/build/public/js`,
		filename: '[name].js'
	},
	module: {
		loaders: [
			{
				test: /.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules|bower_components)/,
				loaders: [
					'react-hot',
					'babel?presets[]=stage-0,presets[]=react,presets[]=es2015'
				]
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /(datepicker|loader|datetime)\.css$/,
				loaders: [
					'style', 'css',
				]
			}
		]
	},

	resolve: {
		extensions: ['', '.js', '.jsx', '.json']
	},
	plugins: plugins
}
