var HotelTap = HotelTap || {};

(function () {
	'use strict';

	var ENTER_KEY = 13;

	HotelTap.TaskRow = React.createClass({

		update: function(to, from) {
			var data = this.props.data.items;
			data.splice(to, 0, data.splice(from,1)[0]);
			this.props.sort(data, to);
		},
		sortEnd: function() {
			this.props.sort(this.props.data.items, undefined);
		},
		sortStart: function(e) {
			this.dragged = e.currentTarget.dataset ?
				e.currentTarget.dataset.id :
				e.currentTarget.getAttribute('data-id');
			e.dataTransfer.effectAllowed = 'move';
			try {
				e.dataTransfer.setData('text/html', null);
			} catch (ex) {
				e.dataTransfer.setData('text', '');
			}
		},
		move: function(over,append) {
			var to = Number(over.dataset.id);
			var from = this.props.data.dragging != undefined ? this.props.data.dragging : Number(this.dragged);
			if(append) to++;
			if(from < to) to--;
			this.update(to,from);
		},
		dragOver: function(e) {
			e.preventDefault();
			var over = e.currentTarget;
			var relX = e.clientX - over.getBoundingClientRect().left;
			var relY = e.clientY - over.getBoundingClientRect().top;
			var height = over.offsetHeight / 2;
			var placement = this.placement ? this.placement(relX, relY, over) : relY > height;
			this.move(over, placement);
		},
		isDragging: function() {
			return this.props.data.dragging == this.props.key
		},

		getInitialState: function () {
			return {
				task: this.props.model
			}
		},

		onChange: function(event) {
			//noinspection JSUnresolvedVariable
			var value = event.target.value.trim();
			if (value === '') return;
			this.state.task.subject = value;
			this.setState({task: this.state.task});
		},

		onDelete: function(event) {
			this.props.onDelete(event.target.id);
		},

		render: function() {

			return (
				<li key={1} className="col-md-12 col-xs-12">
					<div className="task-wrapper">
						<input
							className="transparentTaskInput co1l-md-8"
							id={this.state.task.id}
							onChange={this.onChange}
							value={this.state.task.subject}/>
						<div className="actionIcons pull-right">
								<span id={this.state.task.id}
								      className="glyphicon glyphicon-trash"
								      title="Delete"
								      onClick={this.onDelete}/>
								<span
									className="glyphicon glyphicon-th hidden"
									title="Rearrange"/>
						</div>
					</div>
				</li>
			)
		}
	});

	/**
	 * ChecklistTasks component.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	HotelTap.ChecklistTasks = React.createClass({
		newId: -1,

		getInitialState: function () {
			return {
				model: this.props.model,
				newTask: ''
			}
		},

		onNewTaskChange: function (event) {
			this.setState({newTask: event.target.value});
		},

		onNewTaskKeyDown: function (event) {
			if (event.keyCode !== ENTER_KEY) {
				return;
			}

			event.preventDefault();

			var val = this.state.newTask.trim();

			if (val) {
				var newTask = new HotelTap.ChecklistTaskModel(this.newId, val);
				newTask.changeStatus = 'i';
				this.state.model.push(newTask);
				this.setState({
					newTask: ''
				});
				this.newId -= 1;
			}
		},

		onDelete: function(id) {
			//var id = event.target.id;
			for (var i =0; i < this.state.model.length; i++) {
				if (this.state.model[i].id == id) {
					if (id < 0)
						this.state.model.splice(i, 1);
					else
						this.state.model[i].changeStatus = 'd';
					break;
				}
			}
			this.setState({model: this.state.model});
		},

		render: function () {
			var taskRows = this.state.model.map(task => {
				if (task.changeStatus && task.changeStatus === 'd') return;

				return <HotelTap.TaskRow model={task} onDelete={this.onDelete} key={_.uniqueId()}/>;
			});

			return (
				<div className="form-group">
					<h1>Tasks</h1>
					<ul className="logTasks">
						{taskRows}
					</ul>
					<div className="row">
						<div className="col"></div>
					</div>
					<div className="col-md-12 col-xs-12 add-task-wrapper">
						<input
							value={this.state.newTask}
							onChange={this.onNewTaskChange}
							onKeyDown={this.onNewTaskKeyDown}
							className="form-control"
							placeholder="Add task and press [Enter]"/>
					</div>
					<div className="clearfix" />
				</div>
			);
		}
	});
})();
