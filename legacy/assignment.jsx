var HotelTap = HotelTap || {};

(function () {
	'use strict';
	/**
	 * Assignment component.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	HotelTap.Assignment = React.createClass({

		getInitialState: function () {
			return {
				model: this.props.model
			}
		},

		onEmployeeChange: function (event) {
			var model = this.state.model;
			//noinspection JSUnresolvedVariable
			model.employeeId = event.target.value;
			model.departmentId = -1;
			var c = 1;
			this.setState({model: model});
		},

		onDepartmentChange: function (event) {
			var model = this.state.model;
			model.employeeId = -1;
			model.departmentId = event.target.id;
			this.setState({model: model});
		},

		render: function () {

			const employeeOptions = this.state.model.employees.map(employee => {
				return <option value={employee.id} key={employee.id}>{employee.name}</option>
			});
			var departmentId = this.state.model.departmentId;
			var departmentOptions = this.state.model.departments.map(department => {
				return <img
					id={department.id}
					key={department.id}
					title={department.name}
					src={department.imageUrl}
					className={'department-image ' + (department.id == departmentId ? 'imgselected' : '')}
					onClick={this.onDepartmentChange}/>
			});

			return (
				<div className="elementswrap assignment">
					<div className="">
						<div className="pull-left select-employee-wrapper">
							<div className="boldLabel">Assign To:</div>

							<div className="employee">
								<select
									className="selectborder"
									id="assignedEmployeeId"
									value={this.state.model.employeeId}
									onChange={this.onEmployeeChange}>

									<option value="-1" key="-1">Employee Name</option>
									{employeeOptions}
								</select>
							</div>
						</div>

						<div className="pull-left" id="assignDepartments">
							{departmentOptions}
						</div>
						<div className="clearfix visible-sm"/>
					</div>
					<div className="clearfix"/>
				</div>
			);
		}
	});
})();
