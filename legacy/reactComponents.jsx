const
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col;

class ContentEditable extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			placeholder: this.props.placeholder
		};
		this.emitChange = this.emitChange.bind(this);
	}

	render() {
		return React.createElement(
			this.props.tagName || 'div',
			Object.assign({}, this.props, {
				ref: (e) => this.htmlEl = e,
				onInput: this.emitChange,
				onBlur: this.emitChange,
				onFocus: this.emitChange,
				contentEditable: !this.props.disabled,
				dangerouslySetInnerHTML: {__html: this.props.html || this.state.placeholder}
			}),
			this.props.children);
	}

	shouldComponentUpdate(nextProps) {
		return !this.htmlEl || nextProps.html !== this.htmlEl.innerHTML ||
			this.props.disabled !== nextProps.disabled;
	}

	componentDidUpdate() {
		if ( this.htmlEl && this.props.html !== this.htmlEl.innerHTML ) {
			this.htmlEl.innerHTML = this.props.html;
		}
	}

	emitChange(evt) {
		if (!this.htmlEl) return;
		var html = this.htmlEl.innerHTML;

		if (evt.type == 'blur') {
			this.setState({placeholder: this.props.placeholder});
			if (html == '') html = this.props.placeholder || '';
		}
		if (evt.type == 'focus') {
			this.setState({placeholder: null});
			if (html == this.props.placeholder) html = '';
		}

		if (this.props.onChange && (html !== this.lastHtml || evt.type === 'blur')) {
			evt.target = { value: html };
			this.props.onChange(evt);
		}
		this.lastHtml = html;
	}
}

/**
 * Label and value pair.
 */
const LabelValueView = (props) =>
	<Row>
		<Col xs={5} lg={2} xl={2} md={2}>
			<span>{props.label}</span>
		</Col>
		<Col xs={7} lg={10} xl={10} md={10}>
			<span style={{fontWeight: "normal !important;"}}>{props.value}</span>
		</Col>
	</Row>;


