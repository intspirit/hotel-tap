const
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col;

const VIEW_REPORT_URL = '/hotel/report/';
/**
 * ReportsList component.
 */
class ReportsList extends React.Component {
    constructor(params) {
        super(params);
        this.state = {
            roomLogs: [],
            checkLists: [],
        };

        this.roomLogsAreLoaded = false;
        this.checkListsAreLoaded = false;
    }

    getData() {
        reportService.getRoomLogs().then(roomLogs => {
            this.roomLogsAreLoaded = true;
            this.setState({roomLogs: roomLogs});
        });

        reportService.getChecklists().then(checkLists => {
            this.checkListsAreLoaded = true;
            this.setState({checkLists: checkLists});
        });
    }

    componentDidMount() {
        this.getData();
    }

    getNoDataContent() {
        return <div className="no-data">No data</div>
    }

    getLoadingContent() {
        return <div className="data-loading">Loading...</div>
    }

    reportRow(type, item) {
        let itemId = (type === 'checklists') ? item.checklist_id : item.id;
        let url = VIEW_REPORT_URL + type + '/' + itemId;
        return <Row className="report-item">
            <Col xs={10}>
                {item.name}
            </Col>
            <Col xs={2}>
                <a href={url} title="View report" className="view-report pull-right">
                    90 days
                </a>
            </Col>
        </Row>
    }

    render() {
        let roomLogs = this.getLoadingContent();
        if (this.roomLogsAreLoaded) {
            let hasNoContent = !this.state.roomLogs.length;
            roomLogs = hasNoContent ?
                this.getNoDataContent() :
                this.state.roomLogs.map(x => {
                    return this.reportRow('roomlogs', x);
            });
        }


        let checkLists = this.getLoadingContent();
        if (this.checkListsAreLoaded) {
            let hasNoContent = !this.state.checkLists.length;
            checkLists = hasNoContent ?
                this.getNoDataContent() :
                this.state.checkLists.map(x => {
                    return this.reportRow('checklists', x);
            });
        }
        return <div className="col-2 reports-wrapper">
            <section className="content-header content-h1">
                <h2>Reports</h2>
            </section>
            <div className="reports-list">
                <div className="roomlogs-wrapper">
                    <div className='roomlogs-header section-header'>Room Logs</div>
                    {roomLogs}
                </div>
                <div className="checklists-wrapper">
                    <div className='checklists-header section-header'>Checklists</div>
                    {checkLists}
                </div>
            </div>
        </div>
    }
}
if (typeof module !== 'undefined')
    module.exports = ReportsList;

/**
 * Renders component in the view.
 */
function render() {
    ReactDOM.render(<ReportsList />, document.getElementById('view'));
}

render();
