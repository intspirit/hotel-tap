const
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col;

/**
 * Report component.
 */
class Report extends React.Component {
    constructor(params) {
        super(params);

        const viewUrl = window.location.pathname;
        const urlParts = viewUrl.split('/');

        this.state = {
            type: urlParts[urlParts.length -2],
            id: urlParts[urlParts.length -1],
            reportData: {},
            reportTypeLabel: '',
            reportLabel: '',
        }

        this.print = this.print.bind(this);
    }

    getViewModel(report) {
        let groupedData = {};
        report.data.forEach(x => {
            let day = moment(x.completedAt).format('YYYY-MM-DD');
            groupedData[day] = groupedData[day] || [];
            groupedData[day].push({
                itemName: x.name,
                completedBy: [x.userFirstName, x.userMiddleName, x.userLastName].join(' '),
                completedAt: moment(x.completedAt).format('h:mm a')
            });
        });

        let typesMap = {
            roomlogs: 'Room Log Report',
            checklists: 'Checklist Report',
        }

        let viewModel = {
            reportData: groupedData,
            reportTypeLabel: typesMap[this.state.type] || '',
            reportLabel: report.reportName
        }
        return viewModel;
    }

    getData() {
        reportService.getReport(this.state.type, this.state.id).then(report => {
            let viewModel = this.getViewModel(report);
            this.setState(viewModel);
        }).catch(err => {
            window.location.href = '/hotel/reports';
        });
    }

    componentDidMount() {
        this.getData();
    }

    print() {
        this.printElement(document.getElementById("printIt"));
        window.print();
    }

    printElement(elem, append, delimiter) {
        var domClone = elem.cloneNode(true);

        var $printSection = document.getElementById("printSection");

        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            document.body.appendChild($printSection);
        }

        if (append !== true) {
            $printSection.innerHTML = "";
        }

        else if (append === true) {
            if (typeof(delimiter) === "string") {
                $printSection.innerHTML += delimiter;
            }
            else if (typeof(delimiter) === "object") {
                $printSection.appendChlid(delimiter);
            }
        }

        $printSection.appendChild(domClone);
    }

    getNoDataContent() {
        return <div className="no-data">No data</div>
    }

    getDataItemsView(items) {
        return items.map(x => {
            return <Row className="items-row">
                <Col xs={4} className="item-name column">
                    {x.itemName}
                </Col>
                <Col xs={8} className="item-completed-by column">
                    {x.completedBy} <small>at</small> {x.completedAt}
                </Col>
            </Row>
        });
    }

    getDataItemsHeader() {
        return <Row className="items-header">
            <Col xs={4} className="item-name-header column">
                {this.state.type === 'checklists' ? 'Name' : 'Room'}
            </Col>
            <Col xs={8} className="item-completed-by-header column">
                Completed By
            </Col>
        </Row>
    }

    render() {

        let view = Object.keys(this.state.reportData).map(x => {
            return <div className="date-data-wrapper">
                <div className="date section-header">{moment(x).format('MMM D, YYYY')}</div>
                {this.getDataItemsHeader()}
                <div className="date-data">
                    {this.getDataItemsView(this.state.reportData[x])}
                </div>
            </div>
        });

        return <div className="col-2 report-wrapper">
            <div id="printIt">
                <section className="content-header">
                    <div className="report-actions pull-right">
                        <a className="btn btn-link" href="/hotel/reports">Back to reports</a>
                        <div className="btn btn-primary" onClick={this.print}>Print</div>
                    </div>
                    <h2>{this.state.reportTypeLabel} - {this.state.reportLabel}</h2>
                    <div className="clearfix"></div>
                </section>
                <div className="report">
                    {view}
                </div>
            </div>
        </div>
    }
}
if (typeof module !== 'undefined')
    module.exports = Report;

/**
 * Renders component in the view.
 */
function render() {
    ReactDOM.render(<Report />, document.getElementById('view'));
}

render();
