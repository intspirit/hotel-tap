var HotelTap = HotelTap || {};

(function () {
	'use strict';

	/**
	 * Log name component.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	HotelTap.NameDescription = React.createClass({
		getInitialState: function () {
			return {
				name: this.props.model.name || '',
				description: this.props.model.description || '',
				placeholder: this.props.model.placeholder || this.getPlaceholder()
			};
		},

		onNameChange: function (event) {
			this.setState({name: event.target.value});
			if (event.target.value.trim() === '') return;
			this.props.model.name = event.target.value;
		},

		onDescriptionChange: function (event) {
			console.log(event.type );
			this.props.model.description = event.target.value;
			this.setState({description: event.target.value});

			if (event.type === 'blur') this.onChangeCompleted();
		},

		onChangeCompleted() {
			if (this.props.onChange) this.props.onChange(this.state.name.trim(), this.state.description.trim());
		},

		getPlaceholder() {
			return '<div class="placeholder">Description</div>';
		},

		render: function () {
			return (
				//TODO: margin-top
				<div className="form-group">
					<input
						className="form-control"
						placeholder={this.props.model.nameHint}
						onChange={this.onNameChange}
						onBlur={this.onChangeCompleted.bind(this)}
						value={this.state.name}
						autoFocus={true}/>
					<ContentEditable
						className="contentdiv depted-tags description"
						key="description"
						id="description"
						html={this.state.description}
						placeholder={this.state.placeholder}
						onChange={this.onDescriptionChange}/>
				</div>
			);
		}
	});
})();
