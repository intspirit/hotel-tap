'use strict';

var HotelTap = HotelTap || {};

(function () {
	'use strict';

	/**
  * TODO List Task.
  * @type {__React.ClassicComponentClass<P>}
  */

	var TodoListTaskView = React.createClass({
		displayName: 'TodoListTaskView',


		getInitialState: function getInitialState() {
			return {
				model: this.props.model,
				scheduleId: false,
				displayCommentForm: false,
				processing: false,
				commentText: '',
				displayAttachments: true,
				newAttachments: []
			};
		},

		onCompleteChange: function onCompleteChange(event) {
			var taskId = this.state.model.id;
			var action = $(event.target).prop('checked') ? 'complete' : 'uncomplete';
			$.ajax({
				url: '/api/tasks/' + taskId + '/' + action,
				dataType: 'json',
				type: 'POST',
				data: { roomScheduleId: this.props.scheduleId },
				success: function (data) {
					this.setState({ model: data });
					get_todo_list_ajax();
				}.bind(this),
				error: function (xhr, status, err) {
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		addComment: function addComment(event) {
			event.preventDefault();
			if (this.state.processing) {
				return;
			}

			this.setState({ processing: true });

			var data = new FormData();
			this.state.newAttachments.forEach(function (attachmentInput) {
				data.append(attachmentInput.id, attachmentInput);
			});

			data.append('comment', this.state.commentText);
			data.append('scheduleId', this.props.scheduleId);

			$.ajax({
				url: '/api/tasks/' + this.props.model.id + '/comments/', // @TODO: using props
				dataType: 'json',
				type: 'POST',
				data: data,
				cache: false,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string reques
				success: function (data) {
					this.setState({
						model: data,
						displayCommentForm: false,
						commentText: '',
						processing: false,
						newAttachments: []
					});
					get_todo_list_ajax();
				}.bind(this),
				error: function (xhr, status, err) {
					this.setState({ processing: false });
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		toggleForm: function toggleForm(event) {
			event.preventDefault();

			this.setState({ displayCommentForm: !this.state.displayCommentForm });
			this.adjustPosition(event);
		},

		adjustPosition: function adjustPosition(event) {
			var anchor = $(event.target).parents('.roomlog-task').find('.form-wrapper');
			if (this.state.displayCommentForm) {
				anchor = $(event.target).parents('.roomlog-task');
			}
			$('html, body').animate({
				scrollTop: anchor.offset().top - 100
			}, 500);
		},

		removeAttachment: function removeAttachment(attachment, event) {
			event.preventDefault();

			this.setState({
				'newAttachments': this.state.newAttachments.filter(function (item) {
					return attachment.id !== item.id;
				})
			});

			return false;
		},

		changeAttachement: function changeAttachement(files, event) {
			files.map(function (file) {
				file.id = _.uniqueId();
			});
			this.setState({ 'newAttachments': this.state.newAttachments.concat(files) });
		},

		commentChange: function commentChange(event) {
			this.setState({ commentText: event.target.value });
		},

		getAttachmentPreview: function getAttachmentPreview(attachment) {
			if (attachment.type.indexOf('image') > -1) return React.createElement('img', { src: attachment.preview });

			return React.createElement(
				'div',
				{ className: 'attachment-non-image' },
				React.createElement('div', { className: 'fa fa-file-text-o attachment-icon' }),
				React.createElement(
					'div',
					{ className: 'attachment-name' },
					attachment.name
				)
			);
		},

		render: function render() {
			var _this = this;

			return React.createElement(
				'div',
				{ key: this.state.model.id },
				React.createElement(
					'div',
					{ className: 'roomlog-task' },
					React.createElement(
						'div',
						{
							className: 'pull-right btn btn-default comment-btn',
							onClick: this.toggleForm,
							style: { display: this.state.displayCommentForm ? 'none' : 'block' } },
						React.createElement('i', { className: 'fa fa-comment comment-icon' }),
						'Comment'
					),
					React.createElement(
						'div',
						{ className: 'task-control' },
						React.createElement(
							'div',
							{ className: 'checkbox' },
							React.createElement(
								'label',
								null,
								React.createElement('input', {
									className: 'hidden',
									checked: this.state.model.completed,
									type: 'checkbox',
									onChange: this.onCompleteChange }),
								React.createElement('div', {
									className: classNames('fake-checkbox', { active: this.state.model.completed }) }),
								React.createElement(
									'span',
									{ className: 'task-subject' },
									this.state.model.subject
								)
							),
							React.createElement(
								'p',
								{
									className: 'completed-text',
									style: { display: this.state.model.completed ? 'block' : 'none' } },
								React.createElement(
									'span',
									{ className: 'completed-by' },
									this.state.model.completedBy
								),
								React.createElement(
									'span',
									{ className: 'completed-label' },
									' completed this task at '
								),
								React.createElement(
									'span',
									{ className: 'completed-date' },
									moment(this.state.model.completionDateTime).format('MMM D h:mm A')
								)
							)
						)
					),
					React.createElement(
						'div',
						{
							className: 'task-comments',
							style: { display: this.state.model.comments && this.state.model.comments.length ? 'block' : 'none' } },
						this.state.model.comments.map(function (comment) {
							return React.createElement(
								'table',
								{ key: comment.comment_id, className: 'commentdiv' },
								React.createElement(
									'tbody',
									null,
									React.createElement(
										'tr',
										null,
										React.createElement(
											'td',
											null,
											React.createElement(
												'div',
												{ className: 'comment-header' },
												React.createElement(
													'a',
													{ className: 'comment-author',
														href: "/hotel/empboard/" + comment.created_by },
													comment.author
												),
												' ',
												React.createElement(
													'span',
													null,
													comment.created_date_diff
												)
											),
											React.createElement(
												'div',
												{ className: 'comment-text' },
												comment.comment
											)
										)
									),
									React.createElement(
										'tr',
										null,
										React.createElement(
											'td',
											null,
											React.createElement(HotelTap.Attachments, { model: comment.attachments || [] })
										)
									)
								)
							);
						})
					),
					React.createElement(
						'div',
						{ className: 'form-wrapper' },
						React.createElement(
							'div',
							{ className: 'task-comment-form',
								style: { display: this.state.displayCommentForm ? 'block' : 'none' } },
							React.createElement('textarea', {
								name: 'comment',
								value: this.state.commentText,
								onChange: this.commentChange,
								className: 'comment-text' }),
							React.createElement('div', { className: 'clearfix' }),
							React.createElement(
								'div',
								{ className: 'attachments-preview' },
								this.state.newAttachments.map(function (attachment) {
									return React.createElement(
										'div',
										{ className: 'attachment-preview' },
										React.createElement(
											'div',
											{ className: 'attachment' },
											_this.getAttachmentPreview(attachment)
										),
										React.createElement(
											'div',
											{ className: 'attachment-actions' },
											React.createElement(
												'span',
												{ className: 'remove-attachment',
													onClick: _this.removeAttachment.bind(_this, attachment) },
												'Remove'
											)
										)
									);
								})
							),
							React.createElement(
								Dropzone,
								{ className: 'dropzone', onDrop: this.changeAttachement,
									style: { display: this.state.displayAttachments ? 'block' : 'none' } },
								React.createElement(
									'div',
									null,
									'Try dropping some files here, or click to select files to upload.'
								)
							),
							React.createElement('div', { className: 'clearfix' }),
							React.createElement(
								'div',
								{ className: 'pull-right comment-actions' },
								React.createElement(
									'button',
									{ className: 'btn btn-link btn-sm', onClick: this.toggleForm },
									'Cancel'
								),
								React.createElement(
									'button',
									{ className: 'btn btn-success btn-sm', onClick: this.addComment },
									'Save Comment'
								)
							),
							React.createElement('div', { className: 'clearfix' })
						)
					)
				),
				React.createElement('div', { className: 'clearfix' })
			);
		}
	});

	/**
  * TODO List View.
  * @type {__React.ClassicComponentClass<P>}
  */
	var TodoListView = React.createClass({
		displayName: 'TodoListView',


		getInitialState: function getInitialState() {
			return {
				schedule: {},
				roomLog: {},
				tasks: [],
				employees: []
			};
		},

		componentDidMount: function componentDidMount() {
			this.loadSchedule(this.props.roomScheduleId);
		},

		loadSchedule: function loadSchedule(scheduleId) {
			$.ajax({
				url: '/api/schedules/' + scheduleId,
				dataType: 'json',
				type: 'GET',
				success: function (data) {
					this.setState({
						schedule: data,
						roomLog: data.roomLog,
						tasks: data.tasks,
						employees: data.employees,
						commentForm: 0
					});
				}.bind(this),
				error: function (xhr, status, err) {
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		getTitle: function getTitle() {
			return this.state.roomLog.name + ' - ' + this.state.schedule.roomName;
		},

		isPointSystem: function isPointSystem() {
			return this.state.roomLog.type == 2;
		},

		render: function render() {
			var _this2 = this;

			if (this.state.tasks.length === 0) return null;
			if (this.isPointSystem()) return React.createElement(InspectionEntryContainer, { model: this.state });

			var backUrl = '/hotel/' + (this.state.roomLog.assignmentType == 'e' ? 'empboard' : 'board') + '/' + this.state.roomLog.assigneeId;

			return React.createElement(
				'div',
				{ id: 'todoListView' },
				React.createElement(BoardViewHeader, {
					url: backUrl,
					showNavigation: false,
					buttonLabel: 'Back to board',
					buttonLabelSmall: 'Back',
					header: this.getTitle() }),
				React.createElement(
					'div',
					{ className: this.state.schedule.pastDue ? 'past-due' : '' },
					'Due to:',
					React.createElement(
						'span',
						{ className: 'label label-default due-date-value' },
						moment(this.state.schedule.dueDateTime).format('MMM D')
					),
					React.createElement(
						'span',
						{ className: 'label label-danger past-due-mark' },
						'Past Due'
					)
				),
				React.createElement(
					'div',
					{ className: 'tasks-list' },
					this.state.tasks.map(function (task) {
						return React.createElement(TodoListTaskView, {
							key: task.id,
							model: task,
							roomLog: _this2.state.roomLog,
							scheduleId: _this2.state.schedule.id });
					})
				)
			);
		}
	});

	var roomScheduleId = $('#view').data('schedule');
	ReactDOM.render(React.createElement(TodoListView, { roomScheduleId: roomScheduleId }), document.getElementById('view'));
})();
//# sourceMappingURL=todoList.js.map