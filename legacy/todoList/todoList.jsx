var HotelTap = HotelTap || {};

(function () {
	'use strict';

	/**
	 * TODO List Task.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	var TodoListTaskView = React.createClass({

		getInitialState: function () {
			return {
				model: this.props.model,
				scheduleId: false,
				displayCommentForm: false,
				processing: false,
				commentText: '',
				displayAttachments: true,
				newAttachments: []
			};
		},

		onCompleteChange: function (event) {
			var taskId = this.state.model.id;
			var action = $(event.target).prop('checked') ? 'complete' : 'uncomplete';
			$.ajax({
				url: '/api/tasks/' + taskId + '/' + action,
				dataType: 'json',
				type: 'POST',
				data: {roomScheduleId: this.props.scheduleId},
				success: function (data) {
					this.setState({model: data});
					get_todo_list_ajax();
				}.bind(this),
				error: function (xhr, status, err) {
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		addComment: function (event) {
			event.preventDefault();
			if (this.state.processing) {
				return;
			}

			this.setState({processing: true});

			var data = new FormData();
			this.state.newAttachments.forEach(function (attachmentInput) {
				data.append(attachmentInput.id, attachmentInput);
			});

			data.append('comment', this.state.commentText);
			data.append('scheduleId', this.props.scheduleId);

			$.ajax({
				url: '/api/tasks/' + this.props.model.id + '/comments/', // @TODO: using props
				dataType: 'json',
				type: 'POST',
				data: data,
				cache: false,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string reques
				success: function (data) {
					this.setState({
						model: data,
						displayCommentForm: false
						, commentText: '',
						processing: false,
						newAttachments: []
					});
					get_todo_list_ajax();
				}.bind(this),
				error: function (xhr, status, err) {
					this.setState({processing: false});
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		toggleForm: function (event) {
			event.preventDefault();

			this.setState({displayCommentForm: !this.state.displayCommentForm});
			this.adjustPosition(event);
		},

		adjustPosition: function (event) {
			var anchor = $(event.target).parents('.roomlog-task').find('.form-wrapper');
			if (this.state.displayCommentForm) {
				anchor = $(event.target).parents('.roomlog-task');
			}
			$('html, body').animate({
				scrollTop: anchor.offset().top - 100
			}, 500);
		},

		removeAttachment: function (attachment, event) {
			event.preventDefault();

			this.setState({
				'newAttachments': this.state.newAttachments.filter(function (item) {
					return attachment.id !== item.id;
				})
			});

			return false;
		},

		changeAttachement: function (files, event) {
			files.map(function (file) {
				file.id = _.uniqueId();
			});
			this.setState({'newAttachments': this.state.newAttachments.concat(files)});
		},

		commentChange: function (event) {
			this.setState({commentText: event.target.value});
		},

		getAttachmentPreview: function(attachment) {
			if (attachment.type.indexOf('image') > -1) return <img src={attachment.preview}/>;

			return <div className="attachment-non-image">
				<div className="fa fa-file-text-o attachment-icon"></div>
				<div className="attachment-name">{attachment.name}</div>
			</div>;
		},

		render: function () {
			return (
				<div key={this.state.model.id}>
					<div className="roomlog-task">
						<div
							className="pull-right btn btn-default comment-btn"
							onClick={this.toggleForm}
							style={{display: this.state.displayCommentForm ? 'none' : 'block'}}>
							<i className="fa fa-comment comment-icon"/>
							Comment
						</div>
						<div className="task-control">
							<div className="checkbox">
								<label>
									<input
										className="hidden"
										checked={this.state.model.completed}
										type="checkbox"
										onChange={this.onCompleteChange}/>
									<div
										className={classNames('fake-checkbox', {active: this.state.model.completed})}>
									</div>
									<span className="task-subject">
										{this.state.model.subject}
									</span>
								</label>
								<p
									className="completed-text"
									style={{ display: this.state.model.completed ? 'block' : 'none'}}>
									<span className="completed-by">{this.state.model.completedBy}</span>
									<span className="completed-label"> completed this task at </span>
									<span className="completed-date">
										{moment(this.state.model.completionDateTime).format('MMM D h:mm A')}
									</span>
								</p>
							</div>
						</div>
						<div
							className="task-comments"
							style={{display: this.state.model.comments && this.state.model.comments.length ? 'block' : 'none'}}>
							{this.state.model.comments.map(function (comment) {
								return <table key={comment.comment_id} className="commentdiv">
									<tbody>
									<tr>
										<td>
											<div className="comment-header">
												<a className="comment-author"
												   href={"/hotel/empboard/" + comment.created_by}>{comment.author}</a>&nbsp;
												<span>{comment.created_date_diff}</span>
											</div>

											<div className="comment-text">{comment.comment}</div>
										</td>
									</tr>
									<tr>
										<td>
											<HotelTap.Attachments model={comment.attachments || []}/>
										</td>
									</tr>
									</tbody>
								</table>
							})}
						</div>
						<div className="form-wrapper">
							<div className="task-comment-form"
							     style={{display: this.state.displayCommentForm ? 'block' : 'none'}}>
								<textarea
									name="comment"
									value={this.state.commentText}
									onChange={this.commentChange}
									className="comment-text"/>

								<div className="clearfix"></div>
								<div className="attachments-preview">
									{this.state.newAttachments.map(attachment => {
										return <div className="attachment-preview">
											<div className="attachment">
												{this.getAttachmentPreview(attachment)}
											</div>
											<div className="attachment-actions">
												<span className="remove-attachment"
												      onClick={this.removeAttachment.bind(this, attachment)}>Remove
												</span>
											</div>
										</div>
									})}
								</div>
								<Dropzone className="dropzone" onDrop={this.changeAttachement}
								          style={{display: this.state.displayAttachments ? 'block' : 'none'}}>
									<div>Try dropping some files here, or click to select files to upload.</div>
								</Dropzone>
								<div className="clearfix"></div>

								<div className="pull-right comment-actions">
									<button className="btn btn-link btn-sm" onClick={this.toggleForm}>Cancel</button>
									<button className="btn btn-success btn-sm" onClick={this.addComment}>Save Comment
									</button>
								</div>
								<div className="clearfix"></div>
							</div>
						</div>
					</div>
					<div className="clearfix"></div>
				</div>
			);
		}
	});

	/**
	 * TODO List View.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	var TodoListView = React.createClass({

		getInitialState: function () {
			return {
				schedule: {},
				roomLog: {},
				tasks: [],
				employees: []
			};
		},

		componentDidMount: function () {
			this.loadSchedule(this.props.roomScheduleId);
		},

		loadSchedule: function (scheduleId) {
			$.ajax({
				url: `/api/schedules/${scheduleId}`,
				dataType: 'json',
				type: 'GET',
				success: function (data) {
					this.setState({
						schedule: data,
						roomLog: data.roomLog,
						tasks: data.tasks,
						employees: data.employees,
						commentForm: 0
					});
				}.bind(this),
				error: function (xhr, status, err) {
					console.error(this.props.url, status, err.toString());
				}.bind(this)
			});
		},

		getTitle: function () {
			return `${this.state.roomLog.name} - ${this.state.schedule.roomName}`
		},

		isPointSystem: function () {
			return this.state.roomLog.type == 2;
		},

		render: function () {
			if (this.state.tasks.length === 0) return null;
			if (this.isPointSystem()) return <InspectionEntryContainer model={this.state} />;

			const backUrl = '/hotel/' +
				(this.state.roomLog.assignmentType == 'e' ? 'empboard' : 'board') +
				'/' +
				this.state.roomLog.assigneeId;

			return (

				<div id="todoListView">
					<BoardViewHeader
						url={backUrl}
						showNavigation={false}
						buttonLabel="Back to board"
						buttonLabelSmall="Back"
						header={this.getTitle()}/>
					<div className={this.state.schedule.pastDue ? 'past-due' : ''}>
						Due to:
						<span className="label label-default due-date-value">
							{moment(this.state.schedule.dueDateTime).format('MMM D')}</span>
						<span className="label label-danger past-due-mark">Past Due</span>
					</div>

					<div className="tasks-list">
						{this.state.tasks.map(task => {
							return <TodoListTaskView
								key={task.id}
								model={task}
								roomLog={this.state.roomLog}
								scheduleId={this.state.schedule.id}/>;
						})}
					</div>
				</div>
			);
		}
	});

	var roomScheduleId = $('#view').data('schedule');
	ReactDOM.render(<TodoListView roomScheduleId={roomScheduleId}/>, document.getElementById('view'));

})();


