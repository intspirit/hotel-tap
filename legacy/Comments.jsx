const
	Utils = Utils || {},
	Row = ReactBootstrap.Row,
	Button = ReactBootstrap.Button,
	Image = ReactBootstrap.Image,
	Glyphicon = ReactBootstrap.Glyphicon,
	Col = ReactBootstrap.Col;


/**
 * Comments component.
 */
class Comments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {viewModel: new CommentsViewModel(props.parentType, props.parentId, props.count, [])};
	}

	/**
	 * Shows/hides new comment entry form.
	 */
	toggleEntry() {
		this.state.viewModel.toggleEntry();
		this.setState({viewModel: this.state.viewModel});
	}

	/**
	 * Saves new comment.
	 */
	save() {
		let entryField = this.refs[this.state.viewModel.entryFieldRef];
		const newComment = entryField.lastHtml.trim();
		if (newComment.length === 0) return;

		this.state.viewModel.addComment(newComment, this.props.user);
		this.setState({viewModel: this.state.viewModel});
		commentService.add(this.state.viewModel.parentType, this.state.viewModel.parentId, newComment).then( x => {
			console.log('%o', x);
		});
	}

	/**
	 * Cancels new comment entry.
	 */
	cancel() {
		this.toggleEntry();
	}

	/**
	 * Gets comments from the server.
	 */
	getComments() {
		commentService.getTasksForDashboard(this.props.parentType, this.props.parentId).then(data => {
			this.state.viewModel.setComments(data, this.props.users);
			this.showHideComments();
		});
	}

	/**
	 * Shows/hides comments.
	 */
	showHideComments() {
		this.state.viewModel.showHideList();
		this.setState({viewModel: this.state.viewModel});
	}

	/**
	 * Shows/hides comments and gets data from the server if needed.
	 */
	toggleList() {
		if (this.state.viewModel.hasData ) {
			this.showHideComments();
		} else {
			this.getComments();
		}
	}

	/**
	 * Renders comments.
	 * @param comments
	 * @returns {*}
	 */
	renderComments(comments) {
		return comments.map(x => {
			return <Row key={x.id} className='comments-comment-row'>
				<Col xsOffset={1} xs={8}>
					<Row>
						<Col xs={1}>
							<Image src={x.userImageUrl} className='comments-assignee-image'/>
						</Col>
						<Col xs={11}>
							<span className='comments-user-name'>{x.userFullName}</span>
							<span className='comments-date'>{x.date}</span><br/>
							<div dangerouslySetInnerHTML={{__html: x.comment}}></div>
						</Col>
					</Row>
				</Col>
			</Row>
		});
	}

	scroll($element) {
		const offset = $element.offset().top - 200;
		$("html, body").animate({scrollTop: offset});
	}

	scrollAndFocusOnEntryField() {
		if (!this.state.viewModel.entryVisible || this.state.viewModel.listVisible) return;

		const $element = $(`#${this.state.viewModel.entryFieldId}`);
		this.scroll($element);
		$element.focus();
	}

	scrollToExpandedCommentList() {
		if (!this.state.viewModel.listVisible) return;

		const $element = $(`#${this.state.viewModel.entryFieldId}`);
		this.scroll($element);
		$element.focus();
	}

	componentDidUpdate() {
		this.scrollAndFocusOnEntryField();
		this.scrollToExpandedCommentList();
	}

	render() {
		const viewModel = this.state.viewModel;
		const commentList = viewModel.listVisible ? this.renderComments(viewModel.comments) : null;

		return <div key={viewModel.parentId}>
			<Row className='comments-view-comment-row'>
				<Col xs={10}>
					<a
						id={viewModel.viewCommentId}
						href='#'
						onClick={this.toggleList.bind(this)}>{viewModel.viewHideLabel}</a>
				</Col>
				<Col xs={2}>
					{!viewModel.entryVisible ?
					<a href='#' className="pull-right" onClick={this.toggleEntry.bind(this)}>
						<Glyphicon glyph="comment" style={{fontFamily: 'Glyphicons Halflings !important'}}/>
						<span className='comments-comment-link'>Comment</span>
					</a> :
						null
						}
				</Col>
			</Row>
			{commentList}
			{viewModel.entryVisible ?
			<Row className='task-row'>
				<Col xs={8} xsOffset={1}>
					<ContentEditable
						className="comments-entry"
						key={viewModel.entryFieldId}
						id={viewModel.entryFieldId}
						ref={viewModel.entryFieldRef}
						html={viewModel.newComment}/>
					<Button className='pull-right' bsStyle="success" onClick={this.save.bind(this)}>Post</Button>
					<Button className='pull-right' bsStyle="link" onClick={this.cancel.bind(this)}>Cancel</Button>
				</Col>
			</Row> :
				null
				}
		</div>
	}
}
