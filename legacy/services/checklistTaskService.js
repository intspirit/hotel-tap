'use strict';

var checklistTaskService = {
	getByChecklistIdId: function getByChecklistIdId(id) {
		return serviceBase.get('/api/inspections/' + id);
	},

	getById: function getById(id) {
		return serviceBase.get('/api/inspections/' + id);
	},

	add: function add(checklistId, data) {
		return serviceBase.post('/api/inspections/' + checklistId + '/tasks', { data: data });
	},

	delete: function _delete(checklistId, taskId) {
		return serviceBase.delete('/api/inspections/' + checklistId + '/tasks/' + taskId, {});
	},

	save: function save(id, data) {
		return serviceBase.put('/api/inspections/' + id, { data: data });
	}
};

//# sourceMappingURL=checklistTaskService.js.map