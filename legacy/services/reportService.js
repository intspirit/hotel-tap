'use strict';

var reportService = {
	getRoomLogs: function getRoomLogs() {
		return serviceBase.get('/api/roomlogs/');
	},

	getChecklists: function getChecklists() {
		return serviceBase.get('/api/checklists/');
	},

	getReport: function getReport(type, id) {
		return serviceBase.get('/api/reports/' + type + '/' + id);
	}
};

//# sourceMappingURL=reportService.js.map