'use strict';

var commentService = {
	getTasksForDashboard: function getTasksForDashboard(parentType, parentId) {
		return serviceBase.get("/hotel/apiGetCommentsBySource?sourceType=" + parentType + "&sourceId=" + parentId);
	},

	add: function add(parentType, parentId, comment) {
		var request = {
			task_id: parentId,
			content: comment,
			mentions: {
				"emp": [], "dept": [], "tag": []
			}
		};
		return serviceBase.post('/index.php/hotel/addtaskcomment', request);
	}
};

//# sourceMappingURL=commentService.js.map