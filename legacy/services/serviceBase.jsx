'use strict';
const credentials = {credentials: 'same-origin'};

function checkStatus(response) {
	if (response.status >= 200 && response.status < 300) {
		return response;
	} else {
		let error = new Error(response.statusText);
		error.response = response;
		throw error;
	}
}

function parseJson(response) {
	return response.json();
}

const serviceBase = {
	get: (url) => {
		return fetch(url, credentials).
			then(checkStatus).
			then(parseJson);
	},
	postPutDelete: (url, method, request) => {
		return fetch(url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: method,
			credentials: 'same-origin',
			body: JSON.stringify(request)
		}).
		then(checkStatus).
		then(parseJson);
	},
	post: (url, request) => {
		return serviceBase.postPutDelete(url, 'POST', request)
	},
	put: (url, request) => {
		return serviceBase.postPutDelete(url, 'PUT', request)
	},
	delete: (url, request) => {
		return serviceBase.postPutDelete(url, 'DELETE', request)
	}
};
