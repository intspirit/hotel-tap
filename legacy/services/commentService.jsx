'use strict';
const commentService = {
	getTasksForDashboard: (parentType, parentId) => {
		return serviceBase.get(`/hotel/apiGetCommentsBySource?sourceType=${parentType}&sourceId=${parentId}`);
	},

	add: (parentType, parentId, comment) => {
		const request = {
			task_id: parentId,
			content: comment,
			mentions: {
				"emp": [], "dept": [], "tag": []
			}
		};
		return serviceBase.post('/index.php/hotel/addtaskcomment', request);
	}
};
