'use strict';
const reportService = {
	getRoomLogs: () => {
		return serviceBase.get('/api/roomlogs/');
	},

	getChecklists: () => {
		return serviceBase.get('/api/checklists/');
	},

	getReport: (type, id) => {
		return serviceBase.get(`/api/reports/${type}/${id}`);
	}
};
