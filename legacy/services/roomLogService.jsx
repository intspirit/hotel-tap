'use strict';
const roomLogService = {
	prefix: 'roomLogService',

	getAll: () => {
		return serviceBase.get('/hotel/apiRoomLogDashboardLogs');
	},

	getTotalCompletedForYear: (roomLogId, year) => {
		return serviceBase.get(`/hotel/apiGetRoomLogTotalCompletedForYear/${roomLogId}/${year}`);
	},

	getCurrentUser: () => {
		return serviceBase.get('/hotel/apiGetCurrentUser');
	},

	getYears: () => {
		return serviceBase.get('/hotel/apiGetRoomLogYear');
	},

	getFilters: (roomLogId, key) => {
		if (!Modernizr.localstorage) { return false; }

		return roomLogService.getCurrentUser().then(user => {
			let storageKey  = [roomLogService.prefix, user.id, roomLogId].join('.');
			let filters = JSON.parse(localStorage.getItem(storageKey)) || {};

			return filters[key];
		});
	},

	setFilters: (roomLogId, filters) => {
		if (!Modernizr.localstorage) { return false; }

		return roomLogService.getCurrentUser().then(user => {
			let storageKey  = [roomLogService.prefix, user.id, roomLogId].join('.');
			let currentFilters = JSON.parse(localStorage.getItem(storageKey)) || {};

			localStorage.setItem(storageKey, JSON.stringify(Object.assign(currentFilters, filters)));
		});
	}
};
