'use strict';
const checklistTaskService = {
	getByChecklistIdId: (id) => {
		return serviceBase.get(`/api/inspections/${id}`);
	},

	getById: (id) => {
		return serviceBase.get(`/api/inspections/${id}`);
	},

	add: (checklistId, data) => {
		return serviceBase.post(`/api/inspections/${checklistId}/tasks`, {data: data});
	},

	delete: (checklistId, taskId) => {
		return serviceBase.delete(`/api/inspections/${checklistId}/tasks/${taskId}`, {});
	},

	save: (id, data) => {
		return serviceBase.put(`/api/inspections/${id}`, {data: data});
	}
};
