'use strict';

var credentials = { credentials: 'same-origin' };

function checkStatus(response) {
	if (response.status >= 200 && response.status < 300) {
		return response;
	} else {
		var error = new Error(response.statusText);
		error.response = response;
		throw error;
	}
}

function parseJson(response) {
	return response.json();
}

var serviceBase = {
	get: function get(url) {
		return fetch(url, credentials).then(checkStatus).then(parseJson);
	},
	postPutDelete: function postPutDelete(url, method, request) {
		return fetch(url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: method,
			credentials: 'same-origin',
			body: JSON.stringify(request)
		}).then(checkStatus).then(parseJson);
	},
	post: function post(url, request) {
		return serviceBase.postPutDelete(url, 'POST', request);
	},
	put: function put(url, request) {
		return serviceBase.postPutDelete(url, 'PUT', request);
	},
	delete: function _delete(url, request) {
		return serviceBase.postPutDelete(url, 'DELETE', request);
	}
};

//# sourceMappingURL=serviceBase.js.map