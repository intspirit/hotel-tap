'use strict';

var roomLogService = {
	prefix: 'roomLogService',

	getAll: function getAll() {
		return serviceBase.get('/hotel/apiRoomLogDashboardLogs');
	},

	getTotalCompletedForYear: function getTotalCompletedForYear(roomLogId, year) {
		return serviceBase.get('/hotel/apiGetRoomLogTotalCompletedForYear/' + roomLogId + '/' + year);
	},

	getCurrentUser: function getCurrentUser() {
		return serviceBase.get('/hotel/apiGetCurrentUser');
	},

	getYears: function getYears() {
		return serviceBase.get('/hotel/apiGetRoomLogYear');
	},

	getFilters: function getFilters(roomLogId, key) {
		if (!Modernizr.localstorage) {
			return false;
		}

		return roomLogService.getCurrentUser().then(function (user) {
			var storageKey = [roomLogService.prefix, user.id, roomLogId].join('.');
			var filters = JSON.parse(localStorage.getItem(storageKey)) || {};

			return filters[key];
		});
	},

	setFilters: function setFilters(roomLogId, filters) {
		if (!Modernizr.localstorage) {
			return false;
		}

		return roomLogService.getCurrentUser().then(function (user) {
			var storageKey = [roomLogService.prefix, user.id, roomLogId].join('.');
			var currentFilters = JSON.parse(localStorage.getItem(storageKey)) || {};

			localStorage.setItem(storageKey, JSON.stringify(Object.assign(currentFilters, filters)));
		});
	}
};

//# sourceMappingURL=roomLogService.js.map