const AdminViewHeader = (props) => {
	const printButton = props.printButton ?
		<div className="pull-right print-button-wrapper">
			{props.printButton}
		</div> :
		null;

	return (
		<section className="content-header content-h1">
			{printButton}
			<div className="pull-right">
				<a
					href={props.url}
					className="btn btn-primary hidden-sm hidden-xs"
					onClick={props.buttonClicked}>{props.buttonLabel}
				</a>
				<a
					href={props.url}
					className="btn btn-primary hidden-lg hidden-md"
					onClick={props.buttonClicked}>{props.buttonLabelSmall || props.buttonLabel}
				</a>
			</div>
			{props.showNavigation ? <div className="mobile-icon"><img src="/images/nav.png" alt="Menu"/></div> : null}
			<h1>{props.header}</h1>
		</section>
	);
};
AdminViewHeader.defaultProps = {
	url: '#create',
	buttonClicked: null,
	showNavigation: true
};

const BoardViewHeader = (props) => {
	const printButton = props.printButton ?
		<div className="pull-right print-button-wrapper">
			{props.printButton}
		</div> :
		null;

	return (
		<section className="content-header content-h1">
			{printButton}
			<div className="pull-right">
				<a
					href={props.url}
					className="btn btn-primary hidden-sm hidden-xs"
					onClick={props.buttonClicked}>{props.buttonLabel}
				</a>
				<a
					href={props.url}
					className="btn btn-primary hidden-lg hidden-md"
					onClick={props.buttonClicked}>{props.buttonLabelSmall || props.buttonLabel}
				</a>
			</div>
			{props.showNavigation ? <div className="mobile-icon"><img src="/images/nav.png" alt="Menu"/></div> : null}
			<h1>{props.header}</h1>
		</section>
	);
};
BoardViewHeader.defaultProps = {
	url: '#',
	buttonClicked: null,
	showNavigation: true
};

