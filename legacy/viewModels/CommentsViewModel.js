'use strict';

/**
 * Gets view comments label based on the number of comments.
 * @param count
 * @returns {*}
 */

var getViewHideCommentsLabel = function getViewHideCommentsLabel(count) {

	switch (count) {
		case '0':
			return '';
			break;
		case '1':
			return 'View 1 comment';
			break;
		default:
			return 'View all ' + count + ' comments';
	}
};

/**
 * Comments view model.
 * @param parentType
 * @param parentId
 * @param count
 * @param comments
 * @constructor
 */
function CommentsViewModel(parentType, parentId, count, comments) {
	this.parentType = parentType;
	this.parentId = parentId;
	this.listVisible = false;
	this.count = count;
	this.comments = comments;
	this.viewHideLabel = getViewHideCommentsLabel(count);
	this.entryVisible = false;
	this.newComment = '';
	this.newId = -1;
	this.hasData = false;

	this.entryFieldRef = 'entryField-' + this.parentId;
	this.entryFieldId = 'entry-' + this.parentType + '-' + this.parentId;
	this.viewCommentId = 'view-comments-' + this.parentType + '-' + this.parentId;
}

/**
 * Sets model comments.
 * @param data
 * @param users
 */
CommentsViewModel.prototype.setComments = function (data, users) {
	var comments = data.map(function (comment) {
		var employee = users[comment.created_by];
		var imageUrl = Utils.getUserImageUrl(employee.imageUrl);

		return {
			id: comment.comment_id,
			userId: comment.created_by,
			userFullName: employee.fullName,
			userImageUrl: imageUrl,
			date: moment(comment.created_date).calendar(),
			comment: comment.comment
		};
	});
	this.hasData = true;
	this.comments = comments;
};

/**
 * Sets the state of the view comments label.
 */
CommentsViewModel.prototype.setViewHideListLabel = function () {
	this.viewHideLabel = this.listVisible ? 'Hide comments' : this.getViewHideListLabel(this.count);
};

/**
 * Gets the label for the View/Hide Comments link.
 * @param count
 * @returns {*}
 */
CommentsViewModel.prototype.getViewHideListLabel = function (count) {
	switch (count) {
		case '0':
			return '';
			break;
		case '1':
			return 'View 1 comment';
			break;
		default:
			return 'View all ' + count + ' comments';
	}
};

/**
 * Shows or hides coment list.
 */
CommentsViewModel.prototype.showHideList = function () {
	var listVisible = !this.listVisible;
	this.listVisible = listVisible;
	this.entryVisible = listVisible;
	this.setViewHideListLabel();
};

/**
 * Shows or hides new comment entry field.
 */
CommentsViewModel.prototype.toggleEntry = function () {
	this.entryVisible = !this.entryVisible;
};

/**
 * Adds new comment.
 * @param comment
 * @param user
 */
CommentsViewModel.prototype.addComment = function (comment, user) {
	var newComment = {
		id: this.newId--,
		userImageUrl: Utils.getUserImageUrl(user.imageUrl),
		userFullName: user.fullName,
		date: moment().calendar(),
		comment: comment
	};
	this.comments.push(newComment);
	this.newComment = '';
	this.listVisible = true;
	this.count = ++this.count;
	this.setViewHideListLabel();
};

//# sourceMappingURL=CommentsViewModel.js.map