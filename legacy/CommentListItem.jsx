'use strict';

let HotelTap = HotelTap || {};

/**
 * Comment item in a list.
 * @type {{new(*=): {render: (function())}}}
 */
HotelTap.CommentListItem = class extends React.Component {
	constructor(params) {
		super(params);
	}


	render() {
		const comment = this.props.comment;

		return <div key={comment.id} className='comment-item'>
			<div className="comment-user-picture">
				<img src={comment.userPicture || "/images/default.png"} />
			</div>
			<div className="comment-info">
				<div className="comment-user">{comment.employee}</div>
				<div className="comment-date">{moment(comment.dateTime).format('MMM D [at] h:mm a')}</div>
				<div className="clearfix"></div>
				<div className="comment-text">{comment.text}</div>
				<HotelTap.Attachments model={comment.attachments || []}/>
			</div>
			<div className="clearfix"></div>
		</div>
	}
};

