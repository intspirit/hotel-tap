'use strict';

let HotelTap = HotelTap || {};

HotelTap.AttachmentFile = class extends React.Component {
	constructor(params) {
		super(params);

		this.state = {
			model: this.props.model
		};
	}

	render() {
		return <div className="attachment-file">
	        <i className="fa fa-file-text-o attachment-icon"></i>
	        <a href={this.state.model.filePath} target="_blank">{this.state.model.fileName}</a>
	    </div>
	}
};

HotelTap.AttachmentImage = class extends React.Component {
	constructor(params) {
		super(params);

		this.state = {
			model: this.props.model
		};
	}

	render() {
		return <div className="attachment-image">
			<a href={this.state.model.filePath} target="_blank">
				<img src={this.state.model.filePath} />
			</a>
		</div>
	}
};

HotelTap.Attachments = class extends React.Component {
	constructor(params) {
		super(params);

		let model = {
			all: this.props.model
			, images: []
			, files: []
		}

		this.props.model.forEach(function (attachment) {
			if (attachment.isImage) {
				model.images.push(attachment);
				return;
			}

			model.files.push(attachment);
		});

		this.state = {
			model: model
		};
	}

	render() {
		return <div className="attachements">
			<div className="attachements-images">
				{this.state.model.images.map(function(image) {
					return <HotelTap.AttachmentImage key={image.attachmentId} model={image}/>
				})}
			</div>
			<div className="attachements-files">
				{this.state.model.files.map(function(file) {
					return <HotelTap.AttachmentFile key={file.attachmentId} model={file}/>
				})}
			</div>
			<div className="clearfix"></div>
		</div>
	}
};


