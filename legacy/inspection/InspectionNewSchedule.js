'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Inspection new schedule component.
 */

var InspectionNewSchedule = function (_React$Component) {
	_inherits(InspectionNewSchedule, _React$Component);

	function InspectionNewSchedule(props) {
		_classCallCheck(this, InspectionNewSchedule);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionNewSchedule).call(this, props));

		_this.state = InspectionNewSchedule.getDefaultState();
		return _this;
	}

	_createClass(InspectionNewSchedule, [{
		key: 'onEmployeeChange',
		value: function onEmployeeChange(event) {
			var errors = this.state.errors || {};
			errors.employeeId = false;

			this.setState({
				employeeId: event.target.value,
				employeeName: event.target.selectedOptions[0].label,
				errors: errors
			});
		}
	}, {
		key: 'onSupervisorChange',
		value: function onSupervisorChange(event) {
			var errors = this.state.errors || {};
			errors.supervisorId = false;
			this.setState({
				supervisorId: event.target.value,
				supervisorName: event.target.selectedOptions[0].label,
				errors: errors
			});
		}
	}, {
		key: 'onRoomChange',
		value: function onRoomChange(event) {
			var errors = this.state.errors || {};
			errors.roomId = false;
			this.setState({
				roomId: event.target.value,
				room: event.target.selectedOptions[0].label,
				errors: errors
			});
		}
	}, {
		key: 'onDateChange',
		value: function onDateChange(event) {
			var errors = this.state.errors || {};
			errors.date = false;
			this.setState({
				date: event.target.value,
				errors: errors
			});
		}
	}, {
		key: 'validationPassed',
		value: function validationPassed() {
			var passed = true;
			var errors = this.state.errors || {};
			if (this.state.date == -1 || !this.state.date) {
				errors.date = true;
				passed = false;
			}

			if (this.state.roomId == -1 || !this.state.roomId) {
				errors.roomId = true;
				passed = false;
			}

			if (this.state.employeeId == -1 || !this.state.employeeId) {
				errors.employeeId = true;
				passed = false;
			}

			if (this.state.supervisorId == -1 || !this.state.supervisorId) {
				errors.supervisorId = true;
				passed = false;
			}

			this.setState({ errors: errors });

			return passed;
		}
	}, {
		key: 'onAddClick',
		value: function onAddClick() {
			if (!this.validationPassed()) {
				return;
			}

			var newSchedule = {
				id: this.state.id,
				dueDateTime: this.state.date,
				employeeId: this.state.employeeId,
				employeeName: this.state.employeeName,
				supervisorId: this.state.supervisorId,
				supervisorName: this.state.supervisorName,
				roomId: this.state.roomId,
				roomName: this.state.room,
				status: "Assigned"
			};
			this.props.onScheduleAdded(newSchedule);
			this.setState(InspectionNewSchedule.getDefaultState());
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'tr',
				{ className: 'newRow', key: this.state.id },
				React.createElement(
					'td',
					null,
					React.createElement('input', { type: 'date',
						value: this.state.date,
						className: this.state.errors.date ? 'error' : '',
						onChange: this.onDateChange.bind(this) })
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{ id: 'employeeId',
							value: this.state.employeeId,
							className: this.state.errors.employeeId ? 'error' : '',
							onChange: this.onEmployeeChange.bind(this) },
						React.createElement(
							'option',
							{ value: '-1', disabled: 'disabled' },
							'--Select employee--'
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{ id: 'supervisorId',
							value: this.state.supervisorId,
							className: this.state.errors.supervisorId ? 'error' : '',
							onChange: this.onSupervisorChange.bind(this) },
						React.createElement(
							'option',
							{ value: '-1', disabled: 'disabled' },
							'--Select supervisor--'
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{ id: 'roomId',
							value: this.state.roomId,
							className: this.state.errors.roomId ? 'error' : '',
							onChange: this.onRoomChange.bind(this) },
						React.createElement(
							'option',
							{ value: '-1', disabled: 'disabled' },
							'--Select location--'
						),
						this.props.roomOptions
					)
				),
				React.createElement('td', null),
				React.createElement('td', null),
				React.createElement('td', null),
				React.createElement(
					'td',
					{ width: '10%' },
					React.createElement(
						'button',
						{ type: 'button', className: 'btn btn-link', onClick: this.onAddClick.bind(this) },
						'Add'
					)
				)
			);
		}
	}], [{
		key: 'getDefaultState',
		value: function getDefaultState() {
			return {
				id: -1,
				date: new Date().toISOString().substring(0, 10),
				employeeId: -1,
				supervisorId: -1,
				roomId: -1,
				employeeName: '',
				supervisorName: '',
				room: '',
				errors: {}
			};
		}
	}]);

	return InspectionNewSchedule;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionNewSchedule;
//# sourceMappingURL=InspectionNewSchedule.js.map