const pointsMin = 1;
const pointsMax = 10;
/**
 * Inspection task component.
 */
class InspectionTask extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props.task;
	}

	onChange(event) {
		if (this.refs.points.value) {
			if (this.refs.points.value > pointsMax) this.refs.points.value = pointsMax;
			if (this.refs.points.value < pointsMin) this.refs.points.value = pointsMin;
		}

		let changes = {
			subject: this.refs.subject.value,
			points: this.refs.points.value,
			failAll: this.refs.failAll.checked || false
		};
		if (this.state.changeStatus !== 'i') {
			changes.changeStatus = 'u';
			this.props.onTaskUpdated(this.state.id, changes);
		}

		//noinspection JSUnresolvedVariable
		this.setState(changes);
	}

	onDelete() {
		let isNew = this.state.changeStatus === 'i';
		this.state.changeStatus = 'd';
		this.props.onTaskDeleted(this.state.id, isNew);
	}

	componentWillReceiveProps(nextProps) {
		this.setState(nextProps.task);
	}

	render() {
		if (this.state.changeStatus === 'd') return null;

		const checked = this.state.failAll == 1 ? 'checked' : null;

		return <tr key={this.state.id}>
			<th>
				<input
					ref="subject"
					type="text"
					className="transparentInput"
					value={this.state.subject}
					onChange={this.onChange.bind(this)}/>
			</th>
			<th className="center">
				<input
					ref="points"
					type="number"
					className="transparentInput"
					value={this.state.points}
					onChange={this.onChange.bind(this)}/>
			</th>
			<th className="center">
				<input
					ref="failAll"
					type="checkbox"
					checked={checked}
					value="failAll"
					onChange={this.onChange.bind(this)}/>
			</th>
			<th className="actionIcons">
				<span id='1'
				      className="glyphicon glyphicon-trash"
				      title="Delete"
				      onClick={this.onDelete.bind(this)}/>
			</th>
		</tr>;
	}
}
if (typeof module !== 'undefined') module.exports = InspectionTask;
