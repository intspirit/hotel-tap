const
	inspectionService = inspectionService || require("inspectionService");

/**
 * Inspection schedules.
 */
class InspectionSchedules extends React.Component {
	constructor(props) {
		super(props);

		this.employeeOptions =
			this.props.activeEmployees.map(x => <option value={x.user_id} key={x.user_id}>{x.fullName}</option>);
		this.roomOptions = this.props.rooms.map(x => <option value={x.id} key={x.id}>{x.name}</option>);

		this.schedulesStore = this.props.schedules;
		this.state = {
			schedules: this.schedulesStore.map(x => {
				const employee = _.find(this.props.activeEmployees, {user_id: x.employeeId});
				const supervisor = _.find(this.props.activeEmployees, {user_id: x.supervisorId});
				return _.assign(x, {
					employeeName: employee ? employee.fullName : '',
					supervisorName: supervisor ? supervisor.fullName : '',
				});
			}),
			filters: {},
			sorting: {},
			filterVisible: true
		};
		this.sortSchedules('dueDateTime');
	}

	componentDidMount() {
		this.setState({filterVisible: false});
	}

	onScheduleUpdated(schedule) {
		const year = parseInt(schedule.dueDateTime.substr(0, 4));
		const month = parseInt(schedule.dueDateTime.substr(5, 2));
		const date = parseInt(schedule.dueDateTime.substr(8, 4));
		const data = {
			roomId: schedule.roomId,
			dueYear: year,
			dueMonth: month,
			dueDay: date,
			dueDateTime: new Date(year, month, date, 23, 59, 59, 0),
			employeeId: schedule.employeeId,
			supervisorId: schedule.supervisorId
		};

		schedule = _.assign(schedule, {
			dueYear: year.toString(),
			dueMonth: month.toString(),
			dueDay: date.toString()
		});

		inspectionService.updateSchedule(this.props.inspectionId, schedule.id, data).then(() => {
			this.schedulesStore.map(x => {
				if (x.id === schedule.id) x = _.assign(x, schedule);
				return x;
			});
			const schedules = this.getFiltered(this.schedulesStore);
			this.setState({
				schedules: this.getSorted(schedules)
			});
		}).catch(() => {
			alert('Failed to save inspection schedule');
		});
	}

	onScheduleAdded(schedule) {
		const year = parseInt(schedule.dueDateTime.substr(0, 4));
		const month = parseInt(schedule.dueDateTime.substr(5, 2));
		const date = parseInt(schedule.dueDateTime.substr(8, 4));
		const data = {
			roomId: schedule.roomId,
			dueYear: year,
			dueMonth: month,
			dueDay: date,
			dueDateTime: new Date(year, month, date, 23, 59, 59, 0),
			employeeId: schedule.employeeId,
			supervisorId: schedule.supervisorId
		};

		schedule = _.assign(schedule, {
			dueYear: year.toString(),
			dueMonth: month.toString(),
			dueDay: date.toString()
		});

		inspectionService.addSchedule(this.props.inspectionId, data).then((response) => {
			schedule.id = response.id;
			this.schedulesStore.push(schedule);
			const schedules = this.getFiltered(this.schedulesStore);
			this.setState({schedules: this.getSorted(schedules)});
		}).catch(() => {
			alert('Failed to add inspection schedule');
		});
	}

	getFiltered(schedules) {
		const filters = this.state.filters;
		return _.filter(schedules, x => {
			let isValid = true;
			_.forEach(filters, (value, key) => {
				if (isValid === false) {
					return false;
				}

				if (key === "status") {
					let status = x.completionDateTime == null ? 'assigned' : 'completed';
					isValid = status === value;

					return;
				}

				isValid = x[key] === value;
			});

			return isValid;
		})
	}

	onFilterChanged(filters) {
		this.state.filters = filters;

		let schedules = this.getFiltered(this.schedulesStore);
		schedules = this.getSorted(schedules);

		this.setState({
			schedules: schedules,
			filters: this.state.filters
		});
	}

	toggleFilter() {
		this.setState({
			filterVisible: !this.state.filterVisible
		});
	}

	getSorted(schedules) {
		return schedules.sort(naturalSort(this.state.sorting));
	}

	sortSchedules(key) {
		this.state.sorting = {
			key: key,
			direction: (this.state.sorting.direction === 'asc' && this.state.sorting.key === key) ? 'desc' : 'asc'
		}

		this.setState({
			schedules: this.getSorted(this.state.schedules),
			sorting: this.state.sorting
		});
	}

	render() {
		const filterLabel = !this.state.filterVisible ? 'Show Filter' : 'Hide Filter';
		const columns = {
			dueDateTime: "Date",
			employeeName: "Employee",
			supervisorName: "Supervisor",
			roomName: "Location",
			completionDateTime: "Status",
			scorePercentage: "Score, %",
			inspectionPassed: "Result"
		}
		return <div className="form-group" style={{overflowX: 'scroll'}}>
			<h1>
				<span className="btn btn-link btn-sm pull-right" onClick={this.toggleFilter.bind(this)}>{filterLabel}</span>
				<span>Inspections</span>
			</h1>
			<table className="table table-bordered" id="inspectionTaskTable">
				<thead>
				<tr className="tblheading">
					{Object.keys(columns).map(x => {
						return <th onClick={this.sortSchedules.bind(this, x)}
								className={'sorting-available ' + (this.state.sorting.key === x ? 'sorted-header' : '')}>
							{columns[x]}
							<span className={'sorting-arrow ' + (this.state.sorting.key !== x ? 'hidden' : '')}>
								<i className={"glyphicon glyphicon-chevron-" +
									(this.state.sorting.direction === 'desc' ? 'down' : 'up')}/>
							</span>
						</th>
					})}
					<th width="15%">Options</th>
				</tr>
				</thead>
				<tbody>
				<InspectionScheduleFilter
					onFilterChanged={this.onFilterChanged.bind(this)}
					employeeOptions={this.employeeOptions}
					isVisible={this.state.filterVisible}
					roomOptions={this.roomOptions}/>
				<InspectionNewSchedule
					onScheduleAdded={this.onScheduleAdded.bind(this)}
					employeeOptions={this.employeeOptions}
					roomOptions={this.roomOptions}/>
				{this.state.schedules.map(x => <InspectionSchedule
					key={x.id}
					onScheduleUpdated={this.onScheduleUpdated.bind(this)}
					inspectionId={this.props.inspectionId}
					schedule={x}
					employees={this.props.activeEmployees}
					allEmployees={this.props.allEmployees}
					rooms={this.props.rooms}
					employeeOptions={this.employeeOptions}
					roomOptions={this.roomOptions}
				/>)}
				</tbody>
			</table>
		</div>
	}
}

if (typeof module !== 'undefined') module.exports = InspectionSchedules;
