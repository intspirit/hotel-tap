const
	Button = ReactBootstrap.Button,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col;


/**
 * Inspection entry - action buttons.
 * @param score
 * @param passingPoints
 * @returns {number}
 */
function getScorePercentage(score, passingPoints) {
	return parseInt(score / passingPoints * 100);
}


/**
 *
 * @param props
 * @constructor
 */
const InspectionEntryActionButtons = (props) =>
	<Row>
		<Col xs={12}>
			<Button bsStyle='primary' className="pull-right" onClick={props.onSaveClicked}>Save</Button>
			<Button bsStyle='link' className="pull-right" onClick={props.onCancelClicked}>Cancel</Button>
		</Col>
	</Row>;


/**
 * Inspection entry component.
 */
class InspectionEntry extends React.Component {
	constructor(props) {
		super(props);
		this.employeeName = Utils.getEmployeeName(this.props.schedule.employeeId, this.props.employees);
		this.userId = this.props.schedule.supervisorId;
	}

	get title() {
		return `${this.props.roomLog.name} - ${this.employeeName} on 
		${moment(this.props.schedule.dueDateTime).format('L')}`;
	}

	get titleSmall() {
		return <span>{this.props.roomLog.name}<br/>
			by {this.employeeName}<br/>
			on {moment(this.props.schedule.dueDateTime).format('L')}
			</span>;
	}

	render() {
		const errorMessage = this.props.showErrors ?
			<div className="alert alert-danger" role="alert">
				Please complete all tasks before saving the inspection.
			</div> :
			null;
		const backUrl = '/hotel/empboard/' + this.userId;
		return <div id="inspectionEntry">
			<BoardViewHeader
				url={backUrl}
				showNavigation={false}
				buttonLabel="Back to board"
				buttonLabelSmall="Back"
				header={this.title}/>
			<LabelValueView label='Date:' value={moment(this.props.schedule.dueDateTime).format('L')}/>
			<LabelValueView
				label='Employee:'
				value={this.employeeName}/>
			<LabelValueView
				label='Supervisor:'
				value={Utils.getEmployeeName(this.props.schedule.supervisorId, this.props.employees)}/>
			<LabelValueView label='Location:' value={this.props.schedule.roomName}/>

			<InspectionEntryTasks
				roomLog={this.props.roomLog}
				result={this.props.result}
				scorePoints={this.props.scorePoints}
				scorePercentage={this.props.scorePercentage}
				taskScoreChanged={this.props.taskScoreChanged}
				taskCommentAdded={this.props.taskCommentAdded}
				tasks={this.props.tasks}/>
			{errorMessage}
			<InspectionEntryActionButtons
				onSaveClicked={this.props.onSaveClicked}
				onCancelClicked={this.props.onCancelClicked}/>
		</div>;
	}
}
