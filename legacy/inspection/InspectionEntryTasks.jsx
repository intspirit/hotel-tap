const
	Grid = ReactBootstrap.Grid,
	FormControls = ReactBootstrap.FormControls,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col,
	ButtonGroup = ReactBootstrap.ButtonGroup,
	Button = ReactBootstrap.Button,
	Table = ReactBootstrap.Table;

/**
 * Inspection entry - total component.
 */
const InspectionTotal = (props) =>
	<tr>
		<td>TOTAL:&nbsp;
			<span className="hidden-md hidden-lg">{props.roomLog.totalPoints}</span>
			<span className="hidden-md hidden-lg pull-right">
				RESULT: {props.scorePoints}
			</span>
		</td>
		<td className="hidden-xs hidden-sm">{props.roomLog.totalPoints}</td>
		<td className="hidden-xs hidden-sm"/>
		<td className="hidden-xs hidden-sm">{props.scorePoints}</td>
		<td className="hidden-xs hidden-sm"/>
	</tr>;


/**
 * Inspection entry - result component. 
 */
const InspectionResult = (props) => {
	const resultClass = props.result == 'Pass' ? "text-green" : "text-red";
	return <tr>
		<td>
			<span className="hidden-xs hidden-sm">INSPECTION SCORE/RESULT:</span>
			<span className="hidden-md hidden-lg">SCORE: {props.roomLog.passingScorePercentage} %</span>
			<span className="hidden-md hidden-lg pull-right">RESULT: {props.scorePercentage} %</span>
			<div className="hidden-md hidden-lg">INSPECTION RESULT: <span className={resultClass}>{props.result}</span>
			</div>
		</td>
		<td className="hidden-xs hidden-sm"/>
		<td className="hidden-xs hidden-sm">{props.roomLog.passingScorePercentage} %</td>
		<td className="hidden-xs hidden-sm">{props.scorePercentage} %</td>
		<td className="hidden-xs hidden-sm"><span className={resultClass}>{props.result}</span></td>
	</tr>
}


/**
 * Inspection entry - tasks component.
 */
class InspectionEntryTasks extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return <Row className="show-grid" style={{marginTop: '15px'}}>
			<Col xs={12} md={12}>
				<Table striped bordered condensed hover id="inspectionTable">
					<thead>
					<tr>
						<th>Task</th>
						<th className="hidden-xs hidden-sm">Points</th>
						<th style={{width: '350px'}} className="hidden-xs hidden-sm">Status</th>
						<th className="hidden-xs hidden-sm">Result</th>
						<th style={{width: '55px'}} className="hidden-xs hidden-sm">Fail All</th>
					</tr>
					</thead>
					<tbody>
					{this.props.tasks.map(x => <InspectionEntryTask
						key={x.id}
						task={x}
						taskScoreChanged={this.props.taskScoreChanged}
						taskCommentAdded={this.props.taskCommentAdded}
					/>)}
					</tbody>
					<tfoot>
					<InspectionTotal
						roomLog={this.props.roomLog}
						scorePoints={this.props.scorePoints}
						scorePercentage={this.props.scorePercentage}/>
					<InspectionResult
						roomLog={this.props.roomLog}
						scorePoints={this.props.scorePoints}
						result={this.props.result}
						scorePercentage={this.props.scorePercentage}/>
					</tfoot>
				</Table>
			</Col>
		</Row>
	}
}