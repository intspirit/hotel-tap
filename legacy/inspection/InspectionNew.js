'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var nameDescriptionRef = 'nameDescription',
    NameDescription = HotelTap.NameDescription;

/**
 * Inspection new entry.
 */

var InspectionNew = function (_React$Component) {
	_inherits(InspectionNew, _React$Component);

	function InspectionNew(props) {
		_classCallCheck(this, InspectionNew);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionNew).call(this, props));

		_this.state = {};
		return _this;
	}

	_createClass(InspectionNew, [{
		key: 'saveButtonClicked',
		value: function saveButtonClicked() {
			var nameDescriptionModel = this.refs[nameDescriptionRef].state;
			if (nameDescriptionModel.name.trim().length == 0) return;

			inspectionService.add(nameDescriptionModel.name, nameDescriptionModel.description).then(function (data) {
				var id = data.id;
				window.location = "/admin/inspections/" + id;
			}).catch(function () {
				alert('Failed to add inspection.');
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'aside',
				{ className: 'right-side' },
				React.createElement(AdminViewHeader, {
					url: '/admin/inspections',
					buttonLabel: 'Back to Inspections',
					header: 'Add Inspection' }),
				React.createElement(
					'section',
					{ className: 'content' },
					React.createElement(
						'div',
						{ className: 'whitecentercol' },
						React.createElement(NameDescription, { ref: nameDescriptionRef, model: { nameHint: 'Inspection name' } }),
						React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'a',
								{ href: '#', className: 'btn btn-primary', onClick: this.saveButtonClicked.bind(this) },
								'Save'
							)
						)
					)
				)
			);
		}
	}]);

	return InspectionNew;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionNew;
ReactDOM.render(React.createElement(InspectionNew, null), document.getElementById('view'));

//# sourceMappingURL=InspectionNew.js.map