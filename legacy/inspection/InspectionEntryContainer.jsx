const
	Grid = ReactBootstrap.Grid,
	FormControls = ReactBootstrap.FormControls,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col,
	ButtonGroup = ReactBootstrap.ButtonGroup,
	Button = ReactBootstrap.Button,
	Table = ReactBootstrap.Table;

/**
 * Inspection entry container component.
 * The root for the inspection entry.
 */
class InspectionEntryContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			scorePoints: '',
			scorePercentage: '',
			tasks: this.props.model.tasks,
			result: '',
			attachments: [],
			showErrors: false
		}

	}

	taskScoreChanged(taskId, result) {
		const task = _.find(this.state.tasks, x => x.id == taskId);
		switch (result) {
			case 'fail':
				task.score = 0;
				task.done = true;
				break;
			case 'pass':
				task.score = task.points;
				task.done = true;
				break;
			default:
				task.score = 0;
				task.done = false;
				break;
		}
		this.calcResult();
	}

	taskCommentAdded(taskId, data, attachments) {
		const task = _.find(this.state.tasks, x => x.id == taskId);
		let uniqueId = _.uniqueId();
		data.uniqueId = uniqueId;

		task.comments = task.comments || [];
		task.comments.push(data);
		attachments.forEach(x => {
			x.commentUniqueId = uniqueId;
		});
		this.setState({attachments: this.state.attachments.concat(attachments)});
	}

	calcResult() {
		let result = '';
		let score = 0;
		let completedCount = 0;
		_(this.state.tasks).forEach(x => {
			if (result === 'Fail' ||
				(x.done && x.failAll == 1 && x.score == 0)) result = 'Fail';
			completedCount += x.done ? 1 : 0;
			score += x.done ? parseInt(x.score) : 0;
		});
		if (result != 'Fail') {
			result = (completedCount == this.state.tasks.length) ?
				(score >= this.props.model.roomLog.passingScorePoints ? 'Pass' : 'Fail') :
				'';
		}
		const scorePercentage = getScorePercentage(score, this.props.model.roomLog.totalPoints);
		this.setState({
			scorePoints: score > 0 ? score : null,
			scorePercentage: scorePercentage > 0 ? scorePercentage : null,
			tasks: this.state.tasks,
			result: result
		});
	}

	validationPassed() {
		let passed = true;
		this.state.tasks.forEach(x => {
			if (!x.done) {
				passed = false;
			}
		});

		return passed;
	}

	onSaveClicked() {
		if (!this.validationPassed()) {
			this.setState({showErrors: true});
			return;
		}
		if (this.state.savingInProcess) return;

		this.setState({savingInProcess: true});

		let schedule = this.props.model.schedule;
		schedule.scorePercentage = this.state.scorePercentage;
		schedule.scorePoints = this.state.scorePoints;
		schedule.inspectionPassed = this.state.result == 'Pass' ? 1 : 0;

		let data = new FormData();
		this.state.attachments.forEach(x => {
			data.append(x.commentUniqueId + '[]', x);
		});

		const request = {
			id: this.props.model.schedule.id,
			tasks: this.state.tasks,
			scorePoints: this.state.scorePoints,
			scorePercentage: this.scorePercentage,
			schedule: schedule
		};

		data.append('data', JSON.stringify(request));

		$.ajax({
			url: '/api/inspections/entry',
			dataType: 'json',
			type: 'POST',
			data: data,
			cache: false,
			processData: false, // Don't process the files
			contentType: false, // Set content type to false as jQuery will tell the server its a query string reques
			success: function () {
				this.setState({savingInProcess: false});
				this.onCancelClicked();
			}.bind(this),
			error: function (xhr, status, err) {
				this.setState({savingInProcess: false});
				alert('Failed to save the inspection');
			}.bind(this)
		});
	}

	onCancelClicked() {
		const url = $('#backToBoardButton').attr('href');
		window.location.assign(url);
	}

	render() {
		if (this.state.tasks.length == 0) return null;

		return <InspectionEntry
			taskScoreChanged={this.taskScoreChanged.bind(this)}
			taskCommentAdded={this.taskCommentAdded.bind(this)}
			employees={this.props.model.employees}
			roomLog={this.props.model.roomLog}
			result={this.state.result}
			scorePoints={this.state.scorePoints}
			scorePercentage={this.state.scorePercentage}
			showErrors={this.state.showErrors}
			schedule={this.props.model.schedule}
			onSaveClicked={this.onSaveClicked.bind(this)}
			onCancelClicked={this.onCancelClicked.bind(this)}
			tasks={this.props.model.tasks}
		/>;
	}
}


$('#backToBoardButton').hide();
