/**
 * Inspection list view.
 */
class InspectionList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			list: []
		};
	}

	componentDidMount() {
		inspectionService.getAll().
		then(data => {
			this.setState({list: data.logs});
		}).catch(() => {
			this.setState({list: []});
			alert('Failed to get inspections');
		});
	}

	render() {
		return <aside className="right-side">
			<AdminViewHeader
				url='/admin/inspections/create'
				buttonLabel="Add Inspection"
				header="Inspections"/>
			<section className="content">
				<table className="table table-bordered">
					<thead>
					<tr className="tblheading">
						<th>Inspection Name</th>
						<th width="10%">Options</th>
					</tr>
					</thead>
					<InspectionListRow list={this.state.list}/>
				</table>
			</section>
		</aside>
	}
}

if (typeof module !== 'undefined') module.exports = InspectionList;
ReactDOM.render(<InspectionList />, document.getElementById('view'));
