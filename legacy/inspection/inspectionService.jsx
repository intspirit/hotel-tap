'use strict';
const inspectionService = {
	getAll: () => {
		return serviceBase.get('/api/inspections');
	},

	getById: (id) => {
		return serviceBase.get(`/api/inspections/${id}`);
	},

	add: (name, description) => {
		const data = {
			name: name,
			description: description,
			type: 2
		};
		return serviceBase.post('/api/inspections', {data: data});
	},

	save: (id, data) => {
		return serviceBase.put(`/api/inspections/${id}`, {data: data});
	},

	addSchedule: (id, data) => {
		return serviceBase.post(`/api/inspections/${id}/schedules`, {data: data});
	},

	updateSchedule: (inspectionId, scheduleId, data) => {
		return serviceBase.put(`/api/inspections/${inspectionId}/schedules/${scheduleId}`, {data: data});
	},
};
