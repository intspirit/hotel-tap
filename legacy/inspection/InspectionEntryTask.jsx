const
	Grid = ReactBootstrap.Grid,
	FormControls = ReactBootstrap.FormControls,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col,
	ButtonGroup = ReactBootstrap.ButtonGroup,
	Button = ReactBootstrap.Button,
	Table = ReactBootstrap.Table;

/**
 * Inspection entry - task component.
 */
class InspectionEntryTask extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			done: false,
			incompleteButtonStyle: 'warning',
			passButtonStyle: 'default',
			failButtonStyle: 'default',
			result: null,
			passed: false,
			completed: false,

			displayCommentForm: false,
			processingComment: false,
			commentText: '',
			displayAttachments: true,
			newAttachments: [],
			comments: []
		};
	}

	onClick(event) {
		event.preventDefault();
		this.props.taskScoreChanged(this.props.task.id, event.target.id);
		switch (event.target.id) {
			case 'pass' :
				this.setState({
					incompleteButtonStyle: 'default',
					passButtonStyle: 'success',
					failButtonStyle: 'default',
					result: this.props.task.points,
					completed: true,
					passed: true
				});
				break;
			case 'fail' :
				this.setState({
					incompleteButtonStyle: 'default',
					passButtonStyle: 'default',
					failButtonStyle: 'danger',
					result: 0,
					completed: true,
					passed: false
				});
				break;
			default :
				this.setState({
					incompleteButtonStyle: 'warning',
					passButtonStyle: 'default',
					failButtonStyle: 'default',
					result: null,
					completed: false,
					passed: false
				});
				break;
		}
	}

	toggleForm(event) {
		event.preventDefault();

		this.setState({displayCommentForm: !this.state.displayCommentForm});
	}

	addComment(event) {
		event.preventDefault();
		if (!this.state.commentText && this.state.newAttachments.length == 0) {
			return;
		}

		let data = {
			comment: this.state.commentText
		}
		this.props.taskCommentAdded(this.props.task.id, data, this.state.newAttachments);

		this.setState({
			displayCommentForm: false,
			commentText: '',
			newAttachments: [],
			comments: this.state.comments.concat(data)
		});
	}

	commentChange(event) {
		this.setState({commentText: event.target.value});
	}

	changeAttachment(files, event) {
		files.map(function (file) {
			file.id = _.uniqueId();
		});
		this.setState({newAttachments: this.state.newAttachments.concat(files)});
	}

	getAttachmentPreview(attachment) {
		if (attachment.type.indexOf('image') > -1) return <img src={attachment.preview}/>;

		return <div className="attachment-non-image">
			<div className="fa fa-file-text-o attachment-icon"></div>
			<div className="attachment-name">{attachment.name}</div>
		</div>;
	}

	removeAttachment(attachment, event) {
		event.preventDefault();

		this.setState({
			newAttachments: this.state.newAttachments.filter(function (item) {
				return attachment.id !== item.id;
			})
		});

		return false;
	}

	render() {
		const passFailButtons = <ButtonGroup justified onClick={this.onClick.bind(this)} data-task={this.props.task.id}>
			<Button style={{fontWeight: 'normal'}} id='incomplete' href="#"
			        bsStyle={this.state.incompleteButtonStyle}>
				<small>Incomplete</small>
			</Button>
			<Button style={{fontWeight: 'normal'}} id='pass' href="#"
			        bsStyle={this.state.passButtonStyle}>Pass</Button>
			<Button style={{fontWeight: 'normal'}} id='fail' href="#"
			        bsStyle={this.state.failButtonStyle}>Fail</Button>
		</ButtonGroup>;

		return <tr key={this.props.task.id}>
			<td>
				<span style={{fontWeight: 'normal'}}>{this.props.task.subject}</span><br/>
				<Row className="hidden-md hidden-lg">
					<Col xs={4}>Points: {this.props.task.points}&nbsp;</Col>
					<Col xs={4}>Result: {this.state.result}</Col>
					<Col xs={4}>
						<span className="pull-right" style={{color: 'red;'}}>
							{this.props.task.failAll == 0 ? null : 'fail all'}
						</span>
					</Col>
				</Row>
				<div className="hidden-md hidden-lg" style={{marginTop: '8px;', marginBottom: '8px;'}}>
					{passFailButtons}
				</div>

				<div style={{display: this.state.displayCommentForm ? 'none' : 'block'}}>
					<a href='#'
					   style={{fontWeight: "normal !important;", fontSize: '12px'}}
					   onClick={this.toggleForm.bind(this)}>
						Comment
					</a>
					<span className="badge comments-count"
					      style={{display: this.state.comments.length ? 'inline-block' : 'none'}}>
						{this.state.comments.length}
					</span>
				</div>
				<div className="comment-form"
				     style={{display: this.state.displayCommentForm ? 'block' : 'none'}}>
					<textarea name="comment"
					          className="comment-text"
					          value={this.state.commentText}
					          onChange={this.commentChange.bind(this)}/>

					<div className="clearfix"></div>
					<div className="attachments-preview">
						{this.state.newAttachments.map(attachment => {
							return <div className="attachment-preview">
								<div className="attachment">
									{this.getAttachmentPreview(attachment)}
								</div>
								<div className="attachment-actions">
									<span className="remove-attachment"
									      onClick={this.removeAttachment.bind(this, attachment)}>Remove
									</span>
								</div>
							</div>
						})}
					</div>

					<Dropzone className="dropzone" onDrop={this.changeAttachment.bind(this)}>
						<div>Try dropping some files here, or click to select files to upload.</div>
					</Dropzone>

					<div className="clearfix"></div>
					<div className="pull-right comment-actions">
						<button className="btn btn-link btn-sm" onClick={this.toggleForm.bind(this)}>Cancel</button>
						<button className="btn btn-success btn-sm" onClick={this.addComment.bind(this)}>Add
						</button>
					</div>
					<div className="clearfix"></div>
				</div>
			</td>
			<td className="hidden-xs hidden-sm">
				<span style={{fontWeight: 'normal'}}>{this.props.task.points}</span>
			</td>
			<td className="hidden-xs hidden-sm" style={{width: '350px;'}}>
				{passFailButtons}
			</td>
			<td className="hidden-xs hidden-sm">
				<span style={{fontWeight: 'normal'}}>{this.state.result}</span>
			</td>
			<td className="hidden-xs hidden-sm">
				<span style={{fontWeight: 'normal'}}>{this.props.task.failAll == 0 ? null : 'X'}</span>
			</td>
		</tr>
	}
}
InspectionEntryTask.propTypes = {};
InspectionEntryTask.defaultProps = {
	task: {
		id: -1,
		subject: '',
		points: 0,
		failAll: 0,
		done: false
	}
};