'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Button = ReactBootstrap.Button,
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col;

/**
 * Inspection entry - action buttons.
 * @param score
 * @param passingPoints
 * @returns {number}
 */
function getScorePercentage(score, passingPoints) {
	return parseInt(score / passingPoints * 100);
}

/**
 *
 * @param props
 * @constructor
 */
var InspectionEntryActionButtons = function InspectionEntryActionButtons(props) {
	return React.createElement(
		Row,
		null,
		React.createElement(
			Col,
			{ xs: 12 },
			React.createElement(
				Button,
				{ bsStyle: 'primary', className: 'pull-right', onClick: props.onSaveClicked },
				'Save'
			),
			React.createElement(
				Button,
				{ bsStyle: 'link', className: 'pull-right', onClick: props.onCancelClicked },
				'Cancel'
			)
		)
	);
};

/**
 * Inspection entry component.
 */

var InspectionEntry = function (_React$Component) {
	_inherits(InspectionEntry, _React$Component);

	function InspectionEntry(props) {
		_classCallCheck(this, InspectionEntry);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionEntry).call(this, props));

		_this.employeeName = Utils.getEmployeeName(_this.props.schedule.employeeId, _this.props.employees);
		_this.userId = _this.props.schedule.supervisorId;
		return _this;
	}

	_createClass(InspectionEntry, [{
		key: 'render',
		value: function render() {
			var errorMessage = this.props.showErrors ? React.createElement(
				'div',
				{ className: 'alert alert-danger', role: 'alert' },
				'Please complete all tasks before saving the inspection.'
			) : null;
			var backUrl = '/hotel/empboard/' + this.userId;
			return React.createElement(
				'div',
				{ id: 'inspectionEntry' },
				React.createElement(BoardViewHeader, {
					url: backUrl,
					showNavigation: false,
					buttonLabel: 'Back to board',
					buttonLabelSmall: 'Back',
					header: this.title }),
				React.createElement(LabelValueView, { label: 'Date:', value: moment(this.props.schedule.dueDateTime).format('L') }),
				React.createElement(LabelValueView, {
					label: 'Employee:',
					value: this.employeeName }),
				React.createElement(LabelValueView, {
					label: 'Supervisor:',
					value: Utils.getEmployeeName(this.props.schedule.supervisorId, this.props.employees) }),
				React.createElement(LabelValueView, { label: 'Location:', value: this.props.schedule.roomName }),
				React.createElement(InspectionEntryTasks, {
					roomLog: this.props.roomLog,
					result: this.props.result,
					scorePoints: this.props.scorePoints,
					scorePercentage: this.props.scorePercentage,
					taskScoreChanged: this.props.taskScoreChanged,
					taskCommentAdded: this.props.taskCommentAdded,
					tasks: this.props.tasks }),
				errorMessage,
				React.createElement(InspectionEntryActionButtons, {
					onSaveClicked: this.props.onSaveClicked,
					onCancelClicked: this.props.onCancelClicked })
			);
		}
	}, {
		key: 'title',
		get: function get() {
			return this.props.roomLog.name + ' - ' + this.employeeName + ' on \n\t\t' + moment(this.props.schedule.dueDateTime).format('L');
		}
	}, {
		key: 'titleSmall',
		get: function get() {
			return React.createElement(
				'span',
				null,
				this.props.roomLog.name,
				React.createElement('br', null),
				'by ',
				this.employeeName,
				React.createElement('br', null),
				'on ',
				moment(this.props.schedule.dueDateTime).format('L')
			);
		}
	}]);

	return InspectionEntry;
}(React.Component);
//# sourceMappingURL=InspectionEntry.js.map