'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Grid = ReactBootstrap.Grid,
    FormControls = ReactBootstrap.FormControls,
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    ButtonGroup = ReactBootstrap.ButtonGroup,
    Button = ReactBootstrap.Button,
    Table = ReactBootstrap.Table;

/**
 * Inspection entry - task component.
 */

var InspectionEntryTask = function (_React$Component) {
	_inherits(InspectionEntryTask, _React$Component);

	function InspectionEntryTask(props) {
		_classCallCheck(this, InspectionEntryTask);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionEntryTask).call(this, props));

		_this.state = {
			done: false,
			incompleteButtonStyle: 'warning',
			passButtonStyle: 'default',
			failButtonStyle: 'default',
			result: null,
			passed: false,
			completed: false,

			displayCommentForm: false,
			processingComment: false,
			commentText: '',
			displayAttachments: true,
			newAttachments: [],
			comments: []
		};
		return _this;
	}

	_createClass(InspectionEntryTask, [{
		key: 'onClick',
		value: function onClick(event) {
			event.preventDefault();
			this.props.taskScoreChanged(this.props.task.id, event.target.id);
			switch (event.target.id) {
				case 'pass':
					this.setState({
						incompleteButtonStyle: 'default',
						passButtonStyle: 'success',
						failButtonStyle: 'default',
						result: this.props.task.points,
						completed: true,
						passed: true
					});
					break;
				case 'fail':
					this.setState({
						incompleteButtonStyle: 'default',
						passButtonStyle: 'default',
						failButtonStyle: 'danger',
						result: 0,
						completed: true,
						passed: false
					});
					break;
				default:
					this.setState({
						incompleteButtonStyle: 'warning',
						passButtonStyle: 'default',
						failButtonStyle: 'default',
						result: null,
						completed: false,
						passed: false
					});
					break;
			}
		}
	}, {
		key: 'toggleForm',
		value: function toggleForm(event) {
			event.preventDefault();

			this.setState({ displayCommentForm: !this.state.displayCommentForm });
		}
	}, {
		key: 'addComment',
		value: function addComment(event) {
			event.preventDefault();
			if (!this.state.commentText && this.state.newAttachments.length == 0) {
				return;
			}

			var data = {
				comment: this.state.commentText
			};
			this.props.taskCommentAdded(this.props.task.id, data, this.state.newAttachments);

			this.setState({
				displayCommentForm: false,
				commentText: '',
				newAttachments: [],
				comments: this.state.comments.concat(data)
			});
		}
	}, {
		key: 'commentChange',
		value: function commentChange(event) {
			this.setState({ commentText: event.target.value });
		}
	}, {
		key: 'changeAttachment',
		value: function changeAttachment(files, event) {
			files.map(function (file) {
				file.id = _.uniqueId();
			});
			this.setState({ newAttachments: this.state.newAttachments.concat(files) });
		}
	}, {
		key: 'getAttachmentPreview',
		value: function getAttachmentPreview(attachment) {
			if (attachment.type.indexOf('image') > -1) return React.createElement('img', { src: attachment.preview });

			return React.createElement(
				'div',
				{ className: 'attachment-non-image' },
				React.createElement('div', { className: 'fa fa-file-text-o attachment-icon' }),
				React.createElement(
					'div',
					{ className: 'attachment-name' },
					attachment.name
				)
			);
		}
	}, {
		key: 'removeAttachment',
		value: function removeAttachment(attachment, event) {
			event.preventDefault();

			this.setState({
				newAttachments: this.state.newAttachments.filter(function (item) {
					return attachment.id !== item.id;
				})
			});

			return false;
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var passFailButtons = React.createElement(
				ButtonGroup,
				{ justified: true, onClick: this.onClick.bind(this), 'data-task': this.props.task.id },
				React.createElement(
					Button,
					{ style: { fontWeight: 'normal' }, id: 'incomplete', href: '#',
						bsStyle: this.state.incompleteButtonStyle },
					React.createElement(
						'small',
						null,
						'Incomplete'
					)
				),
				React.createElement(
					Button,
					{ style: { fontWeight: 'normal' }, id: 'pass', href: '#',
						bsStyle: this.state.passButtonStyle },
					'Pass'
				),
				React.createElement(
					Button,
					{ style: { fontWeight: 'normal' }, id: 'fail', href: '#',
						bsStyle: this.state.failButtonStyle },
					'Fail'
				)
			);

			return React.createElement(
				'tr',
				{ key: this.props.task.id },
				React.createElement(
					'td',
					null,
					React.createElement(
						'span',
						{ style: { fontWeight: 'normal' } },
						this.props.task.subject
					),
					React.createElement('br', null),
					React.createElement(
						Row,
						{ className: 'hidden-md hidden-lg' },
						React.createElement(
							Col,
							{ xs: 4 },
							'Points: ',
							this.props.task.points,
							' '
						),
						React.createElement(
							Col,
							{ xs: 4 },
							'Result: ',
							this.state.result
						),
						React.createElement(
							Col,
							{ xs: 4 },
							React.createElement(
								'span',
								{ className: 'pull-right', style: { color: 'red;' } },
								this.props.task.failAll == 0 ? null : 'fail all'
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'hidden-md hidden-lg', style: { marginTop: '8px;', marginBottom: '8px;' } },
						passFailButtons
					),
					React.createElement(
						'div',
						{ style: { display: this.state.displayCommentForm ? 'none' : 'block' } },
						React.createElement(
							'a',
							{ href: '#',
								style: { fontWeight: "normal !important;", fontSize: '12px' },
								onClick: this.toggleForm.bind(this) },
							'Comment'
						),
						React.createElement(
							'span',
							{ className: 'badge comments-count',
								style: { display: this.state.comments.length ? 'inline-block' : 'none' } },
							this.state.comments.length
						)
					),
					React.createElement(
						'div',
						{ className: 'comment-form',
							style: { display: this.state.displayCommentForm ? 'block' : 'none' } },
						React.createElement('textarea', { name: 'comment',
							className: 'comment-text',
							value: this.state.commentText,
							onChange: this.commentChange.bind(this) }),
						React.createElement('div', { className: 'clearfix' }),
						React.createElement(
							'div',
							{ className: 'attachments-preview' },
							this.state.newAttachments.map(function (attachment) {
								return React.createElement(
									'div',
									{ className: 'attachment-preview' },
									React.createElement(
										'div',
										{ className: 'attachment' },
										_this2.getAttachmentPreview(attachment)
									),
									React.createElement(
										'div',
										{ className: 'attachment-actions' },
										React.createElement(
											'span',
											{ className: 'remove-attachment',
												onClick: _this2.removeAttachment.bind(_this2, attachment) },
											'Remove'
										)
									)
								);
							})
						),
						React.createElement(
							Dropzone,
							{ className: 'dropzone', onDrop: this.changeAttachment.bind(this) },
							React.createElement(
								'div',
								null,
								'Try dropping some files here, or click to select files to upload.'
							)
						),
						React.createElement('div', { className: 'clearfix' }),
						React.createElement(
							'div',
							{ className: 'pull-right comment-actions' },
							React.createElement(
								'button',
								{ className: 'btn btn-link btn-sm', onClick: this.toggleForm.bind(this) },
								'Cancel'
							),
							React.createElement(
								'button',
								{ className: 'btn btn-success btn-sm', onClick: this.addComment.bind(this) },
								'Add'
							)
						),
						React.createElement('div', { className: 'clearfix' })
					)
				),
				React.createElement(
					'td',
					{ className: 'hidden-xs hidden-sm' },
					React.createElement(
						'span',
						{ style: { fontWeight: 'normal' } },
						this.props.task.points
					)
				),
				React.createElement(
					'td',
					{ className: 'hidden-xs hidden-sm', style: { width: '350px;' } },
					passFailButtons
				),
				React.createElement(
					'td',
					{ className: 'hidden-xs hidden-sm' },
					React.createElement(
						'span',
						{ style: { fontWeight: 'normal' } },
						this.state.result
					)
				),
				React.createElement(
					'td',
					{ className: 'hidden-xs hidden-sm' },
					React.createElement(
						'span',
						{ style: { fontWeight: 'normal' } },
						this.props.task.failAll == 0 ? null : 'X'
					)
				)
			);
		}
	}]);

	return InspectionEntryTask;
}(React.Component);

InspectionEntryTask.propTypes = {};
InspectionEntryTask.defaultProps = {
	task: {
		id: -1,
		subject: '',
		points: 0,
		failAll: 0,
		done: false
	}
};

//# sourceMappingURL=InspectionEntryTask.js.map