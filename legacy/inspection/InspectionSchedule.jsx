/**
 * Inspection schedule component.
 */
class InspectionSchedule extends React.Component {
	constructor(props) {
		super(props);
		this.employees = _.keyBy(this.props.employees, 'user_id');
		this.allEmployees = _.keyBy(this.props.allEmployees, 'user_id');
		this.rooms = _.keyBy(this.props.rooms, 'id');

		const schedule = this.prepareSchedule(this.props.schedule);

		this.state = {
			schedule: schedule,
			savedValues: _.clone(schedule),
			errors: {},
			mode: 'view'
		};
	}

	componentWillReceiveProps(nextProps) {
		const schedule = this.prepareSchedule(nextProps.schedule);
		this.setState({
			schedule: schedule,
			savedValues: _.clone(schedule)
		});
	}

	prepareSchedule(schedule) {
		const supervisor = this.employees[schedule.supervisorId];
		const isAssigned = schedule.completionDateTime == null;
		if (!supervisor && isAssigned) {
			schedule.supervisorId = -1;
			schedule.supervisorName = 'Unassigned';
		}

		const employee = this.employees[schedule.employeeId];
		if (!employee && isAssigned) {
			schedule.employeeId = schedule.supervisorId;
			schedule.employeeName = schedule.supervisorName;
		}

		return schedule;
	}

	getViewModel() {
		const employee = this.allEmployees[this.state.schedule.employeeId];
		const supervisor = this.allEmployees[this.state.schedule.supervisorId];
		const room = this.rooms[this.state.schedule.roomId] || {name: 'Location deactivated'};

		const status = this.state.schedule.completionDateTime == null ? 'Assigned' : 'Completed';
		const result = status === 'Assigned' ?
			'' :
			(this.state.schedule.inspectionPassed == 1 ? 'Pass' : 'Fail');

		return {
			status: status,
			result: result,
			rowClassName: status === 'Assigned' ? 'color-assigned' : '',
			date: this.state.schedule.dueDateTime.split(' ')[0],
			employeeName: employee ? employee.fullName : 'Unassigned',
			employeeNameClass: employee ? '' : 'text-red',
			supervisorName: supervisor ? supervisor.fullName : 'Unassigned',
			supervisorNameClass: supervisor ? '' : 'text-red',
			roomName: room.name,
			roomNameClass: room.name === 'Location deactivated' ? 'text-red' : '',
			score: this.state.schedule.scorePercentage || '',
			resultClassName: result === 'Pass' ? 'c-green' : 'c-red',
			editViewButtonLabel: status === 'Assigned' ? 'Edit' : 'View'
		};
	}

	onEditButtonClick() {
		if (this.state.schedule.completionDateTime != null) {
			window.location.assign(`/admin/inspections/${this.props.inspectionId}/schedules/${this.props.schedule.id}`);
		} else {
			this.setState({mode: 'edit'});
		}
	}

	get viewRow() {
		const viewModel = this.getViewModel();

		return <tr key={this.state.schedule.id} className={viewModel.rowClassName}>
			<th>{viewModel.date}</th>
			<th className={viewModel.employeeNameClass}>{viewModel.employeeName}</th>
			<th className={viewModel.supervisorNameClass}>{viewModel.supervisorName}</th>
			<th className={viewModel.roomNameClass}>{viewModel.roomName}</th>
			<th>{viewModel.status}</th>
			<th>{viewModel.score}</th>
			<th className={viewModel.resultClassName}>{viewModel.result}</th>
			<th width="10%">
				<a
					type="button"
					onClick={this.onEditButtonClick.bind(this)}
					className="btn btn-link">{viewModel.editViewButtonLabel}
				</a>
			</th>
		</tr>;
	}

	isValid() {
		let isValid = true;
		let errors = this.state.errors || {};
		if (this.state.schedule.dueDateTime == -1 || !this.state.schedule.dueDateTime) {
			errors.date = true;
			isValid = false;
		}

		if (this.state.schedule.roomId == -1 || !this.state.schedule.roomId) {
			errors.roomId = true;
			isValid = false;
		}

		if (this.state.schedule.employeeId == -1 || !this.state.schedule.employeeId) {
			errors.employeeId = true;
			isValid = false;
		}

		if (this.state.schedule.supervisorId == -1 || !this.state.schedule.supervisorId) {
			errors.supervisorId = true;
			isValid = false;
		}

		this.setState({errors: errors});

		return isValid;
	}

	onSaveClick() {
		if (!this.isValid()) {
			return;
		}

		this.props.onScheduleUpdated(this.state.schedule);
		this.setState({mode: 'view'});
	}

	onCancelClick() {
		this.setState({
			schedule: _.clone(this.state.savedValues),
			mode: 'view'
		});
	}

	onDateChange(event) {
		let errors = this.state.errors || {};
		errors.date = false;

		this.state.schedule.dueDateTime = event.target.value;
		this.setState({
			schedule: this.state.schedule,
			errors: errors
		})
	}

	onEmployeeChange(event) {
		let errors = this.state.errors || {};
		errors.employeeId = false;

		this.state.schedule.employeeId = event.target.value;

		const employee = this.employees[event.target.value];
		this.state.schedule.employeeName = employee ? employee.fullName : '';

		this.setState({
			schedule: this.state.schedule,
			errors: errors
		})
	}

	onSupervisorChange(event) {
		let errors = this.state.errors || {};
		errors.supervisorId = false;

		this.state.schedule.supervisorId = event.target.value;

		const supervisor = this.employees[event.target.value];
		this.state.schedule.supervisorName = supervisor ? supervisor.fullName : '';

		this.setState({
			schedule: this.state.schedule,
			errors: errors
		})
	}

	onRoomChange(event) {
		let errors = this.state.errors || {};
		errors.roomId = false;

		this.state.schedule.roomId = event.target.value;

		const room = this.rooms[event.target.value];
		this.state.schedule.roomName = room ? room.name : '';

		this.setState({
			schedule: this.state.schedule,
			errors: errors
		})
	}

	get editRow() {
		const date = this.state.schedule.dueDateTime.split(' ')[0];
		const supervisorId = this.employees[this.state.schedule.supervisorId]
			? this.state.schedule.supervisorId
			: -1;

		return <tr className="newRow" key={this.state.schedule.id}>
			<td>
				<input type="date"
					value={date}
					className={this.state.errors.date ? 'error' : ''}
					onChange={this.onDateChange.bind(this)}/>
			</td>
			<td>
				<select
					id="employeeId"
					value={this.state.schedule.employeeId}
					className={this.state.errors.employeeId ? 'error' : ''}
					onChange={this.onEmployeeChange.bind(this)}>
					<option value="-1" disabled="disabled">--Select employee--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select
					id="supervisorId"
					value={supervisorId}
					className={this.state.errors.supervisorId ? 'error' : ''}
					onChange={this.onSupervisorChange.bind(this)}>
					<option value="-1" disabled="disabled">--Select supervisor--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select id="roomId"
					key={this.state.schedule.roomId}
					value={this.state.schedule.roomId}
					className={this.state.errors.roomId ? 'error' : ''}
					onChange={this.onRoomChange.bind(this)}>
					{this.props.roomOptions}
				</select>
			</td>
			<td/>
			<td/>
			<td/>
			<td width="10%">
				<button type="button" className="btn btn-link" onClick={this.onSaveClick.bind(this)}>Save</button>
				<button type="button" className="btn btn-link" onClick={this.onCancelClick.bind(this)}>Cancel</button>
			</td>
		</tr>;
	}

	render() {
		return this.state.mode === 'view' ? this.viewRow : this.editRow;
	}
}
if (typeof module !== 'undefined') module.exports = InspectionSchedule;
