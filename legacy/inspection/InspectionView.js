'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HotelTap = HotelTap || {};

/**
 * Completed inspection view task comment with attachments.
 * @param props
 * @returns {XML}
 * @constructor
 */
var InspectionViewTaskComment = function InspectionViewTaskComment(props) {
	var attachments = props.attachments.filter(function (x) {
		return x.commentId == props.comment.comment_id;
	});
	attachments.map(function (x) {
		x.filePath = '' + props.attachmentRoot + x.filePath;
	});
	return React.createElement(
		'div',
		null,
		React.createElement('div', { dangerouslySetInnerHTML: { __html: props.comment.comment }, style: { marginTop: '8px;' } }),
		React.createElement(
			'div',
			null,
			React.createElement(HotelTap.Attachments, { model: attachments || [] })
		)
	);
};

/**
 * Completed inspection view task comments.
 * @param props
 * @returns {*}
 * @constructor
 */
var InspectionViewTaskComments = function InspectionViewTaskComments(props) {
	var comments = props.comments.filter(function (x) {
		return x.source_id == props.task.id;
	});
	if (comments.length == 0) return React.createElement('div', null);

	return React.createElement(
		'div',
		{ style: { marginTop: '10px;' } },
		React.createElement(
			'span',
			{ style: { fontStyle: 'italic;' } },
			'Comments:'
		),
		comments.map(function (x) {
			return React.createElement(InspectionViewTaskComment, {
				comment: x,
				attachments: props.attachments,
				attachmentRoot: props.attachmentRoot
			});
		})
	);
};

/**
 * Completed inspection view task row component.
 */
var InspectionViewTask = function InspectionViewTask(props) {
	var isPassed = props.task.points == props.task.score;
	var status = isPassed ? 'passed' : 'failed';
	return React.createElement(
		'tr',
		{ key: props.task.id, className: 'task-' + status },
		React.createElement(
			'td',
			null,
			React.createElement(
				'strong',
				null,
				props.task.subject
			),
			React.createElement(InspectionViewTaskComments, props)
		),
		React.createElement(
			'td',
			null,
			props.task.points
		),
		React.createElement(
			'td',
			{ className: 'status-label' },
			isPassed ? 'Pass' : 'Fail'
		),
		React.createElement(
			'td',
			null,
			props.task.score
		),
		React.createElement(
			'td',
			null,
			props.task.failAll == 1 ? 'Yes' : ''
		)
	);
};

/**
 * Completed inspection view component.
 */

var InspectionView = function (_React$Component) {
	_inherits(InspectionView, _React$Component);

	function InspectionView(props) {
		_classCallCheck(this, InspectionView);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionView).call(this, props));

		_this.state = { gotData: false };
		return _this;
	}

	_createClass(InspectionView, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			$.ajax({
				url: '/api/schedules/' + this.props.scheduleId,
				dataType: 'json',
				type: 'GET',
				success: function success(data) {
					_this2.setState({
						gotData: true,
						completionDateTime: moment(data.completionDateTime).format('L'),
						employeeName: Utils.getEmployeeName(data.employeeId, data.employees),
						supervisorName: Utils.getEmployeeName(data.supervisorId, data.employees),
						roomNumber: data.roomName,
						scorePoints: (data.scorePoints || 0) + ' of ' + (data.passingScorePoints || data.roomLog.passingScorePoints) + '\n\t\t\t\t\t (total points: ' + data.roomLog.totalPoints + ')',
						scorePercentage: (data.scorePercentage || 0) + '%\n\t\t\t\t\t (passing score: ' + (data.passingScorePercentage || data.roomLog.passingScorePercentage) + '%)',
						result: data.inspectionPassed == 1 ? 'Passed' : 'Failed',
						tasks: data.completedTasks,
						attachments: data.attachments,
						comments: data.comments,
						attachmentRoot: data.attachmentRoot
					});
				},
				error: function error() {
					alert('Failed to show the inspection');
					window.location.assign('/admin/inspections/' + _this2.props.inspectionId);
				}
			});
		}
	}, {
		key: 'onPrintClick',
		value: function onPrintClick() {
			this.printElement(document.getElementById("printIt"));
			window.print();
		}
	}, {
		key: 'printElement',
		value: function printElement(elem, append, delimiter) {
			var domClone = elem.cloneNode(true);

			var $printSection = document.getElementById("printSection");

			if (!$printSection) {
				var $printSection = document.createElement("div");
				$printSection.id = "printSection";
				document.body.appendChild($printSection);
			}

			if (append !== true) {
				$printSection.innerHTML = "";
			} else if (append === true) {
				if (typeof delimiter === "string") {
					$printSection.innerHTML += delimiter;
				} else if ((typeof delimiter === 'undefined' ? 'undefined' : _typeof(delimiter)) === "object") {
					$printSection.appendChlid(delimiter);
				}
			}

			$printSection.appendChild(domClone);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			if (!this.state.gotData) return null;

			var printButton = React.createElement(
				'span',
				{ className: 'btn btn-primary', onClick: this.onPrintClick.bind(this) },
				'Print'
			);
			return React.createElement(
				'aside',
				{ className: 'right-side', id: 'printIt' },
				React.createElement(AdminViewHeader, {
					url: '/admin/inspections/' + this.props.inspectionId,
					buttonLabel: 'Back to Inspections',
					printButton: printButton,
					header: 'Completed Inspection' }),
				React.createElement(
					'section',
					{ className: 'content' },
					React.createElement(
						'h1',
						null,
						this.title
					),
					React.createElement(LabelValueView, { label: 'Date:', value: this.state.completionDateTime }),
					React.createElement(LabelValueView, { label: 'Employee:', value: this.state.employeeName }),
					React.createElement(LabelValueView, { label: 'Supervisor:', value: this.state.supervisorName }),
					React.createElement(LabelValueView, { label: 'Location:', value: this.state.roomNumber }),
					React.createElement(LabelValueView, { label: 'Score (points):', value: this.state.scorePoints }),
					React.createElement(LabelValueView, { label: 'Score (%):', value: this.state.scorePercentage }),
					React.createElement(LabelValueView, { label: 'Result:', value: this.state.result }),
					React.createElement(
						'table',
						{ className: 'table table-bordered', style: { marginTop: '15px', backgroundColor: 'white' } },
						React.createElement(
							'thead',
							null,
							React.createElement(
								'tr',
								{ className: 'tblheading' },
								React.createElement(
									'th',
									null,
									'Task'
								),
								React.createElement(
									'th',
									{ width: '100px' },
									'Points'
								),
								React.createElement(
									'th',
									{ width: '100px' },
									'Pass/Fail'
								),
								React.createElement(
									'th',
									{ width: '100px' },
									'Result'
								),
								React.createElement(
									'th',
									{ width: '100px' },
									'Fail All'
								)
							)
						),
						React.createElement(
							'tbody',
							null,
							this.state.tasks.map(function (x) {
								return React.createElement(InspectionViewTask, {
									task: x,
									comments: _this3.state.comments,
									attachments: _this3.state.attachments,
									attachmentRoot: _this3.state.attachmentRoot
								});
							})
						)
					)
				)
			);
		}
	}, {
		key: 'title',
		get: function get() {
			return 'Inspection of ' + this.state.roomNumber + ' by ' + this.state.employeeName + ' on ' + this.state.completionDateTime;
		}
	}]);

	return InspectionView;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionView;
var $viewContainer = $('#view');
ReactDOM.render(React.createElement(InspectionView, {
	inspectionId: $($viewContainer).data('inspection'),
	scheduleId: $($viewContainer).data('schedule')
}), document.getElementById('view'));
//# sourceMappingURL=InspectionView.js.map