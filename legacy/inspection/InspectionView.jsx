'use strict';

let HotelTap = HotelTap || {};

/**
 * Completed inspection view task comment with attachments.
 * @param props
 * @returns {XML}
 * @constructor
 */
const InspectionViewTaskComment = (props) => {
	const attachments = props.attachments.filter(x => x.commentId == props.comment.comment_id);
	attachments.map(x => {
		x.filePath = `${props.attachmentRoot}${x.filePath}`
	});
	return <div>
		<div dangerouslySetInnerHTML={{__html: props.comment.comment}} style={{marginTop: '8px;'}}></div>
		<div>
			<HotelTap.Attachments model={attachments || []}/>
		</div>
	</div>
};


/**
 * Completed inspection view task comments.
 * @param props
 * @returns {*}
 * @constructor
 */
const InspectionViewTaskComments = (props) => {
	const comments = props.comments.filter(x => (x.source_id == props.task.id));
	if (comments.length == 0) return <div></div>;

	return <div style={{marginTop: '10px;'}}>
		<span style={{fontStyle: 'italic;'}}>Comments:</span>
		{comments.map(x => <InspectionViewTaskComment
			comment={x}
			attachments={props.attachments}
			attachmentRoot={props.attachmentRoot}
		/>)}
	</div>;
};


/**
 * Completed inspection view task row component.
 */
const InspectionViewTask = (props) => {
	const isPassed = props.task.points == props.task.score;
	const status = isPassed ? 'passed' : 'failed';
	return <tr key={props.task.id} className={`task-${status}`}>
		<td>
			<strong>{props.task.subject}</strong>
			<InspectionViewTaskComments {...props} />
		</td>
		<td>{props.task.points}</td>
		<td className="status-label">{isPassed ? 'Pass' : 'Fail'}</td>
		<td>{props.task.score}</td>
		<td>{props.task.failAll == 1 ? 'Yes' : ''}</td>
	</tr>;
}


/**
 * Completed inspection view component.
 */
class InspectionView extends React.Component {
	constructor(props) {
		super(props);
		this.state = {gotData: false}
	}

	get title() {
		return `Inspection of ${this.state.roomNumber} by ${this.state.employeeName} on ${this.state.completionDateTime}`;
	}

	componentDidMount() {
		$.ajax({
			url: `/api/schedules/${this.props.scheduleId}`,
			dataType: 'json',
			type: 'GET',
			success: (data) => {
				this.setState({
					gotData: true,
					completionDateTime: moment(data.completionDateTime).format('L'),
					employeeName: Utils.getEmployeeName(data.employeeId, data.employees),
					supervisorName: Utils.getEmployeeName(data.supervisorId, data.employees),
					roomNumber: data.roomName,
					scorePoints: `${data.scorePoints || 0} of ${data.passingScorePoints|| data.roomLog.passingScorePoints}
					 (total points: ${data.roomLog.totalPoints})`,
					scorePercentage: `${data.scorePercentage || 0}%
					 (passing score: ${data.passingScorePercentage || data.roomLog.passingScorePercentage}%)`,
					result: data.inspectionPassed == 1 ? 'Passed' : 'Failed',
					tasks: data.completedTasks,
					attachments: data.attachments,
					comments: data.comments,
					attachmentRoot: data.attachmentRoot
				});
			},
			error: () => {
				alert('Failed to show the inspection');
				window.location.assign(`/admin/inspections/${this.props.inspectionId}`);
			}
		});
	}

	onPrintClick() {
		this.printElement(document.getElementById("printIt"));
		window.print();
	}

	printElement(elem, append, delimiter) {
		var domClone = elem.cloneNode(true);

		var $printSection = document.getElementById("printSection");

		if (!$printSection) {
			var $printSection = document.createElement("div");
			$printSection.id = "printSection";
			document.body.appendChild($printSection);
		}

		if (append !== true) {
			$printSection.innerHTML = "";
		}

		else if (append === true) {
			if (typeof(delimiter) === "string") {
				$printSection.innerHTML += delimiter;
			}
			else if (typeof(delimiter) === "object") {
				$printSection.appendChlid(delimiter);
			}
		}

		$printSection.appendChild(domClone);
	}

	render() {
		if (!this.state.gotData) return null;

		const printButton = <span className="btn btn-primary" onClick={this.onPrintClick.bind(this)}>Print</span>;
		return (
			<aside className="right-side" id="printIt">
				<AdminViewHeader
					url={`/admin/inspections/${this.props.inspectionId}`}
					buttonLabel="Back to Inspections"
					printButton={printButton}
					header="Completed Inspection"/>
				<section className="content">
					<h1>{this.title}</h1>

					<LabelValueView label='Date:' value={this.state.completionDateTime}/>
					<LabelValueView label='Employee:' value={this.state.employeeName}/>
					<LabelValueView label='Supervisor:' value={this.state.supervisorName}/>
					<LabelValueView label='Location:' value={this.state.roomNumber}/>
					<LabelValueView label='Score (points):' value={this.state.scorePoints}/>
					<LabelValueView label='Score (%):' value={this.state.scorePercentage}/>
					<LabelValueView label='Result:' value={this.state.result}/>

					<table className="table table-bordered" style={{marginTop: '15px', backgroundColor: 'white'}}>
						<thead>
						<tr className="tblheading">
							<th>Task</th>
							<th width='100px'>Points</th>
							<th width='100px'>Pass/Fail</th>
							<th width='100px'>Result</th>
							<th width='100px'>Fail All</th>
						</tr>
						</thead>
						<tbody>
						{this.state.tasks.map(x => <InspectionViewTask
							task={x}
							comments={this.state.comments}
							attachments={this.state.attachments}
							attachmentRoot={this.state.attachmentRoot}
						/>)}
						</tbody>
					</table>
				</section>
			</aside>
		);
	}
}

if (typeof module !== 'undefined') module.exports = InspectionView;
const $viewContainer = $('#view');
ReactDOM.render(<InspectionView
	inspectionId={$($viewContainer).data('inspection')}
	scheduleId={$($viewContainer).data('schedule')}
/>, document.getElementById('view'));
