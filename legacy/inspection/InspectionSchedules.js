'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var inspectionService = inspectionService || require("inspectionService");

/**
 * Inspection schedules.
 */

var InspectionSchedules = function (_React$Component) {
	_inherits(InspectionSchedules, _React$Component);

	function InspectionSchedules(props) {
		_classCallCheck(this, InspectionSchedules);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionSchedules).call(this, props));

		_this.employeeOptions = _this.props.activeEmployees.map(function (x) {
			return React.createElement(
				'option',
				{ value: x.user_id, key: x.user_id },
				x.fullName
			);
		});
		_this.roomOptions = _this.props.rooms.map(function (x) {
			return React.createElement(
				'option',
				{ value: x.id, key: x.id },
				x.name
			);
		});

		_this.schedulesStore = _this.props.schedules;
		_this.state = {
			schedules: _this.schedulesStore.map(function (x) {
				var employee = _.find(_this.props.activeEmployees, { user_id: x.employeeId });
				var supervisor = _.find(_this.props.activeEmployees, { user_id: x.supervisorId });
				return _.assign(x, {
					employeeName: employee ? employee.fullName : '',
					supervisorName: supervisor ? supervisor.fullName : ''
				});
			}),
			filters: {},
			sorting: {},
			filterVisible: true
		};
		_this.sortSchedules('dueDateTime');
		return _this;
	}

	_createClass(InspectionSchedules, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.setState({ filterVisible: false });
		}
	}, {
		key: 'onScheduleUpdated',
		value: function onScheduleUpdated(schedule) {
			var _this2 = this;

			var year = parseInt(schedule.dueDateTime.substr(0, 4));
			var month = parseInt(schedule.dueDateTime.substr(5, 2));
			var date = parseInt(schedule.dueDateTime.substr(8, 4));
			var data = {
				roomId: schedule.roomId,
				dueYear: year,
				dueMonth: month,
				dueDay: date,
				dueDateTime: new Date(year, month, date, 23, 59, 59, 0),
				employeeId: schedule.employeeId,
				supervisorId: schedule.supervisorId
			};

			schedule = _.assign(schedule, {
				dueYear: year.toString(),
				dueMonth: month.toString(),
				dueDay: date.toString()
			});

			inspectionService.updateSchedule(this.props.inspectionId, schedule.id, data).then(function () {
				_this2.schedulesStore.map(function (x) {
					if (x.id === schedule.id) x = _.assign(x, schedule);
					return x;
				});
				var schedules = _this2.getFiltered(_this2.schedulesStore);
				_this2.setState({
					schedules: _this2.getSorted(schedules)
				});
			}).catch(function () {
				alert('Failed to save inspection schedule');
			});
		}
	}, {
		key: 'onScheduleAdded',
		value: function onScheduleAdded(schedule) {
			var _this3 = this;

			var year = parseInt(schedule.dueDateTime.substr(0, 4));
			var month = parseInt(schedule.dueDateTime.substr(5, 2));
			var date = parseInt(schedule.dueDateTime.substr(8, 4));
			var data = {
				roomId: schedule.roomId,
				dueYear: year,
				dueMonth: month,
				dueDay: date,
				dueDateTime: new Date(year, month, date, 23, 59, 59, 0),
				employeeId: schedule.employeeId,
				supervisorId: schedule.supervisorId
			};

			schedule = _.assign(schedule, {
				dueYear: year.toString(),
				dueMonth: month.toString(),
				dueDay: date.toString()
			});

			inspectionService.addSchedule(this.props.inspectionId, data).then(function (response) {
				schedule.id = response.id;
				_this3.schedulesStore.push(schedule);
				var schedules = _this3.getFiltered(_this3.schedulesStore);
				_this3.setState({ schedules: _this3.getSorted(schedules) });
			}).catch(function () {
				alert('Failed to add inspection schedule');
			});
		}
	}, {
		key: 'getFiltered',
		value: function getFiltered(schedules) {
			var filters = this.state.filters;
			return _.filter(schedules, function (x) {
				var isValid = true;
				_.forEach(filters, function (value, key) {
					if (isValid === false) {
						return false;
					}

					if (key === "status") {
						var status = x.completionDateTime == null ? 'assigned' : 'completed';
						isValid = status === value;

						return;
					}

					isValid = x[key] === value;
				});

				return isValid;
			});
		}
	}, {
		key: 'onFilterChanged',
		value: function onFilterChanged(filters) {
			this.state.filters = filters;

			var schedules = this.getFiltered(this.schedulesStore);
			schedules = this.getSorted(schedules);

			this.setState({
				schedules: schedules,
				filters: this.state.filters
			});
		}
	}, {
		key: 'toggleFilter',
		value: function toggleFilter() {
			this.setState({
				filterVisible: !this.state.filterVisible
			});
		}
	}, {
		key: 'getSorted',
		value: function getSorted(schedules) {
			return schedules.sort(naturalSort(this.state.sorting));
		}
	}, {
		key: 'sortSchedules',
		value: function sortSchedules(key) {
			this.state.sorting = {
				key: key,
				direction: this.state.sorting.direction === 'asc' && this.state.sorting.key === key ? 'desc' : 'asc'
			};

			this.setState({
				schedules: this.getSorted(this.state.schedules),
				sorting: this.state.sorting
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this4 = this;

			var filterLabel = !this.state.filterVisible ? 'Show Filter' : 'Hide Filter';
			var columns = {
				dueDateTime: "Date",
				employeeName: "Employee",
				supervisorName: "Supervisor",
				roomName: "Location",
				completionDateTime: "Status",
				scorePercentage: "Score, %",
				inspectionPassed: "Result"
			};
			return React.createElement(
				'div',
				{ className: 'form-group', style: { overflowX: 'scroll' } },
				React.createElement(
					'h1',
					null,
					React.createElement(
						'span',
						{ className: 'btn btn-link btn-sm pull-right', onClick: this.toggleFilter.bind(this) },
						filterLabel
					),
					React.createElement(
						'span',
						null,
						'Inspections'
					)
				),
				React.createElement(
					'table',
					{ className: 'table table-bordered', id: 'inspectionTaskTable' },
					React.createElement(
						'thead',
						null,
						React.createElement(
							'tr',
							{ className: 'tblheading' },
							Object.keys(columns).map(function (x) {
								return React.createElement(
									'th',
									{ onClick: _this4.sortSchedules.bind(_this4, x),
										className: 'sorting-available ' + (_this4.state.sorting.key === x ? 'sorted-header' : '') },
									columns[x],
									React.createElement(
										'span',
										{ className: 'sorting-arrow ' + (_this4.state.sorting.key !== x ? 'hidden' : '') },
										React.createElement('i', { className: "glyphicon glyphicon-chevron-" + (_this4.state.sorting.direction === 'desc' ? 'down' : 'up') })
									)
								);
							}),
							React.createElement(
								'th',
								{ width: '15%' },
								'Options'
							)
						)
					),
					React.createElement(
						'tbody',
						null,
						React.createElement(InspectionScheduleFilter, {
							onFilterChanged: this.onFilterChanged.bind(this),
							employeeOptions: this.employeeOptions,
							isVisible: this.state.filterVisible,
							roomOptions: this.roomOptions }),
						React.createElement(InspectionNewSchedule, {
							onScheduleAdded: this.onScheduleAdded.bind(this),
							employeeOptions: this.employeeOptions,
							roomOptions: this.roomOptions }),
						this.state.schedules.map(function (x) {
							return React.createElement(InspectionSchedule, {
								key: x.id,
								onScheduleUpdated: _this4.onScheduleUpdated.bind(_this4),
								inspectionId: _this4.props.inspectionId,
								schedule: x,
								employees: _this4.props.activeEmployees,
								allEmployees: _this4.props.allEmployees,
								rooms: _this4.props.rooms,
								employeeOptions: _this4.employeeOptions,
								roomOptions: _this4.roomOptions
							});
						})
					)
				)
			);
		}
	}]);

	return InspectionSchedules;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionSchedules;
//# sourceMappingURL=InspectionSchedules.js.map