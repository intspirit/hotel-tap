'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Inspection schedule component.
 */

var InspectionSchedule = function (_React$Component) {
	_inherits(InspectionSchedule, _React$Component);

	function InspectionSchedule(props) {
		_classCallCheck(this, InspectionSchedule);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionSchedule).call(this, props));

		_this.employees = _.keyBy(_this.props.employees, 'user_id');
		_this.allEmployees = _.keyBy(_this.props.allEmployees, 'user_id');
		_this.rooms = _.keyBy(_this.props.rooms, 'id');

		var schedule = _this.prepareSchedule(_this.props.schedule);

		_this.state = {
			schedule: schedule,
			savedValues: _.clone(schedule),
			errors: {},
			mode: 'view'
		};
		return _this;
	}

	_createClass(InspectionSchedule, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			var schedule = this.prepareSchedule(nextProps.schedule);
			this.setState({
				schedule: schedule,
				savedValues: _.clone(schedule)
			});
		}
	}, {
		key: 'prepareSchedule',
		value: function prepareSchedule(schedule) {
			var supervisor = this.employees[schedule.supervisorId];
			var isAssigned = schedule.completionDateTime == null;
			if (!supervisor && isAssigned) {
				schedule.supervisorId = -1;
				schedule.supervisorName = 'Unassigned';
			}

			var employee = this.employees[schedule.employeeId];
			if (!employee && isAssigned) {
				schedule.employeeId = schedule.supervisorId;
				schedule.employeeName = schedule.supervisorName;
			}

			return schedule;
		}
	}, {
		key: 'getViewModel',
		value: function getViewModel() {
			var employee = this.allEmployees[this.state.schedule.employeeId];
			var supervisor = this.allEmployees[this.state.schedule.supervisorId];
			var room = this.rooms[this.state.schedule.roomId] || { name: 'Location deactivated' };

			var status = this.state.schedule.completionDateTime == null ? 'Assigned' : 'Completed';
			var result = status === 'Assigned' ? '' : this.state.schedule.inspectionPassed == 1 ? 'Pass' : 'Fail';

			return {
				status: status,
				result: result,
				rowClassName: status === 'Assigned' ? 'color-assigned' : '',
				date: this.state.schedule.dueDateTime.split(' ')[0],
				employeeName: employee ? employee.fullName : 'Unassigned',
				employeeNameClass: employee ? '' : 'text-red',
				supervisorName: supervisor ? supervisor.fullName : 'Unassigned',
				supervisorNameClass: supervisor ? '' : 'text-red',
				roomName: room.name,
				roomNameClass: room.name === 'Location deactivated' ? 'text-red' : '',
				score: this.state.schedule.scorePercentage || '',
				resultClassName: result === 'Pass' ? 'c-green' : 'c-red',
				editViewButtonLabel: status === 'Assigned' ? 'Edit' : 'View'
			};
		}
	}, {
		key: 'onEditButtonClick',
		value: function onEditButtonClick() {
			if (this.state.schedule.completionDateTime != null) {
				window.location.assign('/admin/inspections/' + this.props.inspectionId + '/schedules/' + this.props.schedule.id);
			} else {
				this.setState({ mode: 'edit' });
			}
		}
	}, {
		key: 'isValid',
		value: function isValid() {
			var isValid = true;
			var errors = this.state.errors || {};
			if (this.state.schedule.dueDateTime == -1 || !this.state.schedule.dueDateTime) {
				errors.date = true;
				isValid = false;
			}

			if (this.state.schedule.roomId == -1 || !this.state.schedule.roomId) {
				errors.roomId = true;
				isValid = false;
			}

			if (this.state.schedule.employeeId == -1 || !this.state.schedule.employeeId) {
				errors.employeeId = true;
				isValid = false;
			}

			if (this.state.schedule.supervisorId == -1 || !this.state.schedule.supervisorId) {
				errors.supervisorId = true;
				isValid = false;
			}

			this.setState({ errors: errors });

			return isValid;
		}
	}, {
		key: 'onSaveClick',
		value: function onSaveClick() {
			if (!this.isValid()) {
				return;
			}

			this.props.onScheduleUpdated(this.state.schedule);
			this.setState({ mode: 'view' });
		}
	}, {
		key: 'onCancelClick',
		value: function onCancelClick() {
			this.setState({
				schedule: _.clone(this.state.savedValues),
				mode: 'view'
			});
		}
	}, {
		key: 'onDateChange',
		value: function onDateChange(event) {
			var errors = this.state.errors || {};
			errors.date = false;

			this.state.schedule.dueDateTime = event.target.value;
			this.setState({
				schedule: this.state.schedule,
				errors: errors
			});
		}
	}, {
		key: 'onEmployeeChange',
		value: function onEmployeeChange(event) {
			var errors = this.state.errors || {};
			errors.employeeId = false;

			this.state.schedule.employeeId = event.target.value;

			var employee = this.employees[event.target.value];
			this.state.schedule.employeeName = employee ? employee.fullName : '';

			this.setState({
				schedule: this.state.schedule,
				errors: errors
			});
		}
	}, {
		key: 'onSupervisorChange',
		value: function onSupervisorChange(event) {
			var errors = this.state.errors || {};
			errors.supervisorId = false;

			this.state.schedule.supervisorId = event.target.value;

			var supervisor = this.employees[event.target.value];
			this.state.schedule.supervisorName = supervisor ? supervisor.fullName : '';

			this.setState({
				schedule: this.state.schedule,
				errors: errors
			});
		}
	}, {
		key: 'onRoomChange',
		value: function onRoomChange(event) {
			var errors = this.state.errors || {};
			errors.roomId = false;

			this.state.schedule.roomId = event.target.value;

			var room = this.rooms[event.target.value];
			this.state.schedule.roomName = room ? room.name : '';

			this.setState({
				schedule: this.state.schedule,
				errors: errors
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return this.state.mode === 'view' ? this.viewRow : this.editRow;
		}
	}, {
		key: 'viewRow',
		get: function get() {
			var viewModel = this.getViewModel();

			return React.createElement(
				'tr',
				{ key: this.state.schedule.id, className: viewModel.rowClassName },
				React.createElement(
					'th',
					null,
					viewModel.date
				),
				React.createElement(
					'th',
					{ className: viewModel.employeeNameClass },
					viewModel.employeeName
				),
				React.createElement(
					'th',
					{ className: viewModel.supervisorNameClass },
					viewModel.supervisorName
				),
				React.createElement(
					'th',
					{ className: viewModel.roomNameClass },
					viewModel.roomName
				),
				React.createElement(
					'th',
					null,
					viewModel.status
				),
				React.createElement(
					'th',
					null,
					viewModel.score
				),
				React.createElement(
					'th',
					{ className: viewModel.resultClassName },
					viewModel.result
				),
				React.createElement(
					'th',
					{ width: '10%' },
					React.createElement(
						'a',
						{
							type: 'button',
							onClick: this.onEditButtonClick.bind(this),
							className: 'btn btn-link' },
						viewModel.editViewButtonLabel
					)
				)
			);
		}
	}, {
		key: 'editRow',
		get: function get() {
			var date = this.state.schedule.dueDateTime.split(' ')[0];
			var supervisorId = this.employees[this.state.schedule.supervisorId] ? this.state.schedule.supervisorId : -1;

			return React.createElement(
				'tr',
				{ className: 'newRow', key: this.state.schedule.id },
				React.createElement(
					'td',
					null,
					React.createElement('input', { type: 'date',
						value: date,
						className: this.state.errors.date ? 'error' : '',
						onChange: this.onDateChange.bind(this) })
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{
							id: 'employeeId',
							value: this.state.schedule.employeeId,
							className: this.state.errors.employeeId ? 'error' : '',
							onChange: this.onEmployeeChange.bind(this) },
						React.createElement(
							'option',
							{ value: '-1', disabled: 'disabled' },
							'--Select employee--'
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{
							id: 'supervisorId',
							value: supervisorId,
							className: this.state.errors.supervisorId ? 'error' : '',
							onChange: this.onSupervisorChange.bind(this) },
						React.createElement(
							'option',
							{ value: '-1', disabled: 'disabled' },
							'--Select supervisor--'
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					'td',
					null,
					React.createElement(
						'select',
						{ id: 'roomId',
							key: this.state.schedule.roomId,
							value: this.state.schedule.roomId,
							className: this.state.errors.roomId ? 'error' : '',
							onChange: this.onRoomChange.bind(this) },
						this.props.roomOptions
					)
				),
				React.createElement('td', null),
				React.createElement('td', null),
				React.createElement('td', null),
				React.createElement(
					'td',
					{ width: '10%' },
					React.createElement(
						'button',
						{ type: 'button', className: 'btn btn-link', onClick: this.onSaveClick.bind(this) },
						'Save'
					),
					React.createElement(
						'button',
						{ type: 'button', className: 'btn btn-link', onClick: this.onCancelClick.bind(this) },
						'Cancel'
					)
				)
			);
		}
	}]);

	return InspectionSchedule;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionSchedule;
//# sourceMappingURL=InspectionSchedule.js.map