/**
 * Inspection list row.
 */
class InspectionListRow extends React.Component {
	constructor(props) {
		super(props);
		this.state = {list: []};
	}

	render() {
		return <tbody>
		{this.props.list.map(x => {

			return <tr key={x.id}>
				<td>{x.name}</td>
				<td>
					<a href={`/admin/inspections/${x.id}`} title="Edit" className="glyphicon glyphicon-edit green"/>
				</td>
			</tr>
		})}
		</tbody>
	}
}
if (typeof module !== 'undefined') module.exports = InspectionListRow;
