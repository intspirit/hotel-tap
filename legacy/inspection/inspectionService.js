'use strict';

var inspectionService = {
	getAll: function getAll() {
		return serviceBase.get('/api/inspections');
	},

	getById: function getById(id) {
		return serviceBase.get('/api/inspections/' + id);
	},

	add: function add(name, description) {
		var data = {
			name: name,
			description: description,
			type: 2
		};
		return serviceBase.post('/api/inspections', { data: data });
	},

	save: function save(id, data) {
		return serviceBase.put('/api/inspections/' + id, { data: data });
	},

	addSchedule: function addSchedule(id, data) {
		return serviceBase.post('/api/inspections/' + id + '/schedules', { data: data });
	},

	updateSchedule: function updateSchedule(inspectionId, scheduleId, data) {
		return serviceBase.put('/api/inspections/' + inspectionId + '/schedules/' + scheduleId, { data: data });
	}
};

//# sourceMappingURL=inspectionService.js.map