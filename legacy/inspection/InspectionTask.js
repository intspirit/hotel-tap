'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var pointsMin = 1;
var pointsMax = 10;
/**
 * Inspection task component.
 */

var InspectionTask = function (_React$Component) {
	_inherits(InspectionTask, _React$Component);

	function InspectionTask(props) {
		_classCallCheck(this, InspectionTask);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionTask).call(this, props));

		_this.state = _this.props.task;
		return _this;
	}

	_createClass(InspectionTask, [{
		key: 'onChange',
		value: function onChange(event) {
			if (this.refs.points.value) {
				if (this.refs.points.value > pointsMax) this.refs.points.value = pointsMax;
				if (this.refs.points.value < pointsMin) this.refs.points.value = pointsMin;
			}

			var changes = {
				subject: this.refs.subject.value,
				points: this.refs.points.value,
				failAll: this.refs.failAll.checked || false
			};
			if (this.state.changeStatus !== 'i') {
				changes.changeStatus = 'u';
				this.props.onTaskUpdated(this.state.id, changes);
			}

			//noinspection JSUnresolvedVariable
			this.setState(changes);
		}
	}, {
		key: 'onDelete',
		value: function onDelete() {
			var isNew = this.state.changeStatus === 'i';
			this.state.changeStatus = 'd';
			this.props.onTaskDeleted(this.state.id, isNew);
		}
	}, {
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			this.setState(nextProps.task);
		}
	}, {
		key: 'render',
		value: function render() {
			if (this.state.changeStatus === 'd') return null;

			var checked = this.state.failAll == 1 ? 'checked' : null;

			return React.createElement(
				'tr',
				{ key: this.state.id },
				React.createElement(
					'th',
					null,
					React.createElement('input', {
						ref: 'subject',
						type: 'text',
						className: 'transparentInput',
						value: this.state.subject,
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					{ className: 'center' },
					React.createElement('input', {
						ref: 'points',
						type: 'number',
						className: 'transparentInput',
						value: this.state.points,
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					{ className: 'center' },
					React.createElement('input', {
						ref: 'failAll',
						type: 'checkbox',
						checked: checked,
						value: 'failAll',
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					{ className: 'actionIcons' },
					React.createElement('span', { id: '1',
						className: 'glyphicon glyphicon-trash',
						title: 'Delete',
						onClick: this.onDelete.bind(this) })
				)
			);
		}
	}]);

	return InspectionTask;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionTask;

//# sourceMappingURL=InspectionTask.js.map