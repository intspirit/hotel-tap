"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Inspection list row.
 */

var InspectionListRow = function (_React$Component) {
	_inherits(InspectionListRow, _React$Component);

	function InspectionListRow(props) {
		_classCallCheck(this, InspectionListRow);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionListRow).call(this, props));

		_this.state = { list: [] };
		return _this;
	}

	_createClass(InspectionListRow, [{
		key: "render",
		value: function render() {
			return React.createElement(
				"tbody",
				null,
				this.props.list.map(function (x) {

					return React.createElement(
						"tr",
						{ key: x.id },
						React.createElement(
							"td",
							null,
							x.name
						),
						React.createElement(
							"td",
							null,
							React.createElement("a", { href: "/admin/inspections/" + x.id, title: "Edit", className: "glyphicon glyphicon-edit green" })
						)
					);
				})
			);
		}
	}]);

	return InspectionListRow;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionListRow;

//# sourceMappingURL=InspectionListRow.js.map