'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Inspection list view.
 */

var InspectionList = function (_React$Component) {
	_inherits(InspectionList, _React$Component);

	function InspectionList(props) {
		_classCallCheck(this, InspectionList);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionList).call(this, props));

		_this.state = {
			list: []
		};
		return _this;
	}

	_createClass(InspectionList, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var _this2 = this;

			inspectionService.getAll().then(function (data) {
				_this2.setState({ list: data.logs });
			}).catch(function () {
				_this2.setState({ list: [] });
				alert('Failed to get inspections');
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return React.createElement(
				'aside',
				{ className: 'right-side' },
				React.createElement(AdminViewHeader, {
					url: '/admin/inspections/create',
					buttonLabel: 'Add Inspection',
					header: 'Inspections' }),
				React.createElement(
					'section',
					{ className: 'content' },
					React.createElement(
						'table',
						{ className: 'table table-bordered' },
						React.createElement(
							'thead',
							null,
							React.createElement(
								'tr',
								{ className: 'tblheading' },
								React.createElement(
									'th',
									null,
									'Inspection Name'
								),
								React.createElement(
									'th',
									{ width: '10%' },
									'Options'
								)
							)
						),
						React.createElement(InspectionListRow, { list: this.state.list })
					)
				)
			);
		}
	}]);

	return InspectionList;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionList;
ReactDOM.render(React.createElement(InspectionList, null), document.getElementById('view'));

//# sourceMappingURL=InspectionList.js.map