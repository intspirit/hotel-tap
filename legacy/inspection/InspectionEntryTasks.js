"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Grid = ReactBootstrap.Grid,
    FormControls = ReactBootstrap.FormControls,
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    ButtonGroup = ReactBootstrap.ButtonGroup,
    Button = ReactBootstrap.Button,
    Table = ReactBootstrap.Table;

/**
 * Inspection entry - total component.
 */
var InspectionTotal = function InspectionTotal(props) {
	return React.createElement(
		"tr",
		null,
		React.createElement(
			"td",
			null,
			"TOTAL: ",
			React.createElement(
				"span",
				{ className: "hidden-md hidden-lg" },
				props.roomLog.totalPoints
			),
			React.createElement(
				"span",
				{ className: "hidden-md hidden-lg pull-right" },
				"RESULT: ",
				props.scorePoints
			)
		),
		React.createElement(
			"td",
			{ className: "hidden-xs hidden-sm" },
			props.roomLog.totalPoints
		),
		React.createElement("td", { className: "hidden-xs hidden-sm" }),
		React.createElement(
			"td",
			{ className: "hidden-xs hidden-sm" },
			props.scorePoints
		),
		React.createElement("td", { className: "hidden-xs hidden-sm" })
	);
};

/**
 * Inspection entry - result component. 
 */
var InspectionResult = function InspectionResult(props) {
	var resultClass = props.result == 'Pass' ? "text-green" : "text-red";
	return React.createElement(
		"tr",
		null,
		React.createElement(
			"td",
			null,
			React.createElement(
				"span",
				{ className: "hidden-xs hidden-sm" },
				"INSPECTION SCORE/RESULT:"
			),
			React.createElement(
				"span",
				{ className: "hidden-md hidden-lg" },
				"SCORE: ",
				props.roomLog.passingScorePercentage,
				" %"
			),
			React.createElement(
				"span",
				{ className: "hidden-md hidden-lg pull-right" },
				"RESULT: ",
				props.scorePercentage,
				" %"
			),
			React.createElement(
				"div",
				{ className: "hidden-md hidden-lg" },
				"INSPECTION RESULT: ",
				React.createElement(
					"span",
					{ className: resultClass },
					props.result
				)
			)
		),
		React.createElement("td", { className: "hidden-xs hidden-sm" }),
		React.createElement(
			"td",
			{ className: "hidden-xs hidden-sm" },
			props.roomLog.passingScorePercentage,
			" %"
		),
		React.createElement(
			"td",
			{ className: "hidden-xs hidden-sm" },
			props.scorePercentage,
			" %"
		),
		React.createElement(
			"td",
			{ className: "hidden-xs hidden-sm" },
			React.createElement(
				"span",
				{ className: resultClass },
				props.result
			)
		)
	);
};

/**
 * Inspection entry - tasks component.
 */

var InspectionEntryTasks = function (_React$Component) {
	_inherits(InspectionEntryTasks, _React$Component);

	function InspectionEntryTasks(props) {
		_classCallCheck(this, InspectionEntryTasks);

		return _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionEntryTasks).call(this, props));
	}

	_createClass(InspectionEntryTasks, [{
		key: "render",
		value: function render() {
			var _this2 = this;

			return React.createElement(
				Row,
				{ className: "show-grid", style: { marginTop: '15px' } },
				React.createElement(
					Col,
					{ xs: 12, md: 12 },
					React.createElement(
						Table,
						{ striped: true, bordered: true, condensed: true, hover: true, id: "inspectionTable" },
						React.createElement(
							"thead",
							null,
							React.createElement(
								"tr",
								null,
								React.createElement(
									"th",
									null,
									"Task"
								),
								React.createElement(
									"th",
									{ className: "hidden-xs hidden-sm" },
									"Points"
								),
								React.createElement(
									"th",
									{ style: { width: '350px' }, className: "hidden-xs hidden-sm" },
									"Status"
								),
								React.createElement(
									"th",
									{ className: "hidden-xs hidden-sm" },
									"Result"
								),
								React.createElement(
									"th",
									{ style: { width: '55px' }, className: "hidden-xs hidden-sm" },
									"Fail All"
								)
							)
						),
						React.createElement(
							"tbody",
							null,
							this.props.tasks.map(function (x) {
								return React.createElement(InspectionEntryTask, {
									key: x.id,
									task: x,
									taskScoreChanged: _this2.props.taskScoreChanged,
									taskCommentAdded: _this2.props.taskCommentAdded
								});
							})
						),
						React.createElement(
							"tfoot",
							null,
							React.createElement(InspectionTotal, {
								roomLog: this.props.roomLog,
								scorePoints: this.props.scorePoints,
								scorePercentage: this.props.scorePercentage }),
							React.createElement(InspectionResult, {
								roomLog: this.props.roomLog,
								scorePoints: this.props.scorePoints,
								result: this.props.result,
								scorePercentage: this.props.scorePercentage })
						)
					)
				)
			);
		}
	}]);

	return InspectionEntryTasks;
}(React.Component);

//# sourceMappingURL=InspectionEntryTasks.js.map