const
	nameDescriptionRef = 'nameDescription',
	NameDescription = HotelTap.NameDescription;

/**
 * Inspection new entry.
 */
class InspectionNew extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	saveButtonClicked() {
		const nameDescriptionModel = this.refs[nameDescriptionRef].state;
		if (nameDescriptionModel.name.trim().length == 0) return;

		inspectionService.add(nameDescriptionModel.name, nameDescriptionModel.description).
		then(data => {
			const id = data.id;
			window.location = "/admin/inspections/" + id;
		}).catch(() => {
			alert('Failed to add inspection.')
		});
	}

	render() {
		return (
			<aside className="right-side">
				<AdminViewHeader
					url='/admin/inspections'
					buttonLabel="Back to Inspections"
					header="Add Inspection"/>
				<section className="content">
					<div className="whitecentercol">
						<NameDescription ref={nameDescriptionRef} model={{nameHint: 'Inspection name'}}/>

						<div className="form-group">
							<a href='#' className="btn btn-primary" onClick={this.saveButtonClicked.bind(this)}>Save</a>
						</div>
					</div>
				</section>
			</aside>
		);
	}
}

if (typeof module !== 'undefined') module.exports = InspectionNew;
ReactDOM.render(<InspectionNew />, document.getElementById('view'));
