const pointsMin = 1;
const pointsMax = 10;
/**
 * New inspection task component.
 */
class InspectionNewTask extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.newId,
			subject: '',
			points: pointsMin,
			failAll: false,
			pointsError: false,
			subjectError: false,
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({id: nextProps.newId});
	}

	onChange() {
		//noinspection JSUnresolvedVariable
		if (this.refs.points.value) {
			if (this.refs.points.value > pointsMax) this.refs.points.value = pointsMax;
			if (this.refs.points.value < pointsMin) this.refs.points.value = pointsMin;
		}

		this.setState({
			subject: this.refs.subject.value,
			points: this.refs.points.value,
			failAll: this.refs.failAll.checked || false,
			pointsError: false,
			subjectError: false
		});
	}

	save() {
		//TODO: vvs p1 validation
		let errors = false;
		if (!this.refs.subject.value.trim()) {
			errors = true;
			this.setState({subjectError: true});
		}

		let availablePoints = _.range(pointsMin, pointsMax + 1);
		if (availablePoints.indexOf(parseInt(this.refs.points.value)) == -1) {
			errors = true;
			this.setState({pointsError: true});
		}

		if (errors) {
			return;
		}
		//noinspection JSUnresolvedVariable
		this.props.onTaskAdded({
			id: this.state.id,
			subject: this.refs.subject.value,
			points: parseInt(this.refs.points.value),
			failAll: this.refs.failAll.checked || false,
			changeStatus: 'i'
		});
		this.setState({
			subject: '',
			points: pointsMin,
			failAll: false
		});
	}

	render() {
		const checked = this.state.failAll ? 'checked' : null;

		return <tr key={this.state.id} className="newRow">
			<th>
				<input
					id="newTaskSubject"
					ref="subject"
					type="text"
					value={this.state.subject}
					className={this.state.subjectError ? 'error' : ''}
					onChange={this.onChange.bind(this)}/>
			</th>
			<th className="center">
				<input
					ref="points"
					type="number"
					min={pointsMin}
					max={pointsMax}
					className={'inspection-task-points ' + (this.state.pointsError ? 'error' : '')}
					value={this.state.points}
					onChange={this.onChange.bind(this)}/>
			</th>
			<th className="center">
				<input
					ref="failAll"
					type="checkbox"
					checked={checked}
					value="failAll"
					onChange={this.onChange.bind(this)}/>
			</th>
			<th>
				<button type="button" className="btn btn-link" onClick={this.save.bind(this)}>Add</button>
			</th>
		</tr>;
	}
}
if (typeof module !== 'undefined') module.exports = InspectionNewTask;
