const
	checklistTaskService = checklistTaskService || require("checklistTaskService"),
	inspectionService = inspectionService || require("inspectionService");

/**
 * Inspection tasks component.
 */
class InspectionTasks extends React.Component {
	constructor(props) {
		super(props);
		const points = InspectionTasks.calcPoints(this.props.tasks, this.props.passingScore);
		this.state = {
			tasks: this.props.tasks || [],
			totalPoints: points.totalPoints,
			passingScore: this.props.passingScore || 90,
			passingPoints: points.passingPoints
		};
		this.newId = -1;
	}

	static calcPoints(tasks, passingScore) {
		if (!tasks) return [0, 0];

		const totalPoints = _.sumBy(tasks, x => x.changeStatus === 'd' ? 0 : parseInt(x.points));
		const passingPoints = totalPoints * passingScore / 100;
		return {totalPoints: totalPoints, passingPoints: passingPoints};
	}

	onPassingScoreChange(event) {
		this.setState({
			passingPoints: this.state.totalPoints * event.target.value / 100,
			passingScore: event.target.value
		});
	}

	onPassingScoreBlur() {
		const points = InspectionTasks.calcPoints(this.state.tasks, this.state.passingScore);
		this.props.onScoreChange(points.totalPoints, points.passingPoints, this.state.passingScore);
	}

	onTaskAdded(task) {
		this.state.tasks.push(task);
		this.newId --;
		this.updateState();
	}

	onTaskDeleted(taskId, isNew) {
		if (isNew) {
			_.remove(this.state.tasks, x => {
				return x.id === taskId;
			});
		} else {
			let index = _.findIndex(this.state.tasks, x => x.id === taskId);
			this.state.tasks[index].changeStatus = 'd';
		}
		this.updateState();
	}

	onTaskUpdated(taskId, changes) {
		let index = _.findIndex(this.state.tasks, x => x.id === taskId);
		this.state.tasks[index] = Object.assign(this.state.tasks[index], changes);
		this.state.tasks[index].state = 'u';
		this.updateState();
	}

	updateState() {
		const points = InspectionTasks.calcPoints(this.state.tasks, this.state.passingScore);
		this.props.onScoreChange(points.totalPoints, points.passingPoints, this.state.passingScore);
		this.setState({
			tasks: this.state.tasks,
			totalPoints: points.totalPoints,
			passingPoints: points.passingPoints
		});
	}

	componentWillReceiveProps(nextProps) {
		this.setState({tasks: nextProps.tasks});
	}

	render() {
		const taskRows = this.state.tasks.map(x => <InspectionTask task={x}
						key={x.id}
						onTaskDeleted={this.onTaskDeleted.bind(this)}
						onTaskUpdated={this.onTaskUpdated.bind(this)}/>
		);

		const total = <tr key='totalTaskScoreRow' className="newRow">
			<th>
				<span className="pull-right">TOTAL:</span>
			</th>
			<th colSpan="3">
				<span>{this.state.totalPoints}</span>
			</th>
		</tr>;
		const passingScore = <tr key='apssingScoreRow' className="newRow">
			<th>
				<span className="pull-right">PASSING SCORE:</span>
			</th>
			<th>
				<input
					type="number"
					min="10"
					max="100"
					step="5"
					onBlur={this.onPassingScoreBlur.bind(this)}
					onChange={this.onPassingScoreChange.bind(this)}
					value={this.state.passingScore}/>&nbsp;%
			</th>
			<th colSpan="2">
				<span>{this.state.passingPoints}</span>&nbsp;points
			</th>
		</tr>;

		return <div className="form-group">
			<h1>Tasks</h1>
			<table className="table table-bordered" id="inspectionTaskTable">
				<thead>
				<tr className="tblheading">
					<th>Task</th>
					<th width="10%">Points (1-10)</th>
					<th width="10%">Fail All</th>
					<th width="10%">Options</th>
				</tr>
				</thead>
				<tbody>
				{taskRows}
				<InspectionNewTask onTaskAdded={this.onTaskAdded.bind(this)} newId={this.newId}/>
				{total}
				{passingScore}
				</tbody>
			</table>
		</div>
	}
}
if (typeof module !== 'undefined') module.exports = InspectionTasks;
