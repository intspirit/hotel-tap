const
	checklistTaskService = checklistTaskService || require("checklistTaskService"),
	inspectionService = inspectionService || require("inspectionService"),
	nameDescriptionRef = 'nameDescription',
	NameDescription = HotelTap.NameDescription;

var d = false;
/**
 * Error message.
 * @param props
 * @constructor
 */
const ErrorMessage = (props = {showErrors: false, errorMessages: []}) =>
	<div className={`alert alert-danger ${props.showErrors ? '' : 'hidden'}`} role="alert">
		<ul>
			{props.errorMessages.map(x => <li key={x}>{x}</li>)}
		</ul>
	</div>;
ErrorMessage.propTypes = {props: React.PropTypes.object};

/**
 * Inspection edit component.
 */
class InspectionEdit extends React.Component {
	/**
	 * @param {{schedules:string}} this
	 */
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			description: '',
			showErrors: false,
			errorMessages: []
		};
		this.gotData = false;
		this.id = -1;
		this.allEmployees = [];
		this.activeEmployees = [];
		this.rooms = [];
		this.schedules = [];
	}

	componentDidMount() {
		const urlParts = window.location.pathname.split('/');
		this.id = urlParts[urlParts.length - 1];
		//TODO vvs p1 why it doesn't work?
		let that = this;
		const taskState = {changeStatus: ''};
		inspectionService.getById(this.id).then(data => {
			that.gotData = true;
			that.tasks = data.tasks.map(x => _.assign(x, taskState));
			that.allEmployees = data.employees;
			that.activeEmployees = _.filter(data.employees, x => {return x.status !== 'deleted'});
			that.rooms = data.rooms.sort(naturalSort({key: 'name'}));
			that.schedules = data.schedule;
			that.setState({
				name: data.name,
				description: data.description,
				totalPoints: data.totalPoints,
				passingScorePercentage: data.passingScorePercentage,
				passingScorePoints: data.passingScorePoints
			});
		}).catch(() => {
			alert('Failed to get instpection data');
			window.location = "/admin/inspections/";
		});
	}

	onNameDescriptionChange(name, description) {
		if (this.state.name === name && this.state.description === description) return;
		this.setState({
			name: name,
			description: description,
			showErrors: false,
			errorMessages: []
		});
	}

	onScoreChange(totalPoints, passingPoints, passingPercentage) {
		this.state.passingScorePercentage = passingPercentage;
		this.state.totalPoints = totalPoints;
		this.state.passingScorePoints = passingPoints;
	}

	isValid() {
		let result = true;
		let messages = [];
		if (this.refs.tasks.state.tasks.length === 0) {
			result = false;
			messages.push('Please add at least one task.');
		}
		if (this.state.passingScorePercentage < 10 || this.state.passingScorePercentage > 100) {
			result = false;
			messages.push('Passing score % should be between 10 and 100.');
		}

		if (result) return true;

		this.setState({
			showErrors: true,
			errorMessages: messages
		});
	}

	onSave(event) {
		event.preventDefault();

		if (this.state.name.trim() === '') return;
		if (!this.isValid()) return;

		var request = this.getSaveRequest();
		$.ajax({
			url: '/api/roomlogs/',
			dataType: 'json',
			type: 'POST',
			data: request,
			success: function () {
			}.bind(this),
			error: function (xhr, status, err) {
				console.error(status, err.toString());
			}.bind(this)
		});
	}

	getSaveRequest() {
		let result = {
			id: this.id,
			name: this.state.name,
			description: this.state.description,
			type: 2,
			assignmentType: null,
			assigneeId: null,
			totalPoints: this.state.totalPoints,
			passingScorePercentage: this.state.passingScorePercentage,
			passingScorePoints: this.state.passingScorePoints,
			tasks: [],
			schedules: []
		};

		let order = 0;
		this.refs.tasks.state.tasks.forEach(x => {
			if (!x.changeStatus || x.changeStatus === '') return;

			var task = {
				id: x.changeStatus === 'i' ? -1 : x.id,
				subject: x.subject,
				description: '',
				order: order,
				points: x.points,
				failAll: x.failAll ? 1 : 0,
				changeStatus: x.changeStatus
			};
			result.tasks.push(task);
			order += 1;
			if (x.changeStatus === 'i' || x.changeStatus === 'u') x.changeStatus = '';
		});

		return result;
	}

	render() {
		if (!this.gotData) return null;

		return (
			<aside className="right-side">
				<AdminViewHeader
					url='/admin/inspections'
					showNavigation={false}
					buttonLabel="Back to Inspections"
					buttonLabelSmall="Back"
					header="Inspection Template"/>
				<section className="content">
					<div className="whitecentercol">
						<NameDescription
							ref={nameDescriptionRef}
							onChange={this.onNameDescriptionChange.bind(this)}
							model={{
								name: this.state.name,
								description: this.state.description
							}}/>
						<InspectionTasks
							ref="tasks"
							inspectionId={this.id}
							tasks={this.tasks}
							passingScore={this.state.passingScorePercentage}
							onScoreChange={this.onScoreChange.bind(this)}/>
						<ErrorMessage showErrors={this.state.showErrors} errorMessages={this.state.errorMessages}/>
						<div>
							<a href='#' className="btn btn-primary" onClick={this.onSave.bind(this)}>Save Template</a>
						</div>
						<InspectionSchedules
							allEmployees={this.allEmployees}
							activeEmployees={this.activeEmployees}
							inspectionId={this.id}
							schedules={this.schedules}
							rooms={this.rooms}
						/>
					</div>
				</section>
			</aside>
		);
	}
}
if (typeof module !== 'undefined') module.exports = InspectionEdit;
ReactDOM.render(<InspectionEdit />, document.getElementById('view'));
