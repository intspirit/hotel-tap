"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Inspection new schedule component.
 */

var InspectionScheduleFilter = function (_React$Component) {
	_inherits(InspectionScheduleFilter, _React$Component);

	function InspectionScheduleFilter(props) {
		_classCallCheck(this, InspectionScheduleFilter);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionScheduleFilter).call(this, props));

		_this.years = ["2015", "2016", "2017"];
		_this.months = _.range(1, 13);

		_this.state = {
			filters: InspectionScheduleFilter.getDefaultFilters(),
			isVisible: props.isVisible
		};
		_this.onFilterChange();
		return _this;
	}

	_createClass(InspectionScheduleFilter, [{
		key: "componentWillReceiveProps",
		value: function componentWillReceiveProps(nextProps) {
			this.setState({
				isVisible: nextProps.isVisible
			});
		}
	}, {
		key: "onYearChange",
		value: function onYearChange(event) {
			this.state.filters.dueYear = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onMonthChange",
		value: function onMonthChange(event) {
			this.state.filters.dueMonth = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onEmployeeChange",
		value: function onEmployeeChange(event) {
			this.state.filters.employeeId = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onSupervisorChange",
		value: function onSupervisorChange(event) {
			this.state.filters.supervisorId = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onRoomChange",
		value: function onRoomChange(event) {
			this.state.filters.roomId = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onStatusChange",
		value: function onStatusChange(event) {
			this.state.filters.status = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onResultChange",
		value: function onResultChange(event) {
			this.state.filters.inspectionPassed = event.target.value;
			this.onFilterChange();
		}
	}, {
		key: "onResetClick",
		value: function onResetClick() {
			this.state.filters = InspectionScheduleFilter.getDefaultFilters();
			this.onFilterChange();
		}
	}, {
		key: "onFilterChange",
		value: function onFilterChange() {
			this.setState({
				filters: this.state.filters
			});

			var filters = {};
			_.forEach(this.state.filters, function (x, i) {
				if (x != -1) filters[i] = x;
			});
			this.props.onFilterChanged(filters);
		}
	}, {
		key: "render",
		value: function render() {
			if (!this.state.isVisible) return null;

			return React.createElement(
				"tr",
				{ className: "filterRow", key: this.state.id },
				React.createElement(
					"td",
					null,
					React.createElement(
						"span",
						{ className: "filter-year-wrapper" },
						React.createElement(
							"select",
							{ id: "filterYear",
								value: this.state.filters.dueYear,
								onChange: this.onYearChange.bind(this) },
							this.years.map(function (x) {
								return React.createElement(
									"option",
									{ value: x },
									x
								);
							})
						)
					),
					React.createElement(
						"span",
						{ className: "filter-month-wrapper" },
						React.createElement(
							"select",
							{ id: "filterMonth",
								value: this.state.filters.dueMonth,
								onChange: this.onMonthChange.bind(this) },
							this.months.map(function (x) {
								return React.createElement(
									"option",
									{ value: x },
									moment().month(x - 1).format('MMMM')
								);
							})
						)
					)
				),
				React.createElement(
					"td",
					null,
					React.createElement(
						"select",
						{ id: "filterEmployeeId",
							value: this.state.filters.employeeId,
							onChange: this.onEmployeeChange.bind(this) },
						React.createElement(
							"option",
							{ value: "-1" },
							"--All employees--"
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					"td",
					null,
					React.createElement(
						"select",
						{ id: "filterSupervisorId",
							value: this.state.filters.supervisorId,
							onChange: this.onSupervisorChange.bind(this) },
						React.createElement(
							"option",
							{ value: "-1" },
							"--All supervisors--"
						),
						this.props.employeeOptions
					)
				),
				React.createElement(
					"td",
					null,
					React.createElement(
						"select",
						{ id: "filterRoomId",
							value: this.state.filters.roomId,
							onChange: this.onRoomChange.bind(this) },
						React.createElement(
							"option",
							{ value: "-1" },
							"--All locations--"
						),
						this.props.roomOptions
					)
				),
				React.createElement(
					"td",
					null,
					React.createElement(
						"select",
						{ id: "filterStatus",
							value: this.state.filters.status,
							onChange: this.onStatusChange.bind(this) },
						React.createElement(
							"option",
							{ value: "-1" },
							"--All statuses--"
						),
						React.createElement(
							"option",
							{ value: "assigned" },
							"Assigned"
						),
						React.createElement(
							"option",
							{ value: "completed" },
							"Completed"
						)
					)
				),
				React.createElement("td", null),
				React.createElement(
					"td",
					null,
					React.createElement(
						"select",
						{ id: "filterResult",
							value: this.state.filters.inspectionPassed,
							onChange: this.onResultChange.bind(this) },
						React.createElement(
							"option",
							{ value: "-1" },
							"--All results--"
						),
						React.createElement(
							"option",
							{ value: "1" },
							"Passed"
						),
						React.createElement(
							"option",
							{ value: "0" },
							"Failed"
						)
					)
				),
				React.createElement(
					"td",
					{ width: "10%" },
					React.createElement(
						"button",
						{ type: "button", className: "btn btn-sm", onClick: this.onResetClick.bind(this) },
						"Clear Filter"
					)
				)
			);
		}
	}], [{
		key: "getDefaultFilters",
		value: function getDefaultFilters() {
			return {
				dueYear: moment().format('Y'),
				dueMonth: moment().format('M'),
				employeeId: -1,
				supervisorId: -1,
				roomId: -1,
				status: -1,
				inspectionPassed: -1
			};
		}
	}]);

	return InspectionScheduleFilter;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionScheduleFilter;
//# sourceMappingURL=InspectionScheduleFilter.js.map