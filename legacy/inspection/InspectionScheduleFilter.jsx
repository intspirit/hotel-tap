/**
 * Inspection new schedule component.
 */
class InspectionScheduleFilter extends React.Component {
	constructor(props) {
		super(props);

		this.years = ["2015", "2016", "2017"];
		this.months = _.range(1,13);

		this.state = {
			filters: InspectionScheduleFilter.getDefaultFilters(),
			isVisible: props.isVisible
		}
		this.onFilterChange();
	}

	static getDefaultFilters() {
		return {
			dueYear: moment().format('Y'),
			dueMonth: moment().format('M'),
			employeeId: -1,
			supervisorId: -1,
			roomId: -1,
			status: -1,
			inspectionPassed: -1
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			isVisible: nextProps.isVisible
		});
	}

	onYearChange(event) {
		this.state.filters.dueYear = event.target.value;
		this.onFilterChange();
	}

	onMonthChange(event) {
		this.state.filters.dueMonth = event.target.value;
		this.onFilterChange();
	}

	onEmployeeChange(event) {
		this.state.filters.employeeId = event.target.value;
		this.onFilterChange();
	}

	onSupervisorChange(event) {
		this.state.filters.supervisorId = event.target.value;
		this.onFilterChange();
	}

	onRoomChange(event) {
		this.state.filters.roomId = event.target.value;
		this.onFilterChange();
	}

	onStatusChange(event) {
		this.state.filters.status = event.target.value;
		this.onFilterChange();
	}

	onResultChange(event) {
		this.state.filters.inspectionPassed = event.target.value;
		this.onFilterChange();
	}

	onResetClick() {
		this.state.filters = InspectionScheduleFilter.getDefaultFilters();
		this.onFilterChange();
	}

	onFilterChange() {
		this.setState({
			filters: this.state.filters
		});

		const filters = {};
		_.forEach(this.state.filters, (x,i) => {
			if (x != -1) filters[i] = x;
		});
		this.props.onFilterChanged(filters);
	}

	render() {
		if (!this.state.isVisible) return null;

		return <tr className="filterRow" key={this.state.id}>
			<td>
				<span className="filter-year-wrapper">
					<select id="filterYear"
							value={this.state.filters.dueYear}
							onChange={this.onYearChange.bind(this)}>
						{this.years.map(x => {
							return <option value={x}>{x}</option>
						})}
					</select>
				</span>
				<span className="filter-month-wrapper">
					<select id="filterMonth"
							value={this.state.filters.dueMonth}
							onChange={this.onMonthChange.bind(this)}>
						{this.months.map(x => {
							return <option value={x}>{moment().month(x - 1).format('MMMM')}</option>
						})}
					</select>
				</span>
			</td>
			<td>
				<select id="filterEmployeeId"
						value={this.state.filters.employeeId}
						onChange={this.onEmployeeChange.bind(this)}>
					<option value="-1" >--All employees--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select id="filterSupervisorId"
						value={this.state.filters.supervisorId}
						onChange={this.onSupervisorChange.bind(this)}>
					<option value="-1">--All supervisors--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select id="filterRoomId"
						value={this.state.filters.roomId}
						onChange={this.onRoomChange.bind(this)}>
					<option value="-1">--All locations--</option>
					{this.props.roomOptions}
				</select>
			</td>
			<td>
				<select id="filterStatus"
						value={this.state.filters.status}
						onChange={this.onStatusChange.bind(this)}>
					<option value="-1">--All statuses--</option>
					<option value="assigned">Assigned</option>
					<option value="completed">Completed</option>
				</select>
			</td>
			<td/>
			<td>
				<select id="filterResult"
						value={this.state.filters.inspectionPassed}
						onChange={this.onResultChange.bind(this)}>
					<option value="-1">--All results--</option>
					<option value="1">Passed</option>
					<option value="0">Failed</option>
				</select>
			</td>
			<td width="10%">
				<button type="button" className="btn btn-sm" onClick={this.onResetClick.bind(this)}>Clear Filter</button>
			</td>
		</tr>
	}
}
if (typeof module !== 'undefined') module.exports = InspectionScheduleFilter;
