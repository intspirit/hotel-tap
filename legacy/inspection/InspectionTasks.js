"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var checklistTaskService = checklistTaskService || require("checklistTaskService"),
    inspectionService = inspectionService || require("inspectionService");

/**
 * Inspection tasks component.
 */

var InspectionTasks = function (_React$Component) {
	_inherits(InspectionTasks, _React$Component);

	function InspectionTasks(props) {
		_classCallCheck(this, InspectionTasks);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionTasks).call(this, props));

		var points = InspectionTasks.calcPoints(_this.props.tasks, _this.props.passingScore);
		_this.state = {
			tasks: _this.props.tasks || [],
			totalPoints: points.totalPoints,
			passingScore: _this.props.passingScore || 90,
			passingPoints: points.passingPoints
		};
		_this.newId = -1;
		return _this;
	}

	_createClass(InspectionTasks, [{
		key: "onPassingScoreChange",
		value: function onPassingScoreChange(event) {
			this.setState({
				passingPoints: this.state.totalPoints * event.target.value / 100,
				passingScore: event.target.value
			});
		}
	}, {
		key: "onPassingScoreBlur",
		value: function onPassingScoreBlur() {
			var points = InspectionTasks.calcPoints(this.state.tasks, this.state.passingScore);
			this.props.onScoreChange(points.totalPoints, points.passingPoints, this.state.passingScore);
		}
	}, {
		key: "onTaskAdded",
		value: function onTaskAdded(task) {
			this.state.tasks.push(task);
			this.newId--;
			this.updateState();
		}
	}, {
		key: "onTaskDeleted",
		value: function onTaskDeleted(taskId, isNew) {
			if (isNew) {
				_.remove(this.state.tasks, function (x) {
					return x.id === taskId;
				});
			} else {
				var index = _.findIndex(this.state.tasks, function (x) {
					return x.id === taskId;
				});
				this.state.tasks[index].changeStatus = 'd';
			}
			this.updateState();
		}
	}, {
		key: "onTaskUpdated",
		value: function onTaskUpdated(taskId, changes) {
			var index = _.findIndex(this.state.tasks, function (x) {
				return x.id === taskId;
			});
			this.state.tasks[index] = Object.assign(this.state.tasks[index], changes);
			this.state.tasks[index].state = 'u';
			this.updateState();
		}
	}, {
		key: "updateState",
		value: function updateState() {
			var points = InspectionTasks.calcPoints(this.state.tasks, this.state.passingScore);
			this.props.onScoreChange(points.totalPoints, points.passingPoints, this.state.passingScore);
			this.setState({
				tasks: this.state.tasks,
				totalPoints: points.totalPoints,
				passingPoints: points.passingPoints
			});
		}
	}, {
		key: "componentWillReceiveProps",
		value: function componentWillReceiveProps(nextProps) {
			this.setState({ tasks: nextProps.tasks });
		}
	}, {
		key: "render",
		value: function render() {
			var _this2 = this;

			var taskRows = this.state.tasks.map(function (x) {
				return React.createElement(InspectionTask, { task: x,
					key: x.id,
					onTaskDeleted: _this2.onTaskDeleted.bind(_this2),
					onTaskUpdated: _this2.onTaskUpdated.bind(_this2) });
			});

			var total = React.createElement(
				"tr",
				{ key: "totalTaskScoreRow", className: "newRow" },
				React.createElement(
					"th",
					null,
					React.createElement(
						"span",
						{ className: "pull-right" },
						"TOTAL:"
					)
				),
				React.createElement(
					"th",
					{ colSpan: "3" },
					React.createElement(
						"span",
						null,
						this.state.totalPoints
					)
				)
			);
			var passingScore = React.createElement(
				"tr",
				{ key: "apssingScoreRow", className: "newRow" },
				React.createElement(
					"th",
					null,
					React.createElement(
						"span",
						{ className: "pull-right" },
						"PASSING SCORE:"
					)
				),
				React.createElement(
					"th",
					null,
					React.createElement("input", {
						type: "number",
						min: "10",
						max: "100",
						step: "5",
						onBlur: this.onPassingScoreBlur.bind(this),
						onChange: this.onPassingScoreChange.bind(this),
						value: this.state.passingScore }),
					" %"
				),
				React.createElement(
					"th",
					{ colSpan: "2" },
					React.createElement(
						"span",
						null,
						this.state.passingPoints
					),
					" points"
				)
			);

			return React.createElement(
				"div",
				{ className: "form-group" },
				React.createElement(
					"h1",
					null,
					"Tasks"
				),
				React.createElement(
					"table",
					{ className: "table table-bordered", id: "inspectionTaskTable" },
					React.createElement(
						"thead",
						null,
						React.createElement(
							"tr",
							{ className: "tblheading" },
							React.createElement(
								"th",
								null,
								"Task"
							),
							React.createElement(
								"th",
								{ width: "10%" },
								"Points (1-10)"
							),
							React.createElement(
								"th",
								{ width: "10%" },
								"Fail All"
							),
							React.createElement(
								"th",
								{ width: "10%" },
								"Options"
							)
						)
					),
					React.createElement(
						"tbody",
						null,
						taskRows,
						React.createElement(InspectionNewTask, { onTaskAdded: this.onTaskAdded.bind(this), newId: this.newId }),
						total,
						passingScore
					)
				)
			);
		}
	}], [{
		key: "calcPoints",
		value: function calcPoints(tasks, passingScore) {
			if (!tasks) return [0, 0];

			var totalPoints = _.sumBy(tasks, function (x) {
				return x.changeStatus === 'd' ? 0 : parseInt(x.points);
			});
			var passingPoints = totalPoints * passingScore / 100;
			return { totalPoints: totalPoints, passingPoints: passingPoints };
		}
	}]);

	return InspectionTasks;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionTasks;
//# sourceMappingURL=InspectionTasks.js.map