"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var checklistTaskService = checklistTaskService || require("checklistTaskService"),
    inspectionService = inspectionService || require("inspectionService"),
    nameDescriptionRef = 'nameDescription',
    NameDescription = HotelTap.NameDescription;

var d = false;
/**
 * Error message.
 * @param props
 * @constructor
 */
var ErrorMessage = function ErrorMessage() {
	var props = arguments.length <= 0 || arguments[0] === undefined ? { showErrors: false, errorMessages: [] } : arguments[0];
	return React.createElement(
		"div",
		{ className: "alert alert-danger " + (props.showErrors ? '' : 'hidden'), role: "alert" },
		React.createElement(
			"ul",
			null,
			props.errorMessages.map(function (x) {
				return React.createElement(
					"li",
					{ key: x },
					x
				);
			})
		)
	);
};
ErrorMessage.propTypes = { props: React.PropTypes.object };

/**
 * Inspection edit component.
 */

var InspectionEdit = function (_React$Component) {
	_inherits(InspectionEdit, _React$Component);

	/**
  * @param {{schedules:string}} this
  */

	function InspectionEdit(props) {
		_classCallCheck(this, InspectionEdit);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionEdit).call(this, props));

		_this.state = {
			name: '',
			description: '',
			showErrors: false,
			errorMessages: []
		};
		_this.gotData = false;
		_this.id = -1;
		_this.allEmployees = [];
		_this.activeEmployees = [];
		_this.rooms = [];
		_this.schedules = [];
		return _this;
	}

	_createClass(InspectionEdit, [{
		key: "componentDidMount",
		value: function componentDidMount() {
			var urlParts = window.location.pathname.split('/');
			this.id = urlParts[urlParts.length - 1];
			//TODO vvs p1 why it doesn't work?
			var that = this;
			var taskState = { changeStatus: '' };
			inspectionService.getById(this.id).then(function (data) {
				that.gotData = true;
				that.tasks = data.tasks.map(function (x) {
					return _.assign(x, taskState);
				});
				that.allEmployees = data.employees;
				that.activeEmployees = _.filter(data.employees, function (x) {
					return x.status !== 'deleted';
				});
				that.rooms = data.rooms.sort(naturalSort({ key: 'name' }));
				that.schedules = data.schedule;
				that.setState({
					name: data.name,
					description: data.description,
					totalPoints: data.totalPoints,
					passingScorePercentage: data.passingScorePercentage,
					passingScorePoints: data.passingScorePoints
				});
			}).catch(function () {
				alert('Failed to get instpection data');
				window.location = "/admin/inspections/";
			});
		}
	}, {
		key: "onNameDescriptionChange",
		value: function onNameDescriptionChange(name, description) {
			if (this.state.name === name && this.state.description === description) return;
			this.setState({
				name: name,
				description: description,
				showErrors: false,
				errorMessages: []
			});
		}
	}, {
		key: "onScoreChange",
		value: function onScoreChange(totalPoints, passingPoints, passingPercentage) {
			this.state.passingScorePercentage = passingPercentage;
			this.state.totalPoints = totalPoints;
			this.state.passingScorePoints = passingPoints;
		}
	}, {
		key: "isValid",
		value: function isValid() {
			var result = true;
			var messages = [];
			if (this.refs.tasks.state.tasks.length === 0) {
				result = false;
				messages.push('Please add at least one task.');
			}
			if (this.state.passingScorePercentage < 10 || this.state.passingScorePercentage > 100) {
				result = false;
				messages.push('Passing score % should be between 10 and 100.');
			}

			if (result) return true;

			this.setState({
				showErrors: true,
				errorMessages: messages
			});
		}
	}, {
		key: "onSave",
		value: function onSave(event) {
			event.preventDefault();

			if (this.state.name.trim() === '') return;
			if (!this.isValid()) return;

			var request = this.getSaveRequest();
			$.ajax({
				url: '/api/roomlogs/',
				dataType: 'json',
				type: 'POST',
				data: request,
				success: function () {}.bind(this),
				error: function (xhr, status, err) {
					console.error(status, err.toString());
				}.bind(this)
			});
		}
	}, {
		key: "getSaveRequest",
		value: function getSaveRequest() {
			var result = {
				id: this.id,
				name: this.state.name,
				description: this.state.description,
				type: 2,
				assignmentType: null,
				assigneeId: null,
				totalPoints: this.state.totalPoints,
				passingScorePercentage: this.state.passingScorePercentage,
				passingScorePoints: this.state.passingScorePoints,
				tasks: [],
				schedules: []
			};

			var order = 0;
			this.refs.tasks.state.tasks.forEach(function (x) {
				if (!x.changeStatus || x.changeStatus === '') return;

				var task = {
					id: x.changeStatus === 'i' ? -1 : x.id,
					subject: x.subject,
					description: '',
					order: order,
					points: x.points,
					failAll: x.failAll ? 1 : 0,
					changeStatus: x.changeStatus
				};
				result.tasks.push(task);
				order += 1;
				if (x.changeStatus === 'i' || x.changeStatus === 'u') x.changeStatus = '';
			});

			return result;
		}
	}, {
		key: "render",
		value: function render() {
			if (!this.gotData) return null;

			return React.createElement(
				"aside",
				{ className: "right-side" },
				React.createElement(AdminViewHeader, {
					url: "/admin/inspections",
					showNavigation: false,
					buttonLabel: "Back to Inspections",
					buttonLabelSmall: "Back",
					header: "Inspection Template" }),
				React.createElement(
					"section",
					{ className: "content" },
					React.createElement(
						"div",
						{ className: "whitecentercol" },
						React.createElement(NameDescription, {
							ref: nameDescriptionRef,
							onChange: this.onNameDescriptionChange.bind(this),
							model: {
								name: this.state.name,
								description: this.state.description
							} }),
						React.createElement(InspectionTasks, {
							ref: "tasks",
							inspectionId: this.id,
							tasks: this.tasks,
							passingScore: this.state.passingScorePercentage,
							onScoreChange: this.onScoreChange.bind(this) }),
						React.createElement(ErrorMessage, { showErrors: this.state.showErrors, errorMessages: this.state.errorMessages }),
						React.createElement(
							"div",
							null,
							React.createElement(
								"a",
								{ href: "#", className: "btn btn-primary", onClick: this.onSave.bind(this) },
								"Save Template"
							)
						),
						React.createElement(InspectionSchedules, {
							allEmployees: this.allEmployees,
							activeEmployees: this.activeEmployees,
							inspectionId: this.id,
							schedules: this.schedules,
							rooms: this.rooms
						})
					)
				)
			);
		}
	}]);

	return InspectionEdit;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionEdit;
ReactDOM.render(React.createElement(InspectionEdit, null), document.getElementById('view'));
//# sourceMappingURL=InspectionEdit.js.map