'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var pointsMin = 1;
var pointsMax = 10;
/**
 * New inspection task component.
 */

var InspectionNewTask = function (_React$Component) {
	_inherits(InspectionNewTask, _React$Component);

	function InspectionNewTask(props) {
		_classCallCheck(this, InspectionNewTask);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionNewTask).call(this, props));

		_this.state = {
			id: _this.props.newId,
			subject: '',
			points: pointsMin,
			failAll: false,
			pointsError: false,
			subjectError: false
		};
		return _this;
	}

	_createClass(InspectionNewTask, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			this.setState({ id: nextProps.newId });
		}
	}, {
		key: 'onChange',
		value: function onChange() {
			//noinspection JSUnresolvedVariable
			if (this.refs.points.value) {
				if (this.refs.points.value > pointsMax) this.refs.points.value = pointsMax;
				if (this.refs.points.value < pointsMin) this.refs.points.value = pointsMin;
			}

			this.setState({
				subject: this.refs.subject.value,
				points: this.refs.points.value,
				failAll: this.refs.failAll.checked || false,
				pointsError: false,
				subjectError: false
			});
		}
	}, {
		key: 'save',
		value: function save() {
			//TODO: vvs p1 validation
			var errors = false;
			if (!this.refs.subject.value.trim()) {
				errors = true;
				this.setState({ subjectError: true });
			}

			var availablePoints = _.range(pointsMin, pointsMax + 1);
			if (availablePoints.indexOf(parseInt(this.refs.points.value)) == -1) {
				errors = true;
				this.setState({ pointsError: true });
			}

			if (errors) {
				return;
			}
			//noinspection JSUnresolvedVariable
			this.props.onTaskAdded({
				id: this.state.id,
				subject: this.refs.subject.value,
				points: parseInt(this.refs.points.value),
				failAll: this.refs.failAll.checked || false,
				changeStatus: 'i'
			});
			this.setState({
				subject: '',
				points: pointsMin,
				failAll: false
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var checked = this.state.failAll ? 'checked' : null;

			return React.createElement(
				'tr',
				{ key: this.state.id, className: 'newRow' },
				React.createElement(
					'th',
					null,
					React.createElement('input', {
						id: 'newTaskSubject',
						ref: 'subject',
						type: 'text',
						value: this.state.subject,
						className: this.state.subjectError ? 'error' : '',
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					{ className: 'center' },
					React.createElement('input', {
						ref: 'points',
						type: 'number',
						min: pointsMin,
						max: pointsMax,
						className: 'inspection-task-points ' + (this.state.pointsError ? 'error' : ''),
						value: this.state.points,
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					{ className: 'center' },
					React.createElement('input', {
						ref: 'failAll',
						type: 'checkbox',
						checked: checked,
						value: 'failAll',
						onChange: this.onChange.bind(this) })
				),
				React.createElement(
					'th',
					null,
					React.createElement(
						'button',
						{ type: 'button', className: 'btn btn-link', onClick: this.save.bind(this) },
						'Add'
					)
				)
			);
		}
	}]);

	return InspectionNewTask;
}(React.Component);

if (typeof module !== 'undefined') module.exports = InspectionNewTask;

//# sourceMappingURL=InspectionNewTask.js.map