/**
 * Inspection new schedule component.
 */
class InspectionNewSchedule extends React.Component {
	constructor(props) {
		super(props);
		this.state = InspectionNewSchedule.getDefaultState();
	}

	static getDefaultState() {
		return {
			id: -1,
			date: (new Date()).toISOString().substring(0, 10),
			employeeId: -1,
			supervisorId: -1,
			roomId: -1,
			employeeName: '',
			supervisorName: '',
			room: '',
			errors: {}
		};
	}

	onEmployeeChange(event) {
		let errors = this.state.errors || {};
		errors.employeeId = false;

		this.setState({
			employeeId: event.target.value,
			employeeName: event.target.selectedOptions[0].label,
			errors: errors
		});
	}

	onSupervisorChange(event) {
		let errors = this.state.errors || {};
		errors.supervisorId = false;
		this.setState({
			supervisorId: event.target.value,
			supervisorName: event.target.selectedOptions[0].label,
			errors: errors
		});
	}

	onRoomChange(event) {
		let errors = this.state.errors || {};
		errors.roomId = false;
		this.setState({
			roomId: event.target.value,
			room: event.target.selectedOptions[0].label,
			errors: errors
		});
	}

	onDateChange(event) {
		let errors = this.state.errors || {};
		errors.date = false;
		this.setState({
			date: event.target.value,
			errors: errors
		});
	}

	validationPassed() {
		let passed = true;
		let errors = this.state.errors || {};
		if (this.state.date == -1 || !this.state.date) {
			errors.date = true;
			passed = false;
		}

		if (this.state.roomId == -1 || !this.state.roomId) {
			errors.roomId = true;
			passed = false;
		}

		if (this.state.employeeId == -1 || !this.state.employeeId) {
			errors.employeeId = true;
			passed = false;
		}

		if (this.state.supervisorId == -1 || !this.state.supervisorId) {
			errors.supervisorId = true;
			passed = false;
		}

		this.setState({errors: errors});

		return passed;
	}

	onAddClick() {
		if (!this.validationPassed()) {
			return;
		}

		const newSchedule = {
			id: this.state.id,
			dueDateTime: this.state.date,
			employeeId: this.state.employeeId,
			employeeName: this.state.employeeName,
			supervisorId: this.state.supervisorId,
			supervisorName: this.state.supervisorName,
			roomId: this.state.roomId,
			roomName: this.state.room,
			status: "Assigned"
		};
		this.props.onScheduleAdded(newSchedule);
		this.setState(InspectionNewSchedule.getDefaultState());
	}

	render() {
		return <tr className="newRow" key={this.state.id}>
			<td>
				<input type="date"
						value={this.state.date}
						className={this.state.errors.date ? 'error' : ''}
						onChange={this.onDateChange.bind(this)}/>
			</td>
			<td>
				<select id="employeeId"
						value={this.state.employeeId}
						className={this.state.errors.employeeId ? 'error' : ''}
						onChange={this.onEmployeeChange.bind(this)}>
					<option value="-1" disabled="disabled">--Select employee--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select id="supervisorId"
						value={this.state.supervisorId}
						className={this.state.errors.supervisorId ? 'error' : ''}
						onChange={this.onSupervisorChange.bind(this)}>
					<option value="-1" disabled="disabled">--Select supervisor--</option>
					{this.props.employeeOptions}
				</select>
			</td>
			<td>
				<select id="roomId"
						value={this.state.roomId}
						className={this.state.errors.roomId ? 'error' : ''}
						onChange={this.onRoomChange.bind(this)}>
					<option value="-1" disabled="disabled">--Select location--</option>
					{this.props.roomOptions}
				</select>
			</td>
			<td/>
			<td/>
			<td/>
			<td width="10%">
				<button type="button" className="btn btn-link" onClick={this.onAddClick.bind(this)}>Add</button>
			</td>
		</tr>
	}
}
if (typeof module !== 'undefined') module.exports = InspectionNewSchedule;
