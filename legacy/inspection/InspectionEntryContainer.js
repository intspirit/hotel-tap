'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Grid = ReactBootstrap.Grid,
    FormControls = ReactBootstrap.FormControls,
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    ButtonGroup = ReactBootstrap.ButtonGroup,
    Button = ReactBootstrap.Button,
    Table = ReactBootstrap.Table;

/**
 * Inspection entry container component.
 * The root for the inspection entry.
 */

var InspectionEntryContainer = function (_React$Component) {
	_inherits(InspectionEntryContainer, _React$Component);

	function InspectionEntryContainer(props) {
		_classCallCheck(this, InspectionEntryContainer);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(InspectionEntryContainer).call(this, props));

		_this.state = {
			scorePoints: '',
			scorePercentage: '',
			tasks: _this.props.model.tasks,
			result: '',
			attachments: [],
			showErrors: false
		};

		return _this;
	}

	_createClass(InspectionEntryContainer, [{
		key: 'taskScoreChanged',
		value: function taskScoreChanged(taskId, result) {
			var task = _.find(this.state.tasks, function (x) {
				return x.id == taskId;
			});
			switch (result) {
				case 'fail':
					task.score = 0;
					task.done = true;
					break;
				case 'pass':
					task.score = task.points;
					task.done = true;
					break;
				default:
					task.score = 0;
					task.done = false;
					break;
			}
			this.calcResult();
		}
	}, {
		key: 'taskCommentAdded',
		value: function taskCommentAdded(taskId, data, attachments) {
			var task = _.find(this.state.tasks, function (x) {
				return x.id == taskId;
			});
			var uniqueId = _.uniqueId();
			data.uniqueId = uniqueId;

			task.comments = task.comments || [];
			task.comments.push(data);
			attachments.forEach(function (x) {
				x.commentUniqueId = uniqueId;
			});
			this.setState({ attachments: this.state.attachments.concat(attachments) });
		}
	}, {
		key: 'calcResult',
		value: function calcResult() {
			var result = '';
			var score = 0;
			var completedCount = 0;
			_(this.state.tasks).forEach(function (x) {
				if (result === 'Fail' || x.done && x.failAll == 1 && x.score == 0) result = 'Fail';
				completedCount += x.done ? 1 : 0;
				score += x.done ? parseInt(x.score) : 0;
			});
			if (result != 'Fail') {
				result = completedCount == this.state.tasks.length ? score >= this.props.model.roomLog.passingScorePoints ? 'Pass' : 'Fail' : '';
			}
			var scorePercentage = getScorePercentage(score, this.props.model.roomLog.totalPoints);
			this.setState({
				scorePoints: score > 0 ? score : null,
				scorePercentage: scorePercentage > 0 ? scorePercentage : null,
				tasks: this.state.tasks,
				result: result
			});
		}
	}, {
		key: 'validationPassed',
		value: function validationPassed() {
			var passed = true;
			this.state.tasks.forEach(function (x) {
				if (!x.done) {
					passed = false;
				}
			});

			return passed;
		}
	}, {
		key: 'onSaveClicked',
		value: function onSaveClicked() {
			if (!this.validationPassed()) {
				this.setState({ showErrors: true });
				return;
			}
			if (this.state.savingInProcess) return;

			this.setState({ savingInProcess: true });

			var schedule = this.props.model.schedule;
			schedule.scorePercentage = this.state.scorePercentage;
			schedule.scorePoints = this.state.scorePoints;
			schedule.inspectionPassed = this.state.result == 'Pass' ? 1 : 0;

			var data = new FormData();
			this.state.attachments.forEach(function (x) {
				data.append(x.commentUniqueId + '[]', x);
			});

			var request = {
				id: this.props.model.schedule.id,
				tasks: this.state.tasks,
				scorePoints: this.state.scorePoints,
				scorePercentage: this.scorePercentage,
				schedule: schedule
			};

			data.append('data', JSON.stringify(request));

			$.ajax({
				url: '/api/inspections/entry',
				dataType: 'json',
				type: 'POST',
				data: data,
				cache: false,
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string reques
				success: function () {
					this.setState({ savingInProcess: false });
					this.onCancelClicked();
				}.bind(this),
				error: function (xhr, status, err) {
					this.setState({ savingInProcess: false });
					alert('Failed to save the inspection');
				}.bind(this)
			});
		}
	}, {
		key: 'onCancelClicked',
		value: function onCancelClicked() {
			var url = $('#backToBoardButton').attr('href');
			window.location.assign(url);
		}
	}, {
		key: 'render',
		value: function render() {
			if (this.state.tasks.length == 0) return null;

			return React.createElement(InspectionEntry, {
				taskScoreChanged: this.taskScoreChanged.bind(this),
				taskCommentAdded: this.taskCommentAdded.bind(this),
				employees: this.props.model.employees,
				roomLog: this.props.model.roomLog,
				result: this.state.result,
				scorePoints: this.state.scorePoints,
				scorePercentage: this.state.scorePercentage,
				showErrors: this.state.showErrors,
				schedule: this.props.model.schedule,
				onSaveClicked: this.onSaveClicked.bind(this),
				onCancelClicked: this.onCancelClicked.bind(this),
				tasks: this.props.model.tasks
			});
		}
	}]);

	return InspectionEntryContainer;
}(React.Component);

$('#backToBoardButton').hide();

//# sourceMappingURL=InspectionEntryContainer.js.map