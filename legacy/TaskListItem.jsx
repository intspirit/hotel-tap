'use strict';

let HotelTap = HotelTap || {};

/**
 * Task item in a list.
 * @type {{new(*=): {render: (function())}}}
 */
HotelTap.TaskListItem = class extends React.Component {
	constructor(params) {
		super(params);
	}

	render() {
		const task = this.props.task;
		const comments = task.comments.map(x => <HotelTap.CommentListItem comment={x} key={x.id}/>);

		return <div className="task-wrapper">
			<div key={task.id} className='task-item'>
				<div className="task-info">
					<span className="glyphicon glyphicon-ok icon-checked"></span>
					<div className="task-subject">
						{task.subject}
					</div>
					<div className="task-description">
						{task.description}
					</div>
					<div className="task-comments">
						{comments}
					</div>
				</div>
				<div className="task-completed-by">
					<img src={task.userPicture || "/images/default.png"} />
				</div>
				<div className="task-completion-date">
					{moment(task.completionDateTime).format('MMM D')}
				</div>
				<div className="clearfix"></div>
			</div>
		</div>
	}
};


