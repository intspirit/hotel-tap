/**
 * Utility functions.
 */
class Utils {

	/**
	 * Gets user image URL.
	 * @param fileName
	 * @returns {string}
	 */
	static getUserImageUrl(fileName) {
		return fileName === '' ?
			'/images/user-icon.png' :
			`${HotelTap.imagesRootUrl}img/profile/${fileName}`
	}

	/***
	 * Gets employee full name.
	 * @param id
	 * @param employees
	 * @returns {*}
	 */
	static getEmployeeName(id, employees) {
		if (!employees || !id) return '';

		const employee = _.find(employees, x => x.user_id == id);
		return employee ? `${employee.first_name} ${employee.last_name}` : '';
	}

	static isString(str) {
		return str && typeof str === "string";
	}

	static isImage(str) {
		return Utils.isString(str) && str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
	}
}
