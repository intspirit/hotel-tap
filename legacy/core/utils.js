'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Utility functions.
 */

var Utils = function () {
	function Utils() {
		_classCallCheck(this, Utils);
	}

	_createClass(Utils, null, [{
		key: 'getUserImageUrl',


		/**
   * Gets user image URL.
   * @param fileName
   * @returns {string}
   */
		value: function getUserImageUrl(fileName) {
			return fileName === '' ? '/images/user-icon.png' : HotelTap.imagesRootUrl + 'img/profile/' + fileName;
		}

		/***
   * Gets employee full name.
   * @param id
   * @param employees
   * @returns {*}
   */

	}, {
		key: 'getEmployeeName',
		value: function getEmployeeName(id, employees) {
			if (!employees || !id) return '';

			var employee = _.find(employees, function (x) {
				return x.user_id == id;
			});
			return employee ? employee.first_name + ' ' + employee.last_name : '';
		}
	}, {
		key: 'isString',
		value: function isString(str) {
			return str && typeof str === "string";
		}
	}, {
		key: 'isImage',
		value: function isImage(str) {
			return Utils.isString(str) && str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
		}
	}]);

	return Utils;
}();
//# sourceMappingURL=utils.js.map