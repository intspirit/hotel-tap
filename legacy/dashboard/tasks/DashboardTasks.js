'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = ReactBootstrap.Input,
    Popover = ReactBootstrap.Popover,
    Row = ReactBootstrap.Row,
    Image = ReactBootstrap.Image,
    Col = ReactBootstrap.Col,
    OverlayTrigger = ReactBootstrap.OverlayTrigger,
    dateTimeFormat = 'MM/DD/YY hh:mm a',
    //02/10/2016 9:51 am
dateFormat = 'MM/DD/YY'; //02/10/2016

/**
 * Dashboard Tasks component.
 */

var DashboardTasks = function (_React$Component) {
	_inherits(DashboardTasks, _React$Component);

	function DashboardTasks(props) {
		_classCallCheck(this, DashboardTasks);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DashboardTasks).call(this, props));

		_this.state = {};
		_this.users = [];
		_this.departments = [];
		_this.user = {};
		_this.data = {};
		return _this;
	}

	_createClass(DashboardTasks, [{
		key: 'getGroupByDate',
		value: function getGroupByDate(x) {
			var now = new Date();
			var format = x.type === 'roomLog' ? dateFormat : dateTimeFormat;
			x.dateClass = '';
			if (x.dueDateTime === '0000-00-00 00:00:00') {
				x.due = '3:No due date';
				x.date = '';
			} else if (moment(x.dueDateTime).isBefore(now)) {
				x.due = '0:Overdue';
				x.date = moment(x.dueDateTime).format(format);
				x.dateClass = 'red';
			} else if (moment(x.dueDateTime).isSame(now, 'day')) {
				x.due = '1:Today';
				x.date = moment(x.dueDateTime).format(format);
			} else if (moment(x.dueDateTime).isAfter(now, 'day')) {
				x.due = '2:Future';
				x.date = moment(x.dueDateTime).format(format);
			}
			x.assigneeName = '';
			var imageUrl = '';
			if (x.assigneeType === 'e') {
				var employee = this.users[x.assigneeId];
				if (!employee) {
					employee = {
						imageUrl: '',
						fullName: 'former employee'
					};
				}
				imageUrl = Utils.getUserImageUrl(employee.imageUrl);
				x.assigneeName = employee.fullName;
			} else {
				var department = this.departments[x.assigneeId];
				if (!department) {
					department = {
						imageUrl: '/images/home.png',
						name: 'Deleted department'
					};
				}
				imageUrl = department.imageUrl;
				x.assigneeName = department.name;
			}
			x.assigneeImageUrl = imageUrl === '' ? '/images/user-icon.png' : imageUrl;
			x.commentEntryVisible = false;
			x.flagVisible = x.flag;

			return x;
		}
	}, {
		key: 'getGroupByAssignment',
		value: function getGroupByAssignment(x) {
			var now = new Date();
			var format = x.type === 'roomLog' ? dateFormat : dateTimeFormat;

			var imageUrl = '';
			x.assigneeName = '';
			if (x.assigneeType === 'e') {
				var employee = this.users[x.assigneeId];
				if (!employee) return;
				imageUrl = Utils.getUserImageUrl(employee.imageUrl);
				x.assigneeName = employee.fullName;
			} else {
				var department = this.departments[x.assigneeId];
				imageUrl = department.imageUrl;
				x.assigneeName = department.name;
			}
			x.group = x.assigneeId + ':' + x.assigneeName;

			x.date = moment(x.dueDateTime).format(format);
			x.dateClass = '';

			if (x.dueDateTime === '0000-00-00 00:00:00') {
				x.date = '';
			} else if (moment(x.dueDateTime).isBefore(now)) {
				x.date = moment(x.dueDateTime).format(format);
				x.dateClass = 'red';
			} else {
				x.due = '1:Today';
				x.date = moment(x.dueDateTime).format(format);
			}

			x.assigneeImageUrl = imageUrl === '' ? '/images/user-icon.png' : imageUrl;
			x.commentEntryVisible = false;
			x.flagVisible = x.flag;

			return x;
		}
	}, {
		key: 'convertEmployeesToLookup',
		value: function convertEmployeesToLookup(employees) {
			this.users = employees.reduce(function (map, x) {
				map[x.userId] = x;
				return map;
			}, {});
		}
	}, {
		key: 'convertDeprtmentsToLookups',
		value: function convertDeprtmentsToLookups(departments) {
			this.departments = departments.reduce(function (map, x) {
				map[x.id] = x;
				return map;
			}, {});
		}
	}, {
		key: 'getGroupsViewModelByDueDate',
		value: function getGroupsViewModelByDueDate() {
			var _this2 = this;

			var tasks = _.sortBy(this.data.tasks, 'dueDateTime');
			var tasksWithGroups = tasks.map(function (x) {
				return _this2.getGroupByDate(x);
			});
			var result = _.chain(tasksWithGroups).groupBy('due').toPairs().map(function (y) {
				return _.zipObject(['due', 'tasks'], y);
			}).each(function (x) {
				var _x$due$split = x.due.split(':');

				var _x$due$split2 = _slicedToArray(_x$due$split, 2);

				var id = _x$due$split2[0];
				var name = _x$due$split2[1];

				x.id = id;
				x.name = name;
				x.count = x.tasks.length;
				x.dateClass = '';
			}).orderBy('id').value();

			return result;
		}
	}, {
		key: 'getGroupsViewModelByAssignment',
		value: function getGroupsViewModelByAssignment() {
			var _this3 = this;

			var tasks = _.sortBy(this.data.tasks, 'assigneeId');
			var tasksWithGroups = tasks.map(function (x) {
				return _this3.getGroupByAssignment(x);
			});
			var result = _.chain(tasksWithGroups).groupBy('group').toPairs().map(function (y) {
				return _.zipObject(['group', 'tasks'], y);
			}).each(function (x) {
				var _x$group$split = x.group.split(':');

				var _x$group$split2 = _slicedToArray(_x$group$split, 2);

				var id = _x$group$split2[0];
				var name = _x$group$split2[1];

				x.id = id;
				x.name = name;
				x.count = x.tasks.length;
				x.dateClass = '';
			}).orderBy('name').value();

			return result;
		}
	}, {
		key: 'getData',
		value: function getData() {
			var _this4 = this;

			taskService.getTasksForDashboard().then(function (data) {
				_this4.data = data;
				_this4.convertEmployeesToLookup(_this4.data.employees);
				_this4.convertDeprtmentsToLookups(_this4.data.departments);
				var groupViewModel = _this4.getGroupsViewModelByDueDate();
				var viewModel = {
					title: 'Tasks',
					groups: groupViewModel
				};
				_this4.user = _this4.users[data.user];
				_this4.setState(viewModel);
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.getData();
		}
	}, {
		key: 'getFlag',
		value: function getFlag(visible) {
			return visible ? React.createElement('label', { className: 'flag flag-red pull-right dashboard-tasks-flag', title: 'Flag' }) : React.createElement(
				'span',
				null,
				' '
			);
		}
	}, {
		key: 'getDescriptionHtml',
		value: function getDescriptionHtml(taskRow) {
			var descriptionHtml = { __html: taskRow.description };

			if (taskRow.type == 'roomLog' || taskRow.type == 'inspection') {
				descriptionHtml = null;
			}

			return descriptionHtml;
		}
	}, {
		key: 'getCommentSection',
		value: function getCommentSection(taskRow) {
			return taskRow.type === 'task' ? React.createElement(Comments, {
				parentType: 'tasks',
				parentId: taskRow.id,
				count: taskRow.commentCount,
				user: this.user,
				users: this.users }) : null;
		}
	}, {
		key: 'task',
		value: function task(taskRow) {
			if (!taskRow) return null;
			return React.createElement(
				'div',
				null,
				React.createElement(
					Row,
					{ key: taskRow.id, className: 'task-row' },
					React.createElement(
						Col,
						{ xs: 8 },
						React.createElement(
							'span',
							{ className: 'task-subject' },
							taskRow.subject
						)
					),
					React.createElement(
						Col,
						{ xs: 4, style: { fontSize: '12px' } },
						this.getFlag(taskRow.flagVisible),
						React.createElement(
							OverlayTrigger,
							{
								trigger: 'hover',
								placement: 'left',
								overlay: React.createElement(
									Popover,
									{
										title: 'Assigned to' },
									React.createElement(Image, { src: taskRow.assigneeImageUrl }),
									React.createElement(
										'span',
										{ className: 'dashboard-tasks-assignee-name-popover' },
										taskRow.assigneeName
									)
								) },
							React.createElement(Image, { className: 'pull-right assignee-image', src: taskRow.assigneeImageUrl })
						),
						React.createElement(
							'span',
							{ className: 'pull-right dashboard-due-date ' + taskRow.dateClass },
							taskRow.date
						),
						React.createElement(
							'span',
							{ className: 'pull-right ' },
							'Assigned to ' + taskRow.assigneeName
						)
					)
				),
				React.createElement(
					Row,
					null,
					React.createElement(
						Col,
						{ xs: 8, className: 'description' },
						React.createElement('div', { dangerouslySetInnerHTML: this.getDescriptionHtml(taskRow) })
					)
				),
				this.getCommentSection(taskRow),
				React.createElement('div', { className: 'task-divider' })
			);
		}
	}, {
		key: 'groupByChanged',
		value: function groupByChanged(event) {
			var groups = event.target.value == 1 ? this.getGroupsViewModelByDueDate() : this.getGroupsViewModelByAssignment();

			var viewModel = { groups: groups };
			this.setState(viewModel);
		}
	}, {
		key: 'getFilterComponent',
		value: function getFilterComponent() {
			return React.createElement(
				Input,
				{
					type: 'select',
					label: '',
					placeholder: 'select',
					onChange: this.groupByChanged.bind(this),
					wrapperClassName: 'col-xs-9',
					labelClassName: 'col-xs-2' },
				React.createElement(
					'option',
					{ value: '1' },
					'Due date'
				),
				React.createElement(
					'option',
					{ value: '2' },
					'Assignment'
				)
			);
		}
	}, {
		key: 'getTitle',
		value: function getTitle() {
			return React.createElement(
				'div',
				{ className: 'title-row' },
				React.createElement(
					'h2',
					null,
					React.createElement(
						'span',
						{ className: 'mobileNav-icon' },
						React.createElement('i', { className: 'fa fa-navicon' })
					),
					React.createElement(
						'span',
						null,
						this.state.title
					)
				)
			);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this5 = this;

			if (!this.state.groups) return null;

			var groups = this.state.groups.map(function (x) {
				return React.createElement(
					'div',
					null,
					React.createElement(
						'h2',
						{ key: x.id },
						x.name,
						React.createElement(
							'span',
							{ className: 'pull-right' },
							x.count
						)
					),
					x.tasks.map(function (y) {
						return _this5.task(y);
					})
				);
			});
			return React.createElement(
				'div',
				{ className: 'col-2' },
				React.createElement(
					'div',
					{ className: 'dashboard-tasks-filler' },
					' '
				),
				React.createElement(
					'div',
					{ role: 'tabpanel', className: 'tab-section' },
					React.createElement(DashboardTabs, { active: 'tasks' }),
					React.createElement(
						'div',
						{ className: 'tab-content' },
						React.createElement(
							'div',
							{ role: 'tabpanel', className: 'active', id: 'tasks' },
							React.createElement(
								Row,
								null,
								React.createElement(
									'div',
									{ className: 'dashboard-tasks-filter-label' },
									React.createElement(
										'h1',
										{ className: 'pull-left' },
										'Incomplete tasks by'
									)
								),
								React.createElement(
									'div',
									{ className: 'dashboard-tasks-filter' },
									this.getFilterComponent()
								)
							),
							groups
						)
					)
				)
			);
		}
	}]);

	return DashboardTasks;
}(React.Component);

if (typeof module !== 'undefined') module.exports = DashboardTasks;

/**
 * Renders component in the view.
 */
function render() {
	ReactDOM.render(React.createElement(DashboardTasks, null), document.getElementById('view'));
}

render();
//# sourceMappingURL=DashboardTasks.js.map