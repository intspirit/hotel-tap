const
	Input = ReactBootstrap.Input,
	Popover = ReactBootstrap.Popover,
	Row = ReactBootstrap.Row,
	Image = ReactBootstrap.Image,
	Col = ReactBootstrap.Col,
	OverlayTrigger = ReactBootstrap.OverlayTrigger,
	dateTimeFormat = 'MM/DD/YY hh:mm a', //02/10/2016 9:51 am
	dateFormat = 'MM/DD/YY'; //02/10/2016


/**
 * Dashboard Tasks component.
 */
class DashboardTasks extends React.Component {

	constructor(props) {
		super(props);
		this.state = {};
		this.users = [];
		this.departments = [];
		this.user = {};
		this.data = {};
	}

	getGroupByDate(x) {
		const now = new Date();
		const format = x.type === 'roomLog' ? dateFormat : dateTimeFormat;
		x.dateClass = '';
		if (x.dueDateTime === '0000-00-00 00:00:00') {
			x.due = '3:No due date';
			x.date = '';
		} else if (moment(x.dueDateTime).isBefore(now)) {
			x.due = '0:Overdue';
			x.date = moment(x.dueDateTime).format(format);
			x.dateClass = 'red';
		} else if (moment(x.dueDateTime).isSame(now, 'day')) {
			x.due = '1:Today';
			x.date = moment(x.dueDateTime).format(format);
		} else if (moment(x.dueDateTime).isAfter(now, 'day')) {
			x.due = '2:Future';
			x.date = moment(x.dueDateTime).format(format);
		}
		x.assigneeName = '';
		let imageUrl = '';
		if (x.assigneeType === 'e') {
			let employee = this.users[x.assigneeId];
			if (!employee) {
				employee = {
					imageUrl: '',
					fullName: 'former employee'
				}
			}
			imageUrl = Utils.getUserImageUrl(employee.imageUrl);
			x.assigneeName = employee.fullName;
		} else {
			let department = this.departments[x.assigneeId];
			if (!department) {
				department = {
					imageUrl: '/images/home.png',
					name: 'Deleted department'
				}
			}
			imageUrl = department.imageUrl;
			x.assigneeName = department.name;
		}
		x.assigneeImageUrl = imageUrl === '' ? '/images/user-icon.png' : imageUrl;
		x.commentEntryVisible = false;
		x.flagVisible = x.flag;

		return x;
	}

	getGroupByAssignment(x) {
		const now = new Date();
		const format = x.type === 'roomLog' ? dateFormat : dateTimeFormat;

		let imageUrl = '';
		x.assigneeName = '';
		if (x.assigneeType === 'e') {
			const employee = this.users[x.assigneeId];
			if (!employee) return;
			imageUrl = Utils.getUserImageUrl(employee.imageUrl);
			x.assigneeName = employee.fullName;
		} else {
			const department = this.departments[x.assigneeId];
			imageUrl = department.imageUrl;
			x.assigneeName = department.name;
		}
		x.group = `${x.assigneeId}:${x.assigneeName}`;

		x.date = moment(x.dueDateTime).format(format);
		x.dateClass = '';

		if (x.dueDateTime === '0000-00-00 00:00:00') {
			x.date = '';
		} else if (moment(x.dueDateTime).isBefore(now)) {
			x.date = moment(x.dueDateTime).format(format);
			x.dateClass = 'red';
		} else {
			x.due = '1:Today';
			x.date = moment(x.dueDateTime).format(format);
		}

		x.assigneeImageUrl = imageUrl === '' ? '/images/user-icon.png' : imageUrl;
		x.commentEntryVisible = false;
		x.flagVisible = x.flag;

		return x;
	}

	convertEmployeesToLookup(employees) {
		this.users = employees.reduce((map, x) => {
			map[x.userId] = x;
			return map;
		}, {});
	}

	convertDeprtmentsToLookups(departments) {
		this.departments = departments.reduce((map, x) => {
			map[x.id] = x;
			return map;
		}, {});
	}

	getGroupsViewModelByDueDate() {
		const tasks = _.sortBy(this.data.tasks, 'dueDateTime');
		const tasksWithGroups = tasks.map(x => this.getGroupByDate(x));
		const result = _.chain(tasksWithGroups).
		groupBy('due').
		toPairs().
		map(y => {
			return _.zipObject(['due', 'tasks'], y)
		}).
		each(x => {
			let [id, name] = x.due.split(':');
			x.id = id;
			x.name = name;
			x.count = x.tasks.length;
			x.dateClass = '';
		}).
		orderBy('id').
		value();

		return result;
	}

	getGroupsViewModelByAssignment() {
		const tasks = _.sortBy(this.data.tasks, 'assigneeId');
		const tasksWithGroups = tasks.map(x => this.getGroupByAssignment(x));
		const result = _.chain(tasksWithGroups).
		groupBy('group').
		toPairs().
		map(y => {
			return _.zipObject(['group', 'tasks'], y)
		}).
		each(x => {
			let [id, name] = x.group.split(':');
			x.id = id;
			x.name = name;
			x.count = x.tasks.length;
			x.dateClass = '';
		}).
		orderBy('name').
		value();

		return result;
	}

	getData() {
		taskService.getTasksForDashboard().then(data => {
			this.data = data;
			this.convertEmployeesToLookup(this.data.employees);
			this.convertDeprtmentsToLookups(this.data.departments);
			const groupViewModel = this.getGroupsViewModelByDueDate();
			const viewModel = {
				title: 'Tasks',
				groups: groupViewModel
			};
			this.user = this.users[data.user];
			this.setState(viewModel);
		});
	}

	componentDidMount() {
		this.getData();
	}

	getFlag(visible) {
		return visible ?
			<label className='flag flag-red pull-right dashboard-tasks-flag' title='Flag'/> :
			<span>&nbsp;</span>;
	}

	getDescriptionHtml(taskRow) {
		let descriptionHtml = {__html: taskRow.description};

		if (taskRow.type == 'roomLog' || taskRow.type == 'inspection') {
			descriptionHtml = null;
		}

		return descriptionHtml;
	}

	getCommentSection(taskRow) {
		return taskRow.type === 'task' ?
			<Comments
				parentType='tasks'
				parentId={taskRow.id}
				count={taskRow.commentCount}
				user={this.user}
				users={this.users}/> :
			null;
	}

	task(taskRow) {
		if (!taskRow) return null;
		return <div>
			<Row key={taskRow.id} className='task-row'>
				<Col xs={8}>
					<span className='task-subject'>{taskRow.subject}</span>
				</Col>
				<Col xs={4} style={{fontSize: '12px'}}>
					{this.getFlag(taskRow.flagVisible)}
					<OverlayTrigger
						trigger='hover'
						placement='left'
						overlay={<Popover
							title='Assigned to'>
								<Image  src={taskRow.assigneeImageUrl}/>
								<span className='dashboard-tasks-assignee-name-popover'>{taskRow.assigneeName}</span>

							</Popover>}>
						<Image className='pull-right assignee-image' src={taskRow.assigneeImageUrl}/>
					</OverlayTrigger>

					<span className={`pull-right dashboard-due-date ${taskRow.dateClass}`}>{taskRow.date}</span>
					<span className='pull-right '>{`Assigned to ${taskRow.assigneeName}`}</span>
				</Col>
			</Row>
			<Row>
				<Col xs={8} className='description'>
					<div dangerouslySetInnerHTML={this.getDescriptionHtml(taskRow)}></div>
				</Col>
			</Row>
			{this.getCommentSection(taskRow)}
			<div className='task-divider'></div>
		</div>
	}

	groupByChanged(event) {
		const groups = (event.target.value == 1) ?
			this.getGroupsViewModelByDueDate() :
			this.getGroupsViewModelByAssignment();

		const viewModel = {groups: groups};
		this.setState(viewModel);
	}

	getFilterComponent() {
		return <Input
			type="select"
			label=""
			placeholder="select"
			onChange={this.groupByChanged.bind(this)}
			wrapperClassName="col-xs-9"
			labelClassName="col-xs-2">
			<option value="1">Due date</option>
			<option value="2">Assignment</option>
		</Input>
	}

	getTitle() {
		return <div className='title-row'>
			<h2>
				<span className='mobileNav-icon'><i className='fa fa-navicon'/></span>
				<span>{this.state.title}</span>
			</h2>
		</div>
	}

	render() {
		if (!this.state.groups) return null;

		const groups = this.state.groups.map(x => {
			return <div>
				<h2 key={x.id}>
					{x.name}
					<span className='pull-right'>{x.count}</span>
				</h2>
				{x.tasks.map(y => this.task(y))}
			</div>
		});
		return (
			<div className="col-2">
				<div className='dashboard-tasks-filler'>&nbsp;</div>
				<div role='tabpanel' className='tab-section'>
					<DashboardTabs active="tasks"/>
					<div className='tab-content'>
						<div role='tabpanel' className='active' id='tasks'>
							<Row>
								<div className='dashboard-tasks-filter-label'>
									<h1 className='pull-left'>Incomplete tasks by</h1>
								</div>
								<div className='dashboard-tasks-filter'>
									{this.getFilterComponent()}
								</div>
							</Row>
							{groups}
						</div>
					</div>
				</div>
			</div>)
	}
}
if (typeof module !== 'undefined')
	module.exports = DashboardTasks;

/**
 * Renders component in the view.
 */
function render() {
	ReactDOM.render(<DashboardTasks />, document.getElementById('view'));
}

render();
