/**
 * Dashboard navigation tabs.
 */
class DashboardTabs extends React.Component {
	constructor(props) {
		super(props);
		this.state = props;
	}

	render() {
		return <ul className='nav nav-tabs' role='tablist'>
			<li role='presentation' className={(this.state.active == 'tasks') ? 'active' : '' }>
				<a href='/hotel/dashboard/tasks' aria-controls='tasks' role='tab'>Tasks</a>
			</li>
			<li role='presentation' className=''>
				<a href='/hotel/dashboard/maintenance' aria-controls='maintenance'
				   role='tab'>Maintenance
				</a>
			</li>
			<li role='presentation' className=''>
				<a href='/hotel/dashboard/complaints' aria-controls='complaints' role='tab'>Complaints</a>
			</li>
			<li role='presentation' className={(this.state.active == 'roomlog') ? 'active' : '' }>
				<a href='/hotel/dashboard/roomlog' aria-controls='roomlog' role='tab'>Room Logs</a>
			</li>
		</ul>
	}
}
