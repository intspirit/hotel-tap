'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Dashboard navigation tabs.
 */

var DashboardTabs = function (_React$Component) {
	_inherits(DashboardTabs, _React$Component);

	function DashboardTabs(props) {
		_classCallCheck(this, DashboardTabs);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DashboardTabs).call(this, props));

		_this.state = props;
		return _this;
	}

	_createClass(DashboardTabs, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'ul',
				{ className: 'nav nav-tabs', role: 'tablist' },
				React.createElement(
					'li',
					{ role: 'presentation', className: this.state.active == 'tasks' ? 'active' : '' },
					React.createElement(
						'a',
						{ href: '/hotel/dashboard/tasks', 'aria-controls': 'tasks', role: 'tab' },
						'Tasks'
					)
				),
				React.createElement(
					'li',
					{ role: 'presentation', className: '' },
					React.createElement(
						'a',
						{ href: '/hotel/dashboard/maintenance', 'aria-controls': 'maintenance',
							role: 'tab' },
						'Maintenance'
					)
				),
				React.createElement(
					'li',
					{ role: 'presentation', className: '' },
					React.createElement(
						'a',
						{ href: '/hotel/dashboard/complaints', 'aria-controls': 'complaints', role: 'tab' },
						'Complaints'
					)
				),
				React.createElement(
					'li',
					{ role: 'presentation', className: this.state.active == 'roomlog' ? 'active' : '' },
					React.createElement(
						'a',
						{ href: '/hotel/dashboard/roomlog', 'aria-controls': 'roomlog', role: 'tab' },
						'Room Logs'
					)
				)
			);
		}
	}]);

	return DashboardTabs;
}(React.Component);

//# sourceMappingURL=DashboardTabs.js.map