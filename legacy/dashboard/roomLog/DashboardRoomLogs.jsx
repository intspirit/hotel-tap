const
	Input = ReactBootstrap.Input,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col,
	Image = ReactBootstrap.Image;

/**
 * Dashboard Tasks component.
 */
class DashboardRoomLogs extends React.Component {
	constructor(params) {
		super(params);
		this.state = {
			roomLogs: []
		};
		this.dataLoaded = false;
	}

	getViewModel(logsAndYears) {
		let roomLogsData = logsAndYears[0] || {};
		let years = logsAndYears[1] || {};
		return roomLogsData.roomLogs.map(roomLog => {
			roomLog.years = years[roomLog.roomLogId] || [];
			return roomLog;
		});
	}

	getData() {
		Promise.all([roomLogService.getAll(), roomLogService.getYears()]).then(logsAndYears => {
			this.dataLoaded = true;
			const viewModel = this.getViewModel(logsAndYears);
			this.setState({
				roomsCount: logsAndYears[0].roomsCount,
				roomLogs: viewModel
			});
		});
	}

	componentDidMount() {
		this.getData();
	}

	getNoDataContent() {
		return this.dataLoaded ?
			<div id='noData' className="text-center">
				<Image src="/images/logo.png"/>
				<p>
					Please create Room Logs for recurring guest room tasks such as<br/>
					- Preventive Maintenance<br/>
					- Housekeeping Deep Cleaning<br/>
					- Pest Control<br/>
					- Safety Inspections, etc<br/><br/>
					from the
					<a href='/admin/roomlogs'> Admin Settings </a>
					Menu or call HotelTap support at <a href="tel:8443817221">844-381-7221</a>
				</p>
			</div>
			: <div className='text-center'>
			<i className="fa fa-spinner fa-spin" style={{fontSize: '96px;', marginTop: '40px'}}/>
		</div>;
	}

	render() {
		const hasNoContent = !this.state.roomLogs.length;

		const tabContent = hasNoContent ?
			this.getNoDataContent() :
			<div>
				{this.state.roomLogs.map(roomLog => {

					return <DashboardRoomLog
						roomLog={roomLog}
						roomsCount={this.state.roomsCount}/>
					})}
			</div>;

		return (
			<div className="col-2 tabpanel-wrapper">
				<div role='tabpanel' className='tab-section'>
					<DashboardTabs active="roomlog"/>
					<div className='tab-content'>
						<div role='tabpanel' className='active' id='roomlog'>
							{tabContent}
						</div>
					</div>
				</div>
			</div>)
	}
}
if (typeof module !== 'undefined')
	module.exports = DashboardRoomLogs;

/**
 * Renders component in the view.
 */
function render() {
	ReactDOM.render(<DashboardRoomLogs />, document.getElementById('view'));
}

render();
