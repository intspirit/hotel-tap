'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    LineChart = window['react-chartjs'].Line;

/**
 *
 */

var DashboardRoomLog = function (_React$Component) {
    _inherits(DashboardRoomLog, _React$Component);

    function DashboardRoomLog(props) {
        _classCallCheck(this, DashboardRoomLog);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DashboardRoomLog).call(this, props));

        var availableMonths = _.range(1, 13);
        _this.state = {
            data: [],
            roomsCount: props.roomsCount,
            targetCount: props.roomsCount,
            roomLog: props.roomLog,
            availableMonths: availableMonths,
            activeYear: moment().format('YYYY'),
            targetPeriods: [{ value: 12, label: 'Year' }, { value: 6, label: 'Six Months' }, { value: 4, label: 'Four Months' }, { value: 3, label: 'Quarter' }, { value: 2, label: 'Other Month' }, { value: 1, label: 'Month' }],
            targetPeriod: 1
        };
        roomLogService.getFilters(props.roomLog.roomLogId, 'targetPeriod').then(function (x) {
            _this.updateTarget(x || 1);
        });
        return _this;
    }

    _createClass(DashboardRoomLog, [{
        key: 'prepareData',
        value: function prepareData(rawData) {
            var _this2 = this;

            var months = {};
            var sumYTD = 0;

            rawData = rawData.map(function (x) {
                return {
                    'monthNumber': x.month,
                    'month': moment().month(x.month - 1).format('MMM'),
                    'completed': x.roomCount,
                    'percentage': Math.round(x.roomCount / _this2.state.roomsCount * 100),
                    'roomsCount': _this2.state.roomsCount
                };
            });

            return this.state.availableMonths.map(function (x, i) {
                months[i] = x;
                return rawData.filter(function (y) {
                    return y.monthNumber == x;
                });
            }).map(function (x, i) {
                var data = x[0] || {
                    'monthNumber': months[i],
                    'month': moment().month(months[i] - 1).format('MMM'),
                    'completed': 0,
                    'percentage': 0,
                    'roomsCount': _this2.state.roomsCount
                };
                sumYTD = parseInt(sumYTD) + parseInt(data.completed);
                data.averageYTD = Math.round(sumYTD / data.monthNumber);

                return data;
            });
        }
    }, {
        key: 'getData',
        value: function getData() {
            var _this3 = this;

            return roomLogService.getTotalCompletedForYear(this.state.roomLog.roomLogId, this.state.activeYear).then(function (rawData) {
                var data = _this3.prepareData(rawData);
                _this3.setState({ data: data });
            });
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this4 = this;

            this.getData().then(function (x) {
                var legend = _this4.refs.chart.getChart().generateLegend();
                _this4.setState({ legend: legend });
            });
        }
    }, {
        key: 'getChartDatasets',
        value: function getChartDatasets() {
            var _this5 = this;

            return [{
                name: 'Total Rooms',
                color: 'rgba(226,170,111,1)',
                fillColor: 'rgba(226,170,111,0.2)',
                data: this.state.availableMonths.map(function () {
                    return _this5.state.roomsCount;
                })
            }, {
                name: 'Completed',
                color: 'rgba(161,204,143,1)',
                fillColor: 'rgba(161,204,143,0.2)',
                data: this.state.data.map(function (x) {
                    return x.completed;
                })
            }, {
                name: 'Average YTD',
                color: 'rgba(151,187,205,1)',
                fillColor: 'rgba(151,187,205,0.2)',
                data: this.state.data.map(function (x) {
                    return x.averageYTD;
                })
            }, {
                name: 'Target',
                color: 'rgba(205,186,151,1)',
                fillColor: 'rgba(205,186,151,0.2)',
                data: this.state.availableMonths.map(function () {
                    return _this5.state.targetCount;
                })
            }];
        }
    }, {
        key: 'onSelectYear',
        value: function onSelectYear(event) {
            var year = event.target.value;
            this.setState({ activeYear: year }, this.getData);
        }
    }, {
        key: 'onSelectTargetPeriod',
        value: function onSelectTargetPeriod(event) {
            var targetPeriod = event.target.value;
            this.updateTarget(targetPeriod);
            roomLogService.setFilters(this.state.roomLog.roomLogId, { targetPeriod: targetPeriod });
        }
    }, {
        key: 'updateTarget',
        value: function updateTarget(targetPeriod) {
            this.setState({ targetPeriod: targetPeriod });
            this.setState({ targetCount: Math.round((this.state.roomsCount || 0) / targetPeriod) });
        }
    }, {
        key: 'getFilterComponent',
        value: function getFilterComponent() {
            var _this6 = this;

            return React.createElement(
                Row,
                null,
                React.createElement(
                    Col,
                    { xs: 12 },
                    React.createElement(
                        Input,
                        {
                            type: 'select',
                            label: 'Year: ',
                            placeholder: 'select',
                            groupClassName: 'filter-component-wrapper',
                            wrapperClassName: 'filter-input',
                            labelClassName: 'filter-label',
                            onChange: this.onSelectYear.bind(this) },
                        this.state.roomLog.years.map(function (x) {
                            return React.createElement(
                                'option',
                                { value: x, selected: _this6.state.activeYear == x ? 'selected' : '' },
                                x
                            );
                        }),
                        ';'
                    ),
                    React.createElement(
                        Input,
                        {
                            type: 'select',
                            label: 'All rooms to be completed once every: ',
                            placeholder: 'select',
                            groupClassName: 'filter-component-wrapper',
                            wrapperClassName: 'filter-input',
                            labelClassName: 'filter-label',
                            onChange: this.onSelectTargetPeriod.bind(this) },
                        this.state.targetPeriods.map(function (x) {
                            return React.createElement(
                                'option',
                                { value: x.value, selected: _this6.state.targetPeriod == x.value ? 'selected' : '' },
                                x.label
                            );
                        }),
                        ';'
                    )
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            if (!this.state.data.length) {
                return false;
            };
            var chartData = {
                labels: this.state.availableMonths.map(function (x) {
                    return moment().month(x - 1).format('MMM');
                }),
                datasets: this.getChartDatasets().map(function (x) {
                    return {
                        label: x.name,
                        fillColor: x.fillColor,
                        strokeColor: x.color,
                        pointColor: x.color,
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: x.color,
                        data: x.data
                    };
                })
            };
            var chartOptions = {
                multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>"
            };

            return React.createElement(
                Row,
                { className: 'roomlog' },
                React.createElement(
                    'h2',
                    null,
                    this.state.roomLog.name
                ),
                this.getFilterComponent(),
                React.createElement(
                    'div',
                    { className: 'roomlog-chart-wrapper' },
                    React.createElement(LineChart, { ref: 'chart', data: chartData, options: chartOptions, className: 'roomlog-chart' }),
                    React.createElement('div', { className: 'line-legend-wrapper', dangerouslySetInnerHTML: { __html: this.state.legend } })
                ),
                React.createElement(
                    'div',
                    { className: 'room-log-table' },
                    React.createElement(
                        Row,
                        { className: 'roomlog-header' },
                        React.createElement(
                            Col,
                            { xs: 2 },
                            'Month'
                        ),
                        React.createElement(
                            Col,
                            { xs: 2 },
                            'Total Rooms'
                        ),
                        React.createElement(
                            Col,
                            { xs: 2 },
                            'Completed'
                        ),
                        React.createElement(
                            Col,
                            { xs: 2 },
                            'Percentage'
                        ),
                        React.createElement(
                            Col,
                            { xs: 2 },
                            'Average YTD'
                        )
                    ),
                    this.state.data.map(function (item) {
                        return React.createElement(
                            Row,
                            null,
                            React.createElement(
                                Col,
                                { xs: 2 },
                                item.month
                            ),
                            React.createElement(
                                Col,
                                { xs: 2 },
                                item.roomsCount
                            ),
                            React.createElement(
                                Col,
                                { xs: 2 },
                                item.completed
                            ),
                            React.createElement(
                                Col,
                                { xs: 2 },
                                item.percentage,
                                '%'
                            ),
                            React.createElement(
                                Col,
                                { xs: 2 },
                                item.averageYTD
                            )
                        );
                    })
                )
            );
        }
    }]);

    return DashboardRoomLog;
}(React.Component);
//# sourceMappingURL=DashboardRoomLog.js.map