const
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    LineChart = window['react-chartjs'].Line;

/**
 *
 */
class DashboardRoomLog extends React.Component {
    constructor(props) {
        super(props);

        const availableMonths = _.range(1,13);
        this.state = {
            data: [],
            roomsCount: props.roomsCount,
            targetCount: props.roomsCount,
            roomLog: props.roomLog,
            availableMonths: availableMonths,
            activeYear: moment().format('YYYY'),
            targetPeriods: [
                {value: 12, label: 'Year'},
                {value: 6, label: 'Six Months'},
                {value: 4, label: 'Four Months'},
                {value: 3, label: 'Quarter'},
                {value: 2, label: 'Other Month'},
                {value: 1, label: 'Month'}
            ],
            targetPeriod: 1
        };
        roomLogService.getFilters(props.roomLog.roomLogId, 'targetPeriod')
        .then(x => {
            this.updateTarget(x || 1);
        });
    }

    prepareData(rawData) {
        const months = {};
        let sumYTD = 0;

        rawData = rawData.map(x => {
            return {
                'monthNumber': x.month,
                'month': moment().month(x.month - 1).format('MMM'),
                'completed': x.roomCount,
                'percentage': Math.round(x.roomCount / this.state.roomsCount * 100),
                'roomsCount': this.state.roomsCount
            };
        })

        return this.state.availableMonths.map((x,i) => {
            months[i] = x;
            return rawData.filter(y => {
                return y.monthNumber == x;
            });
        }).map((x,i) => {
            let data = x[0] || {
                'monthNumber': months[i],
                'month': moment().month(months[i] - 1).format('MMM'),
                'completed': 0,
                'percentage': 0,
                'roomsCount': this.state.roomsCount
            };
            sumYTD = parseInt(sumYTD) + parseInt(data.completed);
            data.averageYTD = Math.round(sumYTD / data.monthNumber);

            return data;
        });
    }

    getData() {
        return roomLogService.getTotalCompletedForYear(this.state.roomLog.roomLogId, this.state.activeYear)
        .then(rawData => {
            let data = this.prepareData(rawData);
            this.setState({data: data});
        });
    }

    componentDidMount() {
        this.getData().then(x => {
            const legend = this.refs.chart.getChart().generateLegend();
            this.setState({legend: legend});
        });
    }

    getChartDatasets() {
        return [
            {
                name: 'Total Rooms',
                color: 'rgba(226,170,111,1)',
                fillColor: 'rgba(226,170,111,0.2)',
                data: this.state.availableMonths.map(() => {
                    return this.state.roomsCount;
                })
            },
            {
                name: 'Completed',
                color: 'rgba(161,204,143,1)',
                fillColor: 'rgba(161,204,143,0.2)',
                data: this.state.data.map(x => {
                    return x.completed;
                })
            },
            {
                name: 'Average YTD',
                color: 'rgba(151,187,205,1)',
                fillColor: 'rgba(151,187,205,0.2)',
                data: this.state.data.map(x => {
                    return x.averageYTD;
                })
            },
            {
                name: 'Target',
                color: 'rgba(205,186,151,1)',
                fillColor: 'rgba(205,186,151,0.2)',
                data: this.state.availableMonths.map(() => {
                    return this.state.targetCount;
                })
            }
        ];
    }

    onSelectYear(event) {
        let year = event.target.value;
        this.setState({activeYear: year}, this.getData);
    }

    onSelectTargetPeriod(event) {
        let targetPeriod = event.target.value;
        this.updateTarget(targetPeriod);
        roomLogService.setFilters(this.state.roomLog.roomLogId, {targetPeriod: targetPeriod});
    }

    updateTarget(targetPeriod) {
        this.setState({targetPeriod: targetPeriod});
        this.setState({targetCount: Math.round((this.state.roomsCount || 0) / targetPeriod)});
    }

    getFilterComponent() {
        return <Row>
            <Col xs={12}>
                <Input
                    type="select"
                    label="Year: "
                    placeholder="select"
                    groupClassName="filter-component-wrapper"
                    wrapperClassName="filter-input"
                    labelClassName="filter-label"
                    onChange={this.onSelectYear.bind(this)}>
                    {this.state.roomLog.years.map(x => {
                        return <option value={x} selected={this.state.activeYear == x ? 'selected' : ''}>{x}</option>
                        })};
                </Input>
                <Input
                    type="select"
                    label="All rooms to be completed once every: "
                    placeholder="select"
                    groupClassName="filter-component-wrapper"
                    wrapperClassName="filter-input"
                    labelClassName="filter-label"
                    onChange={this.onSelectTargetPeriod.bind(this)}>
                    {this.state.targetPeriods.map(x => {
                        return <option value={x.value} selected={this.state.targetPeriod == x.value ? 'selected' : ''}>
                            {x.label}
                        </option>
                    })};
                </Input>
            </Col>
        </Row>
    }

    render() {
        if (!this.state.data.length) {return false};
        const chartData = {
            labels: this.state.availableMonths.map((x) => {
                return moment().month(x - 1).format('MMM');
            }),
            datasets: this.getChartDatasets().map(x => {
                return {
                    label: x.name,
                    fillColor: x.fillColor,
                    strokeColor: x.color,
                    pointColor: x.color,
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: x.color,
                    data: x.data
                }
            })
        };
        const chartOptions = {
            multiTooltipTemplate: "<%= datasetLabel %>: <%= value %>"
        };

        return <Row className="roomlog">
            <h2>
                {this.state.roomLog.name}
            </h2>
            {this.getFilterComponent()}
            <div className="roomlog-chart-wrapper">
                <LineChart ref="chart" data={chartData} options={chartOptions} className="roomlog-chart"/>
                <div className="line-legend-wrapper" dangerouslySetInnerHTML={{ __html: this.state.legend }} />
            </div>
            <div className="room-log-table">
                <Row className="roomlog-header">
                    <Col xs={2}>Month</Col>
                    <Col xs={2}>Total Rooms</Col>
                    <Col xs={2}>Completed</Col>
                    <Col xs={2}>Percentage</Col>
                    <Col xs={2}>Average YTD</Col>
                </Row>
                {this.state.data.map(item => {
                    return <Row>
                        <Col xs={2}>{item.month}</Col>
                        <Col xs={2}>{item.roomsCount}</Col>
                        <Col xs={2}>{item.completed}</Col>
                        <Col xs={2}>{item.percentage}%</Col>
                        <Col xs={2}>{item.averageYTD}</Col>
                    </Row>
                })}
            </div>
        </Row>
    }
}
