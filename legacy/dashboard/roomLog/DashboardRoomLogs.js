"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = ReactBootstrap.Input,
    Row = ReactBootstrap.Row,
    Col = ReactBootstrap.Col,
    Image = ReactBootstrap.Image;

/**
 * Dashboard Tasks component.
 */

var DashboardRoomLogs = function (_React$Component) {
	_inherits(DashboardRoomLogs, _React$Component);

	function DashboardRoomLogs(params) {
		_classCallCheck(this, DashboardRoomLogs);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DashboardRoomLogs).call(this, params));

		_this.state = {
			roomLogs: []
		};
		_this.dataLoaded = false;
		return _this;
	}

	_createClass(DashboardRoomLogs, [{
		key: "getViewModel",
		value: function getViewModel(logsAndYears) {
			var roomLogsData = logsAndYears[0] || {};
			var years = logsAndYears[1] || {};
			return roomLogsData.roomLogs.map(function (roomLog) {
				roomLog.years = years[roomLog.roomLogId] || [];
				return roomLog;
			});
		}
	}, {
		key: "getData",
		value: function getData() {
			var _this2 = this;

			Promise.all([roomLogService.getAll(), roomLogService.getYears()]).then(function (logsAndYears) {
				_this2.dataLoaded = true;
				var viewModel = _this2.getViewModel(logsAndYears);
				_this2.setState({
					roomsCount: logsAndYears[0].roomsCount,
					roomLogs: viewModel
				});
			});
		}
	}, {
		key: "componentDidMount",
		value: function componentDidMount() {
			this.getData();
		}
	}, {
		key: "getNoDataContent",
		value: function getNoDataContent() {
			return this.dataLoaded ? React.createElement(
				"div",
				{ id: "noData", className: "text-center" },
				React.createElement(Image, { src: "/images/logo.png" }),
				React.createElement(
					"p",
					null,
					"Please create Room Logs for recurring guest room tasks such as",
					React.createElement("br", null),
					"- Preventive Maintenance",
					React.createElement("br", null),
					"- Housekeeping Deep Cleaning",
					React.createElement("br", null),
					"- Pest Control",
					React.createElement("br", null),
					"- Safety Inspections, etc",
					React.createElement("br", null),
					React.createElement("br", null),
					"from the",
					React.createElement(
						"a",
						{ href: "/admin/roomlogs" },
						" Admin Settings "
					),
					"Menu or call HotelTap support at ",
					React.createElement(
						"a",
						{ href: "tel:8443817221" },
						"844-381-7221"
					)
				)
			) : React.createElement(
				"div",
				{ className: "text-center" },
				React.createElement("i", { className: "fa fa-spinner fa-spin", style: { fontSize: '96px;', marginTop: '40px' } })
			);
		}
	}, {
		key: "render",
		value: function render() {
			var _this3 = this;

			var hasNoContent = !this.state.roomLogs.length;

			var tabContent = hasNoContent ? this.getNoDataContent() : React.createElement(
				"div",
				null,
				this.state.roomLogs.map(function (roomLog) {

					return React.createElement(DashboardRoomLog, {
						roomLog: roomLog,
						roomsCount: _this3.state.roomsCount });
				})
			);

			return React.createElement(
				"div",
				{ className: "col-2 tabpanel-wrapper" },
				React.createElement(
					"div",
					{ role: "tabpanel", className: "tab-section" },
					React.createElement(DashboardTabs, { active: "roomlog" }),
					React.createElement(
						"div",
						{ className: "tab-content" },
						React.createElement(
							"div",
							{ role: "tabpanel", className: "active", id: "roomlog" },
							tabContent
						)
					)
				)
			);
		}
	}]);

	return DashboardRoomLogs;
}(React.Component);

if (typeof module !== 'undefined') module.exports = DashboardRoomLogs;

/**
 * Renders component in the view.
 */
function render() {
	ReactDOM.render(React.createElement(DashboardRoomLogs, null), document.getElementById('view'));
}

render();

//# sourceMappingURL=DashboardRoomLogs.js.map