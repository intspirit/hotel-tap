var HotelTap = HotelTap || {};

(function () {
	'use strict';

	var API_LIST_URL = '/api/roomlogs/';
	var LOG_EDIT_URL = '/admin/roomlogs/';

	/**
	 * List of room logs.
	 */
	var LogList = React.createClass({

		getInitialState: function () {
			return {
				logs: []
			}
		},

		loadCommentsFromServer: function () {
			$.ajax({
				url: API_LIST_URL,
				dataType: 'json',
				cache: false,
				success: function (data) {
					this.setState({logs: data});
				}.bind(this),
				error: function (xhr, status, err) {
					//console.error(API_LIST_URL, status, err.toString());
				}.bind(this)
			});
		},

		componentDidMount: function () {
			this.loadCommentsFromServer();
		},

		render: function() {
			return (
				<section className="content">
					<LogRow items={this.state.logs}/>
				</section>
			);
		}
	});

	/**
	 * Room Log list row.
	 */
	var LogRow = React.createClass({
		render: function () {
			var rows = this.props.items.map((log) => {
				var url = LOG_EDIT_URL + log.id;
				return <tr key={log.id}>
					<td>{log.name}</td>
					<td>
						<a href={url} title="Edit" className="glyphicon glyphicon-edit green"/>
					</td>

				</tr>
			});

			return (
				<table className="table table-bordered">
					<thead>
					<tr className="tblheading">
						<th>Log Name</th>
						<th width="10%">Options</th>
					</tr>
					</thead>
					<tbody>
					{rows}
					</tbody>
				</table>
			)
		}
	});

	/**
	 * Room Logs list view.
	 */
	var LogView = React.createClass({
		render: function () {
			return (
				<aside id="roomLogsView" className="right-side">
					<AdminViewHeader url="/admin/roomlogs/create" buttonLabel="Add Log" header="Room Logs"/>
					<LogList/>
				</aside>
			);
		}
	});



	function render() {
		ReactDOM.render(<LogView/>, document.getElementById('view'));
	}

	render();
})();






