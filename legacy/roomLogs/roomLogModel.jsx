var HotelTap = HotelTap || {};

(function () {
	'use strict';

	/**
	 * Name model.
	 * @param name
	 * @param description
	 * @param nameHint
	 * @param descriptionHint
	 * @constructor
	 */
	HotelTap.NameDescriptionModel = function (name, description, nameHint, descriptionHint) {
		this.name = name;
		this.description = description;
		this.nameHint = nameHint;
		this.descriptionHint = descriptionHint;
	};

	/**
	 * Assignment model.
	 * @constructor
	 */
	HotelTap.AssignmentModel = function (data) {
		this.assignmentType = data.assignmentType;
		this.assigneeId = data.assigneeId;
		this.employeeId = -1;
		this.departmentId = -1;
		switch (data.assignmentType) {
			case 'e':
				this.employeeId = data.assigneeId;
				break;
			case 'd':
				this.departmentId = data.assigneeId;
				break;
		}

		this.employees = data.employees.map(x => {
			return {
				id: x.userId,
				name: x.fullName
			}
		});
		this.departments = data.departments.map(x => {
			return {
				id: x.departmentId,
				imageUrl: x.departmentImage,
				name: x.departmentName
			}
		});

		/**
		 * Sets assignee based on the whether employee or department is assigned.
		 */
		this.setAssignee = function () {
			if (this.employeeId === -1) {
				this.assigneeId = this.departmentId;
				this.assignmentType = 'd';
			} else {
				this.assigneeId = this.employeeId;
				this.assignmentType = 'e';
			}
		}
	};


	HotelTap.RoomModel = function (id, name) {
		this.id = id;
		this.name = name;
	};

	/**
	 * Checklist task model.
	 * @param id
	 * @param subject
	 * @constructor
	 */
	HotelTap.ChecklistTaskModel = function (id, subject) {
		this.key = subject;
		this.id = id;
		this.subject = subject;
	};

	HotelTap.ScheduleModel = function (id, roomId, year, month, day, dateTime, completionDateTime) {
		this.id = id;
		this.roomId = roomId;
		this.year = year;
		this.month = month;
		this.day = day;
		this.dateTime = dateTime;
		if (completionDateTime) {
			this.completionDateTime = new Date(completionDateTime);
			this.completionMonth = this.completionDateTime.getMonth() + 1;
			this.completionDate = this.completionDateTime.getDate();
			this.completionYear = this.completionDateTime.getFullYear();
			this.completed = true;
			this.completedDay = this.completionDate;

		} else {
			this.completed = false;
		}
	};

	/**
	 * View model for the schedules table.
	 * @param year
	 * @param rooms
	 * @param schedules
	 * @constructor
	 */
	HotelTap.ScheduleViewModel = function (year, rooms, schedules) {
		this.rows = [];
		this.year = year;

		var that = this;
		var row = {};
		var newId = -1;
		rooms.map(room => {
			row = {
				id: room.id,
				name: room.name,
				columns: []
			};
			for (let i = 1; i <= 12; i++) {
				let cellValue = '',
					cellId = newId,
					completed = false,
					lastMonthCompletionDay = null,
					lastMonthScheduleId = -1;

				if (schedules.length > 0) {
					schedules.forEach(schedule => {
						if (schedule.completed && schedule.completionYear != that.year) return;

						var month = schedule.completed ? schedule.completionMonth : schedule.month;
						if (schedule.roomId == room.id && month == i) {
							if (schedule.completed && schedule.completionMonth != schedule.month) {
								lastMonthCompletionDay = schedule.completedDay;
								lastMonthScheduleId = schedule.id;
							} else {
								cellValue = schedule.completed ? schedule.completedDay : schedule.day;
								cellId = schedule.id;
								completed = schedule.completed;
							}
						}
					});
				}

				row.columns[i-1] = {
					id: cellId,
					roomId: row.id,
					month: i,
					value: cellValue,
					oldValue: cellValue,
					completed: completed,
					lastMonthCompletionDay: lastMonthCompletionDay,
					lastMonthScheduleId: lastMonthScheduleId
				};
				newId -= 1;
			}
			that.rows.push(row);
		});
	};

	/**
	 * Room Log model.
	 * @param data
	 * @constructor
	 */
	HotelTap.RoomLogModel = function (data) {
		this.key = 'rooLog';

		this.id = data.id;
		this.nameDescription = new HotelTap.NameDescriptionModel(data.name, data.description, 'Log Name', 'Describe the log');
		this.assignment = new HotelTap.AssignmentModel(data);
		this.rooms = data.rooms;

		this.tasks = data.tasks || [];
		this.year = (new Date()).getFullYear();
		this.schedules = data.schedules.map(x => {
			return new HotelTap.ScheduleModel(
				x.id, x.roomId, x.dueYear, x.dueMonth, x.dueDay, x.dueDateTime, x.completionDateTime);
		});

		/**
		 * Get the POST request to send to server.
		 * @returns {{}}
		 */
		this.getSaveRequest = function () {
			this.assignment.setAssignee();

			var result = {
				id: this.id,
				name: this.nameDescription.name,
				description: this.nameDescription.description,
				assignmentType: this.assignment.assignmentType,
				assigneeId: this.assignment.assigneeId,

				tasks: [],
				schedules: []
			};

			var order = 0;
			this.tasks.forEach(x => {
				if (!x.changeStatus) return;

				var task = {
					id: x.changeStatus === 'i' ? -1 : x.id,
					subject: x.subject,
					description: '',
					order: order,
					changeStatus: x.changeStatus
				};
				result.tasks.push(task);
				order += 1;
			});

			this.schedules.forEach(x => {
				if (!x.changeStatus) return;

				var schedule = {
					id: x.changeStatus === 'i' ? -1 : x.id,
					roomId: x.roomId,
					dueYear: x.year,
					dueMonth: x.month,
					dueDay: x.day,
					dueDateTime: new Date(x.year, x.month - 1, x.day, 23, 59, 59),
					changeStatus: x.changeStatus
				};
				result.schedules.push(schedule);
			});

			return result;
		}
	};
})();
