//import _ from "lodash";
var HotelTap = HotelTap || {};

(function () {
	'use strict';

	var ScheduleCell = HotelTap.ScheduleCell;
	/**
	 *
	 * @type {__React.ClassicComponentClass<P>}
	 */
	HotelTap.RoomSchedule = React.createClass({
		getInitialState: function() {
			return {
				year: (new Date()).getFullYear(),
				schedules: this.props.schedule,
				rooms: this.props.rooms.sort(naturalSort({key: 'name'}))
			}
		},

		componentDidMount: function() {
			this.setState({
				schedules: this.props.schedule,
				rooms: this.props.rooms.sort(naturalSort({key: 'name'}))
			});
		},

		setNewValue: function(roomId, month, value) {
			var existing = false;
			var schedules = this.state.schedules;
			for (var i = schedules.length - 1; i >= 0; i--) {
				var schedule = schedules[i];

				if (schedule.roomId == roomId && schedule.month == month) {
					existing = true;
					if (schedule.changeStatus === 'i' && value === '') {
						schedules.splice(i, 1);
					} else {
						schedule.day = value;
						schedule.changeStatus = value === '' ? 'd' : 'u';
					}
				}
			}
			if (!existing) {
				var newSchedule = new HotelTap.ScheduleModel(_.uniqueId(), roomId, this.state.year, month, value, '');
				newSchedule.changeStatus = 'i';
				schedules.push(newSchedule);
			}
			//this.props.schedule = schedules;
			this.setState({schedules: schedules});
		},

		getYearData: function(logId, year) {
			var url = '/api/roomlogs/' + logId + '/schedules/' + year;
			$.ajax({
				url: url,
				dataType: 'json',
				cache: false,
				success: function (data) {
					var schedules = data.schedules.map(function (x) {
						return new HotelTap.ScheduleModel(
							x.id, x.roomId, x.dueYear, x.dueMonth, x.dueDay, x.dueDateTime, x.completionDateTime);
					});

					//this.props.schedule = schedules;
					this.setState({
						year: year,
						schedules: schedules
					});
				}.bind(this),
				error: function (xhr, status, err) {
					//console.error(API_LIST_URL, status, err.toString());
				}.bind(this)
			});

		},

		onNextYearClick: function() {
			var maxYear = (new Date()).getFullYear() + 5;
			if (this.state.year > maxYear) return;
			var $year = this.state.year + 1;
			this.getYearData(this.props.logId, $year);
		},

		onPreviousYearClick: function() {
			if (this.state.year < 2010) return;
			var $year = this.state.year - 1;
			this.getYearData(this.props.logId, $year);
		},

		render: function () {
            var viewModel = new HotelTap.ScheduleViewModel(this.state.year, this.state.rooms, this.state.schedules);
			var that = this;

			var getRoomColumns = function (row) {
				return row.columns.map(col => {
					return <th key={col.id}>
						<ScheduleCell
							model={col}
							year={that.state.year}
							handleChange={that.setNewValue}
							handleClick={that.props.onClick}/>
					</th>;
				});
			};

			var roomRows = viewModel.rows.map(row => {
				return <tr key={row.id} id={row.id}>
					<th key={row.name}>{row.name}</th>
					{getRoomColumns(row)}
				</tr>
			});

			return (
				<div className="form-group roomlog-schedules">
					<h1>Room Schedules
						<div className="pull-right">
							<span
								onClick={this.onPreviousYearClick}
								className="glyphicon glyphicon-chevron-left leftMergin5"
								ariaHidden="true"/>
							<span className="leftMergin5">{this.state.year}</span>
							<span
								onClick={this.onNextYearClick}
								className="glyphicon glyphicon-chevron-right leftMergin5"
								ariaHidden="true"/>
						</div>
					</h1>

					<table id="roomScheduleTable" width="100%" borderWidth="0" cellSpacing="0" cellPadding="0"
					       className="table table-bordered {tableVisibleClass}">
						<thead>
						<tr className="tblheading">
							<th width="15%">Room</th>
							<th>Jan</th>
							<th>Feb</th>
							<th>Mar</th>
							<th>Apr</th>
							<th>May</th>
							<th>Jun</th>
							<th>Jul</th>
							<th>Aug</th>
							<th>Sep</th>
							<th>Oct</th>
							<th>Nov</th>
							<th>Dec</th>
						</tr>
						</thead>
						<tbody>
						{roomRows}
						</tbody>
					</table>
				</div>
			);
		}
	});
})();
