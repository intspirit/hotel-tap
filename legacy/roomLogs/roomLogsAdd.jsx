let HotelTap = HotelTap || {};

(function () {
	'use strict';

	const
		NameDescription = HotelTap.NameDescription,
		Assignment = HotelTap.Assignment,
		ChecklistTasks = HotelTap.ChecklistTasks,
		RoomSchedule = HotelTap.RoomSchedule,
		RoomLog = HotelTap.RoomLog;


	/**
	 * Room Log View.
	 * @type {__React.ClassicComponentClass<P>}
	 */
	const LogView = React.createClass({

		getInitialState: () => {
			return {
				logVisible: false,
				scheduleId: -1
			};
		},

		onSave: function () {
			if (this.props.model.nameDescription.name.trim() === '') return;
			if (this.props.model.assignment.employeeId <= 0 && this.props.model.assignment.departmentId == -1) return;

			this.props.model.schedules = this.refs.schedule.state.schedules;
			var request = this.props.model.getSaveRequest();

			$.ajax({
				url: '/api/roomlogs/',
				dataType: 'json',
				type: 'POST',
				data: request,
				success: function () {
					window.location = "/admin/roomlogs";
				}.bind(this),
				error: function (xhr, status, err) {
					console.error(status, err.toString());
				}.bind(this)
			});
		},

		getLogView: function () {
			console.log(this.state);
			if (this.refs['roomLog']) this.refs['roomLog'].open(this.state.scheduleId);
			return this.state.logVisible ? <RoomLog ref='roomLog' scheduleId={this.state.scheduleId}/> : '';
		},

		showLog: function (scheduleId) {
			this.setState({
				logVisible: true,
				scheduleId: scheduleId
			});
		},

		render: function () {
			return (
				<aside id="roomLogsView" className="right-side">
					<AdminViewHeader url="/admin/roomlogs" buttonLabel="Back to Logs" header="Add Room Log"/>
					<section className="content">
						<div className="whitecentercol">

							<NameDescription model={this.props.model.nameDescription}/>

							{this.getLogView()}

							<Assignment model={this.props.model.assignment}/>
							<div className="clearfix"/>
							<ChecklistTasks model={this.props.model.tasks}/>
							<RoomSchedule ref="schedule"
							              logId={this.props.model.id}
							              schedule={this.props.model.schedules}
							              rooms={this.props.model.rooms}
							              onClick={this.showLog}/>

							<div>
								<a href='#' className="btn btn-primary" onClick={this.onSave}>Save</a>
							</div>
						</div>
					</section>
				</aside>
			);
		}
	});

	function render() {
		const viewUrl = window.location.pathname;
		const urlParts = viewUrl.split('/');
		const url = urlParts[urlParts.length - 1] ===
		'create' ? '/api/roomlogs/add' : '/api/roomlogs/' + urlParts[urlParts.length - 1];
		$.ajax({
			url: url,
			dataType: 'json',
			cache: false,
			success: function (data) {
				var model = new HotelTap.RoomLogModel(data);
				ReactDOM.render(<LogView model={model}/>, document.getElementById('view'));
			}
		});
	}

	render();
})();


