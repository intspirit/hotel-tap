'use strict';

let HotelTap = HotelTap || {};
HotelTap.Services = HotelTap.Services || {};

HotelTap.Services.roomLog = {

	getById: (id) => {
		return $.ajax('/api/campaigns/' + id, {
			type: 'GET'
		})
	},

	//getAll: (page, limit) => {
	//	return $.ajax('/api/campaigns', {
	//		type: 'GET',
	//		data: {
	//			from: (page - 1) * limit,
	//			limit: limit
	//		}
	//	})
	//},

	getCompletedById: (id) => {
		return $.ajax('/api/campaigns/' + id, {
			type: 'GET'
		})
	}
};


