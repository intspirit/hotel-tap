'use strict';

let HotelTap = HotelTap || {};
const
	Modal = ReactBootstrap.Modal,
	Button = ReactBootstrap.Button;

/**
 * Single room log view.
 */
HotelTap.RoomLog = class RoomLog extends React.Component {
	constructor() {
		super();
		this.state = {
			showModal: true,
			viewModel: {
				tasks: []
			}
		};
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.print = this.print.bind(this);
	}

	getData(scheduleId) {
		if (!this.state.showModal) return;

		$.ajax({
			url: `/api/schedules/${scheduleId}`,
			dataType: 'json',
			type: 'GET',
			success: function (data) {
				const viewModel = {
					id: data.roomLog.id,
					logName: data.roomLog.name,
					roomNumber: data.roomName,
					dueDate: data.dueDateTime.split(' ')[0],
					completionDateTime: data.completionDateTime,
					tasks: data.tasks.map(x => {
						return {
							id: x.id,
							subject: x.subject,
							description: x.description,
							completed: true,
							completedText: x.completed,
							completionDateTime: x.completionDateTime,
							userPicture: x.profile_picture,
							comments: x.comments.map(y => {
								return {
									id: y.comment_id,
									employee: y.author,
									userPicture: y.profile_picture,
									dateTime: y.created_date,
									text: y.comment,
									attachments: y.attachments
								}
							})
						}
					})

				};
				this.setState({
					showModal: true,
					viewModel: viewModel
				});
			}.bind(this),
			error: function (xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			}.bind(this)
		});
	}

	//noinspection JSUnusedGlobalSymbols
	componentDidMount() {
		this.getData(this.props.scheduleId);
	}

	close() {
		this.setState({showModal: false});
	}

	open(scheduleId) {
		this.state.showModal = true;
		//this.setState({showModal: true});
		this.getData(scheduleId);
	}

	getTasks() {
		const result = this.state.viewModel.id ?
			this.state.viewModel.tasks.map(x => <HotelTap.TaskListItem task={x} key={x.id}/>) :
			'';
		return result;
	}

	print() {
		this.printElement(document.getElementById("printIt"));
		window.print();
	}

	printElement(elem, append, delimiter) {
		var domClone = elem.cloneNode(true);

		var $printSection = document.getElementById("printSection");

		if (!$printSection) {
			var $printSection = document.createElement("div");
			$printSection.id = "printSection";
			document.body.appendChild($printSection);
		}

		if (append !== true) {
			$printSection.innerHTML = "";
		}

		else if (append === true) {
			if (typeof(delimiter) === "string") {
				$printSection.innerHTML += delimiter;
			}
			else if (typeof(delimiter) === "object") {
				$printSection.appendChlid(delimiter);
			}
		}

		$printSection.appendChild(domClone);
	}

	render() {
		const model = this.state.viewModel;

		return <div>
			{/*<Button bsStyle="primary" bsSize="large" onClick={this.open}>Launch demo modal</Button>*/}

			<Modal show={this.state.showModal} onHide={this.close}>
				<div id="printIt" className="completedLogView">
					<Modal.Header closeButton>
						<Modal.Title>{model.logName} - {model.roomNumber}</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<div className="list-dates">
							<div className="due-date">
								<span className="date-label">Due date:</span>
								<span className="date-value">{moment(model.dueDate).format('MMM D, YYYY')}</span>
							</div>
							<div className="completion-date">
								<span className="date-label">Completed on:</span>
								<span className="date-value">{moment(model.completionDateTime).format('MMM D, YYYY')}</span>
							</div>
							<div className="clearfix"></div>
						</div>
						<div className="tasks-list">
							{this.getTasks()}
						</div>
					</Modal.Body>

					<Modal.Footer>
						<Button bsStyle="primary" className="btn-print" onClick={this.print}>Print</Button>
					</Modal.Footer>
				</div>

			</Modal>
		</div>
			;
	}
};

