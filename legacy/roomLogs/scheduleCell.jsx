var HotelTap = HotelTap || {};

(function () {

	let today = new Date();
	today.setHours(0);
	today.setMinutes(0);
	today.setSeconds(0);
	const validDays = _.range(1, 32).map(x => { return '' + x});

	//TODO: vvs p3 reimplement
	function invalid (year, month, value) {
		if (value === '') return false;

		if (validDays.indexOf(value) === -1) return true;
		const day = parseInt(value);
		const date = new Date(year, month - 1, day);
		if (date.getDate() !== day) return true;
	}

	/**
	 * Schedule table cell.
	 * @type {ClassicComponentClass<P>}
	 */
	HotelTap.ScheduleCell = React.createClass({
		getInitialState: function () {
			return {
				value: this.props.model.value,
				value: this.props.model.value,
				oldValue: ''
			}
		},

		onFocus: function(event) {
			this.setState({oldValue: event.target.value});
		},

		onBlur: function(event) {
			const newValue = event.target.value.trim();
			if (this.state.oldValue == newValue) return;

			//noinspection JSUnresolvedFunction
			this.props.handleChange(this.props.model.roomId, this.props.model.month, newValue);
		},

		onChange: function (event) {
			const value = event.target.value.trim();
			if (invalid(this.props.year, this.props.model.month, value)) return;

			this.setState({value: value});
		},

		onCompleteClick: function () {
			this.props.handleClick(this.props.model.lastMonthScheduleId);
		},
		onCompleteClick1: function () {
			this.props.handleClick(this.props.model.id);
		},

		render: function () {
			let isOverdue = false;

			if (this.state.value !== '') {
				const dateTime = new Date(this.props.year, this.props.model.month - 1, this.state.value);
				dateTime.setHours(23);
				isOverdue = dateTime < today;
			}

			const getCompletionDay =  this.props.model.lastMonthCompletionDay ?
				<span className="done" onClick={this.onCompleteClick}>{this.props.model.lastMonthCompletionDay}</span> :
				'';

			const input = this.props.model.completed ?
				<span className="done" onClick={this.onCompleteClick1}>{this.props.model.value}</span> :
				<input
					className={classNames('transparentInput', 'cell', {overdue: isOverdue})}
					key={this.props.model.id}
					id={this.props.model.id}
					onBlur={this.onBlur}
					onFocus={this.onFocus}
					readOnly={this.props.model.completed}
					onChange={this.onChange}
					value={this.state.value}/>;

			return <div>
				{getCompletionDay}
				{input}
			</div>
		}
	});
})();
