# HotelTap Web Node.js version
The main HotelTap web application based on Node.js.

## Getting Started

Please check the [Enginering Team Wiki](https://github.com/sandipjariwala/hoteltap-wiki-eng/wiki).

### Quick Start Development
Make sure that Node.js is installed.

1. Download the latest version of the code
2. Fire up a terminal
3. `npm install`
4. `bower install`
5. Start the Webpack in a watch mode
	- `npm run webpack-watch`
6. Fire up another terminal
7. Start Node.js server
	- `npm start`
8. `http://localhost:9006`

### Software Versions
- Node.js - 4.3.0
- npm -3.8.6

The following `npm` packages are included in the `node_modules`,  but you may also conside insalling them locally:

- `webpack`
- `mocha`
- `babel`

## Development

### Check for new versions of the npm packages

Resource: [npm-check-updates](https://www.npmjs.com/package/npm-check-updates)

Run:

    ncu
    ncu -u



## Design

### URLs

#### Views
| URL  | Component  | Notes  |
|---|---|---|
| /          | Layout/Main  | -> /boards/me or /boards/departments/:id based on admin or employee  |
| /boars/me  | Layout/Employee  |   |
| /boards/departments/_:id_  | Layout/Department  |   |
| /inspections  |   |   |
| /room-logs  |   |   |
| /checklists  |   |   |

#### API
|  URL      | HTTP Method | Controller  | Method  | Notes  |
|----------|--------------|-------------|---------|--------|
|  /hotels | GET  |   |   | Default or last used  |
|  /users/_:id_/nav | GET  |   |   |   |
|   |   |   |   |   |


## Migration to `posts` tables

This is a temporary instructions of how to migrate `notes`, `tasks`, ... tables to the new set of `post*` tables.

The migration script doesn't work incrementally, so:
1. In the `legacy-migration-notes.sql` file execute `TRUNCATE ...` statements (at the top of the script).
2. Execute all statements in the `legacy-migration-notes.sql` file.
2. Execute all statements in the `legacy-migration-tasks.sql` file.

These steps will migrate notes and tasks with their comment from the old tables to the new ones.

## Testing

### Webstorm testing settings

Node interpreter: `/usr/local/bin/node`
Node options: `--harmony`
Working directory: `~/Dev/Business/HotelWorks/HotelTap_Web `
Environment variables: `BABEL_ENV=mobile`
Mocha package: `~/Dev/Business/HotelWorks/HotelTap_Web/node_modules/mocha`
Extra Mocha options: `--require ./tests/mocha-babel.js --presets es2015`

## Deployment

### Staging Deployment

#### Manual

Deployment to the staging server should be done from the `dev/main` or `master` branches in most case.

Deployment steps:

- [ ] Get the latest code
- [ ] Make sure that you have `build` folder in the project root
- [ ] Hack: remove `react-native-*` packages from `package.json` (don't forget to put them back after deployment)
- [ ] Execute the `npm run build` command (this will create a deployable version of the app in the `build` folder)
- [ ] Navigate to the `build` folder
- [ ] Execute AWS deployment by running `eb deploy` command from the `build` folder


#### Automated by SemaphoreCI

Deployment steps:

- [ ] merge `dev/main` branch into `dev/deployment`
- [ ] check `#operation` Slack channel

