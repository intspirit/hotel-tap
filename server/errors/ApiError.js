'use strict'

/**
 * Generic API error.
 * @param {string} message - error message that will be logged in the app log.
 * @param {int} statusCode - code that will be returned to the user as a result of the API call (in most cases - HTTP
 * code).
 * @param {int} code - extra code that have app details (e.g. statusCode = 500, code = 1000 - db error).
 * @param {string} userMessage - messages that will be shown to a user.
 * @param {string} [reason] - detailed reason for the error (e.g. statusCode=401, reason=invalidCredentials.
 * @param {string|json|object} [meta] - error metadata.
 * @return {object}
 */
function ApiError(message, statusCode, code, userMessage, reason, meta) {
	this.message = message
	this.stack = new Error().stack
	this.errorType = this.name
	this.statusCode = statusCode
	this.code = code
	this.userMessage = userMessage
	this.meta = meta
	this.reason = reason
}

ApiError.prototype = Object.create(Error.prototype)
ApiError.prototype.name = 'ApiError'

export default ApiError
