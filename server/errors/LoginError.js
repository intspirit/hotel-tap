'use strict'

import constants from '../../shared/constants'

/**
 * Login error.
 * @param {string} message - error message that will be logged in the app log.
 * @param {string|json|object} [meta] - error metadata.
 * @return {object}
 */
function LoginError(message, meta) {
	this.message = message
	this.stack = new Error().stack
	this.errorType = this.name
	this.statusCode = 401
	this.code = 401
	this.userMessage = 'Sign in has failed. Please, try again'
	this.meta = meta
	this.reason = constants.apiErrorReason.invalidCredentials
}

LoginError.prototype = Object.create(Error.prototype)
LoginError.prototype.name = 'LoginError'

export default LoginError
