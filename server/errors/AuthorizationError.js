'use strict'

import constants from '../../shared/constants'

/**
 * Authorization error.
 * @param {string} message - error message that will be logged in the app log.
 * @param {string|json|object} [meta] - error metadata.
 * @return {object}
 */
function AuthorizationError(message, meta) {
	this.message = message
	this.stack = new Error().stack
	this.errorType = this.name
	this.statusCode = 403
	this.code = 43
	this.userMessage = 'Access forbidden'
	this.meta = meta
	this.reason = constants.apiErrorReason.invalidToken
}

AuthorizationError.prototype = Object.create(Error.prototype)
AuthorizationError.prototype.name = 'AuthorizationError'

export default AuthorizationError
