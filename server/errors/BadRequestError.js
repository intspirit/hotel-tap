'use strict'

/**
 * Bad request error.
 * @param {string} message
 * @param {string|json|object} [meta] - error metadata.
 * @return {object}
 */
function BadRequestError(message, meta) {
	this.message = message
	this.stack = new Error().stack
	this.errorType = this.name
	this.statusCode = 400
	this.code = 400
	this.userMessage = 'Something went wrong. Please try again.'
	this.meta = meta
}

BadRequestError.prototype = Object.create(Error.prototype)
BadRequestError.prototype.name = 'BadRequestError'

export default BadRequestError
