'use strict'

// import logger from '../core/logger'
import BadRequestError from './BadRequestError'
import LoginError from './LoginError'
import ServerError from './ServerError'
import ApiError from './ApiError'
import AuthenticationError from './AuthenticationError'
import AuthorizationError from './AuthorizationError'

/**
 * Gets error for API response.
 * @param {Error|object} error
 * @param {int} error.statusCode
 * @param {string} error.userMessage
 * @returns {{error: {code: (int), message: (string)}}}
 */
function getResponseError(error = new Error('Unknown error')) {
	const resultError = error.statusCode ? error : new ServerError(error.message)

	return {
		error: {
			code: resultError.statusCode,
			message: resultError.userMessage,
			reason: resultError.reason || ''
		}
	}
}


//noinspection Eslint
/**
 * Logs error.
 * @param {Error|object} error
 * @constructor
 */
function logError(error) {
	return !error
	//TODO: sk p2 implement. Log stack also.
}

module.exports = {
	ApiError,
	BadRequestError,
	LoginError,
	ServerError,
	AuthenticationError,
	AuthorizationError,
	getResponseError,
	logError: logError
}
