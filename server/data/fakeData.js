'use strict'

import _ from 'lodash'
import faker from 'faker'
import models from '../models'
import constants from '../../shared/constants'
import moment from 'moment-timezone'

//noinspection JSUnusedGlobalSymbols
const fakeData = {
	getTask: function() {
		return {
			id: faker.random.number(),
			subject: faker.lorem.sentence(8),
			description: faker.lorem.sentence(),
			dueDateTime: faker.date.between('2016-06-01', '2016-06-30')
		}
	},

	getTodo: function() {
		return {
			count: 13,
			groups: [
				{id: 1, name: 'overdue', label: 'Overdue', count: 1, tasks: _.range(1).map(() => this.getTask())},
				{id: 2, name: 'today', label: 'Today', count: 1, tasks: _.range(3).map(() => this.getTask())},
				{id: 3, name: 'next', label: 'Next 3 days', count: 1, tasks: _.range(4).map(() => this.getTask())},
				{id: 4, name: 'completed', label: 'Completed', count: 1, tasks: _.range(5).map(() => this.getTask())}
			]
		}
	},

	getEmployee: function(userId, hotelId) {
		return new models.User({
			user_id: (userId || 489),
			hotel_id: (hotelId || 107),
			first_name: 'Javier',
			last_name: 'Lazo',
			email: 'employee@hoteltap.dev',
			profile_picture: 'user.jpg',
			role_id: constants.role.EMPLOYEE
		})
	},

	getAdmin: function(userId, hotelId) {
		return new models.User({
			user_id: (userId || 487),
			hotel_id: (hotelId || 107),
			first_name: 'Jessica',
			last_name: 'Lorenzo',
			email: 'admin@hoteltap.dev',
			profile_picture: 'admin.jpg',
			role_id: constants.role.ADMIN
		})
	},

	getNavigationAdmin: function() {
		// id - user or department id
		return {
			items: [
				{type: 'employee', id: 1, label: 'Mike G Gerald', url: '/employees/me', iconUrl: '/public/img/sample.png'},
				{type: 'general', id: -1, label: 'Settings', url: '/users/me/setting', iconClass: 'fa-wrench'},
				{type: 'department', id: 10, label: 'Hotel', url: '/departments/10', iconUrl: '/public/img/sample.png', isActive: true},
				{type: 'department', id: 15, label: 'Front Desk', url: '/departments/15', iconUrl: '/public/img/sample.png'},
				{type: 'employee', id: 0, label: 'All Employees', url: '/employees', iconClass: 'fa-users'},
				{type: 'general', id: -2, label: 'Dashboard', url: '/dashboard', iconClass: 'fa-dashboard'},
				{type: 'general', id: -3, label: 'Reports', url: '/reports', iconClass: 'fa-bar-chart'},
				{type: 'general', id: -4, label: 'Documents', url: '/documents', iconClass: 'fa-file-text-o'}
			]
		}
	},

	getNavigationEmployee: function() {
		// id - user or department id
		return {
			items: [
				{type: 'department', id: 10, label: 'Hotel', url: '/boards/departments/10', iconUrl: '/public/img/sample.png'},
				{type: 'department', id: 15, label: 'Front Desk', url: '/boards/departments/15', iconUrl: '/public/img/sample.png'},
				{type: 'employee', id: 1, label: 'Mike G Gerald', url: '/boards/employees/me', iconUrl: '/public/img/sample.png'},
				{type: 'employee', id: 0, label: 'All Employees', url: '/boards/employees', iconClass: 'fa-users'},
				{type: 'general', id: -2, label: 'Dashboard', url: '/dashboard', iconClass: 'fa-dashboard'},
				{type: 'general', id: -3, label: 'Reports', url: '/reports', iconClass: 'fa-bar-chart'},
				{type: 'general', id: -4, label: 'Documents', url: '/documents', iconClass: 'fa-file-text-o'}
			]
		}
	},

	getPostsUnreadCount: function() {
		return {
			counts: [
				{type: "employee", id: -10, count: 1},
				{type: "department", id: 654, count: 7},
				{type: "employee", id: 0, count: 6}
			]
		}
	},

	getTags: function () {
		return [
			new models.Tag({
				id: 1,
				name: 'tag1'
			}),
			new models.Tag({
				id: 2,
				name: 'tag2'
			})
		]
	},

	getDepartmentsForAdmin: function () {
		return [
			new models.Department({
				id: 10,
				name: 'Hotel'
			}),
			new models.Department({
				id: 15,
				name: 'Front Desk'
			}),
			new models.Department({
				id: 20,
				name: 'Sales'
			})
		]
	},

	getDepartmentsForEmployee: function () {
		return [
			new models.Department({
				id: 20,
				name: 'Sales'
			})
		]
	},

	getHotelsForAdmin: function () {
		return [
			new models.Hotel({
				id: 1,
				name: 'Hilton',
				hotel_admin: 1,
				webaddr: 'hilton'
			}),
			new models.Hotel({
				id: 2,
				name: 'Best Western',
				hotel_admin: 2,
				webaddr: 'bestwestern'
			}),
			new models.Hotel({
				id: 3,
				name: 'CALIFORNIA INN AND SUITES',
				hotel_admin: 3,
				webaddr: 'cias'
			})
		]
	},

	getHotelForEmployee: function () {
		return new models.Hotel({
			id: 1,
			name: 'Hilton',
			hotel_admin: 1,
			webaddr: 'hilton'
		})
	},

	getAreas: function () {
		return [
			new models.Area({
				id: 1,
				name: 'Rooms'
			}),
			new models.Area({
				id: 2,
				name: 'Guest Areas'
			})
		]
	},

	getMaintenanceTypes: function () {
		return [
			new models.MaintenanceType({
				id: 10,
				name: 'Electrical'
			}),
			new models.MaintenanceType({
				id: 20,
				name: 'Plumbing'
			})
		]
	},

	getTodoItem: function (id, isComplete) {
		let fakeDate = moment.tz('America/Los_Angeles')
								.set('minute', isComplete ? 1 : 59)
								.set('second', 0)
								.set('hour', isComplete ? 0 : 23)
								.utc()
								.format('YYYY-MM-DD HH:mm:ss')

		return new models.Post({
			id: id,
			type_id: 'task',
			hotel_id: 107,
			subject: 'Fake test task',
			description: '<span>@HouseKeeping</span><div>From ATC</div>',
			due_dtm_utc: fakeDate,
			completion_status: isComplete ? 'done' : 'open'
		}, 'America/Los_Angeles');
	},

	getEquipmentGroups: function () {
		return [
			new models.EquipmentGroup({
				id: 10,
				name: 'Laundry Equipment'
			}),
			new models.EquipmentGroup({
				id: 20,
				name: 'Office Equipment'
			}),
			new models.EquipmentGroup({
				id: 30,
				name: 'Common Area Equipment'
			})
		]
	},

	getEquipments: function () {
		return [
			new models.Equipment({
				id: 10,
				groupId: 10,
				name: '40 lbs. Washer'
			}),
			new models.Equipment({
				id: 20,
				groupId: 10,
				name: '45 lbs. Dryer'
			})
		]
	},

	getUser: () => [
		{
			"id": 486,
			"firstName": "Selma",
			"lastName": "Becirovic",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/572cd2e1dfdbb.png"
		}, {
			"id": 487,
			"firstName": "Jessica",
			"lastName": "Lorenzo",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/56fbff32b7544.jpeg"
		}, {
			"id": 488,
			"firstName": "Stephanie",
			"lastName": "Powell",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/default.png"
		}, {
			"id": 489,
			"firstName": "Javier",
			"lastName": "Lazo",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/572cd247a16cb.png"
		}, {
			"id": 652,
			"firstName": "Katariina",
			"lastName": "Forsberg",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/56e9a8213d696.jpg"
		}, {
			"id": 825,
			"firstName": "Vladimir",
			"lastName": "Syerik",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/56fa16a48762a.png"
		}, {
			"id": 485,
			"firstName": "Mike G",
			"lastName": "",
			"imageUrl": "https://s3-us-west-2.amazonaws.com/htap-uploads-dev/img/profile/56786012c6d6b.jpg"
		}],

	getFakeTodoPost: function (type, isOverdue, id) {
		const timezone = 'America/Los_Angeles'
		let fakeDate = moment().tz(timezone).add(isOverdue ? -1 : 1, 'hours')

		let randomId = id
		if (!randomId) {
			const idBottomEdge = 1000000
			randomId = Math.floor(idBottomEdge + Math.random() * idBottomEdge)
		}

		return new models.Post({
			id: randomId,
			type_id: type,
			hotel_id: 107,
			subject: `Fake ${type} with id - ${randomId}`,
			due_dtm_utc: fakeDate.utc(),
			completion_status: 'open',
			assignment_type_id: 'dep',
			assignment_id: 654
		}, timezone)
	},

	getChecklist: function (isOverdue) {
		return this.getFakeTodoPost('cl', isOverdue)
	},

	getInspection: function (isOverdue) {
		return this.getFakeTodoPost('insp', isOverdue)
	},

	getRoomLog: function (id) {
		let fakeRoomLog = this.getFakeTodoPost('rl', false, 1000000000 + id)
		fakeRoomLog = Object.assign(fakeRoomLog, {
			rooms: {
				count: 2,
				collection: [
					this.getFakeTodoPost('rl', false),
					this.getFakeTodoPost('rl', false)
				]
			}
		})

		return fakeRoomLog
	}
}

module.exports = fakeData

