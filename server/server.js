'use strict'

import express from 'express'
import routes from './routes'
import http from 'http'
import winston from 'winston'

import configExpress from './config/express'
import config from './config/environment'
import logger from './core/logger'

let
	app = express(),
	server = http.createServer(app),
	auth = require('./middleware/auth')

/**
 * Initializes the server.
 * @param {Object} options The init options.
 * @returns {void}
 */
function init(options) {
	if (options) {
		auth = options.auth || auth
	}

	configExpress(app, routes, auth)

	process.on('unhandledRejection', (reason, p) => {
		logger.log('info', 'server|init|process.on.unhandledRejection')
		logger.log('error', reason.stack, {promise: p, error: reason})
	})
}

/**
 * Starts the server.
 * @returns {void}
  */
function start() {
	server.listen(config.port, config.ip, () => {
		winston.log('info', `${new Date()} - Express server listening on ${config.port}, in ${app.get('env')} mode.`)
	})
}

function close() {
	server.close()
}

module.exports = {
	init: init,
	start: start,
	close: close,
	app: app
}
