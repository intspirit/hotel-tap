'use strict'

import express from 'express'
import devtoolsController from './devtoolsController'

const router = express.Router()

router.route('/rundbscripts').get(devtoolsController.runDbScripts)

export default router
