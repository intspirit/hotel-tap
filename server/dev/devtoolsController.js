'use strict'

import cleanPostDescription from '../../_db/clean-post-description'
import {BadRequestError, ServerError, getResponseError} from '../errors'


export default {

	async runDbScripts(req, res) {
		try {
			if (req.app.get('env') === 'production') {
				const responseError = getResponseError(
					new BadRequestError('Not available', {}))

				return res.status(responseError.error.code).json(responseError)
			}

			const processedCount = await cleanPostDescription.run()

			return res.json({processedCount: processedCount})

		} catch (error) {
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	}
}
