'use strict'

import _ from 'lodash'
import moment from 'moment'
import logger from '../core/logger'
import {postDb, tagDb, hotelDb, userDb} from '../db'
import Post from '../models/Post'
import constants from '../../shared/constants'
import utils from '../core/utils'

const utcToTimezone = utils.utcToTimezone

const reportLogic = {

	/**
	 * Gets hotel reports
	 *
	 * @param {int} hotelId
	 * @returns {*}
	 */
	async getReports(hotelId) {
		logger.log('info', 'reportLogic|getReports', {hotelId: hotelId})

		try {
			const postTypes = [constants.postTypes.PM, constants.postTypes.CHECKLIST]
			const dbPosts = await postDb.getReportsByPostTypes(hotelId, postTypes)

			return dbPosts.map(x => _.pick(new Post(x), ['id', 'typeId', 'subject']))
		} catch (err) {
			logger.log('error', 'reportLogic|getReports|error:%o', err || 'Error is undefined')

			return []
		}
	},

	async getReportById(hotelId, reportId) {
		logger.log('info', 'reportLogic|getReportById', {hotelId: hotelId, reportId: reportId})

		try {
			const report = await postDb.getReportById(hotelId, reportId)

			return new Post(report)
		} catch (err) {
			logger.log('error', 'reportLogic|getReportById|error:%o', err || 'Error is undefined')

			return null
		}
	},

	async prepareLogsForResponse(hotelId, dbPosts) {
		try {
			const locationIds = dbPosts.map(x => x.location_id)
			const locations = await tagDb.getByIds(locationIds)
			const indexedLocations = _.keyBy(locations, 'id')
			const users = await userDb.getByHotelId(hotelId)
			const indexedUsers = _.keyBy(users, 'id')

			const posts = dbPosts.map(x => {
				const post = new Post(x)

				const location = indexedLocations[post.location.id]
				post.setLocation(location)

				const completedByUser = indexedUsers[x.completed_by]
				post.setCompletedBy(completedByUser)

				return post
			})

			return posts
		} catch (err) {
			logger.log('error', 'reportLogic|preparePreventiveMaintenanceLogsForResponse|error:%o',
				err || 'Error is undefined')

			return []
		}
	},

	async getPreventiveMaintenanceLogsByReportId(hotelId, reportId) {
		logger.log('info', 'reportLogic|getPreventiveMaintenanceLogsByReportId', {hotelId: hotelId, reportId: reportId})

		try {
			const hotel = await hotelDb.getById(hotelId)
			const fromDateTimeUtc = moment.utc().
				subtract(constants.reports.DAYS_FROM_NOW, 'days').
				format('YYYY-MM-DD HH:mm:ss')
			const fromDateTimeHotel = utcToTimezone(fromDateTimeUtc, hotel.timeZone)

			const report = await reportLogic.getReportById(hotelId, reportId)
			const dbPosts = await postDb.getPreventiveMaintenanceLogsByReportId(hotelId, reportId, fromDateTimeHotel)
			const logs = await reportLogic.prepareLogsForResponse(hotelId, dbPosts)

			return {
				report: report,
				logs: logs
			}
		} catch (err) {
			logger.log('error', 'reportLogic|getPreventiveMaintenanceLogsByReportId|error:%o',
				err || 'Error is undefined')

			return {
				report: null,
				logs: []
			}
		}
	},

	async getChecklistLogsByReportId(hotelId, reportId) {
		logger.log('info', 'reportLogic|getChecklistLogsByReportId', {hotelId: hotelId, reportId: reportId})

		try {
			const hotel = await hotelDb.getById(hotelId)
			const fromDateTimeUtc = moment.utc().
				subtract(constants.reports.DAYS_FROM_NOW, 'days').
				format('YYYY-MM-DD HH:mm:ss')
			const fromDateTimeHotel = utcToTimezone(fromDateTimeUtc, hotel.timeZone)

			const report = await reportLogic.getReportById(hotelId, reportId)
			const dbPosts = await postDb.getChecklistLogsByReportId(hotelId, reportId, fromDateTimeHotel)
			const logs = await reportLogic.prepareLogsForResponse(hotelId, dbPosts)

			return {
				report: report,
				logs: logs
			}
		} catch (err) {
			logger.log('error', 'reportLogic|getChecklistLogsByReportId|error:%o', err || 'Error is undefined')

			return {
				report: null,
				logs: []
			}
		}
	}
}

export default reportLogic
