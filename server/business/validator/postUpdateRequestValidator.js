'use strict'

import validator from 'validator'
import _ from 'lodash'

import {postReadStatus, complaintResolutions} from '../../../shared/constants'

const complaintResolutionTypesArray = _.values(complaintResolutions)

const postUpdateRequestValidator = {
	validReadStatus: req => validator.isIn(String(req.readStatus), [postReadStatus.READ, postReadStatus.UNREAD]),

	validFlagStatus: req =>
		validator.isBoolean(String(req.flag)) &&
		(validator.isDate(String(req.flagDue)) || validator.isBoolean(String(req.flagDue))),

	validComplaintResolution: complaintResolution =>
		!validator.isEmpty(complaintResolution.resolution) &&
			validator.isIn(complaintResolution.resolution, complaintResolutionTypesArray) &&
		(_.isNil(complaintResolution.cost) ||
			!_.isNil(complaintResolution.cost) && validator.isInt(String(complaintResolution.cost))) &&
		(_.isNil(complaintResolution.pointsGiven) ||
			!_.isNil(complaintResolution.pointsGiven) && validator.isInt(String(complaintResolution.pointsGiven)))
}

export default postUpdateRequestValidator
