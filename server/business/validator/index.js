'use strict'

import postUpdateRequestValidator from './postUpdateRequestValidator'

export default {
	postUpdateRequestValidator: postUpdateRequestValidator
}
