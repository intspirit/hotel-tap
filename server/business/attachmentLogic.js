'use strict'

import AWS from 'aws-sdk'
import {attachmentDb, postDb} from '../db'
import logger from '../core/logger'
import config from '../config/environment'

AWS.config.update({
	accessKeyId: config.s3.accessKey,
	secretAccessKey: config.s3.secretKey
})

const attachmentLogic = {

	/**
	 * Insert attachments for post
	 * @param {int} sourceId
	 * @param {string} sourceType
	 * @param {Object[]} attachments
	 * @param {int} userId
	 * @param {datetime} nowUTC
	 * @returns {string}
	 */
	async addAttachments(sourceId, sourceType, attachments, userId, nowUTC) {
		if (!attachments.length) return

		await attachmentLogic.insertAttachments(sourceId, sourceType, attachments, userId, nowUTC)

		const attachmentsCount = attachments.length
		await postDb.updateAttachmentsCount(sourceId, attachmentsCount)

		attachmentLogic.moveTmpToStable(sourceId, attachments)
	},

	/**
	 * Inserts attachments.
	 * @param {int} sourceId
	 * @param {string} sourceType
	 * @param {Object[]} attachments
	 * @param {int} userId
	 * @param {datetime} nowUTC
	 * @returns {*|Promise}
	 */
	insertAttachments: async (sourceId, sourceType, attachments, userId, nowUTC) => {
		const insertValues = attachments.filter(x => x.filePath || x.tmpFilePath).map(x => [
			sourceId,
			sourceType,
			x.filePath || '',
			x.tmpFilePath || '',
			userId,
			nowUTC,
			1
		])

		return await attachmentDb.insertBulk(insertValues)
	},

	/**
	 * Move attachments from temp bucket to stable.
	 * @param {int} sourceId
	 * @param {Object[]} attachments
	 * @returns {*|Promise}
	 */
	moveTmpToStable: async (sourceId, attachments) => {
		const s3Object = new AWS.S3()

		attachments.forEach(x => {
			const key = x.tmpFilePath

			s3Object.copyObject({
				Key: `${config.s3.folders.attachment}/${key}`,
				Bucket: config.s3.bucket,
				CopySource: `${config.s3.tmpBucket}/${key}`,
				ACL: 'public-read'
			}, async (err, res) => {
				if (err) {
					logger.error('attachmentLogic|moveTmpToStable|error', {attachment: x, err: err})

					return
				}

				logger.info('attachmentLogic|moveTmpToStable|info', {attachment: x, res: res})
				await attachmentDb.updateFromTmpToStable(sourceId, key, `${config.s3.folders.attachment}/${key}`)
			})
		})

	}
}

export default attachmentLogic
