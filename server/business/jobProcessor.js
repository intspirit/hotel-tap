'use strict'

import moment from 'moment'
import uuid from 'uuid'
import logger from '../core/logger'
import {jobDb, jobStatusDb} from '../db'

const jobProcessor = {

	/**
	 * Submits a new job
	 *
	 * @param  {string} name
	 * @param  {int} uuId
	 * @param  {string|null} meta
	 * @return {int}
	 */
	submit: async (name, uuId, meta) => {
		logger.info('jobProcessor|submit', {name, uuId, meta})

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const job = {
			postId: uuId,
			name: name,
			meta: meta,
			createdDateTimeUtc: nowUtc
		}

		return await jobDb.insert(job)
	},

	/**
	 * Submits multiple jobs
	 *
	 * @param  {string} name
	 * @param  {int[]} uuIds
	 * @param  {string|null} meta
	 * @return {Map} Map object where key is uuId and value is related jobId
	 */
	submitBulk: async (name, uuIds, meta) => {
		logger.log('info', 'jobProcessor|submitBulk', {name, uuIds, meta})

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const groupUuid = uuid.v4()
		const jobs = uuIds.map(x => [x, groupUuid, name, meta, nowUtc])

		await jobDb.insertBulk(jobs)

		const dbJobs = await jobDb.getByGroupUuid(groupUuid)

		return new Map(dbJobs.map(x => [x.post_id, x.id]))
	},

	/**
	 * Updates job status
	 *
	 * @param  {int} jobId
	 * @param  {string} status
	 * @param  {string|null} result
	 * @return {int}
	 */
	updateStatus: async (jobId, status, result) => {
		logger.info('jobProcessor|updateStatus', {jobId, status, result})

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const jobStatus = {
			jobId,
			status,
			result,
			createdDateTimeUtc: nowUtc
		}
		await jobStatusDb.insert(jobStatus)
	}
}

export default jobProcessor
