'use strict'

import moment from 'moment'
import logger from '../core/logger'
import {tagDb, hotelDb} from '../db'
import utils from '../core/utils'
import {BadRequestError} from '../errors'

const utcToTimezone = utils.utcToTimezone

const tagLogic = {

	/**
	 * Inserts a tag.
	 *
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {Object} tag
	 * @return {int}
	 */
	insert: async (hotelId, userId, tag) => {
		logger.log('info', 'tagLogic|insert', {hotelId, userId, tag})

		if (!tag.name || tag.name.trim() === '') {
			throw new BadRequestError('Empty tag name is not allowed', {hotelId, userId, tag})
		}

		const hotel = await hotelDb.getById(hotelId)
		if (!hotel) {
			throw new BadRequestError('Can not find a hotel', {hotelId, userId, tag})
		}

		//TODO: ak p2 validate typeId

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const nowHotel = utcToTimezone(nowUtc, hotel.timeZone)

		const newId = await tagDb.insert(hotelId, userId, tag, nowHotel)

		return {
			insertedDate: nowHotel,
			newId: newId
		}
	}
}

export default tagLogic
