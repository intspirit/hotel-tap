'use strict'

import _ from 'lodash'
import AWS from 'aws-sdk'
import bcrypt from 'bcrypt-nodejs'
import Promise from 'bluebird'
import logger from '../core/logger'
import {LoginError, BadRequestError} from '../errors'
import utils from '../core/utils'
import db from '../db'
import config from '../config/environment'
import messages from '../../shared/core/messages'

AWS.config.update({
	accessKeyId: config.s3.accessKey,
	secretAccessKey: config.s3.secretKey
})

const fieldNames = ['id', 'type', 'label', 'url', 'iconClass', 'iconUrl']
const getNavigationOption = values => _.zipObject(fieldNames, values)
const getNavigationOptionForDepartments = departments => departments.map(department =>
	getNavigationOption(
		[department.id, 'department', department.name,
			`/boards/departments/${department.id}`, '', department.iconFullUrl])
)

/**
 * User related business logic.
 */
export default {

	/**
	 * Gets user navigation.
	 * @param {int} hotelId
	 * @param {Object} user
	 * @param {int} user.id
	 * @param {boolean} user.isAdmin
	 * @param {string} user.fullName
	 * @returns {*|{items}}
	 */
	async getNavigation(hotelId, user) {
		const employeeOption = [
			getNavigationOption([-10, 'employee', user.fullName, '/boards/me', 'fa-home', ''])
		]
		const adminTopOptions = [
			getNavigationOption([-1, 'general', 'Settings', '/admin/settings', 'fa-wrench', ''])
		]
		const bottomOptions = [
			getNavigationOption([0, 'employee', 'All Employees', '/employees', 'fa-users', '']),
			getNavigationOption([-2, 'employee', 'Service Recovery Process', '/boards/srp', 'fa-star', '']),
			getNavigationOption([-3, 'general', 'Checklists', '/checklists', 'fa-list', '']),
			getNavigationOption([-4, 'general', 'Preventive Maintenance', '/pm', 'fa-wrench', '']),
			getNavigationOption([-5, 'general', 'Inspections', '/inspections', 'fa-list-ol', '']),
			getNavigationOption([-6, 'general', 'Dashboard', '/dashboard', 'fa-dashboard', '']),
			getNavigationOption([-7, 'general', 'Reports', '/reports', 'fa-bar-chart', '']),
			getNavigationOption([-8, 'general', 'Documents', '/documents', 'fa-file-text-o', '']),
			getNavigationOption([-9, 'general', 'Creating Notes and Tasks', '/help', 'fa-file-video-o', ''])
		]

		const devOptions = [
			getNavigationOption([-100, 'general', 'Run DB Scripts', '/rundbscripts', 'fa-wrench', ''])
		]
		//TODO: vvs p1 validate that user has access to the hotel (middleware)
		try {
			let result = []
			if (user.isAdmin) {
				const departments = await db.departmentDb.getByHotelId(hotelId)

				result = employeeOption.concat(
					adminTopOptions,
					departments ? getNavigationOptionForDepartments(departments) : [],
					bottomOptions)
			} else {
				const departments = await db.departmentDb.getByUser(hotelId, user.id)
				result = (departments ? getNavigationOptionForDepartments(departments) : []).concat(
					employeeOption, bottomOptions)
			}

			if (config.env !== 'production') {
				result = result.concat(devOptions)
			}

			return result
		} catch (err) {
			logger.error(`userLogic|getNavigation|error:${err}`)

			return employeeOption.concat(bottomOptions)
		}
	},

	async getHotels(user) {
		try {
			let result = []
			if (user.isAdmin) {
				result = await db.hotelDb.getByAdmin(user.id)
			} else {
				const hotel = await db.hotelDb.getById(user.hotelId)
				if (hotel) {
					result.push(hotel)
				}
			}

			return result
		} catch (err) {
			logger.error(`userLogic|getHotels|error:${err}`)

			return []
		}
	},

	async getHotelsFewDetails(user) {
		const fields = ['id', 'name', 'timeZone']
		try {
			const hotels = await this.getHotels(user)

			return hotels.map(hotel => _.pick(hotel, fields))
		} catch (err) {
			logger.error(`userLogic|getHotelsFewDetails|error:${err}`)

			return Promise.resolve([])
		}
	},

	//TODO: vvs p2 REFACTOR wrong place - keep in controller
	async getMe(user) {
		const fields = [
			'id', 'firstName', 'fullName', 'lastName',
			'email', 'imageUrl', 'isAdmin', 'notificationStatus'
		]

		return _.pick(user, fields)
	},

	//TODO: vvs p2 REFACTOR better name
	/**
	 * Open Hotel for Admin user.
	 * @param {object} user
	 * @param {integer} hotelId
	 * @return {{token: *, user: *}}
	 * @throws {LoginError}
	 */
	async openHotel(user, hotelId) {
		if (!(user && hotelId && await this.hasHotelAssess(user, hotelId))) {
			throw new LoginError('Invalid parameters', user)
		}

		user.setHotelId(hotelId)

		return {
			token: utils.generateJWT(user, false),
			user: user.commonDetails
		}
	},

	//TODO: vvs p2 REFACTOR better name
	/**
	 * Check if Admin user has access to hotel.
	 * @param {object} user
	 * @param {integer} hotelId
	 * @return {boolean}
	 */
	async hasHotelAssess(user, hotelId) {
		if (user.hotelId === hotelId) return true

		const users = await db.userDb.getByHotelId(hotelId)

		return Boolean(_.find(users, x => x.id === user.id))
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Get departments
	 *
	 * @param {Number} hotelId
	 * @param {Object} user
	 * @param {Number} user.id
	 * @param {Boolean} user.isAdmin
	 * @returns {Array}
	 */
	async getDepartments(hotelId, user) {
		let departments = []
		const fields = ['id', 'hotelId', 'name', 'imageUrl', 'iconUrl', 'iconFullUrl']

		try {
			if (user.isAdmin) {
				departments = await db.departmentDb.getByHotelId(hotelId)
			} else {
				departments = await db.departmentDb.getByUser(hotelId, user.id)
			}
		} catch (err) {
			logger.error(`userLogic|getDepartments|error:${err}`)
		}

		return departments.map(x => _.pick(x, fields))
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Gets hotel tags.
	 *
	 * @param {Number} hotelId
	 * @returns {Array}
	 */
	async getTags(hotelId) {
		let tags = []

		try {
			tags = await db.tagDb.getByHotelId(hotelId)
		} catch (err) {
			logger.log('error', 'userLogic|getTags|error:%o', err)
		}

		return tags
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Gets hotel areas.
	 *
	 * @param {Number} hotelId
	 * @returns {Array}
	 */
	async getAreas(hotelId) {
		let areas = []

		try {
			areas = await db.areaDb.getByHotelId(hotelId)
		} catch (err) {
			logger.log('error', 'userLogic|getAreas|error:%o', err)
		}

		return areas
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Gets hotel maintenance types.
	 *
	 * @param {Number} hotelId
	 * @returns {Array}
	 */
	async getMaintenanceTypes(hotelId) {
		let mtypes = []

		try {
			mtypes = await db.maintenanceTypeDb.getMaintenanceTypesByHotelId(hotelId)
		} catch (err) {
			logger.log('error', 'userLogic|getMaintenanceTypes|error:%o', err)
		}

		return mtypes
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Gets equipment groups.
	 *
	 * @param {Number} hotelId
	 * @returns {Array}
	 */
	async getEquipmentGroups(hotelId) {
		let equipmentGroups = []

		try {
			equipmentGroups = await db.equipmentGroupDb.getEquipmentGroupsByHotelId(hotelId)
		} catch (err) {
			logger.log('error', 'userLogic|getEquipmentGroups|error:%o', err)
		}

		return equipmentGroups
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Gets equipment groups.
	 *
	 * @param {Number} hotelId
	 * @returns {Array}
	 */
	async getEquipments(hotelId) {
		let equipments = []

		try {
			equipments = await db.equipmentDb.getEquipmentsByHotelId(hotelId)
		} catch (err) {
			logger.log('error', 'userLogic|getEquipments|error:%o', err)
		}

		return equipments
	},

	//TODO: alytyuk p2 REFACTOR wrong place, BadRequestError doesn't belong to business, it's from controller
	/**
	 * Updates user info
	 *
	 * @param {int} userId
	 * @param {object} info
	 * @param {object} user
	 * @returns {*}
	 **/
	async update(userId, info, user) {
		if (info.firstName) return await this.updateProfile(userId, info)

		if (info.hasOwnProperty('oldPassword') && info.hasOwnProperty('newPassword'))
			return await this.updatePassword(userId, info, user)

		if (_.isBoolean(info.notification))
			return await this.updateNotification(userId, Number(info.notification))

		throw new BadRequestError('Bad request for update user info', {
			userId: userId,
			fields: info
		})
	},

	/**
	 * Updates user profile info
	 *
	 * @param {int} userId
	 * @param {object} info
	 * @param {string} info.firstName
	 * @param {string|null} info.lastName
	 * @param {string|null} info.imageFileName
	 * @returns {*}
	 **/
	async updateProfile(userId, info) {
		if (info.hasOwnProperty('imageFileName') && info.imageFileName !== null) {
			const key = info.imageFileName
			const s3Object = new AWS.S3()
			const s3Params = {
				Key: `${config.s3.folders.profile}/${key}`,
				Bucket: config.s3.bucket,
				CopySource: `${config.s3.tmpBucket}/${key}`,
				ACL: 'public-read'
			}

			try {
				await s3Object.copyObject(s3Params).promise()
				await db.userDb.updateProfile(userId, info)

				return await db.userDb.getById(userId).then(user => this.getMe(user))
			} catch (err) {
				throw err
			}
		} else {
			await db.userDb.updateProfile(userId, info)

			return await db.userDb.getById(userId).then(user => this.getMe(user))
		}
	},

	//TODO: alytyuk p2 REFACTOR wrong place, BadRequestError doesn't belong to business, it's from controller
	/**
	 * Updates user's password
	 *
	 * @param {int} userId
	 * @param {object} info
	 * @param {string} info.newPassword
	 * @param {string} info.oldPassword
	 * @param {object} user
	 * @param {function} user.validPassword
	 * @returns {*}
	 **/
	async updatePassword(userId, info, user) {
		const {oldPassword, newPassword} = info

		if (_.isEmpty(oldPassword) || _.isEmpty(newPassword) || !user.validPassword(oldPassword)) {
			const error = new BadRequestError(messages.error.PASSWORD_UPDATE_FAILED)
			error.userMessage = messages.error.PASSWORD_UPDATE_FAILED
			throw error
		}

		if (oldPassword === newPassword) {
			const error = new BadRequestError(messages.error.NEW_PASSWORD_SHOULD_NOT_EQUAL_OLD_PASSWORD)
			error.userMessage = messages.error.NEW_PASSWORD_SHOULD_NOT_EQUAL_OLD_PASSWORD
			throw error
		}

		await bcrypt.hash(newPassword, null, null, async (err, hash) => {
			if (err) {
				logger.error('userLogic|updatePassword|error', {err})

				return
			}

			await db.userDb.updatePassword(userId, hash)
		})

		return await db.userDb.getById(userId).then(userInfo => this.getMe(userInfo))
	},

	/**
	 * Updates user's notification status
	 *
	 * @param {int} userId
	 * @param {int} notification
	 * @returns {*}
	 **/
	async updateNotification(userId, notification) {
		await db.userDb.updateNotification(userId, notification)

		return await db.userDb.getById(userId).then(user => this.getMe(user))
	}
}
