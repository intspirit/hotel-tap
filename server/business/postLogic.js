'use strict'

import _ from 'lodash'
import Promise from 'bluebird'
import logger from '../core/logger'
import {
	userDb,
	postReadStatusDb,
	attachmentDb,
	postDb,
	departmentDb,
	hotelDb,
	postDepartmentDb,
	employeeDepartmentDb,
	postUserDb,
	postTagDb,
	tagDb,
	translationDb,
	languageDb} from '../db'
import Post from '../models/Post'
import constants from '../../shared/constants'
import utils from '../core/utils'
import hotelLogic from './hotelLogic'
import attachmentLogic from './attachmentLogic'
import moment from 'moment-timezone'
import {BadRequestError} from '../errors'
import translationProcessor from './translationProcessor'
import notificationProcessor from './notificationProcessor'

const localToUtc = utils.localToUtc
const localToTimezone = utils.localToTimezone
const utcToTimezone = utils.utcToTimezone
const timezoneToUtc = utils.timezoneToUtc

//TODO: vvs p2 REFACTOR OBSOLETE DO NOT USE. WILL BE REMOVED.
/*eslint-disable no-confusing-arrow */
const assignAssignmentInfo = post =>
	post.assignmentId !== '' && post.assignmentType !== '' ?
		{
			id: post.assignmentId,
			type: post.assignmentType
		} :
		{}

/*eslint-enable no-confusing-arrow */

const postLogic = {

	/**
	 * Gets attachments.
	 * @param {Array} dbPosts
	 * @param {int} dbPosts.attachment_count
	 * @param {int} dbPosts.id
	 * @param {Array} dbComments
	 * @param {int} dbComments.attachment_count
	 * @param {int} dbComments.id
	 * @returns {Array}
	 */
	getAttachments: async (dbPosts, dbComments) => {
		const postWithAttachmentIds = dbPosts.filter(x => x.attachment_count > 0).map(x => x.id)
		const commentWithAttachmentIds = dbComments.filter(x => x.attachment_count > 0).map(x => x.id)
		const allAttachmentIds = postWithAttachmentIds.concat(commentWithAttachmentIds)
		const attachments = await attachmentDb.getByPostIds(allAttachmentIds)

		return _.groupBy(attachments, 'postId')
	},

	/**
	 * Gets comments for a list of posts.
	 * @param {int} hotelId
	 * @param {Array} dbPosts
	 * @param {int} dbPosts.id
	 * @param {int} dbPosts.comment_count
	 * @returns {*|Array|Object|{}}
	 */
	getComments: async (hotelId, dbPosts) => {
		const postWithCommentIds = dbPosts.filter(x => x.comment_count > 0).map(x => x.id)

		return await postDb.getByParentIds(hotelId, postWithCommentIds, constants.postTypes.COMMENT)
	},

	/**
	 * Updates db posts with translations if posts language differ from "language" argument
	 * @param {int} hotelId
	 * @param {string} language
	 * @param {Array} dbPosts
	 * @returns {Array}
	 */
	updateDbPostsWithTranslationsIfRequired: async (hotelId, language, dbPosts) => {
		//TODO: vvs p2 REFACTOR REVIEW
		const postToTranslateIds = _.chain(dbPosts).
										filter(dbPost => dbPost.language_id !== language).
										map('id').
										uniq().
										value()

		if (postToTranslateIds.length) {
			const {translating_phrase: translatingPhrase} = await languageDb.getTranslatingPhraseByLanguageCode(language)

			const foundDbPostsTranslations = await translationDb.getByPostIds(hotelId, postToTranslateIds, language)
			const dbPostTranslationsDictionary = _.keyBy(foundDbPostsTranslations, 'post_id')

			const allTranslationsDictionary = {}

			// fill allTranslationDictionary and intersect it with found db posts translations simultaneously
			_.each(postToTranslateIds, id => {
				allTranslationsDictionary[id] = dbPostTranslationsDictionary[id]
			})

			_.each(dbPosts, dbPost => {
				// check if dbPost requires translation
				if (allTranslationsDictionary.hasOwnProperty(dbPost.id)) {
					const translation = allTranslationsDictionary[dbPost.id]
					if (translation) {
						dbPost.subject = translation.subject
						dbPost.description = translation.description
					} else {
						dbPost.subject = dbPost.subject ? `${translatingPhrase} ${dbPost.subject}` : dbPost.subject
						dbPost.description = dbPost.description ? `${translatingPhrase} ${dbPost.description}` :
																	dbPost.description
					}
				}
			})
		}

		return dbPosts
	},

	/**
	 * Preparing post for response to client
	 * @param   {int}    hotelId
	 * @param   {object} user     authenticated user
	 * @param   {Array}  dbPosts
	 * @returns {Post[]}
	 */
	preparePostsForResponse: async (hotelId, user, dbPosts) => {
		try {
			//TODO: vvs p2 - parallel execution.
			//TODO alytyuk p2 refactor to reduce number of statements (see eslint max-statements)

			const dbComments = await postLogic.getComments(hotelId, dbPosts)

			const postAttachments = await postLogic.getAttachments(dbPosts, dbComments)

			const postIds = dbPosts.map(x => x.id).concat(dbComments.map(x => x.id))
			const readStatuses = await postReadStatusDb.getByPostIds(hotelId, postIds, constants.postReadStatus.READ)

			const users = await userDb.getByHotelId(hotelId)
			const indexedUsers = _.keyBy(users, 'id')
			const departments = await departmentDb.getByHotelId(hotelId)
			const indexedDepartments = _.keyBy(departments, 'id')

			//noinspection Eslint
			const readByUsers = readStatuses.map(x =>
				Object.assign({}, {postId: x.postId, user: indexedUsers[x.userId]}))
			const postReadByUsers = _.groupBy(readByUsers, 'postId')

			if (dbPosts.length && user.defaultLanguage) {
				//TODO: vvs p2 REFACTOR bad name
				await postLogic.updateDbPostsWithTranslationsIfRequired(hotelId, user.defaultLanguage, dbPosts)
			}

			const postUsers = await postUserDb.getByPostIds(hotelId, postIds)
			const indexedPostUsers = _.groupBy(postUsers, 'postId')

			const postTags = await postTagDb.getByPostIds(hotelId, postIds)
			const groupedPostTags = _.groupBy(postTags, 'postId')

			const tags = await tagDb.getByHotelId(hotelId)
			const indexedTags = _.keyBy(tags, 'id')

			const postsComments = dbComments.map(x => {
				const comment = new Post(x)
				const createdBy = indexedUsers[x.created_by]
				comment.setUser(createdBy)

				comment.attachments = postAttachments[comment.id] || []

				comment.tags = groupedPostTags[comment.id] ?
					groupedPostTags[comment.id].map(y => indexedTags[y.tagId]) :
					[]

				comment.ccUsers = indexedPostUsers[comment.id] ? indexedPostUsers[comment.id].
					filter(y => y.category === constants.postUser.CC).
					map(y => ({
						userId: y.userId,
						fullName: utils.getFullName(y)
					})) :
					[]

				return comment
			})
			const groupedPostsComments = _.groupBy(postsComments, 'parentId')

			const posts = dbPosts.map(x => {
				const post = new Post(x)
				const createdBy = indexedUsers[x.created_by]
				post.setUser(createdBy)

				post.attachments = postAttachments[post.id] || []
				post.comments = groupedPostsComments[post.id] || []
				const postReaders = postReadByUsers[post.id]

				//TODO: vvs p2 REFACTOR long string
				/*eslint-disable no-confusing-arrow*/
				post.readBy = postReaders ?
					postReaders.filter(p => p.user && p.user.id !== x.created_by).map(p => p.user.id === user.id ? 'You' : p.user.fullName) : []
				const yourNameInReadersList = _.remove(post.readBy, rb => rb === 'You')
				if (yourNameInReadersList.length > 0) post.readBy = yourNameInReadersList.concat(post.readBy)

				if (x.type_id === constants.postTypes.TASK) {
					const assignmentTo = x.assignment_type_id === constants.assignmentTypes.DEPARTMENT ?
						indexedDepartments[x.assignment_id] :
						indexedUsers[x.assignment_id]
					const completedByUser = indexedUsers[x.completed_by]

					if (assignmentTo) post.setAssignment(x.assignment_type_id, assignmentTo)
					if (completedByUser) post.setCompletedBy(completedByUser)
				}

				post.tags = groupedPostTags[post.id] ?
					groupedPostTags[post.id].
						filter(y => y.childId === 0).
						map(y => indexedTags[y.tagId]) :
					[]

				post.ccUsers = indexedPostUsers[post.id] ? indexedPostUsers[post.id].
					filter(y => y.childId === 0 && y.category === constants.postUser.CC).
					map(y => ({
						userId: y.userId,
						fullName: utils.getFullName(y)
					})) :
					[]

				return post
			})

			return posts
		} catch (error) {
			//TODO vvs p2 to this through ServerError
			logger.log('error', 'postLogic|prepareDataToResponse|error:%o', error || 'Error is undefined')

			return error
		}
	},

	/**
	 * Gets posts for a department.
	 * @param {int}      hotelId
	 * @param {int}      departmentId
	 * @param {object}   user           authenticated user
	 * @param {int|null} nextPageToken
	 * @returns {Post[]}
	 */
	getByDepartmentId: async (hotelId, departmentId, user, nextPageToken) => {
		logger.info('postLogic|getByDepartmentId', {hotelId, departmentId, user, nextPageToken})

		//TODO vvs p1 authorize - access to hotel, access to department
		try {
			const department = await departmentDb.getById(departmentId)
			const skip = Math.abs(parseInt(nextPageToken, 10) || 0)
			const take = constants.pageSize.BOARD
			const newNextPageToken = skip + take

			const dbPosts = await postDb.getByDepartmentId(hotelId, departmentId, user.id, skip, take, ['note', 'task'])

			const posts = await postLogic.preparePostsForResponse(hotelId, user, dbPosts)
			const hasMorePosts = posts && posts.length > 0 && posts.length === constants.pageSize.BOARD

			return {
				hasMore: hasMorePosts,
				nextPageToken: hasMorePosts ? newNextPageToken : skip,
				hasError: false,
				boardType: constants.boardTypes.DEPARTMENT,
				boardName: department.name,
				boardIconUrl: department.iconFullUrl,
				posts: posts
			}
		} catch (error) {
			logger.log('error', 'postLogic|getByDepartmentId|error:%o', error || 'Error is undefined')

			return {
				hasMore: false,
				nextPageToken: 0,
				hasError: true,
				posts: []
			}
		}
	},

	/**
	 * Gets posts for a employee.
	 * @param {int}      hotelId
	 * @param {int}      employeeId
	 * @param {object}   user           authenticated user
	 * @param {int|null} nextPageToken
	 * @returns {Post[]}
	 */
	getByEmployeeId: async (hotelId, employeeId, user, nextPageToken) => {
		logger.info('postLogic|getByEmployeeId', {hotelId, employeeId, user, nextPageToken})

		try {
			const employee = await userDb.getById(employeeId)
			const skip = Math.abs(parseInt(nextPageToken, 10) || 0)
			const take = constants.pageSize.BOARD
			const newNextPageToken = skip + take

			const dbPosts = await postDb.getByEmployeeId(hotelId, employeeId, user.id, [skip, take], ['note', 'task'])

			const posts = await postLogic.preparePostsForResponse(hotelId, user, dbPosts)
			const hasMorePosts = posts && posts.length > 0 && posts.length === constants.pageSize.BOARD

			return {
				hasMore: hasMorePosts,
				nextPageToken: hasMorePosts ? newNextPageToken : skip,
				hasError: false,
				boardName: employee.fullName,
				boardType: user.id === employeeId ?
					constants.boardTypes.HOME :
					constants.boardTypes.EMPLOYEE,
				posts: posts
			}
		} catch (error) {
			logger.log('error', 'postLogic|getByEmployeeId|error:%o', error || 'Error is undefined')

			return {
				hasMore: false,
				nextPageToken: 0,
				hasError: true,
				posts: []
			}
		}
	},

	/**
	 * Gets posts for a tag.
	 *
	 * @param {int}      hotelId
	 * @param {int}      tagId
	 * @param {object}   user           authenticated user
	 * @param {int|null} nextPageToken
	 * @returns {Post[]}
	 */
	getByTagId: async (hotelId, tagId, user, nextPageToken) => {
		logger.info('postLogic|getByTagId', {hotelId, tagId, user, nextPageToken})

		try {
			const tag = await tagDb.getById(tagId)
			const skip = Math.abs(parseInt(nextPageToken, 10) || 0)
			const take = constants.pageSize.BOARD
			const newNextPageToken = skip + take

			const dbPosts = await postDb.getByTagId(hotelId, tagId, user.id, skip, take, ['note', 'task'])

			const posts = await postLogic.preparePostsForResponse(hotelId, user, dbPosts)
			const hasMorePosts = posts && posts.length > 0 && posts.length === constants.pageSize.BOARD

			return {
				hasMore: hasMorePosts,
				nextPageToken: hasMorePosts ? newNextPageToken : skip,
				hasError: false,
				boardName: `@${tag.typeName} #${tag.name}`,
				boardType: constants.boardTypes.TAG,
				posts: posts
			}
		} catch (error) {
			logger.log('error', 'postLogic|getByTagId|error:%o', error || 'Error is undefined')

			return {
				hasMore: false,
				nextPageToken: 0,
				hasError: true,
				posts: []
			}
		}
	},

	/**
	 * Gets posts with guest complaints
	 * @param {int} hotelId
	 * @param {object} user
	 * @param {int|null} nextPageToken
	 * @returns {Post[]}
	 */
	getComplaints: async (hotelId, user, nextPageToken) => {
		logger.info('postLogic|getComplaints', {hotelId, user, nextPageToken})

		try {
			const skip = Math.abs(parseInt(nextPageToken, 10) || 0)
			const take = constants.pageSize.BOARD
			const newNextPageToken = skip + take

			const dbPosts = await postDb.getComplaintsByHotelId(hotelId, skip, take, ['note', 'task'])

			const posts = await postLogic.preparePostsForResponse(hotelId, user, dbPosts)
			const hasMorePosts = posts && posts.length > 0 && posts.length === constants.pageSize.BOARD

			return {
				hasMore: hasMorePosts,
				nextPageToken: hasMorePosts ? newNextPageToken : skip,
				hasError: false,
				boardName: user.fullName,
				boardType: constants.boardTypes.SRP,
				posts: posts
			}
		} catch (error) {
			logger.log('error', 'postLogic|getComplaints|error:%o', error || 'Error is undefined')

			return {
				hasMore: false,
				nextPageToken: 0,
				hasError: true,
				posts: []
			}
		}
	},

	/**
	 * Sets task completion status.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {string} status
	 * @returns {*|Promise}
	 */
	setCompletion: async (hotelId, userId, postId, status) => {
		const timezone = await hotelLogic.getTimezone(hotelId)
		if (!timezone) {
			throw new Error('Hotel timezone is not determined')
		}

		const currentDate = new Date()

		const statusUpdate = status === constants.taskStatus.DONE ?
		{
			status: constants.taskStatus.DONE,
			completionDateTime: localToTimezone(currentDate, timezone),
			completionDateTimeUtc: localToUtc(currentDate)
		} :
		{
			status: constants.taskStatus.OPEN,
			completionDateTime: null,
			completionDateTimeUtc: null
		}
		const completedBy = status === constants.taskStatus.DONE ? userId : null

		const updatedRowsNumber = await postDb.updateTaskCompletionStatus(hotelId, completedBy, postId, statusUpdate)

		// TODO alytyuk p3 change after overall post api controller refactoring
		return {
			updatedRowsNumber: updatedRowsNumber,
			completionDateTime: statusUpdate.completionDateTime
		}
	},

	setDueDateTime: async (hotelId, userId, postId, dueDateTime) => {
		const timezone = await hotelLogic.getTimezone(hotelId)
		if (!timezone) {
			throw new Error('Hotel timezone is not determined')
		}

		const now = new Date()
		const nowHotel = utcToTimezone(now, timezone)
		const properties = {
			dueDateTime: dueDateTime,
			dueDateTimeUtc: timezoneToUtc(dueDateTime, timezone)
		}
		const updatedRowsNumber = await postDb.updateTaskDueDateTime(hotelId, userId, postId, properties)

		return {
			updatedDate: nowHotel,
			updatedRowsNumber: updatedRowsNumber,
			dueDateTime: dueDateTime
		}
	},

	/**
	 * Updates post flag
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {object} flagProperties
	 * @returns {boolean}
	 */
	updateFlag: async (hotelId, userId, postId, flagProperties) => {
		//TODO: vvs p2 REFACTOR cleanup
		const timezone = await hotelLogic.getTimezone(hotelId)
		if (!timezone) {
			throw new Error('Hotel timezone is not determined')
		}

		const now = new Date()
		const nowInHotel = localToTimezone(now, timezone)

		const dbFlag = {
			flag: flagProperties.flag,
			flaggedDateTime: flagProperties.flag ? nowInHotel : null,
			flaggedDateTimeUtc: flagProperties.flag ? localToUtc(now) : null,
			flagDueDateTime: flagProperties.flag && flagProperties.flagDue ?
									localToTimezone(flagProperties.flagDue, timezone) :
									null,
			flagDueDateTimeUtc: flagProperties.flag && flagProperties.flagDue ?
									timezoneToUtc(flagProperties.flagDue, timezone) :
									null
		}

		const updateResult = await Promise.props({
			postDb: postDb.updateFlagStatus(hotelId, userId, postId, dbFlag),
			postUserDb: postUserDb.update(hotelId, postId, nowInHotel),
			postDepartmentDb: postDepartmentDb.update(hotelId, postId, nowInHotel),
			postReadStatusDb: postReadStatusDb.updateStatusForAllButUsers(hotelId, postId,
																			constants.postReadStatus.UNREAD, [userId])
		})

		return updateResult.postDb > 0
	},

	/**
	 * Updates resolution of post with guest complaint
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {object} user
	 * @param {object} complaintResolution
	 * @returns {boolean}
	 */
	updateComplaintResolution: async (hotelId, postId, user, complaintResolution) => {
		const postComplaint = await postDb.getPostComplaintById(hotelId, postId)

		if (!postComplaint) {
			throw new Error(`No post complaint wit a given id: ${postId}`)
		}

		if (!postComplaint.resolution ||
			postComplaint.resolution === constants.complaintResolutions.UNRESOLVED ||
			user.isAdmin) {
			const timezone = await hotelLogic.getTimezone(hotelId)
			if (!timezone) {
				throw new Error('Hotel timezone is not determined')
			}

			complaintResolution.dateTimeUTC = localToUtc()
			complaintResolution.dateTime = utcToTimezone(complaintResolution.dateTimeUTC, timezone)

			const updatedRowsNumber = await postDb.updateComplaintResolution(hotelId, postId, complaintResolution)

			return updatedRowsNumber > 0
		}

		return false
	},

	//TODO: vvs p2 remove throws, return proper error in the catch.
	/**
	 * Gets post by identifier.
	 * @param   {int}    hotelId
	 * @param   {Object} user     authenticated user
	 * @param   {int}    postId
	 * @returns {Object}
	 */
	getById: async (hotelId, user, postId) => {
		try {
			const dbPost = await postDb.getById(hotelId, user.id, postId)

			let response = null
			if (!dbPost) {
				throw new Error('Post was not found')
			} else if (!dbPost.hotel_id || dbPost.hotel_id !== hotelId) {
				throw new Error('Hotel id of the post is undefined or not allowed')
			} else {
				const preparedPosts = await postLogic.preparePostsForResponse(hotelId, user, [dbPost])

				response = {
					hasError: false,
					post: preparedPosts[0]
				}
			}

			return response
		} catch (error) {
			logger.log('error', 'postLogic|getById|error:%o', error || 'Error is undefined')

			return {
				hasError: true,
				post: null
			}
		}
	},

	/**
	 * Inserts s post.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {object} post
	 * @param {string|null} post.board
	 * @param {int[]} post.departments
	 * @param {Date} post.dueDateTimeUtc
	 * @param {object[]} attachments
	 * @return {int}
	 */
	insert: async (hotelId, userId, post, attachments) => { // eslint-disable consistent-return
		const user = await userDb.getById(userId)
		const hotel = await hotelDb.getById(hotelId)

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const nowHotel = utcToTimezone(nowUtc, hotel.timeZone)

		if (post.dueDateTime) {
			const timezone = await hotelLogic.getTimezone(hotelId)
			post.dueDateTimeUtc = timezoneToUtc(post.dueDateTime, timezone)
		}

		const newId = await postLogic.insertPost(hotel, user, post, nowUtc, nowHotel)

		const sourceType = post.typeId === constants.postTypes.TASK ?
				constants.attachmentSources.TASKS :
				constants.attachmentSources.NOTES
		postLogic.addAttachments(newId, sourceType, attachments, userId, nowUtc)

		//TODO: vvs p2 REFACTOR remove, use post.assignmentId
		const assignmentInfo = assignAssignmentInfo(post)

		//TODO: vvs p1 return and to the rest in background
		const postDepartmentIds = await postLogic.getAllPostDepartments(hotelId, user, post, assignmentInfo)
		const postUsers = await postLogic.getAllPostUsers(post.ccList, userId, assignmentInfo)
		const postUsersIds = postUsers.map(x => x.id)

		await postLogic.insertPostDepartments(hotelId, newId, postDepartmentIds, nowUtc)
		await postLogic.insertPostUsers(hotelId, newId, postUsers, nowUtc)
		await postLogic.insertPostTags(hotelId, newId, post)
		await postLogic.insertReadStatuses(hotelId, newId, userId, postDepartmentIds, postUsersIds)

		translationProcessor.translate(hotelId, newId, post, user.defaultLanguage)
		notificationProcessor.notifyAboutPostCreated(hotelId, userId, newId, post, postDepartmentIds)

		return newId
	},

	/**
	 * Inserts a post record.
	 * @param {Object} hotel
	 * @param {Object} user
	 * @param {Object} post
	 * @param {Date} nowUtc
	 * @param {Date} nowHotel
	 * @returns {*|Promise<int>}
	 */
	insertPost: async (hotel, user, post, nowUtc, nowHotel) => {
		const newPost = {
			typeId: post.typeId,
			hotelId: hotel.id,
			description: post.description,
			descriptionText: post.description,
			languageId: user.defaultLanguage,
			private: post.privatePost || false,
			guestComplaint: post.guestComplaint || false,
			createdBy: user.id,
			createdDateTime: nowHotel,
			createdDateTimeUtc: nowUtc,
			orderDateTimeUtc: nowUtc,
			assignmentId: post.assignmentId,
			assignmentType: post.assignmentType,
			subject: post.subject,
			dueDateTime: post.dueDateTime,
			dueDateTimeUtc: post.dueDateTimeUtc,
			parentId: post.parentId,
			completionStatus: constants.taskStatus.OPEN
		}

		return await postDb.insert(hotel.id, user.id, newPost)
	},

	/**
	 * Gets all departments related to the post.
	 *
	 * @param {int} hotelId
	 * @param {Object} user
	 * @param {Object} post
	 * @param {string} post.board
	 * @param {int[]} post.maintenanceTypeList
	 * @param {int[]} post.departments
	 * @param {Object} assignmentInfo
	 * @param {int} assignmentInfo.id
	 * @param {string} assignmentInfo.type
	 * @returns {int[]}
	 */
	getAllPostDepartments: async (hotelId, user, post, assignmentInfo) => {
		const board = post.board || ''
		const maintenanceTypeList = post.maintenanceTypeList || []
		const maintenanceDepartment = maintenanceTypeList.length > 0 ?
			await hotelLogic.getMaintenanceDepartment(hotelId) :
			null
		let departmentIds = [].concat(post.departments)

		if (assignmentInfo.type === constants.assignmentTypes.DEPARTMENT) {
			departmentIds.push(assignmentInfo.id)
		}

		if (maintenanceTypeList.length > 0) {
			departmentIds.push(maintenanceDepartment.id)
		}

		if (board.includes(constants.boardTypes.DEPARTMENT)) {
			let postDepartment = parseInt(board.split('/')[1], 10)
			if (user.roleId === constants.role.EMPLOYEE) {
				const employeeDepartments = await employeeDepartmentDb.getByUserId(user.id)
				postDepartment = employeeDepartments.find(x => x.departmentId === postDepartment) ?
					postDepartment :
					employeeDepartments[0].departmentId
			}
			const allDepartments = postDepartment && departmentIds.indexOf(postDepartment) === -1 ?
				departmentIds.concat([postDepartment]) :
				departmentIds

			return _.uniq(allDepartments)
		}

		return departmentIds
	},

	/**
	 * Inserts post departments.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {int[]} departmentIds
	 * @param {Date} now
	 * @returns {*|Promise}
	 */
	insertPostDepartments: async (hotelId, postId, departmentIds, now) => {
		const postDepartments = departmentIds.map(x => [hotelId, postId, x, 0, now])

		if (postDepartments.length > 0) return await postDepartmentDb.insertBulk(postDepartments)

		return await []
	},

	/**
	 * Gets all users with category related to the post.
	 *
	 * @param {int[]} ccList
	 * @param {int} userId
	 * @param {Object} assignmentInfo
	 * @param {int} assignmentInfo.id
	 * @param {string} assignmentInfo.type
	 * @param {int|null} originallyCreatedBy
	 * @returns {Array}
	 */
	getAllPostUsers: async (ccList, userId, assignmentInfo, originallyCreatedBy = null) => {
		let users = ccList ?
			ccList.map(x => (
				{
					id: x,
					category: constants.postUser.CC
				}
			)) :
			[]

		users.push({
			id: userId,
			category: constants.postUser.CREATOR
		})

		if (assignmentInfo.type === constants.assignmentTypes.EMPLOYEE) {
			//TODO: vvs p1 ASSIGNEE
			users.push({
				id: assignmentInfo.id,
				category: constants.postUser.EXECUTOR
			})
		}

		if (originallyCreatedBy && originallyCreatedBy !== userId) {
			users.push({
				id: originallyCreatedBy,
				category: constants.postUser.CC
			})
		}

		return users
	},

	/**
	 * Inserts post users.
	 * @param {int} hotelId
	 * @param {int} newId
	 * @param {Array} postUsers
	 * @param {int} postUsers.id
	 * @param {string} postUsers.category
	 * @param {Date} now
	 * @returns {*|Promise}
	 */
	insertPostUsers: async (hotelId, newId, postUsers, now) => {
		const users = postUsers.map(x => [hotelId, newId, x.id, 0, x.category, now])

		return await postUserDb.insertBulk(users)
	},

	/**
	 * Inserts post tags.
	 * @param {int} hotelId
	 * @param {int} newId
	 * @param {Object} post
	 * @returns {*|Promise}
	 */
	insertPostTags: async (hotelId, newId, post) => {
		const postTags = post.tags.map(x => [hotelId, newId, x, 0])

		if (postTags.length > 0) return await postTagDb.insertBulk(postTags)

		return await []
	},

	/**
	 * Inserts read statuses.
	 * @param {int} hotelId
	 * @param {int} newId
	 * @param {int} userId
	 * @param {int[]} postDepartmentIds
	 * @param {int[]} postUsersIds
	 * @returns {*|Promise}
	 */
	insertReadStatuses: async (hotelId, newId, userId, postDepartmentIds, postUsersIds) => {
		const departmentEmployees = await employeeDepartmentDb.getByDepartmentIds(postDepartmentIds)
		const allUsers = await userDb.getByHotelId(hotelId)
		const admins = allUsers.
			filter(x => x.roleId === constants.role.ADMIN).
			map(x => x.id)
		const allReaders = departmentEmployees.map(x => x.userId).concat(postUsersIds, admins)
		const uniqueReaders = _.uniq(allReaders)
		const readStatuses = uniqueReaders.map(x =>
			[hotelId, x, newId, x === userId ?
				constants.postReadStatus.READ :
				constants.postReadStatus.UNREAD
			])

		return await postReadStatusDb.insertBulk(readStatuses)
	},

	/**
	 * Inserts comment.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} parentId
	 * @param {object} commentData
	 * @returns {*|Promise}
	 */
	insertComment: async (hotelId, userId, parentId, commentData) => {
		const comment = commentData.comment
		const attachments = commentData.attachments
		const commentText = comment.description

		if ((!commentText || commentText.trim() === '') && (!attachments || attachments.length === 0)) {
			throw new BadRequestError('Empty comment not allowed', {
				hotelId: hotelId,
				userId: userId,
				postId: parentId,
				status: comment
			})
		}

		const hotel = await hotelDb.getById(hotelId)

		if (!hotel) {
			throw new BadRequestError('Can not find a hotel', {
				hotelId: hotelId,
				userId: userId,
				postId: parentId,
				status: comment
			})
		}

		const newComment = Post.getNewComment(hotelId, userId, parentId, commentText, hotel.timeZone)
		const newCommentId = await postDb.insertComment(hotelId, userId, parentId, newComment)

		if (newCommentId < 0) {
			throw new BadRequestError('Failed to add comment', {
				hotelId: hotelId,
				userId: userId,
				postId: parentId,
				status: comment
			})
		}

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')

		if (attachments) {
			attachmentLogic.addAttachments(newCommentId, constants.attachmentSources.COMMENTS, attachments, userId, nowUtc)
		}

		await postDb.updatePostAfterCommentAdded(parentId, nowUtc)

		const commentDepartmentIds = await postLogic.getAllCommentDepartments(hotelId, comment)

		//TODO: ak p1 ask vvs do we still need postDepartmentDb.update here
		await postDepartmentDb.update(hotelId, parentId, nowUtc)

		await postLogic.insertCommentDepartments(hotelId, newCommentId, parentId, commentDepartmentIds, nowUtc)
		await postLogic.insertCommentUsers(hotelId, newCommentId, parentId, userId, comment.ccList, nowUtc)
		await postLogic.insertCommentTags(hotelId, newCommentId, parentId, comment)
		await postLogic.updatePostReadStatusesAfterCommentAdded(hotelId, parentId, userId, comment.ccList,
			commentDepartmentIds)

		notificationProcessor.notifyAboutCommentCreated(hotelId, userId, parentId, newCommentId, newComment)

		return newCommentId
	},

	/**
	 * Gets all departments related to the comment.
	 *
	 * @param {int} hotelId
	 * @param {Object} comment
	 * @param {int[]} comment.maintenanceTypeList
	 * @param {int[]} comment.departments
	 * @returns {int[]}
	 */
	getAllCommentDepartments: async (hotelId, comment) => {
		const maintenanceTypeList = comment.maintenanceTypeList || []
		const maintenanceDepartment = maintenanceTypeList.length > 0 ?
			await hotelLogic.getMaintenanceDepartment(hotelId) :
			null
		const departmentIds = maintenanceTypeList.length > 0 ?
			[maintenanceDepartment.id].concat(comment.departments) :
			[].concat(comment.departments)

		return departmentIds
	},

	/**
	 * Gets all users with category related to the comment.
	 *
	 * @param {int[]} ccList
	 * @param {int} userId
	 * @param {int|null} childId
	 * @returns {Array}
	 */
	getAllCommentUsers: (ccList, userId, childId = null) => {
		let users = ccList ?
			ccList.map(x => (
				{
					id: x,
					category: constants.postUser.CC
				}
			)) :
			[]

		users.push({
			id: userId,
			category: childId ?
				constants.postUser.COMMENTATOR :
				constants.postUser.CREATOR
		})

		return users
	},

	/**
	 * Inserts comment departments.
	 *
	 * @param {int} hotelId
	 * @param {int} commentId
	 * @param {int} postId
	 * @param {int[]} departmentIds
	 * @param {Date|string} nowUtc
	 * @returns {*|Promise}
	 */
	insertCommentDepartments: async (hotelId, commentId, postId, departmentIds, nowUtc) => {
		await postLogic.updateOrInsertPostDepartments(hotelId, commentId, departmentIds, nowUtc)
		await postLogic.updateOrInsertPostDepartments(hotelId, postId, departmentIds, nowUtc, commentId)
	},

	/**
	 * Inserts comment users.
	 *
	 * @param {int} hotelId
	 * @param {int} commentId
	 * @param {int} postId
	 * @param {int} userId
	 * @param {int[]} ccList
	 * @param {Date|string} nowUtc
	 * @returns {*|Promise}
	 */
	insertCommentUsers: async (hotelId, commentId, postId, userId, ccList, nowUtc) => {
		const commentUsers = postLogic.getAllCommentUsers(ccList, userId)
		await postLogic.updateOrInsertPostUsers(hotelId, commentId, commentUsers, nowUtc)

		const postUsers = postLogic.getAllCommentUsers(ccList, userId, commentId)
		await postLogic.updateOrInsertPostUsers(hotelId, postId, postUsers, nowUtc, commentId)
	},

	/**
	 * Inserts comment tags.
	 *
	 * @param {int} hotelId
	 * @param {int} commentId
	 * @param {int} postId
	 * @param {Object} comment
	 * @returns {*|Promise}
	 */
	insertCommentTags: async (hotelId, commentId, postId, comment) => {
		await postLogic.updateOrInsertPostTags(hotelId, commentId, comment)
		await postLogic.updateOrInsertPostTags(hotelId, postId, comment, commentId)
	},

	/**
	 * Updates post read statuses after comment added
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {int} userId
	 * @param {int[]} ccList
	 * @param {int[]} departmentIds
	 * @returns {*|Promise}
	 */
	updatePostReadStatusesAfterCommentAdded: async (hotelId, postId, userId, ccList, departmentIds) => {
		const commentUsers = postLogic.getAllCommentUsers(ccList, userId)
		const commentUsersIds = commentUsers.map(x => x.id)

		return await postLogic.updateOrInsertReadStatuses(hotelId, postId, userId, departmentIds, commentUsersIds)
	},

	/**
	 * Converts note to a post.
	 *
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {Object} props
	 * @param {string|null} props.typeId
	 * @param {string|null} props.description
	 * @param {string|null} props.subject
	 * @param {string|null} props.assignmentType
	 * @param {int|null} props.assignmentId
	 * @param {string|null} props.dueDateTime
	 * @param {int[]|null} props.departments
	 * @param {int[]|null} props.tags
	 * @param {int[]|null} props.ccList
	 * @param {int|null} props.originallyCreatedBy
	 * @return {*}
	 */
	convertNoteToTask: async (hotelId, userId, postId, props) => {
		const user = await userDb.getById(userId)
		const hotel = await hotelDb.getById(hotelId)

		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const nowHotel = utcToTimezone(nowUtc, hotel.timeZone)

		const updatedRowsNumber = await postLogic.convertPostToTask(hotel, user, postId, props, nowUtc, nowHotel)

		const sourceType = constants.attachmentSources.TASKS
		postLogic.addAttachments(postId, sourceType, props.attachments, userId, nowUtc)

		const assignmentInfo = assignAssignmentInfo(props)
		const postDepartmentIds = await postLogic.getAllPostDepartments(hotelId, user, props, assignmentInfo)
		const postUsers = await postLogic.getAllPostUsers(props.ccList, userId, assignmentInfo, props.originallyCreatedBy)
		const postUsersIds = postUsers.map(x => x.id)

		if (props.originallyCreatedBy !== userId) {
			await postUserDb.delete(hotelId, postId, props.originallyCreatedBy, constants.postUser.CREATOR)
			await postUserDb.delete(hotelId, postId, userId, constants.postUser.CC)
		}

		await postLogic.updateOrInsertPostDepartments(hotelId, postId, postDepartmentIds, nowUtc)
		await postLogic.updateOrInsertPostUsers(hotelId, postId, postUsers, nowUtc)
		await postLogic.updateOrInsertPostTags(hotelId, postId, props)
		await postLogic.updateOrInsertReadStatuses(hotelId, postId, userId, postDepartmentIds, postUsersIds)

		// TODO: ak p3 translationProcessor.translate???
		// TODO: ak p3 notificationProcessor.notifyAboutPostCreated???

		return _.merge({
			updatedDate: nowHotel,
			updatedRowsNumber: updatedRowsNumber
		}, props)
	},

	/**
	 * Converts a post record to task.
	 *
	 * @param {Object} hotel
	 * @param {Object} user
	 * @param {int} postId
	 * @param {Object} props
	 * @param {string|null} props.typeId
	 * @param {string|null} props.description
	 * @param {string|null} props.subject
	 * @param {string|null} props.assignmentType
	 * @param {int|null} props.assignmentId
	 * @param {string|null} props.dueDateTime
	 * @param {bool|null} props.guestComplaint
	 * @param {Date} nowUtc
	 * @param {Date} nowHotel
	 * @return {*}
	 */
	convertPostToTask: async (hotel, user, postId, props, nowUtc, nowHotel) => {
		if (props.dueDateTime) {
			//TODO: vvs p2 REFACTOR - no props mutation
			props.dueDateTimeUtc = timezoneToUtc(props.dueDateTime, hotel.timeZone)
		}

		const updatedProps = {
			typeId: props.typeId,
			description: props.description,
			subject: props.subject,
			assignmentType: props.assignmentType,
			assignmentId: props.assignmentId,
			dueDateTime: props.dueDateTime,
			dueDateTimeUtc: props.dueDateTimeUtc,
			guestComplaint: props.guestComplaint,
			createdDateTime: nowHotel,
			createdDateTimeUtc: nowUtc,
			flag: null,
			flaggedDateTime: null,
			flaggedDateTimeUtc: null,
			flagDueDateTime: null,
			flagDueDateTimeUtc: null
		}

		return await postDb.convertPostToTask(hotel.id, user.id, postId, updatedProps)
	},

	updateOrInsertPostDepartments: async (hotelId, postId, departmentIds, now, childId = 0) => {
		logger.info('postLogic|updateOrInsertPostDepartments', {hotelId, postId, departmentIds, now})

		const postDepartments = departmentIds.map(x => [hotelId, postId, x, childId, now])

		if (postDepartments.length > 0) return await postDepartmentDb.updateOrInsertBulk(postDepartments)

		return await []
	},

	updateOrInsertPostUsers: async (hotelId, postId, postUsers, now, childId = 0) => {
		logger.info('postLogic|updateOrInsertPostUsers', {hotelId, postId, postUsers, now})

		const users = postUsers.map(x => [hotelId, postId, x.id, childId, x.category, now])

		return await postUserDb.updateOrInsertBulk(users)
	},

	updateOrInsertPostTags: async (hotelId, postId, post, childId = 0) => {
		logger.info('postLogic|updateOrInsertPostTags', {hotelId, postId, post})

		const postTags = post.tags.map(x => [hotelId, postId, x, childId])

		if (postTags.length > 0) return await postTagDb.updateOrInsertBulk(postTags)

		return await []
	},

	updateOrInsertReadStatuses: async (hotelId, postId, userId, postDepartmentIds, postUsersIds) => {
		logger.info('postLogic|updateOrInsertReadStatuses', {hotelId, postId, userId, postDepartmentIds, postUsersIds})

		const departmentEmployees = await employeeDepartmentDb.getByDepartmentIds(postDepartmentIds)
		const allUsers = await userDb.getByHotelId(hotelId)
		const admins = allUsers.
			filter(x => x.roleId === constants.role.ADMIN).
			map(x => x.id)
		const allReaders = departmentEmployees.map(x => x.userId).concat(postUsersIds, admins)
		const uniqueReaders = _.uniq(allReaders)
		const readStatuses = uniqueReaders.map(x =>
			[hotelId, x, postId, x === userId ?
				constants.postReadStatus.READ :
				constants.postReadStatus.UNREAD
			])

		return await postReadStatusDb.updateOrInsertBulk(readStatuses)
	},

	addAttachments: (postId, sourceType, attachments, userId, nowUtc) => {
		if (!(attachments && attachments.length !== 0)) return

		attachmentLogic.addAttachments(postId, sourceType, attachments, userId, nowUtc)
	}
}

export default postLogic
