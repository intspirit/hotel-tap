'use strict'

import _ from 'lodash'
import Promise from 'bluebird'
import config from '../config/environment'
import logger from '../core/logger'
import mailer from '../core/mailer'
import constants from '../../shared/constants'
import {
	departmentDb,
	hotelDb,
	notificationSettingDb,
	postDepartmentDb,
	postTagDb,
	postUserDb,
	tagNotificationSettingDb,
	userDb
} from '../db'
import jobProcessor from './jobProcessor'
import templateProcessor from './templateProcessor'

//TODO: ak p1 refactoring it
const assignAssignmentTo = async (post) => {
	if (post.assignmentType === constants.assignmentTypes.DEPARTMENT) {
		const department = await departmentDb.getById(post.assignmentId)

		return department.name
	}

	if (post.assignmentType === constants.assignmentTypes.EMPLOYEE) {
		const user = await userDb.getById(post.assignmentId)

		return user.fullName
	}

	return ''
}

//TODO: ak p1 refactoring it
const assignBoard = async (post) => {
	if (post.board.includes(constants.boardTypes.DEPARTMENT)) {
		const departmentId = parseInt(post.board.split('/')[1], 10)
		const department = await departmentDb.getById(departmentId)

		return department.name
	}

	if (post.board.includes(constants.boardTypes.EMPLOYEE)) {
		const employeeId = parseInt(post.board.split('/')[1], 10)
		const user = await userDb.getById(employeeId)

		return user.fullName
	}

	return ''
}

const notificationProcessor = {

	/**
	 * Gets all users ids to recieve notifications
	 *
	 * @param  {int} hotelId
	 * @param  {int} userId
	 * @param  {int[]} departmentIds
	 * @param  {int[]} userIds
	 * @param  {int[]} tagIds
	 * @return {int[]}
	 */
	getEmailNotificationUserIds: async (hotelId, userId, departmentIds, userIds, tagIds) => {
		const notificationSettingsByDepartments = await notificationSettingDb.getByDepartmentIds(hotelId, departmentIds)
		const notificationSettingsByTags = await tagNotificationSettingDb.getByTagIds(hotelId, tagIds)
		const notificationUserIds = _.chain(notificationSettingsByDepartments).
				concat(notificationSettingsByTags).
				filter(x => x.email_notes === 1 || x.email_task === 1).
				map(x => x.user_id).
				concat(userIds).
				uniq().
				filter(x => x !== userId).
				value()

		return notificationUserIds
	},

	/**
	 * Sends email notificaions to users
	 *
	 * @param  {Object} users
	 * @param  {Object} email
	 * @param  {int} jobId
	 * @return {Array}
	 */
	sendEmailNotifications: async (users, email, jobId) => {
		const useEmailJobIds = await jobProcessor.submitBulk(
			constants.job.NOTIFICATION,
			users.map(x => x.id),
			JSON.stringify({
				notificationJobId: jobId,
				email: email
			})
		)

		await jobProcessor.updateStatus(jobId, constants.jobStatus.DONE, JSON.stringify(useEmailJobIds))

		return await Promise.map(users, async (x) => {
			try {
				const sendMailResult = await mailer.send(config.brand.mailFromInfoShort, x.notificationEmail, email)
				await jobProcessor.updateStatus(useEmailJobIds.get(x.id), constants.jobStatus.DONE,
					JSON.stringify(_.merge({
						from: config.brand.mailFromInfoShort,
						to: x.notificationEmail
					}, sendMailResult))
				)

				return sendMailResult
			} catch (err) {
				const sendMailResult = {
					code: err.code,
					statusCode: err.statusCode,
					message: err.message
				}

				await jobProcessor.updateStatus(useEmailJobIds[x.id], constants.jobStatus.FAILED,
					JSON.stringify(sendMailResult)
				)

				return sendMailResult
			}
		})
	},

	/**
	 * Sends notifications when a new post created
	 *
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {Object} post
	 * @param {int[]} departmentIds
	 * @returns {*}
	 */
	notifyAboutPostCreated: async (hotelId, userId, postId, post, departmentIds) => {
		logger.info('notificationProcessor|notifyAboutPostCreated', {hotelId, userId, postId, post, departmentIds})

		const user = await userDb.getById(userId)
		const hotel = await hotelDb.getById(hotelId)
		const jobMeta = {
			postId: postId
		}

		const jobId = await jobProcessor.submit(constants.job.NOTIFICATION, postId, JSON.stringify(jobMeta))

		notificationProcessor.notifyAboutPostCreatedViaEmail(hotel, user, postId, post, departmentIds, jobId)

		//TODO: 3 different notification
	},

	/**
	 * Sends email notifications when a new post created
	 *
	 * @param  {Object} hotel
	 * @param  {Object} user
	 * @param  {int} postId
	 * @param  {Object} post
	 * @param  {int[]} post.tags
	 * @param  {int[]} post.ccList
	 * @param  {string} post.board
	 * @param  {int} post.assignmentId
	 * @param  {string} post.assignmentType
	 * @param  {int[]} departmentIds
	 * @param  {int} jobId
	 * @return {*}
	 */
	notifyAboutPostCreatedViaEmail: async (hotel, user, postId, post, departmentIds, jobId) => {
		logger.info('notificationProcessor|notifyAboutPostCreatedViaEmail',
			{hotelId: hotel.id, userId: user.id, postId, post, departmentIds, jobId})

		const notificationUserIds = await notificationProcessor.getEmailNotificationUserIds(hotel.id, user.id,
			departmentIds, post.ccList, post.tags)
		const notificationUsers = await userDb.getByIds(notificationUserIds)
		const usersWithEmailAndLetNotifications = notificationUsers.
			filter(x => x.notificationEmail && x.notificationStatus)

		if (usersWithEmailAndLetNotifications.length > 0) {
			//TODO: ak p1 refactoring it
			const board = await assignBoard(post)
			//TODO: ak p1 refactoring it
			const assignmentTo = await assignAssignmentTo(post)
			const locals = {hotel, user, post, board, assignmentTo, postId}
			const email = templateProcessor.getEmail(constants.emailTemplates.POST_CREATED, locals)

			return await notificationProcessor.sendEmailNotifications(usersWithEmailAndLetNotifications, email, jobId)
		}

		return []
	},

	/**
	 * Sends notifications when a new comment created
	 *
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {int} commentId
	 * @param {Object} comment
	 * @returns {*}
	 */
	notifyAboutCommentCreated: async (hotelId, userId, postId, commentId, comment) => {
		logger.info('notificationProcessor|notifyAboutCommentCreated', {hotelId, userId, postId, commentId, comment})

		const user = await userDb.getById(userId)
		const hotel = await hotelDb.getById(hotelId)
		const jobMeta = {
			postId: postId,
			commentId: commentId
		}

		const jobId = await jobProcessor.submit(constants.job.NOTIFICATION, postId, JSON.stringify(jobMeta))

		notificationProcessor.notifyAboutCommentCreatedViaEmail(hotel, user, postId, commentId, comment, jobId)
	},

	/**
	 * Sends email notifications when a new comment created
	 *
	 * @param  {Object} hotel
	 * @param  {Object} user
	 * @param  {int} postId
	 * @param  {int} commentId
	 * @param  {Object} comment
	 * @param  {int} jobId
	 * @return {*}
	 */
	notifyAboutCommentCreatedViaEmail: async (hotel, user, postId, commentId, comment, jobId) => {
		logger.info('notificationProcessor|notifyAboutPostCreatedViaEmail',
			{hotelId: hotel.id, userId: user.id, postId, commentId, comment, jobId})

		const postDepartments = await postDepartmentDb.getByPostId(hotel.id, postId)
		const postDepartmentIds = postDepartments.map(x => x.department_id)

		const postUsers = await postUserDb.getByPostIds(hotel.id, [postId])
		const postUserIds = postUsers.map(x => x.userId)

		const postTags = await postTagDb.getByPostIds(hotel.id, [postId])
		const postTagIds = postTags.map(x => x.tagId)

		const notificationUserIds = await notificationProcessor.getEmailNotificationUserIds(hotel.id, user.id,
			postDepartmentIds, postUserIds, postTagIds)
		const notificationUsers = await userDb.getByIds(notificationUserIds)
		const usersWithEmailAndLetNotifications = notificationUsers.
			filter(x => x.notificationEmail && x.notificationStatus)

		if (usersWithEmailAndLetNotifications.length > 0) {
			const locals = {hotel, user, comment, postId}
			const email = templateProcessor.getEmail(constants.emailTemplates.COMMENT_CREATED, locals)

			return await notificationProcessor.sendEmailNotifications(usersWithEmailAndLetNotifications, email, jobId)
		}

		return []
	}
}

export default notificationProcessor
