'use strict'

import _ from 'lodash'
import crypto from 'crypto'
import userDb from '../db/userDb'
import {LoginError} from '../errors'
import constants from '../../shared/constants'
import utils from '../core/utils'

export default {

	/**
	 * Logs user in.
	 * @param {object} req
	 * @param {object} req.loginRequest
	 * @param {string} req.password
	 * @return {{token: *, user: *}}
	 * @throws {LoginError}
	 */
	async login(req) {
		if (this.invalidLoginRequest(req.loginRequest, req.password)) throw new LoginError('Invalid parameters', req)

		return req.loginRequest.userType === constants.userType.ADMIN ?
			this.loginAdmin(req.loginRequest, req.password) :
			this.loginEmployee(req.loginRequest, req.password)
	},

	/**
	 * Validates login request parameters.
	 * @param {object} request
	 * @param {string} password
	 * @return {boolean}
	 */
	invalidLoginRequest(request, password) {
		if (!password || !request.userName) {
			return true
		}

		if ([constants.userType.ADMIN, constants.userType.EMPLOYEE].indexOf(request.userType) === -1) {
			return true
		}

		if (request.userType === constants.userType.EMPLOYEE && !request.hotelId) {
			return true
		}

		return false
	},


	/**
	 * Logs employee in.
	 * @param {object} req
	 * @param {string} password
	 * @return {{token: *, user: *}}
	 * @throws {LoginError}
	 */
	async loginEmployee(req, password) {
		const user = await userDb.getEmployeeForLogin(req.hotelId, req.userName)

		return this.getLoginResponse(req, user, password)
	},

	/**
	 * Logs admin in.
	 * @param {object} req
	 * @param {string} password
	 * @return {{token: *, user: *}}
	 * @throws {LoginError}
	 */
	async loginAdmin(req, password) {
		const users = await userDb.getAdminForLogin(req.userName)

		if (users.length === 0) throw new LoginError('User Not Found', req)

		const user = _.find(users, x => x.validPassword(password))
		if (!user) throw new LoginError('User Not Found', req)

		return this.getLoginResponse(req, user, password)
	},

	/**
	 * Generates JWT token.
	 * @param {object} user
	 * @param {boolean} remember
	 * @return {string}
	 */
	generateToken: (user, remember) => utils.generateJWT(user, remember),

	/**
	 * Generates One-off access token.
	 * @param {object} user
	 * @return {string}
	 */
	accessToken: user => {
		const token = crypto.randomBytes(32).toString('hex')
		userDb.setAccessToken(user.id, token)

		return token
	},

	/**
	 * Validates user and returns login response.
	 * @param {object} req
	 * @param {object} user
	 * @param {string} password
	 * @return {{token: *, user: *}}
	 * @throws {LoginError}
	 */
	getLoginResponse(req, user, password) {
		if (!user) throw new LoginError('User Not Found', req)

		if (!user.validPassword(password)) throw new LoginError('Invalid password', req)

		return {
			token: this.generateToken(user),
			user: user.commonDetails
		}
	}
}
