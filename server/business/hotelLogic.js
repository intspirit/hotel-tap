'use strict'

import {hotelDb, departmentDb} from '../db'
import logger from '../core/logger'

const hotelLogic = {

	/**
	 * Gets hotel timezone
	 * @param {int} hotelId identifier of a hotel
	 * @returns {string}
	 */
	async getTimezone(hotelId) {
		let timezone = null
		try {
			let hotel = await hotelDb.getById(hotelId)
			if (hotel) {
				timezone = hotel.timeZone
			}
		} catch (error) {
			let errorMessage = error ? error.message : 'Error is undefined'
			logger.log(`error|getHotelTimezone|error:${errorMessage}`)
		}

		return timezone
	},

	/**
	 * Gets hotel maintenance department
	 *
	 * @param {int} hotelId identifier of a hotel
	 * @returns {Object}
	 */
	async getMaintenanceDepartment(hotelId) {
		let department = null

		try {
			const departments = await departmentDb.getByHotelId(hotelId)
			department = departments.find(x => x.isMaintenance)
		} catch (error) {
			let errorMessage = error ? error.message : 'Error is undefined'
			logger.log(`error|getMaintenanceDepartment|error:${errorMessage}`)
		}

		return department
	}
}

export default hotelLogic
