'use strict'

import config from '../config/environment'

export default {

	getSettings() {
		return {
			environment: config.env,
			adminUrl: config.adminApp.url
		}
	}
}
