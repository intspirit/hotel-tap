'use strict'

import {postPMDb, areaDb, hotelDb} from '../db'
import constants from '../../shared/constants'
import utils from '../core/utils'
import logger from '../core/logger'

/**
 * PM business logic.
 */
const pmLogic = {

	/**
	 * Gets PM by id.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @return {Promise.<Object>}
	 */
	getById: async(hotelId, userId, postId) => {
		logger.info('api/pmLogic|getById:', {hotelId, userId, postId})

		const [pm, tasks] = await Promise.all([
			postPMDb.getById(hotelId, postId),
			postPMDb.getByParentIds(hotelId, [postId], constants.postTypes.PM_TASK)
		])
		const locations = pm.locationType === constants.locationType.ROOM ?
			await areaDb.getRoomsByHotelId(hotelId) :
			await areaDb.getAreasByHotelId(hotelId)

		pm.tasks = tasks
		pm.locations = locations

		return pm
	},

	/**
	 * Updates the PM.
	 * @param {int} hotelId
	 * @param {Object} user
	 * @param {int} pmId
	 * @param {Object} changes - changes made to PM.
	 * @returns {number}
	 */
	update: async(hotelId, user, pmId, changes) => {
		logger.info('api/pmLogic|update:', {hotelId, user, pmId, changes})

		//TODO: vvs p2 update breaks after partial change
		//TODO: vvs p2 admin permission (middleware)

		let result = 0
		if (user.roleId === constants.role.ADMIN && changes.name) {
			result += await postPMDb.updatePM(
				hotelId, pmId, {
					name: changes.name,
					description: changes.description,
					assignmentType: changes.assignmentType,
					assignmentId: changes.assignmentId
				})
		}

		if (user.roleId === constants.role.ADMIN && changes.tasks && changes.tasks.length > 0) {
			const pmTasks = changes.tasks.map(x => [
				x.id === -1 ? null : x.id,
				constants.postTypes.PM_TASK,
				hotelId,
				pmId,
				x.subject,
				x.sortOrder,
				x.changeStatus === constants.changeStatus.DELETE ? 1 : 0,
				''
			])

			result += await postPMDb.insertUpdatePMTasksBulk(pmTasks)
		}

		if (changes.schedules && changes.schedules.length > 0) {
			const hotel = await hotelDb.getById(hotelId)

			const pmSchedules = changes.schedules.map(x => [
				x.id < 0 ? null : x.id,
				constants.postTypes.PM_SCHEDULE,
				hotelId,
				pmId,
				x.dueDateTimeString,
				x.changeStatus === constants.changeStatus.DELETE ?
					null :
					utils.timezoneToUtc(x.dueDateTimeString, hotel.timeZone),
				constants.taskStatus.OPEN,
				x.location.type,
				x.location.id,
				x.changeStatus === constants.changeStatus.DELETE ? 1 : 0,
				''
			])

			result += await postPMDb.insertUpdatePMSchedulesBulk(pmSchedules)
		}

		return result
	}
}

export default pmLogic
