'use strict'

import _ from 'lodash'
import moment from 'moment-timezone'

import logger from '../core/logger'
import {postDb} from '../db'
import Post from '../models/Post'
import hotelLogic from './hotelLogic'
import postLogic from './postLogic'
import constants from '../../shared/constants'

const sortTypes = constants.todoSortTypes
const groupNames = constants.todoGroupNames
const todoTypes = constants.postTypes

const todoTypesForDbQuery = [
	constants.postTypes.TASK,
	constants.postTypes.CHECKLIST_SCHEDULE,
	constants.postTypes.PM_SCHEDULE,
	constants.postTypes.INSPECTION_SCHEDULE
]

const requiredToDoProperties = ['id', 'typeId', 'subject', 'completion', 'dueDateTime', 'description',
	'descriptionText', 'dueDateTimeUtc']

const requiredCompletionProperties = ['status']

const itemsCountForRoomLogs = (roomLogs) =>
	roomLogs.reduce((count, roomLog) => count + parseInt((roomLog.rooms || {}).count, 10), 0)

const normalizePost = post => {
	const postWithRequiredProperties = _.pick(post, requiredToDoProperties)
	postWithRequiredProperties.completion = _.pick(postWithRequiredProperties.completion, requiredCompletionProperties)

	return postWithRequiredProperties
}

const createTodoGroup = (name, title, items) => ({
	name: name,
	title: title,
	collection: {
		count: name === groupNames.PM_SCHEDULE ? itemsCountForRoomLogs(items) : items.length,
		items: _.map(items, item => {
			if (item.collection && item.collection.count) {
				item.collection.items = _.map(item.collection.items, normalizePost)

				return item
			}

			return normalizePost(item)
		})
	}
})

const createDueDateSortGroups = (overdue, incomplete, future) => [
	createTodoGroup(groupNames.OVERDUE, 'Overdue', overdue || []),
	createTodoGroup(groupNames.TODAY, 'Today', incomplete || []),
	createTodoGroup(groupNames.FUTURE, 'Future', future || [])
]

const createTodoTypeSortGroups = (tasks, checkLists, inspections, roomLogs) => [
	createTodoGroup(groupNames.TASKS, 'Tasks', tasks || []),
	createTodoGroup(groupNames.CHECKLIST_SCHEDULE, 'Checklists', checkLists || []),
	createTodoGroup(groupNames.INSPECTIONS, 'Inspections', inspections || []),
	createTodoGroup(groupNames.PM, 'Room Logs', roomLogs || [])
]

const getHotelMoments = (timeZone) => {
	let date = moment().tz(timeZone)

	let endOfDay = date.clone().endOf('day')

	return {
		hotelNowInUTC: date.utc(),
		endOfHotelDayInUTC: endOfDay.utc()
	}
}

const momentComparerFactory = baseDateTime => comparingDatetime => baseDateTime.isBefore(comparingDatetime)

const createTodayAndFutureComparers = timezone => {
	const {hotelNowInUTC, endOfHotelDayInUTC} = getHotelMoments(timezone)

	return {
		isAfterToday: momentComparerFactory(endOfHotelDayInUTC),
		isTodayLater: momentComparerFactory(hotelNowInUTC)
	}
}

const markTodoByDueDateTimeFactory = (isAfterToday, isTodayLater) => todo => {
	if (isAfterToday(todo.dueDateTimeUtc)) {
		todo.isFuture = true
	} else if (isTodayLater(todo.dueDateTimeUtc)) {
		todo.isToday = true
	} else {
		todo.isOverdue = true
	}
}

const mergePMScheduleProperties = (post, dbTodo) => {
	const newPost = Object.assign({}, post, {
		subject: dbTodo.parent_subject,
		description: dbTodo.parent_description,
		languageId: dbTodo.parent_language_id,
		location: Object.assign({}, post.location, {name: dbTodo.location_name})
	})

	return newPost
}

const mergeChecklistScheduleProperties = (post, dbTodo) => {
	const newPost = Object.assign({}, post, {
		parentSubject: dbTodo.parent_subject,
		parentDescription: dbTodo.parent_description,
		location: Object.assign({}, post.location, {name: dbTodo.location_name})
	})

	return newPost
}

const groupPMSchedules = (type, schedules) => {
	const groupedSchedules = _.groupBy(schedules, 'parentId')

	return _.map(groupedSchedules, group => ({
				subject: group[0] ? group[0].subject : '',
				description: group[0] ? group[0].description : '',
				typeId: type,
				collection: {
					count: group.length,
					items: _.map(group, schedule => Object.assign({}, schedule, {
						subject: schedule.location.name || ''
					}))
				}
			}))
}

const groupChecklistSchedules = (schedules) => {
	const groupedSchedules = _.groupBy(schedules, 'parentId')

	return _.map(groupedSchedules, group => ({
				subject: group[0] ? group[0].parentSubject : '',
				description: group[0] ? group[0].parentDescription : '',
				typeId: todoTypes.CHECKLIST_SCHEDULE,
				collection: {
					count: group.length,
					items: group
				}
			}))
}

const sortByDueDate = (posts, timezone) => {
	let today = []
	let overdue = []
	let future = []

	const {isAfterToday, isTodayLater} = createTodayAndFutureComparers(timezone)

	_.forEach(posts, post => {
		let array = null

		if (!post.dueDateTimeUtc || isAfterToday(post.dueDateTimeUtc)) {
			array = future
		} else if (isTodayLater(post.dueDateTimeUtc)) {
			array = today
		} else {
			array = overdue
		}

		if (post.typeId === todoTypes.INSPECTION_SCHEDULE || post.typeId === todoTypes.PM_SCHEDULE) {
			const prefixedLocationName = post.location.name ? ` - ${post.location.name}` : ''
			post.subject = `${post.subject} ${prefixedLocationName}`
		}

		array.push(post)
	})

	return {
		totalCount: today.length + overdue.length + future.length,
		groups: createDueDateSortGroups(overdue, today, future)
	}
}

const sortByPostType = (posts, timezone) => {
	let todoGroups = _.groupBy(posts, 'typeId')

	if (todoGroups[todoTypes.INSPECTION_SCHEDULE]) {
		todoGroups[todoTypes.INSPECTION_SCHEDULE] =
			groupPMSchedules(todoTypes.INSPECTION_SCHEDULE, todoGroups[todoTypes.INSPECTION_SCHEDULE])
	}

	if (todoGroups[todoTypes.PM_SCHEDULE]) {
		todoGroups[todoTypes.PM_SCHEDULE] = groupPMSchedules(todoTypes.PM_SCHEDULE, todoGroups[todoTypes.PM_SCHEDULE])
	}

	if (todoGroups[todoTypes.CHECKLIST_SCHEDULE]) {
		todoGroups[todoTypes.CHECKLIST_SCHEDULE] = groupChecklistSchedules(todoGroups[todoTypes.CHECKLIST_SCHEDULE])
	}

	const {isAfterToday, isTodayLater} = createTodayAndFutureComparers(timezone)
	const markTodoByDateTime = markTodoByDueDateTimeFactory(isAfterToday, isTodayLater)

	let totalCount = 0
	const markAndCount = todo => {
		markTodoByDateTime(todo)
		if (todo.isOverdue || todo.isToday || todo.isFuture) {
			totalCount += 1
		}
	}

	_.forOwn(todoGroups, (todoGroup, key) =>
		_.forEach(todoGroup, (post) => {
			if (key === todoTypes.TASK) {
				return markAndCount(post)
			}

			const schedules = post.items
			if (schedules && schedules.count) {
				return _.forEach(schedules.collection, markAndCount)
			}

			return null
		})
	)

	return {
		totalCount: totalCount,
		groups: createTodoTypeSortGroups(todoGroups[todoTypes.TASK],
										todoGroups[todoTypes.CHECKLIST_SCHEDULE],
										todoGroups[todoTypes.INSPECTION_SCHEDULE],
										todoGroups[todoTypes.PM_SCHEDULE])
	}
}

const sortTodos = (todoPosts, sortType, timezone) => {
	let todos = null

	switch (sortType) {
		case sortTypes.TODO_TYPE:
			todos = sortByPostType(todoPosts, timezone)
			break
		case sortTypes.DUE_DATETIME:
		default:
			todos = sortByDueDate(todoPosts, timezone)
	}

	return todos
}

const prepareTodos = async (dbTodos, user, hotelId, sortType) => {
	const timezone = await hotelLogic.getTimezone(hotelId)
	if (!timezone) {
		throw new Error('Timezone is undefined')
	}

	if (dbTodos.length && user.defaultLanguage) {
		await postLogic.updateDbPostsWithTranslationsIfRequired(hotelId, user.defaultLanguage, dbTodos)
	}

	const posts = _.map(dbTodos, item => {
		let post = new Post(item)

		if (post.typeId === todoTypes.INSPECTION_SCHEDULE || post.typeId === todoTypes.PM_SCHEDULE) {
			post = mergePMScheduleProperties(post, item)
		} else if (post.typeId === todoTypes.CHECKLIST_SCHEDULE) {
			post = mergeChecklistScheduleProperties(post, item)
		}

		return post
	})

	return sortTodos(posts, sortType, timezone)
}

const createErrorResponse = (error, todoSortType) => {
	const errorResponse = {
		groups: {}
	}

	if (todoSortType === sortTypes.TODO_TYPE) {
		errorResponse.groups = createTodoTypeSortGroups()
	} else if (todoSortType === sortTypes.DUE_DATETIME) {
		errorResponse.groups = createDueDateSortGroups()
	}

	return Object.assign({}, errorResponse, {
		totalCount: 0,
		error: 'Cannot get todo items'
	})
}

const todoLogic = {

	/**
	 * Gets todo items counts grouped by assignment
	 * @param {int} hotelId
	 * @param {string} sortType
	 * @returns {Object}
	 */
	async getCount (hotelId) {
		return postDb.getToDoItemsCount(hotelId, todoTypesForDbQuery)
	},

	/**
	 * Gets todo items by department id
	 * @param {int} hotelId
	 * @param {int} departmentId
	 * @param {object} user
	 * @param {string} sortType
	 * @returns {object}
	 */
	async getByDepartmentId(hotelId, departmentId, user, sortType) {
		let response = null

		try {
			const dbTodos = await postDb.getToDoItemsByDepartmentId(hotelId, departmentId, todoTypesForDbQuery)
			response = await prepareTodos(dbTodos, user, hotelId, sortType)
		} catch (error) {
			logger.log('error', `todoLogic|getByDepartmentId`, {hotelId, departmentId, user, sortType, error})
			response = createErrorResponse(error, sortType)
		}

		return response
	},

	/**
	 * Gets todo items by employee id
	 * @param {int} hotelId
	 * @param {int} employeeId
	 * @param {object} user
	 * @param {string} sortType
	 * @returns {object}
	 */
	async getByEmployeeId(hotelId, employeeId, user, sortType) {
		let response = null

		try {
			const dbTodos = await postDb.getToDoItemsByEmployeeId(hotelId, employeeId, todoTypesForDbQuery)
			response = prepareTodos(dbTodos, user, hotelId, sortType)
		} catch (error) {
			logger.log('error', `todoLogic|getByEmployeeId`, {hotelId, employeeId, user, sortType, error})
			response = createErrorResponse(error, sortType)
		}

		return response
	},

	/**
	 * Gets todo items for all employees of a hotel
	 * @param {int} hotelId
	 * @param {object} user
	 * @param {string} sortType
	 * @returns {object}
	 */
	async getForAllHotelEmployees(hotelId, user, sortType) {
		let response = null

		try {
			const dbTodos = await postDb.getEmployeesToDoItems(hotelId, todoTypesForDbQuery)
			response = prepareTodos(dbTodos, user, hotelId, sortType)
		} catch (error) {
			logger.log('error', `todoLogic|getForAllHotelEmployees`, {hotelId, user, sortType, error})
			response = createErrorResponse(error, sortType)
		}

		return response
	}
}

export default todoLogic
