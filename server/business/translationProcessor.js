'use strict'

import MsTranslator from 'mstranslator'
import Promise from 'bluebird'
import _ from 'lodash'

import {hotelLanguageDb, jobDb, translationDb} from '../db'
import config from '../config/environment'
import constants from '../../shared/constants'
import logger from '../core/logger'
import utils from '../core/utils'

const localToUtc = utils.localToUtc

Promise.promisifyAll(MsTranslator.prototype)

const msTranslator = new MsTranslator({
	client_id: config.msTranslatorAPI.clientId, // eslint-disable-line
	client_secret: config.msTranslatorAPI.clientSecret // eslint-disable-line
}, true)

/**
 * Wraps MsTranslator translate method
 * @param {string} text
 * @param {string} fromLanguage
 * @param {string} toLanguage
 * @returns {string}
 */
const translateText = async (text, fromLanguage, toLanguage) => {
	let translation = text

	if (text) {
		translation = await msTranslator.translateAsync({
			text: text,
			from: fromLanguage,
			to: toLanguage
		})
	}

	return translation
}

/**
 * Translation processor
 */
export default {

	/**
	 * Translates a post asynchronously
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {Object} post
	 * @param {string} fromLanguage
	 * @returns {void}
	 */
	async translate(hotelId, postId, post, fromLanguage) {
		try {
			const hotelLanguages = await hotelLanguageDb.getByHotelId(hotelId)

			const activeLanguages = _.chain(hotelLanguages).
										filter('status').
										map('code').
										value()

			const toLanguages = _.without(activeLanguages, fromLanguage)

			if (toLanguages.length && fromLanguage) {
				const job = {
					postId: postId,
					name: constants.job.TRANSLATION,
					meta: '',
					createdDateTimeUtc: localToUtc(new Date())
				}

				await jobDb.insert(job)

				await Promise.map(toLanguages, async (toLanguage) => {
					const {subject, description} = await Promise.props({
						subject: translateText(post.subject, fromLanguage, toLanguage),
						description: translateText(post.description, fromLanguage, toLanguage)
					})

					await translationDb.insert(hotelId, postId, toLanguage, subject, description)
				})
			}
		} catch (error) {
			logger.log('error', 'translationProcessor|translate', error)
		}
	}
}
