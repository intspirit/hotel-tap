'use strict'

import appLogic from './appLogic'
import attachmentLogic from './attachmentLogic'
import authLogic from './authLogic'
import hotelLogic from './hotelLogic'
import inspectionLogic from './inspectionLogic'
import jobProcessor from './jobProcessor'
import notificationProcessor from './notificationProcessor'
import pmLogic from './pmLogic'
import postLogic from './postLogic'
import reportLogic from './reportLogic'
import tagLogic from './tagLogic'
import templateProcessor from './templateProcessor'
import todoLogic from './todoLogic'
import translationProcessor from './translationProcessor'
import userLogic from './userLogic'

module.exports = {
	appLogic,
	attachmentLogic,
	authLogic,
	hotelLogic,
	inspectionLogic,
	jobProcessor,
	notificationProcessor,
	postLogic,
	pmLogic,
	reportLogic,
	tagLogic,
	templateProcessor,
	todoLogic,
	translationProcessor,
	userLogic
}
