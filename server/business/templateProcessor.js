'use strict'

import logger from '../core/logger'

const templateProcessor = {
	getEmail (name, locals) {
		logger.info('templateProcessor|getEmail', {name, locals})

		const subject = require(`../templates/${name}/subject`).render(locals)
		const html = require(`../templates/${name}/html`).render(locals)

		return {
			subject,
			html
		}
	}
}

export default templateProcessor
