'use strict'

import logger from '../core/logger'
import constants from '../../shared/constants'
import Promise from 'bluebird'
import hotelLogic from './hotelLogic'
import utils from '../core/utils'
import {areaDb, inspectionDb, postDb} from '../db'

const inspectionLogic = {

	/**
	 * Gets an inspection by id
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @returns {Promise}
	 */
	getById: async (hotelId, userId, postId) => {
		logger.log('info', 'inspectionLogic|getById', {hotelId, userId, postId})

		const [inspection, tasks, rooms] = await Promise.all([
			inspectionDb.getById(hotelId, postId),
			inspectionDb.getByParentIds(hotelId, [postId], constants.postTypes.INSPECTION_TASK),
			areaDb.getRoomsByHotelId(hotelId)
		])

		inspection.tasks = tasks.map(task => ({
			id: task.id,
			subject: task.subject,
			sortOrder: task.sortOrder,
			inspectionPoints: task.inspectionPoints || 0,
			failsAll: Boolean(task.failsAll)
		}))
		inspection.rooms = rooms

		return inspection
	},

	/**
	 * Gets a list of inspection schedules
	 * @param {int} hotelId
	 * @param {int} inspectionId
	 * @returns {*}
	 */
	getSchedules: async (hotelId, inspectionId) => {
		logger.log('info', 'inspectionLogic|getSchedules', {hotelId, inspectionId})

		const dbInspectionSchedules = await postDb.getByParentIdsAndPostTypes(hotelId, [inspectionId],
																	[constants.postTypes.INSPECTION_SCHEDULE])

		const inspectionSchedules = dbInspectionSchedules.map(dbSchedule => ({
			id: dbSchedule.id,
			employeeId: dbSchedule.inspection_by,
			supervisorId: dbSchedule.assignment_id,
			roomId: dbSchedule.location_id,
			inspectionPoints: dbSchedule.inspection_points,
			failsAll: dbSchedule.fails_all,
			completionStatus: dbSchedule.completion_status,
			completionDateTime: dbSchedule.completion_dtm,
			completionDateTimeUtc: dbSchedule.completion_dtm_utc,
			dueDateTime: dbSchedule.due_dtm,
			dueDateTimeUtc: dbSchedule.due_dtm_utc
		}))

		return inspectionSchedules
	},

	/**
	 * Updates an inspection
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} inspectionId
	 * @param {object} inspection
	 * @returns {int} number of updated rows
	 */
	update: async (hotelId, userId, inspectionId, inspection) => {
		logger.log('info', 'inspectionLogic|update', {hotelId, userId, inspectionId, inspection})

		let result = 0
		if (inspection.name) {
			result += await inspectionDb.updateInspection(hotelId, inspectionId, {
				name: inspection.name,
				description: inspection.description,
				inspectionPoints: inspection.inspectionPoints
			})
		}

		if (inspection.tasks && inspection.tasks.length > 0) {
			result += await inspectionLogic.updateInspectionTasks(hotelId, inspectionId, inspection.tasks)
		}

		if (inspection.schedules && inspection.schedules.length > 0) {
			result += await inspectionLogic.updateInspectionSchedules(hotelId, inspectionId, inspection.schedules)
		}

		return result
	},

	/**
	 * Updates a list of inspection tasks as a whole
	 * @param {int} hotelId
	 * @param {int} inspectionId
	 * @param {object[]} tasks
	 * @returns {*}
	 */
	updateInspectionTasks: async (hotelId, inspectionId, tasks) => {
		const inspectionTasks = tasks.map(task => [
			// CAUTION: order of items matters
			task.id < 0 ? null : task.id,
			constants.postTypes.INSPECTION_TASK,
			hotelId,
			inspectionId,
			task.subject,
			'',
			task.sortOrder,
			task.changeStatus === constants.changeStatus.DELETE ? 1 : 0,
			task.inspectionPoints || 0,
			task.failsAll
		])

		return inspectionDb.insertUpdateInspectionTasksBulk(inspectionTasks)
	},

	/**
	 * Updates a list of inspection schdules as a whole
	 * @param {int} hotelId
	 * @param {int} inspectionId
	 * @param {object[]} schedules
	 * @returns {*}
	 */
	updateInspectionSchedules: async(hotelId, inspectionId, schedules) => {
		const timezone = await hotelLogic.getTimezone(hotelId)

		const inspectionSchedules = schedules.map(schedule => [
			// CAUTION: order of items matters
			schedule.id < 0 ? null : schedule.id,
			constants.postTypes.INSPECTION_SCHEDULE,
			hotelId,
			inspectionId,
			schedule.employeeId,
			constants.assignmentTypes.EMPLOYEE,
			schedule.supervisorId,
			constants.locationType.ROOM,
			schedule.roomId,
			schedule.dueDateTime,
			utils.timezoneToUtc(schedule.dueDateTime, timezone),
			constants.taskStatus.OPEN,
			schedule.changeStatus === constants.changeStatus.DELETE ? 1 : 0,
			''
		])

		return inspectionDb.insertUpdateInspectionSchedulesBulk(inspectionSchedules)
	}
}

export default inspectionLogic
