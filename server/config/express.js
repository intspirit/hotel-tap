'use strict'

/**
 * Express configuration.
 */

import express from 'express'
import favicon from 'serve-favicon'
import morgan from 'morgan'
import compression from 'compression'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import errorHandler from 'errorhandler'
import path from 'path'
import helmet from 'helmet'
import config from './environment'
import logger from '../core/logger'
import {getResponseError} from '../errors'

// import swaggerJSDoc from 'swagger-jsdoc'

// function setupSwagger(app) {
// 	const swaggerDefinition = {
// 		info: {
// 			title: 'HotelTap',
// 			version: '1.0.0',
// 			description: 'HotelTap internal API for web and mobile'
// 		},
// 		host: 'localhost:9006'
// 	}
// 	const options = {
// 		swaggerDefinition: swaggerDefinition,
// 		apis: ['./server/api/index.js'],
// 		basePath: '/api'
// 	}
// 	const swaggerSpec = swaggerJSDoc(options)
//
// 	app.get('/api-docs.json', (req, res) => {
// 		res.setHeader('Content-Type', 'application/json')
// 		res.send(swaggerSpec)
// 	})
// }

/**
 * Logs API call errors.
 * @param {Object} err
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @return {void}
 */
function logErrors(err, req, res, next) { // eslint-disable-line no-unused-vars
	// logger.error(err.stack)
	logger.error(`express|logError|error:${err}`, {stack: err.stack})
	next(err)
}

function appErrorHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
	const responseError = getResponseError(err)

	return res.status(responseError.error.code).json(responseError)
}

module.exports = (app, routes, auth) => {
	const env = app.get('env')

	app.use(helmet())
	app.use(compression({}))
	app.use(bodyParser.urlencoded({extended: false}))
	app.use(bodyParser.json())
	app.use(cookieParser())

	if (env === 'development' || env === 'test') {
		app.use(favicon(path.join(config.root, 'client/public', 'favicon.ico')))
		app.set('clientPath', path.join(config.root, 'client/public'))
		app.use(express.static(path.join(config.root, 'client/public')))

		app.use(morgan('common'))
		app.use(errorHandler())

		// setupSwagger(app)
	} else {
		app.use(favicon(path.join(config.root, 'public', 'favicon.ico')))
		app.set('clientPath', path.join(config.root, 'public'))
		app.use(express.static(path.join(config.root, 'public')))
		app.use(morgan('combined'))
	}

	routes(app, auth)
	app.use(logErrors)
	app.use(appErrorHandler)
}
