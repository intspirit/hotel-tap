'use strict'

/**
 * Staging configuration.
 */
module.exports = {
	port: 8081,
	database: {
		host: process.env.DB_HOST,
		database: process.env.DB_DATABASE,
		userName: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		options: {
			logging: false,
			define: {
				timestamps: false
			}
		}
	},
	s3: {
		bucket: 'htap-files-test',
		tmpBucket: 'htap-tmp-files-test',
		useSsl: true,
		verifyPeer: true,
		accessKey: process.env.S3_KEY,
		secretKey: process.env.S3_SECRET,
		folders: {
			attachment: 'uploads',
			profile: 'img/profile',
			department: 'img/dept',
			document: 'img/doc'
		}
	},
	brand: {
		url: 'http://test.hoteltap.com'
	},
	JWT: {
		jwtSecret: process.env.JWT_SECRET,
		expirationPeriod: '15 days'
	},
	adminApp: {
		url: 'https://stage.hoteltap.com'
	},
	msTranslatorAPI: {
		clientId: process.env.MS_TEXT_TRANSLATOR_ID,
		clientSecret: process.env.MS_TEXT_TRANSLATOR_SECRET
	},
	ses: {
		accessKeyId: process.env.SES_KEY,
		secretAccessKey: process.env.SES_SECRET,
		region: 'us-west-2'
	}
}
