'use strict'

//noinspection Eslint
/**
 * Application configurations.
 * @type {any|*}
 */
const
	path = require('path'),
	_ = require('lodash'),

	environment = process.env.NODE_ENV || 'development',
	all = {
		env: environment,
		root: path.normalize(`${__dirname}${'/../../..'}`),
		port: process.env.PORT || 9006,
		ip: process.env.IP || '127.0.0.1',
		secrets: {
			session: 'undone'
		},
		s3: {
			rootUrl: 'https://s3-us-west-2.amazonaws.com/'
		},
		brand: {
			name: 'HotelTap',
			url: 'http://hoteltap.com',
			mailFromInfo: 'The HotelTap Team <info@hoteltap.com>',
			mailFromInfoShort: 'HotelTap <info@hoteltap.com>'
		}
	},
	configuration = _.merge(
		all,
		require(`./${environment}.js`) || {}
	)

module.exports = configuration
