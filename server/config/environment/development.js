'use strict'

/**
 * Development configuration.
 */
module.exports = {
	port: 9006,
	database: {
		host: 'localhost',
		database: 'shiftlinq',
		userName: 'shiftlinq',
		password: '123',
		options: {
			logging: false,
			define: {
				timestamps: false
			}
		}
	},
	s3: {
		bucket: 'htap-uploads-dev',
		tmpBucket: 'htap-tmp-files-dev',
		useSsl: true,
		verifyPeer: true,
		accessKey: '',
		secretKey: '',
		folders: {
			attachment: 'uploads',
			profile: 'img/profile',
			department: 'img/dept',
			document: 'img/doc'
		}
	},
	brand: {
		url: 'http://localhost:9006'
	},
	JWT: {
		jwtSecret: '4qQ175vAzvptNaxFt0P3w62TwIVdJqu7',
		expirationPeriod: '15 days'
	},
	adminApp: {
		url: 'http://hotels.dev'
	},
	msTranslatorAPI: {
		clientId: 'HotelTap',
		clientSecret: 'XpS2/KX/DjmJcQtw1cw/BIKY0w23rOdJpZY+EMMeaKo='
	},
	ses: {
		accessKeyId: '',
		secretAccessKey: ''
	}
}
