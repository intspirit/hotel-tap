'use strict'

/**
 * Tag model.
 *
 * @param {Object} dbData
 * @param {Number} dbData.tag_id
 * @param {String} dbData.name
 * @param {String} dbData.type
 * @param {Number} dbData.type_id
 * @param {string} dbData.type_type
 * @param {int} dbData.status
 */
class Tag {
	constructor(dbData) {
		this.id = dbData.tag_id
		this.name = dbData.name
		this.type = dbData.type
		this.typeId = dbData.type_id
		this.typeName = dbData.type_name
		this.status = dbData.status

		if ((/^room\d+$/i).test(dbData.name)) {
			let foundNumbersArray = (/\d+/).exec(dbData.name)
			if (foundNumbersArray) {
				this.roomNumber = parseInt(foundNumbersArray[0], 10)
			}
		}
	}
}

export default Tag

