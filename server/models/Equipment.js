'use strict'

/**
 * Equipment model.
 *
 * @param {Object} dbData
 * @param {Number} dbData.equipment_id
 * @param {Number} dbData.equipment_group_id
 * @param {String} dbData.name

 */
class Equipment {
	constructor(dbData) {
		this.id = dbData.equipment_id
		this.groupId = dbData.equipment_group_id
		this.name = dbData.name
	}
}

export default Equipment

