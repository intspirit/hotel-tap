'use strict'

/**
 * Post unread statuses table model.
 */
class PostReadStatus {

	/**
	 * Initializes a new instance of the PostReadStatus class.
	 * @param {Object} dbData
	 * @param {int} dbData.hotel_id
	 * @param {int} dbData.user_id
	 * @param {int} dbData.post_id
	 * @param {string} dbData.status
	 *
	 */
	constructor(dbData) {
		this.hotelId = dbData.hotel_id
		this.userId = dbData.user_id
		this.postId = dbData.post_id
		// r - read, u - unread
		this.status = dbData.status
	}
}

export default PostReadStatus

