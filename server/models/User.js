'use strict'

import _ from 'lodash'
import bcrypt from 'bcrypt-nodejs'
import utils from '../core/utils'
import constants from '../../shared/constants'

/**
 * User model.
 */
class User {

	/**
	 * @param {Object} dbData
	 * @param {int} dbData.user_id
	 * @param {int} dbData.hotel_id
	 * @param {int} dbData.notification_status
	 * @param {string} dbData.notification_email
	 * @param {string} dbData.last_name
	 * @param {string} dbData.first_name
	 * @param {string} dbData.username
	 * @param {string} dbData.company
	 * @param {string} dbData.email
	 * @param {string} dbData.password
	 * @param {int} dbData.role_id
	 * @param {string} dbData.status
	 * @param {string} dbData.profile_picture
	 * @param {string} dbData.default_lang
	 */
	constructor(dbData) {
		this.id = dbData.user_id
		this.hotelId = dbData.hotel_id
		this.notificationStatus = dbData.notification_status === 1
		this.notificationEmail = dbData.notification_email
		this.firstName = dbData.first_name
		this.lastName = dbData.last_name
		this.userName = dbData.username
		this.companyName = dbData.company
		this.email = dbData.email
		this.password = dbData.password
		this.roleId = dbData.role_id
		this.status = dbData.status
		this.imageFileName = dbData.profile_picture
		this.defaultLanguage = dbData.default_lang
	}

	get fullName() {
		return `${this.firstName} ${this.lastName}`
	}

	get isAdmin() {
		return this.roleId === constants.role.ADMIN
	}

	get imageUrl() {
		return utils.getUserImageUrl(this.imageFileName)
	}

	/**
	 * @param {string} password
	 * @returns {bool}
	 * @description preparedHash is a fix for compatibility with PHP
	 * @description more http://www.php.net/security/crypt_blowfish.php
	 * @description more http://stackoverflow.com/questions/26643587/bcrypt-hashes-generated-by-php-failing-in-node-js
	 */
	validPassword(password) {
		const preparedHash = this.password.replace('$2y$', '$2a$')

		/*eslint-disable no-sync */
		//noinspection JSUnresolvedFunction
		const result = bcrypt.compareSync(password, preparedHash)

		/*eslint-enable no-sync */

		return result
	}

	setHotelId(hotelId) {
		this.hotelId = hotelId
	}

	get commonDetails() {
		return _.pick(this, ['id', 'hotelId', 'email', 'firstName', 'lastName', 'fullName', 'isAdmin', 'imageUrl'])
	}
}

export default User
