'use strict'
import moment from 'moment-timezone'
import htmlConverter from '../../shared/core/htmlConverter'
import constants from '../../shared/constants'


/**
 * Post model.
 */
class Post {

	/**
	 * Initializes a new instance of the Post class.
	 * @param {Object} dbData
	 * @param {int} dbData.id
	 * @param {string} dbData.type_id
	 * @param {int} dbData.hotel_id
	 * @param {string} dbData.description
	 * @param {int} dbData.description_text
	 * @param {int} dbData.guest_complaint
	 * @param {int} dbData.language_id
	 * @param {int} dbData.flagged
	 * @param {int} dbData.flagged_by
	 * @param {int} dbData.flagged_dtm
	 * @param {int} dbData.flag_dtm_utc
	 * @param {int} dbData.flag_due_dtm
	 * @param {int} dbData.flagged_due_dtm_utc
	 * @param {int} dbData.private
	 * @param {int} dbData.status
	 * @param {int} dbData.created_by
	 * @param {int} dbData.created_dtm
	 * @param {int} dbData.created_dtm_utc
	 * @param {int} dbData.parent_id
	 * @param {int} dbData.order_dtm_utc
	 * @param {int} dbData.comment_count
	 * @param {int} dbData.hotel_id
	 * @param {int} dbData.attachment_count
	 * @param {string} dbData.read_status
	 * @param {string} dbData.assignment_type_id
	 * @param {int} dbData.assignment_id
	 * @param {string} dbData.subject
	 * @param {Date} dbData.due_dtm
	 * @param {Date} dbData.due_dtm_utc
	 * @param {string} dbData.completion_status
	 * @param {Date} dbData.completion_dtm
	 * @param {Date} dbData.completion_dtm_utc
	 * @param {int} dbData.completed_by
	 * @param {int} dbData.linked_post_id
	 * @param {bool} dbData.deleted
	 */
	//eslint-disable-next-line max-statements
	constructor(dbData) {
		this.id = dbData.id
		this.typeId = dbData.type_id
		this.hotelId = dbData.hotel_id
		this.description = Post.cleanDescription(dbData.description)
		this.descriptionText = dbData.description ?
			this.descriptionText = htmlConverter.convertToText(
				dbData.description.replace(constants.guestComplaint, '')) :
			''
		this.languageId = dbData.language_id
		this.private = dbData.private === 1
		this.status = dbData.status
		// this.createdBy = dbData.created_by
		this.createdDateTime = dbData.created_dtm
		this.createdDateTimeUtc = dbData.created_dtm_utc
		this.parentId = dbData.parent_id
		this.orderDateTimeUtc = dbData.order_dtm_utc
		this.commentCount = dbData.comment_count
		this.attachmentCount = dbData.attachment_count
		this.readStatus = dbData.read_status
		this.subject = dbData.subject
		this.dueDateTime = dbData.due_dtm
		this.dueDateTimeUtc = dbData.due_dtm_utc
		this.linkedPostId = dbData.linked_post_id
		this.deleted = dbData.deleted

		this.location = {
			id: dbData.location_id || -1,
			type: dbData.location_type_id,
			name: ''
		}

		this.taskDescription = {
			subject: dbData.subject,
			taskDueDateTime: this.dueDateTime
		}

		//TODO: vvs p1 fix to hotel dtm (use correct timezone)
		this.completion = {
			status: dbData.completion_status,
			dateTime: dbData.completion_dtm,
			completedBy: {
				id: dbData.completed_by || -1,
				fullName: '',
				imageUrl: ''
			}
		}
		this.assignmentTo = {
			id: -1,
			type: '',
			fullName: '',
			imageUrl: ''
		}
		this.createdBy = {
			id: -1,
			fullName: '',
			imageUrl: ''
		}
		this.comments = []
		this.attachments = []
		this.readBy = []
		this.ccUsers = []
		this.tags = []

		this.flag = {
			set: false,
			expirationDateTime: null
		}
		// if flag not expired yet
		if (dbData.flagged && dbData.flag_due_dtm_utc && moment.utc(dbData.flag_due_dtm_utc).isAfter()) {
			this.flagged = dbData.flagged
			this.flaggedDateTimeUtc = dbData.flagged_dtm_utc

			this.flag.set = dbData.flagged === 1
			this.flag.expirationDateTime = dbData.flag_due_dtm_utc
		}

		this.guestComplaint = dbData.guest_complaint === 1
		if (this.guestComplaint) {
			this.complaintResolution = {
				resolved: Boolean(dbData.resolution) &&
							dbData.resolution !== constants.complaintResolutions.UNRESOLVED,
				resolution: dbData.resolution,
				cost: dbData.cost,
				pointsGiven: dbData.points_given,
				resolutionDateTime: dbData.resolution_dtm,
				resolutionDateTimeUtc: dbData.resolution_dtm_utc,
				duration: dbData.resolution_dtm_utc && dbData.created_dtm_utc ?
							moment(dbData.resolution_dtm_utc).diff(moment(dbData.created_dtm_utc)) :
							null
			}
		}

		if (dbData.type_id === constants.postTypes.INSPECTION ||
			dbData.type_id === constants.postTypes.INSPECTION_TASK ||
			dbData.type_id === constants.postTypes.INSPECTION_SCHEDULE) {
			this.inspectionBy = dbData.inspection_by
			this.inspectionPoints = dbData.inspection_points
			this.failsAll = dbData.fails_all
		}
	}

	/**
	 * Removes hard-codded env path from URL (legacy code cleanup).
	 * @param {string}  description
	 * @returns {*}
	 */
	static cleanDescription(description) {
		//TODO: vvs p3 better logic with RegEx
		return description ?
				description.
					replace(/http(s)?:\/\/(app|stage)\.hoteltap\.com(\/index\.php)?/, '').
					replace(/hotel\/empboard\/(\d)/, 'boards/employee/$1').
					replace(/hotel\/board\/(\d)/, 'boards/departments/$1') :
				description
	}

	/**
	 * Sets the created by user.
	 * @param {User} user
	 * @constructor
	 */
	setUser(user) {
		if (!user) return

		this.createdBy.id = user.id
		this.createdBy.fullName = user.fullName
		this.createdBy.imageUrl = user.imageUrl
	}

	/**
	 * Sets the assignment.
	 * @param {string} assignmentTypeId
	 * @param {Object} assignmentInfo
	 * @constructor
	 */
	setAssignment(assignmentTypeId, assignmentInfo) {
		if (assignmentTypeId === constants.assignmentTypes.DEPARTMENT) {
			this.assignmentTo.id = assignmentInfo.id
			this.assignmentTo.type = assignmentTypeId
			this.assignmentTo.fullName = assignmentInfo.name
			this.assignmentTo.imageUrl = assignmentInfo.iconFullUrl
		} else {
			this.assignmentTo.id = assignmentInfo.id
			this.assignmentTo.type = assignmentTypeId
			this.assignmentTo.fullName = assignmentInfo.fullName
			this.assignmentTo.imageUrl = assignmentInfo.imageUrl
		}
	}

	/**
	 * Sets the completed by user.
	 * @param {User} user
	 * @constructor
	 */
	setCompletedBy(user) {
		if (!user) return

		this.completion.completedBy.id = user.id
		this.completion.completedBy.fullName = user.fullName
		this.completion.completedBy.imageUrl = user.imageUrl
	}

	/**
	 * Sets the location
	 * @param {Tag} location
	 * @constructor
	 */
	setLocation(location) {
		if (!location) return

		this.location.name = location.name
	}

	/**
	 * Get new comment for database.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {string} comment
	 * @param {string} hotelTz
	 * @return {{typeId: string, hotelId: *, description: *, descriptionText: *, languageId: string, private: boolean, createdBy: *, createdDateTime: *, createdDateTimeUtc: *, parentId: *, orderDateTimeUtc: *}}
	 */
	static getNewComment(hotelId, userId, postId, comment, hotelTz) {
		const nowUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss')
		const nowHotel = moment.utc().tz(hotelTz).
			format('YYYY-MM-DD HH:mm:ss')

		return {
			typeId: constants.postTypes.COMMENT,
			hotelId: hotelId,
			description: comment,
			descriptionText: comment,
			languageId: 'en',
			private: false,
			createdBy: userId,
			createdDateTime: nowHotel,
			createdDateTimeUtc: nowUtc,
			parentId: postId,
			orderDateTimeUtc: nowUtc
		}
	}
}

export default Post
