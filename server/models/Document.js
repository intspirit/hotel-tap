'use strict'

import utils from '../core/utils'

/**
 * Document model.
 */
class Document {

	/**
	 * Initializes a new instance of the Document class.
	 * @constructor
	 * @param {Object} dbData
	 * @param {int} dbData.list_id
	 * @param {string} dbData.title
	 * @param {string} dbData.doc_path
	 * @param {string} dbData.created_date
	 **/
	constructor(dbData) {
		this.id = dbData.list_id
		this.title = dbData.title
		this.documentUrl = utils.getDocumentUrl(dbData.doc_path)
	}
}

export default Document
