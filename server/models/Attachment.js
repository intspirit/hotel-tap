'use strict'

import mime from 'mime-types'
import path from 'path'
import utils from '../core/utils'

/**
 * Attachment model.
 */
class Attachment {

	/**
	 * Initializes a new instance of the Attachment class.
	 * @constructor
	 * @param {Object} dbData
	 * @param {int} dbData.attachment_id
	 * @param {string} dbData.file_path
	 * @param {int} dbData.post_id
	 **/
	constructor(dbData) {
		this.id = dbData.attachment_id
		this.type = mime.lookup(dbData.file_path || dbData.tmp_file_path)
		if (dbData.file_path) {
			this.url = utils.getAttachmentUrl(dbData.file_path)
		} else {
			this.url = utils.getTmpAttachmentUrl(dbData.tmp_file_path)
		}
		this.name = path.basename(utils.getAttachmentOriginalName(dbData.file_path || dbData.tmp_file_path))
		this.postId = dbData.post_id
	}
}

export default Attachment
