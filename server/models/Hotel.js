'use strict'

/**
 * Hotel model.
 * @param {Object} dbData
 */
class Hotel {

	// Not sure about archive status value
	static statuses = {
		INACTIVE: 0,
		ACTIVE: 1,
		ARCHIVE: 2,
		TRIAL: 3
	}

	static get activeStatuses() {
		return [
			this.statuses.ACTIVE,
			this.statuses.TRIAL
		]
	}

	constructor(dbData) {
		this.id = dbData.hotel_id
		this.adminId = dbData.hotel_admin
		this.name = dbData.name
		this.webAddress = dbData.webaddr
		this.roomsCount = dbData.num_rooms
		this.timeZone = dbData.timezone
		this.createdBy = dbData.created_by
		this.createdAt = dbData.created_date
		this.modifiedBy = dbData.modified_by
		this.modifiedAt = dbData.modified_date
		this.status = dbData.status
		this.statusModifiedAt = dbData.status_modified_date
		this.street = dbData.street_address
		this.city = dbData.city
		this.state = dbData.state
		this.zip = dbData.zipcode
		this.phone = dbData.phone
		this.notesArchiveDuration = dbData.notes_archive_duration
		this.notesArchiveDurationType = dbData.notes_archive_duration_type
		this.tasksArchiveDuration = dbData.tasks_archive_duration
		this.tasksArchiveDurationType = dbData.task_archive_duration_type
	}
}

export default Hotel
