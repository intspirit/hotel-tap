'use strict'

/**
 * Maintenance type model.
 *
 * @param {Object} dbData
 * @param {Number} dbData.maintenance_type_id
 * @param {String} dbData.name
 */
class MaintenanceType {
	constructor(dbData) {
		this.id = dbData.maintenance_type_id
		this.name = dbData.name
	}
}

export default MaintenanceType

