'use strict'

/**
 * Employee department model.
 * @param {Object} dbData
 * @param {int} dbData.emp_dept_id
 * @param {int} dbData.emp_id
 * @param {int} dbData.dept_id
 */
class EmployeeDepartment {
	constructor(dbData) {
		this.id = dbData.emp_dept_id
		this.userId = dbData.emp_id
		this.departmentId = dbData.dept_id
	}
}

export default EmployeeDepartment

