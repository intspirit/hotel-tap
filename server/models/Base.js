'use strict'

/**
 * Base model.
 *
 * @param {Object} dbData
 * @param {Date} dbData.created_date
 * @param {Number} dbData.created_by
 * @param {Date} dbData.modified_date
 * @param {Number} dbData.modified_by
 */
class Base {
	constructor(dbData) {

		// TODO: ak p3 consider Object.assign to use here
		// http://stackoverflow.com/questions/32925460/spread-operator-vs-object-assign
		// http://www.2ality.com/2014/01/object-assign.html

		this.createdDate = dbData.created_date
		this.createdBy = dbData.created_by || dbData.createdBy
		this.modifiedDate = dbData.modified_date
		this.modifiedBy = dbData.modified_by
	}
}

export default Base
