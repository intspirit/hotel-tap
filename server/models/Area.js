'use strict'

/**
 * Area model.
 *
 * @param {Object} dbData
 * @param {Number} dbData.area_id
 * @param {String} dbData.area_name
 * @param {String} dbData.name

 */
class Area {
	constructor(dbData) {
		this.id = dbData.area_id
		this.areaName = dbData.area_name
		this.name = dbData.name
	}
}

export default Area

