'use strict'

/**
 * Equipment group model.
 *
 * @param {Object} dbData
 * @param {Number} dbData.equipment_group_id
 * @param {String} dbData.name

 */
class EquipmentGroup {
	constructor(dbData) {
		this.id = dbData.equipment_group_id
		this.name = dbData.name
	}
}

export default EquipmentGroup

