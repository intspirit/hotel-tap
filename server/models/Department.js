'use strict'

import utils from '../core/utils'
import constants from '../../shared/constants'

/**
 * Department model.
 * @param {Object} dbData
 * @param {int} dbData.department_id
 * @param {int} dbData.hotel_id
 * @param {String} dbData.name
 * @param {String} dbData.dept_image
 * @param {String} dbData.special
 */
class Department {
	constructor(dbData) {
		this.id = dbData.department_id
		this.hotelId = dbData.hotel_id
		this.iconUrl = dbData.dept_image
		this.name = dbData.name
		this.special = dbData.special
	}

	get iconFullUrl() {
		return utils.getDepartmentImageUrl(this.iconUrl)
	}

	get isMaintenance() {
		return this.special === constants.specialLegacyTypes.MAINTENANCE
	}
}

export default Department

