'use strict'

import Area from './Area'
import Department from './Department'
import EquipmentGroup from './EquipmentGroup'
import Equipment from './Equipment'
import Hotel from './Hotel'
import MaintenanceType from './MaintenanceType'
import Post from './Post'
import Tag from './Tag'
import User from './User'
import EmployeeDepartment from './EmployeeDepartment'

export default {
	Area,
	Department,
	EquipmentGroup,
	Equipment,
	Hotel,
	MaintenanceType,
	Post,
	Tag,
	User,
	EmployeeDepartment
}
