'use strict'

import path from 'path'
import s3Router from 'react-s3-uploader/s3router'
import AWS from 'aws-sdk'
import logger from './core/logger'

import api from './api'
import dev from './dev'
import authController from './api/authController'
import config from './config/environment'
import errors from './components/errors'


/**
 * Defines routes.
 * @param {Object} app - express server.
 * @param {Object} auth - authentication processor.
 * @return {void}
 */
module.exports = (app, auth) => {
	const indexFileName = config.env === 'development' ? 'index-dev.html' : 'index.html'
	const indexFile = path.resolve(`${app.get('clientPath')}/${indexFileName}`)
	logger.log('info', `route|route|indexFile:${indexFile}`, {environment: config.env})

	app.use('/api/login', authController.login)
	app.use('/api/access/admin', auth.authenticate, authController.loginAdmin)
	app.use('/api', auth.authenticate, api)

	if (config.env !== 'production') {
		app.use('/dev', dev)
	}

	AWS.config.update({
		accessKeyId: config.s3.accessKey,
		secretAccessKey: config.s3.secretKey
	})
	app.use('/s3', s3Router({
		bucket: config.s3.tmpBucket,
		ACL: 'public-read',
		headers: {'Access-Control-Allow-Origin': '*'}
	}))

	/*app.get('/logout', (req, res) => {
		req.logout()
		res.redirect('/')
	})*/

	app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(errors[404])

	app.route('/*').get((req, res) => {
		res.sendFile(indexFile)
	})
}
