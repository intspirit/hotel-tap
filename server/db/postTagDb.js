'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Post Tags db.
 */
export default {

	/**
	 * Gets post tags for multiple posts.
	 * @param {int} hotelId
	 * @param {int[]} postIds
	 * @return {*}
	 */
	getByPostIds: async (hotelId, postIds) => {
		logger.log('info', 'postTagDb|getByPostIds', {hotelId, postIds})

		const inClause = utils.getSelectInClause(postIds)
		const sql =
			`SELECT
				post_id AS postId,
				tag_id AS tagId,
				child_id AS childId
			FROM post_tags
			WHERE hotel_id = ? AND post_id IN (${inClause})`

		return await dbBase.findAll(sql, [hotelId], [])
	},

	/**
	 * Inserts multiple tags for a post.
	 *
	 * @param {Array} postTags
	 * @returns {Promise}
	 */
	insertBulk: async (postTags) => {
		logger.log('info', 'postTagDb|insertBulk', {postTags})

		const sql =
			`INSERT INTO post_tags
					(hotel_id, post_id, tag_id, child_id)
				VALUES ?`

		return dbBase.insert(sql, [postTags])
	},

	/**
	 * Update OR Insert multiple tag rows for a post.
	 *
	 * @param {Array} postTags
	 * @returns {Promise}
	 */
	updateOrInsertBulk: async (postTags) => {
		logger.log('info', 'postTagDb|updateOrInsertBulk', {postTags})

		const sql =
			`INSERT INTO post_tags
				(hotel_id, post_id, tag_id, child_id)
			VALUES ?
			ON DUPLICATE KEY UPDATE
				tag_id = tag_id`

		return dbBase.insert(sql, [postTags])
	},

	/**
	 * Deletes post tags by post id
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @return {Promise}
	 */
	deleteByPostId: async (hotelId, postId) => {
		logger.log('info', 'postTagDb|deleteByPostId', {hotelId, postId})

		const sql = `DELETE FROM post_tags WHERE hotel_id = ? AND post_id = ?`
		const criteria = [hotelId, postId]

		return dbBase.delete(sql, criteria)
	}
}
