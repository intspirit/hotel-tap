'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import EquipmentGroup from '../models/EquipmentGroup'

/**
 * Equipment group db.
 */
export default {

	/**
	 * Gets equipment groups.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getEquipmentGroupsByHotelId: async (hotelId) => {
		logger.log('info', `equipmentGroupDb|getEquipmentGroupsByHotelId|hotelId:${hotelId}`)

		const sql = `
			SELECT
				equipment_group_id, name
			FROM equipment_groups
			WHERE hotel_id = ? AND status = 1`
		const where = [hotelId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new EquipmentGroup(x))
	}
}
