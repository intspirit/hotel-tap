'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Tag notification setting db.
 */
export default {

	/**
	 * Gets tag notification settings by tag ids.
	 *
	 * @param {int} hotelId
	 * @param {int[]} tagIds
	 * @return {Promise}
	 */
	getByTagIds: async (hotelId, tagIds) => {
		logger.log('info', `tagNotificationSettingDb|getByTags`, {hotelId, tagIds})

		const inClause = utils.getSelectInClause(tagIds)
		const sql =
			`SELECT
				user_id,
				email_notes,
				email_task
			FROM
				tag_notification_settings
			WHERE
				hotel_id = ? AND tag_id IN (${inClause})`
		const where = [hotelId]

		return await dbBase.findAll(sql, where)
	}
}


