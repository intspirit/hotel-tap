'use strict'

import dbBase from './dbBase'
import User from '../models/User'
import Hotel from '../models/Hotel'
import utils from '../core/utils'


/**
 * User db.
 */
export default {

	/**
	 * Gets user by id.
	 * @param {int} id
	 * @returns {null | User}
	 */
	getById: async id => {
		const sql = `
			SELECT
				user_id, hotel_id, first_name, last_name, username, email,
				password, old_password, role_id, status, profile_picture, default_lang,
				notification_status, company
			FROM users
			WHERE user_id = ?`
		const dbData = await dbBase.findOne(sql, [id], null)

		return dbData === null ? null : new User(dbData)
	},

	getEmployeeForLogin: async (webAddress, username) => {
		const activeStatuses = Hotel.activeStatuses
		const inClause = utils.getSelectInClause(activeStatuses)
		const sql = `
			SELECT
				users.*,
				hotels.name,
				hotels.hotel_id,
				hotels.timezone
			FROM
				users,
				hotels
			WHERE
				hotels.status IN (${inClause}) AND
				users.status = :status AND
				users.hotel_id = hotels.hotel_id AND
				users.username = :username AND
				hotels.webaddr = :webAddress AND
				users.role_id <> :roleId`

		const where = {
			webAddress: webAddress,
			username: username,
			status: 1,
			roleId: 2
		}

		const dbData = await dbBase.findOne(sql, where, null)

		return dbData === null ? null : new User(dbData)
	},

	getAdminForLogin: async (email) => {
		const activeStatuses = Hotel.activeStatuses
		const inClause = utils.getSelectInClause(activeStatuses)
		const sql = `
			SELECT
				users.*,
				IF(users.hotel_id <> 0, users.hotel_id, hotels.hotel_id) AS hotel_id
			FROM
				users
			JOIN hotels ON (
				users.hotel_id = hotels.hotel_id OR
				users.user_id = hotels.hotel_admin
			)
			WHERE
				hotels.status IN (${inClause}) AND
				users.email = :email AND
				users.role_id = :roleId AND
				users.status = :status
			ORDER BY
				users.user_id desc`

		const where = {
			email: email,
			roleId: 2,
			status: 1
		}

		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new User(x))
	},

	/**
	 * Gets users by a list of ids.
	 * @param {Array} ids
	 * @returns {Array}
	 */
	getByIds: async ids => {
		const inClause = utils.getSelectInClause(ids)
		//TODO: vvs p2 hotelId ???
		const sql = `
			SELECT
				user_id, first_name, last_name, profile_picture, notification_email, notification_status
			FROM users
			WHERE user_id IN (${inClause})`
		const where = []
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new User(x))
	},

	/**
	 * Gets hotel users.
	 * @param {int} hotelId
	 * @returns {Array}
	 */
	getByHotelId: async hotelId => {
		const sql = `
			SELECT DISTINCT *
			FROM (
					(SELECT
						user_id,
						first_name,
						last_name,
						role_id,
						profile_picture
					FROM users
					WHERE hotel_id = :hotelId AND status != 2
					)
					UNION
					(SELECT
						user_id,
						first_name,
						last_name,
						2 AS role_id,
						profile_picture
					FROM users
						INNER JOIN hotels ON users.user_id = hotels.hotel_admin
					WHERE hotels.hotel_id = :hotelId AND users.status != 2
					)
				) all_hotel_users
			ORDER BY first_name, last_name;`
		const where = {hotelId: hotelId}
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new User(x))
	},

	/**
	 * Updates user profile info.
	 * @param {int} userId
	 * @param {object} profileInfo
	 * @param {string} profileInfo.firstName
	 * @param {string|null} profileInfo.lastName
	 * @param {string|null} profileInfo.imageFileName
	 * @returns {Promise}
	 */
	updateProfile: async (userId, profileInfo) => {
		const profileImageIsSame = 1

		const sql =
			`UPDATE
				users
			SET
				first_name = :firstName,
				last_name = :lastName,
				profile_picture = IF(:imageFileName = ${profileImageIsSame}, profile_picture, :imageFileName)
			WHERE
				user_id = :userId;
			`

		const criteria = {
			firstName: profileInfo.firstName,
			lastName: profileInfo.lastName,
			imageFileName: profileInfo.hasOwnProperty('imageFileName') ?
				profileInfo.imageFileName : profileImageIsSame,
			userId
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates user's password.
	 *
	 * @param {int} userId
	 * @param {string} password
	 * @returns {Promise}
	 */
	updatePassword: async (userId, password) => {
		const sql =
			`UPDATE
				users
			SET
				old_password = password,
				password = :password
			WHERE
				user_id = :userId;
			`

		const criteria = {
			password,
			userId
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates user's notification status
	 *
	 * @param {int} userId
	 * @param {int} notificationStatus
	 * @returns {Promise}
	 */
	updateNotification: async (userId, notificationStatus) => {
		const sql =
			`UPDATE
				users
			SET
				notification_status = :notificationStatus
			WHERE
				user_id = :userId;
			`

		const criteria = {
			userId,
			notificationStatus
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Sets user's access token.
	 * @param {int} userId
	 * @param {string} accessToken
	 * @returns {Promise}
	 */
	setAccessToken: async (userId, accessToken) => {
		const sql =
			`UPDATE
				users
			SET
				access_token = :accessToken
			WHERE
				user_id = :userId;
			`

		const criteria = {
			accessToken,
			userId
		}

		return dbBase.update(sql, criteria)
	}
}
