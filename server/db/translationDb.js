'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Translation db
 */
export default {

	/**
	 * Inserts translations of subject and description of a given post in a given language
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {string} language
	 * @param {string} subject
	 * @param {string} description
	 * @returns {Promise}
	 */
	insert: async (hotelId, postId, language, subject, description) => {
		logger.log('info', 'translationDb|insert', {hotelId: hotelId, postId: postId, language: language})

		const sql =
			`INSERT INTO
				translations(hotel_id, post_id, language, subject, description)
			VALUES
				(:hotelId, :postId, :language, :subject, :description)`

		const criteria = {
			hotelId: hotelId,
			postId: postId,
			language: language,
			subject: subject,
			description: description
		}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Inserts multiple translations at once
	 * @param {Array} translations
	 * @returns {Promise}
	 */
	insertBulk: async (translations) => {
		logger.log('info', 'translationDb|insertBulk', {translations: translations})

		const sql =
			`INSERT INTO 
				translations
				(hotel_id, post_id, language, subject, description)
			VALUES ?`

		return dbBase.insert(sql, [translations])
	},

	getByPostIds: async (hotelId, postIds, language) => {
		logger.log('info', 'translationDb|getByPostIds', {hotelId: hotelId, postIds: postIds, language: language})

		const inClause = utils.getSelectInClause(postIds)

		const sql =
			`SELECT
				translations.hotel_id,
				translations.post_id,
				translations.language,
				translations.subject,
				translations.description
			FROM
				translations
			WHERE
					translations.post_id IN (${inClause})
				AND
					translations.hotel_id = ?
				AND
					translations.language = ?`

		const where = [hotelId, language]

		return dbBase.findAll(sql, where, [])
	}
}
