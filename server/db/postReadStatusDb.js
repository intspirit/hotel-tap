'use strict'

import dbBase from './dbBase'
import PostReadStatus from '../models/PostReadStatus'
import utils from '../core/utils'
import logger from '../core/logger'

/**
 * Unread post status db.
 */
export default {

	/**
	 * Gets counts of read or unread posts for a user by departments.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {string} status
	 * @return {Promise}
	 **/
	getDepartmentCountsByUserId: async (hotelId, userId, status) => {
		const sql =
			`SELECT
			'department' AS type,
				post_departments.department_id AS id,
				COUNT(post_read_statuses.post_id) AS count
			FROM post_read_statuses
			INNER JOIN post_departments ON post_read_statuses.post_id = post_departments.post_id
			INNER JOIN departments ON post_departments.department_id = departments.department_id
			WHERE
				post_read_statuses.hotel_id = ? AND
				user_id = ? AND post_read_statuses.status = ? AND
				departments.status = 1
			GROUP BY type, post_departments.department_id
			HAVING count > 0`
		const where = [hotelId, userId, status]

		return await dbBase.findAll(sql, where, null)
	},

	/**
	 * Gets counts of read or unread posts for a user.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {string} status
	 * @return {Promise}
	 **/
	getUserCountsByUserId: async (hotelId, userId, status) => {
		const sql =
			`SELECT 'employee' AS type, user_id AS id, count(*) AS count
			FROM post_read_statuses
			WHERE hotel_id = ? AND user_id = ? AND status = ?;`
		const where = [hotelId, userId, status]

		return await dbBase.findAll(sql, where, null)
	},

	/**
	 * Gets read statuses for a list of posts and status.
	 * @param {int} hotelId
	 * @param {int[]} postIds
	 * @param {string} status
	 * @returns {PostReadStatus[]|[]}
	 */
	getByPostIds: async (hotelId, postIds, status) => {
		const inClause = utils.getSelectInClause(postIds)
		const sql =
			`SELECT post_id, user_id FROM post_read_statuses WHERE hotel_id = ? AND post_id IN (${inClause}) AND status = ?`

		const where = [hotelId, status]
		const dbData = await dbBase.findAll(sql, where, [])

		return dbData.map(x => new PostReadStatus(x))
	},

	/**
	 * Updates a post.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {string} status
	 * @return {Promise}
	 **/
	update: async (hotelId, userId, postId, status) => {
		logger.log('info', 'postStatusReadDb|update', {hotelId, userId, postId, status})

		const sql =
			`UPDATE
				post_read_statuses
			SET
				status = :status
			WHERE
				hotel_id = :hotelId AND user_id = :userId AND post_id = :postId
			`
		const criteria = {hotelId, userId, postId, status}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Update OR Insert user's read status.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {string} status
	 * @return {Promise}
	**/
	updateOrInsert: async (hotelId, userId, postId, status) => {
		logger.log('info', 'postReadStatusDb|updateOrInsert', {hotelId, userId, postId, status})

		const sql = `
			INSERT INTO post_read_statuses
				(hotel_id, user_id, post_id, status)
			VALUES (:hotelId, :userId, :postId, :status)
			ON DUPLICATE KEY UPDATE
				status = :status`

		const criteria = {hotelId, postId, userId, status}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Set post as unread for all users except user with id = userId.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @return {Promise}
	 **/
	setStatusUnread: async (hotelId, userId, postId) => {
		logger.log('info', 'postReadStatusDb|setStatusUnread', {hotelId, userId, postId})

		const sql =
			`UPDATE
				post_read_statuses
			SET
				status = :status
			WHERE
				hotel_id = :hotelId AND user_id != :userId AND post_id = :postId
			`
		const criteria = {hotelId: hotelId, userId: userId, postId: postId, status: 'u'}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Inserts multiple read statuses for a post.
	 * @param {Array} postReadStatuses
	 * @returns {Promise}
	 */
	insertBulk: async (postReadStatuses) => {
		logger.log('info', 'postReadStatusDb|insertBulk', {postReadStatuses: postReadStatuses})

		const sql =
			`INSERT INTO post_read_statuses
					(hotel_id, user_id, post_id, status)
				VALUES ?`

		return dbBase.insert(sql, [postReadStatuses])
	},

	/**
	 * Update OR Insert user's read statuses
	 *
	 * @param {Array} postReadStatuses
	 * @return {Promise}
	**/
	updateOrInsertBulk: async (postReadStatuses) => {
		logger.log('info', 'postReadStatusDb|updateOrInsertBulk', {postReadStatuses})

		const sql = `
			INSERT INTO post_read_statuses
				(hotel_id, user_id, post_id, status)
			VALUES ?
			ON DUPLICATE KEY UPDATE
				status = VALUES(status)`

		return dbBase.insert(sql, [postReadStatuses])
	},

	/**
	 * Deletes post read statuses by post id
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @return {Promise}
	 */
	deleteByPostId: async (hotelId, postId) => {
		logger.log('info', 'postReadStatusDb|deleteByPostId', {hotelId, postId})

		const sql = `DELETE FROM post_read_statuses WHERE hotel_id = ? AND post_id = ?`
		const criteria = [hotelId, postId]

		return dbBase.delete(sql, criteria)
	},

	/**
	 * Updates read status for all but users from array "to skip"
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {string} status
	 * @param {int[]} userIdsToSkip
	 * @returns {Promise}
	 */
	updateStatusForAllButUsers: async (hotelId, postId, status, userIdsToSkip) => {
		logger.log('info', `postStatusReadDb|updateForAllButUsers|hotelId:${hotelId},postId:${postId},status:${status},
																	userIdsToSkip:${userIdsToSkip}`)

		const inClause = utils.getSelectInClause(userIdsToSkip)

		const sql =
			`UPDATE
				post_read_statuses
			SET
				status = :status
			WHERE
					hotel_id = :hotelId
				AND
					post_id = :postId
				AND user_id NOT IN (${inClause})
			`
		const criteria = {hotelId: hotelId, postId: postId, status: status}

		return dbBase.update(sql, criteria)
	}
}
