'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import Document from '../models/Document'

/**
 * Document db.
 */
export default {

	/**
	 * Gets documents by hotel ID.
	 * @param {number} hotelId
	 * @returns {Array}
	 */
	getByHotelId: async (hotelId) => {
		logger.log('info', 'documentDb|getByHotelId', {hotelId})

		const sql = `
			SELECT
				list_id, title, doc_path
			FROM
			  lists
			WHERE hotel_id = ?`

		const dbData = await dbBase.findAll(sql, [hotelId], [])

		return dbData.map(x => new Document(x))
	}
}
