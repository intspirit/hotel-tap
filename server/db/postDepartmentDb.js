'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'

/**
 * Post Department db.
 */
export default {

	/**
	 * Updates a post.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {string} orderDateTime
	 * @return {Promise}
	**/
	update: async (hotelId, postId, orderDateTime) => {
		logger.log('info', `postDepartmentDb|update|hotelId:${hotelId}, postId:${postId}, orderDateTime:${orderDateTime}`)

		const sql =
			`UPDATE
				post_departments
			SET
				order_dtm_utc = :orderDateTime
			WHERE
				hotel_id = :hotelId AND post_id = :postId`

		const criteria = {hotelId, postId, orderDateTime}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Get all post departments.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @return {Promise}
	 */
	getByPostId: async (hotelId, postId) => {
		logger.log('info', 'postDepartmentDb|getAll|', {hotelId: hotelId, postId: postId})

		const sql = 'SELECT department_id, order_dtm_utc FROM post_departments WHERE hotel_id = ? AND post_id = ?'

		const where = [hotelId, postId]

		return dbBase.findAll(sql, where, [])

	},

	/**
	 * Inserts multiple departments for a post.
	 *
	 * @param {Array} postDepartments
	 * @returns {Promise}
	 */
	insertBulk: async (postDepartments) => {
		logger.log('info', 'postDepartmentDb|insertBulk', {postDepartments})

		const sql =
			`INSERT INTO post_departments
					(hotel_id, post_id, department_id, child_id, order_dtm_utc)
				VALUES ?`

		return dbBase.insert(sql, [postDepartments])
	},

	/**
	 * Update OR Insert multiple department rows for a post.
	 *
	 * @param {Array} postDepartments
	 * @return {Promise}
	**/
	updateOrInsertBulk: async (postDepartments) => {
		logger.log('info', 'postDepartmentDb|updateOrInsertBulk', {postDepartments})

		const sql = `
			INSERT INTO post_departments
				(hotel_id, post_id, department_id, child_id, order_dtm_utc)
			VALUES ?
			ON DUPLICATE KEY UPDATE
				order_dtm_utc = VALUES(order_dtm_utc)`

		return dbBase.insert(sql, [postDepartments])
	},

	/**
	 * Deletes post departments by post id
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @return {Promise}
	 */
	deleteByPostId: async (hotelId, postId) => {
		logger.log('info', 'postDepartmentDb|deleteByPostId', {hotelId, postId})

		const sql = `DELETE FROM post_departments WHERE hotel_id = ? AND post_id = ?`
		const criteria = [hotelId, postId]

		return dbBase.delete(sql, criteria)
	}
}
