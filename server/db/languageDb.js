'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'

/**
 * Language db.
 */
export default {

	/**
	 * Gets a translating phrase by language code.
	 * @param {string} languageCode
	 * @returns {Promise}
	 */
	getTranslatingPhraseByLanguageCode: async (languageCode) => {
		logger.log('info', 'languageDb|getTranslatingPhraseByLanguageCode', {languageCode: languageCode})

		const sql =
			`SELECT 
				languages.translating_phrase
			FROM
				languages
			WHERE
				languages.code = ?`

		const where = [languageCode]

		return dbBase.findOne(sql, where, null)
	}
}
