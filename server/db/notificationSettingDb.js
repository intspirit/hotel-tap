'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Notification setting db.
 */
export default {

	/**
	 * Gets notification settings by departments ids.
	 *
	 * @param {int} hotelId
	 * @param {int[]} departmentIds
	 * @return {Promise}
	 */
	getByDepartmentIds: async (hotelId, departmentIds) => {
		logger.log('info', `notificationSettingDb|getByDepartmentIds`, {hotelId, departmentIds})

		const inClause = utils.getSelectInClause(departmentIds)
		const sql =
			`SELECT
				user_id,
				email_notes,
				email_task
			FROM
				notification_settings
			WHERE
				hotel_id = ? AND dept_id IN (${inClause})`
		const where = [hotelId]

		return await dbBase.findAll(sql, where)
	}
}


