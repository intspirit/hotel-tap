'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import MaintenanceType from '../models/MaintenanceType'

/**
 * Maintenance type db.
 */
export default {

	/**
	 * Gets maintenance types.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getMaintenanceTypesByHotelId: async (hotelId) => {
		logger.log('info', `maintenanceTypeDb|getMaintenanceTypesByHotelId|hotelId:${hotelId}`)

		const sql = `
			SELECT
				maintenance_type_id, name
			FROM maintenance_types
			WHERE hotel_id = ? AND status = 1`
		const where = [hotelId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new MaintenanceType(x))
	}
}
