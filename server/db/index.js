'use strict'

import attachmentDb from './attachmentDb'
import areaDb from './areaDb'
import departmentDb from './departmentDb'
import documentsDb from './documentsDb'
import equipmentDb from './equipmentDb'
import equipmentGroupDb from './equipmentGroupDb'
import employeeDepartmentDb from './employeeDepartmentDb'
import hotelDb from './hotelDb'
import hotelLanguageDb from './hotelLanguageDb'
import inspectionDb from './inspectionDb'
import jobDb from './jobDb'
import jobStatusDb from './jobStatusDb'
import languageDb from './languageDb'
import maintenanceTypeDb from './maintenanceTypeDb'
import notificationSettingDb from './notificationSettingDb'
import postDb from './postDb'
import postDepartmentDb from './postDepartmentDb'
import postReadStatusDb from './postReadStatusDb'
import postUserDb from './postUserDb'
import postTagDb from './postTagDb'
import tagDb from './tagDb'
import tagNotificationSettingDb from './tagNotificationSettingDb'
import translationDb from './translationDb'
import userDb from './userDb'
import postPMDb from './postPMDb'


module.exports = {
	attachmentDb,
	areaDb,
	departmentDb,
	documentsDb,
	equipmentDb,
	employeeDepartmentDb,
	equipmentGroupDb,
	hotelDb,
	hotelLanguageDb,
	inspectionDb,
	jobDb,
	jobStatusDb,
	languageDb,
	maintenanceTypeDb,
	notificationSettingDb,
	postDb,
	postDepartmentDb,
	postReadStatusDb,
	postUserDb,
	postTagDb,
	tagDb,
	tagNotificationSettingDb,
	translationDb,
	userDb,
	postPMDb
}
