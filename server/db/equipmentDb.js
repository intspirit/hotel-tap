'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import Equipment from '../models/Equipment'

/**
 * Equipment db.
 */
export default {

	/**
	 * Gets equipments.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getEquipmentsByHotelId: async (hotelId) => {
		logger.log('info', `equipmentDb|getEquipmentsByHotelId|hotelId:${hotelId}`)

		const sql = `
			SELECT
				equipment_id, equipment_group_id, name
			FROM equipment
			WHERE hotel_id = ? AND status = 1`
		const where = [hotelId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Equipment(x))
	}
}
