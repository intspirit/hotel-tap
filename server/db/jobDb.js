'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'

/**
 * Jobs db
 */
export default {

	/**
	 * Inserts a job
	 *
	 * @param {Object} job
	 * @param {int} job.postId
	 * @param {string|null} job.groupUuid
	 * @param {string} job.name
	 * @param {string|null} job.meta
	 * @param {Date|string} job.createdDateTimeUtc
	 * @returns {Promise}
	 */
	insert: async (job) => {
		logger.log('info', 'jobDb|insert', {job})

		const sql =
			`INSERT INTO
				jobs (post_id, group_uuid, name, meta, created_dtm_utc)
			VALUES
				(:postId, :groupUuid, :name, :meta, :createdDateTimeUtc)`
		const criteria = {
			postId: job.postId,
			groupUuid: job.groupUuid,
			name: job.name,
			meta: job.meta,
			createdDateTimeUtc: job.createdDateTimeUtc
		}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Inserts multiple jobs
	 *
	 * @param {Array} jobs
	 * @returns {Promise}
	 */
	insertBulk: async (jobs) => {
		logger.log('info', 'jobDb|insertBulk', {jobs})

		const sql =
			`INSERT INTO jobs
					(post_id, group_uuid, name, meta, created_dtm_utc)
				VALUES ?`

		return dbBase.insert(sql, [jobs])
	},

	/**
	 * Gets jobs by group uuid
	 *
	 * @param {string} groupUuid
	 * @returns {Array}
	 */
	getByGroupUuid: async (groupUuid) => {
		logger.log('info', 'jobDb|getByGroupUuid', {groupUuid})

		const sql =
			`SELECT id, name, post_id, group_uuid, meta, created_dtm_utc
				FROM jobs
			WHERE group_uuid = ?`
		const where = [groupUuid]

		return await dbBase.findAll(sql, where, [])
	}

}
