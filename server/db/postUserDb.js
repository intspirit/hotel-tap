'use strict'

import dbBase from './dbBase'
import utils from '../core/utils'
import logger from '../core/logger'

/**
 * Post User db.
 */
export default {

	/**
	 * Gets post users (cc) for multiple posts.
	 * @param {int} hotelId
	 * @param {int[]} postIds
	 * @return {*}
	 */
	getByPostIds: async (hotelId, postIds) => {
		logger.log('info', 'postUserDb|getByPostIds', {hotelId: hotelId, postIds: postIds})

		const inClause = utils.getSelectInClause(postIds)

		const sql = `
			SELECT
				post_users.post_id AS postId,
				post_users.user_id AS userId,
				post_users.child_id AS childId,
				post_users.category,
				users.first_name AS firstName,
				users.last_name AS lastName
			FROM post_users
				JOIN users ON post_users.user_id = users.user_id
			WHERE post_users.hotel_id = ? AND post_users.post_id IN (${inClause});`

		return await dbBase.findAll(sql, [hotelId], [])
	},

	/**
	 * Update OR Insert user row for post.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {int} userId
	 * @param {int} childId
	 * @param {string} category
	 * @param {Date} nowUtc
	 * @return {Promise}
	**/
	updateOrInsert: async (hotelId, postId, userId, childId, category, nowUtc) => {
		const sql = `
			INSERT INTO post_users
				(hotel_id, post_id, user_id, child_id, category, order_dtm_utc)
			VALUES (:hotelId, :postId, :userId, :childId, :category, :nowUtc)
			ON DUPLICATE KEY UPDATE
				category = :category`

		const criteria = {hotelId, postId, userId, childId, category, nowUtc}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Updates a post.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {string} orderDateTime
	 * @return {Promise}
	**/
	update: async (hotelId, postId, orderDateTime) => {
		logger.log('info', 'postUserDb|update', {hotelId: hotelId, postId: postId, orderDateTime: orderDateTime})

		const sql =
			`UPDATE
				post_users
			SET
				order_dtm_utc = :orderDateTime
			WHERE
				hotel_id = :hotelId AND post_id = :postId`

		const criteria = {hotelId, postId, orderDateTime}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Inserts multiple users for a post.
	 *
	 * @param {Array} postUsers
	 * @returns {Promise}
	 */
	insertBulk: async (postUsers) => {
		logger.log('info', 'postUserDb|insertBulk', {postUsers: postUsers})

		const sql =
			`INSERT INTO post_users
					(hotel_id, post_id, user_id, child_id, category, order_dtm_utc)
				VALUES ?`

		return dbBase.insert(sql, [postUsers])
	},

	/**
	 * Update OR Insert multiple user rows for a post.
	 *
	 * @param {Array} postUsers
	 * @returns {Promise}
	 */
	updateOrInsertBulk: async (postUsers) => {
		logger.log('info', 'postUserDb|updateOrInsertBulk', {postUsers})

		const sql =
			`INSERT INTO post_users
				(hotel_id, post_id, user_id, child_id, category, order_dtm_utc)
			VALUES ?
			ON DUPLICATE KEY UPDATE
				order_dtm_utc = VALUES(order_dtm_utc)`

		return dbBase.insert(sql, [postUsers])
	},

	/**
	 * Deletes post users by post id
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @return {Promise}
	 */
	deleteByPostId: async (hotelId, postId) => {
		logger.log('info', 'postUserDb|deleteByPostId', {hotelId, postId})

		const sql = `DELETE FROM post_users WHERE hotel_id = ? AND post_id = ?`
		const criteria = [hotelId, postId]

		return dbBase.delete(sql, criteria)
	},

	/**
	 * Deletes post user
	 *
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {int} userId
	 * @param {string} category
	 * @param {int|null} childId
	 * @return {Promise}
	 */
	delete: async (hotelId, postId, userId, category, childId = 0) => {
		logger.log('info', 'postUserDb|deleteByPostId', {hotelId, postId, userId, category, childId})

		const sql = `DELETE FROM post_users
			WHERE hotel_id = ? AND post_id = ? AND user_id = ? AND category = ? AND child_id = ?`
		const criteria = [hotelId, postId, userId, childId, category]

		return dbBase.delete(sql, criteria)
	}

}
