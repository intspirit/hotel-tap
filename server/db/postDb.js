'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'
import Post from '../models/Post'

/**
 * Post db.
 */
export default {

	/**
	 * Gets posts by type id.
	 * @param {int} hotelId
	 * @param {string} typeId
	 * @return {Promise}
	 * @description Mostly used to get RL, CL and inspection list.
	 */
	getByType: async (hotelId, typeId) => {
		logger.log('info', 'postDb|getByType', {hotelId, typeId})
		const sql = `
			SELECT *
			FROM
			  posts
			WHERE hotel_id = ? AND type_id = ?`

		return await dbBase.findAll(sql, [hotelId, typeId], [])

	},

	// TODO: alytyuk p2 this method seems to be replaced with "getByTypeAndParentAndDateRange" after changing it's sql query
	getByParentIdsAndPostTypes: async (hotelId, parentIds, postTypes) => {
		logger.log('info', 'postDb|getByParentIdsAndPostTypes', {hotelId, parentIds, postTypes})

		const parentIdsClause = utils.getSelectInClause(parentIds)
		const postTypesClause = utils.getSelectInClause(postTypes)
		const sql =
			`SELECT
				posts.*
			FROM
				posts
			WHERE
					posts.hotel_id = ?
				AND
					posts.type_id IN (${postTypesClause})
				AND
					posts.parent_id IN (${parentIdsClause})
				AND
					posts.deleted = 0;`

		return dbBase.findAll(sql, [hotelId], [])
	},

	/**
	 * Gets posts by type id and date range.
	 * @param {int} hotelId
	 * @param {string} typeId
	 * @param {int} parentId
	 * @param {string} dateFrom
	 * @param {string} dateTo
	 * @return {Promise}
	 * @description Mostly used to get RL, CL and inspection list.
	 */
	getByTypeAndParentAndDateRange: async (hotelId, typeId, parentId, dateFrom, dateTo) => {
		logger.log('info', 'postDb|getByTypeAndDateRange', {hotelId, typeId, dateFrom, dateTo})

		const sql = `
			SELECT *
			FROM posts
			WHERE 
				hotel_id = ? AND 
				type_id = ? AND
				parent_id = ? AND 
				(due_dtm BETWEEN ? AND ? OR completion_dtm BETWEEN ? AND ?);`
		const result = await dbBase.findAll(
			sql, [hotelId, typeId, parentId, dateFrom, dateTo, dateFrom, dateTo], [])

		return result.map(x => new Post(x))
	},

	/**
	 * Gets posts for department.
	 * @param {int} hotelId
	 * @param {int} departmentId
	 * @param {int} userId
	 * @param {int} skip
	 * @param {int} take
	 * @param {Array}postTypes
	 * @returns {Array|Object|*|{}}
	 */
	getByDepartmentId: async(hotelId, departmentId, userId, skip, take, postTypes) => {
		logger.log('info', 'postDb|getByDepartmentId',
			{hotelId: hotelId, departmentId: departmentId, skip: skip, take: take, postTypes: postTypes})
		const inClause = utils.getSelectInClause(postTypes)
		const sql = `
			SELECT
			  posts.*,
			  post_read_statuses.status AS read_status,
			  users.first_name,
			  users.last_name,
			  users.profile_picture
			FROM
			  posts
			  INNER JOIN users ON posts.created_by = users.user_id
			  INNER JOIN post_read_statuses
			    ON posts.hotel_id = post_read_statuses.hotel_id AND
				  posts.id = post_read_statuses.post_id AND
				  post_read_statuses.user_id = :userId
			  LEFT OUTER JOIN (
				  SELECT DISTINCT hotel_id, post_id, user_id
				  FROM post_users
				  WHERE
					hotel_id = :hotelId AND
					user_id = :userId
				) AS post_users_dist
				ON
				  posts.hotel_id = post_users_dist.hotel_id AND
				  posts.id = post_users_dist.post_id AND
				  post_users_dist.user_id = :userId
			WHERE posts.hotel_id = :hotelId AND
				(! posts.private OR (posts.private AND post_users_dist.user_id IS NOT NULL)) AND
				type_id IN (${inClause}) AND
				posts.id IN (
				  SELECT DISTINCT post_id
				  FROM post_departments
				  WHERE hotel_id = :hotelId AND department_id = :departmentId
				)
			ORDER BY posts.order_dtm_utc DESC
			LIMIT :skip, :take;`

		const criteria = {userId, hotelId, departmentId, skip, take}

		return await dbBase.findAll(sql, criteria, [])
	},

	/**
	 * Gets posts for department.
	 * @param {int} hotelId
	 * @param {int[]} parentIds
	 * @param {string} postType
	 * @param {string[]} fields
	 * @returns {Array|Object|*|{}}
	 */
	getByParentIds: async(hotelId, parentIds, postType, fields = '*') => {
		logger.log('info', 'postDb|getByParentIds', {hotelId, parentIds, postType, fields})
		const inClause = utils.getSelectInClause(parentIds)
		const sql = `
			SELECT ${fields} 
			FROM posts
			WHERE hotel_id = ? AND type_id = ? AND parent_id IN (${inClause}) AND deleted = 0;`
		const where = [hotelId, postType]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets posts for employee.
	 * @param {int} hotelId
	 * @param {int} employeeId
	 * @param {int} userId
	 * @param {Array} limitOptions
	 * @param {Array}postTypes
	 * @returns {Array|Object|*|{}}
	 */
	getByEmployeeId: async(hotelId, employeeId, userId, limitOptions, postTypes) => {
		const postTypesInClause = utils.getSelectInClause(postTypes)
		const sql = `SELECT
			  posts.*,
			  post_read_statuses.status AS read_status,
			  users.first_name,
			  users.last_name,
			  users.profile_picture
			FROM
			  posts
			  INNER JOIN users ON posts.created_by = users.user_id
			  INNER JOIN post_read_statuses
				ON posts.hotel_id = post_read_statuses.hotel_id AND
				  posts.id = post_read_statuses.post_id AND
				  post_read_statuses.user_id = :userId
			  LEFT OUTER JOIN (
				  SELECT DISTINCT hotel_id, post_id, user_id
				  FROM post_users
				  WHERE
					hotel_id = :hotelId AND
					user_id = :userId
				) AS post_users_dist
				ON
				  posts.hotel_id = post_users_dist.hotel_id AND
				  posts.id = post_users_dist.post_id AND
				  post_users_dist.user_id = :userId
			WHERE
			  posts.hotel_id = :hotelId AND
			  (! posts.private OR (posts.private AND post_users_dist.user_id IS NOT NULL)) AND
			  posts.type_id IN (${postTypesInClause}) AND
			  posts.id IN (
			    SELECT DISTINCT post_id
			    FROM post_users
			    WHERE hotel_id = :hotelId AND user_id = :employeeId
			  )
			ORDER BY posts.order_dtm_utc DESC
			LIMIT :skip, :take;`

		const skip = limitOptions[0]
		const take = limitOptions[1]
		const criteria = {hotelId, employeeId, userId, skip, take}

		return await dbBase.findAll(sql, criteria, [])
	},

	/**
	 * Gets posts for tag.
	 *
	 * @param {int} hotelId
	 * @param {int} tagId
	 * @param {int} userId
	 * @param {int} skip
	 * @param {int} take
	 * @param {Array} postTypes
	 * @returns {Array|Object|*|{}}
	 */
	getByTagId: async(hotelId, tagId, userId, skip, take, postTypes) => {
		logger.log('info', 'postDb|getByTagId',
			{hotelId: hotelId, tagId: tagId, userId: userId, skip: skip, take: take, postTypes: postTypes})
		const inClause = utils.getSelectInClause(postTypes)
		const sql = `
			SELECT
				posts.*,
				post_read_statuses.status AS read_status,
				users.first_name,
				users.last_name,
				users.profile_picture
			FROM
				posts
			INNER JOIN users ON posts.created_by = users.user_id
			INNER JOIN post_read_statuses ON posts.hotel_id = post_read_statuses.hotel_id AND
				posts.id = post_read_statuses.post_id AND
				post_read_statuses.user_id = ?
			LEFT OUTER JOIN (
				SELECT DISTINCT
					hotel_id,
					post_id,
					user_id
				FROM post_users
				WHERE
					hotel_id = ? AND
					user_id = ?
			) AS post_users_dist ON
				posts.hotel_id = post_users_dist.hotel_id AND
				posts.id = post_users_dist.post_id AND
				post_users_dist.user_id = ?
			WHERE posts.hotel_id = ? AND
				type_id IN (${inClause}) AND
				(
					! posts.private
					OR
					(
						posts.private AND post_users_dist.user_id IS NOT NULL
					)
				) AND
				posts.id IN (
					SELECT DISTINCT post_id
					FROM post_tags
					WHERE hotel_id = ? AND tag_id = ?
				)
			ORDER BY posts.order_dtm_utc DESC
			LIMIT ?, ?;`
		const where = [userId, hotelId, tagId, userId, hotelId, hotelId, tagId, skip, take]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 *
	 * @param {int} hotelId
	 * @param {int} skip
	 * @param {int} take
	 * @param {Array} postTypes
	 * @returns {Array|Object|*|{}}
	 */
	getComplaintsByHotelId: async(hotelId, skip, take, postTypes) => {
		const postTypesInClause = utils.getSelectInClause(postTypes)

		const sql =
			`SELECT
			  posts.*,
			  users.first_name,
			  users.last_name,
			  users.profile_picture
			FROM
			  posts
			INNER JOIN users ON posts.created_by = users.user_id
			WHERE
			  posts.hotel_id = :hotelId AND
			  posts.type_id IN (${postTypesInClause}) AND
			  posts.guest_complaint = 1
			ORDER BY posts.order_dtm_utc DESC
			LIMIT :skip, :take;`

		const criteria = {hotelId, skip, take}

		return await dbBase.findAll(sql, criteria, [])
	},

	/**
	 * Updates a post.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} postId
	 * @param {Object} flagProperties
	 * @return {Promise}
	 **/
	updateFlagStatus: async(hotelId, userId, postId, flagProperties) => {
		logger.log('info',
			`postDb|updateFlagStatus|hotelId:${hotelId},userId:${userId},postId:${postId}, flagProperties:${flagProperties}`)

		const sql =
			`UPDATE
				posts
			SET
				flagged = :flag,
				flagged_by = :userId,
				flagged_dtm = :flaggedDateTime,
				flagged_dtm_utc = :flaggedDateTimeUtc,
				flag_due_dtm_utc = :flagDueDateTimeUtc,
				flag_due_dtm = :flagDueDateTime
			WHERE
				hotel_id = :hotelId AND id = :postId
			`

		const criteria = {
			hotelId: hotelId,
			userId: flagProperties.flag ? userId : null,
			postId: postId,
			flag: flagProperties.flag,
			flaggedDateTime: flagProperties.flaggedDateTime,
			flaggedDateTimeUtc: flagProperties.flaggedDateTimeUtc,
			flagDueDateTime: flagProperties.flagDueDateTime,
			flagDueDateTimeUtc: flagProperties.flagDueDateTimeUtc
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates task completion status.
	 * @param {int} hotelId
	 * @param {int|null} userId
	 * @param {int} postId
	 * @param {Object} statusUpdate
	 * @return {Promise}
	 **/
	updateTaskCompletionStatus: async(hotelId, userId, postId, statusUpdate) => {
		logger.log('info', 'postDb|updateTaskCompletionStatus',
			{hotelId: hotelId, userId: userId, postId: postId, status: statusUpdate})

		const sql =
			`UPDATE
				posts
			SET
				completion_status = :status,
				completion_dtm = :completionDateTime,
				completion_dtm_utc = :completionDateTimeUtc,
				completed_by = :userId
			WHERE
				hotel_id = :hotelId AND id = :postId
			`

		const criteria = {
			hotelId: hotelId,
			userId: userId,
			postId: postId,
			status: statusUpdate.status,
			completionDateTime: statusUpdate.completionDateTime,
			completionDateTimeUtc: statusUpdate.completionDateTimeUtc
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates task due datetime.
	 *
	 * @param {int} hotelId
	 * @param {int|null} userId
	 * @param {int} postId
	 * @param {Object} properties
	 * @return {Promise}
	 **/
	updateTaskDueDateTime: async(hotelId, userId, postId, properties) => {
		logger.log('info', 'postDb|updateTaskDueDateTime',
			{hotelId: hotelId, userId: userId, postId: postId, properties: properties})

		const sql =
			`UPDATE
				posts
			SET
				due_dtm = :dueDateTime,
				due_dtm_utc = :dueDateTimeUtc
			WHERE
				hotel_id = :hotelId AND id = :postId
			`

		const criteria = {
			hotelId: hotelId,
			userId: userId,
			postId: postId,
			dueDateTime: properties.dueDateTime,
			dueDateTimeUtc: properties.dueDateTimeUtc
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates resolution info of post with guest complaint
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {object} complaintResolution
	 * @returns {Promise}
	 */
	updateComplaintResolution: async(hotelId, postId, complaintResolution) => {
		logger.log('info', 'postDb|updateComplaintResolution',
			{hotelId: hotelId, postId: postId, complaintResolutions: complaintResolution})

		const sql =
			`UPDATE
				posts
			SET
				resolution = :resolution,
				cost = :cost,
				points_given = :pointsGiven,
				resolution_dtm = :dateTime,
				resolution_dtm_utc = :dateTimeUTC
			WHERE
					hotel_id = :hotelId
				AND
					id = :postId`

		const criteria = {
			hotelId: hotelId,
			postId: postId,
			resolution: complaintResolution.resolution,
			cost: complaintResolution.cost,
			pointsGiven: complaintResolution.pointsGiven,
			dateTime: complaintResolution.dateTime,
			dateTimeUTC: complaintResolution.dateTimeUTC
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Insert a comment.
	 * @param {int} hotelId
	 * @param {int|null} userId
	 * @param {int} postId
	 * @param {Object} commentInfo
	 * @return {Promise}
	 */
	insertComment(hotelId, userId, postId, commentInfo) {
		logger.log('info', 'postDb|insertComment',
			{hotelId: hotelId, userId: userId, postId: postId, status: commentInfo})

		const sql =
			`INSERT INTO posts
					(type_id, hotel_id, description, description_text, language_id, private,
					created_by, created_dtm, created_dtm_utc, parent_id, order_dtm_utc)
				VALUES
					(:typeId, :hotelId, :description, :descriptionText, :languageId, :private,
					:createdBy, :createdDateTime, :createdDateTimeUtc, :parentId, :orderDateTimeUtc)
				`

		const criteria = {
			typeId: commentInfo.typeId,
			hotelId: hotelId,
			description: commentInfo.description,
			descriptionText: commentInfo.descriptionText,
			languageId: commentInfo.languageId,
			private: commentInfo.private,
			createdBy: userId,
			createdDateTime: commentInfo.createdDateTime,
			createdDateTimeUtc: commentInfo.createdDateTimeUtc,
			parentId: commentInfo.parentId,
			orderDateTimeUtc: commentInfo.orderDateTimeUtc
		}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Updates (increments or decrements) comment count.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {number} count
	 * @return {*}
	 */
	updateCommentCount(hotelId, postId, count) {
		logger.log('info', 'postDb|updateCommentCount',
			{hotelId: hotelId, postId: postId, count: count})

		const sql =
			`UPDATE
				posts
			SET
				comment_count = IF(comment_count IS null, 1, comment_count + 1)
			WHERE
				hotel_id = :hotelId AND id = :postId
			`

		const criteria = {
			hotelId: hotelId,
			postId: postId,
			count: count
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates post after comment added:
	 * - (increments or decrements) comment count.
	 * - update order date to now (UTC).
	 * @param {int} postId
	 * @param {string} nowUtc
	 * @return {*}
	 */
	async updatePostAfterCommentAdded(postId, nowUtc) {
		const sql =
			`UPDATE
				posts
			SET
				order_dtm_utc = :nowUtc,
				comment_count = IF(comment_count IS null, 1, comment_count + 1)
			WHERE
				id = :postId
			`

		const criteria = {
			nowUtc: nowUtc,
			postId: postId
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Updates post attachments count
	 * @param {int} postId
	 * @param {string} count
	 * @return {*}
	 */
	async updateAttachmentsCount(postId, count) {
		const sql =
			`UPDATE
				posts
			SET
				attachment_count = :count
			WHERE
				id = :postId
			`

		const criteria = {
			count: count,
			postId: postId
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Gets todo items Counts grouped by assignment
	 * @param {int} hotelId
	 * @param {Array} todoTypes
	 * @returns {Array|Object|*|{}}
	 */
	getToDoItemsCount: async(hotelId, todoTypes) => {
		const todoTypesClause = utils.getSelectInClause(todoTypes)

		const sql =
			`SELECT
				posts.assignment_type_id,
				posts.assignment_id,
				COUNT(posts.id) as count
			FROM
				posts
			WHERE
				posts.completion_status = 'open'
			AND
				posts.type_id in (${todoTypesClause})
			AND
				posts.hotel_id = ?
			GROUP BY
				posts.assignment_type_id, posts.assignment_id
			HAVING
				count > 0`
		const where = [hotelId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets todo items by department id
	 * @param {int} hotelId
	 * @param {int} departmentId
	 * @param {Array} todoTypes
	 * @returns {Array|Object|*|{}}
	 */
	getToDoItemsByDepartmentId: async(hotelId, departmentId, todoTypes) => {
		const todoTypesClause = utils.getSelectInClause(todoTypes)

		const sql =
			`SELECT
				posts.id,
				posts.type_id,
				posts.subject,
				posts.description,
				posts.due_dtm,
				posts.completion_status,
				posts.language_id,
				posts.location_type_id,
				posts.location_id,
				posts.parent_id,

				parent_posts.subject as parent_subject,
				parent_posts.description as parent_description,
				parent_posts.language_id as parent_language_id,
				
				tags.name as location_name
			FROM
				posts as posts
			LEFT OUTER JOIN
				posts as parent_posts
			ON
				posts.parent_id = parent_posts.id
			LEFT OUTER JOIN
				tags
			ON
				posts.location_id = tags.tag_id
			WHERE
				posts.completion_status = 'open'
			AND
				posts.hotel_id = ?
			AND
					((posts.assignment_type_id = 'dep' AND posts.assignment_id = ?)
				OR
					(parent_posts.assignment_type_id = 'dep' AND parent_posts.assignment_id = ?))
			AND
				posts.type_id in (${todoTypesClause})
			ORDER BY
				posts.due_dtm_utc
			DESC`

		const where = [hotelId, departmentId, departmentId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets todo items for employee
	 * @param {int} hotelId
	 * @param {int} employeeId
	 * @param {Array} todoTypes
	 * @returns {Array|Object|*|{}}
	 */
	getToDoItemsByEmployeeId: async(hotelId, employeeId, todoTypes) => {
		const todoTypesClause = utils.getSelectInClause(todoTypes)

		const sql =
			`SELECT
				posts.id,
				posts.type_id,
				posts.subject,
				posts.description,
				posts.due_dtm,
				posts.completion_status,
				posts.language_id,
				posts.location_type_id,
				posts.location_id,
				posts.parent_id,

				parent_posts.subject as parent_subject,
				parent_posts.description as parent_description,
				parent_posts.language_id as parent_language_id,
				
				tags.name as location_name
			FROM
				posts as posts
			LEFT OUTER JOIN
				posts as parent_posts
			ON
				posts.parent_id = parent_posts.id
			LEFT OUTER JOIN
				tags
			ON
				posts.location_id = tags.tag_id
			WHERE
				posts.completion_status = 'open'
			AND
				posts.hotel_id = ?
			AND
					((posts.assignment_type_id = 'emp' AND posts.assignment_id = ?)
				OR
					(parent_posts.assignment_type_id = 'emp' AND parent_posts.assignment_id = ?))
			AND
				posts.type_id in (${todoTypesClause})
			ORDER BY
				posts.due_dtm_utc
			DESC`

		const where = [hotelId, employeeId, employeeId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets todo items for all employees in a hotel
	 * @param {int} hotelId
	 * @param {Array} todoTypes
	 * @returns {Array|Object|*|{}}
	 */
	getEmployeesToDoItems: async(hotelId, todoTypes) => {
		const todoTypesClause = utils.getSelectInClause(todoTypes)

		const sql =
			`SELECT
				posts.id,
				posts.type_id,
				posts.subject,
				posts.description,
				posts.due_dtm,
				posts.completion_status,
				posts.language_id,
				posts.location_type_id,
				posts.location_id,
				posts.parent_id,

				parent_posts.subject as parent_subject,
				parent_posts.description as parent_description,
				parent_posts.language_id as parent_language_id,
				
				tags.name as location_name
			FROM
				posts as posts
			LEFT OUTER JOIN
				posts as parent_posts
			ON
				posts.parent_id = parent_posts.id
			LEFT OUTER JOIN
				tags
			ON
				posts.location_id = tags.tag_id
			WHERE
				posts.completion_status = 'open'
			AND
				posts.hotel_id = ?
			AND
					(posts.assignment_type_id = 'emp'
				OR
					parent_posts.assignment_type_id = 'emp')
			AND
				posts.type_id in (${todoTypesClause})
			ORDER BY
				posts.due_dtm_utc
			DESC`

		const where = [hotelId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets post by id
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {int} id
	 * @return {*}
	 */
	getById: async(hotelId, userId, id) => {
		logger.info('postDb|getById', {hotelId, userId, id})
		const sql = `
			SELECT
				posts.*,
				post_read_statuses.status AS read_status,
				users.first_name,
				users.last_name,
				users.profile_picture
			FROM posts
			LEFT OUTER JOIN post_read_statuses ON
				post_read_statuses.hotel_id = posts.hotel_id AND
				post_read_statuses.user_id = ? AND
				post_read_statuses.post_id = posts.id
			LEFT OUTER JOIN users ON posts.created_by = users.user_id
			WHERE
				posts.id = ? AND 
				posts.hotel_id = ?;`

		return await dbBase.findOne(sql, [userId, id, hotelId], null)
	},

	/**
	 * Inserts a post.
	 * @param {int} hotelId
	 * @param {int|null} userId
	 * @param {Object} post
	 * @return {Promise}
	 */
	insert: async(hotelId, userId, post) => {
		logger.log('info', 'postDb|insert',
			{hotelId: hotelId, userId: userId, post: post})

		const sql =
			`INSERT INTO posts
					(type_id, hotel_id, description, description_text, language_id, private,
					guest_complaint, subject, assignment_id, assignment_type_id, created_by, created_dtm,
					created_dtm_utc, order_dtm_utc, due_dtm, due_dtm_utc, completion_status,
					comment_count, attachment_count, flagged, flagged_by, status)
				VALUES
					(:typeId, :hotelId, :description, :descriptionText, :languageId, :private,
					:guestComplaint, :subject, :assignmentId, :assignmentType, :createdBy, :createdDateTime,
					:createdDateTimeUtc, :orderDateTimeUtc, :dueDateTime, :dueDateTimeUtc, :completionStatus,
					0, 0, 0, 0, 1)
				`

		const criteria = {
			typeId: post.typeId,
			hotelId: hotelId,
			description: post.description,
			descriptionText: post.descriptionText,
			languageId: post.languageId,
			private: post.private,
			guestComplaint: post.guestComplaint,
			subject: post.subject,
			assignmentId: post.assignmentId,
			assignmentType: post.assignmentType,
			createdBy: userId,
			createdDateTime: post.createdDateTime,
			createdDateTimeUtc: post.createdDateTimeUtc,
			orderDateTimeUtc: post.orderDateTimeUtc,
			dueDateTime: post.dueDateTime,
			dueDateTimeUtc: post.dueDateTimeUtc,
			completionStatus: post.completionStatus
		}

		return dbBase.insert(sql, criteria)
	},

	/**
	 * Converts a post to task.
	 *
	 * @param {int} hotelId
	 * @param {int|null} userId
	 * @param {int|null} postId
	 * @param {Object} properties
	 * @return {Promise}
	 */
	convertPostToTask: async(hotelId, userId, postId, properties) => {
		logger.log('info', 'postDb|convertPostToTask',
			{hotelId: hotelId, userId: userId, postId: postId, properties: properties})

		const sql =
			`UPDATE
				posts
			SET
				type_id = :typeId,
				description = :description,
				subject = :subject,
				assignment_id = :assignmentId,
				assignment_type_id = :assignmentType,
				due_dtm = :dueDateTime,
				due_dtm_utc = :dueDateTimeUtc,
				guest_complaint = :guestComplaint,
				created_dtm = :createdDateTime,
				created_dtm_utc = :createdDateTimeUtc,
				created_by = :createdBy,
				flagged = :flag,
				flagged_by = :flaggedBy,
				flagged_dtm = :flaggedDateTime,
				flagged_dtm_utc = :flaggedDateTimeUtc,
				flag_due_dtm = :flagDueDateTime,
				flag_due_dtm_utc = :flagDueDateTimeUtc
			WHERE
				hotel_id = :hotelId AND id = :postId`

		const criteria = {
			hotelId: hotelId,
			postId: postId,
			typeId: properties.typeId,
			description: properties.description,
			subject: properties.subject,
			assignmentId: properties.assignmentId,
			assignmentType: properties.assignmentType,
			dueDateTime: properties.dueDateTime,
			dueDateTimeUtc: properties.dueDateTimeUtc,
			guestComplaint: properties.guestComplaint,
			createdDateTime: properties.createdDateTime,
			createdDateTimeUtc: properties.createdDateTimeUtc,
			createdBy: userId,
			flagged: properties.flagged,
			flaggedBy: properties.flaggedBy,
			flaggedDateTime: properties.flaggedDateTime,
			flaggedDateTimeUtc: properties.flaggedDateTimeUtc,
			flagDueDateTime: properties.flagDueDateTime,
			flagDueDateTimeUtc: properties.flagDueDateTimeUtc
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Gets reports by post types
	 *
	 * @param {int} hotelId
	 * @param {Array} postTypes
	 * @return {Promise}
	 */
	getReportsByPostTypes: async(hotelId, postTypes) => {
		logger.log('info', 'postDb|getReportsByPostTypes', {hotelId: hotelId, postTypes: postTypes})

		const inClause = utils.getSelectInClause(postTypes)
		const sql = `
			SELECT
				posts.*
			FROM
				posts
			WHERE posts.hotel_id = ? AND
				type_id IN (${inClause})`
		const where = [hotelId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets report by id
	 *
	 * @param {int} hotelId
	 * @param {int} reportId
	 * @return {Promise}
	 */
	getReportById: async(hotelId, reportId) => {
		//TODO: vvs p2 REFACTOR - bad code -> getByTypeIds (only needed fields). review/refactor other methods.
		logger.log('info', 'postDb|getReportById', {hotelId: hotelId, reportId: reportId})

		const sql = `
			SELECT
				posts.*
			FROM
				posts
			WHERE posts.hotel_id = ? AND id  = ?`
		const where = [hotelId, reportId]

		return await dbBase.findOne(sql, where, null)
	},

	/**
	 * Gets preventive maintenance logs by report id
	 *
	 * @param {int} hotelId
	 * @param {int} reportId
	 * @param {string|null} completedFromDateTime
	 * @return {Promise}
	 */
	getPreventiveMaintenanceLogsByReportId: async(hotelId, reportId, completedFromDateTime) => {
		logger.log('info', 'postDb|getPreventiveMaintenanceLogsByReportId',
			{hotelId: hotelId, reportId: reportId, completedFromDateTime: completedFromDateTime})

		const completionWhereClause = completedFromDateTime ?
			`posts.completion_dtm IS NOT NULL AND posts.completion_dtm >= '${completedFromDateTime}'` :
			`posts.completion_dtm IS NOT NULL`
		const sql = `
			SELECT
				posts.*,
				tags.name AS room_name
			FROM
				posts
			JOIN tags ON tags.tag_id = posts.location_id
			WHERE posts.hotel_id = ? AND
				posts.parent_id = ? AND
				posts.type_id = 'pms' AND
				posts.location_type_id = 'room' AND
				${completionWhereClause}
			ORDER BY posts.completion_dtm`
		const where = [hotelId, reportId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets checklist logs by report id
	 *
	 * @param {int} hotelId
	 * @param {int} reportId
	 * @param {string|null} completedFromDateTime
	 * @return {Promise}
	 */
	getChecklistLogsByReportId: async(hotelId, reportId, completedFromDateTime) => {
		logger.log('info', 'postDb|getPreventiveMaintenanceLogsByReportId', {hotelId: hotelId, reportId: reportId})

		const completionWhereClause = completedFromDateTime ?
			`posts.completion_dtm IS NOT NULL AND posts.completion_dtm >= '${completedFromDateTime}'` :
			`posts.completion_dtm IS NOT NULL`
		const sql = `
			SELECT
				posts.*
			FROM
				posts
			WHERE posts.hotel_id = ? AND
				posts.parent_id = ? AND
				posts.type_id = 'chls' AND
				${completionWhereClause}
			ORDER BY posts.completion_dtm`
		const where = [hotelId, reportId]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Gets a post with guest complaint by id
	 * @param {int} hotelId
	 * @param {int} postId
	 * @returns {*}
	 */
	getPostComplaintById: async(hotelId, postId) => {
		logger.log('info', 'postDb|getComplaintPostById', {hotelId, postId})

		const sql =
			`SELECT
				*
			FROM
				posts
			WHERE
					posts.id = :postId
				AND
					posts.hotel_id = :hotelId
				AND
					posts.guest_complaint = 1`

		return await dbBase.findOne(sql, {hotelId, postId}, null)
	}
}
