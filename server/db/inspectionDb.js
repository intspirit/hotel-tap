'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Post db for inspections
 */
export default {

	/**
	 * Gets post (of type "inspection") by id
	 * @param {int} hotelId
	 * @param {int} postId
	 * @returns {*}
	 */
	async getById(hotelId, postId) {
		logger.log('info', 'inspectionDb|getById', {hotelId, postId})

		const sql =
			`SELECT
				posts.id,
				posts.subject AS name,
				posts.description,
				posts.fails_all AS failsAll,
				posts.inspection_points AS inspectionPoints
			FROM
				posts
			WHERE
				posts.hotel_id = ? and posts.id = ?;`

		return await dbBase.findOne(sql, [hotelId, postId], null)
	},

	/**
	 * Gets posts of specified type having ids from parentIds array
	 * @param {int} hotelId
	 * @param {int[]} parentIds
	 * @param {string} postType
	 * @returns {*}
	 */
	async getByParentIds(hotelId, parentIds, postType) {
		logger.log('info', 'inspectionDb|getByParentIds', {hotelId, parentIds, postType})

		const inClause = utils.getSelectInClause(parentIds)
		const sql =
			`SELECT
				posts.id,
				posts.subject,
				posts.sort_order as sortOrder,
				posts.inspection_points as inspectionPoints,
				posts.fails_all as failsAll
			FROM
				posts
			WHERE
					posts.hotel_id = ?
				AND
					posts.type_id = ?
				AND
					deleted = 0
				AND
					posts.parent_id
						IN (${inClause})`

		return dbBase.findAll(sql, [hotelId, postType], [])
	},

	/**
	 * Inserts an inspection.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {string} type
	 * @param {string} name
	 * @param {string} description
	 * @returns {*}
	 */
	async insertInspection(hotelId, userId, type, name, description = '') {
		logger.log('info', 'inspectionDb|insertInspection', {hotelId, userId, type, name, description})

		const sql =
			`INSERT INTO
				posts
					(hotel_id, type_id, subject, description, created_by)
			VALUES
				(:hotelId, :type, :name, :description, :userId)`

		return dbBase.insert(sql, {hotelId, userId, type, name, description})
	},

	/**
	 * Updates post (of type "inspection")
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {object} inspection
	 * @returns {*|int}
	 */
	async updateInspection(hotelId, postId, inspection) {
		logger.log('info', 'inspectionDb|updateInspection', {hotelId, postId, inspection})

		const sql =
			`UPDATE
				posts
			SET
				subject = :name,
				description = :description,
				inspection_points = :inspectionPoints
			WHERE
					hotel_id = :hotelId
				AND
					id = :postId;`

		const criteria = {
			hotelId,
			postId,
			name: inspection.name,
			description: inspection.description,
			inspectionPoints: inspection.inspectionPoints
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Inserts and updates multiple inspection tasks at once
	 * @param {Object[]} tasks
	 * @returns {*}
	 */
	async insertUpdateInspectionTasksBulk(tasks) {
		logger.log('info', 'inspectionDb|insertUpdateInspectionTasksBulk', {tasks})

		const sql =
			`INSERT INTO
				posts
					(id, type_id, hotel_id, parent_id, subject, description,
					sort_order, deleted, inspection_points, fails_all)
			VALUES
				?
			ON DUPLICATE KEY UPDATE
				subject = VALUES(subject),
				sort_order = VALUES(sort_order),
				deleted = VALUES(deleted),
				inspection_points = VALUES(inspection_points),
				fails_all = VALUES(fails_all);`

		return dbBase.insert(sql, [tasks])
	},

	/**
	 * Inserts and updates multiple inspection schedules
	 * @param {Object[]} schedules
	 * @returns {*}
	 */
	async insertUpdateInspectionSchedulesBulk(schedules) {
		logger.log('info', 'inspectionDb|insertUpdateInspectionSchedulesBulk', {schedules})

		const sql =
			`INSERT INTO
				posts
					(id, type_id, hotel_id, parent_id, inspection_by, assignment_type_id, assignment_id,
						location_type_id, location_id, due_dtm, due_dtm_utc, completion_status, deleted, description)
			VALUES
				?
			ON DUPLICATE KEY UPDATE
				due_dtm = VALUES(due_dtm),
				due_dtm_utc = VALUES(due_dtm_utc),
				location_id = VALUES(location_id),
				assignment_id = VALUES(assignment_id),
				inspection_by = VALUES(inspection_by),
				deleted = VALUES(deleted);`

		return dbBase.insert(sql, [schedules])
	}
}
