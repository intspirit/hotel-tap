'use strict'

import dbBase from './dbBase'
import EmployeeDepartment from '../models/EmployeeDepartment'
import utils from '../core/utils'

/**
 * Employee department db.
 */
export default {

	/**
	 * Gets employee departments for multiple departments.
	 * @param {int[]} departmentIds
	 * @returns {Promise}
	 */
	getByDepartmentIds: async departmentIds => {
		const inClause = utils.getSelectInClause(departmentIds)
		const sql = `
			SELECT
				emp_dept_id, emp_id, dept_id
			FROM employee_departments
			WHERE dept_id IN (${inClause}) AND status = 1`
		const where = []
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new EmployeeDepartment(x))
	},

	/**
	 * Gets departments for an employee.
	 * @param {int} userId
	 * @returns {Promise}
	 */
	getByUserId: async userId => {
		const sql = `
			SELECT
				emp_dept_id, emp_id, dept_id
			FROM employee_departments
			WHERE emp_id = ? AND status = 1`
		const where = [userId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new EmployeeDepartment(x))
	}
}
