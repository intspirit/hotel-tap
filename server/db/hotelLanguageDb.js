'use strict'

import dbBase from './dbBase'

/**
 * Hotel languages db
 */
export default {

	/**
	 * Gets all languages associated with a given hotel
	 * @param {int} hotelId
	 * @returns {Promise}
	 */
	getByHotelId: async (hotelId) => {
		const sql =
			`SELECT
				hotel_languages.id,
				hotel_languages.hotel_id,
				hotel_languages.language_id,
				hotel_languages.status,
				languages.code
			FROM
				hotel_languages
			INNER JOIN
				languages
			ON
				hotel_languages.language_id = languages.id
			WHERE
				hotel_languages.hotel_id = ?`

		const where = [hotelId]

		return dbBase.findAll(sql, where)
	}
}
