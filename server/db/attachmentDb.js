'use strict'

import dbBase from './dbBase'
import utils from '../core/utils'
import Attachment from '../models/Attachment'
import logger from '../core/logger'

/**
 * Attachment db.
 */
export default {

	/**
	 * Gets attachments by post id list.
	 * @param {Array} postIds
	 * @returns {Array}
	 */
	getByPostIds: async postIds => {
		//TODO: vvs p1 hotelId???
		const inClause = utils.getSelectInClause(postIds)
		const sql = `
			SELECT
				attachment_id, file_path, tmp_file_path, post_id
			FROM attachements
			WHERE post_id IN (${inClause}) AND status = ?`
		const where = [1]
		const dbData = await dbBase.findAll(sql, where, [])

		return dbData.map(x => new Attachment(x))
	},

	/**
	 * Update attachments to stable.
	 * @param {int} postId
	 * @param {string} tmpFilePath
	 * @param {string} filePath
	 * @returns {Array}
	 */
	updateFromTmpToStable: async (postId, tmpFilePath, filePath) => {
		const sql = `
			UPDATE
				attachements
			SET
				file_path = :filePath,
				tmp_file_path = NULL
			WHERE
				tmp_file_path = :tmpFilePath AND
				post_id = :postId`
		const criteria = {postId, tmpFilePath, filePath}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Inserts multiple read statuses for a post.
	 * @param {Array} attachements
	 * @returns {Promise}
	 */
	insertBulk: async (attachements) => {
		logger.log('info', 'attachementDb|insertBulk', {attachements: attachements})

		const sql =
			`INSERT INTO attachements
					(post_id, source, file_path, tmp_file_path, created_by, created_date, status)
				VALUES ?`

		return dbBase.insert(sql, [attachements])
	}
}
