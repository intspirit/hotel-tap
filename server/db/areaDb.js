'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import Area from '../models/Area'

/**
 * Area db.
 */
export default {

	/**
	 * Gets areas for a hotel.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getByHotelId: async (hotelId) => {
		logger.log('info', 'areaDb|getByHotelId', {hotelId: hotelId})

		const sql = `
			SELECT
				area_id, name
			FROM areas
			WHERE hotel_id = ? AND status = 1;`
		const dbData = await dbBase.findAll(sql, [hotelId], null)

		return dbData.map(x => new Area(x))
	},

	/**
	 * Gets hotel rooms.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getRoomsByHotelId: async (hotelId) => {
		logger.log('info', 'areaDb|getRoomsByHotelId', {hotelId})

		const sql = `
			SELECT tags.tag_id area_id, tags.name
			FROM tags
			WHERE
			    status = 1 AND
				type = 'area' AND
				type_id = (
					SELECT area_id
					FROM areas
					WHERE hotel_id = ? AND name = 'Rooms'
					LIMIT 1)
			ORDER BY tags.name;`
		const dbData = await dbBase.findAll(sql, [hotelId], null)

		return dbData.map(x => new Area(x))
	},

	/**
	 * Gets hotel areas.
	 * @description It returns not top level area groups, but speciic areas (others than rooms) from tags table.
	 *
	 * @param {Number} hotelId
	 * @return {Promise}
	 */
	getAreasByHotelId: async (hotelId) => {
		logger.log('info', 'areaDb|getAreasByHotelId', {hotelId})

		const sql = `
			SELECT tags.tag_id AS area_id, areas.name AS area_name, tags.name
			FROM tags
			INNER JOIN areas ON tags.type_id = areas.area_id
			WHERE tags.hotel_id = ? AND areas.name != 'Rooms'
			ORDER BY 2, 3;`
		const dbData = await dbBase.findAll(sql, [hotelId], [])

		return dbData.map(x => new Area(x))
	}
}
