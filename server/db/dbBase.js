'use strict'

import mysql from 'mysql2/promise'
import logger from '../core/logger'
import config from '../config/environment'

logger.info(config)

const getConnection = () => mysql.createConnection({
	host: config.database.host,
	database: config.database.database,
	user: config.database.userName,
	password: config.database.password,
	dateStrings: true,
	namedPlaceholders: true,
	supportBigNumbers: true
})

export default {

	findAll: async (sql, criteria, defaultResult) => {
		let conn = null
		try {
			conn = await getConnection()
			const [rows] = await conn.execute(sql, criteria)
			await conn.end()

			return rows
		} catch (err) {
			logger.error('dbBase|findAll', {error: err, sql: sql, criteria: criteria})
			await conn.end()

			return defaultResult
		}
	},

	findOne: async (sql, criteria, defaultResult) => {
		let conn = null
		try {
			conn = await getConnection()
			const [rows] = await conn.execute(sql, criteria)
			await conn.end()

			return rows.length > 0 ? rows[0] : defaultResult
		} catch (err) {
			logger.log('error', `dbBase|findOne|error:${err}|sql:${sql}`, {criteria: criteria})
			await conn.end()

			return defaultResult
		}
	},

	/**
	 * Executes UPDATE query.
	 * @param {string} sql
	 * @param {Object} criteria
	 * @returns {int} - number of updated records.
	 */
	update: async (sql, criteria) => {
		let conn = null
		try {
			conn = await getConnection()
			const result = await conn.query(sql, criteria)
			await conn.end()

			return result[0].affectedRows
		} catch (err) {
			logger.log('error', `dbBase|update|error:${err}|sql:${sql}`, {criteria: criteria})
			await conn.end()

			throw err
		}
	},

	insert: async (sql, criteria) => {
		let conn = null
		try {
			conn = await getConnection()
			const result = await conn.query(sql, criteria)

			return result[0].insertId
		} catch (err) {
			logger.log('error', `dbBase|insert|error:${err}|sql:${sql}`, {criteria: criteria})
			await conn.end()

			throw err
		}
	},

	delete: async (sql, criteria) => {
		let conn = null
		try {
			conn = await getConnection()
			const result = await conn.query(sql, criteria)
			await conn.end()

			return result[0].affectedRows
		} catch (err) {
			logger.log('error', `dbBase|delete|error:${err}|sql:${sql}`, {criteria: criteria})
			await conn.end()

			throw err
		}
	}
}
