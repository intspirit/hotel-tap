'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import Tag from '../models/Tag'
import {tagLegacyType} from '../../shared/constants'
import utils from '../core/utils'

const buildFetchQuery = whereClause => `
	SELECT
		tags.tag_id,
		tags.type,
		COALESCE(equipment_groups.equipment_group_id, tags.type_id) AS type_id,
		COALESCE(
			departments.name,
			areas.name,
			maintenance_types.name,
			equipment_groups.name) AS type_name,
		tags.name,
		tags.status
	FROM tags
		LEFT JOIN departments ON tags.type = '${tagLegacyType.DEPARTMENT}' AND tags.type_id = departments.department_id
		LEFT JOIN areas ON tags.type = '${tagLegacyType.AREA}' AND tags.type_id = areas.area_id
		LEFT JOIN maintenance_types ON tags.type = '${tagLegacyType.MAINTENANCE}' AND
			tags.type_id = maintenance_types.maintenance_type_id
		LEFT JOIN equipment ON tags.type = '${tagLegacyType.EQUIPMENT}' AND tags.type_id = equipment.equipment_id
		LEFT JOIN equipment_groups ON equipment_groups.equipment_group_id = equipment.equipment_group_id
	WHERE tags.special IS NULL ${whereClause};`

/**
 * Tag db.
 */
export default {

	/**
	 * Gets hotel tags.
	 *
	 * @param {Number} hotelId
	 * @return {Array}
	 */
	getByHotelId: async (hotelId) => {
		logger.log('info', `tagDb|getByHotelId|hotelId:${hotelId}`)

		const whereClause = `AND tags.hotel_id = ?`
		const sql = buildFetchQuery(whereClause)
		const where = [hotelId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Tag(x))
	},

	/**
	 * Gets tag by id.
	 *
	 * @param {int} id
	 * @returns {null | Tag}
	 */
	getById: async (id) => {
		logger.log('info', `tagDb|getById|id:${id}`)

		const whereClause = `AND tags.tag_id = ?`
		const sql = buildFetchQuery(whereClause)
		const dbData = await dbBase.findOne(sql, [id], null)

		return dbData === null ? null : new Tag(dbData)
	},

	/**
	 * Gets tags by a list of ids.
	 *
	 * @param {Array} ids
	 * @returns {Array}
	 */
	getByIds: async (ids) => {
		const inClause = utils.getSelectInClause(ids)
		const whereClause = `AND tags.tag_id IN (${inClause})`
		const sql = buildFetchQuery(whereClause)
		const dbData = await dbBase.findAll(sql, [], null)

		return dbData.map(x => new Tag(x))
	},

	insert: async (hotelId, userId, tag, now) => {
		logger.log('info', 'tagDb|insert', {hotelId, userId, tag, now})

		const sql =
			`INSERT INTO tags
					(hotel_id, name, type, type_id, created_by, created_date, modified_by, modified_date,
						public)
				VALUES
					(:hotelId, :name, :type, :typeId, :createdBy, :createdDateTime, :modifiedBy, :modifiedDateTime,
						0)
				`
		const criteria = {
			hotelId: hotelId,
			name: tag.name,
			type: tag.type,
			typeId: tag.typeId,
			createdBy: userId,
			createdDateTime: now,
			modifiedBy: userId,
			modifiedDateTime: now
		}

		return dbBase.insert(sql, criteria)
	}
}
