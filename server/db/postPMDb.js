'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'
import utils from '../core/utils'

/**
 * Post db for PM.
 */
export default {

	/**
	 * Gets post by id
	 * @param {int} hotelId
	 * @param {int} id
	 * @return {*}
	 */
	getById: async(hotelId, id) => {
		logger.info('postPMDb|getById:', {hotelId, id})
		const sql = `
			SELECT
				id, 
				subject AS name,
				description,
				assignment_type_id AS assignmentType,
				assignment_id AS assignmentId,
				location_type_id AS locationType
			FROM posts
			WHERE id = ? AND hotel_id = ?;`

		return await dbBase.findOne(sql, [id, hotelId], null)
	},

	/**
	 * Gets posts for department.
	 * @param {int} hotelId
	 * @param {int[]} parentIds
	 * @param {string} postType
	 * @returns {Array|Object|*|{}}
	 */
	getByParentIds: async(hotelId, parentIds, postType) => {
		logger.log('info', 'postPMDb|getByParentIds:', {hotelId, parentIds, postType})
		const inClause = utils.getSelectInClause(parentIds)
		const sql = `
			SELECT 
				id, subject,  sort_order AS sortOrder
			FROM posts
			WHERE hotel_id = ? AND type_id = ? AND parent_id IN (${inClause}) AND deleted = 0;`
		const where = [hotelId, postType]

		return await dbBase.findAll(sql, where, [])
	},

	/**
	 * Updates the PM.
	 * @param {int} hotelId
	 * @param {int} postId
	 * @param {Object} pm
	 * @returns {Promise.<int>}
	 */
	async updatePM(hotelId, postId, pm) {
		logger.log('info', 'postPMDb|updatePM:', {hotelId, postId, pm})
		const sql =
			`UPDATE posts
			SET
				subject = :name,
				description = :description,
				assignment_type_id = :assignmentType,
				assignment_id = :assignmentId
			WHERE
				id = :postId AND
				hotel_id = :hotelId;`

		const criteria = {
			name: pm.name,
			description: pm.description,
			assignmentType: pm.assignmentType,
			assignmentId: pm.assignmentId,
			postId: postId,
			hotelId: hotelId
		}

		return dbBase.update(sql, criteria)
	},

	/**
	 * Inserts and updates multiple PM tasks.
	 * @param {Object[]} pmTasks
	 * @returns {*}
	 */
	insertUpdatePMTasksBulk: async (pmTasks) => {
		logger.log('info', 'postPMDb|insertUpdatePMTasksBulk:', {pmTasks})

		const sql =
			`INSERT INTO posts
			(id, type_id, hotel_id, parent_id, subject, sort_order, deleted, description)
			VALUES ?
			ON DUPLICATE KEY UPDATE
			  subject    = VALUES(subject),
			  sort_order = VALUES(sort_order),
			  deleted    = VALUES(deleted);`

		return dbBase.insert(sql, [pmTasks])
	},

	/**
	 * Inserts and updates multiple PM schedules.
	 * @param {Object[]} pmSchedules
	 * @returns {*}
	 */
	insertUpdatePMSchedulesBulk: async (pmSchedules) => {
		logger.log('info', 'postPMDb|insertUpdatePMSchedulesBulk:', {pmSchedules})

		const sql =
			`INSERT INTO posts
			(id, type_id, hotel_id, parent_id, due_dtm, due_dtm_utc, completion_status,
			 location_type_id, location_id, deleted, description)
			VALUES ?
			ON DUPLICATE KEY UPDATE
			  due_dtm    = VALUES(due_dtm),
			  due_dtm_utc = VALUES(due_dtm_utc),
			  location_id = VALUES(location_id),
			  deleted    = VALUES(deleted);`

		return dbBase.insert(sql, [pmSchedules])
	},

	/**
	 * Inserts PM.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @param {string} type
	 * @param {Object} pm
	 * @returns {*}
	 */
	insertPM: async (hotelId, userId, type, pm) => {
		logger.log('info', 'postPMDb|insertPM:', {hotelId, userId, pm})

		let {name, description, locationType} = pm

		const sql =
			`INSERT INTO posts
			(type_id, hotel_id, subject, description, location_type_id, created_by)
			VALUES (:type, :hotelId, :name, :description, :locationType, :userId)`

		return dbBase.insert(sql, {type, hotelId, name, description, locationType, userId})
	}
}
