'use strict'

import dbBase from './dbBase'
import logger from '../core/logger'

/**
 * Job statuses db
 */
export default {

	/**
	 * Inserts a job status
	 *
	 * @param {Object} jobStatus
	 * @param {int} jobStatus.jobId
	 * @param {string|null} jobStatus.meta
	 * @param {string} jobStatus.status
	 * @param {string} jobStatus.result
	 * @param {Date|string} jobStatus.createdDateTimeUtc
	 *
	 * @returns {Promise}
	 */
	insert: async (jobStatus) => {
		logger.log('info', 'jobStatusDb|insert', {jobStatus})

		const sql =
			`INSERT INTO job_statuses
				(job_id, meta, status, result, created_dtm_utc)
			VALUES
				(:jobId, :meta, :status, :result, :createdDateTimeUtc)`

		return dbBase.insert(sql, jobStatus)
	}
}
