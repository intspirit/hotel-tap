'use strict'

import dbBase from './dbBase'
import Department from '../models/Department'
import utils from '../core/utils'

/**
 * Department db.
 */
export default {

	/**
	 * Gets a department by id.
	 * @param {int} id
	 * @return {Promise}
	 */
	getById: async id => {
		const sql = `
			SELECT
				department_id, hotel_id, name, dept_image, special
			FROM departments
			WHERE department_id = ?`
		const where = [id]
		const dbData = await dbBase.findOne(sql, where, null)

		return dbData === null ? null : new Department(dbData)
	},

	/**
	 * Gets hotel departments.
	 * @param {int} hotelId
	 * @return {Promise}
	 */
	getByHotelId: async hotelId => {
		const sql = `
			SELECT
				department_id, hotel_id, name, dept_image, special
			FROM departments
			WHERE hotel_id = ? AND status = 1`
		const where = [hotelId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Department(x))
	},

	/**
	 * Gets user departments.
	 * @param {int} hotelId
	 * @param {int} userId
	 * @return {Promise}
	 */
	getByUser: async (hotelId, userId) => {
		const sql = `
			SELECT
				department_id, hotel_id, name, dept_image, special
			FROM departments
			INNER JOIN employee_departments ON departments.department_id =  employee_departments.dept_id
			WHERE hotel_id = ? AND emp_id = ?`
		const where = [hotelId, userId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Department(x))
	},

	/**
	 * Gets departments by a list of ids.
	 * @param {Array} ids
	 * @returns {Array}
	 */
	getByIds: async ids => {
		const inClause = utils.getSelectInClause(ids)
		const sql = `
			SELECT
				department_id, name, dept_image, special
			FROM departments
			WHERE department_id IN (${inClause})`
		const where = []
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Department(x))
	}
}
