'use strict'

// import models from '../models'
import dbBase from './dbBase'
import Hotel from '../models/Hotel'
import utils from '../core/utils'

/**
 * Hotel db.
 */
export default {

	/**
	 * Gets a hotel by id.
	 * @param {int} id
	 * @return {Promise}
     */
	getById: async id => {
		const sql = `
			SELECT
				hotel_id, hotel_admin, name, webaddr, timezone
			FROM hotels
			WHERE hotel_id = ?`
		const where = [id]
		const dbData = await dbBase.findOne(sql, where, null)

		return dbData === null ? null : new Hotel(dbData)
	},

	/**
	 * Gets admin hotels.
	 * @param {int} adminId
	 * @return {Promise}
     */
	getByAdmin: async adminId => {
		const activeStatuses = Hotel.activeStatuses
		const inClause = utils.getSelectInClause(activeStatuses)
		const sql = `
			SELECT
				hotels.hotel_id, hotels.hotel_admin, hotels.name, hotels.webaddr, hotels.timezone
			FROM
				hotels
			JOIN
				users ON (
					hotels.hotel_admin = users.user_id OR
					users.hotel_id = hotels.hotel_id
				)
			WHERE
				users.user_id = ? AND
				users.role_id = 2 AND
				hotels.status IN (${inClause})`

		const where = [adminId]
		const dbData = await dbBase.findAll(sql, where, null)

		return dbData.map(x => new Hotel(x))
	}
}
