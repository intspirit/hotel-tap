'use strict'

import dbBase from './dbBase'

/**
 * Employee board db.
 */
export default {

	/**
	 * Gets employee boards.
	 * @param {int} userId
	 * @returns {Promise}
	 */
	getByUserId: async userId => {
		const sql = `
			SELECT
				emp_board_id AS id, 
				emp_id AS employeeId,
				dept_id AS departmentId
			FROM employee_boards
			WHERE emp_id_= ? AND status = 0`
		const where = [userId]

		const dbData = await dbBase.findAll(sql, where, null)

		return dbData || []
	}
}
