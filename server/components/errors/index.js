/**
 * Error responses
 */
const errorResponses = []
const pageNotFound = (req, res) => {
	const
		viewFilePath = '404',
		statusCode = 404,
		result = {
			status: statusCode
		}

	res.status(result.status)
	res.render(viewFilePath, {}, (err, html) => {
		if (err) {
			return res.json(result, result.status)
		}

		return res.send(html)
	})
}
errorResponses[404] = pageNotFound

module.exports = errorResponses
