'use strict'

import auth from './auth'
import authRule from './authRule'

export default {
	authRule,
	auth
}
