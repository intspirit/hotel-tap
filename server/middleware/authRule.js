'use strict'

import constants from '../../shared/constants.js'
import {AuthorizationError, logError, getResponseError} from '../errors'

/**
 * Authorization middleware.
 */
const authRule = {

	/**
	 * Rule: is user an admin.
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Function} next
	 * @return {void|Object|json}
	 */
	isAdmin: (req, res, next) => {
		if (req.user.roleId !== constants.role.ADMIN) {
			const error = new AuthorizationError('User is not admin', {user: req.user, req: req})
			logError(error)
			const responseError = getResponseError(error)

			res.status(responseError.error.code)
			res.json(responseError)

			return
		}

		next()
	}
}

export default authRule
