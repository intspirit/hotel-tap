'use strict'

import config from '../../config/environment'
import {dateTimeHelper} from '../../../shared/core'

export function render (locals) {
	return `
		<html>
			<body>
				<table>
					<tr>
						<td><strong>Comment from:</strong></td>
						<td>${locals.user.fullName}<td>
					</tr>
					<tr>
						<td><strong>Posted By:</strong></td>
						<td>${locals.user.fullName}<td>
					</tr>
					<tr>
						<td><strong>Date and Time Posted:</strong></td>
						<td>${dateTimeHelper.getDateTimeFormatted(locals.comment.createdDateTime)}<td>
					</tr>
					<tr>
						<td><strong>Comment:</strong></td>
						<td>${locals.comment.description}<td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="${config.brand.url}/posts/${locals.postId}">Click here</a> to see it in ${config.brand.name}
						<td>
					</tr>
				</table>
			</body>
		</html>`
}

