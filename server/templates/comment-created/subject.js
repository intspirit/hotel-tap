'use strict'

export function render (locals) {
	return `Comment from ${locals.user.fullName} @${locals.hotel.webAddress}`
}
