'use strict'

import _ from 'lodash'
import config from '../../config/environment'
import constants from '../../../shared/constants'
import {dateTimeHelper} from '../../../shared/core'

export function render (locals) {
	return `
		<html>
			<body>
				<table>
					<tr>
						<td><strong>${_.startCase(locals.post.typeId)} from:</strong></td>
						<td>${locals.board}<td>
					</tr>
					<tr>
						<td><strong>Assigned To:</strong></td>
						<td>${locals.assignmentTo || constants.emptyTaskAssignmentMessage}<td>
					</tr>
					<tr>
						<td><strong>Subject:</strong></td>
						<td>${locals.post.subject}<td>
					</tr>
					<tr>
						<td><strong>${_.startCase(locals.post.typeId)} Details:</strong></td>
						<td>${locals.post.description}<td>
					</tr>
					<tr>
						<td><strong>Due Date:</strong></td>
						<td>${locals.post.dueDateTime ?
								dateTimeHelper.getDateTimeFormatted(locals.post.dueDateTime) :
								constants.emptyDueDateTimeMessage}<td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="${config.brand.url}/posts/${locals.postId}">Click here</a> to see it in ${config.brand.name}
						<td>
					</tr>
				</table>
			</body>
		</html>`
}
