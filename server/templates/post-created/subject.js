'use strict'

import _ from 'lodash'

export function render (locals) {
	return `${_.startCase(locals.post.typeId)} from ${locals.board} @${locals.hotel.webAddress}`
}
