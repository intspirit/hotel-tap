'use strict'

import nodemailer from 'nodemailer'
import sesTransport from 'nodemailer-ses-transport'
import stubTransport from 'nodemailer-stub-transport'
import config from '../config/environment'
import logger from './logger'

const initializeTransport = () => {
	switch (process.env.NODE_ENV) {
		case 'stage':
			return sesTransport(config.ses)
		default:
			return stubTransport()
	}
}
const transporter = nodemailer.createTransport(initializeTransport())

const mailer = {
	async send(fromEmail, toEmail, mail) {
		logger.log('info', 'mailer|send', {fromEmail, toEmail, mail})

		const mailToSend = {
			from: fromEmail,
			to: toEmail,
			subject: mail.subject,
			html: mail.html
		}

		return await transporter.sendMail(mailToSend)
	}
}

export default mailer

