'use strict'

import _ from 'lodash'
import jwt from 'jsonwebtoken'
import moment from 'moment-timezone'
import config from '../config/environment'
import constants from '../../shared/constants'
import {utils as sharedUtils} from '../../shared/core'

const JWTConfig = config.JWT

const utils = {

	/**
	 * Gets complete URL for the profile image.
	 * @param {string|null} [fileName=default.png]
	 * @returns {string}
	 */
	getUserImageUrl: fileName =>
		`${config.s3.rootUrl}${config.s3.bucket}/img/profile/${fileName || constants.defaultImageFileName.EMPLOYEE}`,

	/**
	 * Gets complete URL for the attachment.
	 * @param {string|null}fileName
	 * @returns {string}
	 */
	getAttachmentUrl: fileName =>
		`${config.s3.rootUrl}${config.s3.bucket}/${fileName}`,

	/**
	 * Gets complete URL for the attachment stored inTemp bucket.
	 * @param {string|null}fileName
	 * @returns {string}
	 */
	getTmpAttachmentUrl: fileName =>
		`${config.s3.rootUrl}${config.s3.tmpBucket}/${fileName}`,

	/**
	 * Gets the original attachment file name from the name in the storage.
	 * @param {string} fileName
	 * @returns {string}
	 */
	getAttachmentOriginalName: fileName => {
		const underscoreIndex = fileName.indexOf('_')
		if (underscoreIndex === -1) return fileName

		return fileName.substr(underscoreIndex + 1)
	},

	/**
	 * Gets complete URL for the document.
	 * @param {string|null}fileName
	 * @returns {string}
	 */
	getDocumentUrl: fileName =>
		`${config.s3.rootUrl}${config.s3.bucket}/img/doc/${fileName}`,

	/**
	 * Gets complete URL for the department image.
	 * @param {string|null} [fileName=icons/iconD.png]
	 * @returns {string}
	 */
	getDepartmentImageUrl: fileName =>
		`${config.s3.rootUrl}${config.s3.bucket}/img/${fileName || constants.defaultImageFileName.DEPARTMENT}`,

	/**
	 * Gets IN clause for db SELECT statement from array of values.
	 * @param {Array} inValues
	 * @return {string}
	 */
	getSelectInClause: (inValues) => `'${inValues.join("','")}'`,

	/**
	 * Gets SET clause for db UPDATE statement from object using db fields mapping.
	 *
	 * @param {Object} dbFields
	 * @param {Object} properties
	 * @deprecated DO NOT USE - WILL BE REMOVED.
	 * @return {string}
	 */
	//TODO: vvs p2 REFACTOR remove
	getUpdateSetClause: (dbFields, properties) =>
		_.chain(properties).
			omitBy((value, key) => _.isUndefined(dbFields[key])).
			map((value, key) => `${dbFields[key]} = :${key}`).
			value().
			join(',\n'),

	/**
	 * Gets Ip address from the request.
	 * @param {object} req
	 * @param {string[]} [req.headers]
	 * @param {object} [req.connection]
	 * @param {string} [req.connection.remoteAddress]
	 * @return {string}
	 */
	getIp: (req) => req.headers['x-forwarded-for'] || req.connection.remoteAddress,

	/**
	 * Generates JWT token.
	 * @param {object} user
	 * @return {string}
	 */
	generateJWT(user) {
		const payload = {
			userId: user.id,
			hotelId: user.hotelId
		}

		return jwt.sign(payload, JWTConfig.jwtSecret, {
			expiresIn: JWTConfig.expirationPeriod
		})
	},

	/**
	 * Converts utc datetime to a given timezone datetime
	 * @param {Date|String|Number} datetime
	 * @param {String} timezone
	 * @param {String} format timezone datetime format
	 * @return {String}
	 */
	utcToTimezone: (datetime = new Date(), timezone = 'UTC', format = 'YYYY-MM-DD HH:mm:ss') => datetime && moment.
		utc(datetime).
		tz(timezone).
		format(format),

	/**
	 * Converts timezone datetime to an utc
	 * @param {Date|String|Number} datetime
	 * @param {String} timezone
	 * @param {String} format timezone datetime format
	 * @return {String}
	 */
	timezoneToUtc: (datetime = new Date(), timezone = 'UTC', format = 'YYYY-MM-DD HH:mm:ss') => datetime && moment.
		tz(datetime, timezone).
		utc().
		format(format),

	/**
	 * Converts local datetime to a given timezone datetime
	 * @param {Date|String|Number} datetime
	 * @param {String} timezone
	 * @param {String} format timezone datetime format
	 * @return {String}
	 */
	localToTimezone: (datetime = new Date(), timezone = 'UTC', format = 'YYYY-MM-DD HH:mm:ss') => datetime &&
		moment(datetime).
			tz(timezone).
			format(format),

	/**
	 * Converts local datetime to utc
	 * @param {Date|String|Number} datetime
	 * @param {String} format datetime format
	 * @return {String}
	 */
	localToUtc: (datetime = new Date(), format = 'YYYY-MM-DD HH:mm:ss') => datetime && moment(datetime).
		utc().
		format(format)
}

export default _.merge(sharedUtils, utils)

