'use strict'

import express from 'express'
import postController from './postController'
import userController from './userController'
import tagController from './tagController'
import todoController from './todoController'
import departmentController from './departmentController'
import reportController from './reportController'
import pmController from './pmController'
import documentsController from './documentsController'
import inspectionController from './inspectionController'
import authRule from '../middleware/authRule'

const router = express.Router()

/**
 * @swagger
 * resourcePath: /api
 * description: HotelTap REST API
 * schemes:
 *   - http # for development only
 *   - https
 * consumes:
 *   - application/json
 * produces:
 *   - application/json
 */

/**
 * @swagger
 * definition:
 *   BoardPosts:
 *     properties:
 *       posts:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Posts'
 *       hasMore:
 *         type: boolean,
 *         description: indicates whether there are more posts are available
 *       nextPageToken:
 *         type: integer
 *         description: token used to access the next page of the posts, ommit to show the most recent posts
 *       hasError:
 *         type: boolean
 *         description: indicated whether an error has happend on the server. Server will return an empty array in case of an error
 *
 *   User:
 *     properties:
 *       id:
 *         type: int
 *       roleId:
 *         type: int
 *         enum:
 *           - 1
 *           - 2
 *           - 3
 *           - 4
 *       firstName:
 *         type: string
 *       middleName:
 *         type: string
 *       lastName:
 *         type: string
 *       imageUrl:
 *         type: string
 *
 *   Posts:
 *     properties:
 *       id:
 *         type: int
 *       type:
 *         type: string
 *         description: type of the post
 *         enum:
 *           - note
 *           - task
 *           - rl
 *           - chl
 *           - insp
 *
 *   NewPost:
 *     required:
 *       - id
 *     properties:
 *       id:
 *         type: integer
 *       inserted:
 *         type: dateTime
 *
 *   NewTag:
 *     required:
 *       - id
 *     properties:
 *       id:
 *         type: integer
 *       inserted:
 *         type: dateTime
 *
 *   UnreadCounts:
 *     properties:
 *       counts:
 *         type: array
 *         items: object
 *
 *   Tag:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *       type:
 *         type: string
 *         enum:
 *           - dept
 *           - area
 *           - mtype
 *           - eqiup
 *       typeId:
 *         type: integer
 *
 *   Area:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *
 *   MaintenanceType:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *
 *   EquipmentGroup:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *
 *   Equipment:
 *     properties:
 *       id:
 *         type: integer
 *       groupId:
 *         type: integer
 *       name:
 *         type: string
 *
 *   Department:
 *     properties:
 *       id:
 *         type: integer
 *       hotelId:
 *         type: integer
 *       name:
 *         type: string
 *       imageUrl:
 *         type: string
 *       iconUrl:
 *         type: string
 *       iconFullUrl:
 *         type: string
 *   PM:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *       description:
 *         type: string
 *       assignmentType:
 *         type: string
 *       assignmentId:
 *         type: int
 *
 *   Document:
 *     properties:
 *       id:
 *         type: integer
 *       name:
 *         type: string
 *       url:
 *         type: string
 *
 *   Error:
 *     properties:
 *       code:
 *         type: integer
 *       message:
 *         type: string
 *       reason:
 *         type: integer
 */
// User **********************************************************
/**
 * @swagger
 * /users:
 *   user:
 *     tags: Users
 *     description: Return all user for a specific hotel.
 *     operationId: getAll
 *     responses:
 *       - 200:
 *         description: hotel users
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/User'
 *       - default:
 *         description: unexpected error
 *         schema:
 *           $ref: '#/definitions/Error'
 */
router.route('/users').get(userController.getAll)

router.route('/users/me').get(userController.getMe)
router.route('/users/me').put(userController.update)
router.route('/users/me/nav').get(userController.getNavigation)
router.route('/users/me/hotels').get(userController.getHotels)
router.route('/users/me/departments').get(userController.getDepartments)
router.route('/user/open-hotel').post(userController.openHotel)

/**
 * @swagger
 * /tags:
 *   get:
 *     tags: Tags
 *     description: Creates a tag.
 *     operationId: insert
 *     parameters:
 *       - name: name
 *         description: tag name
 *         in: body
 *         required: true
 *         type: string
 *       - name: type
 *         description: tag group type
 *         in: body
 *         required: true
 *         type: string
 *         enum:
 *           - dept
 *           - area
 *           - mtype
 *           - eqiup
 *       - name: typeId
 *         description: tag group id
 *         in: body
 *         required: true
 *         type: integer
 *     responses:
 *       - 200:
 *         description: tag inserted
 *         schema:
 *           $ref: '#/definitions/NewTag'
 *       - default:
 *         description: unexpected error
 *         schema:
 *           $ref: '#/definitions/Error'
 */
router.route('/tags').post(tagController.insert)

/**
 * @swagger
 * /tags:
 *   post:
 *     tags: Tags
 *     description: Gets all tags for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of tags
 *        schema:
 *          type: object
 *          properties:
 *            tags:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Tag'
 */
router.route('/tags').get(userController.getTags)

/**
 * @swagger
 * /areas:
 *   get:
 *     tags: Areas
 *     description: Gets all areas for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of areas
 *        schema:
 *          type: object
 *          properties:
 *            areas:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Area'
 */
router.route('/areas').get(userController.getAreas)

/**
 * @swagger
 * /maintenance:
 *   get:
 *     tags: Maintenance types
 *     description: Gets all maintenance types for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of maintenance types
 *        schema:
 *          type: object
 *          properties:
 *            mtypes:
 *              type: array
 *              items:
 *                $ref: '#/definitions/MaintenanceType'
 */
router.route('/maintenance').get(userController.getMaintenanceTypes)

/**
 * @swagger
 * /equipmentGroups:
 *   get:
 *     tags: Equipments
 *     description: Gets all equipment groups for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of equipment groups
 *        schema:
 *          type: object
 *          properties:
 *            groups:
 *              type: array
 *              items:
 *                $ref: '#/definitions/EquipmentGroup'
 */
router.route('/equipmentGroups').get(userController.getEquipmentGroups)

/**
 * @swagger
 * /equipments:
 *   get:
 *     tags: Equipments
 *     description: Gets all equipments for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of equipments
 *        schema:
 *          type: object
 *          properties:
 *            equipments:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Equipment'
 */
router.route('/equipments').get(userController.getEquipments)


// Posts *********************************************************
/**
 * @swagger
 * /posts/unread/count:
 *   get:
 *     description: Gets the number of the unread posts for a specific user by department and user id
 *     responses:
 *       200:
 *         description: list of departments and user with counts of new messages
 *         schema:
 *           $ref: '#/definitions/UnreadCounts'
 */
router.route('/posts/unread/count').get(postController.getUnreadCount)

/**
 * @swagger
 * /posts/complaints:
 *   get:
 *     tags: Posts
 *     description: Gets posts with guest complaints
 *     parameters:
 *       - name: pageToken
 *         description: token specifying which result page to return. Optional.
 *         in: query
 *         required: false
 *         type: int
 *     responses:
 *       200:
 *         description: list of posts
 *         schema:
 *           $ref: '#/definitions/BoardPosts'
 */
router.route('/posts/complaints').get(postController.getComplaints)

/**
 * @swagger
 * /posts/:id:
 *   get:
 *     tags: Posts
 *     description: gets a post by identifier
 *     parameters:
 *       - name: id
 *         description: post id
 *         in: path
 *         required: true
 *         type: int
 *     responses:
 *       200:
 *         description: post
 */
router.route('/posts/:id').get(postController.getPostById)

/**
 * @swagger
 * /posts:
 *   x-swagger-router-controller: postController
 *   post:
 *     tags: Posts
 *     description: Creates a post.
 *     operationId: insert
 *     parameters:
 *       - name: typeId
 *         description: type of the post
 *         in: body
 *         required: true
 *         type: string
 *         enum:
 *           - note
 *           - task
 *       - name: description
 *         description: post content
 *         in: body
 *         required: true
 *         type: string
 *       - name: private
 *         description: is this a private post or not
 *         in: body
 *         required: false
 *         type: boolean
 *       - name: guestComplaint
 *         description: indicator whether this is a guest complaint or not
 *         in: body
 *         required: false
 *         type: boolean
 *       - name: tags
 *         description: list of the tag ids
 *         in: body
 *         required: true
 *         type: array
 *         items:
 *           type: integer
 *       - name: ccList
 *         description: list of employee ids this post is cc-ed to.
 *         in: body
 *         required: false
 *         type: array
 *         items:
 *           type: integer
 *       - name: departments
 *         description: list of mentioned departments
 *         in: body
 *         required: true
 *         type: array
 *         items:
 *           type: integer
 *       - name: board
 *         description: the board the post is made from (for the web app)
 *         in: body:
 *         required: false
 *         type: string
 *     x-examples:
 *       application/json: { "typeId": "note", "description": "New note", "tags": [301, 505], "ccList": [99], "departments": [33, 17], "board": "departments/44"}
 *     responses:
 *       - 200:
 *         description: post inserted
 *         schema:
 *           $ref: '#/definitions/NewPost'
 *       - default:
 *         description: unexpected error
 *         schema:
 *           $ref: '#/definitions/Error'
 */
router.route('/posts').post(postController.insert)

/**
 * @swagger
 * /posts/department/{id}:
 *   get:
 *     tags: Posts
 *     description: Gets posts for a specific department
 *     parameters:
 *       - name: id
 *         description: Department id
 *         in: path
 *         required: true
 *         type: int
 *       - name: pageToken
 *         description: token specifying which result page to return. Optional.
 *         in: query
 *         required: false
 *         type: int
 *     responses:
 *       200:
 *         description: list of posts
 *         schema:
 *           $ref: '#/definitions/BoardPosts'
 */
router.route('/posts/departments/:id').get(postController.getByDepartmentId)

/**
 * @swagger
 * /posts/employee/{id}:
 *   get:
 *     tags: Posts
 *     description: Gets posts for a specific employee
 *     parameters:
 *       - name: id
 *         description: Employee id
 *         in: path
 *         required: true
 *         type: int
 *       - name: pageToken
 *         description: token specifying which result page to return. Optional.
 *         in: query
 *         required: false
 *         type: int
 *     responses:
 *       200:
 *         description: list of posts
 *         schema:
 *           $ref: '#/definitions/BoardPosts'
 */
router.route('/posts/employee/:id').get(postController.getByEmployeeId)

/**
 * @swagger
 * /posts/tags/{id}:
 *   get:
 *     tags: Posts
 *     description: Gets posts for a specific tag
 *     parameters:
 *       - name: id
 *         description: Tag id
 *         in: path
 *         required: true
 *         type: int
 *       - name: pageToken
 *         description: token specifying which result page to return. Optional.
 *         in: query
 *         required: false
 *         type: int
 *     responses:
 *       200:
 *         description: list of posts
 *         schema:
 *           $ref: '#/definitions/BoardPosts'
 */
router.route('/posts/tags/:id').get(postController.getByTagId)

/**
 * @swagger
 * /posts/{id}:
 *   put:
 *     tags: Posts
 *     description: Update a single post
 *     parameters:
 *       - name: id
 *         description: Post id
 *         in: path
 *         required: true
 *         type: int
 *       - name: readStatus
 *         description: Data that should be updated
 *         in: query
 *         required: false
 *         type: string
 *         enum:
 *           - r
 *           - u
 *     responses:
 *       - 200:
 *         description: updated post
 *       - 404:
 *         description: post is not find
 *
 */
router.route('/posts/:id').put(postController.update)

/**
 * @swagger
 * /posts/{id}/comments:
 *   post:
 *     tags: Posts
 *     description: Adds a comment to a post.
 *     parameters:
 *       - name: id
 *         description: Post id
 *         in: path
 *         required: true
 *         type: int
 *       - name: comment
 *         description: Comment text.
 *         in: query
 *         required: true
 *         type: string
 *     responses:
 *       - 200:
 *         description: comment added
 *       - 404:
 *         description: post was not found
 *
 */
router.route('/posts/:id/comments').post(postController.addComment)


// To do list ****************************************************
/**
 * @swagger
 * /todo/count:
 *   get:
 *     description: Gets the number of the todo items for a specific user by department and user id
 *     responses:
 *       200:
 *         description: list of departments and user with counts of todo items
 *         schema:
 *           $ref: '#/definitions/UnreadCounts'
 */
router.route('/todo/count').get(todoController.getCount)

/**
 * @swagger
 * /todo/departments/{id}:
 *   get:
 *     tags: ToDos
 *     description: fetches a collection of todo items by department id
 *     parameters:
 *       - name: id
 *         description: id of department
 *         in: path
 *         required: true
 *         type: int
 *       - name: sortType
 *         description: type of todo list sort
 *         required: false
 *         in: query
 *         type: string
 *     responses:
 *       - 200:
 *         description: collection of todo items
 */
router.route('/todo/departments/:id').get(todoController.getByDepartmentId)

/**
 * @swagger
 * /todo/employees/me:
 *   get:
 *     tags: ToDos
 *     description: fetches a collection of todo items of current user
 *     parameters:
         - name: sortType
 *         description: type of todo list sort
 *         required: false
 *         in: query
 *         type: string
 *     responses:
 *       - 200:
 *         description: collection of todo items
 */
router.route('/todo/employees/me').get(todoController.getOwn)

/**
 * @swagger
 * /todo/employees/{id}:
 *   get:
 *     tags: ToDos
 *     description: fetches a collection of todo items of specified user
 *     parameters:
 *       - name: id
 *         description: employee id
 *         in: path
 *         required: true
 *         type: int
 *       - name: sortType
 *         description: type of todo list sort
 *         required: false
 *         in: query
 *         type: string
 *     responses:
 *       - 200:
 *         description: collection of todo items
 */
router.route('/todo/employees/:id').get(todoController.getByEmployeeId)

/**
 * @swagger
 * /todo/employees:
 *   get:
 *     tags: ToDos
 *     description: fetches a collection of todo items per hotel
 *     parameters:
 *       - name: sortType
 *         description: type of todo list sort
 *         required: false
 *         in: query
 *         type: string
 *     responses:
 *       - 200:
 *         description: collection of todo items
 */
// TODO: resolve case of empty id for a "getByEmployeeId" method if remove "getForAllHotelEmployees"
router.route('/todo/employees').get(todoController.getForAllHotelEmployees)

/**
 * @swagger
 * /departments:
 *   get:
 *     tags: Departments
 *     description: Gets all departments for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of departments
 *        schema:
 *          type: object
 *          properties:
 *            departments:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Department'
 */
router.route('/departments').get(departmentController.getAll)

/**
 * @swagger
 * /reports:
 *   get:
 *     tags: Reports
 *     description: Gets all reports for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of reports
 *        schema:
 *          type: object
 *          properties:
 *            departments:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Report'
 */
router.route('/reports').get(reportController.getAll)

/**
 * @swagger
 * /reports/pm/{id}:
 *   get:
 *     tags: Reports
 *     description: Gets a preventive maintenance log by report id
 *     parameters:
 *       - name: id
 *         description: Report id
 *         in: path
 *         required: true
 *         type: int
 *     responses:
 *      - 200:
 *        description: the preventive maintenance report and related logs
 *        schema:
 *          properties:
 *            report:
 *              $ref: '#/definitions/Post'
 *            logs:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Post'
 */
router.route('/reports/pm/:id').get(reportController.getPreventiveMaintenanceLogsByReportId)

/**
 * @swagger
 * /reports/check-lists/{id}:
 *   get:
 *     tags: Reports
 *     description: Gets checklist log by report id
 *     parameters:
 *       - name: id
 *         description: Report id
 *         in: path
 *         required: true
 *         type: int
 *     responses:
 *      - 200:
 *        description: the check list report and related logs
 *        schema:
 *          properties:
 *            report:
 *              $ref: '#/definitions/Post'
 *            logs:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Post'
 */
router.route('/reports/check-lists/:id').get(reportController.getChecklistLogsByReportId)

//<editor-fold desc="PM">
/**
 * @swagger
 * /pm:
 *   get:
 *     tags: PM
 *     description: Gets all PM for a specific hotel.
 *     responses:
 *      - 200:
 *        description: list of PM
 *        schema:
 *          type: object
 *          properties:
 *            logs:
 *              type: array
 *              items:
 *                $ref: '#/definitions/PM'
 */
router.route('/pm').get(pmController.getAll)

//TODO: vvs p2 update swager definitions
/**
 * @swagger
 * /pm/{id}:
 *   get:
 *     tags: PM
 *     description: Gets PM by id.
 *     responses:
 *      - 200:
 *        description: PM
 *        schema:
 *          type: object
 */
router.route('/pm/:id').get(pmController.getById)

/**
 * @swagger
 * /pm/{id}:
 *   get:
 *     tags: PM
 *     description: Gets PM schedule for a year.
 *     responses:
 *      - 200:
 *        description: PMSchedule
 *        schema:
 *          type: object
 */
router.route('/pm/:id/schedules/:year').get(pmController.getSchedule)

//TODO: vvs p2 fix swagger
/**
 * @swagger
 * /pm/{id}:
 *   put:
 *     tags: PM
 *     description: Update a PM
 *     parameters:
 *       - name: id
 *         description: PM/Post id
 *         in: path
 *         required: true
 *         type: int
 *     responses:
 *       - 200:
 *
 */
router.route('/pm/:id').put(pmController.update)

//TODO: vvs p2 fix swagger
/**
 * @swagger
 * /pm:
 *   post:
 *     tags: PM
 *     description: Inserts a PM
 *     parameters:
 *     responses:
 *       - 200:
 *
 */
router.route('/pm').post(authRule.isAdmin, pmController.insert)

//</editor-fold>


/**
 * @swagger
 * /documents:
 *   get:
 *     tags: Documents
 *     description: Gets all Documents by hotelId.
 *     responses:
 *      - 200:
 *        description: list of documents
 *        schema:
 *          type: object
 *          properties:
 *            documents:
 *              type: array
 *              items:
 *                $ref: '#/definitions/Document'
 */
router.route('/documents').get(documentsController.getByHotelId)

/**
 * @swagger
 * /inspections:
 *   get:
 *     tags: Inspections
 *     description: Gets inspections by hotelId
 *     responses:
 *       200:
 *         description: list of inspections
 *         schema:
 *           type: object
 *           properties:
 *             inspections:
 *               type: array
 *               items:
 *                 $ref: '#/definitions/Inspection'
 */
router.route('/inspections').get(inspectionController.getAll)

/**
 * @swagger
 * /inspections/{id}:
 *   get:
 *     tags: Inspections
 *     description: Gets an inspection by id
 *     parameters:
 *       - name: id
 *         description: an inspection id
 *         in: path
 *         required: true
 *         type: int
 *     responses:
 *       200:
 *         description: single inspection
 *         schema:
 *           type: object
 *           $ref: '#/definitions/Inspection'
 */
router.route('/inspections/:id').get(inspectionController.getById)

/**
 * /inspections/{id}/schedules:
 *   get:
 *     tags: Inspections
 *     description: Gets a list of schedules associated with a given inspection template
 *     parameters:
 *       - name: id
 *         description: inspection template id
 *         in: path
 *         type: int
 *         required: true
 *     responses:
 *       - 200:
 *         description: list of inspection schedules
 *         schema:
 *           type: object
 *           properties:
 *             schedules:
 *               type: array
 *               items:
 *                 $ref: '#/definitions/InspectionSchedule'
 *
 */
router.route('/inspections/:id/schedules').get(inspectionController.getSchedules)

/**
 * @swagger
 * /inspections:
 *   post:
 *     tags: Inspections
 *     description: Inserts an inspection
 *     operationId: insert
 *     parameters:
 *       - name: name
 *         description: name of inspection
 *         in: body
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of inspection
 *         in: body
 *         required: true
 *         type: string
 *     responses:
 *       - 200:
 *         description: inserted
 */
router.route('/inspections').post(inspectionController.insert)

/**
 * @swagger
 * /inspections/{id}:
 *   put:
 *     tags: Inspections
 *     description: Update an Inspection
 *     parameters:
 *       - name: id
 *         description: Inspection/Post id
 *         in: path
 *         type: int
 *         required: true
 *     responses:
 *       - 200:
 *         description: updated inspection
 */
router.route('/inspections/:id').put(inspectionController.update)

export default router
