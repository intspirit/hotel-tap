'use strict'

import {authLogic} from '../business'
import utils from '../core/utils'
import logger from '../core/logger'
import config from '../config/environment'

/**
 * Authentication controller.
 */
export default {

	/**
	 * Logs user in.
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {function} res.status
	 * @param {object} res.body
	 * @param {string} [res.body.userType]
	 * @param {string} [res.body.hotelId]
	 * @param {string} [res.body.userName]
	 * @param {string} [res.body.password]
	 * @param {string} [res.body.remember]
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async login(req, res, next) {
		logger.info('api/authController|login', {
			userType: req.body.userType, hotelId: req.body.hotelId, userName: req.body.userName})
		try {
			const response = await authLogic.login({
				loginRequest: {
					userType: req.body.userType,
					hotelId: req.body.hotelId ? req.body.hotelId.trim().toLowerCase() : '',
					userName: req.body.userName.trim().toLowerCase(),
					ip: utils.getIp(req)
				},
				password: req.body.password,
				storedHotel: req.body.storedHotel
			})

			return res.json(response)
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Logs user in PHP app.
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {function} res.status
	 * @param {object} res.body
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async loginAdmin(req, res, next) {
		logger.info('api/authController|loginAdmin', {
			userName: req.user.userName, hotelId: req.user.hotelId})
		try {
			const accessToken = await authLogic.accessToken(req.user)
			const response = {
				accessToken: accessToken,
				adminUrl: config.adminApp.url
			}

			return res.json(response)
		} catch (e) {
			return next(e)
		}
	}
}
