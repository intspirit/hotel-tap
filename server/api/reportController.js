'use strict'

import logger from '../core/logger'
import {reportLogic} from '../business'

/**
 * Report controller.
 */
export default {

	/**
	 * Load all hotel reports
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} next
	 * @returns {*}
	 */
	async getAll(req, res, next) {
		logger.info('api/reportController|getAll')

		try {
			const reports = await reportLogic.getReports(req.hotelId)

			return res.json(reports)
		} catch (e) {
			return next(e)
		}
	},


	/**
	 * Load preventive maintenance logs by report id
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} next
	 * @returns {*}
	 */
	async getPreventiveMaintenanceLogsByReportId(req, res, next) {
		logger.info('api/reportController|getPreventiveMaintenanceLogsByReportId')

		try {
			const reportId = parseInt(req.params.id, 10)
			const data = await reportLogic.getPreventiveMaintenanceLogsByReportId(req.hotelId, reportId)

			return res.json(data)
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Load check list log by report id
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} next
	 * @returns {*}
	 */
	async getChecklistLogsByReportId(req, res, next) {
		logger.info('api/reportController|getChecklistLogsByReportId')

		try {
			const reportId = parseInt(req.params.id, 10)
			const data = await reportLogic.getChecklistLogsByReportId(req.hotelId, reportId)

			return res.json(data)
		} catch (e) {
			return next(e)
		}
	}
}
