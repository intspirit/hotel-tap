'use strict'

import logger from '../core/logger'
import {inspectionDb, postDb} from '../db'
import constants from '../../shared/constants'
import {inspectionLogic} from '../business'

export default {

	/**
	 * Gets all inspections
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getAll(req, res, next) {
		logger.log('info', 'inspectionController|geAll', {hotelId: req.hotelId, userId: req.user.id})

		try {
			const inspections = await postDb.getByType(req.hotelId, constants.postTypes.INSPECTION)

			return res.json({inspections})
		} catch (error) {
			logger.log('error', 'api/inspectionController|getAll', error)

			return next(error)
		}
	},

	/**
	 * Gets an inspection by id
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getById(req, res, next) {
		logger.log('info', 'api/inspectionController|getById',
			{hotelId: req.hotelId, userId: req.user.id, id: req.params.id})

		try {
			const inspection = await inspectionLogic.getById(req.hotelId, req.user.id, req.params.id)

			return res.json(inspection)
		} catch (error) {
			logger.log('error', 'api/inspectionController|getById', error)

			return next(error)
		}
	},

	/**
	 * Inserts an inspection
	 * @param {object} req
	 * @param {object} req.body
	 * @param {int} req.hotelId
	 * @param {object} res
	 * @param {Function} next
	 * @returns {json}
	 */
	async insert(req, res, next) {
		logger.log('info', 'api/inspectionController|insert', {hotelId: req.hotelId, inspection: req.body})

		try {
			const newId = await inspectionDb.insertInspection(req.hotelId, req.user.id, constants.postTypes.INSPECTION,
																req.body.name, req.body.description)

			return res.json({
				id: newId,
				inserted: new Date()
			})
		} catch (error) {
			logger.log('error', 'api/inspectionController|insert', error)

			return next(error)
		}
	},

	/**
	 * Updates an inspection
	 * @param {object} req
	 * @param {object} req.body
	 * @param {object} req.params
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {object} res
	 * @param {Function} next
	 * @returns {json}
	 */
	async update(req, res, next) {
		logger.log('info', 'api/inspectionController|update',
			{hotelId: req.hotelId, userId: req.userId, inspectionId: req.params.id, inspection: req.body})

		try {
			const updatedRowsNumber = await inspectionLogic.update(req.hotelId, req.userId, req.params.id, req.body)

			if (updatedRowsNumber === 0) {
				// TODO: alytyuk p1 return error
			}

			return res.json({
				updated: new Date()
			})
		} catch (error) {
			logger.log('error', 'api/inspectionController|update', error)

			return next(error)
		}
	},

	/**
	 * Gets all inspection schedules associated with a given inspection template
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} next
	 * @returns {*}
	 */
	async getSchedules(req, res, next) {
		logger.log('info', 'api/inspectionController|getSchedules',
			{hotelId: req.hotelId, userId: req.user.id, inspectionId: req.params.id})

		try {
			const schedules = await inspectionLogic.getSchedules(req.hotelId, req.params.id)

			return res.json({schedules})
		} catch (error) {
			logger.log('error', 'api/inspectionController|getSchedules', error)

			return next(error)
		}
	}
}
