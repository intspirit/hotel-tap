'use strict'

import {pmLogic} from '../business'
import {postDb} from '../db'
import constants from '../../shared/constants'
import logger from '../core/logger'
import postPMDb from '../db/postPMDb'

/**
 * PM controller.
 */
export default {

	/**
	 * Gets all PM.
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getAll(req, res, next) {
		logger.info('api/pmController|getAll:', {hotelId: req.hotelId})

		try {
			const logs = await postDb.getByType(req.hotelId, constants.postTypes.PM)

			return res.json({pmList: logs})
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Gets PM by id.
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getById(req, res, next) {
		logger.info('api/pmController|getById:', {hotelId: req.hotelId, userId: req.user.id, id: req.params.id})

		try {
			const result = await pmLogic.getById(req.hotelId, req.user.id, req.params.id)

			return res.json(result)
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Gets PM schedule for a specified year.
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getSchedule(req, res, next) {
		logger.info('api/pmController|getSchedule:',
			{hotelId: req.hotelId, userId: req.user.id, id: req.params.id, year: req.params.year})

		try {
			//TODO: vvs p2 year validation
			const year = parseInt(req.params.year, 10)
			const dateFrom = `${year}-01-01`
			const dateTo = `${year + 1}-01-01`
			const result = await postDb.getByTypeAndParentAndDateRange(
				req.hotelId, constants.postTypes.PM_SCHEDULE, req.params.id, dateFrom, dateTo)

			return res.json(result)
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Update a PM.
	 * @param {Object} req
	 * @param {Object} req.body
	 * @param {Object} req.params
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {Object} res
	 * @param {Function} next
	 * @returns {json}
	 */
	update: async (req, res, next) => {
		logger.info('api/pmController|update:',
			{hotelId: req.hotelId, userId: req.userId, pmId: req.params.id, pm: req.body})

		try {
			const updatesCount = await pmLogic.update(req.hotelId, req.user, req.params.id, req.body)

			return res.json({
				inserted: new Date(),
				updateCount: updatesCount
			})
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Inserts a PM.
	 * @param {Object} req
	 * @param {Object} req.body
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {Object} res
	 * @param {Function} next
	 * @returns {json}
	 */
	insert: async (req, res, next) => {
		logger.info('api/pmController|insert:',
			{hotelId: req.hotelId, pm: req.body})

		//TODO: vvs p1 authorization (middleware)
		try {
			const newId = await postPMDb.insertPM(
				req.hotelId, req.userId, constants.postTypes.PM, req.body)

			return res.json({
				inserted: new Date(),
				newId: newId
			})
		} catch (e) {
			return next(e)
		}
	}
}
