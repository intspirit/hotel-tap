'use strict'

import {userLogic} from '../business'
import {getResponseError, BadRequestError, ServerError} from '../errors'
import {userDb} from '../db'
import logger from '../core/logger'
import _ from 'lodash'

/**
 * User controller.
 */
export default {
	getMe: async (req, res) => {
		const user = await userLogic.getMe(req.user)
		res.json({user: user})
	},

	getNavigation: async (req, res) => {
		const nav = await userLogic.getNavigation(req.hotelId, req.user)
		res.json({options: nav})
	},

	getHotels: async (req, res) => {
		const details = req.query.details
		const hotels = details === 'all' ?
			await userLogic.getHotels(req.user) :
			await userLogic.getHotelsFewDetails(req.user)
		res.json({hotels: hotels})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	getDepartments: async (req, res) => {
		const departments = await userLogic.getDepartments(req.hotelId, req.user)
		res.json({departments: departments})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Load hotel tags
	 *
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getTags: async (req, res) => { // eslint-disable-line
		let tags = await userLogic.getTags(req.hotelId)
		tags = _.sortBy(tags, 'roomNumber')
		res.json({tags: tags})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Load hotel areas
	 *
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getAreas: async (req, res) => { // eslint-disable-line
		const areas = await userLogic.getAreas(req.hotelId)
		res.json({areas: areas})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Load hotel areas
	 *
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getMaintenanceTypes: async (req, res) => { // eslint-disable-line
		const mtypes = await userLogic.getMaintenanceTypes(req.hotelId)
		res.json({mtypes: mtypes})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Load hotel equipment groups
	 *
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getEquipmentGroups: async (req, res) => { // eslint-disable-line
		const equipmentGroups = await userLogic.getEquipmentGroups(req.hotelId)
		res.json({groups: equipmentGroups})
	},

	//TODO: vvs p2 REFACTOR wrong place for this code
	/**
	 * Load hotel equipments
	 *
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getEquipments: async (req, res) => { // eslint-disable-line
		const equipments = await userLogic.getEquipments(req.hotelId)
		res.json({equipments: equipments})
	},

	//TODO: vvs p2 REFACTOR better name
	/**
	 * Logs user into hotel.
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {function} res.status
	 * @param {object} res.body
	 * @param {object} [req.user]
	 * @param {integer} [req.body.hotelId]
	 * @param {object} [req.jwtDecoded]
	 * @returns {*}
	 */
	async openHotel (req, res) {
		try {
			const response = await userLogic.openHotel(req.user, req.body.hotelId, req.jwtDecoded)

			return res.json(response)
		} catch (err) {
			const responseError = getResponseError(err)
			logger.log('error', err)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Gets all hotel users.
	 * @param {Object} req
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {Object} res
	 * @param {Function} next
	 * @returns {*}
	 */
	getAll: async (req, res, next) => {
		logger.info('api/userController|getAll', {hotelId: req.hotelId, userId: req.userId})
		try {
			return res.json(await userDb.getByHotelId(req.hotelId))
		} catch (e) {
			return next(e)
		}
	},

	/**
	 * Updates user profile info.
	 *
	 * @param {object} req
	 * @param {int} req.userId
	 * @param {object} req.user
	 * @param {object} req.body
	 * @param {string} req.body.firstName
	 * @param {string|null} req.body.lastName
	 * @param {string|null} req.body.imageFileName
	 * @param {string} req.body.password
	 * @param {boolean} req.body.notificationStatus
	 * @param {object} res
	 * @param {function} next
	 * @returns {json}
	 */
	update: async (req, res) => {
		logger.info('api|userController|update', {userId: req.userId, profileInfo: req.body})
		try {
			const response = await userLogic.update(req.userId, req.body, req.user)

			return res.json(response)
		} catch (error) {
			logger.error('api|userController|error', {error})

			// TODO: alytyuk p2 temp solution, update method should be refactored, including business layer part
			let responseError = null
			if (error instanceof BadRequestError) {
				responseError = getResponseError(error)
			} else {
				responseError = getResponseError(new ServerError(error ? error.message : 'Unknown error'))
			}

			return res.status(responseError.error.code).json(responseError)
		}
	}
}
