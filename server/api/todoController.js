'use strict'

import {todoLogic} from '../business'
import constants from '../../shared/constants'

/**
 * To Do list controller.
 * @type {{getByDepartmentId: (function()), getEmployees: (function()), getByEmployeeId: (function())}}
 */
export default {

	/**
	 * Gets todo items counts grouped by assignment
	 * @param {Object} req
	 * @param {Object} res
	 * @returns {void}
	 */
	getCount: async (req, res) => {
		const result = await todoLogic.getCount(req.hotelId)
		const counts = result.map(x => ({
			count: x.count,
			id: x.assignment_id,
			type: x.assignment_type_id === constants.assignmentTypes.DEPARTMENT ?
					constants.navigationOptionType.DEPARTMENT :
					constants.navigationOptionType.EMPLOYEE
		}))
		res.json({counts})
	},

	/**
	 * Gets todo items for a hotel department.
	 * @param {Object} req
	 * @param {Object} res
	 * @returns {void}
	 */
	getByDepartmentId: async (req, res) =>
		res.json(await todoLogic.getByDepartmentId(req.hotelId, req.params.id, req.user || {id: req.userId},
													req.query.sortType)),

	/**
	 * Gets todo items for all hotel employees.
	 * @param {Object} req
	 * @param {Object} res
	 * @returns {void}
	 */
	getForAllHotelEmployees: async (req, res) =>
		res.json(await todoLogic.getForAllHotelEmployees(req.hotelId, req.user || {id: req.userId},
															req.query.sortType)),

	/**
	 * Gets todo items for current user in a hotel.
	 * @param {Object} req
	 * @param {Object} res
	 * @returns {void}
	 */
	getOwn: async (req, res) =>
		res.json(await todoLogic.getByEmployeeId(req.hotelId, req.userId, req.user || {id: req.userId},
													req.query.sortType)),

	/**
	 * Gets todo items for a giver user in a hotel.
	 * @param {Object} req
	 * @param {Object} res
	 * @returns {void}
	 */
	getByEmployeeId: async (req, res) =>
		res.json(await todoLogic.getByEmployeeId(req.hotelId, req.params.id, req.user || {id: req.userId},
													req.query.sortType))
}
