'use strict'

import documentsDb from '../db/documentsDb'
import logger from '../core/logger'

/**
 * Documents controller.
 */
export default {

	/**
	 * Gets documents by hotel ID.
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getByHotelId(req, res, next) {
		logger.info('api/documentsController|getByHotelId', {hotelId: req.hotelId})

		try {
			const documents = await documentsDb.getByHotelId(req.hotelId)

			return res.json({documents})
		} catch (e) {
			return next(e)
		}
	}
}
