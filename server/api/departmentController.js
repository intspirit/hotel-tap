'use strict'

import {userLogic} from '../business'
import logger from '../core/logger'

/**
 * Department controller.
 */
export default {

	/**
	 * Load all hotel departments
	 *
	 * @param {object} req
	 * @param {object} res
	 * @param {function} res.json
	 * @param {Function} [next]
	 * @returns {*}
	 */
	async getAll(req, res, next) {
		logger.info('api/departmentController|getAll')

		try {
			const departments = await userLogic.getDepartments(req.hotelId, {isAdmin: true})

			return res.json({departments: departments})
		} catch (e) {
			return next(e)
		}
	}
}
