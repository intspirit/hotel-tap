'use strict'

import _ from 'lodash'
import {postReadStatusDb} from '../db'
import {postReadStatus, hiddenUserId, postTypes} from '../../shared/constants'
import logger from '../core/logger'
import postLogic from '../business/postLogic'
import postUpdateRequestValidator from '../business/validator/postUpdateRequestValidator'
import {BadRequestError, ServerError, getResponseError} from '../errors'

//TODO: vvs p2 REFACTOR crap, remove
// TODO alytyuk p3 may be replace req.user = user with req.user = user || {id: <id_here>} in middleware auth
// TODO (continue) or better throw in middleware auth if there is no user in db
const userFromRequest = request => request.user || {id: request.userId}

/**
 * Post controller.
 */
const postController = {

	/**
	 * Gets number of unread posts for employee per departments.
	 * @param {object} req
	 * @param {object} res
	 * @returns {json}
	 */
	getUnreadCount: async(req, res) => { // eslint-disable-line
		try {
			let [departmentCounts, userCounts] = await Promise.all([
				postReadStatusDb.getDepartmentCountsByUserId(req.hotelId, req.userId, postReadStatus.UNREAD),
				postReadStatusDb.getUserCountsByUserId(req.hotelId, req.userId, postReadStatus.UNREAD)])

			if (userCounts.length > 0) userCounts[0].id = hiddenUserId

			const result = departmentCounts.concat(userCounts)

			return res.json({counts: result})
		} catch (error) {
			logger.log('error', 'postController|getUnreadCount|error:%o', error)

			return res.json({counts: []})
		}
	},

	/**
	 * Gets posts for a department.
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @param {Object} req.query
	 * @returns {json}
	 */
	getByDepartmentId: async(req, res) =>
		res.json(await postLogic.getByDepartmentId(req.hotelId, req.params.id, userFromRequest(req),
													req.query.nextPageToken)),


	/**
	 * Gets posts for a employee.
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @param {Object} req.query
	 * @returns {json}
	 */
	getByEmployeeId: async(req, res) =>
		res.json(await postLogic.getByEmployeeId(req.hotelId, parseInt(req.params.id, 10), userFromRequest(req),
													req.query.nextPageToken)),

	/**
	 * Gets posts for a tag.
	 *
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @param {Object} req.query
	 * @returns {json}
	 */
	getByTagId: async(req, res) =>
		res.json(await postLogic.getByTagId(req.hotelId, parseInt(req.params.id, 10), userFromRequest(req),
												req.query.nextPageToken)),

	/**
	 * Gets posts with guest complaints by employee id.
	 *
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @param {Object} req.query
	 * @returns {json}
	 */
	getComplaints: async(req, res) =>
		res.json(await postLogic.getComplaints(req.hotelId, userFromRequest(req), req.query.nextPageToken)),

	/**
	 * Update post item.
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @param {Object} req.body
	 * @param {null|string} req.body.typeId
	 * @param {null|string} req.body.completionStatus
	 * @param {null|string} req.body.dueDateTime
	 * @param {null|string} req.body.readStatus
	 * @param {null|string} req.body.flag
	 * @returns {json}
	 */
	update: async (req, res) => {
		try {
			const updateRequest = req.body

			// TODO: ak p3 is updateRequest.action is better for cases when we have multiple fields updated
			// Example: updateRequest && updateRequest.action === postUpdateAction.SOME_ACTION

			// TODO: ak p3 add validation
			if (updateRequest && updateRequest.typeId && updateRequest.typeId === postTypes.TASK)
				return await postController.convertNoteToTask(req, res)

			if (updateRequest && updateRequest.completionStatus)
				return await postController.updateCompletionStatus(req, res)

			// TODO: ak p3 add validation
			if (updateRequest && typeof updateRequest.dueDateTime === 'string')
				return await postController.updateDueDateTime(req, res)

			//TODO: vvs p3 refactor this code - move most to postLogic
			if (updateRequest && updateRequest.readStatus) {
				if (!postUpdateRequestValidator.validReadStatus(updateRequest))
					return res.status(400).json('Invalid request')

				try {
					await postReadStatusDb.update(req.hotelId, req.userId, req.params.id, updateRequest.readStatus)

					return res.json({updated: new Date(), readStatus: updateRequest.readStatus})
				} catch (err) {
					return res.status(500).json({error: 'Could not update read status'})
				}
			}

			if (updateRequest && updateRequest.hasOwnProperty('flag')) {
				if (!postUpdateRequestValidator.validFlagStatus(updateRequest))
					return res.status(400).json('Invalid request')

				return await postController.updateFlag(req, res)
			}

			if (updateRequest && updateRequest.hasOwnProperty('complaintResolution')) {
				return await postController.resolveGuestComplaint(req, res)
			}

			return res.status(400).json('Invalid request')
		} catch (error) {
			logger.log('error', 'postController|update|error:%o', error)

			return res.json({error})
		}
	},

	/**
	 * Updates task completion status.
	 * @param {object} req
	 * @param {object} res
	 * @returns {*|json}
	 */
	async updateCompletionStatus(req, res) {
		try {
			// TODO alytyuk p3 refactor code below, updateResult is temporary solution
			const updateResult = await postLogic.setCompletion(
				req.hotelId, req.userId, req.params.id, req.body.completionStatus)
			if (updateResult.updatedRowsNumber === 0) {
				const responseError = getResponseError(
					new BadRequestError('Failed to update task status', {
						hotelId: req.hotelId,
						userId: req.userId,
						postId: req.params.id,
						status: req.body.completionStatus
					}))

				return res.status(responseError.error.code).json(responseError)
			}

			// TODO alytyuk p3 refactor code below, updated should be sent in hotel timezone
			return res.json({
				updated: new Date(),
				completionDateTime: updateResult.completionDateTime,
				postId: req.params.id,
				status: req.body.completionStatus
			})
		} catch (error) {
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Updates task due datetime.
	 *
	 * @param {object} req
	 * @param {object} req.body
	 * @param {string} req.body.dueDateTime
	 * @param {object} req.params
	 * @param {object} res
	 * @returns {*|json}
	 */
	async updateDueDateTime(req, res) {
		try {
			// TODO ak p3 refactor code below, updateResult is temporary solution
			const updateResult = await postLogic.setDueDateTime(
				req.hotelId, req.userId, req.params.id, req.body.dueDateTime)

			if (updateResult.updatedRowsNumber === 0) {
				const responseError = getResponseError(
					new BadRequestError('Failed to update task due date time', {
						hotelId: req.hotelId,
						userId: req.userId,
						postId: req.params.id,
						dueDateTime: req.body.dueDateTime
					}))

				return res.status(responseError.error.code).json(responseError)
			}

			return res.json({
				updated: updateResult.updatedDate,
				postId: req.params.id,
				dueDateTime: req.body.dueDateTime
			})

		} catch (error) {
			logger.log('error', 'postController|updateDueDateTime|error:%o', error)
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Updates a post flag
	 * @param {object} req
	 * @param {object} res
	 * @returns {*|json}
	 */
	async updateFlag(req, res) {
		try {
			const isFlagUpdated = await postLogic.updateFlag(req.hotelId, req.userId, req.params.id, req.body)

			if (!isFlagUpdated) {
				const responseError = getResponseError(
					new BadRequestError('Failed to update post flag', {
						hotelId: req.hotelId,
						userId: req.userId,
						postId: req.params.id,
						flag: {
							set: req.body.flag,
							expirationDateTime: req.body.flagDue
						}
					}))

				return res.status(responseError.error.code).json(responseError)
			}

			return res.json({
				updated: new Date(),
				flag: {
					set: req.body.flag,
					expirationDateTime: req.body.flagDue
				}
			})
		} catch (error) {
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Updates a post flag
	 * @param {object} req
	 * @param {object} res
	 * @returns {*|json}
	 */
	async resolveGuestComplaint(req, res) {
		try {
			const complaintResolution = req.body.complaintResolution
			const postId = req.params.id

			if (!postId || !postUpdateRequestValidator.validComplaintResolution(complaintResolution)) {
				const validationError = getResponseError(new BadRequestError('Validation failed', {complaintResolution}))

				return res.status(validationError.error.code).json(validationError)
			}

			const isResolutionUpdated = await postLogic.updateComplaintResolution(req.hotelId, postId, req.user,
																					complaintResolution)
			if (!isResolutionUpdated) {
				const resolutionUpdateError = getResponseError(
					new BadRequestError('Failed to update post complaint resolution', {
						hotelId: req.hotelId,
						postId: req.params.id,
						complaintResolution: complaintResolution
					}))

				return res.status(resolutionUpdateError.error.code).json(resolutionUpdateError)
			}

			return res.json({
				updated: new Date(),
				complaintResolution: complaintResolution
			})
		} catch (error) {
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Converts note to task
	 *
	 * @param {object} req
	 * @param {object} req.body
	 * @param {string} req.body.typeId
	 * @param {string|null} req.body.description
	 * @param {string|null} req.body.subject
	 * @param {string|null} req.body.assignmentType
	 * @param {int|null} req.body.assignmentId
	 * @param {string|null} req.body.dueDateTime
	 * @param {object} req.params
	 * @param {object} res
	 * @returns {*|json}
	 */
	async convertNoteToTask(req, res) {
		try {
			// TODO ak p3 refactor code below, updateResult is temporary solution
			const updateResult = await postLogic.convertNoteToTask(req.hotelId, req.userId, req.params.id, req.body)
			if (updateResult.updatedRowsNumber === 0) {
				const responseError = getResponseError(
					new BadRequestError('Failed to convert note to task', _.merge({
						hotelId: req.hotelId,
						userId: req.userId,
						postId: req.params.id
					}, req.body)))

				return res.status(responseError.error.code).json(responseError)
			}

			return res.json(_.merge({
				updated: updateResult.updatedDate,
				postId: req.params.id
			}, req.body))
		} catch (error) {
			const serverError = new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	/**
	 * Adds comment to a post.
	 * @param {object} req
	 * @param {object} req.body
	 * @param {object} req.params
	 * @param {object} res
	 * @return {*|json}
	 */
	addComment: async (req, res) => {
		try {
			const newId = await postLogic.insertComment(req.hotelId, req.userId, req.params.id, req.body)

			return res.json({
				inserted: new Date(),
				id: newId || -1
			})
		} catch (error) {
			//TODO: vvs p2 REFACTOR fix error handling per https://github.com/sandipjariwala/hoteltap-wiki-eng/wiki/Development-guidelines#handle-route-in-the-controller
			const serverError = error instanceof BadRequestError ?
				error :
				new ServerError(error ? error.message : 'Unknown error')
			const responseError = getResponseError(serverError)

			return res.status(responseError.error.code).json(responseError)
		}
	},

	//TODO" vvs p2 REFACTOR bad name -> getById
	/**
	 * Gets post by id
	 * @param {Object} req
	 * @param {Object} res
	 * @param {Object} req.params
	 * @returns {json}
	 */
	getPostById: async (req, res) => res.json(await postLogic.getById(req.hotelId, userFromRequest(req), req.params.id)),

	/**
	 * Inserts a post.
	 * @param {Object} req
	 * @param {Object} req.body
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {Object} res
	 * @param {Function} next
	 * @returns {json}
	 */
	insert: async (req, res, next) => {
		logger.info('api/postController|insert', {hotelId: req.hotelId, userId: req.userId, post: req.body.post})
		try {
			const newId = await postLogic.insert(req.hotelId, req.userId, req.body.post, req.body.attachments)

			return res.json({
				inserted: new Date(),
				id: newId || -1
			})
		} catch (e) {
			return next(e)
		}
	}
}

export default postController
