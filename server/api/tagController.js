'use strict'

import logger from '../core/logger'
import {tagLogic} from '../business'

/**
 * Tag controller.
 */
export default {

	/**
	 * Inserts a tag.
	 *
	 * @param {Object} req
	 * @param {Object} req.body
	 * @param {int} req.hotelId
	 * @param {int} req.userId
	 * @param {Object} res
	 * @param {Function} next
	 * @returns {*}
	 */
	async insert(req, res, next) {
		logger.info('api/tagController|insert')

		try {
			const insertResult = await tagLogic.insert(req.hotelId, req.userId, req.body)

			return res.json({
				inserted: insertResult.insertedDate,
				id: insertResult.newId || -1
			})
		} catch (e) {
			return next(e)
		}
	}
}
