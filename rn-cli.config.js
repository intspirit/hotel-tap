'use strict'

let blacklist = require('react-native/packager/blacklist')

const config = {
	getBlacklistRE(platform) {
		return blacklist(platform, [
			/HotelTap.Web\/node_modules\/react-avatar\/node_modules\/react\/.*/
		])
	}
}

module.exports = config
