'use strict'

import authService from '../../shared/services/authService'
import appController from '../../shared/core/appController'
import localDb from './localDb'

/**
 * Authentication controller
 */
export default {

	async login(credentials) {
		const response = await authService.login(credentials)
		if (!response.token || !response.user) throw Error('Something went wrong. Please try again.')


		appController.jwtToken = response.token
		appController.authenticated = true
		await localDb.setJwt(response.token)
		await localDb.setUser(response.user)

		return response
	},

	logout() {
		appController.jwtToken = null
		localDb.deleteJwt()
		localDb.deleteUser()
		appController.removeUser()
	}
}
