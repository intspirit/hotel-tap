'use strict'

import _ from 'lodash'
import sharedAppController from '../../shared/core/appController'
import constants from '../../shared/constants'


/**
 * Application controller - controls application state and interaction among various components.
 */
const appController = {
	platform: 'mobile',
	navigator: null,
	user: {
		id: 485,
		firstName: 'Mike',
		lastName: 'Gerald',
		fullName: 'Mike G Gerald',
		admin: true,
		imageUrl: 'https://s3-us-west-2.amazonaws.com/htap-files-test/img/profile/56e180e580977.jpg'
	},
	hotel: {
		name: 'San Carlos Holiday Inn'
	},
	boardComponent: null,
	listComponent: null,
	commentsComponent: null,
	postEntryComponent: null,

	addComment(postId, commentId, comment) {
		//TODO: vvs p2 move this code (newComment) into a proper place.
		const newComment = {
			id: commentId,
			typeId: constants.postTypes.COMMENT,
			//hotelId = hotel_id
			descriptionText: [comment],
			languageId: 'en',
			createdDateTime: new Date(),
			parentId: postId,
			orderDateTimeUtc: new Date(),
			createdBy: {
				id: -1,
				fullName: sharedAppController.user.fullName,
				imageUrl: sharedAppController.user.imageUrl
			},
			attachments: []
		}

		if (this.boardComponent && typeof this.boardComponent.addComment === 'function') {
			this.boardComponent.addComment(postId, newComment)
		}
		if (this.commentsComponent && typeof this.commentsComponent.addComment === 'function') {
			this.commentsComponent.addComment(newComment)
		}
	},

	onTodoComplete(postId, assignmentType, assignmentId) {
		if (this.listComponent && typeof this.listComponent.onTodoComplete === 'function') {
			this.listComponent.onTodoComplete(postId, assignmentType, assignmentId)
		}
		if (this.boardComponent && typeof this.boardComponent.onTodoComplete === 'function') {
			this.boardComponent.onTodoComplete(postId, assignmentType, assignmentId)
		}
	},

	validateNewPost() {
		return this.postEntryComponent.validate()
	}
}

if (!appController.user) {
	_.merge(appController, sharedAppController)
}

export default appController

