'use strict'

import {AsyncStorage} from 'react-native'

const localDb = {
	jwtKey: 'jwtToken',
	userKey: 'user',
	hotelKey: 'hotel',

	getKey(key) {
		return `@HotelTap:${key}`
	},

	async set(key, value) {
		try {
			await AsyncStorage.setItem(this.getKey(key), value)
		} catch (err) {
			//TODO: vvs p2 handle exception
		}
	},

	async remove(key) {
		try {
			await AsyncStorage.removeItem(this.getKey(key))
		} catch (err) {
			//TODO: vvs p2 handle exception
		}
	},

	get(key) {
		try {
			return AsyncStorage.getItem(this.getKey(key))
		} catch (err) {
			//TODO: vvs p2 handle exception
			return null
		}
	},

	async setUser(user) {
		await this.set(this.userKey, JSON.stringify(user))
	},

	async getUser() {
		const user = await this.get(this.userKey)

		return JSON.parse(user) || {}
	},

	async deleteUser() {
		await this.remove(this.userKey)
	},

	async setJwt(jwt) {
		await this.set(this.jwtKey, jwt)
	},

	getJwt() {
		return this.get(this.jwtKey)
	},

	async deleteJwt() {
		await this.remove(this.jwtKey)
	},

	async setHotel(hotel) {
		await this.set(this.hotelKey, JSON.stringify(hotel))
	},

	async getHotel() {
		const hotel = await this.get(this.hotelKey)

		return JSON.parse(hotel)
	},

	async deleteHotel() {
		await this.remove(this.hotelKey)
	}
}

export default localDb
