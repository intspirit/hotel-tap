'use strict'

import _ from 'lodash'

const defaults = {
	BASE_URL: 'https://test.hoteltap.com'
}

let configuration = defaults
// eslint-disable-next-line no-undef
if (__DEV__) {
	configuration = _.merge(configuration, require(`./globals.local.js`) || {})
}

export default configuration
