'use strict'

import appController from './appController'
import authController from './authController'
import globals from './globals'
import localDb from './localDb'

export default module.exports = {
	appController: appController,
	authController: authController,
	globals: globals,
	localDb: localDb
}
