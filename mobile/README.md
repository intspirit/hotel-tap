# Mobile native version of the HotelTap

## Development

### Run application

In order to run the app, first of all make sure [that you have everything needed for the React Native development](https://facebook.github.io/react-native/docs/getting-started.html#content).

Then:

1. Get the latest code from Git.
2. Run `npm i`.
3. In one terminal window run `npm run start:mobile`.
3. In another terminal window run `npm run start:ios`.

#### Local config changes

To apply your custom changes to configs, please create a local config file

`cp mobile/core/globals.local.js.example mobile/core/globals.local.js`

Edit your mobile/core/globals.local.js


#### Access backend services locally

In order to access the backend services locally, you can use any tunneling utilities, like [ngrok](https://ngrok.com):

    /Applications/ngrok http -subdomain=your_subdomain 9006

Set the URL for your tunnelling site in the `/mobile/core/globals.local.js` - `BASE_URL`.

### Deploying

#### Appetize.io

**Preparing (one-time)**

Open project in XCode and make sure that File->Project Settings...->DerivedData is set to `Project-relative Location`

**Deployment**

1. Run `npm run bundle:mobile`.
2. Then run `npm run build:mobile`.
3. Then run `npm run deploy:mobile`.

Or run `npm run publish:mobile`.

### Debugging
[Intro to debugging React Native](http://blog.differential.com/intro-to-debugging-react-native-ios-and-android/)


### Temp dev settings

When developing the mobile, use the following tricks fro now:

2. Make sure that `build` folder exists, but empty.
3. Adjust `BASE_URL` in the `mobile/core/globals.local.js` to the server (local or test) that you use.
