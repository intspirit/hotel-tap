'use strict'

import React from 'react'
import {View, Text, StyleSheet, SwitchIOS, TextInput, ScrollView, AlertIOS} from 'react-native'
import KeyboardSpacer from 'react-native-keyboard-spacer'
import TagSelector from '../tags/TagSelector'
import {PostTags, PostUsers} from '../core'
import UserSelector from '../users/UserSelector'
import appController from '../../core/appController'
import PostEntryComponent from '../../../shared/components/postEntry'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 0
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	tagsTextContainer: {
		flex: 0.9,
		marginLeft: 5,
		marginRight: 5,
		marginBottom: 10,
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: '#C7C7CD'
	},
	tags: {
		color: '#007AFF'
	},
	icon: {
		color: '#C7C7CD'
	},
	bordered: {
		marginTop: 5,
		marginBottom: 5,
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: '#C7C7CD',
		borderTopWidth: 0,
		borderTopColor: '#C7C7CD',
		paddingTop: 3,
		paddingBottom: 3
	},
	flexHalf: {
		flex: 0.5
	},
	pullRight: {
		justifyContent: 'flex-end'
	},
	marginLeft5: {
		marginLeft: 5
	},
	textInput: {
		margin: 10,
		paddingLeft: 10,
		fontSize: 17,
		lineHeight: 35,
		backgroundColor: 'white',
		borderWidth: 0,
		borderRadius: 4
	}
})

class PostEntry extends React.Component {
	static PropTypes = {
		navigator: React.PropTypes.object
	}

	constructor(props) {
		super(props)
		this.postEntryComponent = new PostEntryComponent(appController, this, {})
		this.state = Object.assign(this.postEntryComponent.state, {descriptionHeight: 100})
	}

	/* eslint-disable */
	tagSelectorPressed() {
		const selectedTags = this.postEntryComponent.selectedTags
		const processSelected = this.processSelectedTags.bind(this)
		this.props.navigator.push({
			component: TagSelector,
			rightButtonTitle: 'Done',
			onRightButtonPress: () => {
				this.postEntryComponent.tagSelectionCancelled = false
				this.props.navigator.pop()
			},
			leftButtonTitle: 'Cancel',
			onLeftButtonPress: () => {
				this.postEntryComponent.tagSelectionCancelled = true
				this.props.navigator.pop()
			},
			title: 'Select tags',
			passProps: {selectedTags, processSelected}
		})
	}

	static showError(message) {
		AlertIOS.alert(
			'Error',
			message
		)
		return false
	}

	async validate() {
		const errorMessage = this.postEntryComponent.validate()
		if (errorMessage) return PostEntry.showError(errorMessage)

		try {
			await this.postEntryComponent.save()
			return true
		} catch (err) {
			PostEntry.showError('Something went wrong. Please try again.')
			return false
		}
	}

	userSelectorPressed() {
		const selectedUsers = this.postEntryComponent.selectedUsers
		const processSelectedUsers = this.processSelectedUsers.bind(this)
		this.props.navigator.push({
			component: UserSelector,
			rightButtonTitle: 'Done',
			onRightButtonPress: () => {
				this.postEntryComponent.userSelectionCancelled = false
				this.props.navigator.pop()
			},
			leftButtonTitle: 'Cancel',
			onLeftButtonPress: () => {
				this.postEntryComponent.userSelectionCancelled = true
				this.props.navigator.pop()
			},
			title: 'Copy to',
			passProps: {selectedUsers, processSelectedUsers}
		})
	}
	/* eslint-enable */

	processSelectedUsers(selectedUsers) {
		if (this.postEntryComponent.userSelectionCancelled) return

		this.postEntryComponent.selectedUsers = selectedUsers
		this.setState({userNames: this.postEntryComponent.userNames})
	}

	processSelectedTags(selectedTags) {
		if (this.postEntryComponent.tagSelectionCancelled) return

		this.postEntryComponent.selectedTags = selectedTags
		this.setState({tagNames: this.postEntryComponent.tagNames})
	}

	keyboardToggled(keyboardState, keyboardSpace) {
		this.refs.rv.measure((fx, fy, width, height, px, py) => {
			this.setState({
				descriptionHeight: keyboardState ? 650 - py - keyboardSpace : 600 - py
			})
		})
	}

	updateState(newState) {
		this.setState(newState)
	}

	componentDidMount() {
		this.postEntryComponent.componentDidMount()
	}

	render() {
		return <View style={styles.container}>
			<ScrollView>
				<PostTags
					editMode={true}
					tagNames={this.state.tagNames}
					tagSelectorPressed={this.tagSelectorPressed.bind(this)}/>
				<PostUsers
					editMode={true}
					userNames={this.state.userNames}
					userSelectorPressed={this.userSelectorPressed.bind(this)}/>

				<View style={[styles.row, styles.bordered]}>
					<View style={[styles.flexHalf, styles.row]}>
						<SwitchIOS
							value={this.state.guestComplaint}
							onValueChange={guestComplaint =>
								this.postEntryComponent.guestComplaintChanged(guestComplaint)}/>
						<Text style={styles.marginLeft5}>Guest Complaint</Text>
					</View>
					<View style={[styles.flexHalf, styles.row, styles.pullRight]}>
						<SwitchIOS
							value={this.state.privatePost}
							onValueChange={privatePost => this.postEntryComponent.privatePostChanged(privatePost)}/>
						<Text style={styles.marginLeft5}>Private</Text>
					</View>
				</View>
				<View ref="rv">
					<TextInput
						multiline={true}
						style={[styles.textInput, {height: this.state.descriptionHeight}]}
						placeholder="Write a note..."
						onChangeText={description => this.postEntryComponent.descriptionChanged(description)}
						value={this.state.description}
					/>
				</View>
			</ScrollView>
			<View style={{marginBottom: 40}}>
				<Text/>
			</View>
			<KeyboardSpacer onToggle={this.keyboardToggled.bind(this)}/>

		</View>
	}
}

export default PostEntry
