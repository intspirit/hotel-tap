'use strict'

import React from 'react'
import {StyleSheet, NavigatorIOS, View} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import PostEntry from '../post/PostEntry'
import constants from '../../../shared/constants'
// import appController from '../../core/appController'

const styles = StyleSheet.create({
	navWrap: {
		flex: 1
	},
	nav: {
		flex: 1
	}
})


/**
 *
 */
class NewPostTab extends React.Component {

	static propTypes = {
		selectedTab: React.PropTypes.string.isRequired,
		tabChanged: React.PropTypes.func.isRequired,
		authenticationStateChanged: React.PropTypes.func
	}

	renderView() {
		if (this.props.selectedTab !== constants.mobileMainNavigationTab.ADD_POST) return <View/>

		const newProp = {new: true}

		return (
			<NavigatorIOS
				style={{flex: 1}}
				ref={'addPostRef'}
				itemWrapperStyle={styles.navWrap}
				barTintColor={'black'}
				titleTextColor="#A4AAB3"
				leftButtonTitle={'Cancel'}
				onLeftButtonPress={() => null}
				showTabBar={false}
				rightButtonTitle={'Post'}
				onRightButtonPress={() => null}
				initialRoute={{
					title: 'New note',
					component: PostEntry,
					passProps: {...newProp}
				}}/>
		)
	}

	render() {
		const props = this.props

		return <Icon.TabBarItem
			title={'Add'}
			selected={this.props.selectedTab === constants.mobileMainNavigationTab.ADD_POST}
			selectedIconName={'plus-circle'}
			iconName={'plus'}
			iconSize={20}
			onPress={() => {
				if (props.selectedTab !== constants.mobileMainNavigationTab.ADD_POST) {
					props.tabChanged(constants.mobileMainNavigationTab.ADD_POST)
				} else if (props.selectedTab === constants.mobileMainNavigationTab.ADD_POST) {
					this.refs.addPostRef.popToTop()
				}
			}}>
			{this.renderView()}
		</Icon.TabBarItem>
	}
}

export default NewPostTab
