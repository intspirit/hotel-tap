'use strict'

import React, {Component} from 'react'
import {
	StyleSheet,
	View,
	Text,
	TextInput,
	Image,
	TouchableHighlight,
	TouchableNativeFeedback,
	Platform,
	Dimensions
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import constants from '../../../shared/constants'
import {authController} from '../../core'
import {appController} from '../../../shared/core'
import {Error} from '../core'

const windowSize = Dimensions.get('window')
const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
		flex: 1,
		backgroundColor: 'transparent'
	},
	bg: {
		position: 'absolute',
		left: 0,
		top: 0,
		width: windowSize.width,
		height: windowSize.height
	},
	header: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 0.5,
		backgroundColor: 'transparent'
	},
	mark: {
		width: 147,
		height: 30
	},
	signin: {
		backgroundColor: '#0abbff',
		padding: 20,
		alignItems: 'center'
	},
	signup: {
		justifyContent: 'center',
		alignItems: 'center',
		flex: 0.15
	},
	inputs: {
		marginTop: 0,
		marginBottom: 10,
		flex: 0.70
	},
	inputPassword: {
		marginLeft: 15,
		width: 20,
		height: 21
	},
	inputHotel: {
		marginLeft: 15,
		width: 20,
		height: 20
	},
	inputUsername: {
		marginLeft: 15,
		width: 20,
		height: 20
	},
	inputContainer: {
		padding: 10,
		borderWidth: 1,
		borderBottomColor: '#CCC',
		borderColor: 'transparent',
		justifyContent: 'center'
	},
	input: {
		position: 'absolute',
		left: 61,
		top: 12,
		right: 0,
		height: 20,
		fontSize: 14
	},
	forgotContainer: {
		alignItems: 'flex-end',
		padding: 15
	},
	greyFont: {
		color: '#D8D8D8'
	},
	whiteFont: {
		color: '#FFF'
	},
	backgroundFont: {
		color: '#0abbff'
	},
	selectedUserTypeButton: {
		height: 40,
		padding: 10,
		alignItems: 'center',
		flex: 1,
		backgroundColor: '#0abbff'
	},
	regularUserTypeButton: {
		height: 40,
		padding: 10,
		alignItems: 'center',
		flex: 1,
		backgroundColor: '#7C7C7A',
		borderTopWidth: 1, borderStyle: 'solid', borderTopColor: '#0abbff',
		borderBottomWidth: 1, borderBottomColor: '#0abbff'
	},
	button: {
		borderRadius: 5,
		flex: 1,
		height: 44,
		alignSelf: 'stretch',
		justifyContent: 'center',
		overflow: 'hidden'
	},
	buttonText: {
		fontSize: 18,
		margin: 5,
		textAlign: 'center'
	},
	modalButton: {
		marginTop: 10
	}
})

const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight

/**
 * Login component.
 */
export default class Login extends Component {
	static propTypes = {
		authenticationStateChanged: React.PropTypes.func
	}

	state = {
		hotel: '',
		username: '',
		password: '',
		userType: constants.userType.EMPLOYEE,
		userNamePrompt: 'Username'
	}

	hotelChange(text) {
		this.setState({
			hotel: text
		})
	}

	userNameChange(text) {
		this.setState({
			username: text
		})
	}

	passwordOnChange(text) {
		this.setState({
			password: text
		})
	}

	validCredentials() {
		//TODO: p1 implement validation of entered credentials. Show error in Alert.
		return true
	}

	async loginPressed() {
		if (this.validCredentials()) {
			const credentials = {
				hotelId: this.state.hotel,
				userType: this.state.userType,
				userName: this.state.username,
				password: this.state.password,
				remember: true
			}

			try {
				await authController.login(credentials)

				appController.authenticated = true
				this.props.authenticationStateChanged()
			} catch (err) {
				if (err.response && err.response.error.reason &&
					err.response.error.reason === constants.apiErrorReason.invalidCredentials) {
					//TODO: vvs p2 alert
				} else {
					Error.handleError(err)
				}
			}
		}
	}

	employeeButtonPressed() {
		this.setState({
			userType: constants.userType.EMPLOYEE,
			userNamePrompt: 'Username'
		})
	}

	adminButtonPressed() {
		this.setState({
			userType: constants.userType.ADMIN,
			userNamePrompt: 'Email'
		})
	}


	render() {
		const employeeButtonStyle = this.state.userType === constants.userType.EMPLOYEE ?
			styles.selectedUserTypeButton : styles.regularUserTypeButton
		const adminButtonStyle = this.state.userType === constants.userType.ADMIN ?
			styles.selectedUserTypeButton : styles.regularUserTypeButton
		const employeeFontStyle = this.state.userType === constants.userType.EMPLOYEE ?
			styles.whiteFont : styles.greyFont
		const adminFontStyle = this.state.userType === constants.userType.ADMIN ?
			styles.whiteFont : styles.greyFont

		return <View style={styles.container}>
			<Image style={styles.bg} source={{uri: 'https://i.imgur.com/xlQ56UK.jpg'}}/>
			<View style={styles.header}>
				<Image style={styles.mark} source={{uri: 'https://test.hoteltap.com/img/header-logo.png'}}/>
			</View>
			<View style={{flex: 0.3, flexDirection: 'row'}}>
				<TouchableElement style={employeeButtonStyle} onPress={this.employeeButtonPressed.bind(this)}>
					<View>
						<Text style={employeeFontStyle}>Employee</Text>
					</View>
				</TouchableElement>
				<TouchableElement style={adminButtonStyle} onPress={this.adminButtonPressed.bind(this)}>
					<Text style={adminFontStyle}>Admin</Text>
				</TouchableElement>
			</View>
			<View style={styles.inputs}>
				{this.state.userType === constants.userType.EMPLOYEE ? <View style={styles.inputContainer}>
					<Icon name="bed" style={styles.inputHotel} size={20} color="#C7C7CD"/>
					<TextInput
						style={[styles.input, styles.whiteFont]}
						placeholder="Hotel ID"
						placeholderTextColor="#FFF"
						value={this.state.hotel}To
						onChangeText={this.hotelChange.bind(this)}
					/>
				</View> :
					null}
				<View style={styles.inputContainer}>
					<Icon name="user" style={styles.inputUsername} size={20} color="#C7C7CD"/>
					<TextInput
						style={[styles.input, styles.whiteFont]}
						placeholder={this.state.userNamePrompt}
						placeholderTextColor="#FFF"
						value={this.state.username}
						onChangeText={this.userNameChange.bind(this)}
					/>
				</View>
				<View style={styles.inputContainer}>
					<Icon name="lock" style={styles.inputPassword} size={20} color="#C7C7CD"/>
					<TextInput
						password={true}
						style={[styles.input, styles.whiteFont]}
						placeholder="Password"
						placeholderTextColor="#FFF"
						value={this.state.password}
						onChangeText={this.passwordOnChange.bind(this)}
					/>
				</View>
				{ this.state.userType === constants.userType.ADMIN ?
					<View style={styles.forgotContainer}>
						<Text style={styles.greyFont}>Forgot Password</Text>
					</View> :
					null
				}
			</View>
			<TouchableElement onPress={this.loginPressed.bind(this)}>
				<View style={styles.signin}>
					<Text style={styles.whiteFont}>Sign In</Text>
				</View>
			</TouchableElement>
		</View>
	}
}
