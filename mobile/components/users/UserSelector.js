'use strict'

import React from 'react'
import _ from 'lodash'
import {View, ScrollView, StyleSheet, Text, SwitchIOS, Image} from 'react-native'
import update from 'react-addons-update'
import {db, utils} from '../../../shared/core'
import appController from '../../core/appController'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		padding: 10
	},
	rowIcon: {
		width: 38,
		height: 38,
		overflow: 'visible',
		marginRight: 5
	},
	item: {
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderTopWidth: 1,
		borderTopColor: '#ddd',
		padding: 5
	}
})

/* eslint-disable */
const UserSelectorItem = props => <View style={styles.item}>
	<Image source={{uri: props.user.imageUrl || appController.user.imageUrl}} style={styles.rowIcon}/>
	<Text style={{flex: 0.9}}>{props.user.fullName}</Text>
	<SwitchIOS
		value={props.user.selected}
		onValueChange={value => props.userSelected(props.user.id, value)}/>
</View>
UserSelectorItem.PropTypes = {
	userSelected: React.PropTypes.func,
	user: React.PropTypes.object
}
/* eslint-enable */

/**
 * User selector component.
 */
class UserSelector extends React.Component {
	static propTypes = {
		selectedUsers: React.PropTypes.array,
		processSelectedUsers: React.PropTypes.func
	}
	static defaultProps = {
		selectedUsers: [],
		processSelectedUsers: () => null
	}


	constructor(props) {
		super(props)
		this.state = {
			usersIndexed: [],
			loaded: false
		}
		this.selectedUsers = _.keyBy(props.selectedUsers, 'userId')
	}

	async componentDidMount() {
		const dbUsers = await db.getOtherUsers()

		const users = _.chain(dbUsers).
			map(x => _.assign(x, {selected: false, fullName: utils.getFullName(x)})).
			keyBy('id').
			value()

		this.props.selectedUsers.forEach(x => {
			users[x.userId].selected = true
		})

		this.setState({
			usersIndexed: users,
			loaded: true
		})
	}

	userSelected(userId, selected) {
		this.selectedUsers[userId] = {
			id: this.state.usersIndexed[userId].id,
			name: this.state.usersIndexed[userId].fullName,
			userId: userId,
			selected: selected
		}
		const newUsersIndexed = update(this.state.usersIndexed, {[userId]: {selected: {$set: selected}}})
		this.setState({usersIndexed: newUsersIndexed})
	}

	componentWillUnmount() {
		this.props.processSelectedUsers(Object.values(this.selectedUsers).filter(x => x.selected))
	}

	render() {
		if (!this.state.loaded) return <ScrollView style={styles.container}/>

		return <ScrollView style={styles.container}>
			{Object.values(this.state.usersIndexed).
				sort(x => x.fullName).
				map(x => <UserSelectorItem
					key={x.id}
					userSelected={this.userSelected.bind(this)}
					user={x}/>)}
		</ScrollView>
	}
}

export default UserSelector
