'use strict'

import React from 'react'
import {View, Text, StyleSheet} from 'react-native'

const defaultFontSize = 10
const styles = StyleSheet.create({
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	container: {
		borderTopColor: '#C7C7CD',
		borderStyle: 'solid',
		borderTopWidth: 1,
		padding: 5,
		marginTop: 5
	},
	grey: {
		color: '#C7C7CD'
	}
})

const Readers = props => {
	if (!props.readers || props.readers.length === 0) return null

	return <View style={styles.container}>
		<Text style={styles.defaultFont}>Read by:</Text>
		<Text style={styles.defaultFont}>{props.readers.join(', ')}</Text>
	</View>
}

Readers.propTypes = {
	readers: React.PropTypes.array
}
Readers.defaultProps = {
	readers: []
}

export default Readers
