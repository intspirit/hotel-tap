'use strict'

import {StyleSheet} from 'react-native'

const values = {
	defaultFontSize: 15,
	colorGrey: '#C7C7CD',
	colorBlue: '#007AFF'
}

const styles = StyleSheet.create({
	defaultFontSize: values.defaultFontSize,
	grey: {
		color: values.colorGrey
	},
	blue: {
		color: values.colorBlue
	},
	borderBottomGrey: {
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: values.colorGrey
	},
	padding5: {
		padding: 5
	}
})

module.exports = {
	defaultStyles: styles,
	defaultStyleValues: values
}
