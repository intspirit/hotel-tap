'use strict'

import React from 'react'
import {View, StyleSheet, Text, TouchableHighlight, Modal, PickerIOS} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-end',
		padding: 10,
		backgroundColor: 'rgba(0, 0, 0, 0.5)'
	},
	innerContainer: {
		backgroundColor: '#fff',
		padding: 10,
		borderRadius: 10,
		justifyContent: 'flex-end'
	}
})

export default class Filter extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			modalVisibility: false,
			values: props.values,
			value: props.value || Object.keys(props.values)[0]
		}
	}
	onSelect(value) {
		this.setState({
			value,
			modalVisibility: false
		})
		if (typeof this.props.onSelect === 'function') {
			this.props.onSelect(value)
		}
	}
	render() {
		return <View>
			<Modal
				animationType="slide"
				transparent={true}
				visible={this.state.modalVisibility}
			>
				<View style={styles.container}>
					<View style={styles.innerContainer}>
						<PickerIOS
							selectedValue={this.state.value}
							onValueChange={this.onSelect.bind(this)}
						>
							{
								Object.keys(this.state.values).map(x => {
									let title = this.state.values[x].title
									if (x === this.state.value) {
										title = `> ${title}`
									}

									return <PickerIOS.Item key={x} value={x} label={title}/>
								})
							}
						</PickerIOS>
					</View>
					<TouchableHighlight onPress={() => this.setState({modalVisibility: false})}>
						<View style={[styles.innerContainer, {marginTop: 10}]}>
							<Text style={{textAlign: 'center'}}>Cancel</Text>
						</View>
					</TouchableHighlight>
				</View>
			</Modal>
			<TouchableHighlight onPress={() => this.setState({modalVisibility: !this.state.modalVisibility})}>
				<View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
					<Text style={{color: 'blue'}}>{'--- '}</Text>
					<Text style={{color: 'blue'}}>{this.state.values[this.state.value].title}</Text>
					<Icon size={14} name="angle-down" style={{width: 14, color: 'blue', marginTop: 3, marginLeft: 3}}/>
					<Text style={{color: 'blue'}}>{'---'}</Text>
				</View>
			</TouchableHighlight>
		</View>
	}
}
Filter.propTypes = {
	value: React.PropTypes.string,
	values: React.PropTypes.object,
	onSelect: React.PropTypes.func
}
