'use strict'

import React from 'react'
import {TabBarIOS, NavigatorIOS} from 'react-native'
import BoardsTab from '../boards/BoardsTab'
import TodoTab from '../todo/TodoTab'
import NewPostTab from '../post/NewPostTab'
import PostEntry from '../post/PostEntry'
import constants from '../../../shared/constants'
import appController from '../../core/appController'

/**
 * Main navigation component.
 */
class MainNavigation extends React.Component {
	static propTypes = {
		authenticationStateChanged: React.PropTypes.func
	}

	state = {
		selectedTab: 'boards',
		visible: true
	}

	tabChanged(tab) {
		if (tab === constants.mobileMainNavigationTab.ADD_POST) {
			this.setState({visible: false})
		} else {
			this.setState({selectedTab: tab})
		}
	}

	renderNewPost() {
		return <NavigatorIOS
			style={{flex: 1}}
			ref={'addPostRef'}
			barTintColor={'black'}
			titleTextColor="#A4AAB3"
			leftButtonTitle={'Cancel'}
			onLeftButtonPress={() => this.setState({visible: true})}
			rightButtonTitle={'Post'}
			onRightButtonPress={() => {
				const postResult = appController.validateNewPost()
				if (postResult) this.setState({visible: true})
			}}
			initialRoute={{
					title: 'New note',
					component: PostEntry
				}}/>

	}

	render() {
		if (!this.state.visible) return this.renderNewPost()

		return <TabBarIOS tintColor="white" barTintColor="black" unselectedTintColor="#A4AAB3">
			<BoardsTab {...this.props}
				selectedTab={this.state.selectedTab}
				tabChanged={this.tabChanged.bind(this)}/>
			<NewPostTab {...this.props}
				selectedTab={this.state.selectedTab}
				tabChanged={this.tabChanged.bind(this)}/>
			<TodoTab {...this.props}
				selectedTab={this.state.selectedTab}
				tabChanged={this.tabChanged.bind(this)}/>
		</TabBarIOS>
	}
}

export default MainNavigation
