'use strict'

import React from 'react'
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {defaultStyles, defaultStyleValues} from '../core/defaultStylesheet'

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	icon: {
		color: defaultStyleValues.colorGrey
	},
	iconRequired: {
		color: 'black'
	},
	tagsTextContainer: {
		flex: 0.9,
		marginLeft: 5,
		marginRight: 5
	},
	tags: {
		color: defaultStyleValues.colorBlue
	}
})

const PostTags = props => props.tagNames === '' && !props.editMode ? //eslint-disable-line no-confusing-arrow
	null :
	<View style={[styles.row, defaultStyles.borderBottomGrey, defaultStyles.padding5]}>
		<Icon style={styles.iconRequired} name="tags"/>
		<View style={styles.tagsTextContainer}>
			<Text style={styles.tags}>{props.tagNames}</Text>
		</View>
		{props.editMode ? <TouchableOpacity onPress={props.tagSelectorPressed}>
			<Icon style={styles.icon} name="pencil"/>
		</TouchableOpacity> :
			null}
	</View>

PostTags.propTypes = {
	editMode: React.PropTypes.bool,
	tagNames: React.PropTypes.string,
	tagSelectorPressed: React.PropTypes.func
}
PostTags.defaultProps = {
	editMode: false,
	tagNames: '',
	tagSelectorPressed: () => null
}


export default PostTags

