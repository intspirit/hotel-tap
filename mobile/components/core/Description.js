'use strict'

import React from 'react'
import {Text, StyleSheet} from 'react-native'
import constants from '../../../shared/constants'

const styles = StyleSheet.create({
	defaultFont: {
		fontSize: 14
	},
	defaultFont1: {
		fontSize: 14
	},
	marginAbove: {
		marginTop: 5
	}
})

/**
 * Description component.
 * @param {object} props
 * @param {object} props.post
 * @param {string} props.post.descriptionText
 * @return {object}
 */
const Description = props => {
	if (!props.post.descriptionText ||
		props.post.descriptionText === '' ||
		props.post.typeId === constants.postTypes.TASK &&
		props.post.completion.status === constants.taskStatus.DONE &&
		!props.details)
		return null

	return <Text
		style={[styles.defaultFont, styles.marginAbove, props.style || {}]}
		numberOfLines={props.numberOfLines}>
		{props.post.descriptionText}
	</Text>
}

Description.propTypes = {
	post: React.PropTypes.shape({
		descriptionText: React.PropTypes.string,
		typeId: React.PropTypes.string,
		completion: React.PropTypes.object
	}),
	details: React.PropTypes.bool,
	numberOfLines: React.PropTypes.number
}
Description.defaultProps = {
	post: {
		descriptionText: '',
		typeId: constants.postTypes.NOTE,
		completion: {status: constants.taskStatus.OPEN}
	},
	details: false
}

export default Description
