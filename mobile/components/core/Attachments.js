'use strict'

import React from 'react'
import {View, Image} from 'react-native'
import constants from '../../../shared/constants'

const Attachments = props => {
	if (props.count <= 0) return null

	return <View>
		{
			props.attachments.map(x => {
				if (!x.type.includes(constants.attachmentTypes.IMAGE)) return null

				return <Image
					key={x.id}
					resizeMode="contain"
					style={{height: 100}}
					source={{uri: x.url}}
				/>
			})
		}
	</View>
}
Attachments.propTypes = {
	attachments: React.PropTypes.array,
	count: React.PropTypes.number
}

export default Attachments
