'use strict'

import React from 'react'
import {Text, StyleSheet} from 'react-native'

const styles = StyleSheet.create({
	text: {
		fontSize: 10
	}
})

const DefaultText = props => <Text style={styles.text}>{props.children}</Text>

DefaultText.propTypes = {
	children: React.PropTypes.object
}

export default DefaultText
