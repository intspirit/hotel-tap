'use strict'

import React from 'react'
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import {defaultStyles, defaultStyleValues} from '../core/defaultStylesheet'

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	icon: {
		color: defaultStyleValues.colorGrey
	},
	tagsTextContainer: {
		flex: 0.9,
		marginLeft: 5,
		marginRight: 5
	},
	tags: {
		color: defaultStyleValues.colorBlue
	}
})
/* eslint-disable */
const PostUsers = props => (props.userNames.length > 0 || props.editMode ?
	<View style={[styles.row, defaultStyles.borderBottomGrey, defaultStyles.padding5]}>
		<Icon style={styles.icon} name="user"/>
		<View style={styles.tagsTextContainer}>
			<Text style={styles.tags}>{props.userNames}</Text>
		</View>
		{props.editMode ? <TouchableOpacity onPress={props.userSelectorPressed}>
			<Icon style={styles.icon} name="pencil"/>
		</TouchableOpacity> :
			null}
	</View> :
	null)


PostUsers.propTypes = {
	editMode: React.PropTypes.bool,
	userNames: React.PropTypes.string,
	userSelectorPressed: React.PropTypes.func
}
PostUsers.defaultProps = {
	editMode: false,
	userNames: '',
	userSelectorPressed: () => null
}


export default PostUsers

