'use strict'

import {Alert} from 'react-native'

export default {
	handleError(err, onOk) {
		//noinspection JSUnusedGlobalSymbols
		const onPressOkHandler = onOk || (() => null)
		Alert.alert(
			'Error',
			err.message || 'Something went wrong. Please try again.',
			[{text: 'OK', onPress: () => onPressOkHandler}]
		)
	}
}
