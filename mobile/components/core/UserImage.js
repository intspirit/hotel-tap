'use strict'

import React from 'react'
import {Image, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const styles = StyleSheet.create({
	image: {
		overflow: 'visible',
		marginRight: 5,
		width: 25,
		height: 25
	}
})


const UserImage = props => {
	if (props.url === '') return <Icon style={props.style} name="user" size={5} color={props.color}/>

	return <Image style={props.style} source={{uri: props.url}}/>
}


UserImage.propTypes = {
	style: React.PropTypes.oneOfType([
		React.PropTypes.object,
		React.PropTypes.number
	]),
	url: React.PropTypes.string,
	name: React.PropTypes.string,
	size: React.PropTypes.number,
	color: React.PropTypes.string
}
UserImage.defaultProps = {
	style: styles.image,
	url: '',
	name: '',
	size: 25,
	color: '#C7C7CD'
}

export default UserImage
