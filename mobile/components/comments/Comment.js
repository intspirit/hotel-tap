'use strict'

import React from 'react'
import {View, StyleSheet} from 'react-native'
import PostHeader from '../boards/PostHeader'
import Description from '../core/Description'
import Attachments from '../core/Attachments'

const defaultFontSize = 10
const styles = StyleSheet.create({
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	container: {
		borderBottomColor: '#C7C7CD',
		borderStyle: 'solid',
		borderBottomWidth: 1,
		padding: 5,
		marginTop: 5
	},
	grey: {
		color: '#C7C7CD'
	}
})

const Comment = props => <View style={styles.container}>
	<PostHeader post={props.comment} showTitleDetails={false}/>
	<Description post={props.comment}/>
	<Attachments count={props.comment.attachmentCount} attachments={props.comment.attachments}/>

</View>

Comment.propTypes = {
	comment: React.PropTypes.object
}
Comment.defaultProps = {
	comment: {}
}

export default Comment
