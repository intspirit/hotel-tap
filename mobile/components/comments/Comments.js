'use strict'

import React from 'react'
import {View, StyleSheet} from 'react-native'
import update from 'react-addons-update'
import Comment from './Comment'
import CommentEntry from './CommentEntry'
import appController from '../../core/appController'

const defaultFontSize = 10
const styles = StyleSheet.create({
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	container: {
		borderTopColor: '#C7C7CD',
		borderStyle: 'solid',
		borderTopWidth: 1,
		padding: 5,
		marginTop: 5
	},
	grey: {
		color: '#C7C7CD'
	}
})

class Comments extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			comments: props.post.comments
		}
		appController.commentsComponent = this
	}

	addComment(comment) {
		const comments = update(this.state.comments, {$push: [comment]})
		this.setState({comments: comments})
	}

	render() {
		return <View style={styles.container}>
			{this.state.comments.map(x => <Comment key={x.id} comment={x}/>)}
			<CommentEntry {...this.props}/>
		</View>
	}
}

Comments.propTypes = {
	post: React.PropTypes.object,
	addComment: React.PropTypes.func
}
Comments.defaultProps = {
	post: {
		id: -1,
		comments: []
	},
	addComment: () => null
}

export default Comments
