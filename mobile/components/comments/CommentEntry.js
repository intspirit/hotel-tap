'use strict'

import React from 'react'
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput'
import Icon from 'react-native-vector-icons/FontAwesome'
import {postService} from '../../../shared/services'

import appController from '../../core/appController'

const defaultFontSize = 10
const styles = StyleSheet.create({
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	container: {
		padding: 5,
		marginTop: 5,
		flexDirection: 'row',
		alignItems: 'center'
	},
	grey: {
		color: '#C7C7CD'
	},
	cameraIcon: {
		//flex: 0.1
	},
	input: {
		height: 35,
		margin: 3,
		paddingLeft: 10,
		fontSize: defaultFontSize,
		lineHeight: 35,
		backgroundColor: 'white',
		borderColor: '#C7C7CD',
		borderStyle: 'solid',
		borderWidth: 1,
		borderRadius: 4,
		flex: 0.8
	},
	postButton: {
		fontWeight: 'bold',
		color: 'blue'
	}
})

class CommentEntry extends React.Component {
	state = {
		_text: ''
	}


	onCameraPressed() {
		return null
	}

	async onPostPressed() {

		let comment = this.text._textInput._lastNativeText.trim() // eslint-disable-line
		if (comment === '') {
			this.text._textInput.setNativeProps({text: ''}) // eslint-disable-line

			return
		}

		const response = await postService.insertComment(this.props.post.id, {comment: comment})
		this.text._textInput.setNativeProps({text: ''}) // eslint-disable-line

		// this.props.postsChanged(comment)
		appController.addComment(this.props.post.id, response.id, comment)
	}

	render() {
		return <View style={styles.container}>
			<TouchableOpacity onPress={this.onCameraPressed.bind(this)}>
				<Icon style={styles.cameraIcon} name="camera" size={13} color="#C7C7CD"/>
			</TouchableOpacity>
			<AutoGrowingTextInput
				autoFocus={true}
				style={styles.input}
				placeholder={'Write a comment...'}
				ref={(r) => {
					this.text = r
				}}/>
			<TouchableOpacity onPress={this.onPostPressed.bind(this)}>
				<Text style={styles.postButton}>Post</Text>
			</TouchableOpacity>
		</View>
	}
}

CommentEntry.propTypes = {
	post: React.PropTypes.object,
	addComment: React.PropTypes.func
}
CommentEntry.defaultProps = {
	postId: -1,
	addComment: () => null
}

export default CommentEntry
