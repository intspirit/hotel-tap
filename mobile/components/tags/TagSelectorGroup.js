'use strict'

import React from 'react'
import {View, Text, StyleSheet, SwitchIOS, TouchableOpacity} from 'react-native'
import Collapsible from 'react-native-collapsible'
import Icon from 'react-native-vector-icons/FontAwesome'

const styles = StyleSheet.create({
	group: {
		backgroundColor: '#F7F7F7',
		flex: 1,
		borderTopWidth: 1,
		borderTopColor: '#ddd',
		justifyContent: 'center',
		flexDirection: 'row',
		height: 25
	},
	item: {
		backgroundColor: 'white',
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderTopWidth: 1,
		borderTopColor: '#ddd',
		padding: 5
	}
})

/**
 * Tag selector group.
 * @param {object} props
 * @return {React.Component|null}
 *
 */
class TagSelectorGroup extends React.Component {
	static propTypes = {
		tags: React.PropTypes.object,
		group: React.PropTypes.object,
		tagsIndexed: React.PropTypes.object,
		tagSelected: React.PropTypes.func
	}
	constructor(props) {
		super(props)
		this.state = {
			collapsed: true
		}
	}

	toggleCollapsed() {
		this.setState({
			collapsed: !this.state.collapsed
		})
	}

	render() {
		const tags = this.props.tags[this.props.group.uId]
		if (!tags) return null

		return <View>
			<TouchableOpacity onPress={this.toggleCollapsed.bind(this)}>
				<View style={styles.group}>
					<Text style={{flex: 0.9}}>
						{this.props.group.name}
					</Text>
					<Icon name={this.state.collapsed ? 'chevron-down' : 'chevron-up'} size={15}/>
				</View>
			</TouchableOpacity>

			<Collapsible collapsed={this.state.collapsed}>
				{tags.map(x => <TagSelectorItem
					key={x.uId}
					tag={x}
					groupId={this.props.group.uId}
					tagsIndexed={this.props.tagsIndexed}
					tagSelected={this.props.tagSelected}/>)}
			</Collapsible>
		</View>
	}
}

/**
 * TagSelectorItem component - show individual tag.
 * @param {object} props
 * @return {React.Component}
 */
/* eslint-disable*/ // wrong error on undeclared props.tag
const TagSelectorItem = props => <View style={styles.item}>
	<Text style={{flex: 0.9}}>{props.tag.name}</Text>
	<SwitchIOS
		value={props.tagsIndexed[props.tag.uId].selected}
		onValueChange={value => props.tagSelected(props.groupId, props.tag.uId, value)}/>
</View>
TagSelectorItem.PropTypes = {
	tagsIndexed: React.PropTypes.object,
	tag: React.PropTypes.object,
	groupId: React.PropTypes.string
}

export default TagSelectorGroup
