'use strict'

import React from 'react'
import update from 'react-addons-update'
import _ from 'lodash'
import {View, ScrollView, StyleSheet, SegmentedControlIOS, ActivityIndicator} from 'react-native'
import {tagsMapper} from '../../../shared/core'
import TagSelectorGroup from './TagSelectorGroup'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		padding: 10
	},
	activityIndicatorStyle: {
		height: 80,
		alignItems: 'center',
		justifyContent: 'center'
	}
})

/**
/**
 * TagSelector component - shows tags for selection
 */
class TagSelector extends React.Component {
	static propTypes = {
		selectedTags: React.PropTypes.array,
		processSelected: React.PropTypes.func
	}
	static defaultProps = {
		selectedTags: [],
		processSelected: () => null
	}

	constructor(props) {
		super(props)
		this.state = {
			selectedGroupIndex: 0,
			groups: []
		}
		this.departmentsIndexed = {}
		this.areasIndexed = {}
		this.equipmentGroupsIndexed = {}
		this.selectedGroup = {}

		this.groupedTags = {}
		this.selectedTags = _.keyBy(props.selectedTags, 'tagId')
		// this.build()
	}

	async build() {
		let {departmentsIndexed, groupedTags, tagsIndexed, areasIndexed, equipmentGroupsIndexed} =
			await tagsMapper.map(this.props.selectedTags)
		this.groupedTags = groupedTags
		this.departmentsIndexed = departmentsIndexed
		this.areasIndexed = areasIndexed
		this.equipmentGroupsIndexed = equipmentGroupsIndexed
		this.groups = [departmentsIndexed, areasIndexed, equipmentGroupsIndexed]

		_.assign(this.state, {tagsIndexed: tagsIndexed})
	}

	tagSelected(groupId, tagId, value) {
		this.selectedTags[tagId] = {
			id: this.state.tagsIndexed[tagId].id,
			type: this.state.tagsIndexed[tagId].type,
			tagId: tagId,
			tagName: `#${this.state.tagsIndexed[tagId].name}`,
			groupId: groupId,
			groupName: `@${this.selectedGroup[groupId].name}`,
			selected: value
		}
		const newTagIndexed = update(this.state.tagsIndexed, {[tagId]: {selected: {$set: value}}})
		this.setState({tagsIndexed: newTagIndexed})
	}

	componentWillUnmount() {
		this.props.processSelected(Object.values(this.selectedTags).filter(x => x.selected))
	}

	async componentWillMount() {
		await this.build()
		this.setState({groups: this.groups})
	}

	tagGroupChanged(event) {
		this.setState({
			selectedGroupIndex: event.nativeEvent.selectedSegmentIndex
		})
	}

	renderTagGroup() {
		if (this.state.groups.length === 0) return <ActivityIndicator
			animating={true}
			style={styles.activityIndicatorStyle}
			size="large"
		/>

		this.selectedGroup = this.state.groups[this.state.selectedGroupIndex]

		return Object.values(this.selectedGroup).map(d => <TagSelectorGroup
			key={d.uId}
			group={d}
			tagSelected={this.tagSelected.bind(this)}
			tagsIndexed={this.state.tagsIndexed}
			tags={this.groupedTags}/>)
	}

	render() {
		return <View style={styles.container}>
			<ScrollView>
				<SegmentedControlIOS
					style={{marginBottom: 3}}
					values={['Departments *', 'Locations *', 'Equipment']}
					onChange={this.tagGroupChanged.bind(this)}
					selectedIndex={this.state.selectedGroupIndex}/>
				{this.renderTagGroup()}
			</ScrollView>
		</View>
	}
}

export default TagSelector
