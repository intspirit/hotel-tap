'use strict'

import React from 'react'
import {View} from 'react-native'
import Post from './Post'

/**
 * Multiple posts component.
 * @param {object} props
 * @return {React.Component}
 */
const Posts = props => {
	let {posts, ...other} = props

	return <View>
		{posts.map(x => <Post key={x.id} post={x} {...other}/>)}
	</View>
}

Posts.propTypes = {
	posts: React.PropTypes.array,
	navigator: React.PropTypes.object
}
Posts.defaultProps = {
	posts: []
}

export default Posts
