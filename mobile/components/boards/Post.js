'use strict'

import React from 'react'
import {View, StyleSheet} from 'react-native'
import _ from 'lodash'
import pluralize from 'pluralize'
import PostDetails from './PostDetails'
import constants from '../../../shared/constants'
import {postService} from '../../../shared/services'
import {appController} from '../../../shared/core'
import PostHeader from './PostHeader'
import ViewsAndComments from './ViewsAndComments'
import PostActions from './PostActions'
import TaskSubject from './TaskSubject'
import TaskAssignment from './TaskAssignment'
import PostGuestComplaint from './PostGuestComplaint'
import {Description, Attachments, Readers, PostTags, PostUsers, Error} from '../core'
import Comments from '../comments/Comments'

const defaultFontSize = 10
const styles = StyleSheet.create({
	container: {
		marginBottom: 3,
		padding: 5
	},
	header: {
		flexDirection: 'row'
	},
	image: {
		width: 25,
		height: 25,
		overflow: 'visible',
		marginRight: 5
	},
	headerText: {},
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	commentsView: {
		alignItems: 'flex-end',
		flex: 0.3
	},
	actionButtonsView: {
		borderTopColor: '#C7C7CD',
		borderStyle: 'solid',
		borderTopWidth: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 5
	},
	grey: {
		color: '#C7C7CD'
	},
	actionButtonsLabel: {
		marginLeft: 3,
		marginRight: 15,
		color: '#C7C7CD',
		fontSize: defaultFontSize

	},
	iconButton: {
		flexDirection: 'row'
	},
	read: {
		backgroundColor: 'white'
	},
	notRead: {
		backgroundColor: '#EFEFF4'
	}
})

/**
 * Post (note, task) component.
 */
class Post extends React.Component {
	static propTypes = {
		navigator: React.PropTypes.object,
		details: React.PropTypes.bool,
		post: React.PropTypes.shape({
			readStatus: React.PropTypes.string,
			readBy: React.PropTypes.array,
			attachments: React.PropTypes.array,
			id: React.PropTypes.number,
			attachmentCount: React.PropTypes.number,
			commentCount: React.PropTypes.number,
			typeId: React.PropTypes.string,
			completion: React.PropTypes.object,
			guestComplaint: React.PropTypes.bool,
			createdBy: React.PropTypes.object
		}),
		onComplete: React.PropTypes.func
	}
	static defaultProps = {
		post: {},
		details: false
	}

	constructor(props) {
		super(props)
		this.state = {
			// post: props.post,
			readStatus: props.post.readStatus,
			webViewHeight: 100,
			viewsLabel: Post.readBy2Views(props.post.readBy.length)
		}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			readStatus: nextProps.post.readStatus,
			viewsLabel: Post.readBy2Views(nextProps.post.readBy.length)
		})
	}

	async markReadPressed() {
		const newReadStatus = this.state.readStatus === constants.postReadStatus.READ ?
			constants.postReadStatus.UNREAD :
			constants.postReadStatus.READ

		try {
			const data = await postService.update(this.props.post.id, {readStatus: newReadStatus})
			let viewCount = this.props.post.readBy.length
			if (this.props.post.createdBy.id !== appController.user.id) {
				viewCount += data.readStatus === constants.postReadStatus.READ ? 1 : -1
			}
			this.setState({
				readStatus: data.readStatus,
				viewsLabel: Post.readBy2Views(viewCount)
			})
		} catch (err) {
			//TODO: vvs p2 show alert popup
		}
	}

	showDetails() {
		this.props.navigator.push({
			component: PostDetails,
			title: _.capitalize(this.props.post.typeId),
			passProps: {...this.props}
		})
	}

	static readBy2Views(readByCount) {
		return readByCount === 0 ? null : `${readByCount} ${pluralize('view', readByCount)}`
	}

	flagPressed() {
		return this.state
	}

	async taskDonePressed() {
		let doneStatus = this.props.post.completion.status
		let post = this.props.post
		if (this.props.post.completion.status === constants.taskStatus.DONE) {
			doneStatus = constants.taskStatus.OPEN
			post.completion = {
				completedBy: {
					id: -1,
					fullName: null,
					imageUrl: null
				},
				dateTime: null,
				status: doneStatus
			}
		} else {
			doneStatus = constants.taskStatus.DONE
			post.completion = {
				completedBy: {
					id: appController.user.id,
					fullName: appController.user.fullName,
					imageUrl: appController.user.imageUrl
				},
				dateTime: new Date(),
				status: doneStatus
			}
		}
		try {
			await postService.update(this.props.post.id, {completionStatus: doneStatus})
			this.setState({
				post: post
			})
			if (typeof this.props.onComplete === 'function') {
				this.props.onComplete()
			}
		} catch (error) {
			Error.handleError(error)
		}
	}

	render() {
		const post = this.props.post
		let {markReadLabel, readStyle} = this.state.readStatus === constants.postReadStatus.READ ?
		{markReadLabel: 'Mark unread', readStyle: styles.read} :
		{markReadLabel: 'Mark read', readStyle: styles.notRead}
		const commentsLabel = this.props.post.commentCount > 0 ?
			`${post.commentCount} ${pluralize('Comment', post.commentCount)}` :
			null


		return <View style={[styles.container, readStyle]}>
			<PostHeader {...this.props}/>
			<TaskSubject {...this.props} taskDonePressed={this.taskDonePressed.bind(this)}/>
			<TaskAssignment {...this.props} />
			<PostGuestComplaint guestComplaint={this.props.post.guestComplaint}/>
			<PostTags
				editMode={false}
				tagNames={post.tags.map(x => `@${x.typeName} #${x.name}`).join(', ')}/>
			<PostUsers
				editMode={false}
				userNames={post.ccUsers.map(x => x.fullName).join(', ')}/>
			<Description {...this.props}/>

			{!this.props.details && post.typeId === constants.postTypes.TASK &&
			post.completionStatus === constants.taskStatus.DONE ?
				null :
				<Attachments count={post.attachmentCount} attachments={post.attachments}/>
			}

			{this.props.details || post.typeId === constants.postTypes.TASK &&
			post.completionStatus === constants.taskStatus.DONE ?
				null :
				<ViewsAndComments
					commentsLabel={commentsLabel}
					viewsLabel={this.state.viewsLabel}
					showDetails={this.showDetails.bind(this)}/>
			}

			{this.props.details ? <Readers readers={post.readBy}/> : null}

			{this.props.details ?
				null :
				<PostActions
					showCommentButton={!this.props.details}
					markReadLabel={markReadLabel}
					markReadPressed={this.markReadPressed.bind(this)}
					flagPressed={this.flagPressed.bind(this)}
					showDetails={this.showDetails.bind(this)}/>
			}

			{this.props.details ?
				<Comments {...this.props} /> :
				null}
		</View>
	}

}

export default Post
