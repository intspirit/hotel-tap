'use strict'

import React from 'react'
import {NavigatorIOS} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Boards from '../boards/Boards'
import {authController} from '../../core'
import constants from '../../../shared/constants'


/**
 * Board tab component of the main navigation.
 */
class BoardTab extends React.Component {

	static propTypes = {
		selectedTab: React.PropTypes.string.isRequired,
		tabChanged: React.PropTypes.func.isRequired,
		authenticationStateChanged: React.PropTypes.func
	}

	renderView() {
		return <NavigatorIOS
			style={{flex: 1}}
			ref={'boardsRef'}
			barTintColor="black"
			titleTextColor="#A4AAB3"
			rightButtonTitle={'Search'}
			onRightButtonPress={() => {
				authController.logout()
				this.props.authenticationStateChanged()
			}}
			initialRoute={{
				title: 'Boards',
				component: Boards
			}}/>
	}

	render() {
		const props = this.props

		return <Icon.TabBarItem
			title={'Boards'}
			selected={this.props.selectedTab === constants.mobileMainNavigationTab.BOARDS}
			iconName={'list-alt'}
			iconSize={20}
			onPress={() => {
				if (props.selectedTab !== constants.mobileMainNavigationTab.BOARDS) {
					props.tabChanged(constants.mobileMainNavigationTab.BOARDS)
				} else if (props.selectedTab === constants.mobileMainNavigationTab.BOARDS) {
					this.refs.boardsRef.popToTop()
				}
			}}>
			{this.renderView()}
		</Icon.TabBarItem>
	}
}

export default BoardTab
