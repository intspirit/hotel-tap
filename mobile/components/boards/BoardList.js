'use strict'

import React from 'react'
import {View} from 'react-native'
import BoardListRow from './BoardListRow'

/**
 * List of boards component
 * @param {object} props
 * @props {Array} props.options
 * @constructor
 */
const BoardList = props => <View>
	{props.options.filter(x => x.id > 0 || x.id === -10).
	map(x => <BoardListRow key={x.id} option={x} navigator={props.navigator}/>)}
</View>

BoardList.propTypes = {
	options: React.PropTypes.array,
	navigator: React.PropTypes.object
}

export default BoardList
