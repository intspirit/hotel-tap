'use strict'

import React from 'react'
import {View, StyleSheet, Text, TouchableHighlight} from 'react-native'

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		padding: 5
	},
	defaultFont: {
		fontSize: 12
	},
	grey: {
		color: '#C7C7CD'
	}
})

//noinspection Eslint
/**
 * Component renders views and comments counts.
 * @param {object} props
 * @param {string} props.viewsLabel
 * @param {string} props.commentsLabel
 * @param {func} props.showDetails
 * @constructor
 */
const ViewsAndComments = props => {
	if (!props.viewsLabel && !props.commentsLabel) return null

	return <View style={styles.container}>
		<View style={{flex: 0.7}}>
			<TouchableHighlight onPress={props.showDetails}>
				<Text style={[styles.grey, styles.defaultFont]}>
					{props.viewsLabel}
				</Text>
			</TouchableHighlight>
		</View>
		<View style={styles.commentsView}>
			<TouchableHighlight onPress={props.showDetails}>
				<Text style={[styles.grey, styles.defaultFont]}>{props.commentsLabel}</Text>
			</TouchableHighlight>
		</View>
	</View>
}

ViewsAndComments.propTypes = {
	viewsLabel: React.PropTypes.string,
	commentsLabel: React.PropTypes.string,
	showDetails: React.PropTypes.func
}

ViewsAndComments.defaultProps = {
	viewsLabel: null,
	commentsLabel: null,
	showDetails: () => null
}

export default ViewsAndComments
