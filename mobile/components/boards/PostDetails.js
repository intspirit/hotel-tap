'use strict'

import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Post from './Post'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#C7C7CD'
	}
})

class PostDetails extends React.Component {

	render() {
		//TODO: vvs p1 mark as read
		return <View style={styles.container}>
			<ScrollView>
				<Post post={this.props.post} navigator={this.props.navigator} details={true}/>
			</ScrollView>
		</View>
	}
}

PostDetails.propTypes = {
	post: React.PropTypes.object,
	navigator: React.PropTypes.object
}

export default PostDetails
