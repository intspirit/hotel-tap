'use strict'

import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import UserImage from '../core/UserImage'
import {dateTimeHelper} from '../../../shared/core'

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row'
	},
	image: {
		width: 25,
		height: 25,
		overflow: 'visible',
		marginRight: 5
	},
	headerText: {},
	defaultFont: {
		fontSize: 16
	},
	smallFont: {
		fontSize: 8
	},
	grey: {
		color: '#C7C7CD'
	}
})

/**
 * Post Header component
 * @param {object} props
 * @param {object} props.post
 * @param {string} props.post.typeId
 * @param {string} props.post.createdDateTime
 * @param {object} props.post.createdBy
 * @param {string} props.post.createdBy.fullName
 * @param {string} props.post.createdBy.imageUrl
 * @constructor
 */
const PostHeader = props => <View style={styles.header}>
	<UserImage url={props.post.createdBy.imageUrl}/>
	<View style={styles.headerText}>
		<View style={{flexDirection: 'row', alignItems: 'center'}}>
			<Text style={[styles.defaultFont, {fontWeight: 'bold'}]}>{props.post.createdBy.fullName}</Text>
			{props.showTitleDetails ? <Text style={styles.defaultFont}> added a {props.post.typeId}</Text> : null}
		</View>
		<Text style={[styles.smallFont, styles.grey]}>
			{dateTimeHelper.getDateTimeFormatted(props.post.createdDateTime, 'MMMM DD [at] h:mma')}
		</Text>
	</View>
</View>

PostHeader.propTypes = {
	post: React.PropTypes.object,
	showTitleDetails: React.PropTypes.bool
}
PostHeader.defaultProps = {
	post: {},
	showTitleDetails: true
}

export default PostHeader
