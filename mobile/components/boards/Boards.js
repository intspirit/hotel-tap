'use strict'

import React, {Component} from 'react'
import {StyleSheet, View, ScrollView, ActivityIndicator, Alert, RefreshControl} from 'react-native'
import BoardList from './BoardList'
import navigationLogic from '../../../shared/navigationLogic'
import {postService, userService} from '../../../shared/services'
import appController from '../../core/appController'


const styles = StyleSheet.create({
	centering: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	activityIndicatorStyle: {
		height: 80,
		alignItems: 'center',
		justifyContent: 'center'
	}
})


/**
 * Boards top components.
 */
export default class Boards extends Component {

	static propTypes = {
		navigator: React.PropTypes.object
	}

	state = {
		options: [],
		loaded: false,
		refreshing: false
	}

	showGenericErrorAlert() {
		Alert.alert(
			'Error',
			'Something went wrong. Please try again.',
			[{text: 'OK', onPress: () => this.setState({
				options: [],
				unreadCounts: [],
				loaded: true,
				refreshing: false
			})}]
		)
	}

	async getUnreadCount() {
		try {
			const data = await postService.getUnreadCount()
			const optionsWithCounts = navigationLogic.setUnreadCounts(this.state.options, data.counts)
			this.setState({
				options: optionsWithCounts,
				unreadCounts: data.counts,
				loaded: true
			})
		} catch (err) {
			this.showGenericErrorAlert()
		}
	}

	async fetchData() {
		try {
			let data = await userService.getNavigation()
			await this.setState({
				options: data.options
			}, await this.getUnreadCount)
		} catch (err) {
			this.showGenericErrorAlert()
		}
	}

	async onRefresh() {
		this.setState({refreshing: true})
		await this.fetchData()
		this.setState({refreshing: false})
	}

	async componentWillMount() {
		appController.navigator = this.props.navigator
		await this.fetchData()
	}

	render() {
		const refreshControl = <RefreshControl
			refreshing={this.state.refreshing}
			onRefresh={this.onRefresh.bind(this)}
		/>

		return <View style={{flex: 1, marginTop: 64}}>
			{this.state.loaded ?
				<ScrollView refreshControl={refreshControl}>
					<BoardList options={this.state.options} {...this.props}/>
				</ScrollView> :
				<View style={styles.centering}>
					<ActivityIndicator
						animating={true}
						style={styles.activityIndicatorStyle}
						size="large"
					/>
				</View>
			}
		</View>
	}
}
