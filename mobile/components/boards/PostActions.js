'use strict'

import React from 'react'
import {View, StyleSheet, Text, TouchableHighlight} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

const defaultFontSize = 10
const styles = StyleSheet.create({
	defaultFont: {
		fontSize: defaultFontSize
	},
	smallFont: {
		fontSize: 8
	},
	actionButtonsView: {
		borderTopColor: '#C7C7CD',
		borderStyle: 'solid',
		borderTopWidth: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 5,
		marginTop: 5
	},
	grey: {
		color: '#C7C7CD'
	},
	actionButtonsLabel: {
		marginLeft: 3,
		marginRight: 15,
		color: '#C7C7CD',
		fontSize: 12

	},
	iconButton: {
		flexDirection: 'row'
	},
	read: {
		backgroundColor: 'white'
	},
	notRead: {
		backgroundColor: '#EFEFF4'
	}
})

/**
 * Post actions bar component.
 * @param {object} props
 * @param {string} props.markReadLabel
 * @param {func} props.showDetails
 * @param {func} props.markReadPressed
 * @param {func} props.flagPressed
 * @param {func} props.showDetails
 * @constructor
 */
const PostActions = props => <View style={styles.actionButtonsView}>
	<TouchableHighlight onPress={props.markReadPressed} style={{flex: 0.33}}>
		<View style={styles.iconButton}>
			<Icon name="envelope" size={13} color="#C7C7CD"/>
			<Text style={styles.actionButtonsLabel}>{props.markReadLabel}</Text>
		</View>
	</TouchableHighlight>
	<TouchableHighlight onPress={props.showDetails} style={{flex: 0.33}}>
		<View style={styles.iconButton}>
			<Icon name="comment" size={13} color="#C7C7CD"/>
			<Text style={styles.actionButtonsLabel}>Comment</Text>
		</View>
	</TouchableHighlight>
	<TouchableHighlight onPress={props.flagPressed} style={{flex: 0.33}}>
		<View style={styles.iconButton}>
			<Icon name="flag" size={13} color="#C7C7CD"/>
			<Text style={styles.actionButtonsLabel}>Flag</Text>
		</View>
	</TouchableHighlight>
</View>

PostActions.propTypes = {
	markReadLabel: React.PropTypes.string,
	markReadPressed: React.PropTypes.func,
	flagPressed: React.PropTypes.func,
	showDetails: React.PropTypes.func
}

PostActions.defaultProps = {
	markReadLabel: 'Mark read',
	markReadPressed: () => null,
	flagPressed: () => null,
	showDetails: () => null
}

export default PostActions
