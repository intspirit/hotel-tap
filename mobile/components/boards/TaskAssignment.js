'use strict'

import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import constants from '../../../shared/constants'
import {dateTimeHelper} from '../../../shared/core'
import UserImage from '../core/UserImage'

const defaultFontSize = 10
const styles = StyleSheet.create({
	container: {
		marginTop: 5,
		flexDirection: 'row',
		padding: 5
	},
	assignmentView: {
		flex: 0.5,
		flexDirection: 'row',
		alignItems: 'center'
	},
	assignmentLabel: {
		fontSize: defaultFontSize,
		color: '#C7C7CD'
	},
	dateView: {
		flex: 0.5,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	dateLabel: {
		marginLeft: 3,
		fontSize: defaultFontSize
	},
	defaultFont: {
		fontSize: defaultFontSize
	},
	image: {
		overflow: 'visible',
		marginRight: 5,
		width: 13,
		height: 13
	}
})

const TaskAssignment = props => {
	if (props.post.typeId === constants.postTypes.NOTE) return null

	const now = new Date()
	let assignmentImageUrl = props.post.assignmentTo.imageUrl
	let assignmentLabel = props.post.assignmentTo.fullName
	let dateIconName = 'calendar'
	let dateLabel = props.post.dueDateTime
	let dateColor = new Date(props.post.dueDateTime) < now ? 'red' : '#C7C7CD'
	if (props.post.completion.status === constants.taskStatus.DONE) {
		assignmentImageUrl = props.post.completion.completedBy.imageUrl
		assignmentLabel = props.post.completion.completedBy.fullName
		dateIconName = 'calendar-check-o'
		dateLabel = props.post.completion.dateTime
		dateColor = '#C7C7CD'
	}

	return <View style={styles.container}>
		<View style={styles.assignmentView}>
			<UserImage url={assignmentImageUrl} size={5} style={styles.image} color={'#FFF'}/>
			<Text style={styles.assignmentLabel}>{assignmentLabel}
			</Text>
		</View>
		{dateLabel ? <View style={styles.dateView}>
			<Icon name={dateIconName} size={13} color={dateColor}/>
			<Text
				style={[styles.dateLabel, {color: dateColor}]}>{dateTimeHelper.getDateTimeFormatted(dateLabel)}
			</Text>
		</View> : null
		}
	</View>
}

export default TaskAssignment

TaskAssignment.propTypes = {
	post: React.PropTypes.object
}
TaskAssignment.defaultProps = {
	post: {typeId: constants.postTypes.NOTE}
}
