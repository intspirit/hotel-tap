'use strict'

import React from 'react'
import {View, Text, TouchableHighlight, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import constants from '../../../shared/constants'

const styles = StyleSheet.create({
	defaultFont: {
		fontSize: 14
	},
	smallFont: {
		fontSize: 8
	},
	subject: {
		fontSize: 14,
		marginLeft: 5
	}
})

const TaskSubject = props => {
	let iconName = 'check'
	let iconColor = '#C7C7CD'
	let fontColor = 'black'
	let fontWeight = 'bold'
	if (props.post.completion.status === constants.taskStatus.DONE) {
		iconName = 'check-circle'
		iconColor = 'black'
		fontColor = '#C7C7CD'
		fontWeight = 'normal'
	}

	return props.post.typeId === constants.postTypes.TASK ?
		<View style={{flexDirection: 'row', alignItems: 'center'}}>
			<TouchableHighlight onPress={props.taskDonePressed}>
				<Icon
					name={iconName}
					size={26}
					color={iconColor}/>
			</TouchableHighlight>
			<Text
				style={[styles.subject, {fontWeight: fontWeight}, {color: fontColor}]}>{
				props.post.subject}
			</Text>
		</View> :
		null
}

export default TaskSubject

TaskSubject.propTypes = {
	post: React.PropTypes.object,
	taskDonePressed: React.PropTypes.func
}
TaskSubject.defaultProps = {
	post: {typeId: constants.postTypes.NOTE},
	taskDonePressed: () => null
}
