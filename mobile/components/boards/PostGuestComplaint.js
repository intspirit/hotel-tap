'use strict'

import React from 'react'
import {View, StyleSheet, Text} from 'react-native'

const defaultFontSize = 10
const styles = StyleSheet.create({
	text: {
		fontSize: defaultFontSize,
		color: 'red'
	}
})

/**
 * Post Guest Complaint tag
 * @param {object} props
 * @param {bool} props.guestComplaint
 * @return {Component|null}
 */
const PostGuestComplaint = props => {
	if (!props.guestComplaint) return null

	return <View><Text style={styles.text}>Guest Complaint</Text></View>

}

PostGuestComplaint.propTypes = {
	guestComplaint: React.PropTypes.bool
}
PostGuestComplaint.defaultProps = {
	guestComplaint: false
}

export default PostGuestComplaint
