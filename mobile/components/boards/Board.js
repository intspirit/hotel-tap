'use strict'

import React from 'react'
import {View, Text, ScrollView, StyleSheet, ActivityIndicator, TouchableHighlight, RefreshControl} from 'react-native'
import update from 'react-addons-update'
import {postService} from '../../../shared/services'
import Posts from './Posts'
import appController from '../../core/appController'
import Error from '../core/Error'
import constants from '../../../shared/constants'
import _ from 'lodash'

const styles = StyleSheet.create({
	container: {
		marginTop: 60,
		marginBottom: 60,
		flex: 1,
		backgroundColor: '#C7C7CD'
	},
	centering: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	moreButton: {
		flex: 1,
		alignItems: 'center',
		marginBottom: 3,
		padding: 5,
		backgroundColor: 'white',
		borderRadius: 10
	}
})

class Board extends React.Component {
	static propTypes = {
		option: React.PropTypes.object,
		navigator: React.PropTypes.object
	}

	state = {
		posts: [],
		hasMore: false,
		isLoading: true,
		isLoadingMore: false,
		nexPageToken: 0,
		loaded: false,
		refreshing: false
	}

	constructor(props) {
		super(props)
		appController.boardComponent = this

		this.fetchData = this.fetchData.bind(this)
		this.loadData = this.loadData.bind(this)
	}

	addComment(postId, comment) {
		const postIndex = this.state.posts.findIndex(x => x.id === postId)
		const existingPost = this.state.posts[postIndex]
		const updatedPost = update(existingPost, {
			commentCount: {$set: existingPost.commentCount + 1},
			comments: {$push: [comment]}
		})

		const updatedPosts = update(this.state, {
			posts: {[postIndex]: {$set: updatedPost}}
		})
		this.setState({posts: updatedPosts})
	}

	async fetchData() {
		let method = 'getByDepartmentId'
		let id = this.props.option.id

		if (this.props.option.type === constants.boardTypes.EMPLOYEE) {
			method = 'getByEmployeeId'
			if (id === constants.hiddenUserId) {
				id = appController.user.id
			}
		}

		return await postService[method](id, this.state.nexPageToken)
	}

	async loadData() {
		try {
			const data = await this.fetchData()
			this.setState({...data, loaded: true})
		} catch (err) {
			Error.handleError(err)
		}
	}

	async onRefresh() {
		this.setState({refreshing: true})
		await this.loadData()
		this.setState({refreshing: false})
	}

	async componentDidMount() {
		await this.loadData()
	}

	async getMore() {
		try {
			this.setState({
				isLoadingMore: true
			})
			const data = await this.fetchData()

			this.setState({
				posts: this.state.posts.concat(data.posts),
				hasMore: data.hasMore,
				nextPageToken: data.nextPageToken,
				isLoadingMore: false
			})
		} catch (err) {
			this.setState({
				isLoadingMore: false
			})
		}
	}

	render() {
		if (!this.state.loaded) {
			return <View style={styles.centering}>
				<ActivityIndicator
					animating={true}
					style={styles.activityIndicatorStyle}
					size="large"
				/>
			</View>
		}

		const getMore = this.state.hasMore ? <TouchableHighlight onPress={this.getMore.bind(this)}>
			<View style={styles.moreButton}>
				<Text>{this.state.isLoadingMore ? 'Loading...' : 'Load more'}</Text>
			</View>
		</TouchableHighlight> : null

		const refreshControl = <RefreshControl
			refreshing={this.state.refreshing}
			onRefresh={this.onRefresh.bind(this)}
		/>

		return <View style={styles.container}>
			<ScrollView refreshControl={refreshControl}>
				<Posts navigator={this.props.navigator} posts={_.isEmpty(this.state.posts) ? [] : this.state.posts}/>
				{getMore}
			</ScrollView>
		</View>
	}
}

export default Board
