'use strict'

import React from 'react'
import {View, TouchableHighlight, Image, Text, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import appController from '../../core/appController'
import Board from './Board'

const styles = StyleSheet.create({
	rowIcon: {
		width: 38,
		height: 38,
		overflow: 'visible',
		marginRight: 5
	},
	content: {
		backgroundColor: 'white',
		padding: 10,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderTopWidth: 1,
		borderTopColor: '#ddd'
	},
	label: {
		flex: 1
	},
	rowCounter: {
		marginRight: 5,
		color: '#8F8E94'
	}
})


/**
 * Board list row component.
 */
class BoardListRow extends React.Component {
	static propTypes = {
		option: React.PropTypes.object,
		navigator: React.PropTypes.object
	}
	static defaultProps = {
		option: {
			iconUrl: '',
			label: '',
			count: -1
		}
	}

	boardSelected(item) {
		this.props.navigator.push({
			component: Board,
			title: item.option.label,
			passProps: {...item}
		})
	}

	render() {
		return <TouchableHighlight onPress={this.boardSelected.bind(this, this.props)}>
			<View style={styles.content}>
				<Image source={{uri: this.props.option.iconUrl || appController.user.imageUrl}} style={styles.rowIcon}/>
				<View style={styles.label}>
					<Text>
						{this.props.option.label}
					</Text>
				</View>
				<Text style={styles.rowCounter}>{this.props.option.count > 0 ? this.props.option.count : ''}</Text>
				<Icon color="#8F8E94" name="angle-right" size={23}/>
			</View>
		</TouchableHighlight>
	}
}

export default BoardListRow
