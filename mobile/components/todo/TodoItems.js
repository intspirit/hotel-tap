'use strict'

import React from 'react'
import {View, ScrollView, StyleSheet, Text, ActivityIndicator, RefreshControl} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import Error from '../core/Error'
import constants from '../../../shared/constants'
import {todoService} from '../../../shared/services'
import appController from '../../core/appController'

import TodoItemsRow from './TodoItemsRow'

const groupIconWidth = 17

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#FFFFFF',
		marginTop: 64
	},
	groupHeader: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 5,
		borderRadius: 10,
		borderWidth: 1,
		borderStyle: 'solid',
		backgroundColor: '#e9e9e9',
		borderColor: '#e0e0e0'
	},
	groupHeaderTitle: {
		textAlign: 'left',
		flex: 1
	},
	groupHeaderCounter: {
		width: 20,
		marginRight: 10,
		textAlign: 'right'
	},
	groupHeaderIcon: {
		width: groupIconWidth,
		marginLeft: 5,
		marginRight: 3
	},
	activityIndicatorStyle: {
		height: 80,
		alignItems: 'center',
		justifyContent: 'center'
	}
})

const decor = {
	[constants.todoGroupNames.OVERDUE]: {
		icon: 'warning',
		style: {
			color: '#a90329',
			fontWeight: 'normal'
		}
	},
	[constants.todoGroupNames.TODAY]: {
		icon: 'exclamation',
		style: {
			color: '#000000',
			fontWeight: 'bold'
		}
	},
	[constants.todoGroupNames.FUTURE]: {
		icon: 'arrow-up',
		style: {
			color: '#000000',
			fontWeight: 'normal'
		}
	}
}

const typeIndexed = {
	[constants.boardTypes.DEPARTMENT]: 'departments',
	[constants.boardTypes.EMPLOYEE]: 'employees'
}

//const filter = {
//	today: {title: 'Today'},
//	incomplete: {title: 'Incomplete Tasks'},
//	completed: {title: 'Completed This Week'}
//}

export default class TodoItems extends React.Component {
	static defaultProps = {
		filterValue: 'today'
	}
	static propTypes = {
		filterValue: React.PropTypes.string,
		option: React.PropTypes.object,
		navigator: React.PropTypes.object
	}

	constructor(props) {
		super(props)

		this.state = {
			filterValue: props.filterValue,
			data: {},
			refreshing: false,
			loaded: false
		}
		this.fetchData = this.fetchData.bind(this)
		this.itemMove = this.itemMove.bind(this)
		this.removeItem = this.removeItem.bind(this)
		this.onTodoComplete = this.onTodoComplete.bind(this)

		appController.listComponent = this
	}

	async fetchData() {
		const type = typeIndexed[this.props.option.type]
		try {
			const data = await todoService.getList(type, this.props.option.id)
			this.setState({data, loaded: true})
		} catch (err) {
			Error.handleError(err)
		}
	}

	async onRefresh() {
		this.setState({refreshing: true})
		await this.fetchData()
		this.setState({refreshing: false})
	}

	async componentDidMount() {
		return await this.fetchData()
	}
	onFilterChanged(filterValue) {
		this.setState({filterValue})
	}

	itemMove(srcGroup, index) {
		this.removeItem(srcGroup, index)
		appController.onTodoComplete(null, this.props.option.type, this.props.option.id)
	}

	removeItem(groupName, itemIndex) {
		const data = this.state.data

		const currentGroup = data.groups.find(group => group.name === groupName)
		currentGroup.collection.items.splice(itemIndex, 1)
		currentGroup.collection.count -= 1
		data.totalCount -= 1

		this.setState({data})
	}

	onTodoComplete(postId) {
		let groupName = false
		let itemPosition = false
		this.state.data.groups.map(group => {
			group.collection.items.map((item, itemIndex) => {
				if (item.id === postId) {
					itemPosition = itemIndex
					groupName = group.name
				}

				return true
			})

			return true
		})

		if (groupName !== false && itemPosition !== false) {
			this.removeItem(groupName, itemPosition)
		}
	}


	render() {
		let content = ''
		if (this.state.loaded) {
			const refreshControl = <RefreshControl
				refreshing={this.state.refreshing}
				onRefresh={this.onRefresh.bind(this)}
			/>

			// @TODO: ay p9 - Filtering functionality postponed
			// <Filter value={this.state.filterValue} values={filter} onSelect={this.onFilterChanged.bind(this)}/>
			// content = <ScrollView contentOffset={{x: 0, y: 16}} refreshControl={refreshControl}>
			content = <ScrollView contentOffset={{x: 0, y: 0}} refreshControl={refreshControl}>
				{
					this.state.data.groups.
						map(group => {
							let color = '#000000'
							let fontWeight = 'normal'
							if (decor[group.name]) {
								color = decor[group.name].style.color
								fontWeight = decor[group.name].style.fontWeight
							}

							return <View key={group.name}>
								<View style={styles.groupHeader}>
									<View style={{flex: 1, flexDirection: 'row'}}>
										<Icon
											size={groupIconWidth}
											name={decor[group.name] ? decor[group.name].icon : 'circle-o'}
											style={[styles.groupHeaderIcon, {color}]}
										/>
										<Text style={{color, fontWeight}}>{group.title}</Text>
									</View>
									<Text style={[styles.groupHeaderCounter, {color, fontWeight}]}>
										{group.collection.count}
									</Text>
								</View>
								{
									group.collection.items.map((x, index) =>
										<TodoItemsRow
											key={x.id}
											post={x}
											onChange={this.itemMove.bind(this, group.name, index)}
											navigator={this.props.navigator}
										/>
									)
								}
							</View>
						})
				}
			</ScrollView>
		} else {
			content = <ActivityIndicator animating style={styles.activityIndicatorStyle} size="large"/>
		}

		return <View style={styles.container}>{content}</View>
	}
}
