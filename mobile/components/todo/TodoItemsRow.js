import React, {Component} from 'react'
import {StyleSheet, TouchableHighlight, View, Text, ActivityIndicator} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import moment from 'moment'
import _ from 'lodash'

import TodoDetails from './TodoDetails'

import Description from '../core/Description'

import Error from '../core/Error'
import constants from '../../../shared/constants'
import postService from '../../../shared/services/postService'

const styles = StyleSheet.create({
	todoRow: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		paddingTop: 5,
		paddingBottom: 5,
		paddingLeft: 10,
		paddingRight: 10,
		backgroundColor: '#F6F6F6',
		borderBottomWidth: 1,
		borderColor: '#e9e9e9'
	},
	todoRowTitle: {
		marginBottom: 0,
		fontSize: 14
	},
	todoRowTeaser: {
		marginTop: 0,
		fontSize: 10,
		color: '#696969'
	},
	todoRowDate: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 14
	},
	activityIndicatorStyle: {
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		marginRight: 8,
		marginLeft: 3
	}
})

export default class TodoItemsRow extends Component {
	constructor(props) {
		super(props)
		this.state = {
			isLoading: false,
			item: props.post
		}
	}

	async toggleCompleteness() {
		if (this.state.isLoading) return false

		try {
			this.setState({isLoading: true})

			const completionStatus = this.state.item.completion.status === constants.taskStatus.DONE ?
				constants.taskStatus.OPEN :
				constants.taskStatus.DONE

			const serviceCompletionResponse = await postService.update(this.state.item.id, {completionStatus})
			if (serviceCompletionResponse.status !== completionStatus) {
				throw new Error(`Completion status not updated; value: ${serviceCompletionResponse.status}`)
			}
			const serviceReadResponse = await postService.update(
				this.state.item.id,
				{readStatus: constants.postReadStatus.READ}
			)
			if (serviceReadResponse.readStatus !== constants.postReadStatus.READ) {
				throw new Error(`Read status not updated; value: ${serviceReadResponse.status}`)
			}

			if (typeof this.props.onChange === 'function') {
				this.props.onChange(completionStatus)
			}
		} catch (err) {
			Error.handleError(err)
		}

		return true
	}

	onPress() {
		const {post, ...rest} = this.props
		this.props.navigator.push({
			component: TodoDetails,
			title: _.capitalize(this.props.post.typeId),
			passProps: {postId: post.id, ...rest}
		})
	}

	renderDueDate(dateStr) {
		const date = moment(dateStr)

		let text = date.format('MMM DD')
		if (date.isSame(moment(), 'day')) {
			text = date.format('h:mm a')
		}

		const color = moment().isAfter(date) ? '#CC3333' : '#696969'

		return <View>
			{
				text.split(' ').map((line, i) => <Text key={i} style={[styles.todoRowDate, {color}]}>{line}</Text>)
			}
		</View>
	}

	render() {
		const item = this.state.item
		const isDone = item.completion.status === constants.taskStatus.DONE

		let icon = ''
		if (this.state.isLoading) {
			icon = <ActivityIndicator animating style={styles.activityIndicatorStyle} size="small"/>
		} else {
			icon = <TouchableHighlight onPress={this.toggleCompleteness.bind(this)}>
				<Icon
					size={30}
					name="check-circle-o"
					style={{
						marginRight: 5,
						color: isDone ? '#66CC00' : '#CCCCCC'
					}}
				/>
			</TouchableHighlight>
		}

		const textDecorationLine = isDone ? 'line-through' : 'none'

		return <TouchableHighlight onPress={this.onPress.bind(this)}>
			<View style={styles.todoRow}>
				{icon}
				<View style={{flex: 1}}>
					<Text style={[styles.todoRowTitle, {textDecorationLine}]} numberOfLines={1}>
						{item.subject}
					</Text>
					<Description
						post={item}
						style={[styles.todoRowTeaser, {textDecorationLine}]}
						numberOfLines={2}
					/>
				</View>
				{this.renderDueDate(item.dueDateTime)}
			</View>
		</TouchableHighlight>
	}
}

TodoItemsRow.propTypes = {
	navigator: React.PropTypes.object,
	post: React.PropTypes.object,
	onChange: React.PropTypes.func
}
