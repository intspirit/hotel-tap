import React from 'react'
import {NavigatorIOS} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

import TodoBoards from './TodoBoards'
import authController from '../../core/authController'

const tab = 'todo'

class TodoTab extends React.Component {
	renderView() {
		return <NavigatorIOS
			style={{flex: 1}}
			ref={'todoRef'}
			barTintColor="black"
			titleTextColor="#A4AAB3"
			rightButtonTitle={'Search'}
			onRightButtonPress={() => {
				authController.logout()
				this.props.authenticationStateChanged()
			}}
			initialRoute={{
				title: 'To do',
				component: TodoBoards
			}}
		/>
	}

	render() {
		return <Icon.TabBarItem
			title={'To do'}
			selected={this.props.selectedTab === tab}
			iconName={'check-square'}
			iconSize={20}
			onPress={() => {
				if (this.props.selectedTab !== tab) {
					this.props.tabChanged(tab)
				} else if (this.props.selectedTab === tab) {
					this.refs.todoRef.popToTop()
				}
			}}>
			{this.renderView()}
		</Icon.TabBarItem>
	}
}

TodoTab.propTypes = {
	selectedTab: React.PropTypes.string.isRequired,
	tabChanged: React.PropTypes.func.isRequired,
	authenticationStateChanged: React.PropTypes.func.isRequired
}

export default TodoTab
