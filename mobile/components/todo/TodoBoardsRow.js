'use strict'

import React from 'react'
import {View, TouchableHighlight, Image, Text, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import appController from '../../core/appController'
import TodoItems from './TodoItems'

const styles = StyleSheet.create({
	rowIcon: {
		width: 38,
		height: 38,
		overflow: 'visible',
		marginRight: 5
	},
	content: {
		backgroundColor: 'white',
		padding: 10,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderTopWidth: 1,
		borderTopColor: '#ddd'
	},
	label: {
		flex: 1
	},
	rowCounter: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 14,
		color: 'red',
		marginTop: 12
	}
})


/**
 * Board list row component.
 */
class TodoBoardsRow extends React.Component {
	static propTypes = {
		option: React.PropTypes.object,
		navigator: React.PropTypes.object
	}
	static defaultProps = {
		option: {
			iconUrl: '',
			label: '',
			count: -1
		}
	}

	boardSelected(item) {
		this.props.navigator.push({
			component: TodoItems,
			title: item.option.label,
			passProps: {...item}
		})
	}

	render() {
		return <TouchableHighlight onPress={this.boardSelected.bind(this, this.props)}>
			<View style={styles.content}>
				<Image source={{uri: this.props.option.iconUrl || appController.user.imageUrl}} style={styles.rowIcon}/>
				<View style={styles.label}>
					<Text>
						{this.props.option.label}
					</Text>
				</View>
				<Text style={styles.rowCounter}>{this.props.option.count > 0 ? this.props.option.count : ''}</Text>
				<Icon name="angle-right" size={30}/>
			</View>
		</TouchableHighlight>
	}
}

export default TodoBoardsRow
