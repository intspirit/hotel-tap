'use strict'

import React, {Component} from 'react'
import {StyleSheet, View, ScrollView, ActivityIndicator, RefreshControl} from 'react-native'

import TodoBoardsRow from './TodoBoardsRow'

import Error from '../core/Error'
import navigationLogic from '../../../shared/navigationLogic'
import {todoService, userService} from '../../../shared/services'

import appController from '../../core/appController'

const styles = StyleSheet.create({
	centering: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center'
	},
	activityIndicatorStyle: {
		height: 80,
		alignItems: 'center',
		justifyContent: 'center'
	}
})

/**
 * Boards top components.
 */
export default class TodoBoards extends Component {
	constructor(props) {
		super(props)

		this.state = {
			options: [],
			unreadCounts: 0,
			loaded: false,
			refreshing: false
		}
		this.onTodoComplete = this.onTodoComplete.bind(this)
		this.updateBoardCount = this.updateBoardCount.bind(this)

		this.fetchData = this.fetchData.bind(this)

		appController.boardComponent = this
	}

	async getUnreadCount() {
		try {
			const data = await todoService.getCount()
			const optionsWithCounts = navigationLogic.setUnreadCounts(this.state.options, data.counts)
			await this.setState({
				options: optionsWithCounts,
				unreadCounts: data.counts,
				loaded: true
			})
		} catch (err) {
			Error.handleError(err, () => this.setState({
					options: [],
					unreadCounts: [],
					loaded: true
				})
			)
		}
	}

	onTodoComplete(postId, assignmentType, assignmentId) {
		this.updateBoardCount(assignmentType, assignmentId, -1)
	}

	updateBoardCount(type, id, change) {
		const unreadCounts = this.state.unreadCounts.map(x => {
			if (x.type === type && x.id === id) x.count += change

			return x
		})
		const options = navigationLogic.setUnreadCounts(this.state.options, unreadCounts)
		this.setState({options, unreadCounts})
	}

	async componentWillMount() {
		await this.fetchData()
	}

	async fetchData() {
		try {
			let data = await userService.getNavigation()
			await this.setState({
				options: data.options
			}, await this.getUnreadCount)
		} catch (err) {
			Error.handleError(err)
		}
	}

	async onRefresh() {
		this.setState({refreshing: true})
		await this.fetchData()
		this.setState({refreshing: false})
	}

	render() {
		let content = ''
		if (this.state.loaded) {
			const refreshControl = <RefreshControl
				refreshing={this.state.refreshing}
				onRefresh={this.onRefresh.bind(this)}
			/>
			content = <ScrollView refreshControl={refreshControl}>
				{
					this.state.options.
						filter(x => x.id > 0 || x.id === -10).
						map(x => <View key={x.id}><TodoBoardsRow option={x} navigator={this.props.navigator}/></View>)
				}
			</ScrollView>
		} else {
			content = <ActivityIndicator animating style={styles.activityIndicatorStyle} size="large"/>
		}

		return <View style={{flex: 1, marginTop: 64}}>{content}</View>
	}
}

TodoBoards.propTypes = {
	navigator: React.PropTypes.object.isRequired
}

