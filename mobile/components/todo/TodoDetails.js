'use strict'

import React from 'react'
import {View, StyleSheet, ScrollView, RefreshControl, ActivityIndicator} from 'react-native'

import {postService} from '../../../shared/services'

import Post from '../boards/Post'
import Error from '../core/Error'
import constants from '../../../shared/constants'
import appController from '../../core/appController'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#C7C7CD'
	}
})

class TodoDetails extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			loaded: false,
			refreshing: false
		}
	}

	async fetchData() {
		try {
			const response = await postService.getPostById(this.props.postId)
			this.setState({post: response.post, loaded: true})
		} catch (err) {
			Error.handleError(err)
		}
	}

	async onRefresh() {
		this.setState({refreshing: true})
		await this.fetchData()
		this.setState({refreshing: false})
	}

	async componentDidMount() {
		if (this.state.loaded) return false

		return await this.fetchData()
	}
	onComplete() {
		const type = this.state.post.assignmentTo.type === constants.assignmentTypes.DEPARTMENT ?
			constants.navigationOptionType.DEPARTMENT :
			constants.navigationOptionType.EMPLOYEE
		appController.onTodoComplete(this.state.post.id, type, this.state.post.assignmentTo.id)
	}

	render() {
		let content = <ScrollView>
			<ActivityIndicator animating style={styles.activityIndicatorStyle} size="large"/>
		</ScrollView>
		if (this.state.loaded) {
			const refreshControl = <RefreshControl
				refreshing={this.state.refreshing}
				onRefresh={this.onRefresh.bind(this)}
			/>
			content = <ScrollView refreshControl={refreshControl}>
				<Post
					post={this.state.post}
					navigator={this.props.navigator}
					details={true}
					onComplete={this.onComplete.bind(this)}
				/>
			</ScrollView>
		}

		//TODO: ay p1 mark as read
		return <View style={styles.container}>{content}</View>
	}
}

TodoDetails.propTypes = {
	postId: React.PropTypes.number,
	navigator: React.PropTypes.object
}

export default TodoDetails
