'use strict'

import globals from './mobile/core/globals'
//noinspection JSUnresolvedVariable
GLOBAL.HOTELTAP_SETTINGS = globals

import React from 'react'
import {AppRegistry} from 'react-native'

import MainNavigation from './mobile/components/core/MainNavigation'
import Login from './mobile/components/login/Login'
import sharedAppController from './shared/core/appController'
import localDb from './mobile/core/localDb'


async function initApp() {
	sharedAppController.localDb = localDb
	const jwtToken = await localDb.getJwt()
	if (jwtToken) {
		sharedAppController.jwtToken = jwtToken
		const user = await localDb.getUser()
		sharedAppController.setUser(user)
	}
}

/**
 * Root Layout component.
 */
class Layout extends React.Component {
	state = {
		authenticated: false,
		initialized: false
	}

	async componentDidMount() {
		await initApp()
		this.setState({
			authenticated: sharedAppController.authenticated,
			initialized: true
		})
	}

	authenticationStateChanged() {
		this.setState({
			authenticated: sharedAppController.authenticated
		})
	}

	render() {
		if (!this.state.initialized) return null

		return this.state.authenticated ?
			<MainNavigation authenticationStateChanged={this.authenticationStateChanged.bind(this)}/> :
			<Login authenticationStateChanged={this.authenticationStateChanged.bind(this)}/>
	}
}

//noinspection JSCheckFunctionSignatures
AppRegistry.registerComponent('HotelTap_Web', () => Layout)
