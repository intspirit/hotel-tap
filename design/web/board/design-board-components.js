import React from 'react'

class Board extends React.Component {
	render() {
		return <div>
			<PostEntry />
			<Posts />
		</div>
	}
}

class Posts extends React.Component {
	render() {
		return this.state.posts.map(post => post.type === postTypes.NOTE ?
			<Note note={post}/> :
			<Task task={post}/>)
	}
}

class Note extends React.Component {
	render() {
		return <div>
			<NoteHeader>
				<UserImage /> //should accept named dimensions: SMALL, NORMAL, LARGE, ...
				<PostTitle />
				<PostDateTime />
				<NoteFlag />
			</NoteHeader>
			<Description />
			<TranslatorCaller />
			<Attachments>
				<Attachment /> //img or icon for other file types
			</Attachments>
			<PostActions />
			<Comments>
				<CommentExpander />
				<Comment>
					<UserImage />
					<PostDateTime />
				</Comment>
				<CommentEntry>
					<UserImage />
					<DescriptionEntry />
					<AttachmentEntry />
				</CommentEntry>

			</Comments>

		</div>
	}
}

/* Component in folders:
 /client
	 /components
		 /core
			 Description
			 DescriptionEntry
			 UserImage
			 PostDateTime
			 TranslatorCaller
			 /comments
				 Comments
				 CommentExpander
				 Comment
				 CommentEntry
			 /attachments
				 Attachments
				 Attachment
		 /board
			 Board
			 Posts
			 Note
			 NoteHeader
			 PostTitle
			 NoteFlag
			 PostActions
			 Task

 */

// Some controls may be available already
