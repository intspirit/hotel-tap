'use strict'

import moment from 'moment-timezone'

export default {

	getDateTime(dateTime) {
		return dateTime && moment(dateTime, 'YYYY-MM-DD HH:mm:ss')
	},

	getDateTimeFormatted(dateTime, format) {
		return dateTime && this.getDateTime(dateTime).format(format || 'MMM DD [at] h:mma')
	},

	getUtcDateTime(dateTime, format) {
		return dateTime && moment.utc(this.getDateTimeFormatted(dateTime, format))
	},

	getHotelNow(timezone) {
		return moment.tz(timezone || 'UTC')
	},

	validateDueDateTime(dateTime) {
		const dateTimeMoment = moment.isMoment(dateTime) ? dateTime : moment(dateTime)

		return dateTimeMoment.isAfter(moment().subtract(1, 'days'))
	}
}
