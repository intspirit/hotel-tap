'use strict'

import _ from 'lodash'
import constants from '../constants'
import db from './db'
import Promise from 'bluebird'

/**
 * Generates unique id based on the id and prefix.
 * @param {string} prefix
 * @param {int} id
 * @return {string}
 */
const getUuid = (prefix, id) => `${prefix}-${id}`

/**
 * Add unique id property to the group object.
 * @param {object[]} groups
 * @param {string} groupPrefix
 * @param {string} namePrefix
 * @return {object[]}
 */
const assignUid = (groups, groupPrefix, namePrefix = '') => groups.map(x => (
{
	uId: getUuid(groupPrefix, x.id),
	id: x.id,
	name: namePrefix === '' ? x.name : `${namePrefix}${x.name}`,
	type: groupPrefix
}))

/**
 * Maps row tags to specific groups (departments, areas, equipment).
 * @param {Array} selectedTags
 * @return {{departmentsIndexed: *, groupedTags: *, tagsIndexed: *, areasIndexed: *, equipmentGroupsIndexed: *}}
 */
async function map(selectedTags = []) {
	const [tags, departments, maintenanceTypes, areas, equipmentGroups] =
		await Promise.all([
			db.getTags(),
			db.getDepartments(),
			db.getMaintenanceTypes(),
			db.getAreas(),
			db.getEquipmentGroups()
		])

	const departmentsWithUid = assignUid(departments, constants.tagLegacyType.DEPARTMENT)
	const maintenanceWithUid = assignUid(maintenanceTypes, constants.tagLegacyType.MAINTENANCE, 'Maint - ')
	const departmentsWithMaintenanceTags = departmentsWithUid.concat(maintenanceWithUid)
	const areasWithUid = assignUid(areas, constants.tagLegacyType.AREA)
	const equipmentGroupsWithUid = assignUid(equipmentGroups, constants.tagLegacyType.EQUIPMENT)

	const departmentsIndexed = _.keyBy(departmentsWithMaintenanceTags, constants.UID)
	const areasIndexed = _.keyBy(areasWithUid, constants.UID)
	const equipmentGroupsIndexed = _.keyBy(equipmentGroupsWithUid, constants.UID)

	const tagsIndexed = _.keyBy(tags.map(x =>
		_.assign(x, {uId: getUuid(x.type, x.id), selected: false, disabled: false})), constants.UID)

	selectedTags.forEach(x => {
		tagsIndexed[x.tagId].selected = true
		tagsIndexed[x.tagId].disabled = x.disabled
	})

	const groupedTags = _.groupBy(tags, x => `${x.type}-${x.typeId}`)

	return {
		departmentsIndexed, groupedTags, tagsIndexed, areasIndexed, equipmentGroupsIndexed
	}
}

export default {
	map,
	getUuid
}
