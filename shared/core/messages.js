'use strict'

export default {
	error: {
		NAME_REQUIRED: 'Name is required',
		ASSIGNMENT_REQUIRED: 'Assignment is required',
		OLD_PASSWORD_REQUIRED: 'Please, enter Old Password',
		NEW_PASSWORD_REQUIRED: 'Please, enter New Password',
		NEW_PASSWORD_CONFIRMATION_REQUIRED: 'Please, enter New Password Confirmation',
		MATCH_PASSWORD_AND_CONFIRMATION: 'New Password and Confirmation should match',
		PASSWORD_UPDATE_FAILED: 'Something went wrong. Please try again',
		NO_CREDENTIALS: 'Please, enter credentials',
		INVALID_LOGIN: 'Cannot sign you in. Please, try again',
		NEW_PASSWORD_SHOULD_NOT_EQUAL_OLD_PASSWORD: 'New Password should not be same as Old Password'
	}
}
