'use strict'

const sanitizeHtml = require('sanitize-html')
const Entities = require('html-entities').AllHtmlEntities

const entities = new Entities()

function convertDescription(html, options, doNotDecodeSpecialChars, allowEmptyTags) {
	const defaultOptions = {
		allowedTags: ['a'],
		allowedAttributes: {}
	}

	if (!doNotDecodeSpecialChars) {
		defaultOptions.textFilter = text => entities.decode(text)
	}

	if (!allowEmptyTags) {
		defaultOptions.exclusiveFilter = frame => !frame.text.trim()
	}

	return sanitizeHtml(`${html}`, Object.assign(defaultOptions, options))
}

function convertToText(html) {
	// Conversion for removing our mentions and hashtags
	let text = convertDescription(html, {
		allowedTags: ['a'],
		allowedAttributes: {},
		exclusiveFilter: frame => {
			const matches = frame.text.match(/^[@|#].+$/)

			return !frame.text.trim() || matches
		}
	})

	// Conversion for removing all of tags
	text = sanitizeHtml(text, {
		allowedTags: [],
		allowedAttributes: {}
	})

	return text.replace(/\s+/g, ' ').trim()
}

export default {
	convertDescription: convertDescription,
	convertToText: convertToText
}
