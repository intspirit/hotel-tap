'use strict'

import {userService, departmentService} from '../services'

/* eslint-disable */

/**
 * Local data repository.
 * @type {{departments: Array, user: Array, areas: Array, tags: Array, equipment: Array, getDepartments: (()),
 *     getUsers: (()), getAreas: (()), getEquipment: (())}}
 */
const db = {
	departments: [],

	user: null,

	users: [],

	areas: [],

	tags: [],

	equipments: [],

	equipmentGroups: [],

	maintenanceTypes: [],

	/* eslint-enable */

	/**
	 * Get all hotel departments.
	 * @return {Promise}
	 */
	getDepartments: async() => {
		if (db.departments.length > 0) return Promise.resolve(db.departments)

		const data = await departmentService.getAll()
		db.departments = data.departments

		return db.departments
	},

	/**
	 * Get all hotel maintenance types.
	 * @return {Promise}
	 */
	getMaintenanceTypes: async() => {
		if (db.maintenanceTypes.length > 0) return Promise.resolve(db.maintenanceTypes)

		const data = await userService.getMaintenanceTypes()
		db.maintenanceTypes = data.mtypes

		return db.maintenanceTypes
	},

	/**
	 * Get all hotel users
	 * @return {Promise}
	 */
	getUsers: async() => {
		if (db.users.length > 0) return Promise.resolve(db.users)

		const users = await userService.getAll()
		db.users = users

		return db.users
	},

	/**
	 * Get all hotel users except current one
	 * @return {Promise}
	 */
	getOtherUsers: async() => {
		const user = await db.getUser()
		const users = await db.getUsers()
		const otherUsers = users.filter(x => x.id !== user.id)

		return Promise.resolve(otherUsers)
	},

	/**
	 * Get all hotel areas.
	 * @return {Promise}
	 */
	getAreas: async() => {
		if (db.areas.length > 0) return Promise.resolve(db.areas)

		const data = await userService.getAreas()
		db.areas = data.areas

		return db.areas
	},

	/**
	 * Get all hotel equipments.
	 * @return {Promise}
	 */
	getEquipments: async() => {
		if (db.equipments.length > 0) return Promise.resolve(db.equipments)

		const data = await userService.getEquipments()
		db.equipments = data.equipments

		return db.equipments
	},

	/**
	 * Get all hotel equipment groups.
	 * @return {Promise}
	 */
	getEquipmentGroups: async() => {
		if (db.equipmentGroups.length > 0) return Promise.resolve(db.equipmentGroups)

		const data = await userService.getEquipmentGroups()
		db.equipmentGroups = data.groups

		return db.equipmentGroups
	},

	/**
	 * Get all hotel tags.
	 * @return {Promise}
	 */
	getTags: async() => {
		if (db.tags.length > 0) return Promise.resolve(db.tags)

		const data = await userService.getTags()
		db.tags = data.tags

		return db.tags
	},

	/**
	 * Get current user.
	 * @return {Promise}
	 */
	getUser: async () => {
		if (db.user) return Promise.resolve(db.user)

		const data = await userService.getMe()
		db.user = data.user

		return db.user
	},

	/**
	 * Reset cache
	 * @return {void}
	 */
	reset: () => {
		db.departments = []
		db.areas = []
		db.tags = []
		db.equipments = []
		db.equipmentGroups = []
		db.maintenanceTypes = []
		db.users = []
		db.user = null
	},

	/**
	 * Reset tags cache
	 * @return {void}
	 */
	resetTags: () => {
		db.tags = []
	}
}

export default db
