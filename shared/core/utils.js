'use strict'

import pluralize from 'pluralize'
import constants from '../constants'
import moment from 'moment-timezone'
import _ from 'lodash'

export default {

	/**
	 * Empty func.
	 * @return {void}
	 * @description mostly used to defined a func in the defaultProps for React components.
	 */
	emptyFunc() {}, //eslint-disable-line no-empty-function

	getFullName(user) {
		return `${user.firstName} ${user.lastName}`
	},

	readers2string(readBy, postType) {
		if (!readBy || readBy.length === 0) return null

		if (readBy.length === 1) return `${readBy[0]} read this ${postType}`

		if (readBy.length === 2 && readBy[0] === 'You') return `${readBy[0]} and ${readBy[1]} read this ${postType}`

		if (readBy.length === 2) return `${readBy[0]} and 1 other read this ${postType}`

		let othersCount = readBy.length - (readBy[0] === 'You' ? 2 : 1)
		let others = pluralize('other', othersCount)
		if (readBy.length > 2 && readBy[0] === 'You')
			return `${readBy[0]}, ${readBy[1]} and ${othersCount} ${others} read this ${postType}`
		if (readBy.length > 2)
			return `${readBy[0]} and ${othersCount} ${others} read this ${postType}`

		return ''
	},

	isDepartmentBoard(board) {
		return board.includes(constants.boardTypes.DEPARTMENT)
	},

	assignBoard(boardType, boardId) {
		return `${boardType}/${boardId}`
	},

	formatDateTime(dateTime, timezone = 'UTC', withoutHours = false) {
		if (!moment(dateTime).isValid()) return 'Incorrect time format'

		const date = moment.tz(dateTime, timezone)
		const now = moment.utc().tz(timezone)
		const thisDay = now.clone().startOf('day')
		const endOfThisDay = now.clone().endOf('day')
		const thisYear = now.clone().startOf('year')
		const endOfThisYear = now.clone().endOf('year')

		if (date.isAfter(now)) {
			if (date.isSame(endOfThisDay, 'day'))
				return withoutHours ? `Today` : `Today at ${date.format('h:mma')}`

			if (date.isSame(endOfThisYear, 'year'))
				return withoutHours ? date.format('MMMM D') : date.format('MMMM D [at] h:mma')

			if (date.isAfter(endOfThisYear, 'year'))
				return withoutHours ? date.format('MMMM D, YYYY') : date.format('MMMM D, YYYY [at] h:mma')

			return date.format('MMMM D [at] h:mma')
		}

		if (date.isSame(thisDay, 'day')) return `${now.from(date, true)} ago`

		if (date.isSame(thisYear, 'year'))
			return withoutHours ? date.format('MMMM D') : date.format('MMMM D [at] h:mma')

		if (date.isBefore(thisYear, 'year')) return date.format('MMMM D, YYYY')

		return date.format('MMMM D [at] h:mma')
	},

	getComplaintResolutionKeyFromAbbreviation(abbr) {
		return _.findKey(constants.complaintResolutions, resolution => resolution === abbr)
	},

	/**
	 * Checks whether the date/time is in the past.
	 * @param {String} dateTime
	 * @returns {boolean}
	 */
	isBeforeToday (dateTime) {
		return dateTime && moment(dateTime).isBefore(moment().
			hour(0).
			minute(0).
			second(0))
	},

	/**
	 * Checks whether a task is overdue.
	 * @param {string} dateTime
	 * @returns {boolean}
	 */
	isOverdue(dateTime) {
		return dateTime && moment(dateTime).isBefore(moment())
	}

}
