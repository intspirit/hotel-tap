'use strict'

import db from './db'

/**
 * Application controller - controls application state and interaction among various components.
 */
export default {
	settings: {},

	user: {},
	hotel: {},

	todoComponent: null,
	feedComponent: null,
	navigationComponent: null,
	postsComponent: null,
	postComponent: null,
	tagsSelectorComponent: null,

	authenticated: false,
	jwtToken: '',

	setUser(user) {
		this.authenticated = true
		this.user = user
	},

	removeUser() {
		this.authenticated = false
		this.user = null
	},

	resetCache() {
		db.reset()
	},

	resetTagsCache() {
		db.resetTags()
	},

	setHotel(hotel) {
		this.hotel = hotel
	},

	removeHotel() {
		this.hotel = null
	},

	localDb: {}
}
