'use strict'

export default {

	/**
	 * Merges navigation options with the number of unread items.
	 * @param {Array} options
	 * @param {Array} unreadCounts
	 * @returns {Array}
	 */
	setUnreadCounts: function setUnreadCounts(options, unreadCounts) {
		return options.map(option => {
			if (option.type === 'general' || !unreadCounts || unreadCounts.length === 0) {
				option.count = 0
			} else {
				let optionCount = unreadCounts.find(count => count.type === option.type && count.id === option.id)
				option.count = optionCount ? optionCount.count : 0
			}

			return option
		})

	}
}
