import moment from 'moment'
import {sortCriteria} from './constants'

function sortByDate(lhs, rhs, desc = false) {
	let date1 = moment(desc ? lhs : rhs),
		date2 = moment(desc ? rhs : lhs)

	if (date1.isBefore(date2)) {
		return 1
	} else if (date2.isBefore(date1)) {
		return -1
	}

	return 0
}

export function setSortCriteria(field, desc = false) {
	switch (field) {
		case sortCriteria.BY_FLAG_STATUS:
			return (lhs, rhs) => {
				if (lhs.flag.set && rhs.flag.set) {
					return sortByDate(lhs.flag.expirationDateTime, rhs.flag.expirationDateTime)
				} else if (lhs.flag.set && !rhs.flag.set) {
					return -1
				} else if (!lhs.flag.set && rhs.flag.set) {
					return 1
				}

				return sortByDate(lhs.orderDateTimeUtc, rhs.orderDateTimeUtc, desc)
			}
		default:
			break
	}

	return false
}
