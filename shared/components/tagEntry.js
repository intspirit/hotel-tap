'use strict'

import {tagService} from '../../shared/services'

/**
 * Shared logic for tagEntry components.
 */
class TagEntryComponent {
	constructor(appController, tagEntry) {
		this.group = null

		this.appController = appController
		this.tagEntry = tagEntry

		this.appController.TagEntryComponent = tagEntry

		this.state = this.getDefaultState()
	}

	getDefaultState() {
		return {
			name: ''
		}
	}

	setGroup(group) {
		this.group = group
	}

	validate() {
		if (this.state.name.trim() === '')
			return 'Please enter a tag name'

		return null
	}

	async save() {
		const tag = {
			name: this.state.name.trim(),
			type: this.group.type,
			typeId: this.group.id
		}

		return await tagService.insert(tag)
	}

	setPostId(postId) {
		this.postId = postId
	}

	nameChanged(name) {
		this.state.name = name.trim() || ''
		this.tagEntry.updateState({name})
	}
}

export default TagEntryComponent
