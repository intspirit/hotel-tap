'use strict'

import _ from 'lodash'
import constants from '../constants'
import utils from '../core/utils'
import {postService} from '../../shared/services'
import moment from 'moment'
import htmlConverter from '../../shared/core/htmlConverter'

const isNewPost = postId => postId === -1

/**
 * Shared logic for PostEntry components.
 */
class PostEntryComponent {
	constructor(appController, postEntry, props) {
		this.selectedTags = props.tags || []
		this.selectedUsers = props.users || []
		this.board = props.board || ''
		this.originallyCreatedBy = props.createdBy || -1

		this.tagSelectionCancelled = true
		this.userSelectionCancelled = true
		//noinspection JSUnresolvedVariable
		this.appController = appController
		this.postEntry = postEntry

		this.appController.postEntryComponent = postEntry

		this.state = this.getDefaultState(props)
	}

	getDefaultState(props) {
		return {
			id: props.id || -1,
			typeId: props.typeId || constants.postTypes.NOTE,
			description: props.description || '',
			tagNames: '',
			userNames: '',
			privatePost: props.privatePost || false,
			guestComplaint: props.guestComplaint || false,
			subject: props.subject || '',
			assignmentType: props.assignmentType || '',
			assignmentId: props.assignmentId || -1,
			attachments: props.attachments || [],
			dueDate: props.dueDate || null,
			dueTime: props.dueTime || null,
			dueDateTime: props.dueDateTime || null
		}
	}

	validate() {
		if (this.isDepartmentBoard &&
			(this.invalidDepartmentsTags(this.selectedTags) || this.invalidLocationTags(this.selectedTags)))
			return 'Please select at least one Department and one Location tag to create a note or task'

		if (this.state.typeId === constants.postTypes.TASK) {
			if (this.state.assignmentId === -1)
				return 'Please select Assign to Employee or Department'

			if (this.state.subject.trim() === '')
				return 'Please enter Subject'

			if (this.state.dueDateTime && this.invalidDueDateTime(this.state.dueDateTime))
				return 'Please enter time in future'
		}

		const descriptionText = htmlConverter.convertToText(this.state.description)
		if (descriptionText.trim() === '')
			return 'Please enter notes'

		return null
	}

	invalidDueDateTime(dueDateTime) {
		return moment(dueDateTime).isBefore(moment().seconds(0))
	}

	invalidDepartmentsTags(tags) {
		return !tags.some(x =>
				x.type === constants.tagLegacyType.DEPARTMENT ||
				x.type === constants.tagLegacyType.MAINTENANCE)
	}

	invalidLocationTags(tags) {
		return !tags.some(x => x.type === constants.tagLegacyType.AREA)
	}

	reset() {
		this.selectedTags = []
		this.selectedUsers = []
		this.originallyCreatedBy = -1
		this.state = this.getDefaultState({})
		this.postEntry.updateState(this.state)
	}

	async save() {
		const postId = this.state.id
		const post = {
			typeId: this.state.typeId,
			description: this.state.description,
			privatePost: this.state.privatePost,
			guestComplaint: this.state.guestComplaint,
			tags: this.selectedTags.
				filter(x => !x.disabled).
				map(x => x.id),
			ccList: this.selectedUsers.
				filter(x => !x.disabled).
				map(x => x.id),
			departments: _.uniq(this.selectedTags.
				filter(x => x.type === constants.tagLegacyType.DEPARTMENT).
				map(x => parseInt(x.groupId.replace('dept-', ''), 10))),
			maintenanceTypeList: _.uniq(this.selectedTags.
				filter(x => x.type === constants.tagLegacyType.MAINTENANCE).
				map(x => parseInt(x.groupId.replace('mtype-', ''), 10))),
			board: this.board,
			assignmentType: this.state.assignmentType,
			assignmentId: parseInt(this.state.assignmentId, 10),
			subject: this.state.subject,
			dueDateTime: this.state.dueDateTime
		}
		const attachments = this.state.attachments.map(x => ({tmpFilePath: x.tmpFilePath}))

		return isNewPost(postId) ?
			await this.insert(post, attachments) :
			await this.update(postId, post, attachments)
	}

	async insert(post, attachments) {
		const serviceResponse = await postService.insert({
			post: post,
			attachments: attachments
		})

		if (serviceResponse.inserted) {
			return {
				inserted: true,
				postId: serviceResponse.id
			}
		}

		throw new Error('Post was not inserted.')
	}

	async update(postId, props, attachments) {
		const updatedProps = Object.assign({
			originallyCreatedBy: this.originallyCreatedBy
		}, props)

		if ((attachments || []).length !== 0) {
			updatedProps.attachments = attachments
		}

		const serviceResponse = await postService.update(postId, updatedProps)
		if (serviceResponse.updated) {
			return {
				updated: true,
				postId: postId
			}
		}

		throw new Error('Post was not updated.')
	}

	setBoard(board) {
		this.board = board
	}

	componentDidMount() {
		this.selectedTags = []
		this.postEntry.setState({
			description: '',
			descriptionHeight: 100,
			tagNames: ''
		})
	}

	selectedTagsChanged(selectedTags) {
		if (this.postEntry.updateSelectedTags) this.postEntry.updateSelectedTags(selectedTags)
	}

	descriptionChanged(description) {
		this.state.description = description.trim()
		this.postEntry.updateState({description})
	}

	privatePostChanged(privatePost) {
		this.state.privatePost = privatePost
		this.postEntry.updateState({privatePost})

		if (privatePost) {
			this.selectedTags = this.selectedTags.filter(x => x.type !== constants.tagLegacyType.DEPARTMENT)
			this.selectedTagsChanged(this.selectedTags)
		}
	}

	guestComplaintChanged(guestComplaint) {
		this.state.guestComplaint = guestComplaint
		this.postEntry.updateState({guestComplaint})
	}

	typeIdChanged(typeId) {
		this.state.typeId = typeId

		if (typeId === constants.postTypes.TASK) {
			this.state.privatePost = false
		}

		const newState = typeId === constants.postTypes.TASK ?
			{
				typeId,
				privatePost: false
			} :
			{
				typeId
			}

		this.postEntry.updateState(newState)
	}

	subjectChanged(subject) {
		this.state.subject = subject.trim()
		this.postEntry.updateState({subject})
	}

	employeeIdChanged(employeeId) {
		this.state.assignmentType = constants.assignmentTypes.EMPLOYEE
		this.state.assignmentId = employeeId

		this.postEntry.updateState({
			assignmentType: this.state.assignmentType,
			assignmentId: this.state.assignmentId
		})
	}

	departmentIdChanged(departmentId) {
		this.state.assignmentType = constants.assignmentTypes.DEPARTMENT
		this.state.assignmentId = departmentId

		this.postEntry.updateState({
			assignmentType: this.state.assignmentType,
			assignmentId: this.state.assignmentId
		})
	}

	dueDateChanged(dueDateString) {
		const dueDate = moment(dueDateString)
		let dueTime = this.state.dueTime

		if (dueTime === null) {
			dueTime = dueDate.endOf('day')
			this.state.dueTime = dueTime
		}

		this.state.dueDate = dueDate
		this.dueDateTimeChanged()
		this.postEntry.updateState({dueDate, dueTime})
	}

	dueTimeChanged(dueTimeString) {
		const dueTime = moment(dueTimeString)

		this.state.dueTime = dueTime
		this.dueDateTimeChanged()
		this.postEntry.updateState({dueTime})
	}

	dueDateTimeChanged(dueDateTime) {
		if (dueDateTime) {
			this.state.dueDateTime = dueDateTime
			this.postEntry.updateState({dueDateTime})
		} else {
			const dueDate = this.state.dueDate
			const dueTime = this.state.dueTime
			const dateTime = dueDate.isValid() && dueTime.isValid() ?
				`${dueDate.format('YYYY-MM-DD')} ${dueTime.format('HH:mm:ss')}` :
				null
			this.state.dueDateTime = dateTime
		}
	}

	attachmentsChanged(attachments) {
		this.state.attachments = attachments || []
	}

	get tagNames() {
		return this.selectedTags.map(x => `${x.groupName} ${x.tagName}`).join(', ')
	}

	get userNames() {
		return this.selectedUsers.map(x => x.name).join(', ')
	}

	get isMaintenanceIssue() {
		return this.state.typeId === constants.postTypes.NOTE &&
			this.selectedTags.filter(x => x.type === constants.tagLegacyType.MAINTENANCE).length > 0
	}

	get isDepartmentBoard() {
		return utils.isDepartmentBoard(this.board)
	}
}

export default PostEntryComponent
