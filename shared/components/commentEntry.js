'use strict'

import _ from 'lodash'
import {postService} from '../../shared/services'
import htmlConverter from '../../shared/core/htmlConverter'
import constants from '../../shared/constants'

/**
 * Shared logic for commentEntry components.
 */
class CommentEntryComponent {
	constructor(appController, commentEntry) {
		this.postId = 0

		this.selectedTags = []
		this.selectedUsers = []

		this.appController = appController
		this.commentEntry = commentEntry

		this.appController.CommentEntryComponent = commentEntry

		this.state = this.getDefaultState()
	}

	getDefaultState() {
		return {
			description: '',
			attachments: []
		}
	}

	reset() {
		this.state = this.getDefaultState()
		this.commentEntry.updateState(this.state)
	}

	validate() {
		const descriptionText = htmlConverter.convertToText(this.state.description)
		if (descriptionText.trim() === '' && this.state.attachments.length === 0)
			return 'Please enter Comment or add Attachments'

		return null
	}

	async save() {
		const comment = {
			description: this.state.description,
			tags: this.selectedTags.map(x => x.id),
			ccList: this.selectedUsers.map(x => x.id),
			departments: _.uniq(this.selectedTags.
				filter(x => x.type === constants.tagLegacyType.DEPARTMENT).
				map(x => parseInt(x.groupId.replace('dept-', ''), 10))),
			maintenanceTypeList: _.uniq(this.selectedTags.
				filter(x => x.type === constants.tagLegacyType.MAINTENANCE).
				map(x => parseInt(x.groupId.replace('mtype-', ''), 10)))
		}

		await postService.insertComment(this.postId, {
			comment: comment,
			attachments: this.state.attachments.map(x => ({tmpFilePath: x.tmpFilePath}))
		})
	}

	setPostId(postId) {
		this.postId = postId
	}

	descriptionChanged(description) {
		this.state.description = description.trim() || ''
		this.commentEntry.updateState({
			description: this.state.description
		})
	}

	attachmentsChanged(attachments) {
		this.state.attachments = attachments || []
	}
}

export default CommentEntryComponent
