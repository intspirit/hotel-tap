'use strict'

import serviceBase from './serviceBase'

export default {

	/**
	 * Gets all inspections
	 * @returns {*}
	 */
	getAll: () => serviceBase.get('/api/inspections'),

	/**
	 * Gets an inspection by id
	 * @param {int} id
	 * @returns {*}
	 */
	getById: id => serviceBase.get(`/api/inspections/${id}`),

	/**
	 * Gets schedules associated with a given inspection template
	 * @param {int} inspectionId
	 * @returns {*}
	 */
	getSchedules: inspectionId => serviceBase.get(`/api/inspections/${inspectionId}/schedules`),

	/**
	 * Inserts inspection
	 * @param {object} req
	 * @returns {*}
	 */
	insert: req => serviceBase.post(`/api/inspections`, req),

	/**
	 * Updates inspection
	 * @param {int} id
	 * @param {object} req
	 * @returns {*}
	 */
	update: (id, req) => serviceBase.put(`/api/inspections/${id}`, req)
}
