'use strict'

import serviceBase from './serviceBase'

const reportService = {

	/**
	 * Gets all hotel reports.
	 *
	 * @return {Array}
	 */
	getAll: () => serviceBase.get(`/api/reports`),

	/**
	 * Gets preventive maintenance log by report id.
	 *
	 * @param {int} id
	 * @return {Object}
	 */
	getPreventiveMaintenanceLogsByReportId: (id) => serviceBase.get(`/api/reports/pm/${id}`),

	/**
	 * Gets check list log by report id.
	 *
	 * @param {int} id
	 * @return {Object}
	 */
	getChecklistLogsByReportId: (id) => serviceBase.get(`/api/reports/check-lists/${id}`)

}

export default reportService
