'use strict'

import serviceBase from './serviceBase'

export default {

	/**
	 * Gets all documents.
	 * @returns {*}
	 */
	getAll: () => serviceBase.get('/api/documents')

}
