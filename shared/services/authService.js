'use strict'

import serviceBase from './serviceBase'

const authService = {

	/**
	 * Login User
	 * @param {object} credentials
	 * @returns {*}
	 */
	login: (credentials) => serviceBase.post(`/api/login`, credentials),


	/**
	 * Login User to the PHP app
	 * @param {object} credentials
	 * @returns {*}
	 */
	loginAdmin: (credentials) => serviceBase.post(`/api/access/admin`, credentials)
}

export default authService
