'use strict'

import serviceBase from './serviceBase'

const tagService = {

	/**
	 * Inserts a tag.
	 *
	 * @param {object} req
	 * @return {*}
	 */
	insert: (req) => serviceBase.post('/api/tags', req)
}

export default tagService
