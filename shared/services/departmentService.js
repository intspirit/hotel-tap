'use strict'

import serviceBase from './serviceBase'

const departmentService = {

	/**
	 * Gets all hotel departments.
	 * @return {Array}
	 */
	getAll: () => serviceBase.get(`/api/departments`)

}
export default departmentService
