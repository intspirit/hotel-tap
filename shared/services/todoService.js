'use strict'

import serviceBase from './serviceBase'

const todoService = {

	/**
	 * Gets navigation items
	 * @returns {*}
	 */
	getCount: () => serviceBase.get(`/api/todo/count`),

	/**
	 * Gets to do list - temp method
	 * @param {string} type - {department || employee}
	 * @param {string} id - {department id || 'me' for the current employee}
	 * @param {string} sortType
	 * @returns {*}
	 */
	getList: (type, id = '', sortType = '') => serviceBase.get(`/api/todo/${type}/${id}?sortType=${sortType}`)
}

if (typeof module !== 'undefined') module.exports = todoService
