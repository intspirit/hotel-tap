'use strict'

import serviceBase from './serviceBase'

const appService = {

	/**
	 * Gets application settings
	 * @returns {*}
	 */
	getSettings: () => serviceBase.get(`/api/guest/app/settings`)
}

export default appService
