'use strict'

import serviceBase from './serviceBase'

export default {

	/**
	 * Gets all PM.
	 * @returns {*}
	*/
	getAll: () => serviceBase.get('/api/pm'),

	/**
	 * Gets PM by id.
	 * @param {int} id
	 * @returns {*}
	*/
	getById: (id) => serviceBase.get(`/api/pm/${id}`),

	/**
	 * Gets schedule for a year.
	 * @param {int} id
	 * @param {int} year
	 * @returns {*}
	*/
	getSchedule: (id, year) => serviceBase.get(`/api/pm/${id}/schedules/${year}`),

	/**
	 * Updates pm.
	 * @param {int} id
	 * @param {Object} req
	 * @returns {*}
	 */
	update: (id, req) => serviceBase.put(`/api/pm/${id}`, req),

	/**
	 * Inserts pm.
	 * @param {Object} req
	 * @returns {*}
	 */
	insert: (req) => serviceBase.post(`/api/pm`, req)
}
