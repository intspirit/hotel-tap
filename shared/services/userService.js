'use strict'

import serviceBase from './serviceBase'

const userService = {

	/**
	 * Gets current user
	 * @returns {*}
	 */
	getMe: () => serviceBase.get(`/api/users/me`),

	/**
	 * Gets navigation items
	 * @returns {*}
	 */
	getNavigation: () => serviceBase.get(`/api/users/me/nav`),

	/**
	 * Gets hotels for user
	 * @returns {*}
	 */
	getHotels: () => serviceBase.get(`/api/users/me/hotels?details=all`),

	/**
	 * Gets few information of hotels for user
	 * @returns {*}
	 */
	getHotelsFewDetails: () => serviceBase.get(`/api/users/me/hotels?details=few`),

	/**
	 * Login current User to selected hotel
	 * @param {integer} hotelId
	 * @returns {*}
	 */
	switchHotel: (hotelId) => serviceBase.post(`/api/user/open-hotel`, {hotelId: hotelId}),

	/**
	 * Gets departments
	 * @returns {*}
	 */
	getDepartments: () => serviceBase.get(`/api/users/me/departments`),

	/**
	 * Gets tags
	 * @returns {Array}
	 */
	getTags: () => serviceBase.get(`/api/tags`),

	/**
	 * Gets areas
	 * @returns {Array}
	 */
	getAreas: () => serviceBase.get(`/api/areas`),

	/**
	 * Gets maintenance types
	 * @returns {Array}
	 */
	getMaintenanceTypes: () => serviceBase.get(`/api/maintenance`),

	/**
	 * Gets equipment groups
	 * @returns {Array}
	 */
	getEquipmentGroups: () => serviceBase.get(`/api/equipmentGroups`),

	/**
	 * Gets equipments
	 * @returns {Array}
	 */
	getEquipments: () => serviceBase.get(`/api/equipments`),

	/**
	 * Gets all hotel users.
	 * @return {Array}
	 */
	getAll: () => serviceBase.get(`/api/users`),

	/**
	 * Updates user profile info
	 * @param {object} data
	 * @returns {*}
	 */
	update: (data) => serviceBase.put(`/api/users/me`, data)

}
export default userService
