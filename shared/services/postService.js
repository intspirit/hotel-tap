'use strict'

import serviceBase from './serviceBase'

const postService = {

	/**
	 * Gets navigation items
	 * @returns {*}
	*/
	getUnreadCount: () => serviceBase.get(`/api/posts/unread/count`),

	/**
	 * Gets post items
	 * @param {int} id
	 * @param {int} nextPageToken
	 * @returns {*}
	*/
	getByDepartmentId: (id, nextPageToken) =>
		serviceBase.get(`/api/posts/departments/${id}${nextPageToken ? `?nextPageToken=${nextPageToken}` : ''}`),

	/**
	 * Gets post items
	 * @param {int} id
	 * @param {int} nextPageToken
	 * @returns {*}
	 */
	getByEmployeeId: (id, nextPageToken) =>
		serviceBase.get(`/api/posts/employee/${id}${nextPageToken ? `?nextPageToken=${nextPageToken}` : ''}`),

	/**
	 * Gets post items
	 * @param {int} id
	 * @param {int} nextPageToken
	 * @returns {*}
	*/
	getByTagId: (id, nextPageToken) =>
		serviceBase.get(`/api/posts/tags/${id}${nextPageToken ? `?nextPageToken=${nextPageToken}` : ''}`),

	/**
	 * Update post item
	 * @param {int} id
	 * @param {Object} req
	 * @returns {*}
	*/
	update: (id, req) => serviceBase.put(`/api/posts/${id}`, req),

	/**
	 * Inserts a comment.
	 * @param {int} id
	 * @param {object} req
	 * @returns {*}
	 */
	insertComment: (id, req) => serviceBase.post(`/api/posts/${id}/comments`, req),

	/**
	 * Gets post by id
	 * @param {int} id
	 * @returns {*}
	 */
	getPostById: (id) => serviceBase.get(`/api/posts/${id}`),

	/**
	 * Inserts a post.
	 * @param {object} req
	 * @return {*}
	 */
	insert: (req) => serviceBase.post('/api/posts', req),

	/**
	 * Gets posts with guest complaints
	 * @param {int} nextPageToken
	 * @returns {*}
	 */
	getComplaints: (nextPageToken) =>
		serviceBase.get(`/api/posts/complaints${nextPageToken ? `?nextPageToken=${nextPageToken}` : ''}`)
}

export default postService
