//TODO: vvs p1 do not leak server contstants to the client.
/**
 * Application constants.
 * @type {{role: {SUPER_ADMIN: number, ADMIN: number, MANAGER: number, EMPLOYEE: number}}}
 */
module.exports = {

	/**
	 * User roles.
	 */
	role: {
		SUPER_ADMIN: 1,
		ADMIN: 2,
		MANAGER: 3,
		EMPLOYEE: 4
	},
	defaultImageFileName: {
		EMPLOYEE: 'default.png',
		DEPARTMENT: 'icons/iconD.png'
	},
	interval: {
		ONE_MINUTE: 60000
	},
	navigationOptionType: {
		EMPLOYEE: 'employee',
		DEPARTMENT: 'department',
		GENERAL: 'general'
	},
	postReadStatus: {
		READ: 'r',
		UNREAD: 'u'
	},
	postTypes: {
		NOTE: 'note',
		TASK: 'task',
		COMMENT: 'comm',
		PM: 'pm',
		PM_SCHEDULE: 'pms',
		PM_TASK: 'pmt',
		PM_TASK_COMPLETED: 'pmtc',
		CHECKLIST: 'chl',
		CHECKLIST_SCHEDULE: 'chls',
		INSPECTION: 'insp',
		INSPECTION_TASK: 'inspt',
		INSPECTION_SCHEDULE: 'insps'
	},
	attachmentTypes: {
		IMAGE: 'image'
	},
	attachmentSources: {
		COMMENTS: 'comments',
		NOTES: 'notes',
		TASKS: 'tasks'
	},
	hiddenUserId: -10,
	comments: {
		DEFAULT_COMMENT_COUNT: 2
	},
	readers: {
		DEFAULT_READERS_COUNT: 2
	},
	postedFrom: {
		EMPLOYEE: 'emp',
		DEPARTMENT: 'dep'
	},
	userImageDimensions: {
		SMALL: 30,
		NORMAL: 40,
		LARGE: 80
	},
	pageSize: {
		BOARD: 50
	},
	sortCriteria: {
		BY_FLAG_STATUS: 'byFlagStatus'
	},
	userType: {
		EMPLOYEE: 'empl',
		ADMIN: 'admin'
	},
	apiErrorReason: {
		invalidCredentials: 1,
		invalidToken: 2
	},
	assignmentTypes: {
		EMPLOYEE: 'emp',
		DEPARTMENT: 'dep'
	},
	taskStatus: {
		DONE: 'done',
		OPEN: 'open'
	},
	guestComplaint: '#GuestComplaint',
	defaultUserId: 'me',
	mobileMainNavigationTab: {
		BOARDS: 'boards',
		ADD_POST: 'addPost',
		TO_DO: 'todo'
	},
	UID: 'uId',
	userSettingsType: {
		PROFILE: 'profile',
		ACCOUNT: 'account',
		NOTIFICATIONS: 'notifications'
	},
	tagLegacyType: {
		DEPARTMENT: 'dept',
		MAINTENANCE: 'mtype',
		AREA: 'area',
		EQUIPMENT: 'equip'
	},
	todoSortTypes: {
		TODO_TYPE: 'todoType',
		DUE_DATETIME: 'dueDatetime'
	},
	todoGroupNames: {
		OVERDUE: 'overdue',
		TODAY: 'today',
		FUTURE: 'future',

		TASKS: 'tasks',
		CHECKLIST_SCHEDULE: 'checklists',
		INSPECTIONS: 'inspections',
		PM: 'pm'
	},
	postUser: {
		CREATOR: 'cr',
		CC: 'cc',
		COMMENTATOR: 'com',
		EXECUTOR: 'exe'
	},
	boardTypes: {
		HOME: 'home',
		EMPLOYEE: 'employee',
		DEPARTMENT: 'department',
		PM: 'pm',
		CHECKLIST_SCHEDULE: 'cl',
		INSPECTIONS: 'insp',
		DASHBOARD: 'dasboard',
		REPORTS: 'reports',
		DOCUMENTS: 'docs',
		HELP: 'help',
		TAG: 'tag',
		SRP: 'srp'
	},
	specialLegacyTypes: {
		MAINTENANCE: 'maintenance'
	},
	emptyDueDateTimeMessage: 'Any date and time',
	emptyTaskAssignmentMessage: 'Not assigned',
	job: {
		TRANSLATION: 'translate',
		NOTIFICATION: 'notif',
		NOTIFICATION_EMAIL: 'notif-email'
	},
	jobStatus: {
		OPEN: 'open',
		DONE: 'done',
		FAILED: 'fail'
	},
	reports: {
		DAYS_FROM_NOW: 90
	},
	complaintResolutionNames: {
		UNRESOLVED: 'Unresolved',
		GUEST_HAPPY: 'Guest is very happy',
		GUEST_SATISFIED: 'Guest is satisfied',
		GUEST_NOT_SATISFIED: 'Guest is not satisfied',
		UNKNOWN: 'Unknown'
	},
	complaintResolutions: {
		UNRESOLVED: 'unresolved',
		GUEST_HAPPY: 'happy',
		GUEST_SATISFIED: 'satisf',
		GUEST_NOT_SATISFIED: 'unsatisf',
		UNKNOWN: 'Unknown'
	},
	GAEvents: {
		categories: {
			POST: 'Post',
			TODO: ' To do'
		},
		actions: {
			POST_NOTE: 'Post Note',
			POST_TASK: 'Post Task',
			CONVERT_TO_TASK: 'New Convert to Task',
			CONVERT_TASK_CANCEL: 'Cancel',
			NOTE_TO_TASK: 'Convert to Task',
			TASK_DONE: 'Task Done',
			MARK_READ: 'Mark as Read',
			FLAG: 'Flag',
			COMMENT: 'Comment',
			COMMENT_ADDED: 'Comment Added',
			TODO_COMPLETED: 'Task Completed',
			TODO_SORT_CHANGED: 'Sort Changed'
		},
		labels: {
			READ: 'Read',
			UNREAD: 'Unread',
			FLAGGED: 'Flagged',
			UNFLAGGED: 'Unflagged',
			DONE: 'Done',
			NOT_DONE: 'Not Done',
			BY_DATE: 'By Date',
			BY_TYPE: 'Bt Type'
		}
	},
	keyboardKey: {
		ENTER: 13
	},
	changeStatus: {
		INSERT: 'i',
		DELETE: 'd',
		UPDATE: 'u'
	},
	emailTemplates: {
		POST_CREATED: 'post-created',
		COMMENT_CREATED: 'comment-created'
	},
	inspectionPoints: {
		MAX: 10,
		MIN: 1
	},
	inspectionPassingScore: {
		MIN: 10,
		MAX: 100,
		STEP: 5,
		DEFAULT: 90
	},
	locationType: {
		AREA: 'area',
		ROOM: 'room'
	}
}
